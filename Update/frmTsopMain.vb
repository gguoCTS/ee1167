
Option Explicit On
Option Strict Off
Imports System.Reflection
Imports System.Configuration

Public Class frmTsopMain

  Private mBackgroundColor As Color
  Private Sub btnAbortTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAbortTest.Click
    gControlFlags.AbortTest = True
  End Sub

  Private Sub cboParameterSets_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cboParameterSets.KeyUp
    Try
      If e.KeyValue = Keys.Enter Then
        Me.Cursor = Cursors.WaitCursor

        cboParameterSets.Enabled = False

        gtmrLotShift.Stop()

        gUserTaskListFilter = TaskListFilterEnum.None

        'Display Message to user
        Me.StatusLabel1.Text = "Loading Selected Parameter Set"
        Call LogActivity(Me.StatusLabel1.Text)

        'set defaults based on user selection
        If UCase(lblLotType.Text) = "PROD" Then
          Call SetProductionTSOPDefaults()
        Else
          Call SetNonProductionTSOPDefaults()
        End If
        If Not (gAnomaly Is Nothing) Then
          Throw New TsopAnomalyException
        End If

        ''reinitialize variables
        'gControlFlags.StartScan = False
        'gControlFlags.ManualScan = False

        'tabResults.TabPages.Clear()

        gDatabase.ProcessParameterID = cboParameterSets.SelectedValue

        'Get ProductID
        Call gDatabase.GetProductID()
        If Not (gDatabase.Anomaly Is Nothing) Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        'Load Parameters from Database
        Call LoadParameters()
        If Not (gAnomaly Is Nothing) Then
          Throw New TsopAnomalyException
        End If

        'gControlFlags.UsesOverviewScreen = gControlAndInterface.UsesOverviewScreen

        gdtAppTaskList = gDatabase.GetTaskListParameterArray("TaskList", "TaskList")
        '"TaskIndex"
        '"ModuleName"
        '"MethodName"
        If Not gDatabase.Anomaly Is Nothing Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        'Copy task list table structure to user filtered task list table
        gdtFilteredTaskList = gdtAppTaskList.Clone

        '====================================================================================================
        'Initializtion From ParameterSet
        Call ExecuteTaskListParamSetInit(TaskListFilterEnum.ParamSetInit)
        If Not (gAnomaly Is Nothing) Then
          Throw New TsopAnomalyException
        End If

        'enable menus
        MenuStripMain.Enabled = True

        'Update Status LED
        UpdatePassFailLED(Metric.PassFailStatusEnum.NoData)

        gControlFlags.SetupDone = True
        gControlFlags.ParametersLoaded = True

        gtmrLotShift.Start()

        cboParameterSets.Enabled = True

        'Display Message to user
        Me.StatusLabel1.Text = "Waiting on Start Scan"
        Call LogActivity(Me.StatusLabel1.Text)
        StatusLabel2.Text = ""
        Application.DoEvents()


      Else
        Call cboParameterSets.Focus()
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      LogAnomaly()
      Me.StatusLabel1.Text = "Error Loading Selected Parameter Set"
      cboParameterSets.Enabled = True
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      LogAnomaly()
      Me.StatusLabel1.Text = "Error Loading Selected Parameter Set"
      cboParameterSets.Enabled = True
    Finally
      Me.Cursor = Cursors.Arrow
    End Try
  End Sub

  Public Sub ConfigureLotNamesMenu()
    Dim miMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim drLotName As DataRow
    Try
      mnuLotNames.DropDownItems.Clear()
      For Each drLotName In gdtLotNames.Rows
        miMenuItem = New System.Windows.Forms.ToolStripMenuItem
        miMenuItem.Enabled = True
        'miMenuItem.CheckOnClick = True
        miMenuItem.Text = drLotName.Item("LotNameRun")

        If drLotName.Item("LotID").ToString = gDatabase.LotID.ToString Then
          lblLotName.Text = miMenuItem.Text
        End If

        AddHandler miMenuItem.Click, AddressOf LotName_Click

        'add to drop down menu
        mnuLotNames.DropDownItems.Add(miMenuItem)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Private Sub frmTsopMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    Try
      Call ExecuteTaskListTsopEnd(TaskListFilterEnum.TsopEnd)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      Application.Exit()
      Environment.Exit(0)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call DisplayAnomaly()
      Application.Exit()
      Environment.Exit(0)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
      Application.Exit()
      Environment.Exit(0)
    End Try
  End Sub

  Private Sub frmTsopMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    Try
      mBackgroundColor = Me.BackColor
      Me.Cursor = Cursors.WaitCursor

      Me.Hide()

      'Disable menus
      MenuStripMain.Enabled = False

      StatusLabel2.Text = ""

      Call modAppInitialize.AppInitialize()
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      Me.Text = Me.Text & "  Version " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Build & "." & My.Application.Info.Version.Revision

      'Update Status LED
      UpdatePassFailLED(Metric.PassFailStatusEnum.NoData)

      Me.Show()

      'MenuStripMain.Enabled = True
      mnuFunction.Enabled = False
      mnuTool.Enabled = False
      mnuExternalTestMethods.Visible = False
      mnuExternalTestMethods.Enabled = False
      mnuIncrementLotRun.Enabled = False

      'clear tab pages
      tabResults.TabPages.Clear()

      Call SetupLotTypeMenuItems()

      'Add Production Set Default menu item
      mnuOptions.DropDownItems.Insert(0, mnuOptionsProductionSetDefault)
      'Set Production Set Default menu item value
      mnuOptionsProductionSetDefault.Checked = gblnProductionSet
      'Set Production Set Default menu options
      mnuOptionsProductionSetDefault.CheckOnClick = True
      'Create Production Set Default Selection Handler
      AddHandler mnuOptionsProductionSetDefault.Click, AddressOf mnuOptionsProductionSetDefault_Click

      Call LoadParameterSets()
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If


      gDataGrids.Clear()
      'grpProgSummary.Visible = False

      '\/EXTERNAL TEST METHODS
      'mnuExternalTestMethods.Enabled = True
      'lblExternalTestMethodsSelected.Enabled = True

      'Get External Test Method Relationships Collection From Database
      gExternalTestMethodRelationships = gDatabase.GetRelationsToExternalTestMethodsCollection

      'Build External Test Methods Menu
      'Before Test Methods
      mnuExternalTestMethodsBefore.DropDownItems _
        .Add(mnuExternalTestMethodsBeforeComboBox)
      'mnuExternalTestMethodsBefore.CheckOnClick = True
      mnuExternalTestMethodsBeforeComboBox.ComboBox.Width = 300
      AddHandler mnuExternalTestMethodsBeforeComboBox.SelectedIndexChanged,
        AddressOf BeforeTestMethodsComboBox_SelectedIndexChanged

      'During Test Methods
      mnuExternalTestMethodsDuring.DropDownItems _
        .Add(mnuExternalTestMethodsDuringComboBox)
      mnuExternalTestMethodsDuringInterval.DropDownItems _
        .Add(mnuExternalTestMethodsDuringIntervalTextBox)
      mnuExternalTestMethodsDuring.DropDownItems _
        .Add(mnuExternalTestMethodsDuringInterval)
      mnuExternalTestMethodsDuringComboBox.ComboBox.Width = 300
      'AddHandler mnuExternalTestMethodsDuringComboBox.SelectedIndexChanged, _
      '  AddressOf DuringTestMethodsComboBox_SelectedIndexChanged
      'AddHandler mnuExternalTestMethodsDuringIntervalTextBox.TextChanged, _
      '  AddressOf DuringTestMethodsDurationTextBox_TextChanged
      AddHandler mnuExternalTestMethodsDuringIntervalTextBox.KeyUp,
        AddressOf DuringTestMethodsDurationTextBox_Enter
      AddHandler mnuExternalTestMethodsDuringIntervalTextBox.LostFocus,
        AddressOf DuringTestMethodsDurationTextBox_LostFocus

      'After Test Methods
      mnuExternalTestMethodsAfter.DropDownItems _
        .Add(mnuExternalTestMethodsAfterComboBox)
      mnuExternalTestMethodsAfterComboBox.ComboBox.Width = 300
      AddHandler mnuExternalTestMethodsAfterComboBox.SelectedIndexChanged,
        AddressOf AfterTestMethodsComboBox_SelectedIndexChanged

      AddHandler mnuExternalTestMethodsNone.Click,
        AddressOf NoneExternalTestMethods_Click

      mnuExternalTestMethods.DropDownItems.Add(mnuExternalTestMethodsBefore)
      mnuExternalTestMethods.DropDownItems.Add(mnuExternalTestMethodsDuring)
      mnuExternalTestMethods.DropDownItems.Add(mnuExternalTestMethodsAfter)
      mnuExternalTestMethods.DropDownItems.Add(mnuExternalTestMethodsNone)
      '/\EXTERNAL TEST METHODS

      mnuFunction.Enabled = True
      mnuTool.Enabled = True
      'mnuExternalTestMethods.Enabled = True

      'Initialize TSOP From Task List Section
      Call ExecuteTaskListTsopInit()
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      'set focus to Parameter Set box
      Me.cboParameterSets.Focus()
      'disable other boxes until lot type has been selected
      Me.cboParameterSets.Enabled = True
      'Display message to user
      StatusLabel1.Text = "Select Parameter Set and Press ENTER"
      Call LogActivity(StatusLabel1.Text)
      StatusLabel2.Text = ""
      Application.DoEvents()


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call DisplayAnomaly()
      Application.Exit()
      Environment.Exit(0)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call DisplayAnomaly()
      Application.Exit()
      Environment.Exit(0)
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub


  'Public Sub UpdateText(ByVal txtBox As TextBox, ByVal text As String)
  '  txtBox.Text = text
  'End Sub

  'Public Sub UpdateLabel(ByVal label As Label, ByVal text As String)
  '  label.Text = text
  'End Sub

  'Public Sub UpdateTextBoxBackgroundColor(ByVal txtBox As TextBox, ByVal backgroundColor As Color)
  '  txtBox.BackColor = backgroundColor
  'End Sub




#Region "Menu Code =================================================="
  'Not used?
  Private Shared Sub AddMenuItem(ByVal destination As ContextMenu, ByVal caption As String, ByVal menuItemCaptions() As String, ByVal onItemClick As EventHandler, ByVal onPopup As EventHandler)
    Dim menuItems(menuItemCaptions.Length - 1) As MenuItem
    Dim i As Integer

    While i < menuItemCaptions.Length
      menuItems(i) = New MenuItem(menuItemCaptions(i), onItemClick)
      i = i + 1
    End While
    Dim menu As MenuItem = destination.MenuItems.Add(caption, menuItems)
    AddHandler menu.Popup, onPopup
  End Sub


#Region "Menu Function =================================================="
  'Private Sub mnuFunctionTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionTest.Click
  '  If gControlAndInterface.LinearPart = "True" Then
  '    Exit Sub
  '  End If
  '  mnuFunctionTest.Enabled = False
  '  'disable menus
  '  MenuStripMain.Enabled = False
  '  Me.Cursor = Cursors.WaitCursor
  '  If gblnParametersLoaded And gblnMetricsLoaded And gblnLotFileLoaded Then
  '    '\/TER6Gen, MotorLocationKnown is checked in the timer event, so don't need it here
  '    'If gblnMotorLocationKnown Then
  '    gblnLockPartAfterScan = False
  '    gblnTestInProgress = True
  '    gblnStartScan = True
  '    Me.tmr_Poll_PLC_IO.Interval = 10
  '    Me.tmr_Poll_PLC_IO.Start()
  '    'wait until test is done before allowing next test
  '    Do
  '      System.Windows.Forms.Application.DoEvents()
  '    Loop Until (gblnTestInProgress = False)
  '    'Else
  '    'MessageBox.Show("Motor Position is Unknown" & vbCrLf & "Motor will now Home" & vbCrLf & "REMOVE ANY PARTS FROM FIXTURE AND CLICK OK", "Test F1")
  '    ''start encoder task
  '    'Call modMain.StopEncoderSampleTask()
  '    ''Call modMain.ConfigureEncoderSampleTask(375)
  '    'If gControlAndInterface.LinearPart = "True" Then
  '    '  Call modMain.ConfigureLinearEncoderSampleTask(gMechanicalOperationAndLoad.HomeOffsetToDUTFace)
  '    'Else
  '    '  Call modMain.ConfigureEncoderSampleTask(375)
  '    'End If

  '    ''Home Motor
  '    'Call VIXControl.HomeMotor()

  '    'End If
  '    '/\
  '  Else
  '    MessageBox.Show("No Parameters or Lot Files have been Loaded" & vbCrLf & "Please Select a Parameter Set and Lot File", "Test F1")
  '  End If

  '  If UCase(gControlAndInterface.UsesAdditionalIO) = "TRUE" Then
  '    'Send Cylinder Home
  '    Call RetractCylinder()
  '  End If

  '  gblnStartScan = False
  '  Me.Cursor = Cursors.Arrow
  '  mnuFunctionTest.Enabled = True
  '  'enable menus
  '  MenuStripMain.Enabled = True
  'End Sub

  'Private Sub mnuFunctionTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionTest.Click

  'Public Sub mnuFunctionTest_Click()
  '  Try
  '    gControlFlags.StartScan = True
  '    gControlFlags.ManualScan = True
  '    gtmrOperatorTouchStartTimer.Enabled = False
  '    Call ExecuteTaskList(TaskListFilterEnum.Test)

  '    'If glstAnomaly.Count > 0 Then
  '    '  gControlFlags.TestError = True
  '    '  Call UpdateTestLotCounts()
  '    '  Call UpdateLotDataDisplay()
  '    '  Call UpdatePassFailStatus()
  '    '  Call LogAnomaly() 'Log anomaly also displays anomaly
  '    'End If

  '    If glstAnomaly.Count > 0 Or gControlFlags.AbortTest Then
  '      gControlFlags.TestError = True
  '      SetSupplyVoltage(0)
  '      Call UpdateTestLotCounts()
  '      Call UpdateLotDataDisplay()
  '      Call UpdatePassFailStatus()
  '      Call PrintRejectLabel()
  '    End If

  '    If glstAnomaly.Count > 0 Then
  '      LogAnomaly()
  '    End If

  '    gControlFlags.AbortTest = False
  '    gControlFlags.TestError = False
  '    gControlFlags.TestFailure = False
  '    gControlFlags.StartScan = False

  '    glstAnomaly.Clear()

  '    'Catch ex As TsopAnomalyException
  '    '  gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '    '  Call LogAnomaly() 'Log anomaly also displays anomaly
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '    Call LogAnomaly() 'Log anomaly also displays anomaly
  '  End Try
  'End Sub

  'Private Sub mnuFunctionProgram_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionProgram.Click
  Public Sub mnuFunctionProgram_Click()
    Try
      gControlFlags.StartScan = True
      gControlFlags.ManualScan = True
      Call ExecuteTaskList(TaskListFilterEnum.Program)

      If glstAnomaly.Count > 0 Then
        gControlFlags.ProgError = True
        Call UpdateProgLotCounts()
        'Call UpdateProgLotDataDisplay()
        Call UpdatePassFailStatus()
        Call LogAnomaly() 'Log anomaly also displays anomaly
      End If

      'Catch ex As TsopAnomalyException
      '  gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      '  Call LogAnomaly() 'Log anomaly also displays anomaly
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    End Try

  End Sub

  'Private Sub mnuFunctionProgramTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionProgramTest.Click
  Public Sub mnuFunctionProgramTest_Click()

    Try
      gControlFlags.StartScan = True
      gControlFlags.ManualScan = True
      Call ExecuteTaskList(TaskListFilterEnum.ProgramAndTest)

      If glstAnomaly.Count > 0 Then
        If gControlAndInterface.UsesProgramming And (gDatabase.SubProcessID = gSubProcesses(SubProcessEnum.Programming.ToString)) Then
          gControlFlags.ProgError = True
          Call UpdateProgLotCounts()
          'Call UpdateProgLotDataDisplay()
        Else
          gControlFlags.TestError = True
          Call UpdateTestLotCounts()
          Call UpdateLotDataDisplay()
        End If
        Call UpdatePassFailStatus()
        Call LogAnomaly() 'Log anomaly also displays anomaly
      End If

      'Catch ex As TsopAnomalyException
      '  gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      '  Call LogAnomaly() 'Log anomaly also displays anomaly
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    End Try

  End Sub

  'Private Sub mnuFunctionHomeMotor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles mnuFunctionHomeMotor.Click
  '  ' 3.1.0.2 TER
  '  mnuFunctionHomeMotor.Enabled = False
  '  'disable menus
  '  MenuStripMain.Enabled = False
  '  Me.Cursor = Cursors.WaitCursor

  '  '\/TER6Gen
  '  ''start encoder task
  '  'Call modMain.StopEncoderSampleTask()
  '  'Call modMain.ConfigureEncoderSampleTask(375)
  '  'MessageBox.Show("Motor Position is Unknown" & vbCrLf & "Motor will now Home" & vbCrLf & "REMOVE ANY PARTS FROM FIXTURE AND CLICK OK", "Key Up Event")
  '  ''Home motor
  '  'Call VIXControl.HomeMotor()
  '  '/\

  '  'MessageBox.Show("Motor Position is Unknown" & vbCrLf & "Motor will now Home" & vbCrLf & "REMOVE ANY PARTS FROM FIXTURE AND CLICK OK", "Key Up Event")
  '  'Call HomeRotaryServoSystem()
  '  'Call EE1070.SetupRotaryServoLoadPositionMove()
  '  'Call EE1070.RotaryServoMove()
  '  'Call UpdatePositionMonitor(gDaqEncoder_PositionMonitor.CurrentPosition + gMechanicalOperationAndLoad.HomeBlockOffset)

  '  'Home motor
  '  Call HomeMotor()

  '  mnuFunctionHomeMotor.Enabled = True
  '  'disable menus
  '  MenuStripMain.Enabled = True
  '  Me.Cursor = Cursors.Arrow
  'End Sub

  Public Sub mnuFunctionPrintParameterSet_Click()
    ' 3.1.0.2 TER
    Dim lResult As System.Windows.Forms.DialogResult
    Dim printDoc As New System.Drawing.Printing.PrintDocument
    Dim MyPrintDialog As New System.Windows.Forms.PrintDialog

    Try

      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      'PrintDialog1.Document = PrintTable
      MyPrintDialog.Document = printDoc
      With MyPrintDialog.Document
        .DefaultPageSettings.Margins.Left = 50 - .DefaultPageSettings.HardMarginX '25
        .DefaultPageSettings.Margins.Right = 50 + .DefaultPageSettings.HardMarginX '75
        .DefaultPageSettings.Margins.Top = 50 - .DefaultPageSettings.HardMarginY '25
        .DefaultPageSettings.Margins.Bottom = 50 + .DefaultPageSettings.HardMarginY '75
      End With

      gstrPrintTableKey = "ParameterSetListing"

      lResult = MyPrintDialog.ShowDialog()
      If lResult = Windows.Forms.DialogResult.OK Then
        gintParameterSetPrintRow = 0
        'PrintTable.Print()
        printDoc.Print()
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage
    End Try
  End Sub

  'Private Sub ParameterSetToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
  Public Sub mnuFunctionPrintPreviewParameterSet_Click()
    Dim printDoc As New System.Drawing.Printing.PrintDocument
    Dim MyPrintPreviewDialog As New System.Windows.Forms.PrintPreviewDialog
    Try

      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      MyPrintPreviewDialog.Document = printDoc

      gstrPrintTableKey = "ParameterSetListing"
      With MyPrintPreviewDialog.Document
        .DefaultPageSettings.Margins.Left = 50 - .DefaultPageSettings.HardMarginX '25
        .DefaultPageSettings.Margins.Right = 50 + .DefaultPageSettings.HardMarginX '75
        .DefaultPageSettings.Margins.Top = 50 - .DefaultPageSettings.HardMarginY '25
        .DefaultPageSettings.Margins.Bottom = 50 + .DefaultPageSettings.HardMarginY '75
      End With

      gintParameterSetPrintRow = 0
      MyPrintPreviewDialog.ShowDialog()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage
    End Try
  End Sub

  'Private Sub mnuFunctionPrintPreviewGraphs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintPreviewGraphs.Click
  Public Sub mnuFunctionPrintPreviewGraphs_Click()

    gintCurrentGraphPageNumber = 0
    Select Case gintGraphsPerPage
      Case 1
        PrintGraphs.DefaultPageSettings.Landscape = True
      Case 2
        PrintGraphs.DefaultPageSettings.Landscape = True
      Case 3
        PrintGraphs.DefaultPageSettings.Landscape = False
      Case Else
        PrintGraphs.DefaultPageSettings.Landscape = True
    End Select
    PrintPreviewDialog1.Document = PrintGraphs
    PrintPreviewDialog1.ShowDialog()
  End Sub

  'Private Sub mnuFunctionPrintPreviewTestResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintPreviewTestResults.Click
  Public Sub mnuFunctionPrintPreviewTestResults_Click()
    ' 3.1.0.1 TER
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintPreviewDialog1.Document = printDoc
      gstrPrintTableKey = "FTResults"

      gdgvTableToPrint = gDataGrids(gstrPrintTableKey)

      PrintPreviewDialog1.ShowDialog()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Private Sub mnuFunctionPrintPreviewTestStatistics_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintPreviewTestStatistics.Click
  Public Sub mnuFunctionPrintPreviewTestStatistics_Click()
    ' 3.1.0.1 TER
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintPreviewDialog1.Document = printDoc
      gstrPrintTableKey = "FTStats"

      gdgvTableToPrint = gDataGrids(gstrPrintTableKey)

      PrintPreviewDialog1.ShowDialog()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Private Sub mnuFunctionPrintPreviewProgResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintPreviewProgResults.Click
  Public Sub mnuFunctionPrintPreviewProgResults_Click()
    ' 3.1.0.1 TER
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintPreviewDialog1.Document = printDoc
      gstrPrintTableKey = "ProgResults"

      gdgvTableToPrint = gDataGrids(gstrPrintTableKey)

      PrintPreviewDialog1.ShowDialog()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Private Sub mnuFunctionPrintPreviewProgStatistics_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintPreviewProgStatistics.Click
  Public Sub mnuFunctionPrintPreviewProgStatistics_Click()
    ' 3.1.0.1 TER
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintPreviewDialog1.Document = printDoc
      gstrPrintTableKey = "ProgStats"

      gdgvTableToPrint = gDataGrids(gstrPrintTableKey)

      PrintPreviewDialog1.ShowDialog()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Private Sub mnuFunctionPrintTestResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles mnuFunctionPrintTestResults.Click
  'Public Sub mnuFunctionPrintTestResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles mnuFunctionPrintTestResults.Click
  Public Sub mnuFunctionPrintTestResults_Click()
    ' 3.1.0.1 TER
    Dim lResult As System.Windows.Forms.DialogResult
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintDialog1.Document = printDoc
      gstrPrintTableKey = "FTResults"

      If gDataGrids.ContainsKey(gstrPrintTableKey) Then
        gdgvTableToPrint = gDataGrids(gstrPrintTableKey)
        lResult = PrintDialog1.ShowDialog()
        If lResult = Windows.Forms.DialogResult.OK Then
          printDoc.Print()
        End If
      Else
        MessageBox.Show("You can only print Test Results if the data grid exists.", "Test Results Grid Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Error)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage
    End Try
  End Sub

  'Private Sub mnuFunctionPrintTestStatistics_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintTestStatistics.Click
  Public Sub mnuFunctionPrintTestStatistics_Click()
    ' 3.1.0.1 TER
    Dim lResult As System.Windows.Forms.DialogResult
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintDialog1.Document = printDoc
      gstrPrintTableKey = "FTStats"

      If gDataGrids.ContainsKey(gstrPrintTableKey) Then
        gdgvTableToPrint = gDataGrids(gstrPrintTableKey)
        lResult = PrintDialog1.ShowDialog()
        If lResult = Windows.Forms.DialogResult.OK Then
          printDoc.Print()
        End If
      Else
        MessageBox.Show("You can only print Test Stats if the data grid exists.", "Test Stats Grid Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Error)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Private Sub mnuFunctionPrintProgResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintProgResults.Click
  Public Sub mnuFunctionPrintProgResults_Click()
    ' 3.1.0.1 TER
    Dim lResult As System.Windows.Forms.DialogResult
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintDialog1.Document = printDoc
      gstrPrintTableKey = "ProgResults"

      If gDataGrids.ContainsKey(gstrPrintTableKey) Then
        gdgvTableToPrint = gDataGrids(gstrPrintTableKey)
        lResult = PrintDialog1.ShowDialog()
        If lResult = Windows.Forms.DialogResult.OK Then
          printDoc.Print()
        End If
      Else
        MessageBox.Show("You can only print Progrmming Results if the data grid exists.", "Programming Results Grid Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Error)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Private Sub mnuFunctionPrintProgStatistics_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintProgStatistics.Click
  Public Sub mnuFunctionPrintProgStatistics_Click()
    ' 3.1.0.1 TER
    Dim lResult As System.Windows.Forms.DialogResult
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintDialog1.Document = printDoc
      gstrPrintTableKey = "ProgStats"

      If gDataGrids.ContainsKey(gstrPrintTableKey) Then
        gdgvTableToPrint = gDataGrids(gstrPrintTableKey)
        lResult = PrintDialog1.ShowDialog()
        If lResult = Windows.Forms.DialogResult.OK Then
          printDoc.Print()
        End If
      Else
        MessageBox.Show("You can only print Progrmming Stats if the data grid exists.", "Programming Stats Grid Doesn't Exist", MessageBoxButtons.OK, MessageBoxIcon.Error)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Private Sub mnuFunctionPrintPreviousLot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintPreviousLot.Click
  Public Sub mnuFunctionPrintPreviousLot_Click()
    ' 3.1.0.1 TER
    frmOldLots.Show()
  End Sub

  Public Sub PrintPreviousLot() ' 3.1.0.1 TER
    Dim lResult As System.Windows.Forms.DialogResult
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintDialog1.Document = printDoc
      gstrPrintTableKey = "FTStats"

      lResult = PrintDialog1.ShowDialog()
      If lResult = Windows.Forms.DialogResult.OK Then
        printDoc.Print()
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  Public Sub PrintPreviewPreviousLot() ' 3.1.0.2 TER
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

      PrintPreviewDialog1.Document = printDoc
      gstrPrintTableKey = "FTStats"

      PrintPreviewDialog1.ShowDialog()
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    Finally
      RemoveHandler printDoc.PrintPage, AddressOf PrintDoc_PrintPage

    End Try
  End Sub

  'Public Sub PrintTable_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintTable.PrintPage
  '  ' 3.1.0.1 TER

  '  Try
  '    'If gstrPrintTableKey Like "ParameterSet*" Then
  '    '  If gstrPrintTableKey = "ParameterSetListing" Then
  '    '    Call PrintParameterSetMetrics(sender, e)
  '    '    If gintParameterSetPrintRow = 0 Then
  '    '      gstrPrintTableKey = "ParameterSet_Parameters"
  '    '    End If
  '    '    e.HasMorePages = True
  '    '  ElseIf gstrPrintTableKey = "ParameterSet_Parameters" Then
  '    '    Call PrintParameterSetNonMetrics(sender, e)
  '    '    If gintParameterSetPrintRow <> 0 Then
  '    '      e.HasMorePages = True
  '    '    End If
  '    '  End If
  '    'Else
  '    Call PrintPage(sender, e, gdgvTableToPrint)
  '    'End If
  '    If gAnomaly IsNot Nothing Then
  '      Throw New TsopAnomalyException
  '    End If

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '    Call LogAnomaly() 'Log anomaly also displays anomaly
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '    Call LogAnomaly() 'Log anomaly also displays anomaly
  '  End Try
  'End Sub

  Public Sub PrintDoc_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
    ' 3.1.0.1 TER

    Try
      If gstrPrintTableKey Like "ParameterSet*" Then
        If gstrPrintTableKey = "ParameterSetListing" Then
          Call PrintParameterSetMetrics(sender, e)
          If gintParameterSetPrintRow = 0 Then
            gstrPrintTableKey = "ParameterSet_Parameters"
          End If
          e.HasMorePages = True
        ElseIf gstrPrintTableKey = "ParameterSet_Parameters" Then
          Call PrintParameterSetNonMetrics(sender, e)
          If gintParameterSetPrintRow <> 0 Then
            e.HasMorePages = True
          Else
            gstrPrintTableKey = "ParameterSetListing"
          End If
        End If
      Else
        Call PrintPage(sender, e, gdgvTableToPrint)
      End If
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    End Try
  End Sub

  Public Sub PrintHeaderPageBuildDefinition() ' 3.1.0.1 TER
    Dim myPrintedPageCell As PrintedPageCell
    Dim printRow As DataRow
    Dim printRows() As DataRow
    Try

      gPrintedPageCells.Clear()

      printRows = GetReportSection("ReportHeader", gstrPrintTableKey) 'filters by section

      For Each printRow In printRows
        myPrintedPageCell = New PrintedPageCell

        If (printRow.Item("CellType") <> PrintedPageCellType.NewLine) Then
          myPrintedPageCell.CellType = printRow.Item("CellType")
          myPrintedPageCell.CellAdditionalHeight = CInt(printRow.Item("CellAdditionalHeight"))
          myPrintedPageCell.CellFractionalWidthNum = CInt(printRow.Item("CellFractionalWidthNum"))
          myPrintedPageCell.CellFractionalWidthDem = CInt(printRow.Item("CellFractionalWidthDem"))
          myPrintedPageCell.CellTextAlignment = DirectCast([Enum].Parse(GetType(System.Drawing.StringAlignment), printRow.Item("CellTextAlignment")), Integer)
          myPrintedPageCell.CellTextLineAlignment = DirectCast([Enum].Parse(GetType(System.Drawing.StringAlignment), printRow.Item("CellTextLineAlignment")), Integer)

          Dim cellText As String
          cellText = printRow.Item("CellText")
          If (printRow.Item("CellText") = "None") Then
            Select Case printRow.Item("CellName")
              Case "ReportInfo"
                myPrintedPageCell.CellText = Me.Text
              Case "DateInfo"
                If (gbPreviousLot) Then
                  myPrintedPageCell.CellText = gstrPreviousLotDateTime
                Else
                  myPrintedPageCell.CellText = DateTime.Now
                End If
              Case "ParameterSetInfo"
                myPrintedPageCell.CellText = "Parameter Set: " + Me.cboParameterSets.Text
              Case "LotNameInfo"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = gstrPreviousLotRun
                Else
                  myPrintedPageCell.CellText = "Lot File: " + Me.lblLotName.Text
                End If
              Case "SeriesInfo"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.Series) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.Series).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtSeries.Text
                End If
              Case "OperatorInfo"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.OperatorName) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.OperatorName).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtOperator.Text
                End If
              Case "Sample#Info"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.SampleNumber) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.SampleNumber).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtSampleNumber.Text
                End If
              Case "TestLogInfo"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.TestLogNumber) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.TestLogNumber).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtTestLogNumber.Text
                End If
              Case "ShiftInfo"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.Shift) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.Shift).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtShift.Text
                End If
              Case "TemperatureInfo"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.Temperature) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.Temperature).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtTemperature.Text
                End If
              Case "ExternalSerial#Info"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalSerialNumber) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.ExternalSerialNumber).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtExternalSerialNumber.Text
                End If
              Case "DateCodeInfo"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.DateCode) Then
                    myPrintedPageCell.CellText = gSetupInfoControls(SetupInfoItems.DateCode).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = Me.txtDateCode.Text
                End If
              Case "CommentField"
                If gControlFlags.UsesDynamicSetupInfo Then
                  If gSetupInfoControls.ContainsKey(SetupInfoItems.Comment) Then
                    myPrintedPageCell.CellText = "Comment:  " & gSetupInfoControls(SetupInfoItems.Comment).Text
                  End If
                Else
                  'myPrintedPageCell.CellText = "Comment:  " & txtComment.Text
                End If
            End Select
          Else
            myPrintedPageCell.CellText = printRow.Item("CellText")
          End If
        Else
          myPrintedPageCell.CellType = printRow.Item("CellType")
        End If
        gPrintedPageCells.Add(myPrintedPageCell)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    End Try
  End Sub

  Public Sub PrintFooterPageBuildDefinition() ' 3.1.0.1 TER
    Dim myPrintedPageCell As PrintedPageCell
    Dim printRow As DataRow
    Dim printRows() As DataRow
    Dim strCurrentYieldLabel As String
    Dim sngCurrentYield As Single
    Dim sngLotYield As Single
    Try

      strCurrentYieldLabel = CStr(gStationMonitorAndAlarm.CurrentTestTotalCount & " Part Yield")
      sngCurrentYield = Math.Round((gStationMonitorAndAlarm.CurrentTestPassCount / gStationMonitorAndAlarm.CurrentTestTotalCount), 4, MidpointRounding.AwayFromZero) * 100
      sngLotYield = Math.Round((gTestLot.CountUnitsPassed / gTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100

      gPrintedPageCells.Clear()

      printRows = GetReportSection("ReportFooter", gstrPrintTableKey) 'filters by section

      For Each printRow In printRows
        myPrintedPageCell = New PrintedPageCell

        If (printRow.Item("CellType") <> PrintedPageCellType.NewLine) Then
          myPrintedPageCell.CellType = printRow.Item("CellType")
          myPrintedPageCell.CellAdditionalHeight = CInt(printRow.Item("CellAdditionalHeight"))
          myPrintedPageCell.CellFractionalWidthNum = CInt(printRow.Item("CellFractionalWidthNum"))
          myPrintedPageCell.CellFractionalWidthDem = CInt(printRow.Item("CellFractionalWidthDem"))
          myPrintedPageCell.CellTextAlignment = DirectCast([Enum].Parse(GetType(System.Drawing.StringAlignment), printRow.Item("CellTextAlignment")), Integer)
          myPrintedPageCell.CellTextLineAlignment = DirectCast([Enum].Parse(GetType(System.Drawing.StringAlignment), printRow.Item("CellTextLineAlignment")), Integer)

          Dim cellText As String
          cellText = printRow.Item("CellText")
          If (printRow.Item("CellText") = "None") Then
            Select Case printRow.Item("CellName")
              Case "TestCurrentYieldHeader"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = "Current Part Yield"
                Else
                  myPrintedPageCell.CellText = strCurrentYieldLabel
                End If
              Case "TestTotalUnitsInfo"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = gPreviousTestLot.CountTotalUnits
                Else
                  myPrintedPageCell.CellText = gTestLot.CountTotalUnits
                End If
              Case "TestUnitsPassedInfo"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = gPreviousTestLot.CountUnitsPassed
                Else
                  myPrintedPageCell.CellText = gTestLot.CountUnitsPassed
                End If
              Case "TestUnitsRejectedInfo"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = gPreviousTestLot.CountUnitsRejected
                Else
                  myPrintedPageCell.CellText = gTestLot.CountUnitsRejected
                End If
              Case "TestErrorsInfo"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = gPreviousTestLot.CountErrors
                Else
                  myPrintedPageCell.CellText = gTestLot.CountErrors
                End If
              Case "TestCurrentYieldInfo"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = "n/a"
                Else
                  myPrintedPageCell.CellText = sngCurrentYield
                End If
              Case "TestLotYieldInfo"
                If gbPreviousLot Then
                  myPrintedPageCell.CellText = CStr(Math.Round((gPreviousTestLot.CountUnitsPassed / gPreviousTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100)
                Else
                  myPrintedPageCell.CellText = sngLotYield
                End If
            End Select
          Else
            myPrintedPageCell.CellText = printRow.Item("CellText")
          End If
        Else
          myPrintedPageCell.CellType = printRow.Item("CellType")
        End If
        gPrintedPageCells.Add(myPrintedPageCell)
      Next
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly() 'Log anomaly also displays anomaly
    End Try
  End Sub

  Public Sub PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs _
                        , ByVal grid As DataGridView)
    ' 3.1.0.1 TER
    Dim Cell As System.Windows.Forms.DataGridViewCell
    Dim column As System.Windows.Forms.DataGridViewColumn
    Dim myFont As Font
    Dim myHeaderFont As Font
    Dim i As Integer = 0
    'Dim CharHeight As Integer
    Dim CellImage As System.Drawing.Rectangle
    Dim CellXOffset As Integer
    Dim CellYOffset As Integer
    Dim PreviousColumnWidth As Integer = 0
    Dim ColumnHeaderHeight As Integer
    'Dim lstrHeader As String
    Dim TextFormat As New System.Drawing.StringFormat
    Dim LineHeight As Integer = 0
    Dim myPrintedPageCell As New PrintedPageCell
    Dim myPrintedPageCells As New List(Of PrintedPageCell)
    Dim intPreviousCellHeight As Integer
    Dim multiplier As Double

    Try

      myFont = New Font("Arial", 10, FontStyle.Regular, GraphicsUnit.Pixel)
      myHeaderFont = New Font("Arial", 12, FontStyle.Regular, GraphicsUnit.Pixel)

      PreviousColumnWidth = 0
      TextFormat.Alignment = StringAlignment.Center
      TextFormat.LineAlignment = StringAlignment.Center

      Call PrintHeaderPageBuildDefinition()

      CellXOffset = 0
      CellYOffset = 0
      '==================== Create Report Header ====================
      For Each myPrintedPageCell In gPrintedPageCells
        If myPrintedPageCell.CellType = PrintedPageCellType.NewLine Then
          CellXOffset = 0
          LineHeight = LineHeight + intPreviousCellHeight

          CellYOffset = LineHeight
        Else
          'create new image for cell
          CellImage = New System.Drawing.Rectangle
          'set cell height to font height
          CellImage.Height = myHeaderFont.Height + myPrintedPageCell.CellAdditionalHeight
          'convert column width to percent of page margin
          multiplier = Math.Round(myPrintedPageCell.CellFractionalWidthNum / myPrintedPageCell.CellFractionalWidthDem, 3)
          CellImage.Width = e.MarginBounds.Width * multiplier
          CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)
          TextFormat.Alignment = myPrintedPageCell.CellTextAlignment
          TextFormat.LineAlignment = myPrintedPageCell.CellTextLineAlignment

          'draw cell
          e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
          'draw text
          e.Graphics.DrawString(myPrintedPageCell.CellText, New Font(myHeaderFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

          CellXOffset = CellXOffset + CellImage.Width
          intPreviousCellHeight = CellImage.Height
        End If
      Next

      '==================== Create Header for table ====================
      CellXOffset = 0
      CellYOffset = LineHeight
      PreviousColumnWidth = 0
      'For Each column In gDataGrids(gstrPrintTableKey).Columns
      For Each column In grid.Columns
        TextFormat = StringFormat.GenericDefault
        TextFormat.Alignment = StringAlignment.Near
        'create new image for cell
        CellImage = New System.Drawing.Rectangle
        'set cell height to font height
        ColumnHeaderHeight = myFont.Height + 10
        CellImage.Height = ColumnHeaderHeight
        'convert column width to percent of page margin
        CellImage.Width = (column.Width / Me.tabResults.Width) * e.MarginBounds.Width
        'set offset
        CellXOffset = PreviousColumnWidth
        CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)
        'draw cell
        e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
        'draw text
        e.Graphics.DrawString(column.HeaderText, New Font(myFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)
        PreviousColumnWidth = CellImage.Width + PreviousColumnWidth
      Next

      LineHeight = LineHeight + ColumnHeaderHeight
      CellXOffset = 0
      CellYOffset = 0
      '==================== Create Table ====================
      'for each column
      'For Each column In gDataGrids(gstrPrintTableKey).Columns
      'for each row
      For i = 0 To grid.Rows.Count - 1
        PreviousColumnWidth = 0
        For Each column In grid.Columns
          TextFormat = StringFormat.GenericDefault
          TextFormat.Alignment = StringAlignment.Near
          'create new image for cell
          CellImage = New System.Drawing.Rectangle
          'get cell data
          Cell = grid(column.Index, i)
          'get height of characters
          'CharHeight = myFont.Height * i
          'set cell height to font height
          CellImage.Height = myFont.Height
          'convert column width to percent of page margin
          CellImage.Width = (column.Width / Me.tabResults.Width) * e.MarginBounds.Width
          'set cell offset based on previous column and row
          CellXOffset = PreviousColumnWidth
          CellYOffset = LineHeight
          CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)
          'draw cell
          e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
          'draw text
          e.Graphics.DrawString(Cell.Value, New Font(myFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)
          PreviousColumnWidth = CellImage.Width + PreviousColumnWidth
        Next
        LineHeight = LineHeight + myFont.Height
      Next

      LineHeight = LineHeight + myFont.Height + 10

      '==================== Lot Stats Footer ====================
      If (gstrPrintTableKey = "FTStats") Then
        Call PrintFooterPageBuildDefinition()

        CellXOffset = 0
        CellYOffset = LineHeight
        For Each myPrintedPageCell In gPrintedPageCells
          If myPrintedPageCell.CellType = PrintedPageCellType.NewLine Then
            CellXOffset = 0
            LineHeight = LineHeight + intPreviousCellHeight

            CellYOffset = LineHeight
          Else
            'create new image for cell
            CellImage = New System.Drawing.Rectangle
            'set cell height to font height
            CellImage.Height = myFont.Height + myPrintedPageCell.CellAdditionalHeight
            'convert column width to percent of page margin
            multiplier = Math.Round(myPrintedPageCell.CellFractionalWidthNum / myPrintedPageCell.CellFractionalWidthDem, 3)
            CellImage.Width = e.MarginBounds.Width * multiplier
            CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)
            TextFormat.Alignment = myPrintedPageCell.CellTextAlignment
            TextFormat.LineAlignment = myPrintedPageCell.CellTextLineAlignment

            'draw cell
            e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
            'draw text
            e.Graphics.DrawString(myPrintedPageCell.CellText, New Font(myFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

            CellXOffset = CellXOffset + CellImage.Width
            intPreviousCellHeight = CellImage.Height
          End If
        Next
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Private Sub mnuFunctionPrintGraphs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionPrintGraphs.Click
  Public Sub mnuFunctionPrintGraphs_Click()
    Dim lResult As System.Windows.Forms.DialogResult
    gintCurrentGraphPageNumber = 0
    Select Case gintGraphsPerPage
      Case 1
        PrintGraphs.DefaultPageSettings.Landscape = True
        PrintDialog1.PrinterSettings.DefaultPageSettings.Landscape = True
      Case 2
        PrintGraphs.DefaultPageSettings.Landscape = True
        PrintDialog1.PrinterSettings.DefaultPageSettings.Landscape = True
      Case 3
        PrintGraphs.DefaultPageSettings.Landscape = False
        PrintDialog1.PrinterSettings.DefaultPageSettings.Landscape = False
      Case Else
        PrintGraphs.DefaultPageSettings.Landscape = True
        PrintDialog1.PrinterSettings.DefaultPageSettings.Landscape = True
    End Select
    PrintDialog1.Document = PrintGraphs
    lResult = PrintDialog1.ShowDialog()
    If lResult = Windows.Forms.DialogResult.OK Then
      PrintGraphs.Print()
    End If
  End Sub

  Public Sub PrintParameterSetMetrics(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
    ' 3.1.0.1 TER
    'Dim Cell As System.Windows.Forms.DataGridViewCell
    'Dim column As System.Windows.Forms.DataGridViewColumn
    Dim myFont As Font
    Dim myHeaderFont As Font
    Dim i As Integer = 0
    'Dim CharHeight As Integer
    Dim CellImage As System.Drawing.Rectangle
    Dim CellXOffset As Integer
    Dim CellYOffset As Integer
    Dim PreviousColumnWidth As Integer = 0
    Dim ColumnHeaderHeight As Integer
    'Dim lstrHeader As String
    Dim TextFormat As New System.Drawing.StringFormat
    Dim LineHeight As Integer = 0
    Dim myPrintedPageCell As New PrintedPageCell
    Dim myPrintedPageCells As New List(Of PrintedPageCell)
    Dim intPreviousCellHeight As Integer
    Dim multiplier As Double

    'Dim grid As DataGridView

    '==================================================
    'New variables 
    '==================================================
    Dim drParams As DataRow
    Dim dcParams As DataColumn
    Dim strCellText As String
    Dim sngCellWidths As Single()
    Dim intLoop As Integer
    Dim intParamRow As Integer
    Dim intStartRow As Integer
    Try

      myFont = New Font("Arial", 10, FontStyle.Regular, GraphicsUnit.Pixel)
      myHeaderFont = New Font("Arial", 12, FontStyle.Regular, GraphicsUnit.Pixel)
      PreviousColumnWidth = 0

      '==================================================
      'Report Header, Line 1
      '==================================================
      'create new image for cell
      CellImage = New System.Drawing.Rectangle

      'set cell height to font height
      CellImage.Height = myHeaderFont.Height + 20

      'convert column width to percent of page margin
      multiplier = Math.Round(1 / 1, 3)
      CellImage.Width = e.MarginBounds.Width * multiplier
      CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

      'draw cell
      e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
      'draw text
      TextFormat.Alignment = StringAlignment.Center
      TextFormat.LineAlignment = StringAlignment.Center
      strCellText = Me.Text
      e.Graphics.DrawString(strCellText, New Font(myHeaderFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

      'Prepare for next line
      intPreviousCellHeight = CellImage.Height
      CellXOffset = 0
      LineHeight = LineHeight + intPreviousCellHeight
      CellYOffset = LineHeight

      '==================================================
      'Report Header, Line 2
      '==================================================
      'create new image for cell
      CellImage = New System.Drawing.Rectangle

      'set cell height to font height
      CellImage.Height = myHeaderFont.Height

      'convert column width to percent of page margin
      multiplier = Math.Round(1 / 1, 3)
      CellImage.Width = e.MarginBounds.Width * multiplier
      CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

      'draw cell
      e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
      'draw text
      TextFormat.Alignment = StringAlignment.Center
      TextFormat.LineAlignment = StringAlignment.Center
      strCellText = "Parameter Set Listing: " + Me.cboParameterSets.Text
      e.Graphics.DrawString(strCellText, New Font(myHeaderFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

      'Prepare for next line
      intPreviousCellHeight = CellImage.Height
      CellXOffset = 0
      LineHeight = LineHeight + intPreviousCellHeight
      CellYOffset = LineHeight

      '==================================================
      'Metrics Section Header
      '==================================================
      'create new image for cell
      CellImage = New System.Drawing.Rectangle

      'set cell height to font height
      CellImage.Height = myHeaderFont.Height

      'convert column width to percent of page margin
      multiplier = Math.Round(1 / 1, 3)
      CellImage.Width = e.MarginBounds.Width * multiplier
      CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

      'draw cell
      e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
      'draw text
      TextFormat.Alignment = StringAlignment.Near
      TextFormat.LineAlignment = StringAlignment.Near
      strCellText = "Metric Values"
      e.Graphics.DrawString(strCellText, New Font(myHeaderFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

      'Prepare for next line
      intPreviousCellHeight = CellImage.Height
      CellXOffset = 0
      LineHeight = LineHeight + intPreviousCellHeight

      '==================================================
      'Metrics Table Column Headers
      '==================================================
      'Set cell widths as percentage of total width
      sngCellWidths = New Single() {24, 8.9, 6.3, 7.5, 7.5, 7.1, 7.0, 5.6, 8.5, 8.5, 9}
      CellXOffset = 0
      CellYOffset = LineHeight
      PreviousColumnWidth = 0
      For intLoop = 0 To gdtMetricReportTable.Columns.Count - 1
        'For Each dcParams In gdtMetricReportTable.Columns
        dcParams = gdtMetricReportTable.Columns(intLoop)
        TextFormat = StringFormat.GenericDefault
        TextFormat.Alignment = StringAlignment.Near
        'create new image for cell
        CellImage = New System.Drawing.Rectangle
        'set cell height to font height
        ColumnHeaderHeight = myFont.Height + 10
        CellImage.Height = ColumnHeaderHeight
        'convert column width to percent of page margin
        'CellImage.Width = (column.Width / Me.tabResults.Width) * e.MarginBounds.Width
        multiplier = Math.Round(sngCellWidths(intLoop) / 100, 3)
        CellImage.Width = e.MarginBounds.Width * multiplier
        'set offset
        CellXOffset = PreviousColumnWidth
        CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

        'draw cell
        e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
        'draw text
        e.Graphics.DrawString(dcParams.ColumnName, New Font(myFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

        PreviousColumnWidth = CellImage.Width + PreviousColumnWidth
      Next

      LineHeight = LineHeight + ColumnHeaderHeight
      CellXOffset = 0

      '==================================================
      'Metrics Table 
      '==================================================
      CellXOffset = 0
      CellYOffset = LineHeight
      PreviousColumnWidth = 0
      intStartRow = gintParameterSetPrintRow
      'For Each drParams In gdtMetricReportTable.Rows
      For intParamRow = intStartRow To gdtMetricReportTable.Rows.Count - 1
        drParams = gdtMetricReportTable.Rows(intParamRow)
        PreviousColumnWidth = 0
        For intLoop = 0 To gdtMetricReportTable.Columns.Count - 1
          'dcParams = gdtMetricReportTable.Columns(intLoop)
          TextFormat = StringFormat.GenericDefault
          TextFormat.Alignment = StringAlignment.Near
          'create new image for cell
          CellImage = New System.Drawing.Rectangle
          'set cell height to font height
          'ColumnHeaderHeight = myFont.Height + 10
          'CellImage.Height = ColumnHeaderHeight
          CellImage.Height = myFont.Height
          'convert column width to percent of page margin
          'CellImage.Width = (column.Width / Me.tabResults.Width) * e.MarginBounds.Width
          multiplier = Math.Round(sngCellWidths(intLoop) / 100, 3)
          CellImage.Width = e.MarginBounds.Width * multiplier
          'set offset
          CellXOffset = PreviousColumnWidth
          CellYOffset = LineHeight
          CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

          'draw cell
          e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
          'draw text
          If drParams.Item(intLoop) Is DBNull.Value Then
            strCellText = ""
          Else
            strCellText = drParams.Item(intLoop)
          End If
          e.Graphics.DrawString(strCellText, New Font(myFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

          PreviousColumnWidth = CellImage.Width + PreviousColumnWidth
        Next

        LineHeight = LineHeight + myFont.Height
        CellXOffset = 0

        If LineHeight >= e.MarginBounds.Height Then
          gintParameterSetPrintRow = intParamRow + 1
          Exit For
        Else
          gintParameterSetPrintRow = 0
        End If
      Next

      'LineHeight = LineHeight + myFont.Height + 10
      CellYOffset = LineHeight

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub PrintParameterSetNonMetrics(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
    ' 3.1.0.1 TER
    'Dim Cell As System.Windows.Forms.DataGridViewCell
    'Dim column As System.Windows.Forms.DataGridViewColumn
    Dim myFont As Font
    Dim myHeaderFont As Font
    Dim i As Integer = 0
    'Dim CharHeight As Integer
    Dim CellImage As System.Drawing.Rectangle
    Dim CellXOffset As Integer
    Dim CellYOffset As Integer
    Dim PreviousColumnWidth As Integer = 0
    Dim ColumnHeaderHeight As Integer
    'Dim lstrHeader As String
    Dim TextFormat As New System.Drawing.StringFormat
    Dim LineHeight As Integer = 0
    Dim myPrintedPageCell As New PrintedPageCell
    Dim myPrintedPageCells As New List(Of PrintedPageCell)
    Dim intPreviousCellHeight As Integer
    Dim multiplier As Double

    'Dim grid As DataGridView

    '==================================================
    'New variables 
    '==================================================
    Dim drParams As DataRow
    Dim dcParams As DataColumn
    Dim strCellText As String
    Dim sngCellWidths As Single()
    Dim intLoop As Integer
    Dim intParamRow As Integer
    Dim intStartRow As Integer
    Try

      myFont = New Font("Arial", 10, FontStyle.Regular, GraphicsUnit.Pixel)
      myHeaderFont = New Font("Arial", 12, FontStyle.Regular, GraphicsUnit.Pixel)
      PreviousColumnWidth = 0

      '==================================================
      'Report Header, Line 1
      '==================================================
      'create new image for cell
      CellImage = New System.Drawing.Rectangle

      'set cell height to font height
      CellImage.Height = myHeaderFont.Height + 20

      'convert column width to percent of page margin
      multiplier = Math.Round(1 / 1, 3)
      CellImage.Width = e.MarginBounds.Width * multiplier
      CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

      'draw cell
      e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
      'draw text
      TextFormat.Alignment = StringAlignment.Center
      TextFormat.LineAlignment = StringAlignment.Center
      strCellText = Me.Text
      e.Graphics.DrawString(strCellText, New Font(myHeaderFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

      'Prepare for next line
      intPreviousCellHeight = CellImage.Height
      CellXOffset = 0
      LineHeight = LineHeight + intPreviousCellHeight
      CellYOffset = LineHeight

      '==================================================
      'Report Header, Line 2
      '==================================================
      'create new image for cell
      CellImage = New System.Drawing.Rectangle

      'set cell height to font height
      CellImage.Height = myHeaderFont.Height

      'convert column width to percent of page margin
      multiplier = Math.Round(1 / 1, 3)
      CellImage.Width = e.MarginBounds.Width * multiplier
      CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

      'draw cell
      e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
      'draw text
      TextFormat.Alignment = StringAlignment.Center
      TextFormat.LineAlignment = StringAlignment.Center
      strCellText = "Parameter Set Listing: " + Me.cboParameterSets.Text
      e.Graphics.DrawString(strCellText, New Font(myHeaderFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

      'Prepare for next line
      intPreviousCellHeight = CellImage.Height
      CellXOffset = 0
      LineHeight = LineHeight + intPreviousCellHeight
      CellYOffset = LineHeight

      '==================================================
      'Non-Metrics Section Header
      '==================================================
      'create new image for cell
      CellImage = New System.Drawing.Rectangle

      'set cell height to font height
      CellImage.Height = myHeaderFont.Height

      'convert column width to percent of page margin
      multiplier = Math.Round(1 / 1, 3)
      CellImage.Width = e.MarginBounds.Width * multiplier
      CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

      'draw cell
      e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
      'draw text
      TextFormat.Alignment = StringAlignment.Near
      TextFormat.LineAlignment = StringAlignment.Near
      strCellText = "Non-Metric Values"
      e.Graphics.DrawString(strCellText, New Font(myHeaderFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

      'Prepare for next line
      intPreviousCellHeight = CellImage.Height
      LineHeight = LineHeight + intPreviousCellHeight

      '==================================================
      'Non-Metric Table Column Headers
      '==================================================
      'Set cell widths as percentage of total width
      sngCellWidths = New Single() {25.0, 22.5, 14.4, 38.0}
      CellXOffset = 0
      CellYOffset = LineHeight
      PreviousColumnWidth = 0
      For intLoop = 0 To gdtParameterReportTable.Columns.Count - 1
        'For Each dcParams In gdtParameterReportTable.Columns
        dcParams = gdtParameterReportTable.Columns(intLoop)
        TextFormat = StringFormat.GenericDefault
        TextFormat.Alignment = StringAlignment.Near
        'create new image for cell
        CellImage = New System.Drawing.Rectangle
        'set cell height to font height
        ColumnHeaderHeight = myFont.Height + 10
        CellImage.Height = ColumnHeaderHeight
        'convert column width to percent of page margin
        'CellImage.Width = (column.Width / Me.tabResults.Width) * e.MarginBounds.Width
        multiplier = Math.Round(sngCellWidths(intLoop) / 100, 3)
        CellImage.Width = e.MarginBounds.Width * multiplier
        'set offset
        CellXOffset = PreviousColumnWidth
        CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

        'draw cell
        e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
        'draw text
        e.Graphics.DrawString(dcParams.ColumnName, New Font(myFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

        PreviousColumnWidth = CellImage.Width + PreviousColumnWidth
      Next

      LineHeight = LineHeight + ColumnHeaderHeight

      '==================================================
      'Non-Metrics Table 
      '==================================================
      CellXOffset = 0
      CellYOffset = LineHeight
      PreviousColumnWidth = 0
      'For Each drParams In gdtParameterReportTable.Rows
      intStartRow = gintParameterSetPrintRow
      For intParamRow = intStartRow To gdtParameterReportTable.Rows.Count - 1
        drParams = gdtParameterReportTable.Rows(intParamRow)
        PreviousColumnWidth = 0
        For intLoop = 0 To gdtParameterReportTable.Columns.Count - 1
          'dcParams = gdtParameterReportTable.Columns(intLoop)
          TextFormat = StringFormat.GenericDefault
          TextFormat.Alignment = StringAlignment.Near
          'create new image for cell
          CellImage = New System.Drawing.Rectangle
          'set cell height to font height
          'ColumnHeaderHeight = myFont.Height + 10
          'CellImage.Height = ColumnHeaderHeight
          CellImage.Height = myFont.Height
          'convert column width to percent of page margin
          'CellImage.Width = (column.Width / Me.tabResults.Width) * e.MarginBounds.Width
          multiplier = Math.Round(sngCellWidths(intLoop) / 100, 3)
          CellImage.Width = e.MarginBounds.Width * multiplier
          'set offset
          CellXOffset = PreviousColumnWidth
          CellYOffset = LineHeight
          CellImage.Offset(e.MarginBounds.X + CellXOffset, e.MarginBounds.Y + CellYOffset)

          'draw cell
          e.Graphics.DrawRectangle(Pens.DarkGray, CellImage)
          'draw text
          If drParams.Item(intLoop) Is DBNull.Value Then
            strCellText = ""
          Else
            strCellText = drParams.Item(intLoop)
          End If
          e.Graphics.DrawString(strCellText, New Font(myFont, FontStyle.Regular), Brushes.Black, CellImage, TextFormat)

          PreviousColumnWidth = CellImage.Width + PreviousColumnWidth
        Next

        LineHeight = LineHeight + myFont.Height
        CellXOffset = 0
        If LineHeight >= e.MarginBounds.Height Then
          gintParameterSetPrintRow = intParamRow + 1
          Exit For
        Else
          gintParameterSetPrintRow = 0
        End If

      Next

      LineHeight = LineHeight + myFont.Height + 10

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub mnuFunctionExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFunctionExit.Click
    Me.Close()
  End Sub

#End Region

#Region "Menu Tool =================================================="

  Private Sub mnuToolMelexis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Try
      Dim result As DialogResult
      result = frmPasswordDialog.ShowDialog()
      If result <> Windows.Forms.DialogResult.OK Then
        MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
        Exit Sub
      End If
      ' frmMLX90324.ShowDialog()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
    End Try
  End Sub


  'Private Sub mnuToolReadMelexisSerialNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolReadMelexisSerialNumber.Click
  'Public Sub mnuToolReadMelexisSerialNumber_Click()
  '  Try
  '    frmMLXRead.ShowDialog()

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  Finally
  '  End Try
  'End Sub


  'Private Sub mnuToolMonitorDAQ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolMonitorDAQ.Click


  'Private Sub mnuToolOPC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolOPC.Click
  Public Sub mnuToolOPC_Click()
    Try
      Dim result As DialogResult
      result = frmPasswordDialog.ShowDialog()
      If result <> Windows.Forms.DialogResult.OK Then
        MessageBox.Show("Password was not entered correctly" & vbCrLf & "Please Try Again", "Password ERROR")
        Exit Sub
      End If

      'frmOPCTest.Show()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
    End Try
  End Sub
#End Region

#Region "Menu Lot =================================================="
  Private Sub InitializeLotTypeMenu()       '1.0.0.2 DWD New
    Dim bytIndex As Byte
    Dim LotTypesTable As DataTable
    Try
      LotTypesTable = gDatabase.GetEnabledLotTypesDataTable
      mnuLotTypesCombo.ComboBox.BindingContext = Me.BindingContext
      mnuLotTypesCombo.ComboBox.DataSource = LotTypesTable
      mnuLotTypesCombo.ComboBox.ValueMember = LotTypesTable.Columns("LotTypeID").ToString
      mnuLotTypesCombo.ComboBox.DisplayMember = LotTypesTable.Columns("LotTypeName").ToString
      bytIndex = mnuLotTypesCombo.ComboBox.FindString(gstrDefaultLotType)
      mnuLotTypesCombo.ComboBox.SelectedIndex = bytIndex

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub LotTypesComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      If mnuLotTypesCombo.ComboBox.ValueMember <> "" Then
        gDatabase.LotTypeID = mnuLotTypesCombo.ComboBox.SelectedValue
        lblLotType.Text = mnuLotTypesCombo.ComboBox.Text.ToUpper
        If gDatabase.LotID <> Guid.Empty Then
          gstrLotName = BuildLotName()
          Call GetLotNames(gstrLotName, False)
          Call ConfigureLotNamesMenu()
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub LotName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim strLotName As String
    Dim strLotRun As String
    Dim MenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim MyTabPage As New TabPage
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Try
      MenuItem = sender
      strLotName = MenuItem.Text.Split(".")(0)
      strLotRun = MenuItem.Text.Split(".")(1)

      strFilterExpression = "LotRun = '" & strLotRun & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtLotNames.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      gDatabase.LotID = rowsFound(0).Item("LotID")
      lblLotName.Text = MenuItem.Text
      gstrLotName = strLotName
      gintLotRun = strLotRun
      Call UpdateDisplayForCurrentLot()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub mnuIncrementLotRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuIncrementLotRun.Click
    Try
      If gDatabase.LotID <> Guid.Empty Then
        gstrLotName = BuildLotName()
        Call GetLotNames(gstrLotName, True)
        Call ConfigureLotNamesMenu()
        Call UpdateDisplayForCurrentLot()
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub SetupLotTypeMenuItems()
    'Add Lot Types Menu Combo
    'mnuLotType.DropDownItems.Add(TsopMenuComboLotTypes)
    mnuLotType.DropDownItems.Add(mnuLotTypesCombo)
    'Create Lot Type Selection Handler
    AddHandler mnuLotTypesCombo.SelectedIndexChanged, AddressOf LotTypesComboBox_SelectedIndexChanged
    'Populate Lot Type Menu items
    Call InitializeLotTypeMenu()

  End Sub

#End Region

#Region "Menu Options =================================================="
  Private Sub mnuOptionsProductionSetDefault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Try
      gblnProductionSet = mnuOptionsProductionSetDefault.Checked
      Call LoadParameterSets()
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  'Private Sub mnuOptionsGraphsPerPage_DropDownItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles mnuOptionsGraphsPerPage.DropDownItemClicked
  '  gintGraphsPerPage = DirectCast(e.ClickedItem, System.Windows.Forms.ToolStripItem).Text
  '  For Each item As ToolStripMenuItem In mnuOptionsGraphsPerPage.DropDownItems
  '    item.CheckState = CheckState.Unchecked
  '  Next
  'End Sub

  'Private Sub mnuOptionsGraphsPerPage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOptionsGraphsPerPage.Click
  'Top Menu Responds only to drop down items
  'End Sub

  'Private Sub mnuOptionsDisplayGraphs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOptionsDisplayGraphs.Click
  '  'Drop down items built in Sub InitializeGraphs
  'End Sub

  Private Sub mnuOptionsOnPLCStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOptionsOnPLCStart.Click
    'Top Menu only
  End Sub

  Private Sub mnuOptionsOnPlcStartProgram_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles mnuOptionsOnPlcStartProgram.Click
    'If Me.mnuOptionsOnPlcStartProgram.Checked = True Then
    '  'If gblnProductionMode Then
    '  If gControlFlags.ProductionMode = True Then
    '    'Password required to uncheck
    '    Dim result As DialogResult
    '    result = frmPasswordDialog.ShowDialog()
    '    If result <> Windows.Forms.DialogResult.OK Then
    '      MessageBox.Show("Password was not entered correctly" & vbCrLf & "'Program' still checked.", "Password ERROR")
    '      Exit Sub
    '    End If
    '  End If
    '  Me.mnuOptionsOnPlcStartProgram.Checked = False
    '  gControlFlags.OnPlcStartProgram = False
    'Else
    '  Me.mnuOptionsOnPlcStartProgram.Checked = True
    '  gControlFlags.OnPlcStartProgram = True
    'End If
  End Sub

  Private Sub mnuOptionsOnPlcStartTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles mnuOptionsOnPlcStartTest.Click
    'If Me.mnuOptionsOnPlcStartTest.Checked = True Then
    '  'If gblnProductionMode Then
    '  If gControlFlags.ProductionMode = True Then
    '    'Password required to uncheck
    '    Dim result As DialogResult
    '    result = frmPasswordDialog.ShowDialog()
    '    If result <> Windows.Forms.DialogResult.OK Then
    '      MessageBox.Show("Password was not entered correctly" & vbCrLf & "'Test' still checked.", "Password ERROR")
    '      Exit Sub
    '    End If
    '  End If
    '  Me.mnuOptionsOnPlcStartTest.Checked = False
    '  gControlFlags.OnPlcStartTest = False
    'Else
    '  Me.mnuOptionsOnPlcStartTest.Checked = True
    '  gControlFlags.OnPlcStartTest = True
    'End If
  End Sub


  Public Sub mnuOptionDefaultTabPageSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim MenuItemClicked As System.Windows.Forms.ToolStripMenuItem
    Dim MenuItemLoop As System.Windows.Forms.ToolStripMenuItem
    Dim MyTabPage As New TabPage

    Try
      MenuItemClicked = sender

      For Each MenuItemLoop In mnuOptionsDefaultTabPage.DropDownItems
        MenuItemLoop.Checked = False
      Next

      MenuItemClicked.Checked = True

      gstrDefaultTabPage = MenuItemClicked.Text

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub MenuOptionsCyclePartClick(ByVal menuItem As System.Windows.Forms.ToolStripMenuItem)
    Try
      Dim lstrNumCycles As String

      Me.Cursor = Cursors.WaitCursor

      gControlFlags.CyclePartEnabled = menuItem.Checked

      If gControlFlags.CyclePartEnabled Then
        lstrNumCycles = InputBox("Enter number of cycles:", "Cycle Part", CStr(gintNumCycles))
        If Not IsNumeric(lstrNumCycles) Then
          MsgBox("Invalid charcter!", vbOKOnly, "Incorrect Number of Cycles")
          Exit Sub
        End If
        gintNumCycles = CInt(lstrNumCycles)
        If (gintNumCycles < 2) Or (gintNumCycles > 20) Then
          MsgBox("Number of cycles must be 2 - 20!", vbOKOnly, "Incorrect Number of Cycles")
          gintNumCycles = 5
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  Public Sub MenuOptionsLaserTestClick(ByVal menuItem As System.Windows.Forms.ToolStripMenuItem)
    Try
      If gControlFlags.LaserTestEnabled = False Then
        'If gblnProductionMode Then
        If gControlFlags.ProductionMode = True Then
          'Password required to check
          Dim result As DialogResult
          result = frmPasswordDialog.ShowDialog()
          If result <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("Password was not entered correctly" & vbCrLf & "Laser Test not Enabled.", "Password ERROR")
            menuItem.Checked = False
            Exit Sub
          End If
        End If
        gControlFlags.LaserTestEnabled = True
        menuItem.Checked = True
      Else
        gControlFlags.LaserTestEnabled = False
        menuItem.Checked = False
      End If
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  Public Sub MenuOptionsDeviceMarkingEnableClick(ByVal menuItem As System.Windows.Forms.ToolStripMenuItem)
    Try
      Me.Cursor = Cursors.WaitCursor

      gControlFlags.DeviceMarkingEnabled = menuItem.Checked

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
      Me.Cursor = Cursors.Default
    End Try
  End Sub

  'Private Sub mnuOptionsLockPart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOptionsLockPart.Click
  'Public Sub mnuOptionsLockPart_Click()
  Public Sub mnuOptionsLockPart_Click(ByVal menuItem As System.Windows.Forms.ToolStripMenuItem)
    If menuItem.Checked = True Then
      'If gblnProductionMode Then
      If gControlFlags.ProductionMode = True Then
        'Password required to uncheck
        Dim result As DialogResult
        result = frmPasswordDialog.ShowDialog()
        If result <> Windows.Forms.DialogResult.OK Then
          MessageBox.Show("Password was not entered correctly" & vbCrLf & "'Locked' still checked.", "Password ERROR")
          Exit Sub
        End If
      End If
      gControlFlags.LockIC = False
      menuItem.Checked = False
    Else
      gControlFlags.LockIC = True
      menuItem.Checked = True
    End If
  End Sub

  Public Sub mnuOptionsLockRejectPart_Click(ByVal menuItem As System.Windows.Forms.ToolStripMenuItem)
    If menuItem.Checked = True Then
      'If gblnProductionMode Then
      If gControlFlags.ProductionMode = True Then
        'Password required to uncheck
        Dim result As DialogResult
        result = frmPasswordDialog.ShowDialog()
        If result <> Windows.Forms.DialogResult.OK Then
          MessageBox.Show("Password was not entered correctly" & vbCrLf & "'Locked' still checked.", "Password ERROR")
          Exit Sub
        End If
      End If
      gControlFlags.LockRejectParts = False
      menuItem.Checked = False
    Else
      gControlFlags.LockRejectParts = True
      menuItem.Checked = True
    End If
  End Sub

#End Region

#Region "Menu External Test Methods =================================================="

  Private Sub NoneExternalTestMethods_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      gDatabase.ExternalTestMethodID = Guid.Empty
      gDatabase.ExternalTestMethodRelationshipID = Guid.Empty

      If gControlFlags.UsesDynamicSetupInfo Then
        If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalTestMethodSelected) Then
          gSetupInfoControls(SetupInfoItems.ExternalTestMethodSelected).Text = ""
        End If
      Else
        'lblExternalTestMethodsSelected.Text = ""
      End If

      If mnuExternalTestMethodsDuringIntervalTextBox.Text <> "" Then
        mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
      End If
      If mnuExternalTestMethodsDuringIntervalTextBox.Text <> "" Then
        mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
      End If

      mnuExternalTestMethodsBeforeComboBox.ComboBox.SelectedIndex = 0
      mnuExternalTestMethodsDuringComboBox.ComboBox.SelectedIndex = 0
      mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedIndex = 0

      mnuExternalTestMethodsBefore.CheckState = CheckState.Unchecked
      mnuExternalTestMethodsDuring.CheckState = CheckState.Unchecked
      mnuExternalTestMethodsAfter.CheckState = CheckState.Unchecked
      mnuExternalTestMethodsNone.CheckState = CheckState.Checked

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Private Sub BeforeTestMethodsComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim strExternalTestMethodSelected As String
    Try
      strExternalTestMethodSelected = ""
      If mnuExternalTestMethodsBeforeComboBox.ComboBox.SelectedIndex <> 0 Then
        gDatabase.ExternalTestMethodID = mnuExternalTestMethodsBeforeComboBox.ComboBox.SelectedValue
        gDatabase.ExternalTestMethodRelationshipID = gExternalTestMethodRelationships("Before")

        mnuExternalTestMethodsBefore.CheckState = CheckState.Checked
        mnuExternalTestMethodsDuring.CheckState = CheckState.Unchecked
        mnuExternalTestMethodsAfter.CheckState = CheckState.Unchecked
        mnuExternalTestMethodsNone.CheckState = CheckState.Unchecked

        mnuExternalTestMethodsDuringComboBox.ComboBox.SelectedIndex = 0
        mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedIndex = 0

        'mnuExternalTestMethodsDuringIntervalTextBox.Text = "Interval/Duration = "
        If mnuExternalTestMethodsDuringIntervalTextBox.Text <> "" Then
          mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
        End If
        'mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
        If mnuExternalTestMethodsDuringIntervalTextBox.Text <> "" Then
          mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
        End If

        strExternalTestMethodSelected = "Before " & mnuExternalTestMethodsBeforeComboBox.Text
      Else
        If (mnuExternalTestMethodsDuringComboBox.ComboBox.SelectedIndex = 0 And mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedIndex = 0) Then
          gDatabase.ExternalTestMethodID = Guid.Empty
          gDatabase.ExternalTestMethodRelationshipID = Guid.Empty

          mnuExternalTestMethodsBefore.CheckState = CheckState.Unchecked

          If gControlFlags.UsesDynamicSetupInfo Then
            If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalTestMethodSelected) Then
              gSetupInfoControls(SetupInfoItems.ExternalTestMethodSelected).Text = ""
            End If
          Else
            strExternalTestMethodSelected = ""
          End If
        End If
      End If
      If gControlFlags.UsesDynamicSetupInfo Then
        If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalTestMethodSelected) Then
          gSetupInfoControls(SetupInfoItems.ExternalTestMethodSelected).Text = strExternalTestMethodSelected
        End If
      Else
        'lblExternalTestMethodsSelected.Text = strExternalTestMethodSelected
      End If
      mnuExternalTestMethodsBefore.HideDropDown()
      mnuExternalTestMethods.HideDropDown()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Private Sub DuringTestMethodsComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
  Private Sub DuringTestMethodsComboBox_SelectedIndexChanged()
    Dim strExternalTestMethodSelected As String
    Try
      strExternalTestMethodSelected = ""
      If mnuExternalTestMethodsDuringComboBox.ComboBox.SelectedIndex <> 0 Then
        gDatabase.ExternalTestMethodID = mnuExternalTestMethodsDuringComboBox.ComboBox.SelectedValue
        gDatabase.ExternalTestMethodRelationshipID = gExternalTestMethodRelationships("During")

        mnuExternalTestMethodsDuring.CheckState = CheckState.Checked
        mnuExternalTestMethodsBefore.CheckState = CheckState.Unchecked
        mnuExternalTestMethodsAfter.CheckState = CheckState.Unchecked
        mnuExternalTestMethodsNone.CheckState = CheckState.Unchecked

        mnuExternalTestMethodsBeforeComboBox.ComboBox.SelectedIndex = 0
        mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedIndex = 0

        If mnuExternalTestMethodsDuringIntervalTextBox.Text = "" Then
          strExternalTestMethodSelected = "During " & mnuExternalTestMethodsDuringComboBox.Text
        Else
          strExternalTestMethodSelected = "During " & mnuExternalTestMethodsDuringComboBox.Text _
                    & " at " & mnuExternalTestMethodsDuringInterval.Text & mnuExternalTestMethodsDuringIntervalTextBox.Text
        End If
      Else
        If (mnuExternalTestMethodsBeforeComboBox.ComboBox.SelectedIndex = 0 And mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedIndex = 0) Then
          gDatabase.ExternalTestMethodID = Guid.Empty
          gDatabase.ExternalTestMethodRelationshipID = Guid.Empty

          mnuExternalTestMethodsDuring.CheckState = CheckState.Unchecked

          mnuExternalTestMethodsDuringInterval.Text = "Interval/Duration = "
          mnuExternalTestMethodsDuringIntervalTextBox.Text = ""

          strExternalTestMethodSelected = ""
        End If
      End If

      If gControlFlags.UsesDynamicSetupInfo Then
        If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalTestMethodSelected) Then
          gSetupInfoControls(SetupInfoItems.ExternalTestMethodSelected).Text = strExternalTestMethodSelected
        End If
      Else
        'lblExternalTestMethodsSelected.Text = strExternalTestMethodSelected
      End If

      mnuExternalTestMethodsDuringInterval.HideDropDown()
      mnuExternalTestMethodsDuring.HideDropDown()
      mnuExternalTestMethods.HideDropDown()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Private Sub DuringTestMethodsDurationTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
  '  mnuExternalTestMethodsDuringInterval.Text = "Interval/Duration = " & mnuExternalTestMethodsDuringIntervalTextBox.Text

  '  lblExternalTestMethodsSelected.Text = "During " _
  '        & mnuExternalTestMethodsDuringComboBox.Text & " at " & mnuExternalTestMethodsDuringInterval.Text
  'End Sub

  Private Sub DuringTestMethodsDurationTextBox_Enter(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    Try
      If e.KeyValue = Keys.Enter Then
        Call DuringTestMethodsComboBox_SelectedIndexChanged()
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub DuringTestMethodsDurationTextBox_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs)
    Try
      Call DuringTestMethodsComboBox_SelectedIndexChanged()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Private Sub AfterTestMethodsComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim strExternalTestMethodSelected As String
    Try
      strExternalTestMethodSelected = ""
      If mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedIndex <> 0 Then
        gDatabase.ExternalTestMethodID = mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedValue
        gDatabase.ExternalTestMethodRelationshipID = gExternalTestMethodRelationships("After")

        mnuExternalTestMethodsBefore.CheckState = CheckState.Unchecked
        mnuExternalTestMethodsDuring.CheckState = CheckState.Unchecked
        mnuExternalTestMethodsAfter.CheckState = CheckState.Checked
        mnuExternalTestMethodsNone.CheckState = CheckState.Unchecked

        mnuExternalTestMethodsBeforeComboBox.ComboBox.SelectedIndex = 0
        mnuExternalTestMethodsDuringComboBox.ComboBox.SelectedIndex = 0

        'mnuExternalTestMethodsDuringIntervalTextBox.Text = "Interval/Duration = "
        If mnuExternalTestMethodsDuringIntervalTextBox.Text <> "" Then
          mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
        End If
        'mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
        If mnuExternalTestMethodsDuringIntervalTextBox.Text <> "" Then
          mnuExternalTestMethodsDuringIntervalTextBox.Text = ""
        End If

        strExternalTestMethodSelected = "After " & mnuExternalTestMethodsAfterComboBox.Text
      Else
        If (mnuExternalTestMethodsBeforeComboBox.ComboBox.SelectedIndex = 0 And mnuExternalTestMethodsAfterComboBox.ComboBox.SelectedIndex = 0) Then
          gDatabase.ExternalTestMethodID = Guid.Empty
          gDatabase.ExternalTestMethodRelationshipID = Guid.Empty

          mnuExternalTestMethodsAfter.CheckState = CheckState.Unchecked

          strExternalTestMethodSelected = ""
        End If
      End If
      If gControlFlags.UsesDynamicSetupInfo Then
        If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalTestMethodSelected) Then
          gSetupInfoControls(SetupInfoItems.ExternalTestMethodSelected).Text = strExternalTestMethodSelected
        End If
      Else
        'lblExternalTestMethodsSelected.Text = strExternalTestMethodSelected
      End If

      mnuExternalTestMethodsAfter.HideDropDown()
      mnuExternalTestMethods.HideDropDown()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub SetBackgroundColor()
    If gControlFlags.IgnoreTestBits Or gControlFlags.MasterMode Then
      Me.BackColor = Color.Goldenrod
    Else
      Me.BackColor = mBackgroundColor
    End If
  End Sub



#End Region

#Region "Menu Help =================================================="
  Private Sub mnuHelpAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuHelpAbout.Click
    frmSplash.btnClose.Visible = True
    frmSplash.Show()
    frmSplash.Focus()
  End Sub
#End Region

#End Region



End Class