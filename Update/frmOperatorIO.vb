﻿Public Class frmOperatorIO

  Private mtmrOperatorIO As New Timer

  Private Sub frmOperatorIO_FormClosed(sender As Object, e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
    mtmrOperatorIO.Enabled = False
  End Sub

  Private Sub frmOperatorIO_Load(sender As Object, e As System.EventArgs) Handles Me.Load
    Try

      mtmrOperatorIO.Interval = 2
      AddHandler mtmrOperatorIO.Tick, AddressOf OperatorIOTimer_Tick
      mtmrOperatorIO.Enabled = True

    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    End Try

  End Sub

  Public Sub OperatorIOTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Dim intResult As Integer = 0
    Dim portData As Byte = 0
    Dim lStopwatch As New clsStopWatch
    Try

      mtmrOperatorIO.Enabled = False

      intResult = gDigitalInput.Read(0, portData)

      lStopwatch.DelayTime(25)
      intResult = 0

      intResult = gDigitalInput.Read(0, portData)
      Console.WriteLine(portData)

      If portData = gDIOStructure.StartButtonWithoutProx Then
        lblButtonIndicator.Text = "Button Pressed"
        lblButtonIndicator.BackColor = Color.Lime
      Else
        lblButtonIndicator.Text = "Button Released"
        lblButtonIndicator.BackColor = Color.LightGray
      End If

      If portData = gDIOStructure.Prox Then
        lblProx.Text = "Prox Active"
        lblProx.BackColor = Color.Lime
      Else
        lblProx.Text = "Prox Inactive"
        lblProx.BackColor = Color.LightGray
      End If

      mtmrOperatorIO.Enabled = True

    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call LogAnomaly()
    End Try


  End Sub

End Class