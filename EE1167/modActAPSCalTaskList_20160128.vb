﻿Imports System.Reflection
Imports System.Configuration
Imports System.IO

Module modActAPSCalTaskList

#Region "TSOP Init Tasks =================================================="
  Public Sub InitializeAPSCalVariablesTsop()
    Try
      Call InitializeVariablesTsop() 'Initialize standard variables
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      gControlFlags.UsesExternalDevices = ConfigurationManager.AppSettings("UsesExternalDevices")

      If gControlFlags.UsesExternalDevices Then
        'gDigitalInput
        'gDigitalOutput
        'gDIOStructure.HexHalfByte = ConfigurationManager.AppSettings("HexHalfByte")
        gDIOStructure.YellowLight = ConfigurationManager.AppSettings("YellowLight")
        gDIOStructure.RedLight = ConfigurationManager.AppSettings("RedLight")
        gDIOStructure.GreenLight = ConfigurationManager.AppSettings("GreenLight")
        gDIOStructure.StartButton = ConfigurationManager.AppSettings("StartButton")
        Call InitializeDIOModule()
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      End If

      gSoftwareFiles.LocalExcel = ConfigurationManager.AppSettings("LocalExcelPath")
      gSoftwareFiles.LocalVault = ConfigurationManager.AppSettings("LocalSoftwareVaultPath")
      gSoftwareFiles.NetworkVault = ConfigurationManager.AppSettings("NetworkSoftwareVaultPath")
      gstrEEPROMReadDirectory = ConfigurationManager.AppSettings("EEPROMReadDirectory")
      gdblDUTPowerSupplyUsed = ConfigurationManager.AppSettings("PowerSupplyUsed")
      gstrDUTPowerSupplyMaxCurrent = ConfigurationManager.AppSettings("DUTPowerSupplyMaxCurrent")
      gstrDUTPowerSupplyComPortName = ConfigurationManager.AppSettings("DUTPowerSupplyComPort")
      gstrDUTPowerSupplyBaudRate = ConfigurationManager.AppSettings("DUTPowerSupplyBaudRate")
      'gstrTesterType = ConfigurationManager.AppSettings("TesterType")
      gstrProcessName = ConfigurationManager.AppSettings("ProcessName")
      gstrPostBurninDataFile = ConfigurationManager.AppSettings("PostBurninDataFile")

      gstrTestPassedByteLower = ConfigurationManager.AppSettings("TestPassedByteLower")
      gstrTestPassedByteUpper = ConfigurationManager.AppSettings("TestPassedByteUpper")
      gstrTestStartedByteLower = ConfigurationManager.AppSettings("TestStartedByteLower")
      gstrTestStartedByteUpper = ConfigurationManager.AppSettings("TestStartedByteUpper")

      gstrWrongStationSoundPath = ConfigurationManager.AppSettings("WrongStationSound")
      gintMaxInternalTemperature = ConfigurationManager.AppSettings("MaxInternalTemperature")
      gsngMinBatteryVoltage = ConfigurationManager.AppSettings("MinBatteryVoltage")


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub StartPowerSupplyCommunication()
    Try
      If gControlFlags.UsesExternalDevices And Not gDUTPowerSupplySerialPort.IsOpen Then
        gDUTPowerSupplySerialPort.PortName = gstrDUTPowerSupplyComPortName '"COM5"
        gDUTPowerSupplySerialPort.BaudRate = gstrDUTPowerSupplyBaudRate '9600
        gDUTPowerSupplySerialPort.Parity = IO.Ports.Parity.None
        gDUTPowerSupplySerialPort.StopBits = IO.Ports.StopBits.One
        gDUTPowerSupplySerialPort.ReadBufferSize = 10
        gDUTPowerSupplySerialPort.ReadTimeout = 5000
        gDUTPowerSupplySerialPort.WriteBufferSize = 10
        gDUTPowerSupplySerialPort.WriteTimeout = 5000
        gDUTPowerSupplySerialPort.Open()
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

#End Region

#Region "Param Set Init Tasks =================================================="
  Public Sub InitializeAPSCalVariablesParamSet()
    Try
      Call InitializeVariablesParamSet() 'Initialize standard variables

      If gCanActuator IsNot Nothing Then
        gCanActuator.ShutDown()
        gCanActuator = Nothing
      End If

      gCanActuator = New clsCANActuator(gstrClassConfigFilePath, gDeviceInProcess.ActuatorVariant)
      If gCanActuator.Anomaly IsNot Nothing Then
        gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
        gCanActuator.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gtblParamArrayMap = gDatabase.GetMetricParamArrayMapDataTable
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      gRejectLabel = New clsIntermecRejectLabel
      gRejectLabel.rejectLabelStructure.BOM = gDeviceInProcess.BOM
      gRejectLabel.rejectLabelStructure.ProcessName = gstrProcessName
      gRejectLabel.rejectLabelStructure.Voltage = gElectricalOperationAndLoad.TestVoltage & "V"

      gstrStatusBits = ""

      gProductProgramming = New ProductProgramming(gDatabase)
      If gProductProgramming.Anomaly IsNot Nothing Then
        gAnomaly = gProductProgramming.Anomaly
        gProductProgramming.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


#End Region

#Region "Testing Tasks =================================================="

  Public Sub ResetAPSCalVariablesBeforeTest()
    Try

      Call ResetVariablesBeforeTest()

      'gtmrSendScannerInitTimer.Enabled = True

      gRejectLabel.rejectLabelStructure.Description = ""

      gstrTimeFileName = Format(Now, "hhmmss")


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetPowerSupply()
    Try
      If Not gControlFlags.MasterMode Then
        Call SetSupplyVoltage(gElectricalOperationAndLoad.TestVoltage)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
        frmTsopMain.lblVoltageValue.Text = gElectricalOperationAndLoad.TestVoltage
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub ReadActuatorAppSoftwareVersion()
    Dim strVersion As String = ""
    Dim blnResult As Boolean
    Try
      If gControlFlags.MasterMode Then
        gstrActuatorAppSoftwareVersion = "BITS BYPASSED"
        Exit Sub
      End If
      'read serial
      'frmTsopMain.StatusLabel2.Text = "Reading Actuator Application Software Version"
      'LogActivity(frmTsopMain.StatusLabel2.Text)

      blnResult = gCanActuator.ReadApplicationSoftwareVersion(strVersion)

      If blnResult = False Or strVersion = "" Then

        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
        End If

        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gstrRoutingMessage = "Error Reading Actuator Application Software Version"

        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")

        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)

        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      gDeviceInProcess.SoftwareVersionID = strVersion
      gstrActuatorAppSoftwareVersion = strVersion


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub ReadSerialNumber()
    Dim strSerial As String = ""
    Dim blnResult As Boolean
    Try
      If Not gControlFlags.MasterMode Then
        'frmTsopMain.StatusLabel2.Text = "Reading Serial"
        'LogActivity(frmTsopMain.StatusLabel2.Text)

        blnResult = gCanActuator.ReadSerialNumber(strSerial)

        If blnResult = False Or strSerial = "" Then

          If gCanActuator.Anomaly IsNot Nothing Then
            gCanActuator.Anomaly = Nothing
          End If

          SetSupplyVoltage(0)
          gControlFlags.AbortTest = True
          gstrRoutingMessage = "Error Reading Serial Number"

          gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
          gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")

          gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
          Call LogActivity(gstrRoutingMessage)

          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
          Throw New TsopAnomalyException
        Else
          frmTsopMain.lblEncodedSerialNumber.Text = strSerial
          gOverviewScreenControls(OverviewScreenControls.SerialNumber).Text = strSerial
          gDeviceInProcess.EncodedSerialNumber = strSerial '.txtExternalSerialNumber.Text
          gRejectLabel.rejectLabelStructure.Serial = strSerial
        End If
      Else
        gDeviceInProcess.EncodedSerialNumber = strSerial
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub SetDipAttributes_APSCal()
    Dim drAttributes As DataRow
    Try

      If gControlFlags.MasterMode Then
        gDeviceInProcess.EncodedSerialNumber = gstrMasterNumber
      End If

      gDeviceInProcess.AttributesTable.Clear()

      If gDeviceInProcess.EncodedSerialNumber <> "" Then
        drAttributes = gDeviceInProcess.AttributesTable.NewRow
        drAttributes("AttributeName") = "EncodedSerialNumber"
        drAttributes("AttributeValue") = gDeviceInProcess.EncodedSerialNumber
        drAttributes("IsIdentifier") = 1
        gDeviceInProcess.AttributesTable.Rows.Add(drAttributes)
      End If

      If gDeviceInProcess.SoftwareVersionID <> "" Then
        drAttributes = gDeviceInProcess.AttributesTable.NewRow
        drAttributes("AttributeName") = "SoftwareVersionID"
        drAttributes("AttributeValue") = gDeviceInProcess.SoftwareVersionID
        drAttributes("IsIdentifier") = 0
        gDeviceInProcess.AttributesTable.Rows.Add(drAttributes)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ResetActuator()
    Dim blnResult As Boolean
    Dim lStopwatch As New clsStopWatch
    Dim strSerialNumber As String
    Try
      'If gControlFlags.MasterMode Then
      '  Exit Sub
      'End If

      strSerialNumber = ""

      'frmTsopMain.StatusLabel2.Text = "Resetting Actuator"
      'LogActivity(frmTsopMain.StatusLabel2.Text)

      blnResult = gCanActuator.ResetActuator()

      lStopwatch.DelayTime(200)

      Call gCanActuator.ReadSerialNumber(strSerialNumber) ' Only need this to prevent actuator from running in Burn-In Mode

      If blnResult = False Then
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
        End If

        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gstrRoutingMessage = "Error Resetting Actuator"

        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")

        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)

        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CheckLineStatus()
    Dim strLine1ByteLower As String = ""
    Dim strLine1ByteUpper As String = ""
    Dim strLine2ByteLower As String = ""
    Dim strLine2ByteUpper As String = ""
    Dim strResult As String = ""

    Dim drAssociatedMetric As DataRow
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)

    Try
      If gControlFlags.IgnoreTestBits Then Exit Sub

      gstrRoutingMessage = ""

      'frmTsopMain.StatusLabel2.Text = "Reading Status Bits"
      'Call LogActivity(frmTsopMain.StatusLabel2.Text)

      gstrRoutingMessage = ""

      Call gCanActuator.ReadSpecialMemoryLine1(strLine1ByteUpper, strLine1ByteLower)
      If gCanActuator.Anomaly IsNot Nothing Then
        gCanActuator.Anomaly = Nothing
        gstrRoutingMessage = "Error Reading Line1 Status Bits in CheckLineStatus"
      End If

      If gstrRoutingMessage = "" Then
        Call gCanActuator.ReadSpecialMemoryLine2(strLine2ByteUpper, strLine2ByteLower)
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
          gstrRoutingMessage = "Error Reading Line2 Status Bits in CheckLineStatus"
        End If
      End If

      'Check for Errors
      If gstrRoutingMessage <> "" Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      gstrStatusBits = CheckStatus(strLine1ByteUpper, strLine1ByteLower, strLine2ByteUpper, strLine2ByteLower)
      gstrReadyForStation = gstrStatusBits

      'Lookup Metric that is associated with parameter(property)
      drAssociatedMetric = GetMetricPropertiesRowFromAssiciatedParameter("OnEntryReadyForStationValue")
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If
      'Set metric variable equal to the property's associated metric
      metMetric = gMetrics(drAssociatedMetric.Item("FunctionMetricID"))
      'Add metric to local metric collection
      metMetrics.Add(metMetric.FunctionMetricID, metMetric)

      'strResult = modMain.ProcessFunctionalTestMetrics(ltempKeyCollection)
      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'If strResult = "Failed" And Not gblnAbort Then
      If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject And Not gControlFlags.AbortTest Then
        Select Case gstrStatusBits
          Case "1"
            gstrRoutingMessage = "Take Part to Bit Reset"
          Case "Ready for LeakTest", "Ready for LeakTest with rework"
            gstrRoutingMessage = "Take Part to Leak (A280)"
          Case "Ready for APSCalibration", "Ready for APSCalibration with rework"
            gstrRoutingMessage = "Take Part to APSCAL (A285)"
          Case "Ready for FunctionalTest", "Ready for FunctionalTest with rework"
            gstrRoutingMessage = "Take Part to Functional (A290)"
          Case "Ready for LASERMarker", "Ready for LASERMarker with rework"
            gstrRoutingMessage = "Take Part to Laser (A300)"
          Case "All Stations Pass", "All Stations Pass with rework"
            gstrRoutingMessage = "Contact PC; this part is ready for shipping"
          Case "Started APSCalibration", "Started APSCalibration with rework"
            gstrRoutingMessage = "This Part has already Started APSCAL, Take to Bit Reset"
          Case Else
            gstrRoutingMessage = "Contact PC; this part has invalid bits"
        End Select

        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        Call DisplayMetrics()
        Call SaveTestFailuresToDb()
        Call SaveAlphaTestResultsToDb()
        Call UpdateRTYOEE()
        gstrRoutingMessage = "Part " & gstrStatusBits
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrStatusBits 'strRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        Call PlayWrongStationSound()
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub ReadBurnInMetricValues()
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Dim strResult As String = ""
    Dim blnResult As Boolean
    Dim metMetric As Metric
    Dim blnFailedMetric As Boolean

    Dim strCartID As String = ""
    Dim strCartNumber As String = ""
    Dim strStall As String = ""
    Dim strBom As String = ""

    Try


      ''Temp for debugging
      'Call UpdateLineStatusStarted()
      'Call SetNextTaskListStep("modActAPSCalTaskList", "DownloadActuatorApplication")
      'Exit Sub

      gstrRoutingMessage = ""

      'frmTsopMain.StatusLabel2.Text = "Reading BurnIn Metric Values"
      'Call LogActivity(frmTsopMain.StatusLabel2.Text)

      'Read Temp High Water Mark
      blnResult = gCanActuator.ReadTempHWM(strResult)
      If gCanActuator.Anomaly IsNot Nothing Then
        gCanActuator.Anomaly = Nothing
        gstrRoutingMessage = "Error Reading Temp HWM in ReadBurnInMetricValues"
      Else
        gintHWTemp = CInt(strResult)
      End If

      'Read BurnIn Cycle Count
      If gstrRoutingMessage = "" Then
        blnResult = gCanActuator.ReadBurnInCycles(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
          gstrRoutingMessage = "Error Reading BurnIn Cycles in ReadBurnInMetricValues"
        Else
          gintBurnInCycles = CInt(strResult)
        End If
      End If

      'Read BurnIn Time
      If gstrRoutingMessage = "" Then
        blnResult = gCanActuator.ReadBurnInTime(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
          gstrRoutingMessage = "Error Reading BurnIn Time in ReadBurnInMetricValues"
        Else
          gintBurnInTime = CInt(strResult)
        End If
      End If

      'Check for Errors reading CAN
      If gstrRoutingMessage <> "" Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True

        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")

        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"

        Call LogActivity(gstrRoutingMessage)

        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Filter Metrics
      Call FilterMetricsOnTaskListItem(metMetrics, gFilteredTaskList(gintCurrentTask).Item("TaskIndex"))
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Process Metrics
      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Check for failed Metric
      For Each metMetric In metMetrics.Values
        If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
          blnFailedMetric = True
          Exit For
        End If
      Next

      If blnFailedMetric Then
        Call ReadActuatorEEProm()

        Call EvaluateAPS()

        gstrRoutingMessage = "Part Failed for Burn In Metrics!!" & vbCrLf & "Part CANNOT be reworked" & vbCrLf & "Burn In Cycles: " & gintBurnInCycles & vbCrLf _
            & "Burn  In Time: " & gintBurnInTime & vbCrLf & "HW Temp " & gintHWTemp
        '   & "HW Temp " & gHWTemp

        If gintBurnInCycles < 105 Then

          Call GetPostBurnInData(strCartID, strCartNumber, strStall, strBom)

          Call gDatabase.SendBurnInNotification(strCartID, strCartNumber, strStall, strBom, gDeviceInProcess.EncodedSerialNumber)

          gRejectLabel.rejectLabelStructure.Description = "Burn In Cycles: " & gintBurnInCycles _
            & " Burn  In Time: " & gintBurnInTime & " HW Temp " & gintHWTemp & vbCrLf & "                          Stall: " & strStall & vbCrLf & "        Cart: " & strCartNumber

        Else
          gRejectLabel.rejectLabelStructure.Description = "Burn In Cycles: " & gintBurnInCycles _
            & " Burn  In Time: " & gintBurnInTime & " HW Temp " & gintHWTemp
        End If

        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        Call DisplayMetrics()
        Call SaveTestFailuresToDb()
        Call SaveTestResultsToDb()
        Call UpdateRTYOEE()
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ReadActuatorFaultCount()
    Dim strResult As String = ""
    Dim blnResult As Boolean
    Try
      If gControlFlags.MasterMode Then
        Exit Sub
      End If
      'read serial
      'frmTsopMain.StatusLabel2.Text = "Reading Actuator Fault Count"
      'LogActivity(frmTsopMain.StatusLabel2.Text)

      blnResult = gCanActuator.ReadFaultCount(strResult)

      If blnResult = False Then
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
        End If

        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gstrRoutingMessage = "Error Reading Actuator Fault Count"

        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")

        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)

        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      gintActuatorFaultCount = CInt(strResult)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CheckFaultCountMetric()
    Dim drAssociatedMetric As DataRow
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)

    Try

      'Lookup Metric that is associated with parameter(property)
      drAssociatedMetric = GetMetricPropertiesRowFromAssiciatedParameter("FaultCountMetricLookup")
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If
      'Set metric variable equal to the property's associated metric
      metMetric = gMetrics(drAssociatedMetric.Item("FunctionMetricID"))
      'Add metric to local metric collection
      metMetrics.Add(metMetric.FunctionMetricID, metMetric)

      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        Call DisplayMetrics()
        Call SaveTestFailuresToDb()
        Call SaveTestResultsToDb()
        Call UpdateRTYOEE()
        gstrRoutingMessage = "Part Failed Get Fault Count " & gintActuatorFaultCount
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrStatusBits 'strRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub UpdateLineStatusStarted()
    Dim intRetry As Integer
    Dim strLine1ByteLower As String = ""
    Dim strLine1ByteUpper As String = ""
    Dim strLine2ByteLower As String = ""
    Dim strLine2ByteUpper As String = ""
    Dim strResult As String = ""

    Dim intDynamicWriteBytes(0) As Integer

    Dim drAssociatedMetric As DataRow
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Dim lStopwatch As New clsStopWatch
    Try

      If gControlFlags.MasterMode Or gControlFlags.IgnoreTestBits Then Exit Sub

      gstrRoutingMessage = ""

      If Not gControlFlags.TestFailure And Not gControlFlags.AbortTest Then
        For intRetry = 1 To 5
          If gControlFlags.AbortTest Then Exit Sub

          intDynamicWriteBytes(0) = gDeviceInProcess.TestStatusBitsThisTestPosition
          Call gCanActuator.WriteSpecialMemoryLine1(intDynamicWriteBytes, strResult)
          If gCanActuator.Anomaly IsNot Nothing Then
            gCanActuator.Anomaly = Nothing
            gstrRoutingMessage = "Error Writing Line1 Status Bits in UpdateLineStatusStarted"
            Exit For
          End If

          lStopwatch.DelayTime(100)

          Call gCanActuator.ReadSpecialMemoryLine1(strLine1ByteUpper, strLine1ByteLower)
          If gCanActuator.Anomaly IsNot Nothing Then
            gCanActuator.Anomaly = Nothing
            gstrRoutingMessage = "Error Reading Line1 Status Bits in UpdateLineStatusStarted"
            Exit For
          End If

          If ((strLine1ByteLower = gstrTestStartedByteLower And strLine1ByteUpper = gstrTestStartedByteUpper) Or (strLine1ByteLower = gstrTestStartedByteLower - 1 And strLine1ByteUpper = gstrTestStartedByteUpper)) Then
            Exit For
          End If
        Next

        If gstrRoutingMessage = "" Then
          Call gCanActuator.ReadSpecialMemoryLine2(strLine2ByteUpper, strLine2ByteLower)
          If gCanActuator.Anomaly IsNot Nothing Then
            gCanActuator.Anomaly = Nothing
            gstrRoutingMessage = "Error Reading Line2 Status Bits in UpdateLineStatusStarted"
          End If
        End If

        'Check for Errors
        If gstrRoutingMessage <> "" Then
          SetSupplyVoltage(0)
          gControlFlags.AbortTest = True
          gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
          gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
          gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
          gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
          Call LogActivity(gstrRoutingMessage)
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
          Throw New TsopAnomalyException
        End If

        gstrTestStartedStation = CheckStatus(strLine1ByteUpper, strLine1ByteLower, strLine2ByteUpper, strLine2ByteLower)

        'Lookup Metric that is associated with parameter(property)
        drAssociatedMetric = GetMetricPropertiesRowFromAssiciatedParameter("TestStartedStationValue")
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
        'Set metric variable equal to the property's associated metric
        metMetric = gMetrics(drAssociatedMetric.Item("FunctionMetricID"))
        'Add metric to local metric collection
        metMetrics.Add(metMetric.FunctionMetricID, metMetric)

        'strResult = modMain.ProcessFunctionalTestMetrics(ltempKeyCollection)
        Call ProcessMetrics(metMetrics)
        If Not gAnomaly Is Nothing Then
          Throw New TsopAnomalyException
        End If

        'If strResult = "Failed" Then
        If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
          SetSupplyVoltage(0)
          gControlFlags.AbortTest = True
          gstrRoutingMessage = "Part Failed to set Started Bit"
          Call DisplayMetrics()
          Call SaveTestFailuresToDb()
          Call SaveAlphaTestResultsToDb()
          Call UpdateRTYOEE()
          gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
          gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
          gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
          gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
          Call LogActivity(gstrRoutingMessage)
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
          Throw New TsopAnomalyException
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub ReadAPSType()
    Dim strResult As String = ""
    Dim blnResult As Boolean
    Dim drAssociatedMetric As DataRow
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)

    Try

      If gControlFlags.MasterMode Then
        Exit Sub
      End If

      gstrAPSType = ""

      'frmTsopMain.StatusLabel2.Text = "Reading APS Type"
      'LogActivity(frmTsopMain.StatusLabel2.Text)

      blnResult = gCanActuator.ReadAPSType(strResult)

      If blnResult = False Then
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
        End If

        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gstrRoutingMessage = "Error Reading Reading APS Type"
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      Else
        gstrAPSType = strResult
      End If

      'Lookup Metric that is associated with parameter(property)
      drAssociatedMetric = GetMetricPropertiesRowFromAssiciatedParameter("APSTypeMetricLookup")
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If
      'Set metric variable equal to the property's associated metric
      metMetric = gMetrics(drAssociatedMetric.Item("FunctionMetricID"))
      'Add metric to local metric collection
      metMetrics.Add(metMetric.FunctionMetricID, metMetric)

      'strResult = modMain.ProcessFunctionalTestMetrics(ltempKeyCollection)
      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        Call DisplayMetrics()
        Call SaveTestFailuresToDb()
        Call SaveTestResultsToDb()
        Call UpdateRTYOEE()
        gstrRoutingMessage = "Part " & gstrStatusBits
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrStatusBits 'strRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalibrateAPS()
    Dim strResult As String = ""
    Dim intResult As Integer
    Dim blnResult As Boolean
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Dim intDynamicWriteBytes(0) As Integer
    Dim lDelayStopwatch As New clsStopWatch
    Dim lAPSCalStopWatch As New clsStopWatch
    Dim blnFailedMetric As Boolean
    Try

      If gControlFlags.MasterMode Then Exit Sub

      'frmTsopMain.StatusLabel2.Text = "Calibrating APS"
      'LogActivity(frmTsopMain.StatusLabel2.Text)

      intResult = 2
      gstrRoutingMessage = ""

      'Override default status update time
      intDynamicWriteBytes(0) = 100 'milliseconds, original code had 64d = 40h, probably wanted 100d = 64h
      blnResult = gCanActuator.WriteStatusUpdateTimeOverride(intDynamicWriteBytes, strResult)
      If gCanActuator.Anomaly IsNot Nothing Then
        gCanActuator.Anomaly = Nothing
        gstrRoutingMessage = "Error Writing Status Update Time Override in CalibrateAPS"
      End If

      'APS Calibrate
      If gstrRoutingMessage = "" Then
        intResult = APSCalibrate()

        If intResult = 2 Then
          frmTsopMain.StatusLabel2.Text = frmTsopMain.StatusLabel2.Text & "Calibrating APS Again"
          LogActivity(frmTsopMain.StatusLabel2.Text)

          Call gCanActuator.ResetActuator()
          'lDelayStopwatch.DelayTime(1000) 'Don't need because ResetActuator has built in check for Address Claim Messages, which actuator sends after reset

          intResult = APSCalibrate()

          If gAnomaly IsNot Nothing Then
            gstrRoutingMessage = gAnomaly.AnomalyExceptionMessage
            gAnomaly = Nothing
          End If
        End If
      End If

      If gstrRoutingMessage <> "" Then
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
        End If

        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Eval metrics below
      'Filter Metrics
      Call FilterMetricsOnTaskListItem(metMetrics, gFilteredTaskList(gintCurrentTask).Item("TaskIndex"))
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Process Metrics
      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Check for failed Metric
      blnFailedMetric = False
      For Each metMetric In metMetrics.Values
        If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
          blnFailedMetric = True
          Exit For
        End If
      Next

      If blnFailedMetric Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        Call DisplayMetrics()
        Call SaveTestFailuresToDb()
        Call SaveTestResultsToDb()
        Call UpdateRTYOEE()
        gstrRoutingMessage = "Part Failed Calibrate APS "
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrStatusBits 'strRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ReadActuatorEEProm()
    Dim blnResult As Boolean
    Dim intRecord As Integer
    Dim strEEPromToSave As String
    Dim strFileName As String
    Dim strVersion As String
    Dim intVersionStartPosition As Integer
    Dim intVersionStopPosition As Integer
    Dim strSerialNumber As String = ""
    Dim strTempEEProm As String
    Try

      'frmTsopMain.StatusLabel2.Text = "Reading EEProm"
      'Call LogActivity(frmTsopMain.StatusLabel2.Text)

      strEEPromToSave = ""
      gstrRoutingMessage = ""
      ReDim gstrEEPROMArray(-1)

      Call SetSupplyVoltage(gElectricalOperationAndLoad.TestVoltage)

      blnResult = gCanActuator.EnterBootloader()
      If gCanActuator.Anomaly IsNot Nothing Then
        gCanActuator.Anomaly = Nothing
        gstrRoutingMessage = "Error Entering Bootloader in ReadActuatorEEProm"
      End If

      If gstrRoutingMessage = "" Then
        blnResult = gCanActuator.ReadEEPromIntoIntelHexFormat()
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
          gstrRoutingMessage = "Error Reading EEProm in ReadActuatorEEProm"
        End If
      End If

      Console.WriteLine("Intel Hex Record Count = " & gCanActuator.IntelHexFile.Count)

      If gstrRoutingMessage = "" Then
        blnResult = gCanActuator.StartApplication
        If gCanActuator.Anomaly IsNot Nothing Then
          gCanActuator.Anomaly = Nothing
          gstrRoutingMessage = "Error Restarting Application Software From Bootloader in ReadActuatorEEProm"
        End If
      End If

      Call gCanActuator.ReadSerialNumber(strSerialNumber) ' Only need this to prevent actuator from running in Burn-In Mode

      'Check for Errors
      If gstrRoutingMessage <> "" Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'ReDim gstrEEPROMArray(gCanActuator.IntelHexFile.Count - 1)
      'For intRecord = 0 To gCanActuator.IntelHexFile.Count - 1
      '  If gCanActuator.IntelHexFile(intRecord).RecordType = IntelDataRecord.HEXRecordType.EOFRecord Then
      '    Exit For
      '  End If
      '  gstrEEPROMArray(intRecord) = gCanActuator.RecordtoStringWithChecksum(gCanActuator.IntelHexFile(intRecord))
      '  If gstrEEPROMArray(intRecord) = "" Then Exit For
      '  strEEPromToSave = strEEPromToSave & gstrEEPROMArray(intRecord) & vbCrLf
      'Next

      For intRecord = 0 To gCanActuator.IntelHexFile.Count - 1
        If gCanActuator.IntelHexFile(intRecord).RecordType = IntelDataRecord.HEXRecordType.EOFRecord Then
          Exit For
        End If
        strTempEEProm = gCanActuator.RecordtoStringWithChecksum(gCanActuator.IntelHexFile(intRecord))
        If strTempEEProm = "" Then Exit For
        strEEPromToSave = strEEPromToSave & strTempEEProm & vbCrLf
        ReDim Preserve gstrEEPROMArray(intRecord)
        gstrEEPROMArray(intRecord) = strTempEEProm
      Next

      'Get Excel File Version
      strFileName = UCase(gControlAndInterface.APSCALExcelFilename)
      intVersionStartPosition = strFileName.IndexOf("VERSION_") + 8 + 1
      intVersionStopPosition = strFileName.IndexOf(".XLSM")
      strVersion = Mid(strFileName, intVersionStartPosition, intVersionStopPosition - intVersionStartPosition + 1)

      'Save EEProm to database
      Call gDatabase.UploadEEPROMToSQL(strEEPromToSave, strVersion, gDeviceInProcess.EncodedSerialNumber)
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      'Create Burnin Data Arrays
      Call CreateBurninArraysFromEEProm()
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub EvaluateAPS()
    Dim xlsname As String
    Dim lDate As Date
    Dim lstrYear As String
    Dim lstrMonth As String
    Dim lstrDay As String
    Dim strDate As String = ""
    Dim strEEPROMToSave As String = ""

    Dim oXL As New Microsoft.Office.Interop.Excel.Application
    Dim oWB As Microsoft.Office.Interop.Excel.Workbook
    Dim oSheet As Microsoft.Office.Interop.Excel.Worksheet

    Dim strFilename As String
    Dim intLoop As Integer
    Dim strCell As String = ""

    Try
      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      gstrMonotonicityTestResult = "FAILED"
      gstrBaseCalibrationTestResult = "FAILED"
      gstrCalibrationWraparoundTestResult = "FAILED"
      gstrCalDataIndexTestResult = "FAILED"

      xlsname = gSoftwareFiles.LocalExcel & gControlAndInterface.APSCALExcelFilename

      oXL.Visible = False

      ' Get a new workbook.
      oWB = oXL.Workbooks.Open(Filename:=xlsname)

      oSheet = oWB.Sheets(gControlAndInterface.ExcelOverviewSheet)

      oSheet.Range(gControlAndInterface.ExcelOverviewCheck).Value = "X"

      oSheet = oWB.Sheets(gControlAndInterface.ExcelAPSCALVerifySheet)

      For intLoop = 0 To 32
        strCell = "A" & CStr(intLoop + 4)
        oSheet.Range(strCell).Value = gstrEEPROMArray(intLoop)
      Next

      'oSheet.Range("A4").Value = gstrEEPROMArray(0) '":10071000FFFF0210660842081508E407C107940700"
      'oSheet.Range("A5").Value = gstrEEPROMArray(1) '":1007200062073D071107E006B90690065D06360600"
      'oSheet.Range("A6").Value = gstrEEPROMArray(2) '":100730000B06D905B005880557052D050505D50400"
      'oSheet.Range("A7").Value = gstrEEPROMArray(3) '":10074000A904810454042604FF03D203A3037C0300"
      'oSheet.Range("A8").Value = gstrEEPROMArray(4) '":1007500050031F03F902CD029C0274024A02170200"
      'oSheet.Range("A9").Value = gstrEEPROMArray(5) '":10076000F001C6019201690141010E01E600BF0000"
      'oSheet.Range("A10").Value = gstrEEPROMArray(6) '":100770008B0062003B000800DF0FB80F870F5D0F00"
      'oSheet.Range("A11").Value = gstrEEPROMArray(7) '":10078000360F040FD80EB10E800E540E2E0EFE0D00"
      'oSheet.Range("A12").Value = gstrEEPROMArray(8) '":10079000CF0DA90D7B0D4C0D240DF80CCA0CA10C00"
      'oSheet.Range("A13").Value = gstrEEPROMArray(9) '":1007A000750C460C1D0CF40BC50B990B720B420B00"
      'oSheet.Range("A14").Value = gstrEEPROMArray(10) '":1007B000150BF00ABF0A910A6C0A3C0A0C0AE70900"
      'oSheet.Range("A15").Value = gstrEEPROMArray(11) '":1007C000B7098609630933090209DE08AF087F0800"
      'oSheet.Range("A16").Value = gstrEEPROMArray(12) '":1007D0005B082D08FC07D907AC077B07035B321500"
      'oSheet.Range("A17").Value = gstrEEPROMArray(13) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A18").Value = gstrEEPROMArray(14) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A19").Value = gstrEEPROMArray(15) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A20").Value = gstrEEPROMArray(16) '":1007E000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A21").Value = gstrEEPROMArray(17) ' ":10008000FFFFFFFF91019101FFFFFFFF1F031F0300"
      'oSheet.Range("A22").Value = gstrEEPROMArray(18) ' ":10009000FFFFFFFFFFFFFFFF91019101FFFFFFFF00"
      'oSheet.Range("A23").Value = gstrEEPROMArray(19) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A24").Value = gstrEEPROMArray(20) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A25").Value = gstrEEPROMArray(21) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A26").Value = gstrEEPROMArray(22) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A27").Value = gstrEEPROMArray(23) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A28").Value = gstrEEPROMArray(24) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A29").Value = gstrEEPROMArray(25) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A30").Value = gstrEEPROMArray(26) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A31").Value = gstrEEPROMArray(27) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A32").Value = gstrEEPROMArray(28) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A33").Value = gstrEEPROMArray(29) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A34").Value = gstrEEPROMArray(30) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A35").Value = gstrEEPROMArray(31) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"
      'oSheet.Range("A36").Value = gstrEEPROMArray(32) '":1000A0001F031F03FFFFFFFFFFFFFFFFFFFFFFFF00"

      oSheet.Calculate()

      strFilename = gDeviceInProcess.EncodedSerialNumber & "_" & gstrTimeFileName ' gstrTesterNumber & Format(Now, "yyyyMMddHHmmss")
      gstrAPSCalFileName = strFilename
      gstrXLFileName = gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay & "\" & strFilename & ".xlsm"

      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      If Directory.Exists(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay) = False Then
        Directory.CreateDirectory(gstrEEPROMReadDirectory & lstrMonth & "_" & lstrYear & "\" & lstrDay)
      End If

      oXL.ActiveWorkbook.SaveAs(gstrXLFileName)

      gstrMonotonicityTestResult = CStr(oSheet.Range("E31").Value)
      gstrBaseCalibrationTestResult = CStr(oSheet.Range("E33").Value)
      gstrCalibrationWraparoundTestResult = CStr(oSheet.Range("E35").Value)
      gstrCalDataIndexTestResult = CStr(oSheet.Range("E37").Value)

      Call LogActivity("MonotonictyPassed= " & gstrMonotonicityTestResult)
      Call LogActivity("BaseCalibrationPassed= " & gstrBaseCalibrationTestResult)
      Call LogActivity("CalibrationWraparoundPassed= " & gstrCalibrationWraparoundTestResult)
      Call LogActivity("DataIndexCheckPassed= " & gstrCalDataIndexTestResult)

      oXL.Quit()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CheckBurninElapsedTimeFaults()
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Dim metMetric As Metric
    Dim blnFailedMetric As Boolean

    Try

      gstrRoutingMessage = ""

      'frmTsopMain.StatusLabel2.Text = "Checking Burn In Elapsed Time Faults"
      'Call LogActivity(frmTsopMain.StatusLabel2.Text)

      Call EvaluateBurnInTime()
      If gAnomaly IsNot Nothing Then
        gstrRoutingMessage = gAnomaly.AnomalyExceptionMessage
      End If

      'Check for Errors reading CAN
      If gstrRoutingMessage <> "" Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Filter Metrics
      Call FilterMetricsOnTaskListItem(metMetrics, gFilteredTaskList(gintCurrentTask).Item("TaskIndex"))
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Process Metrics
      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Check for failed Metric
      For Each metMetric In metMetrics.Values
        If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
          blnFailedMetric = True
          Exit For
        End If
      Next

      If blnFailedMetric Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gstrRoutingMessage = "Part Failed for Burn In Elapsed Time Faults: " & gdblBurnInElapsedTimeFaults & " Part CANNOT be reworked"
        Call DisplayMetrics()
        Call SaveTestFailuresToDb()
        Call SaveAlphaTestResultsToDb()
        Call UpdateRTYOEE()
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CheckBurninTempFaults()
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Dim metMetric As Metric
    Dim blnFailedMetric As Boolean

    Try

      gstrRoutingMessage = ""

      'frmTsopMain.StatusLabel2.Text = "Checking Burn In Temp Faults"
      'Call LogActivity(frmTsopMain.StatusLabel2.Text)

      Call EvaluateBurnInTemp()
      If gAnomaly IsNot Nothing Then
        gstrRoutingMessage = gAnomaly.AnomalyExceptionMessage
      End If

      'Check for Errors reading CAN
      If gstrRoutingMessage <> "" Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Filter Metrics
      Call FilterMetricsOnTaskListItem(metMetrics, gFilteredTaskList(gintCurrentTask).Item("TaskIndex"))
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Process Metrics
      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      'Check for failed Metric
      For Each metMetric In metMetrics.Values
        If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
          blnFailedMetric = True
          Exit For
        End If
      Next

      If blnFailedMetric Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gstrRoutingMessage = "Part Failed for Burn In Elapsed Time Faults: " & gdblBurnInElapsedTimeFaults & " Part CANNOT be reworked"
        Call DisplayMetrics()
        Call SaveTestFailuresToDb()
        Call SaveAlphaTestResultsToDb()
        Call UpdateRTYOEE()
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub DownloadActuatorApplication()
    Dim blnResult As Boolean
    Dim strResult As String = ""
    Dim strHardwarePartNumber As String = ""
    Dim intInternalTemperature As Integer
    Dim sngBatteryVoltage As Single
    'Dim metMetrics As New Dictionary(Of Guid, Metric)
    'Dim metMetric As Metric
    'Dim blnFailedMetric As Boolean
    Dim strStatusMessage As String
    Dim lStopWatch As New clsStopWatch
    Try

      'frmTsopMain.StatusLabel2.Text = "Downloading Actuator Application"
      'Call LogActivity(frmTsopMain.StatusLabel2.Text)

      'gControlFlags.AbortTest = True
      'Exit Sub

      strStatusMessage = frmTsopMain.StatusLabel2.Text
      gstrRoutingMessage = ""

      Call SetSupplyVoltage(gElectricalOperationAndLoad.TestVoltage)

      'Write Override Command 
      blnResult = gCanActuator.WriteOverrideCommand(strResult)
      If gCanActuator.Anomaly IsNot Nothing Then
        gstrRoutingMessage = "Error Writing Override Command in DownloadActuatorApplication"
        gCanActuator.Anomaly = Nothing
      End If

      'Read App Software Hex File into memory
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading App Software File"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        Application.DoEvents()
        Call ReadAppSoftwareFile()
        If gAnomaly IsNot Nothing Then
          gstrRoutingMessage = gAnomaly.AnomalyExceptionMessage
          gAnomaly = Nothing
        End If
      End If

      'Read Hardware Part Number
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading Hardware Part Number High Bytes"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        Application.DoEvents()
        blnResult = gCanActuator.ReadHardwarePartNumberHighBytes(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Reading Hardware Part Number in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        Else
          strHardwarePartNumber = strResult
        End If
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading Hardware Part Number Low Bytes"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ReadHardwarePartNumberLowBytes(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Reading Hardware Part Number in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        Else
          strHardwarePartNumber = strHardwarePartNumber & strResult
        End If
      End If
      If gstrRoutingMessage = "" Then
        If strHardwarePartNumber <> gProductProgramming.HardwarePartNumber Then
          gstrRoutingMessage = "Hardware Part Number in Actuator is Incorrect in DownloadActuatorApplication"
        End If
      End If

      'Read Internal Temperature
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading Internal Temperature"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ReadInternalTemperature(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Reading Actuator Internal Temperature in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        Else
          intInternalTemperature = CInt(strResult)
          If intInternalTemperature >= gintMaxInternalTemperature Then
            gstrRoutingMessage = "The Actuator Internal Temperature is Too High to Allow Safe Programming of Flash memory."
          End If
        End If
      End If

      'Read Battery Voltage
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading Battery Voltage"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ReadBatteryVoltage(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Reading Battery Voltage in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        Else
          sngBatteryVoltage = CSng(strResult)
          If sngBatteryVoltage <= gsngMinBatteryVoltage Then
            gstrRoutingMessage = "The Actuator Voltage is too Low to Allow Safe Programming of Flash memory."
          End If
        End If
      End If

      'Reset Actuator
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Resetting Actuator Before Clear EEProm"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ResetActuator()
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Resetting Actuator in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        End If
      End If

      'Clear EEProm All
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Clearing EEProm"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ResetEEPromAll(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Clearing EEProm All in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        End If
      End If

      'Allow 19 seconds for re-initialization to complete
      gCanActuator.Delay(19000)

      'Reset Actuator
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Resetting Actuator After Clear EEProm"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ResetActuator()
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Resetting Actuator in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        End If
      End If

      'Clear EEProm Power Cycles
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Resetting EEProm Power Cycles"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ResetEEPromPowerCycles(strResult)
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Clearing EEProm Power Cycles in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        End If
      End If

      'Wait for Power Cycles to Reset
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Waiting For Power Cycle Reset"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        Call WaitForResetPowerCycles()
        If gAnomaly IsNot Nothing Then
          gstrRoutingMessage = gAnomaly.AnomalyExceptionMessage
          gAnomaly = Nothing
        End If
      End If

      'Enter Bootloader in preparation for Application Download
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Entering Bootloader"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.EnterBootloader()
        If gCanActuator.Anomaly IsNot Nothing Then
          gstrRoutingMessage = "Error Entering Bootloader in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        End If
      End If

      'Download Application File
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Writing Hex File Into EEProm"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        lStopWatch.Reset()
        lStopWatch.Start()
        Call gCanActuator.DownloadIntelHexIntoEEProm()
        Call WaitForApplicationDownloadComplete()
        lStopWatch.Stop()
        Console.WriteLine("Download Time = " & lStopWatch.ElapsedMilliseconds / 1000 & " Seconds")
        Call LogActivity("Download Time = " & lStopWatch.ElapsedMilliseconds / 1000 & " Seconds")
        If gCanActuator.Anomaly IsNot Nothing Then
          If gCanActuator.DefaultBootloaderFault <> 0 Then 'May need other indicator for error detecting DBL attn request response
            'Power Cycle
            'Check for ATTN message every 3ms, use thread.sleep and app.doevents
            Console.WriteLine("Default Bootloader Fault " & gCanActuator.DefaultBootloaderFault & " Occured")
          End If
          gstrRoutingMessage = "Error Downloading Application File in DownloadActuatorApplication"
          gCanActuator.Anomaly = Nothing
        End If
      End If

      'Start Application
      If gstrRoutingMessage = "" Then
        If Not gCanActuator.ApplicationCodeRunning Then
          frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Exiting Bootloader"
          Call LogActivity(frmTsopMain.StatusLabel2.Text)
          blnResult = gCanActuator.StartApplication()
          If gCanActuator.Anomaly IsNot Nothing Then
            gstrRoutingMessage = "Error Re-Starting Application File in DownloadActuatorApplication"
            gCanActuator.Anomaly = Nothing
          End If
        End If
      End If

      'Read Program Calculated Checksum
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Waiting For Checksum Calculation"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = WaitForApplicationChecksumCalculation()
        If gAnomaly IsNot Nothing Then
          'gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gstrRoutingMessage = "Error Reading Program Checksum in DownloadActuatorApplication"
          gAnomaly = Nothing
          'Throw New TsopAnomalyException
        Else
          If blnResult = False Then
            gstrRoutingMessage = "Actuator Calculated Checksum Doesn't Match Checksum in Application Hex File"
          End If
        End If
      End If

      'Read New Software Version
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading New Software Version"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ReadApplicationSoftwareVersion(strResult)
        If gAnomaly IsNot Nothing Then
          'gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gstrRoutingMessage = "Error Reading Software Version in DownloadActuatorApplication"
          gAnomaly = Nothing
          'Throw New TsopAnomalyException
        Else
          gstrPostDownloadActuatorAppSoftwareVersion = strResult
        End If
      End If

      'Read Post Download Fault Counts
      If gstrRoutingMessage = "" Then
        frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading Fault Count After Download"
        Call LogActivity(frmTsopMain.StatusLabel2.Text)
        blnResult = gCanActuator.ReadFaultCount(strResult)
        If gAnomaly IsNot Nothing Then
          'gAnomaly = New clsDbAnomaly(gCanActuator.Anomaly, gDatabase)
          gstrRoutingMessage = "Error Fault Counts in DownloadActuatorApplication"
          gAnomaly = Nothing
          'Throw New TsopAnomalyException
        Else
          gintPostDownloadActuatorFaultCount = CInt(strResult)
        End If
      End If

      'Check for Errors
      If gstrRoutingMessage <> "" Then
        SetSupplyVoltage(0)
        gControlFlags.AbortTest = True
        gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
        gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
        Call LogActivity(gstrRoutingMessage)
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
        Throw New TsopAnomalyException
      End If


      'EE1064 was checking these metrics early, but wasn't directing program flow, so no need to check them early

      ''Filter Metrics
      'Call FilterMetricsOnTaskListItem(metMetrics, gFilteredTaskList(gintCurrentTask).Item("TaskIndex"))
      'If Not gAnomaly Is Nothing Then
      '  Throw New TsopAnomalyException
      'End If

      ''Process Metrics
      'Call ProcessMetrics(metMetrics)
      'If Not gAnomaly Is Nothing Then
      '  Throw New TsopAnomalyException
      'End If

      ''Check for failed Metric
      'For Each metMetric In metMetrics.Values
      '  If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
      '    blnFailedMetric = True
      '    Exit For
      '  End If
      'Next

      'If blnFailedMetric Then
      '  SetSupplyVoltage(0)
      '  gControlFlags.AbortTest = True
      '  gstrRoutingMessage = "Part Failed for Burn In Elapsed Time Faults: " & gdblBurnInElapsedTimeFaults & " Part CANNOT be reworked"
      '  Call DisplayMetrics()
      '  Call SaveTestFailuresToDb()
      '  Call SaveAlphaTestResultsToDb()
      '  Call UpdateRTYOEE()
      '  gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
      '  gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
      '  Call LogActivity(gstrRoutingMessage)
      '  gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
      '  Throw New TsopAnomalyException
      'End If



    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateLineStatusPassed()
    Dim intRetry As Integer

    Dim strLine1ByteLower As String = ""
    Dim strLine1ByteUpper As String = ""
    Dim strLine2ByteLower As String = ""
    Dim strLine2ByteUpper As String = ""
    Dim strResult As String = ""

    Dim intDynamicWriteBytes(0) As Integer

    Dim drAssociatedMetric As DataRow
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Dim lStopwatch As New clsStopWatch

    Try
      If gControlFlags.MasterMode Or gControlFlags.IgnoreTestBits Then Exit Sub

      gstrRoutingMessage = ""

      'Write Override Command 
      Call gCanActuator.WriteOverrideCommand(strResult)
      If gCanActuator.Anomaly IsNot Nothing Then
        gstrRoutingMessage = "Error Writing Override Command in UpdateLineStatusPassed"
        gCanActuator.Anomaly = Nothing
      End If
      lStopwatch.DelayTime(100)

      If Not gControlFlags.TestFailure And Not gControlFlags.AbortTest Then
        For intRetry = 1 To 5
          If gControlFlags.AbortTest Then Exit Sub

          intDynamicWriteBytes(0) = gDeviceInProcess.TestStatusBitsThisTestPosition
          Call gCanActuator.WriteSpecialMemoryLine2(intDynamicWriteBytes, strResult)
          If gCanActuator.Anomaly IsNot Nothing Then
            gCanActuator.Anomaly = Nothing
            gstrRoutingMessage = "Error Writing Line2 Status Bits in UpdateLineStatusPassed"
            Exit For
          End If

          lStopwatch.DelayTime(500)

          Call gCanActuator.ReadSpecialMemoryLine2(strLine2ByteUpper, strLine2ByteLower)
          If gCanActuator.Anomaly IsNot Nothing Then
            gCanActuator.Anomaly = Nothing
            gstrRoutingMessage = "Error Reading Line2 Status Bits in UpdateLineStatusPassed"
            Exit For
          End If

          If (strLine2ByteLower = gstrTestPassedByteLower And strLine2ByteUpper = gstrTestPassedByteUpper) Then
            Exit For
          End If
        Next

        If gstrRoutingMessage = "" Then
          Call gCanActuator.ReadSpecialMemoryLine1(strLine1ByteUpper, strLine1ByteLower)
          If gCanActuator.Anomaly IsNot Nothing Then
            gCanActuator.Anomaly = Nothing
            gstrRoutingMessage = "Error Reading Line1 Status Bits in UpdateLineStatusPassed"
          End If
        End If

        'Check for Errors
        If gstrRoutingMessage <> "" Then
          SetSupplyVoltage(0)
          gControlFlags.AbortTest = True
          gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
          gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
          gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
          gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
          Call LogActivity(gstrRoutingMessage)
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
          Throw New TsopAnomalyException
        End If

        gstrTestPassedStation = CheckStatus(strLine1ByteUpper, strLine1ByteLower, strLine2ByteUpper, strLine2ByteLower)

        'Lookup Metric that is associated with parameter(property)
        drAssociatedMetric = GetMetricPropertiesRowFromAssiciatedParameter("TestPassedStationValue")
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
        'Set metric variable equal to the property's associated metric
        metMetric = gMetrics(drAssociatedMetric.Item("FunctionMetricID"))
        'Add metric to local metric collection
        metMetrics.Add(metMetric.FunctionMetricID, metMetric)

        'strResult = modMain.ProcessFunctionalTestMetrics(ltempKeyCollection)
        Call ProcessMetrics(metMetrics)
        If Not gAnomaly Is Nothing Then
          Throw New TsopAnomalyException
        End If

        If metMetric.PassFailStatus = Metric.PassFailStatusEnum.Reject Then
          SetSupplyVoltage(0)
          gControlFlags.AbortTest = True
          gstrRoutingMessage = "Part Failed to set Passed Bit"
          Call DisplayMetrics()
          Call SaveTestFailuresToDb()
          Call SaveAlphaTestResultsToDb()
          Call UpdateRTYOEE()
          gRejectLabel.rejectLabelStructure.Serial = gDeviceInProcess.EncodedSerialNumber
          gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage
          gRejectLabel.rejectLabelStructure.PrintDateTime = Format(Now, "ddMMMyy hh:mm")
          gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"
          Call LogActivity(gstrRoutingMessage)
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage, gDatabase)
          Throw New TsopAnomalyException
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub


  Public Sub ResetPowerSupply()
    Try
      Call SetSupplyVoltage(0)
      frmTsopMain.lblVoltageValue.Text = 0
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub PrintRejectLabel()

    Try
      If gControlFlags.MasterMode Then
        Exit Sub
      End If

      ''Temp for debugging
      'Exit Sub

      If gstrRoutingMessage <> "" Then
        'print bad label
        gRejectLabel.rejectLabelStructure.PrintDateTime = Now

        gRejectLabel.rejectLabelStructure.Description = gstrRoutingMessage

        gstrRoutingMessage = gstrRoutingMessage & vbCrLf & vbCrLf & " Please attach reject Label"

        gRejectLabel.PrintRejectLabel()

        Call LogActivity(gstrRoutingMessage)
        frmRouting.ShowDialog()
        gstrRoutingMessage = ""
        gControlFlags.TestFailure = True
      End If

      If gControlFlags.MasterMode Then
        MsgBox("Master Mode is Active, Please Disable to run Normal Production")
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub
#End Region

#Region "TSOP End Tasks =================================================="

  Public Sub StopPowerSupplyCommunication()

    Try
      gDUTPowerSupplySerialPort.Close()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub ShutDownCanActuator()
    Try
      gCanActuator.ShutDown()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

#End Region

End Module
