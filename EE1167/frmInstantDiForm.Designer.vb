<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInstantDiForm
  Inherits System.Windows.Forms.Form

  'Form overrides dispose to clean up the component list.
  <System.Diagnostics.DebuggerNonUserCode()> _
  Protected Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing AndAlso components IsNot Nothing Then
      components.Dispose()
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  <System.Diagnostics.DebuggerStepThrough()> _
  Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInstantDiForm))
    Me.pictureBox14 = New System.Windows.Forms.PictureBox()
    Me.pictureBox15 = New System.Windows.Forms.PictureBox()
    Me.pictureBox17 = New System.Windows.Forms.PictureBox()
    Me.PortNum2 = New System.Windows.Forms.Label()
    Me.groupBox3 = New System.Windows.Forms.GroupBox()
    Me.pictureBox10 = New System.Windows.Forms.PictureBox()
    Me.pictureBox11 = New System.Windows.Forms.PictureBox()
    Me.pictureBox13 = New System.Windows.Forms.PictureBox()
    Me.pictureBox12 = New System.Windows.Forms.PictureBox()
    Me.pictureBox16 = New System.Windows.Forms.PictureBox()
    Me.PortHex1 = New System.Windows.Forms.Label()
    Me.PortNum1 = New System.Windows.Forms.Label()
    Me.groupBox2 = New System.Windows.Forms.GroupBox()
    Me.pictureBox00 = New System.Windows.Forms.PictureBox()
    Me.pictureBox01 = New System.Windows.Forms.PictureBox()
    Me.pictureBox03 = New System.Windows.Forms.PictureBox()
    Me.pictureBox02 = New System.Windows.Forms.PictureBox()
    Me.groupBox4 = New System.Windows.Forms.GroupBox()
    Me.pictureBox05 = New System.Windows.Forms.PictureBox()
    Me.pictureBox07 = New System.Windows.Forms.PictureBox()
    Me.pictureBox06 = New System.Windows.Forms.PictureBox()
    Me.PortNum0 = New System.Windows.Forms.Label()
    Me.pictureBox3 = New System.Windows.Forms.PictureBox()
    Me.timer1 = New System.Windows.Forms.Timer(Me.components)
    Me.pictureBox04 = New System.Windows.Forms.PictureBox()
    Me.PortHex0 = New System.Windows.Forms.Label()
    Me.groupBox1 = New System.Windows.Forms.GroupBox()
    Me.pictureBox26 = New System.Windows.Forms.PictureBox()
    Me.pictureBox31 = New System.Windows.Forms.PictureBox()
    Me.pictureBox30 = New System.Windows.Forms.PictureBox()
    Me.groupBox7 = New System.Windows.Forms.GroupBox()
    Me.pictureBox33 = New System.Windows.Forms.PictureBox()
    Me.pictureBox32 = New System.Windows.Forms.PictureBox()
    Me.groupBox8 = New System.Windows.Forms.GroupBox()
    Me.pictureBox34 = New System.Windows.Forms.PictureBox()
    Me.pictureBox35 = New System.Windows.Forms.PictureBox()
    Me.pictureBox37 = New System.Windows.Forms.PictureBox()
    Me.pictureBox36 = New System.Windows.Forms.PictureBox()
    Me.PortHex3 = New System.Windows.Forms.Label()
    Me.panel1 = New System.Windows.Forms.Panel()
    Me.PortNum3 = New System.Windows.Forms.Label()
    Me.groupBox5 = New System.Windows.Forms.GroupBox()
    Me.pictureBox20 = New System.Windows.Forms.PictureBox()
    Me.pictureBox21 = New System.Windows.Forms.PictureBox()
    Me.pictureBox23 = New System.Windows.Forms.PictureBox()
    Me.pictureBox22 = New System.Windows.Forms.PictureBox()
    Me.PortHex2 = New System.Windows.Forms.Label()
    Me.groupBox6 = New System.Windows.Forms.GroupBox()
    Me.pictureBox24 = New System.Windows.Forms.PictureBox()
    Me.pictureBox25 = New System.Windows.Forms.PictureBox()
    Me.pictureBox27 = New System.Windows.Forms.PictureBox()
    Me.imageList1 = New System.Windows.Forms.ImageList(Me.components)
    Me.label4 = New System.Windows.Forms.Label()
    Me.label3 = New System.Windows.Forms.Label()
    Me.label2 = New System.Windows.Forms.Label()
    Me.label1 = New System.Windows.Forms.Label()
    Me.InstantDiCtrl1 = New Automation.BDaq.InstantDiCtrl(Me.components)
    CType(Me.pictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.groupBox3.SuspendLayout()
    CType(Me.pictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.groupBox2.SuspendLayout()
    CType(Me.pictureBox00, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox01, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox03, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox02, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.groupBox4.SuspendLayout()
    CType(Me.pictureBox05, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox07, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox06, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox04, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.groupBox1.SuspendLayout()
    CType(Me.pictureBox26, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox31, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox30, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.groupBox7.SuspendLayout()
    CType(Me.pictureBox33, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox32, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.groupBox8.SuspendLayout()
    CType(Me.pictureBox34, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox35, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox37, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox36, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.panel1.SuspendLayout()
    Me.groupBox5.SuspendLayout()
    CType(Me.pictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.groupBox6.SuspendLayout()
    CType(Me.pictureBox24, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox25, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.pictureBox27, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'pictureBox14
    '
    Me.pictureBox14.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox14.Name = "pictureBox14"
    Me.pictureBox14.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox14.TabIndex = 5
    Me.pictureBox14.TabStop = False
    '
    'pictureBox15
    '
    Me.pictureBox15.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox15.Name = "pictureBox15"
    Me.pictureBox15.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox15.TabIndex = 6
    Me.pictureBox15.TabStop = False
    '
    'pictureBox17
    '
    Me.pictureBox17.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox17.Name = "pictureBox17"
    Me.pictureBox17.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox17.TabIndex = 0
    Me.pictureBox17.TabStop = False
    '
    'PortNum2
    '
    Me.PortNum2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortNum2.Location = New System.Drawing.Point(13, 100)
    Me.PortNum2.Name = "PortNum2"
    Me.PortNum2.Size = New System.Drawing.Size(28, 28)
    Me.PortNum2.TabIndex = 9
    Me.PortNum2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'groupBox3
    '
    Me.groupBox3.Controls.Add(Me.pictureBox10)
    Me.groupBox3.Controls.Add(Me.pictureBox11)
    Me.groupBox3.Controls.Add(Me.pictureBox13)
    Me.groupBox3.Controls.Add(Me.pictureBox12)
    Me.groupBox3.Location = New System.Drawing.Point(183, 54)
    Me.groupBox3.Name = "groupBox3"
    Me.groupBox3.Size = New System.Drawing.Size(110, 38)
    Me.groupBox3.TabIndex = 8
    Me.groupBox3.TabStop = False
    '
    'pictureBox10
    '
    Me.pictureBox10.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox10.Name = "pictureBox10"
    Me.pictureBox10.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox10.TabIndex = 5
    Me.pictureBox10.TabStop = False
    '
    'pictureBox11
    '
    Me.pictureBox11.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox11.Name = "pictureBox11"
    Me.pictureBox11.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox11.TabIndex = 6
    Me.pictureBox11.TabStop = False
    '
    'pictureBox13
    '
    Me.pictureBox13.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox13.Name = "pictureBox13"
    Me.pictureBox13.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox13.TabIndex = 0
    Me.pictureBox13.TabStop = False
    '
    'pictureBox12
    '
    Me.pictureBox12.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox12.Name = "pictureBox12"
    Me.pictureBox12.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox12.TabIndex = 4
    Me.pictureBox12.TabStop = False
    '
    'pictureBox16
    '
    Me.pictureBox16.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox16.Name = "pictureBox16"
    Me.pictureBox16.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox16.TabIndex = 4
    Me.pictureBox16.TabStop = False
    '
    'PortHex1
    '
    Me.PortHex1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortHex1.Location = New System.Drawing.Point(315, 63)
    Me.PortHex1.Name = "PortHex1"
    Me.PortHex1.Size = New System.Drawing.Size(30, 28)
    Me.PortHex1.TabIndex = 7
    Me.PortHex1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'PortNum1
    '
    Me.PortNum1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortNum1.Location = New System.Drawing.Point(13, 61)
    Me.PortNum1.Name = "PortNum1"
    Me.PortNum1.Size = New System.Drawing.Size(28, 28)
    Me.PortNum1.TabIndex = 5
    Me.PortNum1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'groupBox2
    '
    Me.groupBox2.Controls.Add(Me.pictureBox00)
    Me.groupBox2.Controls.Add(Me.pictureBox01)
    Me.groupBox2.Controls.Add(Me.pictureBox03)
    Me.groupBox2.Controls.Add(Me.pictureBox02)
    Me.groupBox2.Location = New System.Drawing.Point(183, 14)
    Me.groupBox2.Name = "groupBox2"
    Me.groupBox2.Size = New System.Drawing.Size(110, 38)
    Me.groupBox2.TabIndex = 4
    Me.groupBox2.TabStop = False
    '
    'pictureBox00
    '
    Me.pictureBox00.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox00.Name = "pictureBox00"
    Me.pictureBox00.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox00.TabIndex = 5
    Me.pictureBox00.TabStop = False
    '
    'pictureBox01
    '
    Me.pictureBox01.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox01.Name = "pictureBox01"
    Me.pictureBox01.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox01.TabIndex = 6
    Me.pictureBox01.TabStop = False
    '
    'pictureBox03
    '
    Me.pictureBox03.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox03.Name = "pictureBox03"
    Me.pictureBox03.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox03.TabIndex = 0
    Me.pictureBox03.TabStop = False
    '
    'pictureBox02
    '
    Me.pictureBox02.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox02.Name = "pictureBox02"
    Me.pictureBox02.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox02.TabIndex = 4
    Me.pictureBox02.TabStop = False
    '
    'groupBox4
    '
    Me.groupBox4.Controls.Add(Me.pictureBox14)
    Me.groupBox4.Controls.Add(Me.pictureBox15)
    Me.groupBox4.Controls.Add(Me.pictureBox17)
    Me.groupBox4.Controls.Add(Me.pictureBox16)
    Me.groupBox4.Location = New System.Drawing.Point(65, 54)
    Me.groupBox4.Name = "groupBox4"
    Me.groupBox4.Size = New System.Drawing.Size(110, 38)
    Me.groupBox4.TabIndex = 6
    Me.groupBox4.TabStop = False
    '
    'pictureBox05
    '
    Me.pictureBox05.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox05.Name = "pictureBox05"
    Me.pictureBox05.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox05.TabIndex = 6
    Me.pictureBox05.TabStop = False
    '
    'pictureBox07
    '
    Me.pictureBox07.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox07.Name = "pictureBox07"
    Me.pictureBox07.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox07.TabIndex = 0
    Me.pictureBox07.TabStop = False
    '
    'pictureBox06
    '
    Me.pictureBox06.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox06.Name = "pictureBox06"
    Me.pictureBox06.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox06.TabIndex = 4
    Me.pictureBox06.TabStop = False
    '
    'PortNum0
    '
    Me.PortNum0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortNum0.Location = New System.Drawing.Point(13, 21)
    Me.PortNum0.Name = "PortNum0"
    Me.PortNum0.Size = New System.Drawing.Size(28, 28)
    Me.PortNum0.TabIndex = 0
    Me.PortNum0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'pictureBox3
    '
    Me.pictureBox3.Image = CType(resources.GetObject("pictureBox3.Image"), System.Drawing.Image)
    Me.pictureBox3.Location = New System.Drawing.Point(266, -4)
    Me.pictureBox3.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
    Me.pictureBox3.Name = "pictureBox3"
    Me.pictureBox3.Size = New System.Drawing.Size(171, 270)
    Me.pictureBox3.TabIndex = 17
    Me.pictureBox3.TabStop = False
    '
    'timer1
    '
    '
    'pictureBox04
    '
    Me.pictureBox04.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox04.Name = "pictureBox04"
    Me.pictureBox04.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox04.TabIndex = 5
    Me.pictureBox04.TabStop = False
    '
    'PortHex0
    '
    Me.PortHex0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortHex0.Location = New System.Drawing.Point(315, 23)
    Me.PortHex0.Name = "PortHex0"
    Me.PortHex0.Size = New System.Drawing.Size(30, 28)
    Me.PortHex0.TabIndex = 3
    Me.PortHex0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'groupBox1
    '
    Me.groupBox1.Controls.Add(Me.pictureBox04)
    Me.groupBox1.Controls.Add(Me.pictureBox05)
    Me.groupBox1.Controls.Add(Me.pictureBox07)
    Me.groupBox1.Controls.Add(Me.pictureBox06)
    Me.groupBox1.Location = New System.Drawing.Point(65, 14)
    Me.groupBox1.Name = "groupBox1"
    Me.groupBox1.Size = New System.Drawing.Size(110, 38)
    Me.groupBox1.TabIndex = 1
    Me.groupBox1.TabStop = False
    '
    'pictureBox26
    '
    Me.pictureBox26.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox26.Name = "pictureBox26"
    Me.pictureBox26.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox26.TabIndex = 4
    Me.pictureBox26.TabStop = False
    '
    'pictureBox31
    '
    Me.pictureBox31.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox31.Name = "pictureBox31"
    Me.pictureBox31.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox31.TabIndex = 6
    Me.pictureBox31.TabStop = False
    '
    'pictureBox30
    '
    Me.pictureBox30.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox30.Name = "pictureBox30"
    Me.pictureBox30.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox30.TabIndex = 5
    Me.pictureBox30.TabStop = False
    '
    'groupBox7
    '
    Me.groupBox7.Controls.Add(Me.pictureBox30)
    Me.groupBox7.Controls.Add(Me.pictureBox31)
    Me.groupBox7.Controls.Add(Me.pictureBox33)
    Me.groupBox7.Controls.Add(Me.pictureBox32)
    Me.groupBox7.Location = New System.Drawing.Point(183, 131)
    Me.groupBox7.Name = "groupBox7"
    Me.groupBox7.Size = New System.Drawing.Size(110, 38)
    Me.groupBox7.TabIndex = 16
    Me.groupBox7.TabStop = False
    '
    'pictureBox33
    '
    Me.pictureBox33.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox33.Name = "pictureBox33"
    Me.pictureBox33.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox33.TabIndex = 0
    Me.pictureBox33.TabStop = False
    '
    'pictureBox32
    '
    Me.pictureBox32.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox32.Name = "pictureBox32"
    Me.pictureBox32.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox32.TabIndex = 4
    Me.pictureBox32.TabStop = False
    '
    'groupBox8
    '
    Me.groupBox8.Controls.Add(Me.pictureBox34)
    Me.groupBox8.Controls.Add(Me.pictureBox35)
    Me.groupBox8.Controls.Add(Me.pictureBox37)
    Me.groupBox8.Controls.Add(Me.pictureBox36)
    Me.groupBox8.Location = New System.Drawing.Point(65, 131)
    Me.groupBox8.Name = "groupBox8"
    Me.groupBox8.Size = New System.Drawing.Size(110, 38)
    Me.groupBox8.TabIndex = 14
    Me.groupBox8.TabStop = False
    '
    'pictureBox34
    '
    Me.pictureBox34.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox34.Name = "pictureBox34"
    Me.pictureBox34.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox34.TabIndex = 5
    Me.pictureBox34.TabStop = False
    '
    'pictureBox35
    '
    Me.pictureBox35.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox35.Name = "pictureBox35"
    Me.pictureBox35.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox35.TabIndex = 6
    Me.pictureBox35.TabStop = False
    '
    'pictureBox37
    '
    Me.pictureBox37.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox37.Name = "pictureBox37"
    Me.pictureBox37.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox37.TabIndex = 0
    Me.pictureBox37.TabStop = False
    '
    'pictureBox36
    '
    Me.pictureBox36.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox36.Name = "pictureBox36"
    Me.pictureBox36.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox36.TabIndex = 4
    Me.pictureBox36.TabStop = False
    '
    'PortHex3
    '
    Me.PortHex3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortHex3.Location = New System.Drawing.Point(315, 140)
    Me.PortHex3.Name = "PortHex3"
    Me.PortHex3.Size = New System.Drawing.Size(30, 28)
    Me.PortHex3.TabIndex = 15
    Me.PortHex3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'panel1
    '
    Me.panel1.AutoScroll = True
    Me.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.panel1.Controls.Add(Me.groupBox7)
    Me.panel1.Controls.Add(Me.PortHex3)
    Me.panel1.Controls.Add(Me.groupBox8)
    Me.panel1.Controls.Add(Me.PortNum3)
    Me.panel1.Controls.Add(Me.groupBox5)
    Me.panel1.Controls.Add(Me.PortHex2)
    Me.panel1.Controls.Add(Me.groupBox6)
    Me.panel1.Controls.Add(Me.PortNum2)
    Me.panel1.Controls.Add(Me.groupBox3)
    Me.panel1.Controls.Add(Me.PortHex1)
    Me.panel1.Controls.Add(Me.groupBox4)
    Me.panel1.Controls.Add(Me.PortNum1)
    Me.panel1.Controls.Add(Me.groupBox2)
    Me.panel1.Controls.Add(Me.PortHex0)
    Me.panel1.Controls.Add(Me.groupBox1)
    Me.panel1.Controls.Add(Me.PortNum0)
    Me.panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.panel1.Location = New System.Drawing.Point(16, 56)
    Me.panel1.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
    Me.panel1.Name = "panel1"
    Me.panel1.Size = New System.Drawing.Size(373, 193)
    Me.panel1.TabIndex = 16
    '
    'PortNum3
    '
    Me.PortNum3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortNum3.Location = New System.Drawing.Point(13, 138)
    Me.PortNum3.Name = "PortNum3"
    Me.PortNum3.Size = New System.Drawing.Size(28, 28)
    Me.PortNum3.TabIndex = 13
    Me.PortNum3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'groupBox5
    '
    Me.groupBox5.Controls.Add(Me.pictureBox20)
    Me.groupBox5.Controls.Add(Me.pictureBox21)
    Me.groupBox5.Controls.Add(Me.pictureBox23)
    Me.groupBox5.Controls.Add(Me.pictureBox22)
    Me.groupBox5.Location = New System.Drawing.Point(183, 93)
    Me.groupBox5.Name = "groupBox5"
    Me.groupBox5.Size = New System.Drawing.Size(110, 38)
    Me.groupBox5.TabIndex = 12
    Me.groupBox5.TabStop = False
    '
    'pictureBox20
    '
    Me.pictureBox20.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox20.Name = "pictureBox20"
    Me.pictureBox20.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox20.TabIndex = 5
    Me.pictureBox20.TabStop = False
    '
    'pictureBox21
    '
    Me.pictureBox21.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox21.Name = "pictureBox21"
    Me.pictureBox21.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox21.TabIndex = 6
    Me.pictureBox21.TabStop = False
    '
    'pictureBox23
    '
    Me.pictureBox23.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox23.Name = "pictureBox23"
    Me.pictureBox23.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox23.TabIndex = 0
    Me.pictureBox23.TabStop = False
    '
    'pictureBox22
    '
    Me.pictureBox22.Location = New System.Drawing.Point(29, 9)
    Me.pictureBox22.Name = "pictureBox22"
    Me.pictureBox22.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox22.TabIndex = 4
    Me.pictureBox22.TabStop = False
    '
    'PortHex2
    '
    Me.PortHex2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.PortHex2.Location = New System.Drawing.Point(315, 102)
    Me.PortHex2.Name = "PortHex2"
    Me.PortHex2.Size = New System.Drawing.Size(30, 28)
    Me.PortHex2.TabIndex = 11
    Me.PortHex2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'groupBox6
    '
    Me.groupBox6.Controls.Add(Me.pictureBox24)
    Me.groupBox6.Controls.Add(Me.pictureBox25)
    Me.groupBox6.Controls.Add(Me.pictureBox27)
    Me.groupBox6.Controls.Add(Me.pictureBox26)
    Me.groupBox6.Location = New System.Drawing.Point(65, 93)
    Me.groupBox6.Name = "groupBox6"
    Me.groupBox6.Size = New System.Drawing.Size(110, 38)
    Me.groupBox6.TabIndex = 10
    Me.groupBox6.TabStop = False
    '
    'pictureBox24
    '
    Me.pictureBox24.Location = New System.Drawing.Point(83, 9)
    Me.pictureBox24.Name = "pictureBox24"
    Me.pictureBox24.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox24.TabIndex = 5
    Me.pictureBox24.TabStop = False
    '
    'pictureBox25
    '
    Me.pictureBox25.Location = New System.Drawing.Point(56, 9)
    Me.pictureBox25.Name = "pictureBox25"
    Me.pictureBox25.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox25.TabIndex = 6
    Me.pictureBox25.TabStop = False
    '
    'pictureBox27
    '
    Me.pictureBox27.Location = New System.Drawing.Point(2, 9)
    Me.pictureBox27.Name = "pictureBox27"
    Me.pictureBox27.Size = New System.Drawing.Size(26, 27)
    Me.pictureBox27.TabIndex = 0
    Me.pictureBox27.TabStop = False
    '
    'imageList1
    '
    Me.imageList1.ImageStream = CType(resources.GetObject("imageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
    Me.imageList1.TransparentColor = System.Drawing.Color.Transparent
    Me.imageList1.Images.SetKeyName(0, "ledLow.png")
    Me.imageList1.Images.SetKeyName(1, "ledHigh.png")
    '
    'label4
    '
    Me.label4.AutoSize = True
    Me.label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.label4.Location = New System.Drawing.Point(333, 41)
    Me.label4.Name = "label4"
    Me.label4.Size = New System.Drawing.Size(29, 15)
    Me.label4.TabIndex = 15
    Me.label4.Text = "Hex"
    '
    'label3
    '
    Me.label3.AutoSize = True
    Me.label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.label3.Location = New System.Drawing.Point(211, 41)
    Me.label3.Name = "label3"
    Me.label3.Size = New System.Drawing.Size(90, 15)
    Me.label3.TabIndex = 14
    Me.label3.Text = "3                       0"
    '
    'label2
    '
    Me.label2.AutoSize = True
    Me.label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.label2.Location = New System.Drawing.Point(75, 41)
    Me.label2.Name = "label2"
    Me.label2.Size = New System.Drawing.Size(107, 15)
    Me.label2.TabIndex = 13
    Me.label2.Text = "Bit 7                       4"
    '
    'label1
    '
    Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.label1.Location = New System.Drawing.Point(-7, 39)
    Me.label1.Name = "label1"
    Me.label1.Size = New System.Drawing.Size(92, 18)
    Me.label1.TabIndex = 12
    Me.label1.Text = "Port No."
    Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'InstantDiCtrl1
    '
    Me.InstantDiCtrl1._StateStream = CType(resources.GetObject("InstantDiCtrl1._StateStream"), Automation.BDaq.DeviceStateStreamer)
    '
    'frmInstantDiForm
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(422, 267)
    Me.Controls.Add(Me.panel1)
    Me.Controls.Add(Me.label4)
    Me.Controls.Add(Me.label3)
    Me.Controls.Add(Me.label2)
    Me.Controls.Add(Me.label1)
    Me.Controls.Add(Me.pictureBox3)
    Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmInstantDiForm"
    Me.Text = "Instant DI"
    CType(Me.pictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
    Me.groupBox3.ResumeLayout(False)
    CType(Me.pictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
    Me.groupBox2.ResumeLayout(False)
    CType(Me.pictureBox00, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox01, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox03, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox02, System.ComponentModel.ISupportInitialize).EndInit()
    Me.groupBox4.ResumeLayout(False)
    CType(Me.pictureBox05, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox07, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox06, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox04, System.ComponentModel.ISupportInitialize).EndInit()
    Me.groupBox1.ResumeLayout(False)
    CType(Me.pictureBox26, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox31, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox30, System.ComponentModel.ISupportInitialize).EndInit()
    Me.groupBox7.ResumeLayout(False)
    CType(Me.pictureBox33, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox32, System.ComponentModel.ISupportInitialize).EndInit()
    Me.groupBox8.ResumeLayout(False)
    CType(Me.pictureBox34, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox35, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox37, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox36, System.ComponentModel.ISupportInitialize).EndInit()
    Me.panel1.ResumeLayout(False)
    Me.groupBox5.ResumeLayout(False)
    CType(Me.pictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
    Me.groupBox6.ResumeLayout(False)
    CType(Me.pictureBox24, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox25, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.pictureBox27, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub
  Private WithEvents pictureBox14 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox15 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox17 As System.Windows.Forms.PictureBox
  Private WithEvents PortNum2 As System.Windows.Forms.Label
  Private WithEvents groupBox3 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox10 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox11 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox13 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox12 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox16 As System.Windows.Forms.PictureBox
  Private WithEvents PortHex1 As System.Windows.Forms.Label
  Private WithEvents PortNum1 As System.Windows.Forms.Label
  Private WithEvents groupBox2 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox00 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox01 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox03 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox02 As System.Windows.Forms.PictureBox
  Private WithEvents groupBox4 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox05 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox07 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox06 As System.Windows.Forms.PictureBox
  Private WithEvents PortNum0 As System.Windows.Forms.Label
  Private WithEvents pictureBox3 As System.Windows.Forms.PictureBox
  Private WithEvents timer1 As System.Windows.Forms.Timer
  Private WithEvents pictureBox04 As System.Windows.Forms.PictureBox
  Private WithEvents PortHex0 As System.Windows.Forms.Label
  Private WithEvents groupBox1 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox26 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox31 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox30 As System.Windows.Forms.PictureBox
  Private WithEvents groupBox7 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox33 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox32 As System.Windows.Forms.PictureBox
  Private WithEvents groupBox8 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox34 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox35 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox37 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox36 As System.Windows.Forms.PictureBox
  Private WithEvents PortHex3 As System.Windows.Forms.Label
  Private WithEvents panel1 As System.Windows.Forms.Panel
  Private WithEvents PortNum3 As System.Windows.Forms.Label
  Private WithEvents groupBox5 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox20 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox21 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox23 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox22 As System.Windows.Forms.PictureBox
  Private WithEvents PortHex2 As System.Windows.Forms.Label
  Private WithEvents groupBox6 As System.Windows.Forms.GroupBox
  Private WithEvents pictureBox24 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox25 As System.Windows.Forms.PictureBox
  Private WithEvents pictureBox27 As System.Windows.Forms.PictureBox
  Private WithEvents imageList1 As System.Windows.Forms.ImageList
  Private WithEvents label4 As System.Windows.Forms.Label
  Private WithEvents label3 As System.Windows.Forms.Label
  Private WithEvents label2 As System.Windows.Forms.Label
  Private WithEvents label1 As System.Windows.Forms.Label
  Friend WithEvents InstantDiCtrl1 As Automation.BDaq.InstantDiCtrl

End Class
