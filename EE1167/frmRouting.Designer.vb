﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRouting
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
    Me.OK_Button = New System.Windows.Forms.Button()
    Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
    Me.btnViewAnomalyMessage = New System.Windows.Forms.Button()
    Me.SuspendLayout()
    '
    'OK_Button
    '
    Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
    Me.OK_Button.Location = New System.Drawing.Point(452, 356)
    Me.OK_Button.Name = "OK_Button"
    Me.OK_Button.Size = New System.Drawing.Size(67, 23)
    Me.OK_Button.TabIndex = 0
    Me.OK_Button.Text = "OK"
    '
    'RichTextBox1
    '
    Me.RichTextBox1.Font = New System.Drawing.Font("Tahoma", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
    Me.RichTextBox1.ForeColor = System.Drawing.Color.Red
    Me.RichTextBox1.Location = New System.Drawing.Point(33, 28)
    Me.RichTextBox1.Name = "RichTextBox1"
    Me.RichTextBox1.Size = New System.Drawing.Size(514, 230)
    Me.RichTextBox1.TabIndex = 1
    Me.RichTextBox1.Text = ""
    '
    'btnViewAnomalyMessage
    '
    Me.btnViewAnomalyMessage.Location = New System.Drawing.Point(311, 356)
    Me.btnViewAnomalyMessage.Name = "btnViewAnomalyMessage"
    Me.btnViewAnomalyMessage.Size = New System.Drawing.Size(135, 23)
    Me.btnViewAnomalyMessage.TabIndex = 2
    Me.btnViewAnomalyMessage.Text = "View Anomaly Message"
    Me.btnViewAnomalyMessage.UseVisualStyleBackColor = True
    '
    'frmRouting
    '
    Me.AcceptButton = Me.OK_Button
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(599, 391)
    Me.Controls.Add(Me.btnViewAnomalyMessage)
    Me.Controls.Add(Me.RichTextBox1)
    Me.Controls.Add(Me.OK_Button)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
    Me.MaximizeBox = False
    Me.MinimizeBox = False
    Me.Name = "frmRouting"
    Me.ShowInTaskbar = False
    Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
    Me.Text = "Routing"
    Me.TopMost = True
    Me.ResumeLayout(False)

  End Sub
    Friend WithEvents OK_Button As System.Windows.Forms.Button
  Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
  Friend WithEvents btnViewAnomalyMessage As System.Windows.Forms.Button

End Class
