﻿Module modAdvantechUSB4716

  Public gDigitalInput As Automation.BDaq.InstantDiCtrl
  Public gDigitalOutput As Automation.BDaq.InstantDoCtrl


  Public Sub InitializeDIOModule()
    Try
      gDigitalInput = New Automation.BDaq.InstantDiCtrl
      gDigitalOutput = New Automation.BDaq.InstantDoCtrl

      gDigitalInput.SelectedDevice = New Automation.BDaq.DeviceInformation(0)
      gDigitalOutput.SelectedDevice = New Automation.BDaq.DeviceInformation(0)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Sub ResetLight()
    Dim blnResult As Boolean = False
    Try
      blnResult = gDigitalOutput.Write(0, 0)
      'Me.BackColor = SystemColors.ButtonFace
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

End Module
