

''' <summary>
''' 
''' *********  Anomaly Class *********
''' 
''' Encapsulates data and functionality for anomalies that occur in TSOP. Anomalies in 6th Gen TSOP are managed
''' externally from TSOP through the TSOP Maintenance Application (TMA). This allows anomalies to be defined at an 
''' enterprise level and reused across all TSOPs at all locations. Anomaly definitions originate and are stored 
''' on the Server database at Elkhart. The Elkhart server will push these anomaly definitions to server databases 
''' at other CTS locations. Servers at each location will push the definitions to the Test Station databases at 
''' each location.
''' 
''' Revision History
''' 
''' Date        By  ID        Purpose of modification 
''' 11/20/2008  TER None      Initial Release
''' 05/11/2009  TER None      Modified Initialize Anomaly Parameters
''' 10/25/2012  TER None      Made SubClass that has database capability
''' 
''' 
''' 
''' 
''' 
''' </summary>
''' <remarks></remarks>

#Region "Imports"

#End Region

Public Class clsDbAnomaly
  Inherits clsAnomaly

#Region "Private Member Variables"

#End Region


#Region "Constructors"

  Public Sub New(ByVal anomalyNum As Int32, ByVal moduleName As String, ByVal procedureName As String, _
      ByVal exceptionMessage As String)

    MyBase.new(anomalyNum, moduleName, procedureName, exceptionMessage)

    MyBase.AnomalyDefinitionID = Guid.Empty
    MyBase.AnomalyMessageTitle = String.Empty
    MyBase.AnomalyMessageText = String.Empty

  End Sub

  Public Sub New(ByVal anomalyNum As Int32, ByVal moduleName As String, ByVal procedureName As String, _
      ByVal exceptionMessage As String, ByVal db As Database)

    MyBase.new(anomalyNum, moduleName, procedureName, exceptionMessage)

    MyBase.AnomalyDefinitionID = Guid.Empty
    MyBase.AnomalyMessageTitle = String.Empty
    MyBase.AnomalyMessageText = String.Empty

    If Not (db Is Nothing) Then
      MyBase.AnomalyNumber = -1
      Call InitializeAnomaly(anomalyNum, db)
      Call InitializeAnomalyActions(db)
    End If

  End Sub

  Public Sub New(ByVal anomalyNum As Int32, ByVal moduleName As String, ByVal procedureName As String, _
      ByVal exceptionMessage As String, ByVal db As Database, ByVal taskNumber As Integer, ByVal taskName As String)

    MyBase.new(anomalyNum, moduleName, procedureName, exceptionMessage, taskNumber, taskName)

    MyBase.AnomalyDefinitionID = Guid.Empty
    MyBase.AnomalyMessageTitle = String.Empty
    MyBase.AnomalyMessageText = String.Empty

    If Not (db Is Nothing) Then
      MyBase.AnomalyNumber = -1
      Call InitializeAnomaly(anomalyNum, db)
      Call InitializeAnomalyActions(db)
    End If

  End Sub

  Public Sub New(ByVal anomalyNum As Int32, ByVal moduleName As String, ByVal procedureName As String, _
      ByVal db As Database)

    MyBase.new(anomalyNum, moduleName, procedureName, String.Empty)

    MyBase.AnomalyDefinitionID = Guid.Empty
    MyBase.AnomalyMessageTitle = String.Empty
    MyBase.AnomalyMessageText = String.Empty

    If Not (db Is Nothing) Then
      MyBase.AnomalyNumber = -1
      Call InitializeAnomaly(anomalyNum, db)
      Call InitializeAnomalyActions(db)
    End If
  End Sub

  Public Sub New(ByVal baseAnomaly As clsAnomaly, ByVal db As Database)

    MyBase.New(baseAnomaly.AnomalyNumber, baseAnomaly.ModuleName, baseAnomaly.ProcedureName, _
                      baseAnomaly.AnomalyExceptionMessage)

    MyBase.AnomalyAttributes = baseAnomaly.AnomalyAttributes
    MyBase.AnomalyDbError = baseAnomaly.AnomalyDbError
    MyBase.CallerList = baseAnomaly.CallerList
    If baseAnomaly.TaskName IsNot Nothing And baseAnomaly.TaskNumber <> -1 Then
      MyBase.TaskName = baseAnomaly.TaskName
      MyBase.TaskNumber = baseAnomaly.TaskNumber
    End If

    MyBase.AnomalyDefinitionID = Guid.Empty
    MyBase.AnomalyMessageTitle = String.Empty
    MyBase.AnomalyMessageText = String.Empty

    If Not (db Is Nothing) Then
      MyBase.AnomalyNumber = -1
      Call InitializeAnomaly(baseAnomaly.AnomalyNumber, db)
      Call InitializeAnomalyActions(db)
    End If

  End Sub

#End Region

#Region "Public Properties"

#End Region

#Region "Public Methods"

  Public Sub WriteToDatabase(ByVal db As Database)
    Try
      'Insert Tsop Anomaly Record
      Dim TsopAnomalyID As Guid

      If MyBase.AnomalyDefinitionID <> Guid.Empty Then
        If Me.TaskName IsNot Nothing And Me.TaskNumber <> -1 Then
          TsopAnomalyID = db.InsertTsopAnomalyWithTaskInfo(MyBase.AnomalyDefinitionID, Me.TaskNumber, Me.TaskName)
        Else
          TsopAnomalyID = db.InsertTsopAnomaly(MyBase.AnomalyDefinitionID)
        End If
        If TsopAnomalyID = Nothing Then
          Throw New Exception()
        End If
      Else
        Throw New Exception()
      End If

      'Insert Anomaly Attributes
      Dim ds As New DataSet("AttributeSet")
      Dim dt As New DataTable
      Dim dtCopy As New DataTable
      Dim row As DataRow
      Dim newRow As DataRow
      Dim sXmlTemp As String
      Dim sXml As String

      dt.Columns.Add("AttributeName", GetType(String))
      dt.Columns.Add("AttributeValue", GetType(String))
      dt.Clear()
      For Each key As String In MyBase.AnomalyAttributes.Keys
        row = dt.NewRow()
        row.Item("AttributeName") = key
        row.Item("AttributeValue") = CleanAttribute(MyBase.AnomalyAttributes(key))
        dt.Rows.Add(row)
      Next

      sXml = ""

      dtCopy.Columns.Add("AttributeName", GetType(String))
      dtCopy.Columns.Add("AttributeValue", GetType(String))

      For Each row In dt.Rows
        newRow = dtCopy.NewRow
        newRow("AttributeName") = row("AttributeName")
        newRow("AttributeValue") = row("AttributeValue")
        dtCopy.Rows.Add(newRow)
        ds.Tables.Add(dtCopy)
        ds.Tables(0).TableName = "AnomalyAttributes"
        sXmlTemp = ds.GetXml
        ds.Tables.Remove("AnomalyAttributes")
        If sXmlTemp.Length < 7000 Then
          sXml = sXmlTemp
        Else
          ds.Clear()
          dtCopy.Clear()
          'Debug.Print("Write Set")
          'Debug.Print(sXml) 'send this
          db.InsertAnomalyAttributeValues(TsopAnomalyID, sXml)
          newRow = dtCopy.NewRow
          newRow("AttributeName") = row("AttributeName")
          newRow("AttributeValue") = row("AttributeValue")
          dtCopy.Rows.Add(newRow)
        End If
      Next
      If dtCopy.Rows.Count > 0 Then
        ds.Tables.Add(dtCopy)
        ds.Tables(0).TableName = "AnomalyAttributes"
        sXml = ds.GetXml
        db.InsertAnomalyAttributeValues(TsopAnomalyID, sXml)
        If db.DbError Then
          Throw New Exception
        End If
      End If

    Catch ex As Exception
      If db.DbError Then
        db.DbError = False
        'MyBase.AnomalyMessageTitle = "Error Occured Logging Anomaly To Database"
        'MyBase.AnomalyMessageText = db.DbErrorMessage
        'Me.AppendCallerList(db.DbErrorProcedure)
        'Me.AppendCallerList("Anomaly.WriteToDatabase")
      End If
      MyBase.AnomalyDbError = True
    End Try
  End Sub

#End Region

#Region "Private Methods"
  Private Sub InitializeAnomaly(ByVal anomalyNum As Int32, ByVal db As Database)

    Dim dt As New DataTable
    dt = db.GetAnomalyInfo(anomalyNum)
    If db.DbError Then
      db.DbError = False
      MyBase.AnomalyDbError = True
      MyBase.AnomalyMessageTitle = "Error Occured Getting Anomaly Information From Database"
      MyBase.AnomalyNumber = anomalyNum
      MyBase.AnomalyMessageText = db.DbErrorMessage
      Call AppendCallerList("Anomaly.New")
      Call AppendCallerList(db.DbErrorProcedure)
      Exit Sub
    End If

    MyBase.AnomalyNumber = dt.Rows(0).Item("TsopAnomalyNumber")
    If MyBase.AnomalyNumber <> anomalyNum Then
      Me.AddAttribute("OriginalAnomalyCategory", AnomalyCategory)
      Me.AddAttribute("OriginalAnomalyNumberSuffix", AnomalyNumberSuffix)
    End If
    MyBase.AnomalyCategory = dt.Rows(0).Item("AnomalyCategoryName")
    MyBase.AnomalyNumberSuffix = dt.Rows(0).Item("AnomalyNumberSuffix")
    MyBase.AnomalyDefinitionID = dt.Rows(0).Item("AnomalyDefinitionID")
    MyBase.AnomalyMessageTitle = dt.Rows(0).Item("AnomalyMessageTitle")
    MyBase.AnomalyMessageText = dt.Rows(0).Item("AnomalyMessageText")

  End Sub

  Private Sub InitializeAnomalyActions(ByVal db As Database)
    MyBase.AnomalyActions = db.GetAnomalyActionsDataTable(MyBase.AnomalyDefinitionID)
  End Sub

#End Region

End Class
