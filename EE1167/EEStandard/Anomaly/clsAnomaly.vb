

''' <summary>
''' 
''' *********  Anomaly Class *********
''' 
''' Encapsulates data and functionality for anomalies that occur in TSOP. Anomalies in 6th Gen TSOP are managed
''' externally from TSOP through the TSOP Maintenance Application (TMA). This allows anomalies to be defined at an 
''' enterprise level and reused across all TSOPs at all locations. Anomaly definitions originate and are stored 
''' on the Server database at Elkhart. The Elkhart server will push these anomaly definitions to server databases 
''' at other CTS locations. Servers at each location will push the definitions to the Test Station databases at 
''' each location.
''' 
''' Revision History
''' 
''' Date        By  ID        Purpose of modification 
''' 11/20/2008  TER None      Initial Release
''' 05/11/2009  TER None      Modified Initialize Anomaly Parameters
''' 10/25/2012  TER None      Pulled out database capability to create a Base Class.
''' 
''' 
''' 
''' 
''' 
''' </summary>
''' <remarks></remarks>

#Region "Imports"

#End Region

Public Class clsAnomaly

#Region "Private Member Variables"

  Private mAnomalyActions As DataTable
  Private mAnomalyAttributes As New Dictionary(Of String, String)
  Private mCallerList As String = String.Empty
  Private mAnomalyCategory As String
  Private mAnomalyDefinitionID As Guid
  Private mAnomalyDbError As Boolean
  Private mAnomalyExceptionMessage As String
  Private mAnomalyMessageText As String
  Private mAnomalyMessageTitle As String
  Private mAnomalyNumber As Int32
  Private mAnomalyNumberSuffix As Int32
  Private mAnomalyModuleName As String
  Private mAnomalyProcedureName As String
  Private mintTaskNumber As Integer = -1
  Private mstrTaskName As String

#End Region

#Region "Private Methods"

#End Region

#Region "Constructors"

  Public Sub New(ByVal anomalyNum As Int32, ByVal moduleName As String, ByVal procedureName As String, _
      ByVal exceptionMessage As String)

    Me.mAnomalyDefinitionID = Guid.Empty
    Me.mAnomalyMessageTitle = String.Empty
    Me.mAnomalyMessageText = String.Empty

    Me.mAnomalyNumber = anomalyNum
    Me.mAnomalyModuleName = moduleName
    Me.mAnomalyProcedureName = procedureName
    Me.mAnomalyExceptionMessage = exceptionMessage

  End Sub

  Public Sub New(ByVal anomalyNum As Int32, ByVal moduleName As String, ByVal procedureName As String, _
    ByVal exceptionMessage As String, ByVal taskNumber As Integer, ByVal taskName As String)

    Me.mAnomalyDefinitionID = Guid.Empty
    Me.mAnomalyMessageTitle = String.Empty
    Me.mAnomalyMessageText = String.Empty

    Me.mAnomalyNumber = anomalyNum
    Me.mAnomalyModuleName = moduleName
    Me.mAnomalyProcedureName = procedureName
    Me.mAnomalyExceptionMessage = exceptionMessage

    Me.mintTaskNumber = taskNumber
    Me.mstrTaskName = taskName

  End Sub

#End Region

#Region "Public Properties"

  Public Property AnomalyActions() As DataTable
    Get
      Return Me.mAnomalyActions
    End Get
    Set(ByVal value As DataTable)
      Me.mAnomalyActions = value
    End Set
  End Property

  Public Property AnomalyAttributes() As Dictionary(Of String, String)
    Get
      Return Me.mAnomalyAttributes
    End Get
    Set(ByVal value As Dictionary(Of String, String))
      Me.mAnomalyAttributes = value
    End Set
  End Property

  Public Property CallerList() As String
    Get
      Return Me.mCallerList
    End Get
    Set(ByVal value As String)
      Me.mCallerList = value
    End Set
  End Property

  Public Property AnomalyCategory() As String
    Get
      Return Me.mAnomalyCategory
    End Get
    Set(ByVal value As String)
      Me.mAnomalyCategory = value
    End Set
  End Property

  Public Property AnomalyDbError() As Boolean
    Get
      Return Me.mAnomalyDbError
    End Get
    Set(ByVal value As Boolean)
      Me.mAnomalyDbError = value
    End Set
  End Property

  Public Property AnomalyDefinitionID() As Guid
    Get
      Return Me.mAnomalyDefinitionID
    End Get
    Set(ByVal value As Guid)
      Me.mAnomalyDefinitionID = value
    End Set
  End Property

  Public Property AnomalyExceptionMessage() As String
    Get
      Return Me.mAnomalyExceptionMessage
    End Get
    Set(ByVal value As String)
      Me.mAnomalyExceptionMessage = value
    End Set
  End Property

  Public Property AnomalyMessageText() As String
    Get
      Return Me.mAnomalyMessageText
    End Get
    Set(ByVal value As String)
      Me.mAnomalyMessageText = value
    End Set
  End Property

  Public Property AnomalyMessageTitle() As String
    Get
      Return Me.mAnomalyMessageTitle
    End Get
    Set(ByVal value As String)
      Me.mAnomalyMessageTitle = value
    End Set
  End Property

  Public Property AnomalyNumber() As Int32
    Get
      Return Me.mAnomalyNumber
    End Get
    Set(ByVal value As Int32)
      Me.mAnomalyNumber = value
    End Set
  End Property

  Public Property AnomalyNumberSuffix() As Int32
    Get
      Return Me.mAnomalyNumberSuffix
    End Get
    Set(ByVal value As Int32)
      Me.mAnomalyNumberSuffix = value
    End Set
  End Property

  Public Property ModuleName() As String
    Get
      Return Me.mAnomalyModuleName
    End Get
    Set(ByVal value As String)
      Me.mAnomalyModuleName = value
    End Set
  End Property

  Public Property ProcedureName() As String
    Get
      Return Me.mAnomalyProcedureName
    End Get
    Set(ByVal value As String)
      Me.mAnomalyProcedureName = value
    End Set
  End Property

  Public Property TaskNumber() As Integer
    Get
      Return Me.mintTaskNumber
    End Get
    Set(ByVal value As Integer)
      Me.mintTaskNumber = value
    End Set
  End Property

  Public Property TaskName() As String
    Get
      Return Me.mstrTaskName
    End Get
    Set(ByVal value As String)
      Me.mstrTaskName = value
    End Set
  End Property

#End Region

#Region "Public Methods"

  Public Sub AddAttribute(ByVal AttributeName As String, ByVal AttributeValue As String)
    Try
      mAnomalyAttributes.Add(AttributeName, AttributeValue)
    Catch ex As Exception

    End Try

  End Sub

  Public Sub AppendCallerList(ByVal element As String)
    If mCallerList = String.Empty Then
      mCallerList = element
    Else
      mCallerList = element & vbCrLf & mCallerList
    End If
  End Sub

  Public Function CleanAttribute(ByVal strAttribute As String) As String
    Dim strCleanedAttribute As String

    strCleanedAttribute = strAttribute
    'strCleanedAttribute = strCleanedAttribute.Replace("'", """")
    strCleanedAttribute = strCleanedAttribute.Replace(vbCrLf, ", ")

    Return strCleanedAttribute
  End Function

#End Region

End Class
