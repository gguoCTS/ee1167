
Option Explicit On
Module Module2
	
	
	Public Structure SystemTime
		Dim wYear As Short
		Dim wMonth As Short
		Dim wDayOfWeek As Short
		Dim wDay As Short
		Dim wHour As Short
		Dim wMinute As Short
		Dim wSecond As Short
		Dim wMilliseconds As Short
	End Structure
	

    Public Declare Function TimeToLocalTime Lib "kernel32" (ByRef lpTime As nican.FileTime, ByRef lpLocalTime As nican.FileTime) As Integer
    Public Declare Function TimeToSystemTime Lib "kernel32" (ByRef lpTime As nican.FileTime, ByRef lpSystemTime As SystemTime) As Integer
    Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Integer)
    Public Declare Sub RtlMoveMemory Lib "kernel32" (ByRef hpvDest As Byte, ByRef hpvSource As nican.NCTYPE_CAN_STRUCT, ByVal cbCopy As Integer)
    Public Declare Sub RtlMoveMemory Lib "kernel32" (ByRef hpvDest As nican.NCTYPE_CAN_STRUCT, ByRef hpvSource As Byte, ByVal cbCopy As Integer)
    Public Declare Function FileTimeToLocalFileTime Lib "kernel32" (ByRef lpFileTime As nican.FileTime, ByRef lpLocalFileTime As nican.FileTime) As Integer
    Public Declare Function FileTimeToSystemTime Lib "kernel32" (ByRef lpFileTime As nican.FileTime, ByRef lpSystemTime As SystemTime) As Integer
End Module