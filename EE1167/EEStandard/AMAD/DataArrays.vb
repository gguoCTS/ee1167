
Option Explicit On
Option Strict Off

''' <summary>
''' DataArrays Class, Revision History
''' 
''' ID           By   Ver      Date        Purpose/Description
'''  
''' 
'''  
''' 
''' 
''' 
''' 
'''  
''' 
''' 
''' 
''' </summary>
''' <remarks></remarks>

Public Class DataArrays

#Region "Private Member Variables"
  'Raw Data Arrays
  Private mAbsoluteLinearityArray() As Double
  Private mAbsoluteLinearityDutOut1Array() As Double
  Private mAbsoluteLinearityDutOut2Array() As Double
  Private mAdrPosEncoderFwd() As Double
  Private mAdrPosEncoderRev() As Double
  Private mAdrPosDutOutPercentVrefFwd() As Double
  Private mAdrPosDutOutPercentVrefRev() As Double

  Private mENRDUTOutValueArray() As Double
  Private mENRDUTOutValueArrayTestRange() As Double
  Private mENRDUTVrefValueArray() As Double
  Private mENREncoderPositionArray() As Double
  Private mENREncoderPositionArrayTestRange() As Double

  Private mForceArray() As Double
  Private mForcePositionArray() As Double
  Private mdblForwardDUTOut1PercentVrefArray() As Double
  Private mdblForwardDUTOut1PositionBasedPercentVrefArray() As Double
  Private mdblForwardDUTOut1VoltageArray() As Double
  Private mdblForwardDUTOut2PercentVrefArray() As Double
  Private mdblForwardDUTOut2PositionBasedPercentVrefArray() As Double
  Private mdblForwardDUTOut2VoltageArray() As Double
  Private mdblForwardDUTOutValueArray() As Double
  Private mdblForwardDUTOutValueArray3() As Double
  Private mdblForwardDUTOutValueArray2() As Double
  Private mdblForwardDUTOutValueArray4() As Double
  Private mdblForwardDUTVrefValueArray() As Double
  Private mdblForwardDUTVrefValueArray2() As Double
  Private mdblForwardEncoderPositionArray() As Double
  Private mdblForwardEncoderPositionArray2() As Double
  Private mdblForwardEncoderPositionPreArray() As Double
  Private mdblForwardEncoderTimeArray() As Double
  Private mdblForwardEncoderTimeArray2() As Double
  Private mForwardENRValueArray() As Double
  Private mForwardEvaluatedTorqueArray() As Double
  Private mForwardEvaluatedENRArray() As Double
  Private mdblForwardForceValueArray() As Double
  Private mdblForwardForceVoltageArray() As Double
  Private mdblForwardPercentVrefArray() As Double
  Private mdblForwardPercentVrefArray2() As Double
  Private mdblForwardPositionBasedForceArray() As Double
  Private mdblForwardPositionBasedPreForceArray() As Double
  Private mdblForwardPositionBasedPercentVrefArray() As Double
  Private mdblForwardPositionBasedPercentVrefArray2() As Double
  Private mdblForwardRepeatabilityValueArray() As Double
  Private mForwardSinglePointLinearityArray() As Double
  Private mForwardTorqueValueArray() As Double
  Private mdblForwardVoltageTimeArray() As Double
  Private mdblForwardVoltageTimeArray2() As Double

  Private mHysteresisArray() As Double
  Private mdblHysteresisDutOut1Array() As Double
  Private mdblHysteresisDutOut2Array() As Double
  Private mHysteresisArrayTestRange() As Double
  Private mHysteresisPositionArray() As Double
  Private mHysteresisPositionArrayTestRange() As Double

  Private mIdealTestCurve() As Double
  Private mIdealTestCurvePosition() As Double
  Private mIndLinearityArray() As Double
  Private mIndLinearityPositionArray() As Double
  Private mInvReverseEvaluatedTorqueArray() As Double

  Private mdblMechanicalSearchForceVoltageArray() As Double
  Private mdblMechanicalSearchEncoderArray() As Double

  Private mMicrogradientArray() As Double
  Private mdblMicrogradientDutOut1Array() As Double
  Private mdblMicrogradientDutOut2Array() As Double
  Private mMicrogradientPositionArray() As Double

  Private mdblOutputCorrelationArray() As Double

  Private mPedalReturnDut1VoltageArray1() As Double
  Private mPedalReturnDut1VoltageArray2() As Double
  Private mPedalReturnDut1VoltageArray3() As Double
  Private mPedalReturnDut1VoltageArray4() As Double
  Private mPedalReturnDut1VoltageArray5() As Double
  Private mPedalReturnDut1VoltageArrayMax() As Double
  Private mPedalReturnDut2VoltageArray1() As Double
  Private mPedalReturnDut2VoltageArray2() As Double
  Private mPedalReturnDut2VoltageArray3() As Double
  Private mPedalReturnDut2VoltageArray4() As Double
  Private mPedalReturnDut2VoltageArray5() As Double
  Private mPedalReturnDut2VoltageArrayMax() As Double
  Private mPedalReturnTimeArray1() As Double
  Private mPedalReturnTimeArray2() As Double
  Private mPedalReturnTimeArray3() As Double
  Private mPedalReturnTimeArray4() As Double
  Private mPedalReturnTimeArray5() As Double
  Private mPedalReturnTimeArrayMax() As Double

  Private mdblReverseDUTOut1PercentVrefArray() As Double
  Private mdblReverseDUTOut1PositionBasedPercentVrefArray() As Double
  Private mdblReverseDUTOut1VoltageArray() As Double
  Private mdblReverseDUTOut2PercentVrefArray() As Double
  Private mdblReverseDUTOut2PositionBasedPercentVrefArray() As Double
  Private mdblReverseDUTOut2VoltageArray() As Double
  Private mdblReverseDUTOutValueArray() As Double
  Private mdblReverseDUTOutValueArray2() As Double
  Private mdblReverseDUTOutValueArray3() As Double
  Private mdblReverseDUTOutValueArray4() As Double
  Private mdblReverseDUTVrefValueArray() As Double
  Private mdblReverseDUTVrefValueArray2() As Double

  Private mdblReverseEncoderPositionArray() As Double
  Private mdblReverseEncoderPositionArray2() As Double
  Private mdblReverseEncoderTimeArray() As Double
  Private mdblReverseEncoderTimeArray2() As Double
  Private mReverseEvaluatedTorqueArray() As Double
  Private mdblReverseForceValueArray() As Double
  Private mdblReverseForceVoltageArray() As Double
  Private mdblReversePercentVrefArray() As Double
  Private mdblReversePercentVrefArray2() As Double
  Private mReversePositionArray() As Double
  Private mdblReversePositionBasedForceArray() As Double
  Private mdblReversePositionBasedPercentVrefArray() As Double
  Private mdblReversePositionBasedPercentVrefArray2() As Double
  Private mdblReverseRepeatabilityValueArray() As Double
  Private mReverseSinglePointLinearityArray() As Double
  Private mReverseTorqueValueArray() As Double
  Private mdblReverseVoltageTimeArray() As Double
  Private mdblReverseVoltageTimeArray2() As Double

  Private mSinglePointLinearityArray() As Double
  Private mdblSinglePointLinearityDutOut1Array() As Double
  Private mdblSinglePointLinearityDutOut2Array() As Double
  Private mSinglePointPositionArray() As Double
  Private mSlopeDeviationArray() As Double

  Private mTorqueXPositionArray() As Double
  Private mTwoPointLinearityArray() As Double
  Private mTwoPointPositionArray() As Double

  Private mdblXPositionArray() As Double
  Private mdblXPositionArray2() As Double


#End Region
#Region "Properties"

  Public Property AbsoluteLinearityArray() As Double()
    Get
      Return mAbsoluteLinearityArray
    End Get
    Set(ByVal value As Double())
      mAbsoluteLinearityArray = value
    End Set
  End Property

  Public Property AbsoluteLinearityDutOut1Array() As Double()
    Get
      Return mAbsoluteLinearityDutOut1Array
    End Get
    Set(ByVal value As Double())
      mAbsoluteLinearityDutOut1Array = value
    End Set
  End Property

  Public Property AbsoluteLinearityDutOut2Array() As Double()
    Get
      Return mAbsoluteLinearityDutOut2Array
    End Get
    Set(ByVal value As Double())
      mAbsoluteLinearityDutOut2Array = value
    End Set
  End Property

  Public Property AdrPosEncoderFwd() As Double()
    Get
      Return mAdrPosEncoderFwd
    End Get
    Set(ByVal value As Double())
      mAdrPosEncoderFwd = value
    End Set
  End Property

  Public Property AdrPosEncoderRev() As Double()
    Get
      Return mAdrPosEncoderRev
    End Get
    Set(ByVal value As Double())
      mAdrPosEncoderRev = value
    End Set
  End Property

  Public Property AdrPosDutOutPercentVrefFwd() As Double()
    Get
      Return mAdrPosDutOutPercentVrefFwd
    End Get
    Set(ByVal value As Double())
      mAdrPosDutOutPercentVrefFwd = value
    End Set
  End Property

  Public Property AdrPosDutOutPercentVrefRev() As Double()
    Get
      Return mAdrPosDutOutPercentVrefRev
    End Get
    Set(ByVal value As Double())
      mAdrPosDutOutPercentVrefRev = value
    End Set
  End Property


  Public Property ENRDUTOutValueArray() As Double()
    Get
      Return mENRDUTOutValueArray
    End Get
    Set(ByVal value As Double())
      mENRDUTOutValueArray = value
    End Set
  End Property

  Public Property ENRDUTOutValueArrayTestRange() As Double() '1.0.1.1DLG
    Get
      Return mENRDUTOutValueArrayTestRange
    End Get
    Set(ByVal value As Double())
      mENRDUTOutValueArrayTestRange = value
    End Set
  End Property

  Public Property ENRDUTVrefValueArray() As Double()
    Get
      Return mENRDUTVrefValueArray
    End Get
    Set(ByVal value As Double())
      mENRDUTVrefValueArray = value
    End Set
  End Property

  Public Property ENREncoderPositionArray() As Double()
    Get
      Return mENREncoderPositionArray
    End Get
    Set(ByVal value As Double())
      mENREncoderPositionArray = value
    End Set
  End Property

  Public Property ENREncoderPositionArrayTestRange() As Double() '1.0.1.1DLG
    Get
      Return mENREncoderPositionArrayTestRange
    End Get
    Set(ByVal value As Double())
      mENREncoderPositionArrayTestRange = value
    End Set
  End Property


  Public Property ForceArray() As Double() '1.0.1.1DLG
    Get
      Return mForceArray
    End Get
    Set(ByVal value As Double())
      mForceArray = value
    End Set
  End Property

  Public Property ForcePositionArray() As Double() '1.0.1.1DLG
    Get
      Return mForcePositionArray
    End Get
    Set(ByVal value As Double())
      mForcePositionArray = value
    End Set
  End Property

  'Public Property ForwardDUTOutValueArray() As Double()
  '  Get
  '    Return mdblForwardDUTOutValueArray
  '  End Get
  '  Set(ByVal value As Double())
  '    mdblForwardDUTOutValueArray = value
  '  End Set
  'End Property

  Public Property ForwardDUTOutValueArray2() As Double()
    Get
      Return mdblForwardDUTOutValueArray2
    End Get
    Set(ByVal value As Double())
      mdblForwardDUTOutValueArray2 = value
    End Set
  End Property

  Public Property ForwardDUTOutValueArray3() As Double()
    Get
      Return mdblForwardDUTOutValueArray3
    End Get
    Set(ByVal value As Double())
      mdblForwardDUTOutValueArray3 = value
    End Set
  End Property

  Public Property ForwardDUTOutValueArray4() As Double()
    Get
      Return mdblForwardDUTOutValueArray4
    End Get
    Set(ByVal value As Double())
      mdblForwardDUTOutValueArray4 = value
    End Set
  End Property

  Public Property ForwardDUTVrefValueArray() As Double()
    Get
      Return mdblForwardDUTVrefValueArray
    End Get
    Set(ByVal value As Double())
      mdblForwardDUTVrefValueArray = value
    End Set
  End Property

  Public Property ForwardDUTVrefValueArray2() As Double()
    Get
      Return mdblForwardDUTVrefValueArray2
    End Get
    Set(ByVal value As Double())
      mdblForwardDUTVrefValueArray2 = value
    End Set
  End Property

  Public Property ForwardDUTOut1PercentVrefArray() As Double()
    Get
      Return Me.mdblForwardDUTOut1PercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblForwardDUTOut1PercentVrefArray = value
    End Set
  End Property

  Public Property ForwardDUTOut1PositionBasedPercentVrefArray() As Double()
    Get
      Return Me.mdblForwardDUTOut1PositionBasedPercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblForwardDUTOut1PositionBasedPercentVrefArray = value
    End Set
  End Property

  Public Property ForwardDUTOut1VoltageArray() As Double()
    Get
      Return mdblForwardDUTOut1VoltageArray
    End Get
    Set(ByVal value As Double())
      mdblForwardDUTOut1VoltageArray = value
    End Set
  End Property

  Public Property ForwardDUTOut2PercentVrefArray() As Double()
    Get
      Return Me.mdblForwardDUTOut2PercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblForwardDUTOut2PercentVrefArray = value
    End Set
  End Property

  Public Property ForwardDUTOut2PositionBasedPercentVrefArray() As Double()
    Get
      Return Me.mdblForwardDUTOut2PositionBasedPercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblForwardDUTOut2PositionBasedPercentVrefArray = value
    End Set
  End Property

  Public Property ForwardDUTOut2VoltageArray() As Double()
    Get
      Return mdblForwardDUTOut2VoltageArray
    End Get
    Set(ByVal value As Double())
      mdblForwardDUTOut2VoltageArray = value
    End Set
  End Property

  Public Property ForwardEncoderPositionPreArray() As Double()
    Get
      Return mdblForwardEncoderPositionPreArray
    End Get
    Set(ByVal value As Double())
      mdblForwardEncoderPositionPreArray = value
    End Set
  End Property

  Public Property ForwardEncoderPositionArray() As Double()
    Get
      Return mdblForwardEncoderPositionArray
    End Get
    Set(ByVal value As Double())
      mdblForwardEncoderPositionArray = value
    End Set
  End Property

  Public Property ForwardEncoderPositionArray2() As Double()
    Get
      Return mdblForwardEncoderPositionArray2
    End Get
    Set(ByVal value As Double())
      mdblForwardEncoderPositionArray2 = value
    End Set
  End Property

  Public Property ForwardEncoderTimeArray() As Double()
    Get
      Return mdblForwardEncoderTimeArray
    End Get
    Set(ByVal value As Double())
      mdblForwardEncoderTimeArray = value
    End Set
  End Property

  Public Property ForwardEncoderTimeArray2() As Double()
    Get
      Return mdblForwardEncoderTimeArray2
    End Get
    Set(ByVal value As Double())
      mdblForwardEncoderTimeArray2 = value
    End Set
  End Property

  Public Property ForwardENRValueArray() As Double()
    Get
      Return mForwardENRValueArray
    End Get
    Set(ByVal value As Double())
      mForwardENRValueArray = value
    End Set
  End Property

  Public Property ForwardEvaluatedENRArray() As Double()
    Get
      Return mForwardEvaluatedENRArray
    End Get
    Set(ByVal value As Double())
      mForwardEvaluatedENRArray = value
    End Set
  End Property

  Public Property ForwardEvaluatedTorqueArray() As Double()
    Get
      Return mForwardEvaluatedTorqueArray
    End Get
    Set(ByVal value As Double())
      mForwardEvaluatedTorqueArray = value
    End Set
  End Property

  Public Property ForwardForceValueArray() As Double()
    Get
      Return Me.mdblForwardForceValueArray
    End Get
    Set(ByVal value As Double())
      Me.mdblForwardForceValueArray = value
    End Set
  End Property

  Public Property ForwardForceVoltageArray() As Double()
    Get
      Return Me.mdblForwardForceVoltageArray
    End Get
    Set(ByVal value As Double())
      Me.mdblForwardForceVoltageArray = value
    End Set
  End Property

  'Public Property ForwardPercentVrefArray() As Double()
  '  Get
  '    Return mdblForwardPercentVrefArray
  '  End Get
  '  Set(ByVal value As Double())
  '    mdblForwardPercentVrefArray = value
  '  End Set
  'End Property

  Public Property ForwardPercentVrefArray2() As Double()
    Get
      Return mdblForwardPercentVrefArray2
    End Get
    Set(ByVal value As Double())
      mdblForwardPercentVrefArray2 = value
    End Set
  End Property

  Public Property ForwardPositionBasedForceArray() As Double()
    Get
      Return mdblForwardPositionBasedForceArray
    End Get
    Set(ByVal value As Double())
      mdblForwardPositionBasedForceArray = value
    End Set
  End Property

  Public Property ForwardPositionBasedPreForceArray() As Double()
    Get
      Return mdblForwardPositionBasedPreForceArray
    End Get
    Set(ByVal value As Double())
      mdblForwardPositionBasedPreForceArray = value
    End Set
  End Property

  'Public Property ForwardPositionBasedPercentVrefArray() As Double()
  '  Get
  '    Return mdblForwardPositionBasedPercentVrefArray
  '  End Get
  '  Set(ByVal value As Double())
  '    mdblForwardPositionBasedPercentVrefArray = value
  '  End Set
  'End Property

  Public Property ForwardPositionBasedPercentVrefArray2() As Double()
    Get
      Return mdblForwardPositionBasedPercentVrefArray2
    End Get
    Set(ByVal value As Double())
      mdblForwardPositionBasedPercentVrefArray2 = value
    End Set
  End Property

  Public Property ForwardRepeatabilityValueArray() As Double()
    Get
      Return Me.mdblForwardRepeatabilityValueArray
    End Get
    Set(ByVal value As Double())
      Me.mdblForwardRepeatabilityValueArray = value
    End Set
  End Property

  Public Property ForwardSinglePointLinearityArray() As Double() '1.0.1.1DLG
    Get
      Return mForwardSinglePointLinearityArray
    End Get
    Set(ByVal value As Double())
      mForwardSinglePointLinearityArray = value
    End Set
  End Property

  Public Property ForwardTorqueValueArray() As Double()
    Get
      Return mForwardTorqueValueArray
    End Get
    Set(ByVal value As Double())
      mForwardTorqueValueArray = value
    End Set
  End Property

  Public Property ForwardVoltageTimeArray() As Double()
    Get
      Return mdblForwardVoltageTimeArray
    End Get
    Set(ByVal value As Double())
      mdblForwardVoltageTimeArray = value
    End Set
  End Property

  Public Property ForwardVoltageTimeArray2() As Double()
    Get
      Return mdblForwardVoltageTimeArray2
    End Get
    Set(ByVal value As Double())
      mdblForwardVoltageTimeArray2 = value
    End Set
  End Property


  Public Property HysteresisArray() As Double()
    Get
      Return mHysteresisArray
    End Get
    Set(ByVal value As Double())
      mHysteresisArray = value
    End Set
  End Property

  Public Property HysteresisDutOut1Array() As Double()
    Get
      Return Me.mdblHysteresisDutOut1Array
    End Get
    Set(ByVal value As Double())
      Me.mdblHysteresisDutOut1Array = value
    End Set
  End Property

  Public Property HysteresisDutOut2Array() As Double()
    Get
      Return Me.mdblHysteresisDutOut2Array
    End Get
    Set(ByVal value As Double())
      Me.mdblHysteresisDutOut2Array = value
    End Set
  End Property

  Public Property HysteresisArrayTestRange() As Double() '1.0.1.1DLG
    Get
      Return mHysteresisArrayTestRange
    End Get
    Set(ByVal value As Double())
      mHysteresisArrayTestRange = value
    End Set
  End Property

  Public Property HysteresisPositionArray() As Double() '1.0.1.1DLG
    Get
      Return mHysteresisPositionArray
    End Get
    Set(ByVal value As Double())
      mHysteresisPositionArray = value
    End Set
  End Property

  Public Property HysteresisPositionArrayTestRange() As Double() '1.0.1.1DLG
    Get
      Return mHysteresisPositionArrayTestRange
    End Get
    Set(ByVal value As Double())
      mHysteresisPositionArrayTestRange = value
    End Set
  End Property


  Public Property IdealTestCurve() As Double() '1.0.1.1DLG
    Get
      Return mIdealTestCurve
    End Get
    Set(ByVal value As Double())
      mIdealTestCurve = value
    End Set
  End Property

  Public Property IdealTestCurvePosition() As Double() '1.0.1.1DLG
    Get
      Return mIdealTestCurvePosition
    End Get
    Set(ByVal value As Double())
      mIdealTestCurvePosition = value
    End Set
  End Property

  Public Property IndLinearityArray() As Double()
    Get
      Return Me.mIndLinearityArray
    End Get
    Set(ByVal value As Double())
      mIndLinearityArray = value
    End Set
  End Property

  Public Property IndLinearityPositionArray() As Double()
    Get
      Return Me.mIndLinearityPositionArray
    End Get
    Set(ByVal value As Double())
      mIndLinearityPositionArray = value
    End Set
  End Property

  Public Property InvReverseEvaluatedTorqueArray() As Double()
    Get
      Return mInvReverseEvaluatedTorqueArray
    End Get
    Set(ByVal value As Double())
      mInvReverseEvaluatedTorqueArray = value
    End Set
  End Property

  Public Property MechanicalSearchEncoderArray() As Double()
    Get
      Return Me.mdblMechanicalSearchEncoderArray
    End Get
    Set(ByVal value As Double())
      Me.mdblMechanicalSearchEncoderArray = value
    End Set
  End Property

  Public Property MechanicalSearchForceVoltageArray() As Double()
    Get
      Return Me.mdblMechanicalSearchForceVoltageArray
    End Get
    Set(ByVal value As Double())
      Me.mdblMechanicalSearchForceVoltageArray = value
    End Set
  End Property

  Public Property MicrogradientArray() As Double()
    Get
      Return mMicrogradientArray
    End Get
    Set(ByVal value As Double())
      mMicrogradientArray = value
    End Set
  End Property

  Public Property MicrogradientDutOut1Array() As Double()
    Get
      Return mdblMicrogradientDutOut1Array
    End Get
    Set(ByVal value As Double())
      mdblMicrogradientDutOut1Array = value
    End Set
  End Property

  Public Property MicrogradientDutOut2Array() As Double()
    Get
      Return mdblMicrogradientDutOut2Array
    End Get
    Set(ByVal value As Double())
      mdblMicrogradientDutOut2Array = value
    End Set
  End Property

  Public Property MicrogradientPositionArray() As Double()
    Get
      Return Me.mMicrogradientPositionArray
    End Get
    Set(ByVal value As Double())
      Me.mMicrogradientPositionArray = value
    End Set
  End Property

  Public Property OutputCorrelationArray() As Double()
    Get
      Return Me.mdblOutputCorrelationArray
    End Get
    Set(ByVal value As Double())
      Me.mdblOutputCorrelationArray = value
    End Set
  End Property


  Public Property PedalReturnDut1VoltageArray1() As Double()
    Get
      Return Me.mPedalReturnDut1VoltageArray1
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut1VoltageArray1 = value
    End Set
  End Property

  Public Property PedalReturnDut1VoltageArray2() As Double()
    Get
      Return Me.mPedalReturnDut1VoltageArray2
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut1VoltageArray2 = value
    End Set
  End Property

  Public Property PedalReturnDut1VoltageArray3() As Double()
    Get
      Return Me.mPedalReturnDut1VoltageArray3
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut1VoltageArray3 = value
    End Set
  End Property

  Public Property PedalReturnDut1VoltageArray4() As Double()
    Get
      Return Me.mPedalReturnDut1VoltageArray4
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut1VoltageArray4 = value
    End Set
  End Property

  Public Property PedalReturnDut1VoltageArray5() As Double()
    Get
      Return Me.mPedalReturnDut1VoltageArray5
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut1VoltageArray5 = value
    End Set
  End Property

  Public Property PedalReturnDut1VoltageArrayMax() As Double()
    Get
      Return Me.mPedalReturnDut1VoltageArrayMax
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut1VoltageArrayMax = value
    End Set
  End Property

  Public Property PedalReturnDut2VoltageArray1() As Double()
    Get
      Return Me.mPedalReturnDut2VoltageArray1
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut2VoltageArray1 = value
    End Set
  End Property

  Public Property PedalReturnDut2VoltageArray2() As Double()
    Get
      Return Me.mPedalReturnDut2VoltageArray2
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut2VoltageArray2 = value
    End Set
  End Property

  Public Property PedalReturnDut2VoltageArray3() As Double()
    Get
      Return Me.mPedalReturnDut2VoltageArray3
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut2VoltageArray3 = value
    End Set
  End Property

  Public Property PedalReturnDut2VoltageArray4() As Double()
    Get
      Return Me.mPedalReturnDut2VoltageArray4
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut2VoltageArray4 = value
    End Set
  End Property

  Public Property PedalReturnDut2VoltageArray5() As Double()
    Get
      Return Me.mPedalReturnDut2VoltageArray5
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut2VoltageArray5 = value
    End Set
  End Property

  Public Property PedalReturnDut2VoltageArrayMax() As Double()
    Get
      Return Me.mPedalReturnDut2VoltageArrayMax
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnDut2VoltageArrayMax = value
    End Set
  End Property

  Public Property PedalReturnTimeArray1() As Double()
    Get
      Return Me.mPedalReturnTimeArray1
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnTimeArray1 = value
    End Set
  End Property

  Public Property PedalReturnTimeArray2() As Double()
    Get
      Return Me.mPedalReturnTimeArray2
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnTimeArray2 = value
    End Set
  End Property

  Public Property PedalReturnTimeArray3() As Double()
    Get
      Return Me.mPedalReturnTimeArray3
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnTimeArray3 = value
    End Set
  End Property

  Public Property PedalReturnTimeArray4() As Double()
    Get
      Return Me.mPedalReturnTimeArray4
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnTimeArray4 = value
    End Set
  End Property

  Public Property PedalReturnTimeArray5() As Double()
    Get
      Return Me.mPedalReturnTimeArray5
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnTimeArray5 = value
    End Set
  End Property

  Public Property PedalReturnTimeArrayMax() As Double()
    Get
      Return Me.mPedalReturnTimeArrayMax
    End Get
    Set(ByVal value As Double())
      Me.mPedalReturnTimeArrayMax = value
    End Set
  End Property

  Public Property ReverseDUTOut1PercentVrefArray() As Double()
    Get
      Return Me.mdblReverseDUTOut1PercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReverseDUTOut1PercentVrefArray = value
    End Set
  End Property

  Public Property ReverseDUTOut1PositionBasedPercentVrefArray() As Double()
    Get
      Return Me.mdblReverseDUTOut1PositionBasedPercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReverseDUTOut1PositionBasedPercentVrefArray = value
    End Set
  End Property

  Public Property ReverseDUTOut1VoltageArray() As Double()
    Get
      Return mdblReverseDUTOut1VoltageArray
    End Get
    Set(ByVal value As Double())
      mdblReverseDUTOut1VoltageArray = value
    End Set
  End Property

  Public Property ReverseDUTOut2PercentVrefArray() As Double()
    Get
      Return Me.mdblReverseDUTOut2PercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReverseDUTOut2PercentVrefArray = value
    End Set
  End Property

  Public Property ReverseDUTOut2PositionBasedPercentVrefArray() As Double()
    Get
      Return Me.mdblReverseDUTOut2PositionBasedPercentVrefArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReverseDUTOut2PositionBasedPercentVrefArray = value
    End Set
  End Property

  Public Property ReverseDUTOut2VoltageArray() As Double()
    Get
      Return mdblReverseDUTOut2VoltageArray
    End Get
    Set(ByVal value As Double())
      mdblReverseDUTOut2VoltageArray = value
    End Set
  End Property

  'Public Property ReverseDUTOutValueArray() As Double()
  '  Get
  '    Return mdblReverseDUTOutValueArray
  '  End Get
  '  Set(ByVal value As Double())
  '    mdblReverseDUTOutValueArray = value
  '  End Set
  'End Property

  Public Property ReverseDUTOutValueArray2() As Double()
    Get
      Return mdblReverseDUTOutValueArray2
    End Get
    Set(ByVal value As Double())
      mdblReverseDUTOutValueArray2 = value
    End Set
  End Property

  Public Property ReverseDUTOutValueArray3() As Double()
    Get
      Return mdblReverseDUTOutValueArray3
    End Get
    Set(ByVal value As Double())
      mdblReverseDUTOutValueArray3 = value
    End Set
  End Property

  Public Property ReverseDUTOutValueArray4() As Double()
    Get
      Return mdblReverseDUTOutValueArray4
    End Get
    Set(ByVal value As Double())
      mdblReverseDUTOutValueArray4 = value
    End Set
  End Property

  Public Property ReverseDUTVrefValueArray() As Double()
    Get
      Return mdblReverseDUTVrefValueArray
    End Get
    Set(ByVal value As Double())
      mdblReverseDUTVrefValueArray = value
    End Set
  End Property

  Public Property ReverseDUTVrefValueArray2() As Double()
    Get
      Return mdblReverseDUTVrefValueArray2
    End Get
    Set(ByVal value As Double())
      mdblReverseDUTVrefValueArray2 = value
    End Set
  End Property

  Public Property ReverseEncoderPositionArray() As Double()
    Get
      Return mdblReverseEncoderPositionArray
    End Get
    Set(ByVal value As Double())
      mdblReverseEncoderPositionArray = value
    End Set
  End Property

  Public Property ReverseEncoderPositionArray2() As Double()
    Get
      Return mdblReverseEncoderPositionArray2
    End Get
    Set(ByVal value As Double())
      mdblReverseEncoderPositionArray2 = value
    End Set
  End Property

  Public Property ReverseEncoderTimeArray() As Double()
    Get
      Return mdblReverseEncoderTimeArray
    End Get
    Set(ByVal value As Double())
      mdblReverseEncoderTimeArray = value
    End Set
  End Property

  Public Property ReverseEncoderTimeArray2() As Double()
    Get
      Return mdblReverseEncoderTimeArray2
    End Get
    Set(ByVal value As Double())
      mdblReverseEncoderTimeArray2 = value
    End Set
  End Property

  Public Property ReverseEvaluatedTorqueArray() As Double()
    Get
      Return mReverseEvaluatedTorqueArray
    End Get
    Set(ByVal value As Double())
      mReverseEvaluatedTorqueArray = value
    End Set
  End Property

  Public Property ReverseForceValueArray() As Double()
    Get
      Return Me.mdblReverseForceValueArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReverseForceValueArray = value
    End Set
  End Property

  Public Property ReverseForceVoltageArray() As Double()
    Get
      Return Me.mdblReverseForceVoltageArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReverseForceVoltageArray = value
    End Set
  End Property

  'Public Property ReversePercentVrefArray() As Double()
  '  Get
  '    Return mdblReversePercentVrefArray
  '  End Get
  '  Set(ByVal value As Double())
  '    mdblReversePercentVrefArray = value
  '  End Set
  'End Property

  Public Property ReversePercentVrefArray2() As Double()
    Get
      Return mdblReversePercentVrefArray2
    End Get
    Set(ByVal value As Double())
      mdblReversePercentVrefArray2 = value
    End Set
  End Property

  Public Property ReversePositionArray() As Double() '1.0.1.1DLG
    Get
      Return mReversePositionArray
    End Get
    Set(ByVal value As Double())
      mReversePositionArray = value
    End Set
  End Property

  Public Property ReversePositionBasedForceArray() As Double()
    Get
      Return Me.mdblReversePositionBasedForceArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReversePositionBasedForceArray = value
    End Set
  End Property

  'Public Property ReversePositionBasedPercentVrefArray() As Double()
  '  Get
  '    Return mdblReversePositionBasedPercentVrefArray
  '  End Get
  '  Set(ByVal value As Double())
  '    mdblReversePositionBasedPercentVrefArray = value
  '  End Set
  'End Property

  Public Property ReversePositionBasedPercentVrefArray2() As Double()
    Get
      Return mdblReversePositionBasedPercentVrefArray2
    End Get
    Set(ByVal value As Double())
      mdblReversePositionBasedPercentVrefArray2 = value
    End Set
  End Property

  Public Property ReverseRepeatabilityValueArray() As Double()
    Get
      Return Me.mdblReverseRepeatabilityValueArray
    End Get
    Set(ByVal value As Double())
      Me.mdblReverseRepeatabilityValueArray = value
    End Set
  End Property

  Public Property ReverseSinglePointLinearityArray() As Double() '1.0.1.1DLG
    Get
      Return mReverseSinglePointLinearityArray
    End Get
    Set(ByVal value As Double())
      mReverseSinglePointLinearityArray = value
    End Set
  End Property

  Public Property ReverseTorqueValueArray() As Double()
    Get
      Return mReverseTorqueValueArray
    End Get
    Set(ByVal value As Double())
      mReverseTorqueValueArray = value
    End Set
  End Property

  Public Property ReverseVoltageTimeArray() As Double()
    Get
      Return mdblReverseVoltageTimeArray
    End Get
    Set(ByVal value As Double())
      mdblReverseVoltageTimeArray = value
    End Set
  End Property

  Public Property ReverseVoltageTimeArray2() As Double()
    Get
      Return mdblReverseVoltageTimeArray2
    End Get
    Set(ByVal value As Double())
      mdblReverseVoltageTimeArray2 = value
    End Set
  End Property


  Public Property SinglePointLinearityArray() As Double()
    Get
      Return Me.mSinglePointLinearityArray
    End Get
    Set(ByVal value As Double())
      mSinglePointLinearityArray = value
    End Set
  End Property

  Public Property SinglePointLinearityDutOut1Array() As Double()
    Get
      Return Me.mdblSinglePointLinearityDutOut1Array
    End Get
    Set(ByVal value As Double())
      Me.mdblSinglePointLinearityDutOut1Array = value
    End Set
  End Property

  Public Property SinglePointLinearityDutOut2Array() As Double()
    Get
      Return Me.mdblSinglePointLinearityDutOut2Array
    End Get
    Set(ByVal value As Double())
      Me.mdblSinglePointLinearityDutOut2Array = value
    End Set
  End Property

  Public Property SinglePointPositionArray() As Double() '1.0.1.1DLG
    Get
      Return mSinglePointPositionArray
    End Get
    Set(ByVal value As Double())
      mSinglePointPositionArray = value
    End Set
  End Property

  Public Property SlopeDeviationArray() As Double()
    Get
      Return Me.mSlopeDeviationArray
    End Get
    Set(ByVal value As Double())
      mSlopeDeviationArray = value
    End Set
  End Property


  Public Property TorqueXPositionArray() As Double()
    Get
      Return mTorqueXPositionArray
    End Get
    Set(ByVal value As Double())
      mTorqueXPositionArray = value
    End Set
  End Property

  Public Property TwoPointLinearityArray() As Double()
    Get
      Return mTwoPointLinearityArray
    End Get
    Set(ByVal value As Double())
      mTwoPointLinearityArray = value
    End Set
  End Property

  Public Property TwoPointPositionArray() As Double() '1.0.1.1DLG
    Get
      Return mTwoPointPositionArray
    End Get
    Set(ByVal value As Double())
      mTwoPointPositionArray = value
    End Set
  End Property


  Public Property XPositionArray() As Double()
    Get
      Return mdblXPositionArray
    End Get
    Set(ByVal value As Double())
      mdblXPositionArray = value
    End Set
  End Property

  Public Property XPositionArray2() As Double()
    Get
      Return mdblXPositionArray2
    End Get
    Set(ByVal value As Double())
      mdblXPositionArray2 = value
    End Set
  End Property


#End Region
End Class

