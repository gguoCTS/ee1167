''' <summary>
''' 6th Gen Test Station Operation Program (TSOP)
''' 
''' Team Members:
''' Daryl Davis, Dan Gregory, Andy Mehl, Tim Rowlison
''' 
''' CTS Automotive Products
''' 1142 West Beardsley Ave
''' Elkhart IN, 46514
''' 
''' Initial Team Development started May of 2012.
''' 
''' This program is intended to be used across all products and test statons.
''' The core/base code will be generic amd reusable. Code specific to particular 
''' products and test stations will be encapsulated in separate classes or modules
''' and used as needed per particular product and station implementations.
''' 
''' 
''' 
''' 
''' ID           By   Ver      Date        Purpose/Description
'''              TER  1.0.0.0      
'''              TER  2.0.0.0  04/19/2013  Released for 586 Czech
''' 2.0.1.0 TER  TER  2.0.1.0  07/18/2013  SCN 5190
'''                                        , Updates for 514 Elkhart Test Lab Digit-1
'''                                        , and Pedal Arm Return Testing (FMVSS)
''' 3.0.0.0 TER  TER  3.0.0.0  09/23/2013  Add ENR Capability
'''                                        , Default Tab Page capability  
''' 3.1.0.0 TER  TER  3.1.0.0  11/11/2013  Add drop down for FMVSS stroke length
'''                                        
''' 3.1.0.1 TER  TER  3.1.0.1  11/11/2013  Refactor Print Page routine for reuse and optimization
'''                                        , Add Graph Axes Definitions
''' 
''' 3.1.0.2 TER  TER  3.1.0.2  03/27/2014  Updates to test 526
'''                                        , code to modify MPC Abscissa with measured value like KneeHighLocation
'''                                        , product specific app initialization
'''                                        , Dynamic menus and menu definition file
'''  
''' 
''' 
'''  
''' 
''' 
''' 
''' </summary>
''' <remarks></remarks>
Module modAppDocumentation




  '
  '
  ' Design intent is for this code base to be used across all products
  ' and test stations
  ' 
  ' This program makes use of Object Oriented Programming techniques
  ' Application level code
  '
  '   This program consists of the following components:  form module(s),
  ' standard module(s) and user control(s).  A form module contains
  ' event procedures (i.e. sections of code that will execute in response to
  ' specific events).  Forms can contain any number of controls or graphical
  ' data for display.  All the forms for this program are contained in the
  ' Forms folder.  A standard module contains only code.  Standard modules
  ' were previously called code modules.  All the standard modules for this
  ' program are contained in the Modules folder.  The user controls are
  ' components built for use on the GUI.
  '
  '   The subroutines found in this program are designed to be modular and
  ' reuseable code segments.  The intent here is to reduce design time for
  ' future programs.
  '  
  '   To make the code more readable, please follow the convention of
  ' making variable names all lower case (except for multiple word names
  ' like datumZero) and constant names (anything assigned with the Const
  ' keyword) all upper case.  It's a big help to be able to tell the
  ' difference between constants and variables and this is a widely used
  ' convention.
  '
  '   In addition to making variable names lower case, please follow the
  ' naming convention for scope and type identification.  This convention
  ' is taken from Microsoft's recommended standard for naming variables
  ' in VB.  The first letter should identify the scope of the variable.
  ' Use "g" for global, "m" for module level or "l" for local. The next
  ' three letters identify the type of the variable. Examples of this are
  ' "bln" for boolean, "int" for integer, and "sng" for single. Follow this
  ' with the name of the variable.  For instance, a global integer used to
  ' detect failures could be named: gintAnomaly.
  '
  '   Also, please help us track this program through its useful life by
  ' changing the version number of the program and placing a brief description
  ' of your modification here.  The version number of the program should
  ' have a digit and a decimal value (X.X).  The "ID" is a stamp that you
  ' should keep close to if not on same lines of any and all modifications
  ' you make. It should appear exactly as typed in the table so others can
  ' find all your modifications by clicking Edit on the menu bar and using
  ' the Find function.  The format that is being used for the ID is the version
  ' number and initials.  A hypothetical example of an ID is "1.0ANM".  Within
  ' the version controlled modules, follow the version control numbering system
  ' used there.
  '

  'Region templates for new classes
#Region "Delegates =================================================="

#End Region

#Region "Events =================================================="

#End Region

#Region "Constructors =================================================="

#End Region

#Region "Private Member Variables =================================================="

#End Region

#Region "Public Constants, Structures, Enums =================================================="

#End Region

#Region "Properties =================================================="

#End Region

#Region "Public Methods =================================================="

#End Region

#Region "Private Methods =================================================="

#End Region


  '==================================================
  'Error Trap for higest app level with display, logging, and app exit
  'Catch ex As TsopAnomalyException
  '  gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Call LogAnomaly() 'Log anomaly also displays anomaly
  '  Environment.Exit(0)
  'Catch ex As Exception
  '  gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  Call LogAnomaly() 'Log anomaly also displays anomaly
  '  Environment.Exit(0)
  'End Try

  '==================================================
  'Error Trap for intermediate Level, No logging
  'Catch ex As TsopAnomalyException
  '  gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  'Catch ex As Exception
  '  gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  'End Try

  '==================================================
  'Error Trap for class modules
  'Catch ex As TsopAnomalyException
  '  mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
  'Catch ex As Exception
  '  mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
  'End Try

  '==================================================
  'Error Trap for Database class module
  'mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

  '==================================================
  'Passing anomaly from component up to application
  'If gServoMotor.Anomaly IsNot Nothing Then
  '  gAnomaly = New clsDbAnomaly(gServoMotor.Anomaly, gDatabase)
  '  gServoMotor.Anomaly = Nothing
  '  Throw New TsopAnomalyException
  'End If

  '==================================================
  'Invokine module methods with reflection
  'Dim argArray(0) As Object
  'Dim mfType As Type = GetType(modAppDataAcquisition)
  'Dim mfMethod As MethodInfo

  'argArray(0) = CType(5.0, Double)
  'mfMethod = mfType.GetMethod("ConfigureSWVrefControl")

  'mfMethod.Invoke(Nothing, Nothing)
  'mfMethod.Invoke(Nothing, argArray)




End Module
