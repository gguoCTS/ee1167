Imports System.Reflection

Public Class DeviceInProcess
#Region "Property Variables"
  Private mstrActuatorVariant As String
  Private mAnomaly As clsDbAnomaly
  Private mAttributesTable As New DataTable

  Private mstrBOM As String

  Private mstrClassConfigFilePath As String

  Private mDatabase As Database
  Private mstrDateCode As String

  Private mEncodedSerialNumber As String
  Private mExternalSerialNumber As String

  Private mInvalidData As Boolean

  Private mPallet As String

  Private mstrSoftwareVersionID As String
  Private mStackTrace As New clsStackTrace

  Private mintTestStatusBitsThisTestPosition As Integer

#End Region

#Region "Properties"
  Public Property ActuatorVariant As String
    Get
      Return mstrActuatorVariant
    End Get
    Set(value As String)
      mstrActuatorVariant = value
    End Set
  End Property

  Public Property Anomaly() As clsDbAnomaly
    Get
      Return Me.mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      Me.mAnomaly = value
    End Set
  End Property

  Public Property AttributesTable() As DataTable
    Get
      Return Me.mAttributesTable
    End Get
    Set(ByVal value As DataTable)
      Me.mAttributesTable = value
    End Set
  End Property

  Public Property BOM As String
    Get
      Return mstrBOM
    End Get
    Set(value As String)
      mstrBOM = value
    End Set
  End Property

  Public Property DateCode() As String
    Get
      Return mstrDateCode
    End Get
    Set(ByVal value As String)
      mstrDateCode = value
    End Set
  End Property

  Public Property EncodedSerialNumber() As String
    Get
      Return mEncodedSerialNumber
    End Get
    Set(ByVal value As String)
      mEncodedSerialNumber = value
    End Set
  End Property

  Public Property ExternalSerialNumber() As String
    Get
      Return mExternalSerialNumber
    End Get
    Set(ByVal value As String)
      mExternalSerialNumber = value
    End Set
  End Property

  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

  Public Property SoftwareVersionID() As String
    Get
      Return mstrSoftwareVersionID
    End Get
    Set(ByVal value As String)
      mstrSoftwareVersionID = value
    End Set
  End Property

  Public Property TestStatusBitsThisTestPosition() As Integer
    Get
      Return Me.mintTestStatusBitsThisTestPosition
    End Get
    Set(ByVal value As Integer)
      Me.mintTestStatusBitsThisTestPosition = value
    End Set
  End Property


#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)
    Try
      mDatabase = db

      Me.mAttributesTable.Columns.Add("AttributeName", GetType(String))
      Me.mAttributesTable.Columns.Add("AttributeValue", GetType(String))
      Me.mAttributesTable.Columns.Add("IsIdentifier", GetType(Boolean))

      Dim strClassName As String
      Dim classType As Type
      Dim dbTable As DataTable
      Dim classProperty As PropertyInfo
      Dim strPropDataType As String
      Dim strPropValue As String
      Dim row As DataRow
      Me.mDatabase = db

      strClassName = Me.GetType.Name
      classType = GetType(DeviceInProcess)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)
        If classProperty Is Nothing Then
          Me.InvalidData = True
          '\/\/ 'TER6g
          mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Property '" & row.Item("PropertyName").ToString & "' Doesn't Exist in Class")
          Throw New TsopAnomalyException
          '/\/\ 'TER6g
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          'Select Case strPropDataType
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, mDatabase)
    End Try
  End Sub
#End Region

#Region "Public Methods =================================================="
  'Public Sub SetDIPAttributes()
  '  Dim row As DataRow

  '  Try
  '    row = Me.mAttributesTable.NewRow
  '    row("AttributeName") = "ExternalSerialNumber"
  '    row("AttributeValue") = Me.mExternalSerialNumber
  '    row("IsIdentifier") = 1
  '    Me.mAttributesTable.Rows.Add(row)

  '  Catch ex As TsopAnomalyException
  '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, mDatabase)
  '  End Try
  'End Sub

  Public Sub GetDipID()
    Dim dt As New DataTable
    Dim ds As New DataSet("AttributeSet")
    Dim sXml As String

    Try
      dt = Me.AttributesTable.Copy
      If dt.Rows.Count <> 0 Then
        ds.Tables.Add(dt)
        ds.Tables(0).TableName = "Attribute"
        sXml = ds.GetXml
        'Debug.Print(sXml)
      Else
        sXml = ""
      End If

      mDatabase.GetDeviceInProcessID(sXml)
      If mDatabase.Anomaly IsNot Nothing Then
        Me.Anomaly = mDatabase.Anomaly
        mDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, mDatabase)
    End Try
  End Sub


#End Region

End Class
