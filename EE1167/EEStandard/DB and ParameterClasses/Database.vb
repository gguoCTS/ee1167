#Region "Documentation"
'*********  Database Class *********
'
'Provides data abstraction layer between the main Test Station Operation Program (TSOP)
'and the database. Originally written for 6th Gen TSOP.
'
'Revision History:
'Date       By      Purpose of modification                                 ID
'10/3/2008  TER     Initial Release                                         None
'10/6/2008  TER     Added exception handling in all methods                 None
'10/23/2008 TER     Added boolean to constructor for using server           TER_10/23/08
'                   database. 
'11/20/2008 TER     Modified exception handling to use Anomaly class        None
'05/06/2009 TER     Added sortIndex parameter to                            TER_05/06/09
'                     'InsertExternalTestMethod' method
'                   Added member variable and property for 
'                     ExternalTestMethodID
'                   Added member variable and property for 
'                     ExternalTestMethodRelationshipID
'
'
'
#End Region

#Region "Imports"
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Text
#End Region

Public Class Database

  Private Const TIMEOUT As Integer = 90

#Region "Private Member Variables"
  Private mAnomaly As clsDbAnomaly
  Private mADRConnection As SqlConnection

  Private mADRStructureID As Guid
  Private mBIConnection As SqlConnection
  Private mConnection As SqlConnection
  Private mServerConnection As SqlConnection
  Private mProcessPCConnection As SqlConnection
  Private mDbError As Boolean = False
  Private mDbErrorMessage As String
  Private mDbErrorProcedure As String
  Private mDeviceInProcessID As Guid
  Private mMasterProcessParameterID As Guid
  Private mLotID As Guid
  Private mLotTypeID As Guid
  Private mProcessParameterID As Guid
  Private mProductID As Guid
  Private mProgDateTime As Date
  Private mProgrammingID As Guid
  Private mRTYConnection As SqlConnection
  Private mStationID As Guid
  Private mSubProcessID As Guid
  Private mTestDateTime As Date
  Private mTestID As Guid
  Private mTsopID As Guid
  Private mTsopModeID As Guid
  Private mTsopStartupID As Guid
  Private mExternalTestMethodID As Guid 'TER_05/06/09
  Private mExternalTestMethodRelationshipID As Guid 'TER_05/06/09
  Private mStackTrace As New clsStackTrace

#End Region

#Region "Private Methods"
  Private Sub GetStationID()

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim stationName As String
      Dim param1 As New SqlParameter
      Dim param2 As New SqlParameter
      stationName = ConfigurationManager.AppSettings("StationName")
      dbCommand = New SqlCommand("pspTsop6gGetStationID", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      param1.ParameterName = "@StationName"
      param1.Value = stationName
      dbCommand.Parameters.Add(param1)

      param2.ParameterName = "@StationID"
      param2.DbType = DbType.Guid
      param2.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(param2)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      If Not dbCommand.Parameters.Item("@StationID").Value.Equals(DBNull.Value) Then
        Me.mStationID = dbCommand.Parameters.Item("@StationID").Value
      Else
        Me.mDbError = True
        Me.mDbErrorProcedure = "Database: GetStationID"
        Me.mDbErrorMessage = "Invalid Station Name"
      End If

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = "Database: GetStationID"
      Me.mDbErrorMessage = ex.Message
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = "Database: GetStationID"
      Me.mDbErrorMessage = ex.Message

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub

  Private Sub GetTsopID()

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim tsopName As String
      Dim param1 As New SqlParameter
      Dim param2 As New SqlParameter

      tsopName = ConfigurationManager.AppSettings("TsopName")
      dbCommand = New SqlCommand("pspTsop6gGetTsopID", Connection)
      dbCommand.CommandType = CommandType.StoredProcedure

      param1.ParameterName = "@TsopName"
      param1.Value = tsopName
      dbCommand.Parameters.Add(param1)

      param2.ParameterName = "@TsopID"
      param2.DbType = DbType.Guid
      param2.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(param2)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      If Not dbCommand.Parameters.Item("@TsopID").Value.Equals(DBNull.Value) Then
        Me.TsopID = dbCommand.Parameters.Item("@TsopID").Value
      Else
        Me.mDbError = True
        Me.mDbErrorProcedure = "Database: GetTsopID"
        Me.mDbErrorMessage = "Invalid TSOP Name"
      End If

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = "Database: GetTsopID"
      Me.mDbErrorMessage = ex.Message
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = "Database: GetTsopID"
      Me.mDbErrorMessage = ex.Message

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Private Sub Initialize()
    Try
      If Not Connection Is Nothing Then
        GetStationID()
        If Not (mAnomaly Is Nothing) Then
          Throw New TsopAnomalyException
        End If
        GetTsopID()
        If Not (mAnomaly Is Nothing) Then
          Throw New TsopAnomalyException
        End If
      Else
        Me.mDbError = True
        Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
        Me.mDbErrorMessage = "Database connection could not be established"
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As SqlException
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    Catch ex As Exception
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    End Try
  End Sub

#End Region

#Region "Constructors"
  Public Sub New(ByVal useServer As Boolean, ByVal usesAdrDatabase As Boolean)
    Dim strConnectionString As String
    Dim strAdrConnectionString As String
    'Dim strBIConnectionString As String
    Dim strRTYConnectionString As String
    'Dim strProcessPCConnectionString As String
    Dim strServerConnectionString As String

    Try
      strConnectionString = ""
      strAdrConnectionString = ""

      If useServer Then
        strConnectionString = ConfigurationManager.ConnectionStrings("dbServerConnectionString").ConnectionString
      Else
        strConnectionString = ConfigurationManager.ConnectionStrings("dbConnectionString").ConnectionString
      End If

      Connection = New SqlConnection(strConnectionString)

      If usesAdrDatabase Then
        If useServer Then
          strAdrConnectionString = ConfigurationManager.ConnectionStrings("dbServerADRConnectionString").ConnectionString
        Else
          strAdrConnectionString = ConfigurationManager.ConnectionStrings("dbADRConnectionString").ConnectionString
        End If
        Me.mADRConnection = New SqlConnection(strAdrConnectionString)
      End If

      'strBIConnectionString = ConfigurationManager.ConnectionStrings("dbLocalBurnInConnectionString").ConnectionString
      'BIConnection = New SqlConnection(strBIConnectionString)

      strRTYConnectionString = ConfigurationManager.ConnectionStrings("dbRTYConnectionString").ConnectionString
      RTYConnection = New SqlConnection(strRTYConnectionString)

      'strProcessPCConnectionString = ConfigurationManager.ConnectionStrings("dbProcessPCConnectionString").ConnectionString
      'ProcessPCConnection = New SqlConnection(strProcessPCConnectionString)

      strServerConnectionString = ConfigurationManager.ConnectionStrings("dbServerConnectionString").ConnectionString
      ServerConnection = New SqlConnection(strServerConnectionString)


      Call Initialize()

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As SqlException
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    End Try

  End Sub



  Public Sub New(ByVal useServer As Boolean)

    Try
      Dim connectionString As String
      If useServer Then '10/23/08_TER
        connectionString = ConfigurationManager.ConnectionStrings("dbServerConnectionString").ConnectionString
      Else
        connectionString = ConfigurationManager.ConnectionStrings("dbConnectionString").ConnectionString
      End If

      Connection = New SqlConnection(connectionString)

      Call Initialize()

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As SqlException
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      mAnomaly.AnomalyDbError = True
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    End Try

  End Sub

  Public Sub New()
    Try
      Dim connectionString As String

      connectionString = ConfigurationManager.ConnectionStrings("dbConnectionString").ConnectionString

      Connection = New SqlConnection(connectionString)

      Call Initialize()

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As SqlException
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    Catch ex As Exception
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    End Try

  End Sub

#End Region

#Region "Public Properties"
  Public Property Anomaly() As clsDbAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      mAnomaly = value
    End Set
  End Property

  Public Property ADRStructureID() As Guid
    Get
      Return Me.mADRStructureID
    End Get
    Set(ByVal value As Guid)
      Me.mADRStructureID = value
    End Set
  End Property

  Public Property BIConnection() As SqlConnection
    Get
      Return mBIConnection
    End Get
    Set(ByVal value As SqlConnection)
      mBIConnection = value
    End Set
  End Property


  Public Property Connection() As SqlConnection
    Get
      Return mConnection
    End Get
    Set(ByVal value As SqlConnection)
      mConnection = value
    End Set
  End Property




  Public Property ProcessPCConnection() As SqlConnection
    Get
      Return mProcessPCConnection
    End Get
    Set(ByVal value As SqlConnection)
      mProcessPCConnection = value
    End Set
  End Property
  Public Property ServerConnection() As SqlConnection
    Get
      Return mServerConnection
    End Get
    Set(ByVal value As SqlConnection)
      mServerConnection = value
    End Set
  End Property
  Public Property DbError() As Boolean
    Get
      Return Me.mDbError
    End Get
    Set(ByVal value As Boolean)
      Me.mDbError = value
    End Set
  End Property

  Public Property DbErrorMessage() As String
    Get
      Return Me.mDbErrorMessage
    End Get
    Set(ByVal value As String)
      Me.mDbErrorMessage = value
    End Set
  End Property

  Public Property DbErrorProcedure() As String
    Get
      Return Me.mDbErrorProcedure
    End Get
    Set(ByVal value As String)
      Me.mDbErrorProcedure = value
    End Set
  End Property

  Public Property DeviceInProcessID() As Guid
    Get
      Return Me.mDeviceInProcessID
    End Get
    Set(ByVal value As Guid)
      Me.mDeviceInProcessID = value
    End Set
  End Property

  Public Property ExternalTestMethodID() As Guid 'TER_05/06/09
    Get
      Return Me.mExternalTestMethodID
    End Get
    Set(ByVal value As Guid)
      Me.mExternalTestMethodID = value
    End Set
  End Property

  Public Property ExternalTestMethodRelationshipID() As Guid 'TER_05/06/09
    Get
      Return Me.mExternalTestMethodRelationshipID
    End Get
    Set(ByVal value As Guid)
      Me.mExternalTestMethodRelationshipID = value
    End Set
  End Property

  Public Property MasterProcessParameterID() As Guid
    Get
      Return Me.mMasterProcessParameterID
    End Get
    Set(ByVal value As Guid)
      Me.mMasterProcessParameterID = value
    End Set
  End Property


  Public Property LotID() As Guid
    Get
      Return Me.mLotID
    End Get
    Set(ByVal value As Guid)
      Me.mLotID = value
    End Set
  End Property

  Public Property LotTypeID() As Guid
    Get
      Return Me.mLotTypeID
    End Get
    Set(ByVal value As Guid)
      Me.mLotTypeID = value
    End Set
  End Property

  Public Property ProcessParameterID() As Guid
    Get
      Return Me.mProcessParameterID
    End Get
    Set(ByVal value As Guid)
      Me.mProcessParameterID = value
    End Set
  End Property

  Public Property ProductID() As Guid
    Get
      Return Me.mProductID
    End Get
    Set(ByVal value As Guid)
      Me.mProductID = value
    End Set
  End Property

  Public Property ProgDateTime() As Date
    Get
      Return Me.mProgDateTime
    End Get
    Set(ByVal value As Date)
      Me.mProgDateTime = value
    End Set
  End Property

  Public Property ProgrammingID() As Guid
    Get
      Return Me.mProgrammingID
    End Get
    Set(ByVal value As Guid)
      Me.mProgrammingID = value
    End Set
  End Property

  Public Property RTYConnection() As SqlConnection
    Get
      Return mRTYConnection
    End Get
    Set(ByVal value As SqlConnection)
      mRTYConnection = value
    End Set
  End Property


  Public Property StationID() As Guid
    Get
      Return mStationID
    End Get
    Set(ByVal value As Guid)
      mStationID = value
    End Set
  End Property

  Public Property SubProcessID() As Guid
    Get
      Return Me.mSubProcessID
    End Get
    Set(ByVal value As Guid)
      Me.mSubProcessID = value
    End Set
  End Property

  Public Property TestDateTime() As Date
    Get
      Return Me.mTestDateTime
    End Get
    Set(ByVal value As Date)
      Me.mTestDateTime = value
    End Set
  End Property

  Public Property TestID() As Guid
    Get
      Return Me.mTestID
    End Get
    Set(ByVal value As Guid)
      Me.mTestID = value
    End Set
  End Property

  Public Property TsopID() As Guid
    Get
      Return mTsopID
    End Get
    Set(ByVal value As Guid)
      mTsopID = value
    End Set
  End Property

  Public Property TsopModeID() As Guid
    Get
      Return Me.mTsopModeID
    End Get
    Set(ByVal value As Guid)
      Me.mTsopModeID = value
    End Set
  End Property

  Public Property TsopStartupID() As Guid
    Get
      Return Me.mTsopStartupID
    End Get
    Set(ByVal value As Guid)
      Me.mTsopStartupID = value
    End Set
  End Property
#End Region

#Region "Public Methods"

  Public Function GetAlphaMetricMPCs(ByVal FunctionMetricID As Guid) As DataTable

    ' EXECUTE @RC = [US47_AutoProductMfgTest].[dbo].[pspPMA_GetAlphaMPC_Values] 
    ' @processParameterID
    ',@functionMetricID
    ',@mpcValueID OUTPUT


    Dim dbCommand As SqlCommand = Nothing
    Dim stationName As String = ""
    Dim dbDataTable As New DataTable
    Dim dbAdpt As SqlDataAdapter

    Try

      dbCommand = New SqlCommand("pspPMA_GetAlphaMPC_Values", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@processParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@functionMetricID", FunctionMetricID))
      dbCommand.Parameters.Add(New SqlParameter("@mpcValueID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)

      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function


  Public Function GetAnomalyActionsDataTable(ByVal anomalyDefinitionID As Guid) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetAnomalyTsopActions", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.mTsopID))
      dbCommand.Parameters.Add(New SqlParameter("@AnomalyDefinitionID", anomalyDefinitionID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)

      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try

  End Function

  Public Function GetAnomalyInfo(ByVal anomalyNumber As Int32) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetAnomalyInfo", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@AnomalyNumber", anomalyNumber))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)

      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Sub GetAcquiredData(ByVal TestID As Guid, ByRef byteArray() As Byte, ByVal intMeasurementNumber As Integer)
    Dim dbCommand As SqlCommand = Nothing
    Dim param1 As New SqlParameter
    Dim param2 As New SqlParameter
    Dim AcquiredDataRecordID As Guid
    Try

      dbCommand = New SqlCommand("pspAdr_GetAdrForTestMeasurement", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TestID", TestID))
      dbCommand.Parameters.Add(New SqlParameter("@MeasurementName", intMeasurementNumber))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      AcquiredDataRecordID = dbCommand.ExecuteScalar

      dbCommand = New SqlCommand("pspAdr_GetAcquiredData", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@AcquiredDataRecordID", AcquiredDataRecordID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      byteArray = dbCommand.ExecuteScalar


    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub

  Public Function GetAdrChannelMeasurementsDataTable(ByVal strChannelName As String) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspAdrChannelMeasurements", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@AdrStructureID", Me.ADRStructureID))
      dbCommand.Parameters.Add(New SqlParameter("@AdrChannelName", strChannelName))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetAdrConversionMethodsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gAdrGetConversionMethodArgs", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@AdrStructureID", Me.ADRStructureID))
      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetAdrConversionMethodsDataTable(ByVal guidAdrStructureID As Guid) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gAdrGetConversionMethodArgs", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@AdrStructureID", guidAdrStructureID))
      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Sub GetAdrStructureID(ByVal strAdrStructureVersion As String)
    Dim dbCommand As SqlCommand = Nothing
    Try

      dbCommand = New SqlCommand("pspADR_GetADR_StructureID", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))
      dbCommand.Parameters.Add(New SqlParameter("@Version", strAdrStructureVersion))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      Me.mADRStructureID = dbCommand.ExecuteScalar

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub

  Public Sub GetAdrStructureID(ByVal strAdrStructureVersion As String, ByRef guidAdrStructureID As Guid)
    Dim dbCommand As SqlCommand = Nothing
    Try

      dbCommand = New SqlCommand("pspADR_GetADR_StructureID", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))
      dbCommand.Parameters.Add(New SqlParameter("@Version", strAdrStructureVersion))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      guidAdrStructureID = dbCommand.ExecuteScalar

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub

  Public Function GetClassPropertyValues(ByVal strClassName As String) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter
      Dim param1 As New SqlParameter
      Dim param2 As New SqlParameter
      Dim classProperties As New ArrayList

      dbCommand = New SqlCommand("pspTsop6gGetClassPropertyNameValues", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      param1.ParameterName = "@ProcessParameterID"
      param1.Value = Me.ProcessParameterID
      dbCommand.Parameters.Add(param1)

      param2.ParameterName = "@ClassName"
      param2.Value = strClassName
      dbCommand.Parameters.Add(param2)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Sub GetDeviceInProcessID(ByVal strXmlString As String)
    Dim dbCommand As SqlCommand = Nothing

    Try
      Dim paramOut As New SqlParameter

      dbCommand = New SqlCommand("pspTsop6gGetDipID", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))

      paramOut.ParameterName = "@DeviceInProcessID"
      paramOut.DbType = DbType.Guid
      paramOut.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(paramOut)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      Me.mDeviceInProcessID = dbCommand.Parameters.Item("@DeviceInProcessID").Value
    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try

  End Sub

  Public Function GetDipAttributeValue(ByVal DipAttributeName As String) As String
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim paramOut As New SqlParameter

      dbCommand = New SqlCommand("pspTsop6gGetDipAttributeValue", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@DeviceInProcessID", Me.DeviceInProcessID))
      dbCommand.Parameters.Add(New SqlParameter("@AttributeName", DipAttributeName))

      paramOut.ParameterName = "@AttributeValue"
      paramOut.DbType = DbType.String
      paramOut.Size = 50
      paramOut.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(paramOut)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)
      Return dbCommand.Parameters.Item("@AttributeValue").Value

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  'Public Function GetDisplayGridColumnProperties(ByVal guidGridID As Guid) As DataTable
  '  Dim dbCommand As SqlCommand = Nothing
  '  Try
  '    Dim dbDataTable As New DataTable
  '    Dim dbAdpt As SqlDataAdapter

  '    dbCommand = New SqlCommand("pspTsop6gGetDisplayGridColProperties", Connection)
  '    dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
  '    dbCommand.CommandType = CommandType.StoredProcedure

  '    dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
  '    dbCommand.Parameters.Add(New SqlParameter("@DisplayGridID", guidGridID))

  '    If dbCommand.Connection.State <> ConnectionState.Open Then
  '      dbCommand.Connection.Open()
  '    End If

  '    dbAdpt = New SqlDataAdapter(dbCommand)
  '    dbAdpt.Fill(dbDataTable)
  '    Return dbDataTable

  '  Catch ex As SqlException
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, "Database", "GetDisplayGridColumnProperties", ex.Message, Me)
  '    Return Nothing
  '  Catch ex As Exception
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, "Database", "GetDisplayGridColumnProperties", ex.Message, Me)
  '    Return Nothing

  '  Finally
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '  End Try
  'End Function

  Public Function GetDisplayGridsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetDisplayGridList", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  'Public Function GetDisplayGridRowProperties(ByVal guidGridID As Guid) As DataTable

  '  Dim dbCommand As SqlCommand = Nothing
  '  Try
  '    Dim dbDataTable As New DataTable
  '    Dim dbAdpt As SqlDataAdapter

  '    dbCommand = New SqlCommand("pspTsop6gGetDisplayGridRowProperties", Connection)
  '    dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
  '    dbCommand.CommandType = CommandType.StoredProcedure

  '    dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
  '    dbCommand.Parameters.Add(New SqlParameter("@DisplayGridID", guidGridID))

  '    If dbCommand.Connection.State <> ConnectionState.Open Then
  '      dbCommand.Connection.Open()
  '    End If

  '    dbAdpt = New SqlDataAdapter(dbCommand)
  '    dbAdpt.Fill(dbDataTable)
  '    Return dbDataTable

  '  Catch ex As SqlException
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, "Database", "GetDisplayGridRowProperties", ex.Message, Me)
  '    Return Nothing
  '  Catch ex As Exception
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, "Database", "GetDisplayGridRowProperties", ex.Message, Me)
  '    Return Nothing

  '  Finally
  '    If Not dbCommand Is Nothing Then
  '      If dbCommand.Connection.State <> ConnectionState.Closed Then
  '        dbCommand.Connection.Close()
  '      End If
  '    End If

  '  End Try
  'End Function

  Public Function GetEnabledLotTypesDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetEnabledLotTypes", Connection)
      dbCommand.CommandType = CommandType.StoredProcedure

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function


  Public Function GetExternalTestMethodsCollection() As Dictionary(Of Guid, String)
    'Dim dbCommand As SqlCommand = Nothing
    'Try
    '  Dim dbDataReader As SqlDataReader
    '  Dim dict As New Dictionary(Of Guid, String)

    '  dbCommand = New SqlCommand("pspTsop6gGetExternalTestMethodList", Connection)
    '  dbCommand.CommandType = CommandType.StoredProcedure

    '  dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))

    '  If dbCommand.Connection.State <> ConnectionState.Open Then
    '    dbCommand.Connection.Open()
    '  End If

    '  dbDataReader = dbCommand.ExecuteReader
    '  Do While dbDataReader.Read
    '    dict.Add(dbDataReader("ExternalTestMethodID"), dbDataReader("ExternalTestMethodName"))
    '  Loop
    '  Return dict

    'Catch ex As SqlException
    '  If dbCommand.Connection.State <> ConnectionState.Closed Then
    '    dbCommand.Connection.Close()
    '  End If
    '  mAnomaly = New clsDbAnomaly(6001, "Database", "GetExternalTestMethodsCollection", ex.Message, Me)
    '  Return Nothing
    'Catch ex As Exception
    '  If dbCommand.Connection.State <> ConnectionState.Closed Then
    '    dbCommand.Connection.Close()
    '  End If
    '  mAnomaly = New clsDbAnomaly(6001, "Database", "GetExternalTestMethodsCollection", ex.Message, Me)
    Return Nothing

    'Finally
    '  If Not dbCommand Is Nothing Then
    '    If dbCommand.Connection.State <> ConnectionState.Closed Then
    '      dbCommand.Connection.Close()
    '    End If
    '  End If

    'End Try
  End Function

  Public Function GetExternalTestMethodsDataAdapter() As SqlDataAdapter
    'Dim dbCommand As SqlCommand = Nothing
    'Try
    '  Dim da As SqlDataAdapter
    '  Dim pc As SqlParameterCollection
    '  ' Dim p As SqlParameter

    '  dbCommand = New SqlCommand("pspTsop6gExternalTestMethodsSelect", Connection)
    '  dbCommand.CommandType = CommandType.StoredProcedure

    '  dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))

    '  da = New SqlDataAdapter(dbCommand)

    '  'Insert Command
    '  da.InsertCommand = New SqlCommand("pspTsop6gExternalTestMethodsInsert", Connection)
    '  da.InsertCommand.CommandType = CommandType.StoredProcedure
    '  pc = da.InsertCommand.Parameters
    '  pc.Add("@ProductID", SqlDbType.UniqueIdentifier, 0, "ProductID")
    '  pc.Add("@ExternalTestMethodName", SqlDbType.VarChar, 255, "ExternalTestMethodName")
    '  pc.Add("@SortIndex", SqlDbType.Real, 0, "SortIndex")

    '  'Update Command
    '  'da.UpdateCommand = New SqlCommand("pspTmaDisplayRowUpdate", Connection)
    '  'da.UpdateCommand.CommandType = CommandType.StoredProcedure
    '  'pc = da.UpdateCommand.Parameters
    '  'pc.Add("@DisplayRowID", SqlDbType.UniqueIdentifier, 0, "DisplayRowID")
    '  'p = pc.Add("@ProcessParameterID", SqlDbType.UniqueIdentifier, 0, "ProcessParameterID")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@DisplayGridID", SqlDbType.UniqueIdentifier, 0, "DisplayGridID")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@RowTypeID", SqlDbType.UniqueIdentifier, 0, "RowTypeID")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@RowNumber", SqlDbType.SmallInt, 0, "RowNumber")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@RowHeader", SqlDbType.VarChar, 50, "RowHeader")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@FunctionMetricID", SqlDbType.UniqueIdentifier, 0, "FunctionMetricID")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@DomainItemTypeID", SqlDbType.UniqueIdentifier, 0, "DomainItemTypeID")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@DomainFunctionMetricID", SqlDbType.UniqueIdentifier, 0, "DomainFunctionMetricID")
    '  'p.SourceVersion = DataRowVersion.Current
    '  'p = pc.Add("@DomainParameterID", SqlDbType.UniqueIdentifier, 0, "DomainParameterID")
    '  'p.SourceVersion = DataRowVersion.Current

    '  'Delete Command
    '  'da.DeleteCommand = New SqlCommand("pspTmaDisplayRowDelete", Connection)
    '  'da.DeleteCommand.CommandType = CommandType.StoredProcedure
    '  'pc = da.DeleteCommand.Parameters
    '  'p = pc.Add("@DisplayRowID", SqlDbType.UniqueIdentifier, 0, "DisplayRowID")
    '  'p.SourceVersion = DataRowVersion.Original

    '  Return da

    'Catch ex As SqlException
    '  If dbCommand.Connection.State <> ConnectionState.Closed Then
    '    dbCommand.Connection.Close()
    '  End If
    '  mAnomaly = New clsDbAnomaly(6001, "Database", "GetExternalTestMethodsDataAdapter", ex.Message, Me)
    '  Return Nothing
    'Catch ex As Exception
    '  If dbCommand.Connection.State <> ConnectionState.Closed Then
    '    dbCommand.Connection.Close()
    '  End If
    '  mAnomaly = New clsDbAnomaly(6001, "Database", "GetExternalTestMethodsDataAdapter", ex.Message, Me)
    Return Nothing

    'Finally
    '  If Not dbCommand Is Nothing Then
    '    If dbCommand.Connection.State <> ConnectionState.Closed Then
    '      dbCommand.Connection.Close()
    '    End If
    '  End If

    'End Try
  End Function

  Public Function GetExternalTestMethodsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetExternalTestMethodList", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetFunctionMetricListDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gFunctionMetricList", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetLotListDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetLotList", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotTypeID", Me.LotTypeID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetPreviousLotTypes(ByVal ProcessParameterID As Guid, ByVal TSOP_ID As Guid) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsopPreviousLot_GetLotTypes", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@TSOP_ID", TSOP_ID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetPreviousLotNames(ByVal ProcessParameterID As Guid, ByVal TSOP_ID As Guid, ByVal LotTypeID As Guid) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsopPreviousLot_GetLotList", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotTypeID", LotTypeID))
      dbCommand.Parameters.Add(New SqlParameter("@TSOP_ID", TSOP_ID))
      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetLotMetricStatisticsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetLotMetricStatistics", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.LotID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetPreviousLotMetricStatisticsDataTable(ByVal lotID As Guid) As DataTable 'change lotid
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetLotMetricStatistics", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", lotID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetLotResultsDataTable(ByVal strSubProcessName As String) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetLotData", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.LotID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))
      dbCommand.Parameters.Add(New SqlParameter("@SubProcessName", strSubProcessName))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetPreviousLotResultsDataTable(ByVal strSubProcessName As String, ByVal lotID As Guid) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetLotData", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", lotID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))
      dbCommand.Parameters.Add(New SqlParameter("@SubProcessName", strSubProcessName))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetLotShiftDefDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetLotShiftDefinitions", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@StationID", Me.StationID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetLotTypesDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetLotTypes", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetLotNamesDataTable(ByVal strLotName As String, ByVal blnIncLotRun As Boolean) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      If blnIncLotRun Then
        dbCommand = New SqlCommand("pspTsop6gIncrementLotRun", Connection)
      Else
        dbCommand = New SqlCommand("pspTsop6gGetLotNames", Connection)
      End If
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@LotTypeID", Me.LotTypeID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))
      dbCommand.Parameters.Add(New SqlParameter("@LotName", strLotName))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetMetricsDisplayGridColumnsDataTable(ByVal guidGridID As Guid) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetMetricsDisplayGridColumns", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@DisplayGridID", guidGridID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetDisplayGridColumnsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetDisplayGridColumns", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  'Public Function GetMetricsDisplayGridsDataTable() As DataTable
  '  Dim dbCommand As SqlCommand = Nothing
  '  Try
  '    Dim dbDataTable As New DataTable
  '    Dim dbAdpt As SqlDataAdapter

  '    dbCommand = New SqlCommand("pspTsop6gGetMetricsDisplayGridList", Connection)
  '    dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
  '    dbCommand.CommandType = CommandType.StoredProcedure

  '    dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

  '    If dbCommand.Connection.State <> ConnectionState.Open Then
  '      dbCommand.Connection.Open()
  '    End If

  '    dbAdpt = New SqlDataAdapter(dbCommand)
  '    dbAdpt.Fill(dbDataTable)
  '    Return dbDataTable

  '  Catch ex As SqlException
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, "Database", "GetMetricsDisplayGridsDataTable", ex.Message, Me)
  '    Return Nothing
  '  Catch ex As Exception
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, "Database", "GetMetricsDisplayGridsDataTable", ex.Message, Me)
  '    Return Nothing

  '  Finally
  '    If Not dbCommand Is Nothing Then
  '      If dbCommand.Connection.State <> ConnectionState.Closed Then
  '        dbCommand.Connection.Close()
  '      End If
  '    End If

  '  End Try
  'End Function

  Public Function GetMetricsDisplayGridRowsDataTable(ByVal guidGridID As Guid) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetMetricsDisplayGridRows", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@DisplayGridID", guidGridID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetDisplayGridRowsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetDisplayGridRows", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetMetricListDataTableForReport() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsopRptFilteredParameterSetMetrics", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetMetricMethodArgsDataTable(ByVal functionMetricID As Guid, ByVal metricType As String, _
                                                ByVal methodName As String) As DataTable

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetMetricMethodArgs", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@FunctionMetricID", functionMetricID))
      dbCommand.Parameters.Add(New SqlParameter("@MetricTypeName", metricType))
      dbCommand.Parameters.Add(New SqlParameter("@MethodName", methodName))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If


      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetMetricMpcValuesDataTable(ByVal guidFunctionMetricID As Guid, _
                                              ByVal strMpcType As String) As DataTable

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetMetricMpcValues", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@FunctionMetricID", guidFunctionMetricID))
      dbCommand.Parameters.Add(New SqlParameter("@MpcType", strMpcType))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetMpcValuesDataTableForAllMetrics(ByVal IsMaster As Boolean) As DataTable

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetMpcValuesForAllMetricsInSet", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      If IsMaster Then
        dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.MasterProcessParameterID))
      Else
        dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      End If

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function


  Public Function GetMetricParamArrayMapDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gMetricParamArrayMap", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetMetricPropertiesDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsopGetMetricProperties", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetParameterArray(ByVal parameterName As String, ByVal parameterValue As String) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetParameterArray", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterName", parameterName))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterValue", parameterValue))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetParameterListDataTableForReport() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsopRptFilteredParameterSetParameters", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetTaskListParameterArray(ByVal parameterName As String, ByVal parameterValue As String) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetTaskListParameterArray", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterName", parameterName))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterValue", parameterValue))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetParameterArrayDataTable(ByVal parameterName As String, ByVal parameterValue As String) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetParameterArray", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterName", parameterName))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterValue", parameterValue))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetParameterArrayDictStrInt(ByVal parameterName As String, ByVal parameterValue As String) As Dictionary(Of String, Integer)
    Dim dbCommand As SqlCommand = Nothing
    Try
      'Dim tableParamArray As New DataTable
      'Dim adapParamArray As SqlDataAdapter
      'Dim rowParamArray As DataRow
      'Dim strParamArray As String()
      'Dim alParameterArray As ArrayList
      Dim strParamArrayDict As New Dictionary(Of String, Integer)
      Dim rdr As SqlDataReader

      dbCommand = New SqlCommand("pspTsop6gGetParameterArray", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterName", parameterName))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterValue", parameterValue))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      'adapParamArray = New SqlDataAdapter(dbCommand)
      'adapParamArray.Fill(tableParamArray)

      'ReDim strParamArray(tableParamArray.Rows.Count - 1)
      'For Each rowParamArray In tableParamArray.Rows
      '  strParamArray(rowParamArray("ParamArrayIndex")) = rowParamArray("ParamArrayValue")
      'Next

      'alParameterArray = New ArrayList(strParamArray)

      rdr = dbCommand.ExecuteReader
      Do While rdr.Read
        strParamArrayDict.Add(rdr("ParamArrayValue"), rdr("ParamArrayIndex"))
      Loop

      Return strParamArrayDict

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetParameterArrayStringArray(ByVal parameterName As String, ByVal parameterValue As String) As String()
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim tableParamArray As New DataTable
      Dim adapParamArray As SqlDataAdapter
      Dim rowParamArray As DataRow
      Dim strParamArray As String()

      dbCommand = New SqlCommand("pspTsop6gGetParameterArray", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterName", parameterName))
      dbCommand.Parameters.Add(New SqlParameter("@ParameterValue", parameterValue))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      adapParamArray = New SqlDataAdapter(dbCommand)
      adapParamArray.Fill(tableParamArray)

      ReDim strParamArray(tableParamArray.Rows.Count - 1)
      For Each rowParamArray In tableParamArray.Rows
        strParamArray(rowParamArray("ParamArrayIndex")) = rowParamArray("ParamArrayValue")
      Next
      Return strParamArray

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetParameterSetsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetParameterSetList", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@StationID", Me.StationID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))
      dbCommand.Parameters.Add(New SqlParameter("@LotTypeID", Me.LotTypeID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetParameterSetsDataTable(ByVal blnProd As Boolean) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter
      If blnProd Then
        dbCommand = New SqlCommand("pspTsop6gGetParameterSets_Prod", Connection)
      Else
        dbCommand = New SqlCommand("pspTsop6gGetParameterSets_All", Connection)
      End If
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@StationID", Me.StationID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Sub GetPreviousSerialNumbers(ByVal dateCode As String, ByRef initialSerialNumber As Integer, ByRef finalSerialNumber As Integer)
    Dim dbCommand As SqlCommand = Nothing
    Dim rdr As SqlDataReader
    Try

      dbCommand = New SqlCommand("pspTsop6gGetPrevSerialNumbers", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))
      dbCommand.Parameters.Add(New SqlParameter("@DateCode", dateCode))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      rdr = dbCommand.ExecuteReader
      Do While rdr.Read
        initialSerialNumber = CInt(rdr("InitialSerialNumber"))
        finalSerialNumber = CInt(rdr("MarkedSerialNumber"))
      Loop

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub


  Public Sub GetProductID()
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim param1 As New SqlParameter
      Dim param2 As New SqlParameter

      dbCommand = New SqlCommand("pspTsop6gGetProductID", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      param1.ParameterName = "@ProcessParameterID"
      param1.Value = Me.ProcessParameterID
      dbCommand.Parameters.Add(param1)

      param2.ParameterName = "@ProductID"
      param2.DbType = DbType.Guid
      param2.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(param2)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      Me.mProductID = dbCommand.Parameters.Item("@ProductID").Value

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Function GetRelationsToExternalTestMethodsCollection() As Dictionary(Of String, Guid)
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataReader As SqlDataReader
      Dim dict As New Dictionary(Of String, Guid)

      dbCommand = New SqlCommand("pspTsop6gGetRelationsToExternalTestMethodList", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbDataReader = dbCommand.ExecuteReader
      Do While dbDataReader.Read
        dict.Add(dbDataReader("Relationship"), dbDataReader("RelationshipID"))
      Loop
      Return dict

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetRTYCurrentShiftAndHour(ByVal strAssemblyLine) As DataTable

    Dim dbCommand As SqlCommand = Nothing
    Dim param1 As New SqlParameter
    Dim param2 As New SqlParameter
    Dim dbDataTable As New DataTable
    Dim dbAdpt As SqlDataAdapter

    Try
      Dim dbGuid As Guid = Nothing

      dbCommand = New SqlCommand("pspOEE_GetCurrentShift_Hour", RTYConnection)
      dbCommand.CommandType = CommandType.StoredProcedure
      dbCommand.Parameters.Add(New SqlParameter("@AssemblyLineID", strAssemblyLine))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)

      Return dbDataTable

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function

  Public Function GetRTYMachineData(ByVal strAssemblyLine) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Dim param1 As New SqlParameter
    Dim param2 As New SqlParameter
    Dim dbDataTable As New DataTable
    Dim dbAdpt As SqlDataAdapter

    Try
      Dim dbGuid As Guid = Nothing

      dbCommand = New SqlCommand("pspOEE_GetMachinesByAssemblyLineID", RTYConnection)
      dbCommand.CommandType = CommandType.StoredProcedure
      dbCommand.Parameters.Add(New SqlParameter("@assemblyLineID", strAssemblyLine))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)

      Return dbDataTable

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try

  End Function

  Public Function GetRTYMachineDataItem(ByVal strMachineID As String) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspOEE_LkpMachineDataItems", RTYConnection)
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@MachineID", strMachineID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)

      Return dbDataTable

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function


  Public Function GetTestReportDataTable(ByVal guidFunctionMetricID As Guid, _
                                              ByVal strMpcType As String) As DataTable

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetMetricMpcValues", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.ProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@FunctionMetricID", guidFunctionMetricID))
      dbCommand.Parameters.Add(New SqlParameter("@MpcType", strMpcType))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetTsopFunctionValuesDictionary() As Dictionary(Of String, Int32)
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter
      Dim dict As New Dictionary(Of String, Int32)
      Dim row As DataRow

      dbCommand = New SqlCommand("pspTsop6gGetTsopFunctionValues", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      For Each row In dbDataTable.Rows
        dict.Add(row("FunctionName"), row("FunctionValue"))
      Next
      Return dict

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetTsopModeValuesDictionary() As Dictionary(Of Int32, Guid)
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter
      Dim dict As New Dictionary(Of Int32, Guid)
      Dim row As DataRow

      dbCommand = New SqlCommand("pspTsop6gGetTsopModeValues", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      For Each row In dbDataTable.Rows
        dict.Add(row("ModeValue"), row("ModeID"))
      Next

      Return dict

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetTsopSubProcessesDictionary() As Dictionary(Of String, Guid)
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter
      Dim dict As New Dictionary(Of String, Guid)
      Dim dbRow As DataRow

      dbCommand = New SqlCommand("pspTsop6gGetTsopSubProcesses", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)

      For Each dbRow In dbDataTable.Rows
        dict.Add(dbRow("TSOP_SubProcessName"), dbRow("TSOP_SubProcessID"))
      Next
      Return dict

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function GetUnreleasedParameterSetsDataTable() As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim dbDataTable As New DataTable
      Dim dbAdpt As SqlDataAdapter

      dbCommand = New SqlCommand("pspTsop6gGetUnreleasedParameterSetList", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@StationID", Me.StationID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Sub InsertAcquiredDataRecord(ByVal byteArray() As Byte, ByVal strSeries As String _
                                      , ByVal strEncodedSerialNumber As String, ByVal intMeasurementNumber As Integer)

    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gADR_InsAcquiredDataRecord", Me.mADRConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@series", strSeries))
      dbCommand.Parameters.Add(New SqlParameter("@StationID", Me.StationID))
      dbCommand.Parameters.Add(New SqlParameter("@testID", Me.TestID))
      If strEncodedSerialNumber Is Nothing Then
        strEncodedSerialNumber = ""
      End If
      dbCommand.Parameters.Add(New SqlParameter("@encodedSerialNumber", strEncodedSerialNumber))
      dbCommand.Parameters.Add(New SqlParameter("@ADR_StructureID", Me.ADRStructureID))
      dbCommand.Parameters.Add(New SqlParameter("@MeasurementName", intMeasurementNumber))
      dbCommand.Parameters.Add(New SqlParameter("@acquiredData", byteArray))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub

  Public Sub InsertAlphaTestResults(ByVal strXmlString As String, ByVal bTestPassed As Boolean, ByVal strMetricList As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertAlphaTestResults", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.mTestID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))
      dbCommand.Parameters.Add(New SqlParameter("@TestPassed", bTestPassed))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message & vbCrLf & vbCrLf & "XML:" & vbCrLf & vbCrLf & strXmlString & vbCrLf & vbCrLf & "Metric List: " & strMetricList, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub InsertAnomalyAttributeValues(ByVal tsopAnomalyID As Guid, ByVal strXmlString As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertAnomalyAttributes", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TsopAnomalyID", tsopAnomalyID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    Catch ex As Exception
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub

  Public Sub InsertExternalTestMethod(ByVal TestMethodName As String, ByVal sortIndex As Single)
    'Dim dbCommand As SqlCommand = Nothing
    'Try
    '  'dbCommand = New SqlCommand("pspTsop6gInsertExternalTestMethod", Connection)
    '  dbCommand = New SqlCommand("pspTsop6gExternalTestMethodsInsert", Connection)
    '  dbCommand.CommandType = CommandType.StoredProcedure

    '  dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))
    '  dbCommand.Parameters.Add(New SqlParameter("@ExternalTestMethodName", TestMethodName))
    '  dbCommand.Parameters.Add(New SqlParameter("@SortIndex", sortIndex)) 'TER_05/06/09
    '  If dbCommand.Connection.State <> ConnectionState.Open Then
    '    dbCommand.Connection.Open()
    '  End If

    '  dbCommand.ExecuteNonQuery()

    'Catch ex As SqlException
    '  If dbCommand.Connection.State <> ConnectionState.Closed Then
    '    dbCommand.Connection.Close()
    '  End If
    '  mAnomaly = New clsDbAnomaly(6001, "Database", "InsertExternalTestMethod", ex.Message, Me)
    'Catch ex As Exception
    '  If dbCommand.Connection.State <> ConnectionState.Closed Then
    '    dbCommand.Connection.Close()
    '  End If
    '  mAnomaly = New clsDbAnomaly(6001, "Database", "InsertExternalTestMethod", ex.Message, Me)

    'Finally
    '  If Not dbCommand Is Nothing Then
    '    If dbCommand.Connection.State <> ConnectionState.Closed Then
    '      dbCommand.Connection.Close()
    '    End If
    '  End If

    'End Try
  End Sub

  Public Sub InsertLotRecord(ByVal strLotName As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertLotRecord", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.mLotID))
      dbCommand.Parameters.Add(New SqlParameter("@LotTypeID", Me.mLotTypeID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopID", Me.TsopID))
      dbCommand.Parameters.Add(New SqlParameter("@LotName", strLotName))
      dbCommand.Parameters.Add(New SqlParameter("@LotDateTime", Now))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub InsertNextActuatorIntoBurnInRack(ByVal CartUniqueID As String, _
                                               ByVal CartNumber As Integer, _
                                               ByVal BOM As String, _
                                               ByVal ActuatorPosition As Integer, _
                                               ByVal ActuatorSerialNumber As String)

    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspInsertActuatorToBurnInRack", BIConnection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure
      dbCommand.Connection = BIConnection 'SqlConn ' Sets which connection you want to use

      dbCommand.Parameters.Add(New SqlParameter("@CartUniqueID", CartUniqueID))
      dbCommand.Parameters.Add(New SqlParameter("@CartNumber", CartNumber))
      dbCommand.Parameters.Add(New SqlParameter("@BOM", BOM))
      dbCommand.Parameters.Add(New SqlParameter("@ActuatorPosition", ActuatorPosition))
      dbCommand.Parameters.Add(New SqlParameter("@ActuatorSerialNumber", ActuatorSerialNumber))
      dbCommand.Parameters.Add(New SqlParameter("@ActuatorEnteredTime", Now))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub


  Public Sub InsertProgExternalTestMethod(ByVal TestMethodID As Guid, ByVal RelationshipID As Guid, _
                                          Optional ByVal DuringInterval As String = Nothing)

    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertProgExternalTestMethod", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProgrammingID", Me.ProgrammingID))
      dbCommand.Parameters.Add(New SqlParameter("@ExternalTestMethodID", TestMethodID))
      dbCommand.Parameters.Add(New SqlParameter("@RelationshipID", RelationshipID))
      If DuringInterval <> Nothing Then
        dbCommand.Parameters.Add(New SqlParameter("@DuringTestMethodInterval", DuringInterval))
      End If

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub InsertProgRecord(ByVal sTsopVersion As String, ByVal sOperator As String, _
                              ByVal sTemperature As String, ByVal sComment As String, _
                              ByVal sSampleNumber As String, ByVal sTestLog As String)

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim parmOut As New SqlParameter

      Me.mProgDateTime = Now

      dbCommand = New SqlCommand("pspTsop6gInsertProgRecord", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@DeviceInProcessID", Me.mDeviceInProcessID))
      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.mProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.mLotID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopStartupID", Me.mTsopStartupID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopModeID", Me.mTsopModeID))
      dbCommand.Parameters.Add(New SqlParameter("@ProgDateTime", Me.mProgDateTime))
      dbCommand.Parameters.Add(New SqlParameter("@TsopVersion", sTsopVersion))
      dbCommand.Parameters.Add(New SqlParameter("@ProgOperator", sOperator))
      dbCommand.Parameters.Add(New SqlParameter("@ProgTemperature", sTemperature))
      dbCommand.Parameters.Add(New SqlParameter("@ProgComment", sComment))
      dbCommand.Parameters.Add(New SqlParameter("@ProgSampleNumber", sSampleNumber))
      dbCommand.Parameters.Add(New SqlParameter("@ProgTestLog", sTestLog))

      parmOut.ParameterName = "@ProgrammingID"
      parmOut.DbType = DbType.Guid
      parmOut.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(parmOut)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      Me.mProgrammingID = dbCommand.Parameters.Item("@ProgrammingID").Value

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub InsertProgResults(ByVal strXmlString As String, ByVal bProgPassed As Boolean, ByVal strMetricList As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertProgResults", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProgrammingID", Me.mProgrammingID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))
      dbCommand.Parameters.Add(New SqlParameter("@ProgPassed", bProgPassed))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      'mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message & vbCrLf & vbCrLf & "XML:" & vbCrLf & vbCrLf & strXmlString & vbCrLf & vbCrLf & "Metric List: " & strMetricList, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Function InsertRTYDataCollected(ByVal dtDataTable As DataTable, ByVal strMachineID As String, _
                                 ByVal strDate As String, ByVal strShift As String, ByVal strBOM As String, ByVal strHour As String, ByVal strProcessDate As String) As String
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspOEE_InsDataItemValues", RTYConnection)
      dbCommand.CommandType = CommandType.StoredProcedure
      dbCommand.Parameters.Add(New SqlParameter("@MachineID", strMachineID))
      dbCommand.Parameters.Add(New SqlParameter("@Date", strDate))
      dbCommand.Parameters.Add(New SqlParameter("@Shift", strShift))
      dbCommand.Parameters.Add(New SqlParameter("@Hour", strHour))
      dbCommand.Parameters.Add(New SqlParameter("@ProcessDate", strProcessDate))
      dbCommand.Parameters.Add(New SqlParameter("@BOM", strBOM))
      dbCommand.Parameters.Add(New SqlParameter("@tblRTY_OEE_DataItemValues", dtDataTable))


      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()
      Return "Database Save OK"

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return "Database Saving Error"
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return "Database Saving Error"
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function

  Public Function InsertRTYDataCollectedTimers(ByRef Position As Integer, ByVal strMachineID As String, ByVal strBOM As String, ByVal strShift As String, ByVal strHour As String, _
                                               ByVal strProcessDate As String, ByVal dblTimer1Val As Double, ByVal dblTimer2Val As Double, ByVal dblTimer3Val As Double, _
                                               ByVal dblTimer4Val As Double, ByVal dblTimer5Val As Double) As String

    Dim dbCommand As SqlCommand = Nothing
    Static intRecordCount As Integer

    Try
      intRecordCount = intRecordCount + 1
      dbCommand = New SqlCommand("pspOEE_InsCumulativeTimes", RTYConnection)
      dbCommand.CommandType = CommandType.StoredProcedure
      dbCommand.Parameters.Add(New SqlParameter("@MachineID", strMachineID))
      dbCommand.Parameters.Add(New SqlParameter("@RTY_CumulativeDate", strProcessDate))
      dbCommand.Parameters.Add(New SqlParameter("@RTY_CumulativeShift", strShift))
      dbCommand.Parameters.Add(New SqlParameter("@RTY_CumulativeHour", strHour))
      dbCommand.Parameters.Add(New SqlParameter("@BOM", strBOM))
      dbCommand.Parameters.Add(New SqlParameter("@RTYT1BBTime", dblTimer1Val))
      dbCommand.Parameters.Add(New SqlParameter("@RTYT2PalletXferTime", dblTimer2Val))
      dbCommand.Parameters.Add(New SqlParameter("@RTYT3OpWaitTime", dblTimer3Val))
      dbCommand.Parameters.Add(New SqlParameter("@RTYT4OpWorkTime", dblTimer4Val))
      dbCommand.Parameters.Add(New SqlParameter("@RTYT5MachineTime", dblTimer5Val))
      dbCommand.Parameters.Add(New SqlParameter("@RecordCount", intRecordCount))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()
      Return "Database Save OK"

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return "Database Saving Error"
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return "Database Saving Error"
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function


  Public Sub InsertTestExternalTestMethod(Optional ByVal DuringInterval As String = Nothing)

    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertTestExternalTestMethod", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.TestID))
      dbCommand.Parameters.Add(New SqlParameter("@ExternalTestMethodID", Me.mExternalTestMethodID))
      dbCommand.Parameters.Add(New SqlParameter("@RelationshipID", Me.mExternalTestMethodRelationshipID))
      If DuringInterval <> Nothing Then
        dbCommand.Parameters.Add(New SqlParameter("@DuringTestMethodInterval", DuringInterval))
      End If

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub InsertTestFailures(ByVal strXmlString As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertTestFailures", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.mTestID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub InsertTestRecord(ByVal sTsopVersion As String, ByVal sOperator As String, _
                              ByVal sTemperature As String, ByVal sComment As String, _
                              ByVal sSampleNumber As String, ByVal sTestLog As String)

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim parmOut As New SqlParameter

      Me.mTestDateTime = Now

      dbCommand = New SqlCommand("pspTsop6gInsertTestRecord", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@DeviceInProcessID", Me.mDeviceInProcessID))
      dbCommand.Parameters.Add(New SqlParameter("@ProcessParameterID", Me.mProcessParameterID))
      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.mLotID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopStartupID", Me.mTsopStartupID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopModeID", Me.mTsopModeID))
      dbCommand.Parameters.Add(New SqlParameter("@TestDateTime", Me.mTestDateTime))
      dbCommand.Parameters.Add(New SqlParameter("@TsopVersion", sTsopVersion))
      dbCommand.Parameters.Add(New SqlParameter("@TestOperator", sOperator))
      dbCommand.Parameters.Add(New SqlParameter("@TestTemperature", sTemperature))
      dbCommand.Parameters.Add(New SqlParameter("@TestComment", sComment))
      dbCommand.Parameters.Add(New SqlParameter("@TestSampleNumber", sSampleNumber))
      dbCommand.Parameters.Add(New SqlParameter("@TestTestLog", sTestLog))

      parmOut.ParameterName = "@TestID"
      parmOut.DbType = DbType.Guid
      parmOut.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(parmOut)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      Me.mTestID = dbCommand.Parameters.Item("@TestID").Value

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub InsertTestResults(ByVal strXmlString As String, ByVal bTestPassed As Boolean, ByVal strMetricList As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gInsertTestResults", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.mTestID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))
      dbCommand.Parameters.Add(New SqlParameter("@TestPassed", bTestPassed))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message & vbCrLf & vbCrLf & "XML:" & vbCrLf & vbCrLf & strXmlString & vbCrLf & vbCrLf & "Metric List: " & strMetricList, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Function InsertTsopAnomaly(ByVal anomalyDefinitionID As Guid, _
                                    Optional ByVal sOperator As String = Nothing) As Guid

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim parmOut As New SqlParameter

      dbCommand = New SqlCommand("pspTsop6gInsertTsopAnomaly", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@AnomalyDefinitionID", anomalyDefinitionID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopSubProcessID", Me.mSubProcessID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopStartupID", Me.mTsopStartupID))
      dbCommand.Parameters.Add(New SqlParameter("@AnomalyDateTime", Now()))
      dbCommand.Parameters.Add(New SqlParameter("@Operator", sOperator))
      If Me.mProgrammingID = Guid.Empty Then
        dbCommand.Parameters.Add(New SqlParameter("@ProgrammingID", Nothing))
      Else
        dbCommand.Parameters.Add(New SqlParameter("@ProgrammingID", Me.mProgrammingID))
      End If
      If Me.mTestID = Guid.Empty Then
        dbCommand.Parameters.Add(New SqlParameter("@TestID", Nothing))
      Else
        dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.mTestID))
      End If

      parmOut.ParameterName = "@TsopAnomalyID"
      parmOut.SqlDbType = SqlDbType.UniqueIdentifier
      parmOut.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(parmOut)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      Return dbCommand.Parameters.Item("@TsopAnomalyID").Value

    Catch ex As SqlException
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing
    Catch ex As Exception
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Function InsertTsopAnomalyWithTaskInfo(ByVal anomalyDefinitionID As Guid, _
            ByVal taskNumber As Integer, ByVal taskName As String, Optional ByVal sOperator As String = Nothing) As Guid

    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim parmOut As New SqlParameter

      dbCommand = New SqlCommand("pspTsop6gInsertTsopAnomalyWithTaskInfo", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@AnomalyDefinitionID", anomalyDefinitionID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopSubProcessID", Me.mSubProcessID))
      dbCommand.Parameters.Add(New SqlParameter("@TsopStartupID", Me.mTsopStartupID))
      dbCommand.Parameters.Add(New SqlParameter("@AnomalyDateTime", Now()))
      dbCommand.Parameters.Add(New SqlParameter("@TaskNumber", taskNumber))
      dbCommand.Parameters.Add(New SqlParameter("@TaskName", taskName))
      dbCommand.Parameters.Add(New SqlParameter("@Operator", sOperator))
      If Me.mProgrammingID = Guid.Empty Then
        dbCommand.Parameters.Add(New SqlParameter("@ProgrammingID", Nothing))
      Else
        dbCommand.Parameters.Add(New SqlParameter("@ProgrammingID", Me.mProgrammingID))
      End If
      If Me.mTestID = Guid.Empty Then
        dbCommand.Parameters.Add(New SqlParameter("@TestID", Nothing))
      Else
        dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.mTestID))
      End If

      parmOut.ParameterName = "@TsopAnomalyID"
      parmOut.SqlDbType = SqlDbType.UniqueIdentifier
      parmOut.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(parmOut)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      Return dbCommand.Parameters.Item("@TsopAnomalyID").Value

    Catch ex As SqlException
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing
    Catch ex As Exception
      Me.mDbError = True
      Me.mDbErrorProcedure = mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName
      Me.mDbErrorMessage = ex.Message
      Return Nothing

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Function

  Public Sub NewStartupID()
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim param1 As New SqlParameter
      Dim param2 As New SqlParameter

      dbCommand = (New SqlCommand("pspTsop6gNewTsopStartupID", Connection))
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      param1.ParameterName = "@TsopStartupDateTime"
      param1.Value = Now()
      dbCommand.Parameters.Add(param1)

      param2.ParameterName = "@TsopStartupID"
      param2.DbType = DbType.Guid
      param2.Direction = ParameterDirection.Output
      dbCommand.Parameters.Add(param2)

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

      Me.mTsopStartupID = dbCommand.Parameters.Item("@TsopStartupID").Value

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub SetupADRDbConnection()
    Dim strConnectionString As String
    Try

      strConnectionString = ConfigurationManager.ConnectionStrings("dbADRConnectionString").ConnectionString
      Me.mADRConnection = New SqlConnection(strConnectionString)

    Catch ex As SqlException
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    End Try
  End Sub

  Public Function SetCartAsComplete(ByVal tbl As String, ByVal UniqueID As String) As String
    Dim dbCommand As New SqlCommand

    Try
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.Text
      dbCommand.Connection = BIConnection 'SqlConn ' Sets which connection you want to use
      dbCommand.CommandText = "UPDATE " & tbl & _
                          " Set CartComplete = 1 " & _
                          " WHERE " & _
                          "CartUniqueID" & "='" & UniqueID & "'"

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteScalar()

      Return "Database Save OK"

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return "Database Saving Error"
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return "Database Saving Error"
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function


  Public Sub UpdateDipAttributeValue(ByVal DipAttributeName As String, ByVal DipAttributeValue As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      Dim paramOut As New SqlParameter

      dbCommand = New SqlCommand("pspTsop6gUpdateDipAttributeValue", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@DeviceInProcessID", Me.DeviceInProcessID))
      dbCommand.Parameters.Add(New SqlParameter("@AttributeName", DipAttributeName))
      dbCommand.Parameters.Add(New SqlParameter("@AttributeValue", DipAttributeValue))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub UpdateLotData(ByVal strXmlString As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gUpdateLotData", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.mLotID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))
      dbCommand.Parameters.Add(New SqlParameter("@LotDate", Now))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub UpdateLotMetricData(ByVal strXmlString As String, ByVal strMetricName As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("[pspTsop6gUpdateLotMetricStatistics]", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.mLotID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))
      dbCommand.Parameters.Add(New SqlParameter("@LotMetricStatsDate", Now))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message & vbCrLf & "XML:" & vbCrLf & strXmlString & vbCrLf & vbCrLf & "Metric: " & strMetricName, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub UpdateMultipleLotMetricData(ByVal strXmlString As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("[pspTsop6gUpdateMultipleLotMetricStatistics]", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@LotID", Me.mLotID))
      dbCommand.Parameters.Add(New SqlParameter("@XmlString", strXmlString))
      dbCommand.Parameters.Add(New SqlParameter("@LotMetricStatsDate", Now))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message & vbCrLf & "XML:" & vbCrLf & strXmlString & vbCrLf & vbCrLf, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub UpdateProductSerialNumbers(ByVal dateCode As String, ByVal initialSerialNumber As Integer, ByVal finalSerialNumber As Integer)
    Dim dbCommand As SqlCommand = Nothing
    Dim strInitialSerialNumber As String
    Dim strFinalSerialNumber As String

    Try

      strInitialSerialNumber = CStr(Format(initialSerialNumber, "00000"))
      strFinalSerialNumber = CStr(Format(finalSerialNumber, "00000"))

      dbCommand = New SqlCommand("pspTsop6gUpdatePrevSerialNumbers", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProductID", Me.ProductID))
      dbCommand.Parameters.Add(New SqlParameter("@DateCode", dateCode))
      dbCommand.Parameters.Add(New SqlParameter("@InitialSerialNumber", strInitialSerialNumber))
      dbCommand.Parameters.Add(New SqlParameter("@MarkedSerialNumber", strFinalSerialNumber))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub



  Public Sub UpdateProgPassedBit(ByVal bProgPassed As Boolean)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gUpdateProgPassed", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@ProgrammingID", Me.mProgrammingID))
      dbCommand.Parameters.Add(New SqlParameter("@ProgPassed", bProgPassed))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub UpdateTestDeviceInProcessID()
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gUpdateTestDeviceInProcess", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.mTestID))
      dbCommand.Parameters.Add(New SqlParameter("@DeviceInProcessID", Me.mDeviceInProcessID))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub UpdateTestPassedBit(ByVal bTestPassed As Boolean)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("pspTsop6gUpdateTestPassed", Connection)
      dbCommand.CommandTimeout = TIMEOUT '1.1.0.3 DLG
      dbCommand.CommandType = CommandType.StoredProcedure

      dbCommand.Parameters.Add(New SqlParameter("@TestID", Me.mTestID))
      dbCommand.Parameters.Add(New SqlParameter("@TestPassed", bTestPassed))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If

    End Try
  End Sub

  Public Sub UploadEEPROMToSQL(ByVal EEPROM_Data As String, ByRef ExcelVersion As String, ByVal strEncodedSerialNumber As String)
    Dim dbCommand As SqlCommand = Nothing
    Try
      dbCommand = New SqlCommand("dbo.pspTsopInsEEPROM_ImageData", Connection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
      dbCommand.Parameters.Add(New SqlParameter("@testID", Me.TestID))
      dbCommand.Parameters.Add(New SqlParameter("@encodedSerialNumber", strEncodedSerialNumber))
      dbCommand.Parameters.Add(New SqlParameter("@EEPROM_Data", EEPROM_Data))
      dbCommand.Parameters.Add(New SqlParameter("@excelVersion", ExcelVersion))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      If dbCommand.Connection.State = ConnectionState.Open Then
        dbCommand.ExecuteScalar()
      End If

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub

  Public Sub SendBurnInNotification(ByRef strCartID As String, ByRef strCartNumber As String, ByRef strStall As String, ByRef strBom As String, _
                                    ByVal strEncodedSerialNumber As String)

    Dim dbCommand As SqlCommand = Nothing
    Dim stationName As String = ""
    Dim dbDataTable As New DataTable

    Try
      dbCommand = New SqlCommand("dbo.pspTsopInsBurnInFailure", ServerConnection)
      dbCommand.CommandTimeout = 60
      dbCommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
      dbCommand.Parameters.Add(New SqlParameter("@CartUniqueID", strCartID))
      dbCommand.Parameters.Add(New SqlParameter("@CartNumber", strCartNumber))
      dbCommand.Parameters.Add(New SqlParameter("@Stall", strStall))
      dbCommand.Parameters.Add(New SqlParameter("@Bom", strBom))
      dbCommand.Parameters.Add(New SqlParameter("@SerialNumber", strEncodedSerialNumber))

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      If dbCommand.Connection.State = ConnectionState.Open Then
        dbCommand.ExecuteScalar()
      End If

    Catch ex As SqlException
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
    Catch ex As Exception
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Sub



#End Region

#Region "Leak Test Methods"
  Public Function GetMostRecentCartNumber(ByVal tbl As String, _
                                          ByVal BOM As String, _
                                          ByRef CartUniqueID As String, _
                                          ByRef CartNumber As String, _
                                          ByRef ActuatorPosition As Integer) As DataTable

    Dim dbDataTable As New DataTable
    Dim dbAdpt As SqlDataAdapter
    Dim dbCommand As New SqlCommand

    Try
      CartUniqueID = Nothing '2.1.0.8DWD New

      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.Connection = BIConnection ' Sets which connection you want to use
      dbCommand.CommandType = CommandType.Text ' Sets the command type to use

      'pspGetCarts
      dbCommand.CommandText = "SELECT * FROM " & tbl & _
                          " where CartComplete=0 " & _
                          " AND BOM='" & BOM & "'" & _
                         " ORDER BY ActuatorEnteredTime DESC;"

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      If dbDataTable Is Nothing Then
        CartNumber = 0
      Else
        If dbDataTable.Rows.Count <> 0 Then
          CartNumber = dbDataTable.Rows(0).Item(columnName:="CartNumber")
        Else
          CartNumber = 0
        End If
      End If

      Return dbDataTable

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function

#End Region

#Region "Development"
  'Public Function GetAnomalyCategoriesDictionary() As Dictionary(Of String, Int32)

  '  Dim dbCommand As SqlCommand
  '  Dim dbDataTable As New DataTable
  '  Dim dbAdpt As SqlDataAdapter
  '  Dim dict As New Dictionary(Of String, Int32)
  '  Dim dbRow As DataRow

  '  dbCommand = New SqlCommand("", Connection)
  '  dbCommand.CommandType = CommandType.StoredProcedure

  '  dbCommand.Connection.Open()

  '  dbAdpt = New SqlDataAdapter(dbCommand)
  '  dbAdpt.Fill(dbDataTable)

  '  For Each dbRow In dbDataTable.Rows
  '    dict.Add(dbRow("TSOP_SubProcessName"), dbRow("TSOP_SubProcessID"))
  '    Debug.Print(dbRow("TSOP_SubProcessName") & " " & dbRow("TSOP_SubProcessID").ToString)
  '  Next

  '  If dbCommand.Connection.State <> ConnectionState.Closed Then
  '    dbCommand.Connection.Close()
  '  End If

  '  Return dict
  'End Function


  '----------------------------------------------------------------------------------------------------
  'Still working on below

  'Public Function GetProcessSequenceSteps() As Dictionary(Of Int16, String)
  '  Dim dbCommand As SqlCommand = New SqlCommand("pspTsopGetProcessingSequenceSteps", Connection)
  '  Dim reader As SqlDataReader
  '  Dim dict As New Dictionary(Of Int16, String)

  '  dbCommand.CommandType = CommandType.StoredProcedure
  '  dbCommand.Parameters.Add(New SqlParameter("@ProcessSequenceName", "FirstTest"))
  '  dbCommand.Connection.Open()
  '  reader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection)

  '  Do While reader.Read()
  '    dict.Add(reader.GetValue(1), reader.GetValue(2))
  '  Loop
  '  Return dict
  'End Function

  'Public Function GetProcessSequenceSteps2() As Dictionary(Of Int16, MethodDefinition)
  '  Dim dbMethodsCommand As SqlCommand = New SqlCommand("pspTsopGetProcessingSequenceSteps", Connection)
  '  Dim dbArgsCommand As SqlCommand = New SqlCommand("pspTsopGetProcessingSeqMethodArgs", Connection)
  '  Dim readerMethods As SqlDataReader
  '  Dim readerArgs As SqlDataReader
  '  Dim dict As New Dictionary(Of Int16, MethodDefinition)
  '  Dim methodDef As New MethodDefinition
  '  Dim stepNumber As Int16
  '  'Dim prevStepNumber As Int16
  '  Dim methodName As String
  '  Dim prevMethodName As String
  '  Dim argNumber As String
  '  Dim argValue As Object

  '  'Connection.Open()

  '  dbMethodsCommand.CommandType = CommandType.StoredProcedure
  '  dbMethodsCommand.Parameters.Add(New SqlParameter("@ProcessSequenceName", "FirstTest"))
  '  If Not dbMethodsCommand.Connection.State = ConnectionState.Open Then
  '    dbMethodsCommand.Connection.Open()
  '  End If
  '  readerMethods = dbMethodsCommand.ExecuteReader(CommandBehavior.CloseConnection)

  '  methodName = ""
  '  prevMethodName = ""
  '  'prevStepNumber = -1
  '  Do While readerMethods.Read()
  '    If Not readerMethods.IsDBNull(0) Then
  '      stepNumber = readerMethods.GetValue(0)
  '      methodName = readerMethods.GetValue(1)
  '      methodDef = New MethodDefinition
  '      dict.Add(stepNumber, methodDef)
  '    End If
  '  Loop
  '  Stop
  '  readerMethods.Close()

  '  dbArgsCommand.CommandType = CommandType.StoredProcedure
  '  If Not dbArgsCommand.Connection.State = ConnectionState.Open Then
  '    dbArgsCommand.Connection.Open()
  '  End If
  '  readerArgs = dbArgsCommand.ExecuteReader(CommandBehavior.CloseConnection)

  '  Do While readerArgs.Read()
  '    'methodDef = dict.Item
  '  Loop
  '  'If Not reader.IsDBNull(2) Then
  '  '  argNumber = reader.GetValue(2)
  '  '  argValue = reader.GetValue(3)
  '  '  methodDef.AddArgument(argNumber, argValue)
  '  'End If

  '  'If Not readerMethods.Read Then
  '  '  dict.Add(stepNumber, methodDef)
  '  'End If

  '  'If methodName <> nextMethodName Then
  '  '  dict.Add(stepNumber, methodDef)
  '  'End If

  '  'dict.Add(prevStepNumber, methodDef)

  '  'If methodName <> nextMethodName Then
  '  '  If nestMethodName <> "" Then
  '  '    dict.Add(prevStepNumber, methodDef)
  '  '  End If
  '  '  methodDef = New MethodDefinition
  '  'End If
  '  'methodDef.AddArgument(argNumber, argValue)
  '  'prevMethodName = methodName
  '  'prevStepNumber = stepNumber


  '  'dict.Add(prevStepNumber, methodDef)
  '  Return dict
  'End Function
#End Region

  'Public Function LookupTestData() As DataTable

  '  'EXECUTE @RC = [US47_AutoProductMfgTest].[dbo].[pspURA_LkplastTestResultSetFromStation] 
  '  '@stationComputerName

  '  Dim dbCommand As SqlCommand = Nothing
  '  Dim stationName As String = ""
  '  Dim dbDataTable As New DataTable
  '  Dim dbAdpt As SqlDataAdapter

  '  Try
  '    stationName = ConfigurationManager.AppSettings("StationName")
  '    dbCommand = New SqlCommand("pspURA_LkplastTestResultSetFromStation", Connection)
  '    dbCommand.CommandTimeout = TIMEOUT
  '    dbCommand.CommandType = CommandType.StoredProcedure

  '    dbCommand.Parameters.Add(New SqlParameter("@stationComputerName", stationName))


  '    If dbCommand.Connection.State <> ConnectionState.Open Then
  '      dbCommand.Connection.Open()
  '    End If


  '    dbAdpt = New SqlDataAdapter(dbCommand)
  '    dbAdpt.Fill(dbDataTable)

  '  Catch ex As SqlException
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
  '  Catch ex As Exception
  '    If dbCommand.Connection.State <> ConnectionState.Closed Then
  '      dbCommand.Connection.Close()
  '    End If
  '    mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)

  '  Finally
  '    If Not dbCommand Is Nothing Then
  '      If dbCommand.Connection.State <> ConnectionState.Closed Then
  '        dbCommand.Connection.Close()
  '      End If
  '    End If


  '  End Try
  '  Return dbDataTable
  'End Function

  Public Function LookupTestData(blnUnReleasedSets) As DataTable
    Dim dbCommand As SqlCommand = Nothing
    Dim stationName As String = ""
    Dim dbDataTable As New DataTable
    Dim dbAdpt As SqlDataAdapter

    Try
      If blnUnReleasedSets Then    '1.1.0.55 DLG \/\/
        stationName = ConfigurationManager.AppSettings("StationName")
        dbCommand = New SqlCommand("pspTSOP_LkplastTestResultSetFromStation", Connection)
        dbCommand.CommandTimeout = TIMEOUT
        dbCommand.CommandType = CommandType.StoredProcedure
        dbCommand.Parameters.Add(New SqlParameter("@stationComputerName", stationName))

      Else
        dbCommand = New SqlCommand("pspURA_LkplastTestOnCurrentDB", Connection)
        dbCommand.CommandTimeout = TIMEOUT
        dbCommand.CommandType = CommandType.StoredProcedure


      End If
      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If


      dbAdpt = New SqlDataAdapter(dbCommand)
      dbAdpt.Fill(dbDataTable)
      Return dbDataTable

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function


  Public Function DidProcessPCPass() As Boolean
    Dim dbCommand As SqlCommand = Nothing
    Dim stationName As String = ""
    Dim dbDataTable As New DataTable
    'Dim dbAdpt As SqlDataAdapter
    Dim strFound As String
    Dim strPassed As String
    'Dim blnFound As Boolean
    'Dim blnPassed As Boolean
    Dim sqlParam1 As New SqlParameter("@found", 0)
    Dim sqlParam2 As New SqlParameter("@passed", 0)

    Try

      '  stationName = ConfigurationManager.AppSettings("StationName")
      dbCommand = New SqlCommand("pspPPMA_LkpLineSortCodeStatus", ProcessPCConnection)
      dbCommand.CommandTimeout = TIMEOUT
      dbCommand.CommandType = CommandType.StoredProcedure
      dbCommand.Parameters.Add(New SqlParameter("@SerialNumber", gDeviceInProcess.EncodedSerialNumber))
      dbCommand.Parameters.Add("@found", SqlDbType.Int)
      dbCommand.Parameters("@found").Direction = ParameterDirection.Output

      dbCommand.Parameters.Add("@passed", SqlDbType.Int)
      dbCommand.Parameters("@passed").Direction = ParameterDirection.Output

      If dbCommand.Connection.State <> ConnectionState.Open Then
        dbCommand.Connection.Open()
      End If

      dbCommand.ExecuteNonQuery()
      dbCommand.Connection.Close()
      strFound = Convert.ToString(dbCommand.Parameters("@found").Value.ToString())
      strPassed = Convert.ToString(dbCommand.Parameters("@passed").Value.ToString())

      If strFound = 0 Or strPassed = 0 Then


        dbCommand = New SqlCommand("pspPPMA_LkpLineSortCodeStatus", ServerConnection)

        dbCommand.CommandType = CommandType.StoredProcedure
        dbCommand.Parameters.Add(New SqlParameter("@SerialNumber", gDeviceInProcess.EncodedSerialNumber))
        dbCommand.Parameters.Add("@found", SqlDbType.Int)
        dbCommand.Parameters("@found").Direction = ParameterDirection.Output
        dbCommand.Parameters.Add("@passed", SqlDbType.Int)
        dbCommand.Parameters("@passed").Direction = ParameterDirection.Output

        If dbCommand.Connection.State <> ConnectionState.Open Then
          dbCommand.Connection.Open()
        End If

        dbCommand.ExecuteNonQuery()
        dbCommand.Connection.Close()
        strFound = Convert.ToString(dbCommand.Parameters("@found").Value.ToString())
        strPassed = Convert.ToString(dbCommand.Parameters("@passed").Value.ToString())

      End If

      If strFound = 1 And strPassed = 1 Then
        Return True
      Else
        Return False
      End If

    Catch ex As SqlException
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Catch ex As Exception
      Console.WriteLine(ex.Message.ToString)
      If dbCommand.Connection.State <> ConnectionState.Closed Then
        dbCommand.Connection.Close()
      End If
      mAnomaly = New clsDbAnomaly(6001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message, Me)
      Return Nothing
    Finally
      If Not dbCommand Is Nothing Then
        If dbCommand.Connection.State <> ConnectionState.Closed Then
          dbCommand.Connection.Close()
        End If
      End If
    End Try
  End Function

End Class
