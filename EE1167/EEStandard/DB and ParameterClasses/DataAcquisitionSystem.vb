
#Region "Imports"
Imports System.Data.SqlClient
Imports System.Reflection
#End Region

Public Class DataAcquisitionSystem
  'Private Const mStackTrace.CurrentClassName As String = "DataAcquisitionSystem" 'TER6g

#Region "Private Member Variables"
  Private mInvalidData As Boolean
  Private mAnomaly As clsDbAnomaly 'TER6g
  Private mDatabase As Database 'TER6g
  Private mStackTrace As New clsStackTrace 'WTG
  Private mstrClassConfigFilePath As String 'WTG
  Private mSamplingRate As Double
  Private mintCh1FilterNumber As Integer
  Private mintCh2FilterNumber As Integer
  Private mintCh3FilterNumber As Integer
  Private mintCh4FilterNumber As Integer
  Private mstrROverallFilterIOAddress As String
  Private mstrROverallFilterIOAddressDutOutput1 As String
  Private mstrROverallFilterIOAddressDutOutput2 As String
  Private mstrRSeriesFilterIOAddress As String

#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)
    Try 'TER6g

      Dim strClassName As String
      Dim classType As Type
      Dim dbTable As DataTable
      Dim classProperty As PropertyInfo
      Dim strPropDataType As String
      Dim strPropValue As String
      Dim row As DataRow

      Me.mDatabase = db 'TER6g
      strClassName = Me.GetType.Name
      classType = GetType(DataAcquisitionSystem)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)

        If classProperty Is Nothing Then
          Me.InvalidData = True
          '\/\/ 'TER6g
          mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Property '" & row.Item("PropertyName").ToString & "' Doesn't Exist in Class")
          Throw New TsopAnomalyException
          '/\/\ 'TER6g
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub
#End Region

#Region "Properties"
  Public Property Anomaly() As clsDbAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      mAnomaly = value
    End Set
  End Property


  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

  Public Property ROverallFilterIOAddress() As String
    Get
      Return Me.mstrROverallFilterIOAddress
    End Get
    Set(ByVal value As String)
      Me.mstrROverallFilterIOAddress = value
    End Set
  End Property

  Public Property ROverallFilterIOAddressDutOutput1() As String
    Get
      Return Me.mstrROverallFilterIOAddressDutOutput1
    End Get
    Set(ByVal value As String)
      Me.mstrROverallFilterIOAddressDutOutput1 = value
    End Set
  End Property

  Public Property ROverallFilterIOAddressDutOutput2() As String
    Get
      Return Me.mstrROverallFilterIOAddressDutOutput2
    End Get
    Set(ByVal value As String)
      Me.mstrROverallFilterIOAddressDutOutput2 = value
    End Set
  End Property

  Public Property RSeriesFilterIOAddress() As String
    Get
      Return Me.mstrRSeriesFilterIOAddress
    End Get
    Set(ByVal value As String)
      Me.mstrRSeriesFilterIOAddress = value
    End Set
  End Property


  Public Property SamplingRate() As Double
    Get
      Return mSamplingRate
    End Get
    Set(ByVal value As Double)
      mSamplingRate = value
    End Set
  End Property

#End Region
End Class
