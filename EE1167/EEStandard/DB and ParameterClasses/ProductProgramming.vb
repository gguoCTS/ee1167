#Region "Imports =================================================="
Imports System.Data.SqlClient
Imports System.Reflection
#End Region

Public Class ProductProgramming
#Region "Private Member Variables =================================================="

  Private mAnomaly As clsAnomaly
  Private mstrAppSoftwareFilename As String
  Private mstrAppSoftwareVersion As String

  Private mstrEEPromHexFileName As String

  Private mstrHardwarePartNumber As String

  Private mInvalidData As Boolean

  Private mStackTrace As New clsStackTrace


#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)
    Dim strClassName As String
    Dim classType As Type
    Dim dbTable As DataTable
    Dim classProperty As PropertyInfo
    Dim strPropDataType As String
    Dim strPropValue As String
    Dim row As DataRow
    Try

      strClassName = Me.GetType.Name
      classType = GetType(ProductProgramming)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)
        If classProperty Is Nothing Then
          Me.InvalidData = True
          Exit For
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          'Select Case strPropDataType
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub
#End Region

#Region "Properties"
  Public Property Anomaly() As clsAnomaly
    Get
      Return Me.mAnomaly
    End Get
    Set(ByVal value As clsAnomaly)
      Me.mAnomaly = value
    End Set
  End Property

  Public Property AppSoftwareFilename
    Get
      Return mstrAppSoftwareFilename
    End Get
    Set(value)
      mstrAppSoftwareFilename = value
    End Set
  End Property

  Public Property EEPromHexFileName
    Get
      Return mstrEEPromHexFileName
    End Get
    Set(value)
      mstrEEPromHexFileName = value
    End Set
  End Property

  Public Property AppSoftwareVersion
    Get
      Return mstrAppSoftwareVersion
    End Get
    Set(value)
      mstrAppSoftwareVersion = value
    End Set
  End Property

  Public Property HardwarePartNumber As String
    Get
      Return mstrHardwarePartNumber
    End Get
    Set(value As String)
      mstrHardwarePartNumber = value
    End Set
  End Property

  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

#End Region

End Class
