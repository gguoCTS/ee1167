
#Region "Imports =================================================="
Imports System.Data.SqlClient
Imports System.Reflection
#End Region

Public Class ElectricalOperationAndLoad

  'Private Const mStackTrace.CurrentClassName As String = "ElectricalOperationAndLoad" 'TER6g

#Region "Private Variables"
  Private mAnomaly As clsDbAnomaly 'TER6g
  Private mDatabase As Database 'TER6g
  Private mStackTrace As New clsStackTrace 'WTG
  Private mstrClassConfigFilePath As String 'WTG
  Private mForceGraphEnabled As Boolean
  Private mInvalidData As Boolean
  Private mMicrogradientGraphEnabled As Boolean
  Private mVrefHighLimit As Double
  Private mVrefLowLimit As Double
  Private mdblTargetVrefLevel As Double
  Private mdblTestVoltage As Double
  Private mblnUsesAPSCal3 As Boolean
#End Region

#Region "Constructors =================================================="
  Public Sub New(ByVal db As Database)
    Try 'TER6g
      Dim strClassName As String
      Dim classType As Type
      Dim dbTable As DataTable
      Dim classProperty As PropertyInfo
      Dim strPropDataType As String
      Dim strPropValue As String
      Dim row As DataRow

      Me.mDatabase = db 'TER6g
      strClassName = Me.GetType.Name
      classType = GetType(ElectricalOperationAndLoad)
      dbTable = db.GetClassPropertyValues(strClassName)

      For Each row In dbTable.Rows
        classProperty = classType.GetProperty(row.Item("PropertyName").ToString)

        If classProperty Is Nothing Then
          Me.InvalidData = True
          '\/\/ 'TER6g
          mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "Property '" & row.Item("PropertyName").ToString & "' Doesn't Exist in Class")
          Throw New TsopAnomalyException
          '/\/\ 'TER6g
        Else
          strPropDataType = classProperty.PropertyType.Name
          strPropValue = row.Item("PropertyValue").ToString
          Select Case strPropDataType
            Case "Single"
              classProperty.SetValue(Me, CSng(strPropValue), Nothing)
            Case "Double"
              classProperty.SetValue(Me, CDbl(strPropValue), Nothing)
            Case "Integer"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "String"
              classProperty.SetValue(Me, CStr(strPropValue), Nothing)
            Case "Boolean"
              classProperty.SetValue(Me, CBool(strPropValue), Nothing)
            Case "Byte"
              classProperty.SetValue(Me, CByte(strPropValue), Nothing)
            Case "Long"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case "Object"
              classProperty.SetValue(Me, CObj(strPropValue), Nothing)
            Case "Int32"
              classProperty.SetValue(Me, CInt(strPropValue), Nothing)
            Case "Int64"
              classProperty.SetValue(Me, CLng(strPropValue), Nothing)
            Case Else
          End Select
        End If
      Next

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsDbAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub
#End Region

#Region "Properties"
  Public Property Anomaly() As clsDbAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsDbAnomaly)
      mAnomaly = value
    End Set
  End Property

  Public Property ForceGraphEnabled() As Boolean
    Get
      Return mForceGraphEnabled
    End Get
    Set(ByVal value As Boolean)
      mForceGraphEnabled = value
    End Set
  End Property

  Public Property InvalidData() As Boolean
    Get
      Return mInvalidData
    End Get
    Set(ByVal value As Boolean)
      mInvalidData = value
    End Set
  End Property

  Public Property MicrogradientGraphEnabled() As Boolean
    Get
      Return mMicrogradientGraphEnabled
    End Get
    Set(ByVal value As Boolean)
      mMicrogradientGraphEnabled = value
    End Set
  End Property

  Public Property TargetVrefLevel() As Double
    Get
      Return Me.mdblTargetVrefLevel
    End Get
    Set(ByVal value As Double)
      Me.mdblTargetVrefLevel = value
    End Set
  End Property

  Public Property TestVoltage() As Double
    Get
      Return mdblTestVoltage
    End Get
    Set(ByVal value As Double)
      mdblTestVoltage = value
    End Set
  End Property

  Public Property VrefHighLimit() As Double
    Get
      Return mVrefHighLimit
    End Get
    Set(ByVal value As Double)
      mVrefHighLimit = value
    End Set
  End Property

  Public Property VrefLowLimit() As Double
    Get
      Return mVrefLowLimit
    End Get
    Set(ByVal value As Double)
      mVrefLowLimit = value
    End Set
  End Property

  Public Property UsesAPSCal3 As Boolean
    Get
      Return mblnUsesAPSCal3
    End Get
    Set(value As Boolean)
      mblnUsesAPSCal3 = value
    End Set
  End Property

#End Region
End Class