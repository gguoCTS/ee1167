Option Explicit On
Option Strict Off
Imports System.Windows.Forms

Public Class frmPasswordDialog

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If txtInput.Text = "Password" Then
            Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Else
            Me.DialogResult = Windows.Forms.DialogResult.Cancel
        End If
        txtInput.Text = ""
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmPasswordDialog_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtInput.Focus()
    End Sub




    Private Sub frmPasswordDialog_VisibleChanged(sender As Object, e As EventArgs) Handles Me.VisibleChanged
        txtInput.Focus()
    End Sub
End Class
