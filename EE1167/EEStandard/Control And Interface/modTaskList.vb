Imports System.Reflection
Imports System.IO
Imports System.Configuration


Module modTaskList


  Public Sub AutoPrintResults()
    Dim lfrmUpdate As frmTsopMain = My.Application.OpenForms("frmTsopMain")
    Dim printDoc As New System.Drawing.Printing.PrintDocument

    Try
      AddHandler printDoc.PrintPage, AddressOf lfrmUpdate.PrintDoc_PrintPage

      'Print Test Results if autoprint is set
      If lfrmUpdate.mnuFunctionAutoPrintTestResults.Checked Then
        gstrPrintTableKey = "FTResults" ' 3.1.0.2 TER
        gdgvTableToPrint = gDataGrids(gstrPrintTableKey) ' 3.1.0.2 TER
        'Call lfrmUpdate.PrintTable.Print()
        Call printDoc.Print()
      End If

      'Print Prog Results if autoprint is set
      If lfrmUpdate.mnuFunctionAutoPrintProgResults.Checked Then
        gstrPrintTableKey = "ProgResults" ' 3.1.0.2 TER
        gdgvTableToPrint = gDataGrids(gstrPrintTableKey) ' 3.1.0.2 TER
        'Call lfrmUpdate.PrintTable.Print()
        Call printDoc.Print()
      End If

      RemoveHandler printDoc.PrintPage, AddressOf lfrmUpdate.PrintDoc_PrintPage

      'print results if autoprint is set
      If lfrmUpdate.mnuFunctionAutoPrintGraphs.Checked Then
        gintCurrentGraphPageNumber = 0
        Select Case gintGraphsPerPage
          Case 1
            lfrmUpdate.PrintGraphs.DefaultPageSettings.Landscape = True
          Case 2
            lfrmUpdate.PrintGraphs.DefaultPageSettings.Landscape = True
          Case 3
            lfrmUpdate.PrintGraphs.DefaultPageSettings.Landscape = False
          Case Else
            lfrmUpdate.PrintGraphs.DefaultPageSettings.Landscape = True
        End Select

        Call lfrmUpdate.PrintGraphs.Print()
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CalcDataArrays() 'Move to SensorTaskList
    Dim i As Integer
    Dim ldblScanTimeOffset As Double
    Try

      '==================================================
      'FORWARD 
      '==================================================
      'Forward DUT Output1, percent vref array
      Call CalcPercentVrefArray(gDataArrays.ForwardDUTOut1VoltageArray, gDataArrays.ForwardDUTVrefValueArray, gDataArrays.ForwardDUTOut1PercentVrefArray)
      If gDataArrays.ForwardEncoderPositionArray.Length <> gDataArrays.ForwardDUTOut1PercentVrefArray.Length Then
        Call ResizeArraysToMatch(gDataArrays.ForwardEncoderPositionArray, gDataArrays.ForwardDUTOut1PercentVrefArray)
      End If

      'Forward Scan2, percent vref array '2.0.1.0 TER
      If gDataArrays.ForwardDUTOutValueArray2 IsNot Nothing Then
        Call CalcPercentVrefArray(gDataArrays.ForwardDUTOutValueArray2, gDataArrays.ForwardDUTVrefValueArray2, gDataArrays.ForwardPercentVrefArray2)
        If gDataArrays.ForwardEncoderPositionArray2.Length <> gDataArrays.ForwardPercentVrefArray2.Length Then
          Call ResizeArraysToMatch(gDataArrays.ForwardEncoderPositionArray2, gDataArrays.ForwardPercentVrefArray2)
        End If
      End If

      'Forward , torque
      If gControlFlags.TorqueScanEnabled Then
        If gDataArrays.ForwardTorqueValueArray.Length > gDataArrays.ForwardEncoderPositionArray.Length Then
          Array.Resize(gDataArrays.ForwardTorqueValueArray, gDataArrays.ForwardEncoderPositionArray.Length)
        End If
      End If

      ''Add HomeBlockOffset to Forward data points
      'Call CalcAddOffsetToArray(gDataArrays.ForwardEncoderPositionArray(), gMechanicalOperationAndLoad.HomeBlockOffset)

      'If finding Eval Start and Stop at Runtime
      If gAmad.OutputAtEvaluationStart <> 0 And gAmad.OutputAtEvaluationStop <> 0 Then
        gAmad.EvaluationStart = FindEvaluationLocation(gAmad.OutputAtEvaluationStart)
        gAmad.EvaluationStart = Math.Round(gAmad.EvaluationStart, 3, MidpointRounding.AwayFromZero)
        If gAnomaly IsNot Nothing Then
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Error Finding Eval Start", gDatabase)
          Throw New TsopAnomalyException
        End If
        gAmad.EvaluationStop = FindEvaluationLocation(gAmad.OutputAtEvaluationStop)
        gAmad.EvaluationStop = Math.Round(gAmad.EvaluationStop, 3, MidpointRounding.AwayFromZero)
        If gAnomaly IsNot Nothing Then
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "Error Finding Eval Start", gDatabase)
          Throw New TsopAnomalyException
        End If
      End If

      'Forward DUT Output1 position based array
      Call CreatePositionBasedDataArray(True, gDataArrays.ForwardEncoderPositionArray, gDataArrays.ForwardDUTOut1PercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, gAmad.ForwardAveragingRangeHigh, gAmad.ForwardAveragingRangeLow, gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray)
      'Call CreateAdrPositionBasedDataArray(gDataArrays.ForwardEncoderPositionArray, gDataArrays.ForwardPercentVrefArray, gDataArrays.AdrPosEncoderFwd, gDataArrays.AdrPosDutOutPercentVrefFwd)

      'Forward Scan2 position based array '2.0.1.0 TER
      If gDataArrays.ForwardEncoderPositionArray2 IsNot Nothing Then
        Call CreatePositionBasedDataArray(True, gDataArrays.ForwardEncoderPositionArray2, gDataArrays.ForwardPercentVrefArray2, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, gAmad.ForwardAveragingRangeHigh, gAmad.ForwardAveragingRangeLow, gDataArrays.ForwardPositionBasedPercentVrefArray2)
      End If

      'Forward  Torque position based array
      If gControlFlags.TorqueScanEnabled = True Then
        Call CreatePositionBasedDataArray(True, gDataArrays.ForwardEncoderPositionArray, gDataArrays.ForwardTorqueValueArray, gAmad.TorqueEvaluationStart, gAmad.TorqueEvaluationStop, gAmad.TorqueEvaluationInterval, gAmad.ForwardAveragingRangeHigh, gAmad.ForwardAveragingRangeLow, gDataArrays.ForwardEvaluatedTorqueArray)
      End If

      'Linear Position based force array
      If gControlFlags.IsLinearPart And gControlFlags.ForceScanEnabled Then
        Call ScaleForceDataNewtons(gDataArrays.ForwardForceVoltageArray, gDataArrays.ForwardForceValueArray)
        'Call CreateLinearForwardPositionBasedForceDataArray() 'need to check this
        Call CreatePositionBasedDataArray(True, gDataArrays.ForwardEncoderPositionArray, gDataArrays.ForwardForceValueArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, gAmad.ForwardAveragingRangeHigh, gAmad.ForwardAveragingRangeLow, gDataArrays.ForwardPositionBasedForceArray)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      End If

      'If gblnENRScan = True And gintAnomaly = 0 Then
      If gControlFlags.ENRScanEnabled = True Then
        'Call CalcAddOffsetToArray(gDataArrays.ENREncoderPositionArray(), gMechanicalOperationAndLoad.HomeBlockOffset)
        Call CreatePositionBasedDataArray(True, gDataArrays.ENREncoderPositionArray, gDataArrays.ENRDUTOutValueArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, gAmad.ForwardAveragingRangeHigh, gAmad.ForwardAveragingRangeLow, gDataArrays.ForwardEvaluatedENRArray)
        Call CalcMultGainToArray(gDataArrays.ForwardEvaluatedENRArray, (1 / gsngENRCurrent))
      End If


      '==================================================
      'REVERSE 
      '==================================================

      If Not gControlFlags.ReverseScanOnly Then
        'Reverse DUT Output1 percent vref array
        Call CalcPercentVrefArray(gDataArrays.ReverseDUTOut1VoltageArray, gDataArrays.ReverseDUTVrefValueArray, gDataArrays.ReverseDUTOut1PercentVrefArray)
        If gDataArrays.ReverseEncoderPositionArray.Length <> gDataArrays.ReverseDUTOut1PercentVrefArray.Length Then
          If gDataArrays.ReverseEncoderPositionArray.Length > gDataArrays.ReverseDUTOut1PercentVrefArray.Length Then
            Call ResizeArraysToMatch(gDataArrays.ReverseEncoderPositionArray, gDataArrays.ReverseDUTOut1PercentVrefArray)
          End If
        End If

        'Reverse Scan2 percent vref array '2.0.1.0 TER
        If gDataArrays.ReverseDUTOutValueArray2 IsNot Nothing Then
          Call CalcPercentVrefArray(gDataArrays.ReverseDUTOutValueArray2, gDataArrays.ReverseDUTVrefValueArray2, gDataArrays.ReversePercentVrefArray2)
          If gDataArrays.ReverseEncoderPositionArray2.Length <> gDataArrays.ReversePercentVrefArray2.Length Then
            If gDataArrays.ReverseEncoderPositionArray2.Length > gDataArrays.ReversePercentVrefArray2.Length Then
              Call ResizeArraysToMatch(gDataArrays.ReverseEncoderPositionArray2, gDataArrays.ReversePercentVrefArray2)
            End If
          End If
        End If

        If gControlFlags.TorqueScanEnabled Then
          If gDataArrays.ReverseTorqueValueArray.Length > gDataArrays.ReverseEncoderPositionArray.Length Then
            Array.Resize(gDataArrays.ReverseTorqueValueArray, gDataArrays.ReverseEncoderPositionArray.Length)
          End If
        End If

        ''Add HomeBlockOffset to Reverse data points
        'Call CalcAddOffsetToArray(gDataArrays.ReverseEncoderPositionArray(), gMechanicalOperationAndLoad.HomeBlockOffset)

        'Reverse DUT Output1 Positon based array
        Call CreatePositionBasedDataArray(False, gDataArrays.ReverseEncoderPositionArray, gDataArrays.ReverseDUTOut1PercentVrefArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, gAmad.ReverseAveragingRangeHigh, gAmad.ReverseAveragingRangeLow, gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray)

        'Reverse Scan2 Positon based array '2.0.1.0 TER
        If gDataArrays.ReverseEncoderPositionArray2 IsNot Nothing Then
          Call CreatePositionBasedDataArray(False, gDataArrays.ReverseEncoderPositionArray2, gDataArrays.ReversePercentVrefArray2, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, gAmad.ReverseAveragingRangeHigh, gAmad.ReverseAveragingRangeLow, gDataArrays.ReversePositionBasedPercentVrefArray2)
        End If

        'Reverse Torque position based array
        If gControlFlags.TorqueScanEnabled = True Then
          'Calc Rev Torque
          Call CreatePositionBasedDataArray(False, gDataArrays.ReverseEncoderPositionArray, gDataArrays.ReverseTorqueValueArray, gAmad.TorqueEvaluationStart, gAmad.TorqueEvaluationStop, gAmad.TorqueEvaluationInterval, gAmad.ReverseAveragingRangeHigh, gAmad.ReverseAveragingRangeLow, gDataArrays.ReverseEvaluatedTorqueArray)
          'Calc Inv Rev Torque
          Call InvertArray(gDataArrays.ReverseEvaluatedTorqueArray, gDataArrays.InvReverseEvaluatedTorqueArray)
        End If
      Else
        gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray = gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray
        gDataArrays.ReverseEncoderPositionArray = gDataArrays.ForwardEncoderPositionArray
        gDataArrays.ReverseDUTOut1VoltageArray = gDataArrays.ForwardDUTOut1VoltageArray
      End If

      'Linear Position based force array
      If gControlFlags.IsLinearPart And gControlFlags.ForceScanEnabled Then
        Call ScaleForceDataNewtons(gDataArrays.ReverseForceVoltageArray, gDataArrays.ReverseForceValueArray)
        'Call CreateLinearForwardPositionBasedForceDataArray() 'need to check this
        Call CreatePositionBasedDataArray(False, gDataArrays.ReverseEncoderPositionArray, gDataArrays.ReverseForceValueArray, gAmad.EvaluationStart, gAmad.EvaluationStop, gAmad.EvaluationInterval, gAmad.ReverseAveragingRangeHigh, gAmad.ReverseAveragingRangeLow, gDataArrays.ReversePositionBasedForceArray)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      End If


      '==================================================
      'Other Arrays 
      '==================================================

      'Scan 1, X-Axis Array for Graphing results
      ReDim gDataArrays.XPositionArray(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray.Length - 1)
      For i = 0 To (gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray.Length - 1)
        'gDataArrays.XPositionArray(i) = gDataArrays.HysteresisPositionArray(i) ' * (gAmad.EvaluationInterval + gAmad.EvaluationStart) * gServoMotor.LinearDistanceConversion
        'gDataArrays.XPositionArray(i) = ((i * gAmad.EvaluationInterval) + gAmad.EvaluationStart) * gdblMotorPerParamSetUnitConversion + gdblXArrayOffsetServo
        gDataArrays.XPositionArray(i) = ((i * gAmad.EvaluationInterval) + gAmad.EvaluationStart) * gdblMotorPerParamSetUnitConversion
      Next

      ''Scan 2, X-Axis Array for Graphing results '2.0.1.0 TER
      'ReDim gDataArrays.XPositionArray2(gDataArrays.ForwardPositionBasedPercentVrefArray2.Length - 1)
      'For i = 0 To (gDataArrays.ForwardPositionBasedPercentVrefArray2.Length - 1)
      '  gDataArrays.XPositionArray2(i) = ((i * gAmad.EvaluationInterval) + gAmad.EvaluationStart) * gdblMotorPerParamSetUnitConversion
      'Next

      'Create Array for Graphing Torque results
      If gControlFlags.TorqueScanEnabled = True Then
        ReDim gDataArrays.TorqueXPositionArray(gDataArrays.ForwardEvaluatedTorqueArray.Length - 1)
        For i = 0 To (gDataArrays.ForwardEvaluatedTorqueArray.Length - 1)
          gDataArrays.TorqueXPositionArray(i) = i * gAmad.TorqueEvaluationInterval + gAmad.TorqueEvaluationStart
        Next
      End If

      'Scan 1, Create Array for Graphing Raw Timebased Encoder Data
      ReDim gDataArrays.ForwardEncoderTimeArray(gDataArrays.ForwardEncoderPositionArray.Length - 1)
      For i = 0 To (gDataArrays.ForwardEncoderTimeArray.Length - 1)
        If gControlFlags.UsesDcMotor Then
          'gDataArrays.ForwardEncoderTimeArray(i) = i * (1 / gDaqEncoder.SamplingRate)
        Else
          gDataArrays.ForwardEncoderTimeArray(i) = i * (1 / gDataAcquisitionSystem.SamplingRate)
        End If
      Next
      ldblScanTimeOffset = gDataArrays.ForwardEncoderTimeArray(gDataArrays.ForwardEncoderTimeArray.Length - 1)
      If Not gControlFlags.ReverseScanOnly Then
        ReDim gDataArrays.ReverseEncoderTimeArray(gDataArrays.ReverseEncoderPositionArray.Length - 1)
        For i = 0 To (gDataArrays.ReverseEncoderPositionArray.Length - 1)
          If gControlFlags.UsesDcMotor Then
            'gDataArrays.ReverseEncoderTimeArray(i) = i * (1 / gDaqEncoder.SamplingRate) + ldblScanTimeOffset
          Else
            gDataArrays.ReverseEncoderTimeArray(i) = i * (1 / gDataAcquisitionSystem.SamplingRate) + ldblScanTimeOffset
          End If
        Next
      Else
        gDataArrays.ReverseEncoderTimeArray = gDataArrays.ForwardEncoderTimeArray
      End If

      'Scan 2, Create Array for Graphing Raw Timebased Encoder Data '2.0.1.0 TER
      If gDataArrays.ForwardEncoderPositionArray2 IsNot Nothing Then
        ReDim gDataArrays.ForwardEncoderTimeArray2(gDataArrays.ForwardEncoderPositionArray2.Length - 1)
        For i = 0 To (gDataArrays.ForwardEncoderTimeArray2.Length - 1)
          If gControlFlags.UsesDcMotor Then
            'gDataArrays.ForwardEncoderTimeArray2(i) = i * (1 / gDaqEncoder.SamplingRate)
          Else
            gDataArrays.ForwardEncoderTimeArray2(i) = i * (1 / gDataAcquisitionSystem.SamplingRate)
          End If
        Next
        ldblScanTimeOffset = gDataArrays.ForwardEncoderTimeArray2(gDataArrays.ForwardEncoderTimeArray2.Length - 1)
        If Not gControlFlags.ReverseScanOnly Then
          ReDim gDataArrays.ReverseEncoderTimeArray2(gDataArrays.ReverseEncoderPositionArray2.Length - 1)
          For i = 0 To (gDataArrays.ReverseEncoderPositionArray2.Length - 1)
            If gControlFlags.UsesDcMotor Then
              'gDataArrays.ReverseEncoderTimeArray2(i) = i * (1 / gDaqEncoder.SamplingRate) + ldblScanTimeOffset
            Else
              gDataArrays.ReverseEncoderTimeArray2(i) = i * (1 / gDataAcquisitionSystem.SamplingRate) + ldblScanTimeOffset
            End If
          Next
        Else
          gDataArrays.ReverseEncoderTimeArray2 = gDataArrays.ForwardEncoderTimeArray2
        End If
      End If

      'Scan 1, Create Array for Graphing Raw Timebased Voltage Data
      ReDim gDataArrays.ForwardVoltageTimeArray(gDataArrays.ForwardDUTOut1VoltageArray.Length - 1)
      For i = 0 To (gDataArrays.ForwardVoltageTimeArray.Length - 1)
        If gControlFlags.UsesDcMotor Then
          'gDataArrays.ForwardVoltageTimeArray(i) = i * (1 / gDaqEncoder.SamplingRate)
        Else
          gDataArrays.ForwardVoltageTimeArray(i) = i * (1 / gDataAcquisitionSystem.SamplingRate)
        End If
      Next
      ldblScanTimeOffset = gDataArrays.ForwardVoltageTimeArray(gDataArrays.ForwardVoltageTimeArray.Length - 1)
      If Not gControlFlags.ReverseScanOnly Then
        ReDim gDataArrays.ReverseVoltageTimeArray(gDataArrays.ReverseDUTOut1VoltageArray.Length - 1)
        For i = 0 To (gDataArrays.ReverseVoltageTimeArray.Length - 1)
          If gControlFlags.UsesDcMotor Then
            'gDataArrays.ReverseVoltageTimeArray(i) = i * (1 / gDaqEncoder.SamplingRate) + ldblScanTimeOffset
          Else
            gDataArrays.ReverseVoltageTimeArray(i) = i * (1 / gDataAcquisitionSystem.SamplingRate) + ldblScanTimeOffset
          End If
        Next
      Else
        gDataArrays.ReverseVoltageTimeArray = gDataArrays.ForwardVoltageTimeArray
      End If

      'Scan 2, Create Array for Graphing Raw Timebased Voltage Data '2.0.1.0 TER
      If gDataArrays.ForwardDUTOutValueArray2 IsNot Nothing Then
        ReDim gDataArrays.ForwardVoltageTimeArray2(gDataArrays.ForwardDUTOutValueArray2.Length - 1)
        For i = 0 To (gDataArrays.ForwardVoltageTimeArray2.Length - 1)
          If gControlFlags.UsesDcMotor Then
            'gDataArrays.ForwardVoltageTimeArray2(i) = i * (1 / gDaqEncoder.SamplingRate)
          Else
            gDataArrays.ForwardVoltageTimeArray2(i) = i * (1 / gDataAcquisitionSystem.SamplingRate)
          End If
        Next
        ldblScanTimeOffset = gDataArrays.ForwardVoltageTimeArray2(gDataArrays.ForwardVoltageTimeArray2.Length - 1)
        If Not gControlFlags.ReverseScanOnly Then
          ReDim gDataArrays.ReverseVoltageTimeArray2(gDataArrays.ReverseDUTOutValueArray2.Length - 1)
          For i = 0 To (gDataArrays.ReverseVoltageTimeArray2.Length - 1)
            If gControlFlags.UsesDcMotor Then
              'gDataArrays.ReverseVoltageTimeArray2(i) = i * (1 / gDaqEncoder.SamplingRate) + ldblScanTimeOffset
            Else
              gDataArrays.ReverseVoltageTimeArray2(i) = i * (1 / gDataAcquisitionSystem.SamplingRate) + ldblScanTimeOffset
            End If
          Next
        Else
          gDataArrays.ReverseVoltageTimeArray2 = gDataArrays.ForwardVoltageTimeArray2
        End If
      End If

      ''Resize fwd and rev Arrays to same size
      'Scan 1
      'If gDataArrays.ForwardPositionBasedPercentVrefArray.Length <> gDataArrays.ReversePositionBasedPercentVrefArray.Length Then
      Call ResizeArraysToMatch(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray, gDataArrays.ReverseDUTOut1PositionBasedPercentVrefArray)
      'End If

      'Scan 2 '2.0.1.0 TER
      If gDataArrays.ForwardPositionBasedPercentVrefArray2 IsNot Nothing Then
        Call ResizeArraysToMatch(gDataArrays.ForwardPositionBasedPercentVrefArray2, gDataArrays.ReversePositionBasedPercentVrefArray2)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub CheckForAutoRetest()
    Try
      'Check if Re-Test is needed
      If gControlFlags.AutoRetestEnabled = True Then
        If gControlFlags.TestFailure = True Then
          'Test Failed
          If gintAutoRetestCount < gControlAndInterface.NumberOfFailureAutoReTests Then
            'Re-Test if retest count is less than the number allowed for failures
            gintAutoRetestTaskStep = gControlAndInterface.AutoRetestTaskStep
            gControlFlags.TestFailure = False
            gintAutoRetestCount += 1
          Else
            'Don't Re-Test if already retested number of times allowed for failures
            gintAutoRetestTaskStep = 0
          End If
        Else
          'Test Passed
          If gintAutoRetestCount = 0 Or gintAutoRetestCount >= gControlAndInterface.NumberOfPassedAutoReTests Then
            'Don't Re-Test
            gintAutoRetestTaskStep = 0
          ElseIf gintAutoRetestCount < gControlAndInterface.NumberOfPassedAutoReTests Then
            'Re-Test if retest count is less than the number allowed for passed
            gintAutoRetestTaskStep = gControlAndInterface.AutoRetestTaskStep
            gControlFlags.TestFailure = False
            gintAutoRetestCount += 1
          End If
        End If 'Test Failure Check
      End If 'Auto Retest Check

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub CheckForMasters()
    'for now do nothing
    Try
      ' If gblMasterMode 
      'gblMasterMode = gblMasterMode
      'gControlFlags.MasterMode = gControlFlags.MasterMode

      If gControlFlags.MasterMode Then
        gstrMasterNumber = InputBox("Please Enter Master Number")
        frmTsopMain.lblEncodedSerialNumber.Text = gstrMasterNumber
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ClearTestDataArrays()
    Try
      gDataArrays = New DataArrays

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub DisplayMetrics()
    'Dim dtGrid As DataTable '= gDatabase.GetMetricsDisplayGridsDataTable
    Dim dtRowInfo As DataTable
    Dim dtColInfo As DataTable
    Dim rowGrid As DataRow
    Dim rowRowInfo As DataRow
    Dim rowColInfo As DataRow
    Dim intRowNumber As Int16
    Dim intColNumber As Int16
    Dim MetricID As Guid
    Dim DomainMetricID As Guid
    Dim strDataFileMetricHeader As String
    Dim sGridName As String = ""
    Dim DomainType As String
    Dim DomainItem As String
    Dim strClassName As String
    Dim strPropertyName As String
    Dim lstrDomainlink As String
    Dim i As Integer
    Dim strErrorMessage As String
    Dim RemoveRowNumbers As New List(Of Int16)
    Dim NumRowsRemoved As Int16
    Dim intMetricRoundingDigits As Integer
    Try

      galDataFileMetricHeaders.Clear()
      galDataFileMetricValues.Clear()

      'each grid page
      For Each rowGrid In gdtDisplayGrids.Rows
        sGridName = rowGrid("DisplayGridName")
        'dtRowInfo = gDatabase.GetMetricsDisplayGridRowsDataTable(rowGrid("DisplayGridID"))
        dtRowInfo = GetDisplayGridRows(rowGrid.Item("DisplayGridID"))
        'each row of grid
        NumRowsRemoved = 0
        For Each rowRowInfo In dtRowInfo.Rows
          intRowNumber = rowRowInfo("RowNumber") - NumRowsRemoved
          If rowRowInfo("RowType") = "Metric" Then
            MetricID = New Guid(rowRowInfo("FunctionMetricID").ToString)
            If gMetrics(MetricID).Enabled Then
              DomainType = rowRowInfo("DomainType")
              'dtColInfo = gDatabase.GetMetricsDisplayGridColumnsDataTable(rowGrid("DisplayGridID"))
              dtColInfo = GetDisplayGridColumns(rowGrid.Item("DisplayGridID"))

              'Get Rounding Digits
              If gMetrics(MetricID).RoundingDigits <> -1 Then
                intMetricRoundingDigits = gMetrics(MetricID).RoundingDigits
              Else
                intMetricRoundingDigits = 3
              End If

              For Each rowColInfo In dtColInfo.Rows
                intColNumber = rowColInfo("ColumnNumber")
                Select Case rowColInfo("ColumnType")
                  Case "MetricValue"
                    'Add header for data file
                    strDataFileMetricHeader = gMetrics(MetricID).MetricName
                    If gMetrics(MetricID).UnitsOfMeasure <> "None" Then
                      strDataFileMetricHeader = strDataFileMetricHeader & " (" & gMetrics(MetricID).UnitsOfMeasure & ")"
                    End If
                    galDataFileMetricHeaders.Add(strDataFileMetricHeader)

                    'Add value for data file
                    If gMetrics(MetricID).MetricType = Metric.MetricTypeEnum.Alpha Then
                      galDataFileMetricValues.Add(gMetrics(MetricID).AlphaMetricValue)
                    Else
                      galDataFileMetricValues.Add(Math.Round(gMetrics(MetricID).MetricValue, intMetricRoundingDigits, MidpointRounding.AwayFromZero))
                    End If

                    Select Case DomainType
                      Case "MetricParameter"
                        DomainMetricID = Nothing
                        lstrDomainlink = " at "
                        DomainMetricID = New Guid(rowRowInfo("DomainFunctionMetricID").ToString)
                        If gMetrics(DomainMetricID).MetricType = Metric.MetricTypeEnum.Alpha Then
                          DomainItem = gMetrics(DomainMetricID).AlphaMetricValue
                        Else
                          DomainItem = Math.Round(gMetrics(DomainMetricID).MetricValue, 3, MidpointRounding.AwayFromZero)
                        End If

                        'Add header for data file
                        strDataFileMetricHeader = gMetrics(DomainMetricID).MetricName
                        If gMetrics(DomainMetricID).UnitsOfMeasure <> "None" Then
                          strDataFileMetricHeader = strDataFileMetricHeader & " (" & gMetrics(DomainMetricID).UnitsOfMeasure & ")"
                        End If
                        galDataFileMetricHeaders.Add(strDataFileMetricHeader)

                        'Add value for data file
                        galDataFileMetricValues.Add(DomainItem)

                        DomainItem = DomainItem & gMetrics(DomainMetricID).UnitsOfMeasure
                      Case "NonMetricParameter"
                        lstrDomainlink = " at "
                        strClassName = rowRowInfo("ClassName")
                        strPropertyName = rowRowInfo("PropertyName")
                        DomainItem = GetClassPropertyInstanceValue(strClassName, strPropertyName)

                        'Add header for data file
                        strDataFileMetricHeader = strPropertyName
                        If rowRowInfo("UnitsOfMeasure") <> "None" And rowRowInfo("UnitsOfMeasure") <> "" Then
                          strDataFileMetricHeader = strDataFileMetricHeader & " (" & rowRowInfo("UnitsOfMeasure") & ")"
                        End If
                        galDataFileMetricHeaders.Add(strDataFileMetricHeader)

                        'Add value for data file
                        galDataFileMetricValues.Add(DomainItem)

                        DomainItem = DomainItem & rowRowInfo("UnitsOfMeasure")
                      Case "CurrentMetricLocation"
                        'DomainMetricID = Nothing
                        lstrDomainlink = " at "
                        'DomainMetricID = New Guid(rowRowInfo("DomainFunctionMetricID").ToString)
                        DomainItem = Math.Round(gMetrics(MetricID).MetricLocation, 3, MidpointRounding.AwayFromZero)

                        'Add header for data file
                        strDataFileMetricHeader = gMetrics(MetricID).MetricName & "Location"
                        If gMetrics(MetricID).DomainUnitsOfMeasure <> "None" Then
                          strDataFileMetricHeader = strDataFileMetricHeader & " (" & gMetrics(MetricID).DomainUnitsOfMeasure & ")"
                        End If
                        galDataFileMetricHeaders.Add(strDataFileMetricHeader)

                        'Add value for data file
                        galDataFileMetricValues.Add(DomainItem)

                        DomainItem = DomainItem & gMetrics(MetricID).DomainUnitsOfMeasure
                      Case Else
                        DomainItem = ""
                        lstrDomainlink = ""
                    End Select

                    Dim sMetricValue As String
                    'dlg ask tim
                    'If gMetrics(DomainMetricID).MetricType = Metric.MetricTypeEnum.Alpha Then
                    If gMetrics(MetricID).MetricType = Metric.MetricTypeEnum.Alpha Then
                      sMetricValue = gMetrics(MetricID).AlphaMetricValue
                    Else
                      sMetricValue = Math.Round(gMetrics(MetricID).MetricValue, intMetricRoundingDigits, MidpointRounding.AwayFromZero)
                    End If

                    If gMetrics(MetricID).UnitsOfMeasure <> "None" Then
                      sMetricValue = sMetricValue & gMetrics(MetricID).UnitsOfMeasure
                    End If
                    sMetricValue = sMetricValue & lstrDomainlink & DomainItem
                    gstrMetricValue(i) = sMetricValue
                    i = i + 1
                    gintMetricRows = i
                    gDataGrids(sGridName)(intColNumber, intRowNumber).Value = sMetricValue

                  Case "PassFailStatus"
                    If gMetrics(MetricID).PassFailStatus = Metric.PassFailStatusEnum.Good Then
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Style.BackColor = Color.Lime
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Value = "Pass"
                    ElseIf gMetrics(MetricID).PassFailStatus = Metric.PassFailStatusEnum.ReportOnly Then
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Style.BackColor = Color.White
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Value = "Report Only"
                    ElseIf gMetrics(MetricID).PassFailStatus = Metric.PassFailStatusEnum.NoData Then
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Style.BackColor = Color.White
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Value = "No Data"
                    Else
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Style.BackColor = Color.Red
                      gDataGrids(sGridName)(intColNumber, intRowNumber).Value = "Reject"
                    End If
                  Case "FailHighCount"
                    gDataGrids(sGridName)(intColNumber, intRowNumber).Value = gMetrics(MetricID).Statistics.FailHighCount
                  Case "FailLowCount"
                    gDataGrids(sGridName)(intColNumber, intRowNumber).Value = gMetrics(MetricID).Statistics.FailLowCount
                  Case "AVGValue"
                    gDataGrids(sGridName)(intColNumber, intRowNumber).Value = Math.Round(gMetrics(MetricID).Statistics.Avg, 2, MidpointRounding.AwayFromZero)
                  Case "STDValue"
                    gDataGrids(sGridName)(intColNumber, intRowNumber).Value = Math.Round(gMetrics(MetricID).Statistics.StDev, 5, MidpointRounding.AwayFromZero)
                  Case "CPKValue"
                  Case "CPValue"
                  Case "MaxValue"
                    gDataGrids(sGridName)(intColNumber, intRowNumber).Value = Math.Round(gMetrics(MetricID).Statistics.Max, 2, MidpointRounding.AwayFromZero)
                  Case "MinValue"
                    gDataGrids(sGridName)(intColNumber, intRowNumber).Value = Math.Round(gMetrics(MetricID).Statistics.Min, 2, MidpointRounding.AwayFromZero)
                End Select
              Next
            Else
              'gDataGrids(sGridName).Rows.RemoveAt(intRowNumber)
              'RemoveRowNumbers.Add(rowRowInfo.Item("RowNumber"))
              NumRowsRemoved += 1
            End If
          ElseIf rowRowInfo("RowType") = "Seperator" Then
            dtColInfo = gDatabase.GetMetricsDisplayGridColumnsDataTable(rowGrid("DisplayGridID"))
            For Each rowColInfo In dtColInfo.Rows
              intColNumber = rowColInfo("ColumnNumber")
              gDataGrids(sGridName)(intColNumber, intRowNumber).Style.BackColor = Color.LightGray
            Next
          End If
        Next
        'NumRowsRemoved = 0
        'For Each intRowNumber In RemoveRowNumbers
        '  gDataGrids(sGridName).Rows.RemoveAt(intRowNumber - NumRowsRemoved)
        '  NumRowsRemoved += 1
        'Next
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      strErrorMessage = ex.Message & vbCrLf &
        sGridName & " col " & intColNumber & "row " & intRowNumber & ex.Message
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
    End Try
  End Sub


  Public Sub EnableOperatorTouchStartTimer()
    Try
      gtmrOperatorTouchStartTimer.Enabled = True

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub EvaluateMetrics()

    Dim metMetrics As New Dictionary(Of Guid, Metric)

    Try

      Call FilterMetricsOnTaskListItem(metMetrics, gFilteredTaskList(gintCurrentTask).Item("TaskIndex"))
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      Call ProcessMetrics(metMetrics)
      If Not gAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If



    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub ExecuteTask(ByVal row As DataRow)
    Dim strModuleName As String
    Dim strMethodName As String
    Dim mfType As Type
    Dim mfMethod As MethodInfo

		Try
			strModuleName = gstrAssemblyName & "." & row.Item("ModuleName")
			strMethodName = row.Item("MethodName")

			mfType = Type.GetType(strModuleName)
			mfMethod = mfType.GetMethod(strMethodName)

			mfMethod.Invoke(Nothing, Nothing)
			If gAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

		Catch ex As TsopAnomalyException
			gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
		Catch ex As Exception
			gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ExecuteTaskList(ByVal intTaskListFilter As TaskListFilterEnum)
    Dim intTaskCount As Integer
    Dim blnTaskListComplete As Boolean
    Try

      gtmrLotShift.Stop()

      frmTsopMain.Cursor = Cursors.WaitCursor

      frmTsopMain.StatusLabel1.Text = "Test In Progress"
      LogActivity(frmTsopMain.StatusLabel1.Text)

      'Only filter on first time, or refilter if it has been changed
      If gUserTaskListFilter <> intTaskListFilter Or gdtFilteredTaskList.Rows.Count = 0 Then
        Call FilterTaskList(intTaskListFilter)
        If gAnomaly Is Nothing Then
          gUserTaskListFilter = intTaskListFilter
        Else
          gUserTaskListFilter = TaskListFilterEnum.None
          Throw New TsopAnomalyException
        End If
      End If

      gControlFlags.AbortTest = False

      'gintAutoRetestCount = 0
      gTestStartTime = DateTime.Now
      blnTaskListComplete = False
      intTaskCount = gdtFilteredTaskList.Rows.Count
      gintCurrentTask = 1

      Do
        If Not gControlFlags.AbortTest Then
          'frmTsopMain.StatusLabel1.Text = "Executing Task Step: " & intCurrentTask & ", " & gFilteredTaskList(intCurrentTask).Item("MethodName")
          frmTsopMain.StatusLabel2.Text = "Task " & gintCurrentTask & " of: " & intTaskCount & " " & gFilteredTaskList(gintCurrentTask).Item("MethodName")

          LogActivity(frmTsopMain.StatusLabel2.Text)

          System.Windows.Forms.Application.DoEvents()

          Call ExecuteTask(gFilteredTaskList(gintCurrentTask))
          If Not (gAnomaly Is Nothing) Then
            Throw New TsopAnomalyException
          End If

          If gControlFlags.AbortTaskList Then
            gControlFlags.AbortTaskList = False
            Exit Do
          End If
          If gintAutoRetestTaskStep <> 0 Then
              gintCurrentTask = gintAutoRetestTaskStep
              gintAutoRetestTaskStep = 0
            ElseIf gintNextTaskStep <> 0 Then
              gintCurrentTask = gintNextTaskStep
              gintNextTaskStep = 0
            Else
              gintCurrentTask += 1
              If gintCurrentTask > gFilteredTaskList.Keys.Count Then
                blnTaskListComplete = True
              End If
            End If
          Else
            blnTaskListComplete = True
        End If
      Loop Until blnTaskListComplete

      gintAutoRetestCount = 0

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      'Call LogAnomaly()
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      'Call LogAnomaly()

    Finally
      If gAnomaly IsNot Nothing Then
        glstAnomaly.Add(gAnomaly)
        gAnomaly = Nothing
        'gControlFlags.TestFailure = True
        gControlFlags.TestError = True

        'If (gDaqEncoder_PositionMonitor.CurrentPosition > gMechanicalOperationAndLoad.LoadLocation + 0.2) _
        ' Or (gDaqEncoder_PositionMonitor.CurrentPosition < gMechanicalOperationAndLoad.LoadLocation - 0.2) Then

        '  Call SetupServoLoadLocationMove()
        '  If gAnomaly IsNot Nothing Then
        '    gAnomaly = Nothing
        '  End If

        '  Call ServoMove()
        '  If gAnomaly IsNot Nothing Then
        '    gAnomaly = Nothing
        '  End If
        'End If

        Call LogActivity("Test Results: Test Failure= " & gControlFlags.TestFailure & " Test Error=" & gControlFlags.TestError)
        If gAnomaly IsNot Nothing Then
          gAnomaly = Nothing
        End If

      End If

      frmTsopMain.StatusLabel1.Text = "Test Complete at " & Now & " ... Ready For Next Part"
      LogActivity(frmTsopMain.StatusLabel1.Text)
      If gAnomaly IsNot Nothing Then
        gAnomaly = Nothing
      End If

      gCycleTimeStopWatch.Stop()
      frmTsopMain.StatusLabel2.Text = "Test Complete at " & Now & " ... Ready For Next Part " & gCycleTimeStopWatch.ElapsedMilliseconds / 1000 & " Secs."
      LogActivity(frmTsopMain.StatusLabel2.Text)
      If gAnomaly IsNot Nothing Then
        gAnomaly = Nothing
      End If

      If gControlFlags.UsesOverviewScreen Then
        gOverviewScreenControls(OverviewScreenControls.CycleTime).Text = gCycleTimeStopWatch.ElapsedMilliseconds / 1000
        gOverviewProgressBar.Value = 100
        gOverviewProgressLabel.Text = "100 % Complete"
        gtmrUpdateStatusBarTimer.Enabled = False
      End If

      gControlFlags.TestInProgress = False
      gtmrLotShift.Start()
      frmTsopMain.Cursor = Cursors.Default
    End Try

  End Sub

  Public Sub ExecuteTaskListParamSetInit(ByVal intTaskListFilter As TaskListFilterEnum)
    Dim strErrorMessage As String = ""
    'Dim intCurrentTask As Integer
    Dim intTaskCount As Integer
    Dim blnTaskListComplete As Boolean
    Try

      frmTsopMain.Cursor = Cursors.WaitCursor

      frmTsopMain.StatusLabel1.Text = "Initializing Program From Parameter Set"
      LogActivity(frmTsopMain.StatusLabel1.Text)

      'Only filter on first time, or refilter if it has been changed
      If gUserTaskListFilter <> intTaskListFilter Or gdtFilteredTaskList.Rows.Count = 0 Then
        Call FilterTaskList(intTaskListFilter)
        If gAnomaly Is Nothing Then
          gUserTaskListFilter = intTaskListFilter
        Else
          gUserTaskListFilter = TaskListFilterEnum.None
          Throw New TsopAnomalyException
        End If
      End If

      gControlFlags.AbortTest = False

      blnTaskListComplete = False

      intTaskCount = gdtFilteredTaskList.Rows.Count
      gintCurrentTask = 1

      Do
        If Not gControlFlags.AbortTest Then

          frmTsopMain.StatusLabel2.Text = "Task " & gintCurrentTask & " of: " & intTaskCount & " " & gFilteredTaskList(gintCurrentTask).Item("MethodName")

          LogActivity(frmTsopMain.StatusLabel2.Text)

          System.Windows.Forms.Application.DoEvents()

          Call ExecuteTask(gFilteredTaskList(gintCurrentTask))
          If Not (gAnomaly Is Nothing) Then
            Throw New TsopAnomalyException
          End If

          gintCurrentTask += 1
          If gintCurrentTask > gFilteredTaskList.Keys.Count Then
            blnTaskListComplete = True
          End If
        Else
          blnTaskListComplete = True
        End If
      Loop Until blnTaskListComplete

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
      frmTsopMain.Cursor = Cursors.Default
    End Try

  End Sub

  Public Sub ExecuteTaskListTsopEnd(ByVal intTaskListFilter As TaskListFilterEnum)
    Dim strErrorMessage As String = ""
    'Dim intCurrentTask As Integer
    Dim intTaskCount As Integer
    Dim blnTaskListComplete As Boolean
    Try

      frmTsopMain.Cursor = Cursors.WaitCursor

      frmTsopMain.StatusLabel1.Text = "Ending Test Station Operation Program"
      LogActivity(frmTsopMain.StatusLabel1.Text)

      'Only filter on first time, or refilter if it has been changed
      If gUserTaskListFilter <> intTaskListFilter Or gdtFilteredTaskList.Rows.Count = 0 Then
        Call FilterTaskList(intTaskListFilter)
        If gAnomaly Is Nothing Then
          gUserTaskListFilter = intTaskListFilter
        Else
          gUserTaskListFilter = TaskListFilterEnum.None
          Throw New TsopAnomalyException
        End If
      End If

      gControlFlags.AbortTest = False

      blnTaskListComplete = False

      intTaskCount = gdtFilteredTaskList.Rows.Count
      gintCurrentTask = 1

      Do
        If Not gControlFlags.AbortTest Then

          frmTsopMain.StatusLabel2.Text = "Task " & gintCurrentTask & " of: " & intTaskCount & " " & gFilteredTaskList(gintCurrentTask).Item("MethodName")

          LogActivity(frmTsopMain.StatusLabel2.Text)

          System.Windows.Forms.Application.DoEvents()

          Call ExecuteTask(gFilteredTaskList(gintCurrentTask))
          If Not (gAnomaly Is Nothing) Then
            Throw New TsopAnomalyException
          End If

          gintCurrentTask += 1
          If gintCurrentTask > gFilteredTaskList.Keys.Count Then
            blnTaskListComplete = True
          End If
        Else
          blnTaskListComplete = True
        End If
      Loop Until blnTaskListComplete

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
      frmTsopMain.Cursor = Cursors.Default
    End Try

  End Sub

  Public Sub ExecuteTaskListTsopInit()
    Dim strErrorMessage As String = ""
    'Dim intCurrentTask As Integer
    Dim intTaskCount As Integer
    Dim blnTaskListComplete As Boolean
    Dim drTaskListRow As DataRow
    Dim intKey As Integer
    Try

      frmTsopMain.StatusLabel1.Text = "Initializing Test Station Operation Program"
      LogActivity(frmTsopMain.StatusLabel1.Text)

      ''Only filter on first time, or refilter if it has been changed
      'If gUserTaskListFilter <> intTaskListFilter Or gdtFilteredTaskList.Rows.Count = 0 Then
      'Call FilterTaskList(intTaskListFilter)
      '  If gAnomaly Is Nothing Then
      '    gUserTaskListFilter = intTaskListFilter
      '  Else
      '    gUserTaskListFilter = TaskListFilterEnum.None
      '    Throw New TsopAnomalyException
      '  End If
      'End If

      gFilteredTaskList.Clear()
      intKey = 1
      For Each drTaskListRow In gdtTsopInitTaskList.Rows
        gFilteredTaskList.Add(intKey, drTaskListRow)
        intKey += 1
      Next

      gControlFlags.AbortTest = False

      blnTaskListComplete = False

      intTaskCount = gdtTsopInitTaskList.Rows.Count
      gintCurrentTask = 1

      Do
        If Not gControlFlags.AbortTest Then

          frmTsopMain.StatusLabel2.Text = "Task " & gintCurrentTask & " of: " & intTaskCount & " " & gFilteredTaskList(gintCurrentTask).Item("MethodName")

          LogActivity(frmTsopMain.StatusLabel2.Text)

          System.Windows.Forms.Application.DoEvents()

          Call ExecuteTask(gFilteredTaskList(gintCurrentTask))
          If Not (gAnomaly Is Nothing) Then
            Throw New TsopAnomalyException
          End If

          gintCurrentTask += 1
          If gintCurrentTask > gFilteredTaskList.Keys.Count Then
            blnTaskListComplete = True
          End If
        Else
          blnTaskListComplete = True
        End If
      Loop Until blnTaskListComplete

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub FilterTaskList(ByVal intTaskListFilter As TaskListFilterEnum)
    Dim intKey As Integer
    Dim drTaskListRow As DataRow

    Try
      gdtFilteredTaskList.Clear()
      Select Case intTaskListFilter
        Case TaskListFilterEnum.Program
          Call GetTaskListSection(TaskListSectionEnum.INITIAL.ToString)
          Call GetTaskListSection(TaskListSectionEnum.PROG.ToString)
        Case TaskListFilterEnum.Test
          Call GetTaskListSection(TaskListSectionEnum.INITIAL.ToString)
          Call GetTaskListSection(TaskListSectionEnum.TEST.ToString)
        Case TaskListFilterEnum.ProgramAndTest
          Call GetTaskListSection(TaskListSectionEnum.INITIAL.ToString)
          Call GetTaskListSection(TaskListSectionEnum.PROG.ToString)
          Call GetTaskListSection(TaskListSectionEnum.TEST.ToString)
        Case TaskListFilterEnum.ParamSetInit
          Call GetTaskListSection(TaskListSectionEnum.PARAMSETINIT.ToString)
        Case TaskListFilterEnum.TsopEnd
          Call GetTaskListSection(TaskListSectionEnum.TSOPEND.ToString)
      End Select

      gFilteredTaskList.Clear()
      intKey = 1
      For Each drTaskListRow In gdtFilteredTaskList.Rows
        gFilteredTaskList.Add(intKey, drTaskListRow)
        intKey += 1
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub FindCurveKneeHigh() ' 3.1.0.2 TER
    Dim intLoop As Integer
    Dim intStartIndex As Integer
    Dim dblIdealSlope As Double
    Dim dblSlope As Double
    Dim dblSlopeAtKnee As Double
    Dim dblKneePosition As Double
    Try

      dblIdealSlope = gAmad.IdealSlope
      dblSlopeAtKnee = dblIdealSlope * 0.3

      intStartIndex = CInt(gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray.Length * 0.7)

      dblKneePosition = 0
      For intLoop = intStartIndex To gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray.Length - 1
        dblSlope = (gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray(intLoop) - gDataArrays.ForwardDUTOut1PositionBasedPercentVrefArray(intLoop - 1)) _
                    / (gDataArrays.XPositionArray(intLoop) - gDataArrays.XPositionArray(intLoop - 1))

        If dblSlope <= dblSlopeAtKnee Then
          If intLoop = intStartIndex Then
            dblKneePosition = 0
          Else
            dblKneePosition = gDataArrays.XPositionArray(intLoop - 1)
          End If
          Exit For
        End If
      Next
      gAmad.KneePositionHigh = dblKneePosition
      'Console.WriteLine(dblKneePosition)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub



  Public Sub GetADRStructure()
    Try

      'ADR DB Setup
      gControlFlags.UsesADRDatabase = gControlAndInterface.UsesADRDatabase
      If gControlFlags.UsesADRDatabase Then
        'clear ADR Channels
        gAdrChannels.Clear()

        'Get ADRStructureID
        Call gDatabase.GetAdrStructureID(gAmad.ADRStructureVersion)
        If gDatabase.Anomaly IsNot Nothing Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
        If gDatabase.ADRStructureID = Guid.Empty Then
          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, "ADR Structure Record Not Found in Database", gDatabase)
          Throw New TsopAnomalyException
        End If

        'Get ADR Conversion Method Arguments
        Call GetADRChannelProperties()
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetDataFilePaths()
    Try
      gstrDataFilePath = gstrDataFileBasePath & gControlAndInterface.SeriesID & gstrDataFileSubFolderPath
      gstrProgDataFilePath = gstrDataFileBasePath & gControlAndInterface.SeriesID & gstrProgFileSubFolderPath

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetDaqDataArrayConfig() ' 2.0.1.0 TER
    Dim strFilter As String
    Try

      ''Set filter f
      strFilter = "" '"GraphGroup = " & """" & gControlAndInterface.GraphGroup & """"

      'Clear  tabl
      gdtDaqDataArrayConfig.Clear()
      'Read graph definitions from file into data table
      'gdtDaqDataArrayConfig = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, gstrDaqDataArrayConfigFile, strFilter)
      gdtDaqDataArrayConfig = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrDaqDataArrayConfigFile, strFilter)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub GetDeviceInProcess() '(ByVal strExternalSerialNumber)
    Try
      gDeviceInProcess.GetDipID()
      If gDeviceInProcess.Anomaly IsNot Nothing Then
        gAnomaly = gDeviceInProcess.Anomaly
        gDeviceInProcess.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetDynamicSetupInfoItems()
    Try

      gstrSetupInfoItems = gDatabase.GetParameterArrayStringArray("SetupInfoItemList", "SetupInfoItemList")
      If Not gDatabase.Anomaly Is Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If
      If gstrSetupInfoItems.Length > 0 Then
        gControlFlags.UsesDynamicSetupInfo = True
      Else
        gControlFlags.UsesDynamicSetupInfo = False
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetGraphList()
    Try

      gstrGraphList = gDatabase.GetParameterArrayStringArray("GraphList", "GraphList")
      If Not gDatabase.Anomaly Is Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetGraphConfig()
    Dim strFilter As String
    Try

      'Set filter for graph definitions based on defined group
      'strFilter = "GraphGroup = " & """" & gControlAndInterface.GraphGroup & """"
      strFilter = "GraphGroup = " & gControlAndInterface.GraphGroup

      'Clear graph definitions table
      gdtGraphDefinitions.Clear()
      'Read graph definitions from file into data table
      'gdtGraphDefinitions = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, gstrGraphDefinitionFile, strFilter)
      gdtGraphDefinitions = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrGraphDefinitionFile, strFilter)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      'Clear graph plots table
      gdtGraphPlotDefinitions.Clear()
      'Read graph plot definitions from file into data table
      'gdtGraphPlotDefinitions = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, gstrGraphPlotDefinitionFile, strFilter)
      gdtGraphPlotDefinitions = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrGraphPlotDefinitionFile, strFilter)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      'Clear graph axes table ' 3.1.0.1 TER
      gdtGraphAxesDefinitions.Clear()
      'Read graph axes definitions from file into data table
      'gdtGraphAxesDefinitions = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, gstrGraphAxesDefinitionFile, strFilter)
      gdtGraphAxesDefinitions = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrGraphAxesDefinitionFile, strFilter)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub GetIncrementalSerialNumbers()
    Try
      gDatabase.GetPreviousSerialNumbers(gstrLotName, gintSerialNumberInitial, gintSerialNumberFinal)
      If Not gDatabase.Anomaly Is Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetMenuDefinitions()
    Dim strFilter As String
    Try

      'Set filter for printed report definitions based on defined group
      'strFilter = "ReportGroup = " & """" & gControlAndInterface.ReportGroup & """"
      'strFilter = "MenuGroup = " & """" & "Sensors" & """"
      strFilter = "MenuGroup = " & gControlAndInterface.MenuGroup

      'Clear printed report definitions table
      gdtMenuDefinitions.Clear()
      'Read printed report definitions from file into data table
      'gdtMenuDefinitions = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, gstrMenuDefinitionsFile, strFilter)
      gdtMenuDefinitions = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrMenuDefinitionsFile, strFilter)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub GetMetricPropertiesTableFromDatabase()
    Try

      gdtMetricProperties.Clear()
      gdtMetricProperties = gDatabase.GetMetricPropertiesDataTable
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub GetMetricReportTable()
    Try

      'Metric list for report
      gdtMetricReportTable = gDatabase.GetMetricListDataTableForReport()
      If Not gDatabase.Anomaly Is Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetParameterReportTable()
    Try

      'Parameter list for Report
      gdtParameterReportTable = gDatabase.GetParameterListDataTableForReport()
      If Not gDatabase.Anomaly Is Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetPrintedReportDefinitions() ' 3.1.0.1 TER
    'Dim strFilter As String
    Try

      'Set filter for printed report definitions based on defined group
      'strFilter = "ReportGroup = " & """" & gControlAndInterface.ReportGroup & """"

      'Clear printed report definitions table
      gdtPrintedReportDefinitions.Clear()
      'Read printed report definitions from file into data table
      'gdtPrintedReportDefinitions = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, gstrPrintedReportDefinitionsFile)
      gdtPrintedReportDefinitions = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrPrintedReportDefinitionsFile)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub GetTsopModeID()
    Dim bTorque As Boolean
    Dim bTest As Boolean
    Dim bENR As Boolean
    Dim intTest As Int32
    Dim intTorque As Int32
    Dim intENR As Int32
    Dim intTsopModeValue As Int32

    Try
      'Set these equal to global flags
      bTorque = gControlFlags.TorqueScanEnabled
      'bTest = gblnStartScan
      bTest = gControlFlags.StartScan
      'bENR = gblnENRScan
      bENR = gControlFlags.ENRScanEnabled

      intTest = Math.Abs(CInt(bTest)) * gTsopFunctionValues("Functional Test")
      'intTorque = Math.Abs(CInt(bTorque)) * gTsopFunctionValues("Test Torque Versus Position")
      'intENR = Math.Abs(CInt(bENR)) * gTsopFunctionValues("Test ENR")

      intTsopModeValue = intTorque + intTest + intENR

      gDatabase.TsopModeID = gTsopModeValues(intTsopModeValue)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  'Public Sub GraphTestResults()
  '  Try
  '    'If gControlFlags.IsLinearPart Then
  '    '  Call modMain.GraphLinearResults()
  '    'Else
  '    Call GraphResults()
  '    If Not (gAnomaly Is Nothing) Then
  '      Throw New TsopAnomalyException
  '    End If
  '    'End If

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub InitializeDefaultTabPageMenu() ' 3.0.0.0 TER
    Dim miMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim myForm As frmTsopMain
    Dim myTabPage As TabPage
    Dim blnDefaultPageFound As Boolean
    Try

      frmTsopMain.mnuOptionsDefaultTabPage.DropDownItems.Clear()

      myForm = My.Application.OpenForms("frmTsopMain")
      blnDefaultPageFound = False
      For Each myTabPage In myForm.tabResults.TabPages
        ''Create new menu item initially
        miMenuItem = New System.Windows.Forms.ToolStripMenuItem
        miMenuItem.Enabled = True
        miMenuItem.CheckOnClick = True
        miMenuItem.Text = myTabPage.Name

        If Not blnDefaultPageFound Then
          If gstrDefaultTabPage = miMenuItem.Text Then
            miMenuItem.Checked = True
            blnDefaultPageFound = True
          End If
        End If

        'AddHandler miMenuItem.CheckStateChanged, AddressOf mnuOptionDefaultTabPageSelection_Click
        AddHandler miMenuItem.Click, AddressOf frmTsopMain.mnuOptionDefaultTabPageSelection_Click

        'add to drop down menu
        frmTsopMain.mnuOptionsDefaultTabPage.DropDownItems.Add(miMenuItem)
      Next

      If blnDefaultPageFound = False Or gstrDefaultTabPage Is Nothing Then
        miMenuItem = frmTsopMain.mnuOptionsDefaultTabPage.DropDownItems(1)
        miMenuItem.Checked = True
        gstrDefaultTabPage = miMenuItem.Text
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub InitializeDisplayGrids()
    Dim rowGrid As DataRow

    Try

      gdtDisplayGrids = gDatabase.GetDisplayGridsDataTable()
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gdtDisplayGridRows = gDatabase.GetDisplayGridRowsDataTable()
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gdtDisplayGridColumns = gDatabase.GetDisplayGridColumnsDataTable()
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gDataGrids.Clear()
      For Each rowGrid In gdtDisplayGrids.Rows
        Call InitializeGrid(rowGrid)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InitializeExternalTestMethodMenu()
    Dim dtBefore As DataTable
    Dim dtDuring As DataTable
    Dim dtAfter As DataTable
    Try

      dtBefore = gDatabase.GetExternalTestMethodsDataTable
      dtDuring = gDatabase.GetExternalTestMethodsDataTable
      dtAfter = gDatabase.GetExternalTestMethodsDataTable

      mnuExternalTestMethodsBeforeComboBox.ComboBox.BindingContext = frmTsopMain.BindingContext
      mnuExternalTestMethodsBeforeComboBox.ComboBox.DataSource = dtBefore
      mnuExternalTestMethodsBeforeComboBox.ComboBox.ValueMember = dtBefore.Columns("TestMethodID").ToString
      mnuExternalTestMethodsBeforeComboBox.ComboBox.DisplayMember = dtBefore.Columns("TestMethodName").ToString

      mnuExternalTestMethodsDuringComboBox.ComboBox.BindingContext = frmTsopMain.BindingContext
      mnuExternalTestMethodsDuringComboBox.ComboBox.DataSource = dtDuring
      mnuExternalTestMethodsDuringComboBox.ComboBox.ValueMember = dtDuring.Columns("TestMethodID").ToString
      mnuExternalTestMethodsDuringComboBox.ComboBox.DisplayMember = dtDuring.Columns("TestMethodName").ToString

      mnuExternalTestMethodsAfterComboBox.ComboBox.BindingContext = frmTsopMain.BindingContext
      mnuExternalTestMethodsAfterComboBox.ComboBox.DataSource = dtAfter
      mnuExternalTestMethodsAfterComboBox.ComboBox.ValueMember = dtAfter.Columns("TestMethodID").ToString
      mnuExternalTestMethodsAfterComboBox.ComboBox.DisplayMember = dtAfter.Columns("TestMethodName").ToString

      mnuExternalTestMethodsNone.CheckState = CheckState.Checked
      mnuExternalTestMethodsBefore.CheckState = CheckState.Unchecked
      mnuExternalTestMethodsDuring.CheckState = CheckState.Unchecked
      mnuExternalTestMethodsAfter.CheckState = CheckState.Unchecked

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InitializeLot()
    Try

      gstrLotName = BuildLotName()
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      Call GetLotNames(gstrLotName, False)
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      Call frmTsopMain.ConfigureLotNamesMenu()
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      frmTsopMain.mnuIncrementLotRun.Enabled = True

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub InitializeMetricsCollection()
    Dim dt As DataTable
    Dim row As DataRow
    Dim newMetric As Metric
    Dim sMetricTypeString As String
    Dim dtMpc As DataTable
    Dim rowMpcSource As DataRow
    Dim rowMpcDest As DataRow
    Dim intRegionCount As Int16
    Dim rowMetricProperties As DataRow
    Dim strPassFailDeterminationType As String
    Try

      'clear metric collection
      gMetrics.Clear()

      dt = gDatabase.GetFunctionMetricListDataTable
      'For each metric in database query
      For Each row In dt.Rows
        newMetric = New Metric
        newMetric.FunctionMetricID = row("FunctionMetricID")
        newMetric.Statistics.MetricID = row("FunctionMetricID")
        newMetric.MethodName = row("MethodName")
        'Debug.Print(newMetric.MethodName)
        newMetric.MetricName = row("MetricName")
        newMetric.UnitsOfMeasure = row("UnitsOfMeasure")
        sMetricTypeString = row("MetricTypeName")
        'Select Case sMetricTypeString
        '  Case Metric.MetricTypeEnum.FunctionalTest.ToString
        '    newMetric.MetricType = Metric.MetricTypeEnum.FunctionalTest
        '  Case Metric.MetricTypeEnum.Programming.ToString
        '    newMetric.MetricType = Metric.MetricTypeEnum.Programming
        '  Case Metric.MetricTypeEnum.Alpha.ToString ' 2.0.1.0 TER
        '    newMetric.MetricType = Metric.MetricTypeEnum.Alpha
        '  Case Metric.MetricTypeEnum.GraphOnly.ToString
        '    newMetric.MetricType = Metric.MetricTypeEnum.GraphOnly
        'End Select
        newMetric.MetricType = DirectCast([Enum].Parse(GetType(Metric.MetricTypeEnum), sMetricTypeString), Integer)

        'Get Metric Properties Data Row 'TER6Gen
        rowMetricProperties = GetMetricPropertiesRow(newMetric.FunctionMetricID)
        If gAnomaly IsNot Nothing Then
          newMetric.Enabled = False
          If rowMetricProperties Is Nothing Then
            gAnomaly.AnomalyExceptionMessage = gAnomaly.AnomalyExceptionMessage & vbCrLf & _
              "Metric '" & newMetric.MetricName & "' Doesn't Exist In Metric Properties Table"
            Throw New TsopAnomalyException
          Else
            Throw New TsopAnomalyException
          End If
        End If
        newMetric.Enabled = CBool(rowMetricProperties.Item("InitiallyEnabled"))
        newMetric.ReportOnly = CBool(rowMetricProperties.Item("ReportOnly"))
        If Not newMetric.ReportOnly Then
          strPassFailDeterminationType = rowMetricProperties.Item("PassFailDeterminationType")
          newMetric.PassFailDeterminationType _
              = DirectCast([Enum].Parse(GetType(Metric.PassFailDeterminationTypeEnum) _
              , strPassFailDeterminationType), Integer)
        End If

        'MPC High Limit Regions
        dtMpc = gDatabase.GetMetricMpcValuesDataTable(newMetric.FunctionMetricID, "High Limit")
        For Each rowMpcSource In dtMpc.Rows 'a row for each region
          intRegionCount = dtMpc.Rows.Count
          rowMpcDest = newMetric.MPC.HighLimitRegionsTable.NewRow()
          rowMpcDest("RegionNumber") = rowMpcSource("MPC_RegionNumber")
          rowMpcDest("Abscissa1") = rowMpcSource("Abscissa1")
          rowMpcDest("Ordinate1") = rowMpcSource("Ordinate1")
          rowMpcDest("Abscissa2") = rowMpcSource("Abscissa2")
          rowMpcDest("Ordinate2") = rowMpcSource("Ordinate2")
          If intRegionCount = 1 _
           And (rowMpcDest("Abscissa2") Is DBNull.Value _
           Or rowMpcDest("Ordinate2") Is DBNull.Value) Then
            rowMpcDest("LimitShape") = MetricPerformanceCriteria.LimitShape.Point
            rowMpcDest("Slope") = 0
            rowMpcDest("YIntercept") = rowMpcDest("Ordinate1")
          Else
            rowMpcDest("LimitShape") = MetricPerformanceCriteria.LimitShape.LineSegment
            rowMpcDest("Slope") = (rowMpcDest("Ordinate2") - rowMpcDest("Ordinate1")) _
                                  / (rowMpcDest("Abscissa2") - rowMpcDest("Abscissa1"))
            rowMpcDest("YIntercept") = rowMpcDest("Ordinate1") - (rowMpcDest("Slope") * rowMpcDest("Abscissa1"))
          End If
          newMetric.MPC.HighLimitRegionsTable.Rows.Add(rowMpcDest)
        Next
        'MPC Ideal Value Regions
        dtMpc = gDatabase.GetMetricMpcValuesDataTable(newMetric.FunctionMetricID, "Ideal")
        For Each rowMpcSource In dtMpc.Rows
          intRegionCount = dtMpc.Rows.Count
          rowMpcDest = newMetric.MPC.IdealValueRegionsTable.NewRow()
          rowMpcDest("RegionNumber") = rowMpcSource("MPC_RegionNumber")
          rowMpcDest("Abscissa1") = rowMpcSource("Abscissa1")
          rowMpcDest("Ordinate1") = rowMpcSource("Ordinate1")
          rowMpcDest("Abscissa2") = rowMpcSource("Abscissa2")
          rowMpcDest("Ordinate2") = rowMpcSource("Ordinate2")
          If intRegionCount = 1 _
           And (rowMpcDest("Abscissa2") Is DBNull.Value _
           Or rowMpcDest("Ordinate2") Is DBNull.Value) Then
            rowMpcDest("LimitShape") = MetricPerformanceCriteria.LimitShape.Point
            rowMpcDest("Slope") = 0
            rowMpcDest("YIntercept") = rowMpcDest("Ordinate1")
          Else
            rowMpcDest("LimitShape") = MetricPerformanceCriteria.LimitShape.LineSegment
            rowMpcDest("Slope") = (rowMpcDest("Ordinate2") - rowMpcDest("Ordinate1")) _
                                  / (rowMpcDest("Abscissa2") - rowMpcDest("Abscissa1"))
            rowMpcDest("YIntercept") = rowMpcDest("Ordinate1") - (rowMpcDest("Slope") * rowMpcDest("Abscissa1"))
          End If
          newMetric.MPC.IdealValueRegionsTable.Rows.Add(rowMpcDest)
        Next
        'MPC Low Limit Regions
        dtMpc = gDatabase.GetMetricMpcValuesDataTable(newMetric.FunctionMetricID, "Low Limit")
        For Each rowMpcSource In dtMpc.Rows
          intRegionCount = dtMpc.Rows.Count
          rowMpcDest = newMetric.MPC.LowLimitRegionsTable.NewRow()
          rowMpcDest("RegionNumber") = rowMpcSource("MPC_RegionNumber")
          rowMpcDest("Abscissa1") = rowMpcSource("Abscissa1")
          rowMpcDest("Ordinate1") = rowMpcSource("Ordinate1")
          rowMpcDest("Abscissa2") = rowMpcSource("Abscissa2")
          rowMpcDest("Ordinate2") = rowMpcSource("Ordinate2")
          If intRegionCount = 1 _
           And (rowMpcDest("Abscissa2") Is DBNull.Value _
           Or rowMpcDest("Ordinate2") Is DBNull.Value) Then
            rowMpcDest("LimitShape") = MetricPerformanceCriteria.LimitShape.Point
            rowMpcDest("Slope") = 0
            rowMpcDest("YIntercept") = rowMpcDest("Ordinate1")
          Else
            rowMpcDest("LimitShape") = MetricPerformanceCriteria.LimitShape.LineSegment
            rowMpcDest("Slope") = (rowMpcDest("Ordinate2") - rowMpcDest("Ordinate1")) _
                                  / (rowMpcDest("Abscissa2") - rowMpcDest("Abscissa1"))
            rowMpcDest("YIntercept") = rowMpcDest("Ordinate1") - (rowMpcDest("Slope") * rowMpcDest("Abscissa1"))
          End If
          newMetric.MPC.LowLimitRegionsTable.Rows.Add(rowMpcDest)
        Next

        dtMpc = gDatabase.GetAlphaMetricMPCs(newMetric.FunctionMetricID)
        For Each rowMpcSource In dtMpc.Rows 'a row for each region
          '    intRegionCount = dtMpc.Rows.Count
          rowMpcDest = newMetric.MPC.AlphaMPCValue.NewRow()
          '    rowMpcDest("ValueIndex") = rowMpcSource("ValueIndex")
          rowMpcDest("Value") = rowMpcSource.ItemArray(3)

          newMetric.MPC.AlphaMPCValue.Rows.Add(rowMpcDest)

        Next

        'Method Arguments
        newMetric.MethodArgsDataTable = gDatabase.GetMetricMethodArgsDataTable(newMetric.FunctionMetricID, _
          sMetricTypeString, newMetric.MethodName)
        gMetrics.Add(newMetric.FunctionMetricID, newMetric)
      Next
      gControlFlags.MetricsLoaded = True

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InitializeOverviewTab()
    Dim tabPageNew As New TabPage()
    'Dim tabPanel As New Panel
    Dim labelNew As Label
    Dim textBoxNew As TextBox
    Dim progressBarNew As ProgressBar = Nothing
    Dim myFontFamily As FontFamily
    Dim MyFont As Font
    Dim intTotalWidth As Integer
    Dim intTotalHeight As Integer
    Dim intNewTop As Integer
    'Dim intNewLeft As Integer
    Dim intLeft As Integer
    Dim intLeftSpacing As Integer
    Dim intTopSpacing As Integer
    Dim intFontHeight As Integer
    Dim drScreen As DataRow

    'Parameterize
    Dim intLeftSpacingPercent As Integer
    Dim intTopSpacingPercent As Integer
    Dim intControlWidth As Integer
    'Dim intControlWidthPercent As Integer
    Dim strLabelText As String
    Dim strControlName As String
    Dim intCurrentColumn As Integer
    Dim intColumnCount As Integer
    Dim intNewLeft As Integer
    Try

      intFontHeight = 14
      gOverviewScreenPanel = New Panel
      'gOverviewScreenPanel = New TableLayoutPanel
      gOverviewScreenControls.Clear()

      myFontFamily = New FontFamily("Microsoft Sans Serif")
      MyFont = New Font(myFontFamily, intFontHeight, FontStyle.Bold, GraphicsUnit.Pixel)

      intTotalWidth = frmTsopMain.tabResults.Width
      intTotalHeight = frmTsopMain.tabResults.Height

      gOverviewScreenPanel.Dock = DockStyle.Fill
      gOverviewScreenPanel.BorderStyle = BorderStyle.Fixed3D
      gOverviewScreenPanel.AutoScroll = True

      tabPageNew.AutoScroll = True
      tabPageNew.Name = "Overview"
      tabPageNew.Text = "Overview"

      intLeft = frmTsopMain.tabResults.Left
      'intNewLeft = intLeft

      Call GetOverviewScreenConfig()

      intNewTop = 0
      intNewLeft = 0
      intCurrentColumn = 1
      intColumnCount = gdtOverviewScreenDefinitions.Rows(gdtOverviewScreenDefinitions.Rows.Count - 1).Item("Column")

      For Each drScreen In gdtOverviewScreenDefinitions.Rows
        If intCurrentColumn <> drScreen.Item("Column") Then
          intNewTop = 0
          intCurrentColumn = drScreen.Item("Column")
          intNewLeft = intNewLeft + CInt((1 / intColumnCount * intTotalWidth))
        End If
        intLeftSpacingPercent = drScreen.Item("LeftSpacingPercentPanelWidth")
        intTopSpacingPercent = drScreen.Item("TopSpacingPercentPanelHeight")
        intControlWidth = drScreen.Item("ControlWidth")
        strLabelText = drScreen.Item("LabelText")
        strControlName = drScreen.Item("ControlName")

        intLeftSpacing = CInt(intLeftSpacingPercent / 100 * intTotalWidth)
        intTopSpacing = CInt(intTopSpacingPercent / 100 * intTotalHeight)

        'Label
        labelNew = New Label
        labelNew.AutoSize = True
        labelNew.Font = MyFont
        labelNew.Left = intNewLeft + intLeftSpacing
        labelNew.Top = intNewTop + intTopSpacing
        labelNew.Text = strLabelText

        'Control
        textBoxNew = New TextBox
        textBoxNew.Font = MyFont
        textBoxNew.Left = intNewLeft + intLeftSpacing
        textBoxNew.Top = labelNew.Bottom
        textBoxNew.Width = intControlWidth
        textBoxNew.BorderStyle = BorderStyle.Fixed3D
        'If intLeft + textBoxNew.Width > intNewLeft Then
        '  intNewLeft = intLeft + textBoxNew.Width
        'End If

        intNewTop = textBoxNew.Bottom

        'Add Label and Textbox to Panel
        gOverviewScreenPanel.Controls.Add(labelNew)
        gOverviewScreenPanel.Controls.Add(textBoxNew)
        gOverviewScreenControls.Add(strControlName, textBoxNew)
      Next

      '==================================================
      'Progress Bar
      '==================================================
      gOverviewProgressLabel = New Label
      gOverviewProgressLabel.Font = MyFont
      gOverviewProgressLabel.Top = frmTsopMain.tabResults.Bottom - gOverviewProgressLabel.Height - 175
      gOverviewProgressLabel.Text = "Percent Complete"
      gOverviewProgressLabel.Width = frmTsopMain.tabResults.Width
      gOverviewProgressLabel.TextAlign = ContentAlignment.MiddleCenter

      'Control
      gOverviewProgressBar = New ProgressBar
      gOverviewProgressBar.Top = gOverviewProgressLabel.Bottom
      gOverviewProgressBar.Width = gOverviewProgressLabel.Width

      'Add to panel
      gOverviewScreenPanel.Controls.Add(gOverviewProgressLabel)
      gOverviewScreenPanel.Controls.Add(gOverviewProgressBar)

      'Set Anchor for progress indicators
      gOverviewProgressLabel.Anchor = AnchorStyles.Left + AnchorStyles.Right + AnchorStyles.Bottom
      gOverviewProgressBar.Anchor = AnchorStyles.Left + AnchorStyles.Right + AnchorStyles.Bottom

      '==================================================
      'Add Panel to Tab Page
      '==================================================
      tabPageNew.Controls.Add(gOverviewScreenPanel)

      '==================================================
      'Add page to Tab Control
      '==================================================
      frmTsopMain.tabResults.TabPages.Add(tabPageNew)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub InitializeSetupInfoTab()
    ' 2.0.1.0 TER
    Dim tabPageNew As New TabPage()
    Dim tablePanel As New TableLayoutPanel
    Dim labelNew As Label
    Dim textBoxNew As TextBox
    Dim comboBoxNew As ComboBox
    'Dim alSetupInfoItems As New ArrayList
    Dim strItem As String
    Dim HeaderFont As Font
    Dim blnComboControl As Boolean
    'Dim dt As New DataTable
    'Dim dr As DataRow

    Try

      'dt.Columns.Add("ReleaseType")

      'dr = dt.NewRow
      'dr("ReleaseType") = "Normal"
      'dt.Rows.Add(dr)
      'dr = dt.NewRow
      'dr("ReleaseType") = "Quick"
      'dt.Rows.Add(dr)

      'gSetupInfoTextBoxes.Clear()
      'gSetupInfoComboBoxes.Clear()
      gSetupInfoControls.Clear()

      HeaderFont = New Font("Sans Serif", 10, FontStyle.Bold)

      'tablePanel.Width = tabResults.Size.Width - 30
      'tablePanel.Height = tabResults.Size.Height - 50

      tablePanel.ColumnCount = 2
      tablePanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.AutoSize))
      tablePanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.AutoSize))

      tablePanel.GrowStyle = TableLayoutPanelGrowStyle.AddRows
      'tablePanel.Anchor = AnchorStyles.Top
      'tablePanel.Anchor = AnchorStyles.Bottom
      'tablePanel.Anchor = AnchorStyles.Left
      'tablePanel.Anchor = AnchorStyles.Right
      tablePanel.Dock = DockStyle.Fill
      tablePanel.CellBorderStyle = TableLayoutPanelCellBorderStyle.OutsetDouble
      tablePanel.AutoScroll = True

      labelNew = New Label
      labelNew.Text = "Setup Item"
      labelNew.Font = HeaderFont
      labelNew.AutoSize = True
      tablePanel.Controls.Add(labelNew)

      labelNew = New Label
      labelNew.Text = "Setup Item Value"
      labelNew.Font = HeaderFont
      labelNew.AutoSize = True
      tablePanel.Controls.Add(labelNew)

      For Each strItem In gstrSetupInfoItems
        blnComboControl = False

        labelNew = New Label
        labelNew.AutoSize = True
        labelNew.Text = strItem
        tablePanel.Controls.Add(labelNew)

        If strItem = SetupInfoItems.PedalArmReleaseType.ToString Then
          If gdtPedalReturnStrokeLengths.Rows.Count > 0 Then
            blnComboControl = True
            comboBoxNew = New ComboBox
            comboBoxNew.Dock = DockStyle.Fill
            comboBoxNew.DataSource = gdtPedalArmReleaseTypes
            comboBoxNew.ValueMember = gdtPedalArmReleaseTypes.Columns("ReleaseType").ToString
            comboBoxNew.DisplayMember = gdtPedalArmReleaseTypes.Columns("ReleaseType").ToString
            tablePanel.Controls.Add(comboBoxNew)
            gSetupInfoControls.Add(strItem, comboBoxNew)
          End If

        End If

        If strItem = SetupInfoItems.StrokeLength.ToString Then ' 3.1.0.0 TER
          If gdtPedalReturnStrokeLengths.Rows.Count > 0 Then
            blnComboControl = True
            comboBoxNew = New ComboBox
            comboBoxNew.Dock = DockStyle.Fill
            comboBoxNew.DataSource = gdtPedalReturnStrokeLengths
            comboBoxNew.ValueMember = gdtPedalReturnStrokeLengths.Columns("StrokeLength").ToString
            comboBoxNew.DisplayMember = gdtPedalReturnStrokeLengths.Columns("StrokeLength").ToString
            tablePanel.Controls.Add(comboBoxNew)
            gSetupInfoControls.Add(strItem, comboBoxNew)
          End If
        End If

        If Not blnComboControl Then
          textBoxNew = New TextBox
          textBoxNew.Dock = DockStyle.Fill
          tablePanel.Controls.Add(textBoxNew)
          gSetupInfoControls.Add(strItem, textBoxNew)
        End If

      Next

      tabPageNew.Name = "SetupInformation"
      tabPageNew.Text = "Setup Information"
      tabPageNew.AutoScroll = True
      tabPageNew.Controls.Add(tablePanel)
      frmTsopMain.tabResults.TabPages.Add(tabPageNew)

      If gSetupInfoControls.ContainsKey(SetupInfoItems.Series) Then
        'gSetupInfoTextBoxes(SetupInfoItems.Series).Text = gControlAndInterface.SeriesID
        gSetupInfoControls(SetupInfoItems.Series).Text = gControlAndInterface.SeriesID
      End If

      If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalTestMethodSelected) Then
        frmTsopMain.mnuExternalTestMethods.Visible = True
        frmTsopMain.mnuExternalTestMethods.Enabled = True
      Else
        frmTsopMain.mnuExternalTestMethods.Visible = False
        frmTsopMain.mnuExternalTestMethods.Enabled = False
      End If

      'set focus on first control
      For Each strkey As String In gSetupInfoControls.Keys
        gSetupInfoControls(strkey).Focus()
        Exit For
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InitializeVariablesParamSet()
    Try

      frmTsopMain.tabResults.TabPages.Clear() 'Was in cboParameterSet KeyUp

      gtmrSendScannerInitTimer.Interval = 1000
      gtmrSendScannerInitTimer.Enabled = False

      gtmrUpdateStatusBarTimer.Interval = 100
      gtmrUpdateStatusBarTimer.Enabled = False

      gControlFlags.MasterMode = False

      gControlFlags.StartScan = False
      gControlFlags.ManualScan = False

      If gControlAndInterface.UsesOverviewScreen Then
        gControlFlags.UsesOverviewScreen = gControlAndInterface.UsesOverviewScreen
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InitializeVariablesTsop()
    Try

      AddHandler gtmrUpdateStatusBarTimer.Tick, AddressOf gtmrUpdateStatusBarTimer_Tick

      gtmrOperatorTouchStartTimer.Interval = 2
      AddHandler gtmrOperatorTouchStartTimer.Tick, AddressOf OperatorTouchStartTimer_Tick
      gtmrOperatorTouchStartTimer.Enabled = False


      gstrRTYAssemblyLine = ConfigurationManager.AppSettings("RTYAssemblyLine")

      gstrRTYMachineName = ConfigurationManager.AppSettings("RTYMachineName")

      gControlFlags.UsesOverviewScreen = ConfigurationManager.AppSettings("UsesOverviewScreen")
      gstrOverviewScreenDefinitionFile = ConfigurationManager.AppSettings("OverviewScreenDefinitionFile")

      gdblExpectedCycleTime = ConfigurationManager.AppSettings("ExpectedCycleTime")

      gControlFlags.LastShiftUseNextLotDay = ConfigurationManager.AppSettings("LastShiftUsesNextLotDay")

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub InsertDbProgRecord()
    Dim lstrProgramVersion As String
    Dim lstrOperator As String
    Dim lstrTemp As String
    Dim lstrComment As String
    Dim lstrTestLog As String
    Dim lstrSampleNum As String
    Dim lfrmUpdate As frmTsopMain

    Try
      If gDatabase.ProgrammingID = Guid.Empty Then

        lfrmUpdate = My.Application.OpenForms("frmTsopMain")

        lstrComment = ""
        lstrOperator = ""
        lstrSampleNum = ""
        lstrTestLog = ""
        lstrTemp = ""

        'set program version string for database
        lstrProgramVersion = My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Build & "." & My.Application.Info.Version.Revision
        If gControlFlags.UsesDynamicSetupInfo Then
          If gSetupInfoControls.ContainsKey(SetupInfoItems.Comment) Then
            lstrComment = gSetupInfoControls(SetupInfoItems.Comment).Text
          End If
          If gSetupInfoControls.ContainsKey(SetupInfoItems.OperatorName) Then
            lstrOperator = gSetupInfoControls(SetupInfoItems.OperatorName).Text
          End If
          'If gSetupInfoControls.ContainsKey(SetupInfoItems.MicronasID) Then '3.0.0.8DWD  Changed from Customer to Micronas ID
          '  lstrSampleNum = gSetupInfoControls(SetupInfoItems.MicronasID).Text
          'End If
          If gSetupInfoControls.ContainsKey(SetupInfoItems.TestLogNumber) Then
            lstrTestLog = gSetupInfoControls(SetupInfoItems.TestLogNumber).Text
          End If
          If gSetupInfoControls.ContainsKey(SetupInfoItems.Temperature) Then
            lstrTemp = gSetupInfoControls(SetupInfoItems.Temperature).Text
          End If

          'Else
          '  lstrComment = lfrmUpdate.txtComment.Text
          '  lstrOperator = lfrmUpdate.txtOperator.Text
          '  lstrSampleNum = lfrmUpdate.txtSampleNumber.Text
          '  lstrTestLog = lfrmUpdate.txtTestLogNumber.Text
          '  lstrTemp = lfrmUpdate.txtTemperature.Text
        End If

        gDatabase.InsertProgRecord(lstrProgramVersion, lstrOperator, lstrTemp, lstrComment, lstrSampleNum, lstrTestLog)
        If gDatabase.Anomaly IsNot Nothing Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InsertDbTestRecord()
    Dim lstrProgramVersion As String
    Dim lstrOperator As String
    Dim lstrTemp As String
    Dim lstrComment As String
    Dim lstrTestLog As String
    Dim lstrSampleNum As String
    Dim lfrmUpdate As frmTsopMain

    Try
      If gDatabase.TestID = Guid.Empty Then

        lfrmUpdate = My.Application.OpenForms("frmTsopMain")

        lstrComment = ""
        lstrOperator = ""
        lstrSampleNum = ""
        lstrTestLog = ""
        lstrTemp = ""

        'set program version string for database
        lstrProgramVersion = My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Build & "." & My.Application.Info.Version.Revision
        If gControlFlags.UsesDynamicSetupInfo Then
          If gSetupInfoControls.ContainsKey(SetupInfoItems.Comment) Then
            lstrComment = gSetupInfoControls(SetupInfoItems.Comment).Text
          End If
          If gSetupInfoControls.ContainsKey(SetupInfoItems.OperatorName) Then
            lstrOperator = gSetupInfoControls(SetupInfoItems.OperatorName).Text
          End If
          If gSetupInfoControls.ContainsKey(SetupInfoItems.SampleNumber) Then
            lstrSampleNum = gSetupInfoControls(SetupInfoItems.SampleNumber).Text
          End If
          If gSetupInfoControls.ContainsKey(SetupInfoItems.TestLogNumber) Then
            lstrTestLog = gSetupInfoControls(SetupInfoItems.TestLogNumber).Text
          End If
          If gSetupInfoControls.ContainsKey(SetupInfoItems.Temperature) Then
            lstrTemp = gSetupInfoControls(SetupInfoItems.Temperature).Text
          End If
        Else
          'lstrComment = lfrmUpdate.txtComment.Text
          'lstrOperator = lfrmUpdate.txtOperator.Text
          'lstrSampleNum = lfrmUpdate.txtSampleNumber.Text
          'lstrTestLog = lfrmUpdate.txtTestLogNumber.Text
          'lstrTemp = lfrmUpdate.txtTemperature.Text
        End If

        gDatabase.InsertTestRecord(lstrProgramVersion, lstrOperator, lstrTemp, lstrComment, lstrSampleNum, lstrTestLog)
        If gDatabase.Anomaly IsNot Nothing Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InsertExternalTestMethodRecord()
    Dim strDuringInterval As String = Nothing
    Try
      'Insert Test External Test Method Record 
      If gDatabase.ExternalTestMethodID <> Guid.Empty Then
        If gDatabase.ExternalTestMethodRelationshipID = gExternalTestMethodRelationships("During") Then
          strDuringInterval = mnuExternalTestMethodsDuringIntervalTextBox.Text
        End If
        Call gDatabase.InsertTestExternalTestMethod(strDuringInterval)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub MarkDeviceDateCodeAndSerialNumber()
    Dim strSerialNumber As String
    Try
      If gControlFlags.DeviceMarkingEnabled = True And gControlFlags.TestFailure = False Then

        strSerialNumber = CStr(Format(gintSerialNumberFinal, "00000"))

        gDeviceMarker.MarkDateCodeSerialNumberAndRev(gControlAndInterface.DeviceMarkingFileName, gDeviceInProcess.DateCode, strSerialNumber, gControlAndInterface.CustomerDrawingRev)
        If gDeviceMarker.Anomaly IsNot Nothing Then
          gAnomaly = New clsDbAnomaly(gDeviceMarker.Anomaly, gDatabase)
          gDeviceMarker.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ProcessAlphaMetrics() ' 2.0.1.0 TER
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Try

      For Each metMetric In gMetrics.Values
        If metMetric.MetricType = Metric.MetricTypeEnum.Alpha Then
          metMetrics.Add(metMetric.FunctionMetricID, metMetric)
        End If
      Next
      Call ProcessMetrics(metMetrics)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ProcessFunctionalTestMetrics()
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Try

      For Each metMetric In gMetrics.Values
        If metMetric.MetricType = Metric.MetricTypeEnum.FunctionalTest Then
          metMetrics.Add(metMetric.FunctionMetricID, metMetric)
        End If
      Next
      Call ProcessMetrics(metMetrics)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ProcessProgrammingMetrics()
    Dim metMetric As Metric
    Dim metMetrics As New Dictionary(Of Guid, Metric)
    Try

      For Each metMetric In gMetrics.Values
        If metMetric.MetricType = Metric.MetricTypeEnum.Programming Then
          metMetrics.Add(metMetric.FunctionMetricID, metMetric)
        End If
      Next
      Call ProcessMetrics(metMetrics)


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub ProgramUnit()
    '  Dim strIOAddress As String

    '  Try

    '    strIOAddress = DIOAddressNameEnum.Programmer1Enable.ToString
    '    If gDaqDigitalIO.ReadAddress(strIOAddress) = False Then
    '      Call gDaqDigitalIO.WriteAddress(strIOAddress, True)
    '    End If

    '    strIOAddress = DIOAddressNameEnum.VRefEnable.ToString
    '    If gDaqDigitalIO.ReadAddress(strIOAddress) = True Then
    '      Call gDaqDigitalIO.WriteAddress(strIOAddress, False)
    '    End If

    '    Call RunSolverMLX90333()
    '    If Not (gAnomaly Is Nothing) Then
    '      Throw New TsopAnomalyException
    '    End If

    '  Catch ex As TsopAnomalyException
    '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    '  Catch ex As Exception
    '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    '  Finally
    '  End Try
  End Sub

  Public Sub ResetMetrics()
    Dim metMetric As Metric
    Try
      'reset metrics
      For Each metMetric In gMetrics.Values
        metMetric.MetricLocation = Nothing
        metMetric.MetricValue = Nothing
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.NoData
        metMetric.Processed = False
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub ResetVariablesBeforeTest()
    Try

      'Update Status LED
      UpdatePassFailLED(Metric.PassFailStatusEnum.NoData)

      gCycleTimeStopWatch.Reset()
      gCycleTimeStopWatch.Start()

      gBtoBTimeStopWatch.Stop()
      frmTsopMain.StatusLabel2.Text = "Button to Button Time was  " & gBtoBTimeStopWatch.ElapsedMilliseconds / 1000 & " Secs."
      LogActivity(frmTsopMain.StatusLabel2.Text)
      If gControlFlags.UsesOverviewScreen Then
        gOverviewScreenControls(OverviewScreenControls.ButtonToButton).Text = gBtoBTimeStopWatch.ElapsedMilliseconds / 1000
        gtmrUpdateStatusBarTimer.Enabled = True
      End If
      gBtoBTimeStopWatch.Reset()
      gBtoBTimeStopWatch.Start()

      'gSWTestTimeStopWatch.Stop()
      'gSWTestTimeStopWatch.Reset()
      'gSWTestTimeStopWatch.Start()

      gActivityLogStopWatch.Reset()
      gActivityLogStopWatch.Start()

      gControlFlags.TestFailure = False
      gControlFlags.TestError = False
      gControlFlags.ProgFailure = False
      gControlFlags.ProgError = False

      gControlFlags.TestInProgress = True

      gDatabase.TestID = Guid.Empty
      gDatabase.ProgrammingID = Guid.Empty

      'clear old data
      gDataArrays = New DataArrays

      ''Call frmTsopMain.ClearGraphs()

      'Initialize Result Data Tables
      gdtTestResultsTable.Clear()
      gdtProgResultsTable.Clear()
      gdtTestFailuresTable.Clear()
      gdtAlphaTestResultsTable.Clear()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub SaveAlphaTestResultsToDb()
    ' 2.0.1.0 TER
    Dim ds As New DataSet("ResultSet")
    Dim dsCopy As New DataSet("ResultSet")
    Dim dt As New DataTable
    Dim row As DataRow
    Dim newrow As DataRow
    Dim dtcopy As New DataTable
    Dim sXml As String
    Dim rowcounter As Integer
    Dim updatecounter As Integer
    Dim lintUpdateCount As Integer
    Dim bTestPassed As Boolean
    Try


      'bTestPassed = Not (gblnTestFailure)
      bTestPassed = Not (gControlFlags.TestFailure)
      dt = gdtAlphaTestResultsTable.Copy
      ds.Tables.Add(dt)
      ds.Tables(0).TableName = "Result"
      sXml = ds.GetXml
      'Debug.Print(sXml)

      'string length limit is 8000 ASCII Characters
      lintUpdateCount = sXml.Length / 7000
      If (sXml.Length Mod 7000) > 0 Then
        lintUpdateCount += 1
      End If

      dtcopy.Columns.Add("MetricID", GetType(Guid))
      dtcopy.Columns.Add("MetricValue", GetType(String))


      For Each row In dt.Rows
        newrow = dtcopy.NewRow
        newrow("MetricID") = row("MetricID")
        newrow("MetricValue") = row("MetricValue")

        dtcopy.Rows.Add(newrow)
        rowcounter += 1
        updatecounter += 1
        If updatecounter > (dt.Rows.Count / lintUpdateCount) Then
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertAlphaTestResults(sXml, bTestPassed, "")
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result")
          dtcopy.Clear()
          updatecounter = 0
        ElseIf rowcounter >= dt.Rows.Count Then ' final update
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertAlphaTestResults(sXml, bTestPassed, "")
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result")
          dtcopy.Clear()
          updatecounter = 0
          rowcounter = 0
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveDataToADR() ' 3.0.0.0 TER
    Dim intMeasurementNumber As Integer

    Try

      'For intMeasurementNumber = 1 To gControlAndInterface.NumberOfScans
      For intMeasurementNumber = 1 To gAdrChannels(0).Measurements.Count
        Call GetAdrArraySize(intMeasurementNumber)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If

        Call CombineArraysForADR(intMeasurementNumber)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If

        'Console.WriteLine("TestID = " & gDatabase.TestID.ToString)

        Call SaveAcquiredDataRecord(intMeasurementNumber)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      Next

      '' ''Use for Debugging
      'Call ReadAcquiredDataRecord_ExampleCode()
      'If gAnomaly IsNot Nothing Then
      '  Throw New TsopAnomalyException
      'End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SavePedalArmReturnADR() ' 2.0.1.0 TER
    Dim intMeasurementNumber As Integer

    Try

      'For intMeasurementNumber = 1 To gControlAndInterface.NumberOfScans
      For intMeasurementNumber = 1 To gAdrChannels(0).Measurements.Count
        Call GetAdrArraySize(intMeasurementNumber)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If

        Call CombineArraysForADR(intMeasurementNumber)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If

        'Console.WriteLine("TestID = " & gDatabase.TestID.ToString)

        Call SaveAcquiredDataRecord(intMeasurementNumber)
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      Next


      ' ''Use for Debugging
      'Call ReadAcquiredDataRecord_ExampleCode()
      'If gAnomaly IsNot Nothing Then
      '  Throw New TsopAnomalyException
      'End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveProgResultsToDb()
    Dim ds As New DataSet("ResultSet")
    Dim dsCopy As New DataSet("ResultSet")
    Dim dt As New DataTable
    Dim row As DataRow
    Dim newrow As DataRow
    Dim dtcopy As New DataTable
    Dim sXml As String
    Dim rowcounter As Integer
    Dim updatecounter As Integer
    Dim lintUpdateCount As Integer
    Dim bTestPassed As Boolean

    Try

      bTestPassed = Not (gControlFlags.ProgFailure)
      dt = gdtProgResultsTable.Copy
      ds.Tables.Add(dt)
      ds.Tables(0).TableName = "Result"
      sXml = ds.GetXml
      'Debug.Print(sXml)

      'string length limit is 8000 ASCII Characters
      lintUpdateCount = sXml.Length / 7000
      If (sXml.Length Mod 7000) > 0 Then
        lintUpdateCount += 1
      End If

      dtcopy.Columns.Add("MetricID", GetType(Guid))
      dtcopy.Columns.Add("MetricValue", GetType(Double))

      For Each row In dt.Rows
        newrow = dtcopy.NewRow
        newrow("MetricID") = row("MetricID")
        newrow("MetricValue") = row("MetricValue")
        dtcopy.Rows.Add(newrow)
        rowcounter += 1
        updatecounter += 1
        If updatecounter > (dt.Rows.Count / lintUpdateCount) Then
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertProgResults(sXml, bTestPassed, "")
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result")
          dtcopy.Clear()
          updatecounter = 0
        ElseIf rowcounter >= dt.Rows.Count Then ' final update
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertProgResults(sXml, bTestPassed, "")
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result")
          dtcopy.Clear()
          updatecounter = 0
          rowcounter = 0
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveTestResultsToDb()
    Dim ds As New DataSet("ResultSet")
    Dim dsCopy As New DataSet("ResultSet")
    Dim dt As New DataTable
    Dim row As DataRow
    Dim newrow As DataRow
    Dim dtcopy As New DataTable
    Dim sXml As String
    Dim rowcounter As Integer
    Dim updatecounter As Integer
    Dim lintUpdateCount As Integer
    Dim bTestPassed As Boolean
    Try


      'bTestPassed = Not (gblnTestFailure)
      bTestPassed = Not (gControlFlags.TestFailure)
      dt = gdtTestResultsTable.Copy
      ds.Tables.Add(dt)
      ds.Tables(0).TableName = "Result"
      sXml = ds.GetXml
      'Debug.Print(sXml)

      'string length limit is 8000 ASCII Characters
      lintUpdateCount = sXml.Length / 7000
      If (sXml.Length Mod 7000) > 0 Then
        lintUpdateCount += 1
      End If

      dtcopy.Columns.Add("MetricID", GetType(Guid))
      dtcopy.Columns.Add("MetricValue", GetType(Double))

      For Each row In dt.Rows
        newrow = dtcopy.NewRow
        newrow("MetricID") = row("MetricID")
        newrow("MetricValue") = row("MetricValue")
        dtcopy.Rows.Add(newrow)
        rowcounter += 1
        updatecounter += 1
        If updatecounter > (dt.Rows.Count / lintUpdateCount) Then
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertTestResults(sXml, bTestPassed, "")
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result") 'ANM
          dtcopy.Clear()
          updatecounter = 0
        ElseIf rowcounter >= dt.Rows.Count Then ' final update
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertTestResults(sXml, bTestPassed, "")
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result") 'ANM
          dtcopy.Clear()
          updatecounter = 0
          rowcounter = 0
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveTestFailuresToDb()
    Dim ds As New DataSet("ResultSet")
    Dim dsCopy As New DataSet("ResultSet")
    Dim dt As New DataTable
    Dim row As DataRow
    Dim newrow As DataRow
    Dim dtcopy As New DataTable
    Dim sXml As String
    Dim rowcounter As Integer
    Dim updatecounter As Integer
    Dim lintUpdateCount As Integer
    Try


      'bTestPassed = Not (gblnTestFailure)
      dt = gdtTestFailuresTable.Copy
      ds.Tables.Add(dt)
      ds.Tables(0).TableName = "Result"
      sXml = ds.GetXml
      'Debug.Print(sXml)

      'string length limit is 8000 ASCII Characters
      lintUpdateCount = sXml.Length / 7000
      If (sXml.Length Mod 7000) > 0 Then
        lintUpdateCount += 1
      End If

      dtcopy.Columns.Add("MetricID", GetType(Guid))

      For Each row In dt.Rows
        newrow = dtcopy.NewRow
        newrow("MetricID") = row("MetricID")
        dtcopy.Rows.Add(newrow)
        rowcounter += 1
        updatecounter += 1
        If updatecounter > (dt.Rows.Count / lintUpdateCount) Then
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertTestFailures(sXml)
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result")
          dtcopy.Clear()
          updatecounter = 0
        ElseIf rowcounter >= dt.Rows.Count Then ' final update
          dsCopy.Tables.Add(dtcopy)
          dsCopy.Tables(0).TableName = "Result"
          sXml = dsCopy.GetXml
          'save to database
          gDatabase.InsertTestFailures(sXml)
          dsCopy.Clear()
          dsCopy.Tables.Remove("Result")
          dtcopy.Clear()
          updatecounter = 0
          rowcounter = 0
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub SaveProgResultsToFile()
  '  '
  '  '   PURPOSE: To save the prog results data to a comma delimited file
  '  '
  '  '  INPUT(S): none
  '  ' OUTPUT(S): none

  '  Dim lintFileNum As Integer
  '  Dim lstrFileName As String
  '  'Dim lstrPath As String
  '  Dim bTestPassed As Boolean
  '  Dim i As Integer
  '  Dim lfrmUpdate As frmTsopMain

  '  Try
  '    bTestPassed = Not (gControlFlags.ProgFailure)
  '    lfrmUpdate = My.Application.OpenForms("frmTsopMain")

  '    'Save data to file if needed
  '    If (lfrmUpdate.mnuOptionsSaveResultsToCsv.Checked = True) Then

  '      'Make the results file name
  '      lstrFileName = frmTsopMain.lblLotName.Text + " Prog Results.csv"

  '      'Get a file
  '      lintFileNum = FreeFile()

  '      'Write out files based on if need headings or not
  '      'If Not My.Computer.FileSystem.FileExists(lstrPath + lstrFileName) Then
  '      If Not My.Computer.FileSystem.FileExists(gstrProgDataFilePath + lstrFileName) Then
  '        'FileOpen(lintFileNum, lstrPath + lstrFileName, OpenMode.Append, OpenAccess.Write)
  '        FileOpen(lintFileNum, gstrProgDataFilePath + lstrFileName, OpenMode.Append, OpenAccess.Write)
  '        'Headings
  '        Microsoft.VisualBasic.Print(lintFileNum, "Parameter Set,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Lot Name,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Date/Time,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Date Code,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Serial Number,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Shift,")
  '        'For i = 0 To gintMetricRows - 1
  '        '  Microsoft.VisualBasic.Print(lintFileNum, gstrMetricID(i) & ",")
  '        'Next i
  '        For i = 62 To galDataFileMetricHeaders.Count - 1
  '          Microsoft.VisualBasic.Print(lintFileNum, galDataFileMetricHeaders(i) & ",")
  '        Next i
  '        Microsoft.VisualBasic.Print(lintFileNum, "PASS/FAIL,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Operator,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Temperature,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Comment" & Chr(10))
  '        'Values
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboParameterSets.Text & ",")
  '        'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboLotFiles.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.lblLotName.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, Microsoft.VisualBasic.DateAndTime.Now & ",")
  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.DateCode) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.DateCode).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalSerialNumber) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.ExternalSerialNumber).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Shift) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Shift).Text & ",")
  '          Else
  '          End If
  '        Else
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtDateCode.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtExternalSerialNumber.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtShift.Text & ",")
  '        End If

  '        'For i = 0 To gintMetricRows - 1
  '        '  Microsoft.VisualBasic.Print(lintFileNum, gstrMetricValue(i) & ",")
  '        'Next i
  '        For i = 62 To galDataFileMetricValues.Count - 1
  '          Microsoft.VisualBasic.Print(lintFileNum, galDataFileMetricValues(i) & ",")
  '        Next i
  '        If bTestPassed Then
  '          Microsoft.VisualBasic.Print(lintFileNum, "PASSED,")
  '        Else
  '          Microsoft.VisualBasic.Print(lintFileNum, "REJECT,")
  '        End If
  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.OperatorName) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.OperatorName).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Temperature) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Temperature).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Comment) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Comment).Text & Chr(10))
  '          End If
  '        Else
  '          '  Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtOperator.Text & ",")
  '          '  Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtTemperature.Text & ",")
  '          '  Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtComment.Text & Chr(10))
  '        End If
  '      Else
  '        'FileOpen(lintFileNum, lstrPath + lstrFileName, OpenMode.Append, OpenAccess.Write)
  '        FileOpen(lintFileNum, gstrProgDataFilePath + lstrFileName, OpenMode.Append, OpenAccess.Write)

  '        'Just values
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboParameterSets.Text & ",")
  '        'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboLotFiles.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.lblLotName.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, Microsoft.VisualBasic.DateAndTime.Now & ",")

  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.DateCode) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.DateCode).Text & ",")
  '          Else
  '            Microsoft.VisualBasic.Print(lintFileNum, "" & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalSerialNumber) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.ExternalSerialNumber).Text & ",")
  '          Else
  '            Microsoft.VisualBasic.Print(lintFileNum, "" & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Shift) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Shift).Text & ",")
  '          Else
  '            Microsoft.VisualBasic.Print(lintFileNum, "" & ",")
  '          End If
  '        Else
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtDateCode.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtExternalSerialNumber.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtShift.Text & ",")
  '        End If

  '        'For i = 0 To gintMetricRows - 1
  '        '  Microsoft.VisualBasic.Print(lintFileNum, gstrMetricValue(i) & ",")
  '        'Next i
  '        For i = 62 To galDataFileMetricValues.Count - 1
  '          Microsoft.VisualBasic.Print(lintFileNum, galDataFileMetricValues(i) & ",")
  '        Next i
  '        If bTestPassed Then
  '          Microsoft.VisualBasic.Print(lintFileNum, "PASSED,")
  '        Else
  '          Microsoft.VisualBasic.Print(lintFileNum, "REJECT,")
  '        End If
  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.OperatorName) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.OperatorName).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Temperature) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Temperature).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Comment) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Comment).Text & Chr(10))
  '          End If
  '        Else
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtOperator.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtTemperature.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtComment.Text & Chr(10))
  '        End If
  '      End If
  '      FileClose(lintFileNum)
  '    End If

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub SaveForceDataToFile(ByVal forceVoltageArray() As Double, ByVal PosArray() As Double)
    '
    '   PURPOSE: To save the force data to a comma delimited file
    '
    '  INPUT(S): none
    ' OUTPUT(S): none

    Dim lintFileNum As Integer
    Dim lstrFileName As String
    'Dim i As Integer
    Dim intLoop As Integer
    Dim sngForce As Single
    Dim dblMultiplier As Double
    Dim dblOffset As Double

    Try
      lstrFileName = frmTsopMain.lblLotName.Text + " " + gDeviceInProcess.EncodedSerialNumber + " Force Results.csv"

      'Get a file
      lintFileNum = FreeFile()
      FileOpen(lintFileNum, gstrDataFilePath + lstrFileName, OpenMode.Append, OpenAccess.Write)

      dblMultiplier = gdblForceAmpGain
      If gintForceAmpGainUnits = ForceAmpGainUnitsEnum.lbfPerVolt Then
        dblMultiplier = dblMultiplier * gConverter.NewtonsPerLbf
      End If

      dblMultiplier = dblMultiplier * gAmad.ForceCosineMultiplier
      dblOffset = gdblForceAmpOffset + gAmad.ForceCorrectionOffset

      For intLoop = 0 To forceVoltageArray.Length - 1
        sngForce = (CSng(forceVoltageArray(intLoop)) * dblMultiplier) + dblOffset
        Microsoft.VisualBasic.Print(lintFileNum, CStr(sngForce) & "," & CStr(PosArray(intLoop)) & Chr(10))
      Next

      FileClose(lintFileNum)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveRawDataToFile()
    '
    '   PURPOSE: To save the raw data to a comma delimited file
    '
    '  INPUT(S): none
    ' OUTPUT(S): none

    Dim lintFileNum As Integer
    Dim lstrFileName As String
    Dim intLoop As Integer 
    Dim Min As Integer

    'Dim i As Integer
    'Dim sngForce As Single
    'Dim dblMultiplier As Double
    'Dim dblOffset As Double

    Try
      lstrFileName = frmTsopMain.lblLotName.Text + " " + gDeviceInProcess.EncodedSerialNumber + " Raw Results.csv"

      'Get a file
      lintFileNum = FreeFile()
      FileOpen(lintFileNum, gstrDataFilePath + lstrFileName, OpenMode.Append, OpenAccess.Write)

      'dblMultiplier = gdblForceAmpGain
      'If gintForceAmpGainUnits = ForceAmpGainUnitsEnum.lbfPerVolt Then
      '  dblMultiplier = dblMultiplier * gConverter.NewtonsPerLbf
      'End If

      'dblMultiplier = dblMultiplier * gAmad.ForceCosineMultiplier
      'dblOffset = gdblForceAmpOffset + gAmad.ForceCorrectionOffset

      Min = gDataArrays.ForwardDUTOut1PercentVrefArray.Length
          
      If gDataArrays.ForwardDUTOut2PercentVrefArray.Length < Min Then
         Min = gDataArrays.ForwardDUTOut2PercentVrefArray.Length
      End If

      If gDataArrays.ReverseDUTOut1PercentVrefArray.Length < Min Then
         Min = gDataArrays.ReverseDUTOut1PercentVrefArray.Length
      End If

      If gDataArrays.ReverseDUTOut2PercentVrefArray.Length < Min Then
         Min = gDataArrays.ReverseDUTOut2PercentVrefArray.Length
      End If

      If gDataArrays.ForwardForceValueArray.Length < Min Then
         Min = gDataArrays.ForwardForceValueArray.Length
      End If

      If gDataArrays.ReverseForceValueArray.Length < Min Then
         Min = gDataArrays.ReverseForceValueArray.Length
      End If

      For intLoop = 0 To Min - 1
        'sngForce = (CSng(forceVoltageArray(intLoop)) * dblMultiplier) + dblOffset
        Microsoft.VisualBasic.Print(lintFileNum, CStr(gDataArrays.ForwardEncoderPositionArray(intLoop)) & "," & CStr(gDataArrays.ForwardDUTOut1PercentVrefArray(intLoop)) & "," & CStr(gDataArrays.ForwardDUTOut2PercentVrefArray(intLoop)) & "," & CStr(gDataArrays.ReverseDUTOut1PercentVrefArray(intLoop)) & "," & CStr(gDataArrays.ReverseDUTOut2PercentVrefArray(intLoop)) & "," & cstr(gDataArrays.ForwardForceValueArray(intLoop)) & "," & cstr(gDataArrays.ReverseForceValueArray(intLoop)) & Chr(10))
      Next

      FileClose(lintFileNum)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub SaveTestResultsToFile()
  '  '
  '  '   PURPOSE: To save the scan results data to a comma delimited file
  '  '
  '  '  INPUT(S): none
  '  ' OUTPUT(S): none

  '  Dim lintFileNum As Integer
  '  Dim lstrFileName As String
  '  'Dim lstrPath As String
  '  Dim bTestPassed As Boolean
  '  Dim i As Integer
  '  Dim lfrmUpdate As frmTsopMain

  '  Try
  '    bTestPassed = Not (gControlFlags.TestFailure)
  '    lfrmUpdate = My.Application.OpenForms("frmTsopMain")

  '    'Save data to file if needed
  '    If (lfrmUpdate.mnuOptionsSaveResultsToCsv.Checked = True) Then

  '      'Make the results file name
  '      'lstrFileName = frmTsopMain.cboLotFiles.Text + " Scan Results.csv"
  '      lstrFileName = frmTsopMain.lblLotName.Text + " Scan Results.csv"

  '      'Get a file
  '      lintFileNum = FreeFile()

  '      'Write out files based on if need headings or not
  '      'If Not My.Computer.FileSystem.FileExists(lstrPath + lstrFileName) Then
  '      If Not My.Computer.FileSystem.FileExists(gstrDataFilePath + lstrFileName) Then
  '        'FileOpen(lintFileNum, lstrPath + lstrFileName, OpenMode.Append, OpenAccess.Write)
  '        FileOpen(lintFileNum, gstrDataFilePath + lstrFileName, OpenMode.Append, OpenAccess.Write)
  '        'Headings
  '        Microsoft.VisualBasic.Print(lintFileNum, "Parameter Set,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Lot Name,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Date/Time,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Date Code,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Serial Number,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Shift,")
  '        'For i = 0 To gintMetricRows - 1
  '        '  Microsoft.VisualBasic.Print(lintFileNum, gstrMetricID(i) & ",")
  '        'Next i
  '        For i = 0 To galDataFileMetricHeaders.Count - 1
  '          Microsoft.VisualBasic.Print(lintFileNum, galDataFileMetricHeaders(i) & ",")
  '        Next i
  '        Microsoft.VisualBasic.Print(lintFileNum, "PASS/FAIL,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Operator,")
  '        Microsoft.VisualBasic.Print(lintFileNum, "Temperature,")
  '                  Microsoft.VisualBasic.Print(lintFileNum, "Comment,")
  '                  Microsoft.VisualBasic.Print(lintFileNum, "Pallet" & Chr(10))
  '        'Values
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboParameterSets.Text & ",")
  '        'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboLotFiles.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.lblLotName.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, Microsoft.VisualBasic.DateAndTime.Now & ",")
  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.DateCode) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.DateCode).Text & ",")
  '          End If

  '          Microsoft.VisualBasic.Print(lintFileNum, gDeviceInProcess.EncodedSerialNumber & ",")

  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Shift) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Shift).Text & ",")
  '          Else
  '          End If
  '        Else
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtDateCode.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, gDeviceInProcess.EncodedSerialNumber & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtShift.Text & ",")
  '        End If

  '        'For i = 0 To gintMetricRows - 1
  '        '  Microsoft.VisualBasic.Print(lintFileNum, gstrMetricValue(i) & ",")
  '        'Next i
  '        For i = 0 To galDataFileMetricValues.Count - 1
  '          Microsoft.VisualBasic.Print(lintFileNum, galDataFileMetricValues(i) & ",")
  '        Next i
  '        If bTestPassed Then
  '          Microsoft.VisualBasic.Print(lintFileNum, "PASSED,")
  '        Else
  '          Microsoft.VisualBasic.Print(lintFileNum, "REJECT,")
  '        End If
  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.OperatorName) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.OperatorName).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Temperature) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Temperature).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Comment) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Comment).Text & ",")
  '                      End If
  '        Else
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtOperator.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtTemperature.Text & ",")
  '          '            Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtComment.Text & ",")
  '                  End If
  '                  Microsoft.VisualBasic.Print(lintFileNum, gDeviceInProcess.Pallet & Chr(10))
  '      Else
  '        'FileOpen(lintFileNum, lstrPath + lstrFileName, OpenMode.Append, OpenAccess.Write)
  '        FileOpen(lintFileNum, gstrDataFilePath + lstrFileName, OpenMode.Append, OpenAccess.Write)

  '        'Just values
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboParameterSets.Text & ",")
  '        'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.cboLotFiles.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.lblLotName.Text & ",")
  '        Microsoft.VisualBasic.Print(lintFileNum, Microsoft.VisualBasic.DateAndTime.Now & ",")

  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.DateCode) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.DateCode).Text & ",")
  '          Else
  '            Microsoft.VisualBasic.Print(lintFileNum, "" & ",")
  '          End If

  '          Microsoft.VisualBasic.Print(lintFileNum, gDeviceInProcess.EncodedSerialNumber & ",")

  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Shift) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Shift).Text & ",")
  '          Else
  '            Microsoft.VisualBasic.Print(lintFileNum, "" & ",")
  '          End If
  '        Else
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtDateCode.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, gDeviceInProcess.EncodedSerialNumber & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtShift.Text & ",")
  '        End If

  '        'For i = 0 To gintMetricRows - 1
  '        '  Microsoft.VisualBasic.Print(lintFileNum, gstrMetricValue(i) & ",")
  '        'Next i
  '        For i = 0 To galDataFileMetricValues.Count - 1
  '          Microsoft.VisualBasic.Print(lintFileNum, galDataFileMetricValues(i) & ",")
  '        Next i
  '        If bTestPassed Then
  '          Microsoft.VisualBasic.Print(lintFileNum, "PASSED,")
  '        Else
  '          Microsoft.VisualBasic.Print(lintFileNum, "REJECT,")
  '        End If
  '        If gControlFlags.UsesDynamicSetupInfo Then
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.OperatorName) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.OperatorName).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Temperature) Then
  '            Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Temperature).Text & ",")
  '          End If
  '          If gSetupInfoControls.ContainsKey(SetupInfoItems.Comment) Then
  '                          Microsoft.VisualBasic.Print(lintFileNum, gSetupInfoControls(SetupInfoItems.Comment).Text & ",")
  '          End If
  '        Else
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtOperator.Text & ",")
  '          'Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtTemperature.Text & ",")
  '          '            Microsoft.VisualBasic.Print(lintFileNum, frmTsopMain.txtComment.Text & ",")
  '        End If
  '                  Microsoft.VisualBasic.Print(lintFileNum, gDeviceInProcess.Pallet & Chr(10))
  '              End If
  '        FileClose(lintFileNum)
  '      End If


  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub SelectDefaultTabPage() ' 3.0.0.0 TER
    Dim myForm As frmTsopMain
    Dim myTabPage As TabPage
    Try

      myForm = My.Application.OpenForms("frmTsopMain")
      If gstrDefaultTabPage <> "" Then
        myTabPage = myForm.tabResults.TabPages(gstrDefaultTabPage)
        myForm.tabResults.SelectedTab = myTabPage
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SelectTabPageResults() ' 2.0.1.0 TER
    Dim myForm As frmTsopMain
    Dim myTabPage As TabPage
    Try

      myForm = My.Application.OpenForms("frmTsopMain")
      myTabPage = myForm.tabResults.TabPages("FTResults")

      myForm.tabResults.SelectedTab = myTabPage

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub




  Public Sub SetDipAttributes_EncodedSerialNumber()
    Dim drAttributes As DataRow
    Try

      If gControlFlags.MasterMode Then
        gDeviceInProcess.EncodedSerialNumber = gstrMasterNumber
      End If

      gDeviceInProcess.AttributesTable.Clear()

      If gDeviceInProcess.EncodedSerialNumber <> "" Then
        drAttributes = gDeviceInProcess.AttributesTable.NewRow
        drAttributes("AttributeName") = "EncodedSerialNumber"
        drAttributes("AttributeValue") = gDeviceInProcess.EncodedSerialNumber
        drAttributes("IsIdentifier") = 1
        gDeviceInProcess.AttributesTable.Rows.Add(drAttributes)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetDipAttributes_ExternalSerialNumber()
    Dim drAttributes As DataRow
    Dim strExternalSerialNumber As String

    Try
      strExternalSerialNumber = ""
      gDeviceInProcess.AttributesTable.Clear()

      If gControlFlags.UsesDynamicSetupInfo Then
        If gSetupInfoControls.ContainsKey(SetupInfoItems.ExternalSerialNumber) Then
          strExternalSerialNumber = gSetupInfoControls(SetupInfoItems.ExternalSerialNumber).Text
        End If
      Else
        'strExternalSerialNumber = frmTsopMain.txtExternalSerialNumber.Text
      End If

      gDeviceInProcess.ExternalSerialNumber = strExternalSerialNumber

      If strExternalSerialNumber <> "" Then
        drAttributes = gDeviceInProcess.AttributesTable.NewRow
        drAttributes("AttributeName") = "ExternalSerialNumber"
        drAttributes("AttributeValue") = gDeviceInProcess.ExternalSerialNumber
        drAttributes("IsIdentifier") = 1
        gDeviceInProcess.AttributesTable.Rows.Add(drAttributes)
      End If

      'gDeviceInProcess.EncodedSerialNumber = "TimTest525ESN"
      'drAttributes = gDeviceInProcess.AttributesTable.NewRow
      'drAttributes("AttributeName") = "EncodedSerialNumber"
      'drAttributes("AttributeValue") = gDeviceInProcess.EncodedSerialNumber
      'drAttributes("IsIdentifier") = 1
      'gDeviceInProcess.AttributesTable.Rows.Add(drAttributes)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetDipAttributes_IncrementalSerialNumberInitial()
    Dim drAttributes As DataRow
    Try

      gintSerialNumberInitial += 1

      'strDateCode = gstrLotDayOfYear & gstrLotYear & gstrLotShiftName

      gDeviceInProcess.DateCode = gstrLotName
      gDeviceInProcess.EncodedSerialNumber = gDeviceInProcess.DateCode & CStr(Format(gintSerialNumberInitial, "00000")) & "Initial"

      gDeviceInProcess.AttributesTable.Clear()

      drAttributes = gDeviceInProcess.AttributesTable.NewRow
      drAttributes("AttributeName") = "DateCode"
      drAttributes("AttributeValue") = gDeviceInProcess.DateCode
      drAttributes("IsIdentifier") = 0
      gDeviceInProcess.AttributesTable.Rows.Add(drAttributes)

      drAttributes = gDeviceInProcess.AttributesTable.NewRow
      drAttributes("AttributeName") = "EncodedSerialNumber"
      drAttributes("AttributeValue") = gDeviceInProcess.EncodedSerialNumber
      drAttributes("IsIdentifier") = 1
      gDeviceInProcess.AttributesTable.Rows.Add(drAttributes)

      'Update Product Serial Numbers
      Call gDatabase.UpdateProductSerialNumbers(gDeviceInProcess.DateCode, gintSerialNumberInitial, gintSerialNumberFinal)
      If Not gDatabase.Anomaly Is Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      If gControlFlags.UsesOverviewScreen Then
        gOverviewScreenControls(OverviewScreenControls.SerialNumber).Text = gDeviceInProcess.EncodedSerialNumber
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetDipAttributes_IncrementalSerialNumberFinal()
    Try

      If Not gControlFlags.TestFailure = True Then
        gintSerialNumberFinal += 1

        gDeviceInProcess.EncodedSerialNumber = gDeviceInProcess.DateCode & CStr(Format(gintSerialNumberFinal, "00000"))

        'Update Product Serial Numbers
        Call gDatabase.UpdateProductSerialNumbers(gDeviceInProcess.DateCode, gintSerialNumberInitial, gintSerialNumberFinal)
        If Not gDatabase.Anomaly Is Nothing Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If

        'Update Dip Attributes`
        Call gDatabase.UpdateDipAttributeValue("EncodedSerialNumber", gDeviceInProcess.EncodedSerialNumber)
        If Not gDatabase.Anomaly Is Nothing Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
      End If

      If gControlFlags.UsesOverviewScreen Then
        gOverviewScreenControls(OverviewScreenControls.SerialNumber).Text = gDeviceInProcess.EncodedSerialNumber
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetNextTaskListStep(ByVal strModuleName As String, ByVal strMethodName As String)
    Dim intKey As Integer
    Dim strMessage As String
    Try
      gintNextTaskStep = 0

      For Each intKey In gFilteredTaskList.Keys
        If gFilteredTaskList(intKey).Item("ModuleName") = strModuleName And gFilteredTaskList(intKey).Item("MethodName") = strMethodName Then
          gintNextTaskStep = intKey
          Exit For
        End If
      Next

      If gintNextTaskStep = 0 Then
        strMessage = "Can't Find Next Task Step: " & strModuleName & "." & strMethodName
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strMessage, gDatabase)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub SetTsopSubProcessFunctionalTest()
    Try
      'Set subprocess to be "Initialization"
      gDatabase.SubProcessID = gSubProcesses(SubProcessEnum.FunctionalTest.ToString)


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetTsopSubProcessProgramming()
    Try
      'Set subprocess to be "Initialization"
      gDatabase.SubProcessID = gSubProcesses(SubProcessEnum.Programming.ToString)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub StopLotShiftTimer()
    Try

      gtmrLotShift.Stop()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateDisplayForCurrentLot()
    Dim metMetric As Metric
    Try
      For Each metMetric In gMetrics.Values
        metMetric.MetricLocation = Nothing
        metMetric.MetricValue = Nothing
        metMetric.PassFailStatus = Metric.PassFailStatusEnum.NoData
        metMetric.Statistics = New Statistics
        metMetric.Statistics.MetricID = metMetric.FunctionMetricID
      Next
      gTestLot.LoadFromDb(gDatabase, "FunctionalTest")
      gProgLot.LoadFromDb(gDatabase, "Programming")
      'reset currnet part counters
      gStationMonitorAndAlarm.CurrentProgPassCount = 0
      gStationMonitorAndAlarm.ConsecutiveAnomalyCount = 0
      gStationMonitorAndAlarm.CurrentProgTotalCount = 0
      gStationMonitorAndAlarm.CurrentTestPassCount = 0
      gStationMonitorAndAlarm.CurrentTestTotalCount = 0

      'Load Lot Metric Statistics
      LoadLotMetricStatsFromDb()

      'Update Display
      Call UpdateLotDataDisplay()

      If gControlAndInterface.UsesProgramming Then
        'Call UpdateProgLotDataDisplay()
      End If

      Call DisplayMetrics()
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

      'Display message to user
      frmTsopMain.StatusLabel1.Text = "Enter Remaining Setup Information or Begin Test"
      Call LogActivity(frmTsopMain.StatusLabel1.Text)

      gControlFlags.LotFileLoaded = True
      frmTsopMain.Cursor = Cursors.Arrow

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateLotDataDisplay()
    Dim lfrmUpdate As frmTsopMain = My.Application.OpenForms("frmTsopMain")
    Dim argArray(1) As Object
    Try
      'argArray(0) = lfrmUpdate.txtTestCountTotalUnits
      'argArray(1) = CStr(gTestLot.CountTotalUnits)
      'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
      lfrmUpdate.txtTestCountTotalUnits.Text = gTestLot.CountTotalUnits

      'argArray(0) = lfrmUpdate.txtTestCountUnitsPassed
      'argArray(1) = CStr(gTestLot.CountUnitsPassed)
      'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
      lfrmUpdate.txtTestCountUnitsPassed.Text = gTestLot.CountUnitsPassed

      'argArray(0) = lfrmUpdate.txtTestCountUnitsRejected
      'argArray(1) = CStr(gTestLot.CountUnitsRejected)
      'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
      lfrmUpdate.txtTestCountUnitsRejected.Text = gTestLot.CountUnitsRejected

      'argArray(0) = lfrmUpdate.txtTestCountErrors
      'argArray(1) = CStr(gTestLot.CountErrors)
      'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
      lfrmUpdate.txtTestCountErrors.Text = gTestLot.CountErrors

      'argArray(0) = lfrmUpdate.txtTestCurrentYield
      'argArray(1) = CStr(Math.Round((gStationMonitorAndAlarm.CurrentTestPassCount / gStationMonitorAndAlarm.CurrentTestTotalCount), 4, MidpointRounding.AwayFromZero) * 100)
      'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
      lfrmUpdate.txtTestCurrentYield.Text = CStr(Math.Round((gStationMonitorAndAlarm.CurrentTestPassCount / gStationMonitorAndAlarm.CurrentTestTotalCount), 4, MidpointRounding.AwayFromZero) * 100)

      'argArray(0) = lfrmUpdate.lblTestCurrentYield
      'argArray(1) = CStr(gStationMonitorAndAlarm.CurrentTestTotalCount & " Part Yield")
      'lfrmUpdate.Invoke(New UpdateLabelText(AddressOf lfrmUpdate.UpdateLabel), argArray)
      lfrmUpdate.lblTestCurrentYield.Text = CStr(gStationMonitorAndAlarm.CurrentTestTotalCount & " Part Yield")

      'argArray(0) = lfrmUpdate.txtTestLotYield
      'argArray(1) = CStr(Math.Round((gTestLot.CountUnitsPassed / gTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100)
      'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
      lfrmUpdate.txtTestLotYield.Text = CStr(Math.Round((gTestLot.CountUnitsPassed / gTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100)

      ''set color of txtbox based on yield parameters
      ''current yield
      If (Math.Round((gStationMonitorAndAlarm.CurrentTestPassCount / gStationMonitorAndAlarm.CurrentTestTotalCount), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
        'argArray(0) = lfrmUpdate.txtTestCurrentYield
        'argArray(1) = Color.Lime
        'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
        lfrmUpdate.txtTestCurrentYield.BackColor = Color.Lime
      ElseIf (Math.Round((gStationMonitorAndAlarm.CurrentTestPassCount / gStationMonitorAndAlarm.CurrentTestTotalCount), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.YellowYieldLimit Then
        'argArray(0) = lfrmUpdate.txtTestCurrentYield
        'argArray(1) = Color.Yellow
        'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
        lfrmUpdate.txtTestCurrentYield.BackColor = Color.Yellow
      Else
        'argArray(0) = lfrmUpdate.txtTestCurrentYield
        'argArray(1) = Color.Red
        'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
        lfrmUpdate.txtTestCurrentYield.BackColor = Color.Red
      End If

      'lot yield
      If (Math.Round((gTestLot.CountUnitsPassed / gTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
        'argArray(0) = lfrmUpdate.txtTestLotYield
        'argArray(1) = Color.Lime
        'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
        lfrmUpdate.txtTestLotYield.BackColor = Color.Lime
      ElseIf (Math.Round((gTestLot.CountUnitsPassed / gTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.YellowYieldLimit Then
        'argArray(0) = lfrmUpdate.txtTestLotYield
        'argArray(1) = Color.Yellow
        'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
        lfrmUpdate.txtTestLotYield.BackColor = Color.Yellow
      Else
        'argArray(0) = lfrmUpdate.txtTestLotYield
        'argArray(1) = Color.Red
        'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
        lfrmUpdate.txtTestLotYield.BackColor = Color.Red
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub UpdateProgLotDataDisplay()
  '  Dim lfrmUpdate As frmTsopMain = My.Application.OpenForms("frmTsopMain")
  '  Dim argArray(1) As Object

  '  Try

  '    'Program
  '    'argArray(0) = lfrmUpdate.txtProgCountTotalUnits
  '    'argArray(1) = CStr(gProgLot.CountTotalUnits)
  '    'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
  '    lfrmUpdate.txtProgCountTotalUnits.Text = gProgLot.CountTotalUnits

  '    'argArray(0) = lfrmUpdate.txtProgCountUnitsPassed
  '    'argArray(1) = CStr(gProgLot.CountUnitsPassed)
  '    'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
  '    lfrmUpdate.txtProgCountUnitsPassed.Text = gProgLot.CountUnitsPassed

  '    'argArray(0) = lfrmUpdate.txtProgCountUnitsRejected
  '    'argArray(1) = CStr(gProgLot.CountUnitsRejected)
  '    'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
  '    lfrmUpdate.txtProgCountUnitsRejected.Text = gProgLot.CountUnitsRejected

  '    'argArray(0) = lfrmUpdate.txtProgCountErrors
  '    'argArray(1) = CStr(gProgLot.CountErrors)
  '    'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
  '    lfrmUpdate.txtProgCountErrors.Text = gProgLot.CountErrors

  '    'argArray(0) = lfrmUpdate.txtProgCurrentYield
  '    'argArray(1) = CStr(Math.Round((gStationMonitorAndAlarm.CurrentProgPassCount / gStationMonitorAndAlarm.CurrentProgTotalCount), 4, MidpointRounding.AwayFromZero) * 100)
  '    'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
  '    lfrmUpdate.txtProgCurrentYield.Text = CStr(Math.Round((gStationMonitorAndAlarm.CurrentProgPassCount / gStationMonitorAndAlarm.CurrentProgTotalCount), 4, MidpointRounding.AwayFromZero) * 100)

  '    'argArray(0) = lfrmUpdate.lblProgCurrentYield
  '    'argArray(1) = CStr(gStationMonitorAndAlarm.CurrentProgTotalCount & " Part Yield")
  '    'lfrmUpdate.Invoke(New UpdateLabelText(AddressOf lfrmUpdate.UpdateLabel), argArray)
  '    lfrmUpdate.lblProgCurrentYield.Text = CStr(gStationMonitorAndAlarm.CurrentProgTotalCount & " Part Yield")

  '    'argArray(0) = lfrmUpdate.txtProgLotYield
  '    'argArray(1) = CStr(Math.Round((gProgLot.CountUnitsPassed / gProgLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100)
  '    'lfrmUpdate.Invoke(New UpdateTextBox(AddressOf lfrmUpdate.UpdateText), argArray)
  '    lfrmUpdate.txtProgLotYield.Text = CStr(Math.Round((gProgLot.CountUnitsPassed / gProgLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100)

  '    ''set color of txtbox based on yield parameters
  '    If (Math.Round((gStationMonitorAndAlarm.CurrentProgPassCount / gStationMonitorAndAlarm.CurrentProgTotalCount), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
  '      'argArray(0) = lfrmUpdate.txtProgCurrentYield
  '      'argArray(1) = Color.Lime
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtProgCurrentYield.BackColor = Color.Lime
  '    ElseIf (Math.Round((gStationMonitorAndAlarm.CurrentProgPassCount / gStationMonitorAndAlarm.CurrentProgTotalCount), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
  '      'argArray(0) = lfrmUpdate.txtProgCurrentYield
  '      'argArray(1) = Color.Yellow
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtProgCurrentYield.BackColor = Color.Yellow
  '    Else
  '      'argArray(0) = lfrmUpdate.txtProgCurrentYield
  '      'argArray(1) = Color.Red
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtProgCurrentYield.BackColor = Color.Red
  '    End If

  '    'lot yield
  '    If (Math.Round((gTestLot.CountUnitsPassed / gTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
  '      'argArray(0) = lfrmUpdate.txtTestLotYield
  '      'argArray(1) = Color.Lime
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtTestLotYield.BackColor = Color.Lime
  '    ElseIf (Math.Round((gTestLot.CountUnitsPassed / gTestLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
  '      'argArray(0) = lfrmUpdate.txtTestLotYield
  '      'argArray(1) = Color.Yellow
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtTestLotYield.BackColor = Color.Yellow
  '    Else
  '      'argArray(0) = lfrmUpdate.txtTestLotYield
  '      'argArray(1) = Color.Red
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtTestLotYield.BackColor = Color.Red
  '    End If
  '    'Prog
  '    If (Math.Round((gProgLot.CountUnitsPassed / gProgLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
  '      'argArray(0) = lfrmUpdate.txtProgLotYield
  '      'argArray(1) = Color.Lime
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtProgLotYield.BackColor = Color.Lime
  '    ElseIf (Math.Round((gProgLot.CountUnitsPassed / gProgLot.CountTotalUnits), 4, MidpointRounding.AwayFromZero) * 100) > gStationMonitorAndAlarm.GreenYieldLimit Then
  '      'argArray(0) = lfrmUpdate.txtProgLotYield
  '      'argArray(1) = Color.Yellow
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtProgLotYield.BackColor = Color.Yellow
  '    Else
  '      'argArray(0) = lfrmUpdate.txtProgLotYield
  '      'argArray(1) = Color.Red
  '      'lfrmUpdate.Invoke(New UpdateTextBoxColor(AddressOf lfrmUpdate.UpdateTextBoxBackgroundColor), argArray)
  '      lfrmUpdate.txtProgLotYield.BackColor = Color.Red
  '    End If


  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub UpdatePassFailStatus()
    Dim lfrmUpdate As frmTsopMain = My.Application.OpenForms("frmTsopMain")
    Dim argArray(1) As Object
    Try

      ''update pass/fail indicator
      ReDim argArray(0)
      If gControlFlags.ProgFailure Or gControlFlags.TestFailure Or gControlFlags.TestError Or gControlFlags.ProgError Then

        If gControlFlags.UsesOverviewScreen Then
          gOverviewProgressBar.Value = 100
          gOverviewProgressLabel.Text = "100 % Complete"
          gtmrUpdateStatusBarTimer.Enabled = False
        End If

        'argArray(0) = Metric.PassFailStatusEnum.Reject
        'lfrmUpdate.Invoke(New UpdatePasFailIndicator(AddressOf UpdatePassFailLED), argArray)
        Call UpdatePassFailLED(Metric.PassFailStatusEnum.Reject)
        If gControlFlags.UsesExternalDevices Then
          gDigitalOutput.Write(0, gDIOStructure.RedLight)
        End If

      Else
        'argArray(0) = Metric.PassFailStatusEnum.Good
        'lfrmUpdate.Invoke(New UpdatePasFailIndicator(AddressOf UpdatePassFailLED), argArray)
        Call UpdatePassFailLED(Metric.PassFailStatusEnum.Good)
        If gControlFlags.UsesExternalDevices Then
          gDigitalOutput.Write(0, gDIOStructure.GreenLight)
        End If

      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateProgLotCounts()
    Try
      If gStationMonitorAndAlarm.CurrentProgTotalCount > gStationMonitorAndAlarm.CurrentYieldCountLimit Then
        gStationMonitorAndAlarm.CurrentProgTotalCount = 0
        gStationMonitorAndAlarm.CurrentProgPassCount = 0
      End If

      'update lot counts
      'If gintAnomaly = 0 Then
      If gControlFlags.ProgFailure Then
        gProgLot.CountUnitsRejected = gProgLot.CountUnitsRejected + 1
      ElseIf gControlFlags.ProgError Then
        gProgLot.CountErrors = gProgLot.CountErrors + 1
      Else
        gProgLot.CountUnitsPassed = gProgLot.CountUnitsPassed + 1
        gStationMonitorAndAlarm.CurrentProgPassCount = gStationMonitorAndAlarm.CurrentProgPassCount + 1
      End If

      'increment part count
      gProgLot.CountTotalUnits = gProgLot.CountTotalUnits + 1
      gStationMonitorAndAlarm.CurrentProgTotalCount = gStationMonitorAndAlarm.CurrentProgTotalCount + 1
      'save lot data
      gProgLot.SaveToDb(gDatabase)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdateRTYOEE()

    If gControlFlags.MasterMode Or gControlFlags.IgnoreTestBits Then Exit Sub
    Try

      Call PrepareRTYData()
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub


  Public Sub UpdateTestLotCounts()
    Try
      If gStationMonitorAndAlarm.CurrentTestTotalCount > gStationMonitorAndAlarm.CurrentYieldCountLimit Then
        gStationMonitorAndAlarm.CurrentTestTotalCount = 0
        gStationMonitorAndAlarm.CurrentTestPassCount = 0
      End If

      'update lot counts
      'If gintAnomaly = 0 Then
      If gControlFlags.TestFailure Then
        gTestLot.CountUnitsRejected = gTestLot.CountUnitsRejected + 1
      ElseIf gControlFlags.TestError Then
        gTestLot.CountErrors = gTestLot.CountErrors + 1
      Else
        gTestLot.CountUnitsPassed = gTestLot.CountUnitsPassed + 1
        gStationMonitorAndAlarm.CurrentTestPassCount = gStationMonitorAndAlarm.CurrentTestPassCount + 1
      End If
      'Else
      ''increment scan error counter
      'TsopTestLot.CountErrors = TsopTestLot.CountErrors + 1
      'SMA.ConsecutiveAnomalyCount = SMA.ConsecutiveAnomalyCount + 1
      'End If

      'increment part count
      gTestLot.CountTotalUnits = gTestLot.CountTotalUnits + 1
      gStationMonitorAndAlarm.CurrentTestTotalCount = gStationMonitorAndAlarm.CurrentTestTotalCount + 1
      'save lot data
      gTestLot.SaveToDb(gDatabase)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub VerifySetupInfo() ' 2.0.1.0 TER
    Dim strkey As String
    Dim strErrorMessage As String
    Try

      For Each strkey In gSetupInfoControls.Keys
        If strkey <> SetupInfoItems.Comment.ToString Then
          If gSetupInfoControls(strkey).Text = "" Then
            gControlFlags.AbortTest = True
            strErrorMessage = "Missing Setup Information. Please enter value for: " & strkey
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
            Throw New TsopAnomalyException
          End If
        End If
      Next


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public gblnRetestOK As Boolean = False

  Public Sub CheckForValidPartNumber()
    'for now do nothing
    Dim strHardwarePartNumber As String
    Dim blnResult As Boolean
    Dim strStatusMessage As String
    Dim strResult As String


    Try
      strStatusMessage = ""
      gstrRoutingMessage = ""
      strHardwarePartNumber = ""
      strResult = ""

      If Not gControlFlags.MasterMode Then ' don't bother for master because supply voltage is not set high

        'set voltage to 13.6 V
        SetPowerSupply12V()

        'Make sure CAN is working
        gCanActuator.ReStartCAN()

                'Read Hardware Part Number
                If gstrRoutingMessage = "" Then
          frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading Hardware Part Number High Bytes"
          Call LogActivity(frmTsopMain.StatusLabel2.Text)
          Application.DoEvents()
          blnResult = gCanActuator.ReadHardwarePartNumberHighBytes(strResult)
          If gCanActuator.Anomaly IsNot Nothing Then
            gstrRoutingMessage = "Error Reading Hardware Part Number"
            gCanActuator.Anomaly = Nothing
          Else
            strHardwarePartNumber = strResult
          End If
          frmTsopMain.StatusLabel2.Text = strStatusMessage & "-" & "Reading Hardware Part Number Low Bytes"
          Call LogActivity(frmTsopMain.StatusLabel2.Text)
          blnResult = gCanActuator.ReadHardwarePartNumberLowBytes(strResult)
          If gCanActuator.Anomaly IsNot Nothing Then
            gstrRoutingMessage = "Error Reading Hardware Part Number"
            gCanActuator.Anomaly = Nothing
          Else
            strHardwarePartNumber = strHardwarePartNumber & strResult
          End If
        End If
        If gstrRoutingMessage = "" Then
          gblnRetestOK = False
          If Trim(strHardwarePartNumber) <> Trim(gProductProgramming.HardwarePartNumber) Then
            gstrRoutingMessage = "Hardware Part Number in Actuator does not match BOM - Call PC or Supervisor to correct BOM, part OK for retest"
            gblnRetestOK = True
            gControlFlags.AbortTest = True
          End If
          'write results in Activity log
          LogActivity(gstrRoutingMessage + " BOM " + gDeviceInProcess.BOM + " " + gProductProgramming.HardwarePartNumber + " device " + strHardwarePartNumber)
        Else
          LogActivity(gstrRoutingMessage)
        End If
        If gstrRoutingMessage <> "" Then
          gAnomaly = New clsAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, gstrRoutingMessage)
          Throw New TsopAnomalyException
        End If

      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


End Module
