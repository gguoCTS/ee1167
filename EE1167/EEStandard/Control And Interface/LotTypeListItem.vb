Option Explicit On
Option Strict Off
Public Class LotTypeListItem

    Private mID As Guid
    Private mValue As String

    Public Sub New(ByVal id As Guid, ByVal strValue As String)
        Me.mID = id
        Me.mValue = strValue
    End Sub

    Public ReadOnly Property ID() As Guid
        Get
            Return Me.mID
        End Get
    End Property

    Public ReadOnly Property Value() As String
        Get
            Return Me.mValue
        End Get
    End Property

    Public Overrides Function ToString() As String
        Return Me.mValue
    End Function

End Class
