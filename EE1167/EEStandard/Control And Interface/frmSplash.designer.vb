<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSplash
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSplash))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.txtAppTitle = New System.Windows.Forms.TextBox
        Me.txtCopyright = New System.Windows.Forms.TextBox
        Me.txtversion = New System.Windows.Forms.TextBox
        Me.lblEENumber = New System.Windows.Forms.Label
        Me.btnClose = New System.Windows.Forms.Button
        Me.lblCAN_ACT_DLLver = New System.Windows.Forms.Label
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(12, 9)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(294, 106)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'txtAppTitle
        '
        Me.txtAppTitle.BackColor = System.Drawing.SystemColors.Control
        Me.txtAppTitle.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtAppTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAppTitle.Location = New System.Drawing.Point(43, 121)
        Me.txtAppTitle.Multiline = True
        Me.txtAppTitle.Name = "txtAppTitle"
        Me.txtAppTitle.Size = New System.Drawing.Size(439, 33)
        Me.txtAppTitle.TabIndex = 1
        Me.txtAppTitle.TabStop = False
        Me.txtAppTitle.Text = "App Title"
        '
        'txtCopyright
        '
        Me.txtCopyright.BackColor = System.Drawing.SystemColors.Control
        Me.txtCopyright.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtCopyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCopyright.Location = New System.Drawing.Point(321, 52)
        Me.txtCopyright.Multiline = True
        Me.txtCopyright.Name = "txtCopyright"
        Me.txtCopyright.Size = New System.Drawing.Size(146, 63)
        Me.txtCopyright.TabIndex = 2
        Me.txtCopyright.TabStop = False
        Me.txtCopyright.Text = "Copyright"
        '
        'txtversion
        '
        Me.txtversion.BackColor = System.Drawing.SystemColors.Control
        Me.txtversion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtversion.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtversion.Location = New System.Drawing.Point(43, 163)
        Me.txtversion.Name = "txtversion"
        Me.txtversion.Size = New System.Drawing.Size(283, 19)
        Me.txtversion.TabIndex = 3
        Me.txtversion.TabStop = False
        Me.txtversion.Text = "version:"
        '
        'lblEENumber
        '
        Me.lblEENumber.AutoSize = True
        Me.lblEENumber.Location = New System.Drawing.Point(329, 22)
        Me.lblEENumber.Name = "lblEENumber"
        Me.lblEENumber.Size = New System.Drawing.Size(43, 13)
        Me.lblEENumber.TabIndex = 4
        Me.lblEENumber.Text = "EE10xx"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(424, 185)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(58, 22)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        Me.btnClose.Visible = False
        '
        'lblCAN_ACT_DLLver
        '
        Me.lblCAN_ACT_DLLver.AutoSize = True
        Me.lblCAN_ACT_DLLver.Location = New System.Drawing.Point(40, 197)
        Me.lblCAN_ACT_DLLver.Name = "lblCAN_ACT_DLLver"
        Me.lblCAN_ACT_DLLver.Size = New System.Drawing.Size(13, 13)
        Me.lblCAN_ACT_DLLver.TabIndex = 6
        Me.lblCAN_ACT_DLLver.Text = "_"
        '
        'frmSplash
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(494, 219)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblCAN_ACT_DLLver)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.lblEENumber)
        Me.Controls.Add(Me.txtversion)
        Me.Controls.Add(Me.txtCopyright)
        Me.Controls.Add(Me.txtAppTitle)
        Me.Controls.Add(Me.PictureBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSplash"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents txtAppTitle As System.Windows.Forms.TextBox
    Friend WithEvents txtCopyright As System.Windows.Forms.TextBox
    Friend WithEvents txtversion As System.Windows.Forms.TextBox
    Friend WithEvents lblEENumber As System.Windows.Forms.Label
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lblCAN_ACT_DLLver As System.Windows.Forms.Label

End Class
