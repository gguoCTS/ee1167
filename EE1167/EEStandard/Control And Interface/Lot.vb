Option Explicit On
Option Strict Off
Imports system.xml.Serialization
Imports System.IO
Imports System.Text

Public Class Lot

  Private mCountTotalUnits As Int16
  Private mCountUnitsPassed As Int16
  Private mCountUnitsRejected As Int16
  Private mCountErrors As Int16
  Private mCountSevere As Int16
  Private mTsopSubProcessID As Guid

  Public Property CountTotalUnits() As Int16
    Get
      Return Me.mCountTotalUnits
    End Get
    Set(ByVal value As Int16)
      Me.mCountTotalUnits = value
    End Set
  End Property

  Public Property CountUnitsPassed() As Int16
    Get
      Return Me.mCountUnitsPassed
    End Get
    Set(ByVal value As Int16)
      Me.mCountUnitsPassed = value
    End Set
  End Property

  Public Property CountUnitsRejected() As Int16
    Get
      Return Me.mCountUnitsRejected
    End Get
    Set(ByVal value As Int16)
      Me.mCountUnitsRejected = value
    End Set
  End Property

  Public Property CountErrors() As Int16
    Get
      Return Me.mCountErrors
    End Get
    Set(ByVal value As Int16)
      Me.mCountErrors = value
    End Set
  End Property

  Public Property CountSevere() As Int16
    Get
      Return Me.mCountSevere
    End Get
    Set(ByVal value As Int16)
      Me.mCountSevere = value
    End Set
  End Property

  Public Property TsopSubProcessID() As Guid
    Get
      Return Me.mTsopSubProcessID
    End Get
    Set(ByVal value As Guid)
      Me.mTsopSubProcessID = value
    End Set
  End Property

  Public Sub LoadFromDb(ByVal db As Database, ByVal strSubProcessName As String)
    Dim dt As DataTable = db.GetLotResultsDataTable(strSubProcessName)
    'Try

    Me.TsopSubProcessID = dt.Rows(0).Item("TSOP_SubProcessID")
    Me.CountTotalUnits = dt.Rows(0).Item("CountTotalUnits")
    Me.CountUnitsPassed = dt.Rows(0).Item("CountUnitsPassed")
    Me.CountUnitsRejected = dt.Rows(0).Item("CountUnitsRejected")
    Me.CountErrors = dt.Rows(0).Item("CountErrors")
    Me.CountSevere = dt.Rows(0).Item("CountSevere")

    'Catch ex As TsopAnomalyException
    '  mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    'Catch ex As Exception
    '  mAnomaly = New Anomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    'End Try

  End Sub

  Public Sub LoadPreviousLotFromDb(ByVal db As Database, ByVal strSubProcessName As String, ByVal lotID As Guid)
    Dim dt As DataTable = db.GetPreviousLotResultsDataTable(strSubProcessName, lotID)
    'Try

    Me.TsopSubProcessID = dt.Rows(0).Item("TSOP_SubProcessID")
    Me.CountTotalUnits = dt.Rows(0).Item("CountTotalUnits")
    Me.CountUnitsPassed = dt.Rows(0).Item("CountUnitsPassed")
    Me.CountUnitsRejected = dt.Rows(0).Item("CountUnitsRejected")
    Me.CountErrors = dt.Rows(0).Item("CountErrors")
    Me.CountSevere = dt.Rows(0).Item("CountSevere")

    'Catch ex As TsopAnomalyException
    '  mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    'Catch ex As Exception
    '  mAnomaly = New Anomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    'End Try

  End Sub

  Public Sub SaveToDb(ByVal db As Database)

    Dim serializer As XmlSerializer = New XmlSerializer(GetType(Lot))
    Dim sw As New StringWriter
    Dim sb As StringBuilder
    Dim intLength As Int16
    'Try
    serializer.Serialize(sw, Me)
    intLength = sw.ToString.IndexOf("<Lot")
    sb = sw.GetStringBuilder.Remove(0, intLength)
    'Debug.Print(sb.ToString)
    db.UpdateLotData(sb.ToString)
    sw.Close()

    'Catch ex As TsopAnomalyException
    '  mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    'Catch ex As Exception
    '  mAnomaly = New Anomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    'End Try

  End Sub

End Class
