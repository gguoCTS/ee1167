Imports System.Diagnostics

Public Class clsStackTrace
  Inherits StackTrace

#Region "Delegates =================================================="

#End Region

#Region "Events =================================================="

#End Region

#Region "Constructors =================================================="


#End Region

#Region "Private Member Variables =================================================="
  Dim mStackTrace As StackTrace

#End Region

#Region "Public Constants, Structures, Enums =================================================="

#End Region

#Region "Properties =================================================="
#End Region

#Region "Public Methods =================================================="
  Public Function CurrentNameSpace() As String
    mStackTrace = New StackTrace
    'Return Me.GetFrame(1).GetMethod.ReflectedType.ToString.Split(".")(0)
    Return mStackTrace.GetFrame(1).GetMethod.ReflectedType.ToString.Split(".")(0)
  End Function
  Public Function CurrentClassName() As String
    mStackTrace = New StackTrace
    Return mStackTrace.GetFrame(1).GetMethod.ReflectedType.ToString.Split(".")(1)
    'Return Me.GetFrame(1).GetMethod.ReflectedType.ToString.Split(".")(1)
  End Function

  Public Function CurrentFunctionName() As String
    mStackTrace = New StackTrace
    Return mStackTrace.GetFrame(1).GetMethod.Name
  End Function

#End Region


#Region "Private Methods =================================================="

#End Region

End Class
