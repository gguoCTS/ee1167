
Imports System.Diagnostics

Public Class clsStopWatch
  Inherits Stopwatch

#Region "Delegates =================================================="

#End Region

#Region "Events =================================================="

#End Region

#Region "Constructors =================================================="

#End Region

#Region "Private Member Variables =================================================="


#End Region

#Region "Public Constants, Structures, Enums =================================================="

#End Region

#Region "Properties =================================================="

#End Region

#Region "Public Methods =================================================="

  Public Sub DelayTime(ByVal milliseconds As Double)

    Me.Reset()
    Me.Start()
    Do
      System.Windows.Forms.Application.DoEvents()
      Threading.Thread.Sleep(1)
    Loop Until Me.ElapsedMilliseconds >= milliseconds
    Me.Stop()
    Me.Reset()
  End Sub

#End Region

#Region "Private Methods =================================================="

#End Region

End Class
