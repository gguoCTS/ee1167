Imports System.io
Imports System.Reflection
Imports System.Data.OleDb
Imports System.Linq

Module modControlAndInterface

#Region "Delegates =================================================="

  'Delegate Sub UpdateTextBox(ByVal txtBox As TextBox, ByVal text As String)
  'Delegate Sub UpdateLabelText(ByVal label As Label, ByVal text As String)
  'Delegate Sub UpdateTextBoxColor(ByVal txtBox As TextBox, ByVal backgroundColor As Color)
  'Delegate Sub UpdatePasFailIndicator(ByVal PassStatus As Metric.PassFailStatusEnum)
  Delegate Sub ConfigLotNamesMenu()
  Delegate Sub UpdateCurrentLotDisplay()

#End Region

#Region "Public Global Variables =================================================="

  'Public gstrPedalArmReturnDataFilePath As String ' 2.0.1.0 TER
  Public gAnomaly As clsDbAnomaly
  Public gtmrAntiTieDownTimer As New Timer
  Public gblnAntiTieDownTimeout As Boolean
  Public gstrActivityLogPath As String
  Public gActivityLogStopWatch As New clsStopWatch
  Public glstAnomaly As New List(Of clsDbAnomaly)
  Public gdtAppTaskList As DataTable
  Public gstrAssemblyName As String
  Public gintAutoRetestCount As Integer
  Public gintAutoRetestTaskStep As Integer

  Public gBtoBTimeStopWatch As New clsStopWatch

  Public gCalibrationConstant As CalibrationConstant
  Public gCanStatus As Integer
  Public gstrClassConfigFilePath As String
  Public gControlFlags As ControlFlags
  Public gControlAndInterface As ControlandInterface
  Public gConverter As New Conversion
  Public gintCurrentGraphPageNumber As Integer
  Public gintCurrentTask As Integer
  Public gCycleTimeStopWatch As New clsStopWatch

  Public gdtDaqDataArrayConfig As New DataTable
  Public gstrDaqDataArrayConfigFile As String
  Public gDataAcquisitionSystem As DataAcquisitionSystem
  Public gDatabase As Database
  Public gstrDataFileBasePath As String
  Public galDataFileMetricHeaders As New ArrayList
  Public galDataFileMetricValues As New ArrayList
  Public gstrDataFilePath As String
  Public gstrProgDataFilePath As String
  Public gstrDataFileSubFolderPath As String
  Public gstrProgFileSubFolderPath As String
  Public gstrDefaultLotType As String
  Public gstrDefaultTabPage As String ' 3.0.0.0 TER
  Public gDeviceInProcess As DeviceInProcess
  Public gDeviceMarker As clsDeviceMarker
  Public gdtDisplayGridColumns As New DataTable
  Public gdtDisplayGridRows As New DataTable
  Public gdtDisplayGrids As New DataTable

  Public gElectricalOperationAndLoad As ElectricalOperationAndLoad
  Public gdblExpectedCycleTime As Double

  Public gdtFilteredTaskList As DataTable
  Public gFilteredTaskList As New Dictionary(Of Integer, DataRow)

  Public gGraphDefaults As New GraphDefaults
  Public gstrGraphDefinitionFile As String
  Public gdtGraphDefinitions As New DataTable
  Public gstrGraphList As String()
  Public gstrGraphPlotDefinitionFile As String
  Public gstrGraphAxesDefinitionFile As String ' 3.1.0.1 TER
  Public gdtGraphPlotDefinitions As New DataTable
  Public gdtGraphAxesDefinitions As New DataTable ' 3.1.0.1 TER
  'Public gGraphsCollection As New Dictionary(Of String, clsGraph)
  Public gintGraphsPerPage As Integer

  'Public gLegendsCollection As New Dictionary(Of String, NationalInstruments.UI.WindowsForms.Legend)
  Public gstrLotDayOfYear As String
  Public gstrLotName As String
  Public gstrLotShiftName As String
  Public gstrLotYear As String
  Public gdtLotNames As New DataTable
  Public gintLotRun As String
  Public gtmrLotShift As New System.Timers.Timer
  Public gLotShiftDefinitions As New List(Of LotShiftDefinition)
  Public mnuLotTypesCombo As New ToolStripComboBox

  Public gstrMasterNumber As String

  Public gMechanicalOperationAndLoad As MechanicalOperationAndLoad
  Public gdtMenuDefinitions As New DataTable
  Public gstrMenuDefinitionsFile As String
  Public gMenuEvents As New Dictionary(Of String, System.Delegate)
  Public gMetricControlFlags As New Dictionary(Of String, Boolean)
  Public gdtMetricReportTable As New DataTable
  Public gstrMetricID(99) As String
  Public gintMetricRows As Integer
  Public gstrMetricValue(99) As String

  Public gintNextTaskStep As Integer

  Public mnuOptionsProductionSetDefault As New ToolStripMenuItem("Use Production Set")
  Public gtmrOperatorTouchStartTimer As New Timer
  Public gOverviewProgressLabel As Label
  Public gOverviewProgressBar As ProgressBar
  Public gOverviewScreenControls As New Dictionary(Of String, Control)
  Public gOverviewScreenPanel As Panel
  Public gdtOverviewScreenDefinitions As New DataTable
  Public gstrOverviewScreenDefinitionFile As String

  Public gdtParameterReportTable As New DataTable
  Public gintParameterSetPrintRow As Integer
  Public gdtPedalArmReleaseTypes As New DataTable ' 2.0.1.0 TER
  Public gdtPedalReturnStrokeLengths As New DataTable ' 3.1.0.0 TER
  Public gPrintedPageCells As New List(Of PrintedPageCell)
  Public gstrPrintedReportDefinitionsFile As String
  Public gdtPrintedReportDefinitions As New DataTable ' 3.1.0.1 TER
  Public gstrProcessName As String
  Public gblnProductionSet As Boolean
  Public gProductProgramming As ProductProgramming
  Public gdblPlotPointSizeHeight As Double
  Public gdblPlotPointSizeWidth As Double
  Public gstrPrintTableKey As String
  Public gstrPreviousLotDateTime As String
  Public gbPreviousLot As Boolean
  Public gstrPreviousLotRun As String

  Public gstrRoutingMessage As String

  Public gintScanTimeoutMilliseconds As Integer
  Public gintSerialNumberInitial As Integer
  Public gintSerialNumberFinal As Integer
  Public gstrSetupInfoItems As String() ' 2.0.1.0 TER
  Public gSetupInfoControls As New Dictionary(Of String, Control) ' 2.0.1.0 TER
  Public gtmrSendScannerInitTimer As New Timer
  'Public gSWTestTimeStopWatch As New clsStopWatch

  Public gStackTrace As New clsStackTrace
  Public gStopwatch As New clsStopWatch
  Public gSubProcesses As Dictionary(Of String, Guid)
  Public gStationMonitorAndAlarm As StationMonitorAndAlarm

  Public gdgvTableToPrint As DataGridView
  Public gTestStartTime As Date
  Public gTsopFunctionValues As Dictionary(Of String, Int32)
  Public gdtTsopInitTaskList As New DataTable
  Public gstrTsopInitTaskListFileName As String
  Public gTsopModeValues As Dictionary(Of Int32, Guid)

  Public gtmrUpdateStatusBarTimer As New Timer
  Public gUserTaskListFilter As TaskListFilterEnum
  Public gblnUseUnreleasedSets As Boolean
  Public Delegate Sub UpdateFrmTsopMainPosition(ByVal Position As Double)


#End Region

#Region "Public Constants, Structures, Enums =================================================="

  Public Enum AnomalyActionsEnum As Integer
    DisplayMessage = 0
    ThrowStationFault = 1
  End Enum

  Public Structure ControlFlags
    Public AbortTaskList As Boolean
    Public AbortTest As Boolean
    Public AutoRetestEnabled As Boolean
    Public CyclePartEnabled As Boolean
    Public LaserTestEnabled As Boolean
    Public DeviceMarkingEnabled As Boolean
    Public ENRScanEnabled As Boolean
    'Public FindHighEndStop As Boolean
    Public ForceScanEnabled As Boolean
    Public FullPedalTravelCheckEnabled As Boolean
    Public GoodOffsetAndGainCodes As Boolean
    Public HasADRDatabase As Boolean ' 2.0.1.0 TER
    Public IgnoreTestBits As Boolean
    Public IsLinearPart As Boolean
    Public KickdownEnable As Boolean
    Public LockPartAfterScan As Boolean
    Public LockIC As Boolean
    Public LockRejectParts As Boolean
    Public LockedPart As Boolean
    Public LotFileLoaded As Boolean
    Public LastShiftUseNextLotDay As Boolean
    Public MetricsLoaded As Boolean
    Public MasterMode As Boolean
    Public OnPlcStartProgram As Boolean
    Public OnPlcStartTest As Boolean
    Public ParametersLoaded As Boolean
    Public PauseTask As Boolean ' 2.0.1.0 TER
    Public PedalFaceFound As Boolean
    Public ProductionMode As Boolean
    Public ProgFailure As Boolean
    Public ProgError As Boolean
    Public RetryMeasurement As Boolean ' 2.0.1.0 TER
    Public ReverseScanOnly As Boolean
    Public SetupDone As Boolean
    Public StartScan As Boolean
    Public ManualScan As Boolean
    Public TestFailure As Boolean
    Public TestError As Boolean
    Public TestInProgress As Boolean
    Public TestStarted As Boolean
    Public TorqueScanEnabled As Boolean
    Public UsesExternalDevices As Boolean
    Public UsesOverviewScreen As Boolean
    Public UsesPLC As Boolean
    Public UsesProgramming As Boolean
    Public UsesADRDatabase As Boolean ' 2.0.1.0 TER
    Public UsesDynamicSetupInfo As Boolean ' 2.0.1.0 TER
    Public UsesMotionControl As Boolean ' 2.0.1.0 TER
    Public UsesDcMotor As Boolean ' 3.1.0.2 TER
  End Structure

  Public Structure GraphDefaults
    Public CaptionBackColor As System.Drawing.Color
    Public CaptionForeColor As System.Drawing.Color
    Public LocationX As Integer
    Public LocationY As Integer
    Public PlotAreaColor As System.Drawing.Color
    Public Width As Integer
    Public Height As Integer
    Public XAxis As GraphXAxisDefaults
    Public YAxis As GraphYAxisDefaults
    Public Legend As LegendDefaults
  End Structure

  Public Enum GraphNameEnum As Integer
    TwoPointLinearity = 0
    Hysteresis = 1
    Torque = 2
    ENR = 3
    VoltageGradient = 4
    Microgradient = 5
    EncoderData = 6
    RawVoltageData = 7
  End Enum

  Public Structure GraphXAxisDefaults
    Public AutoMinorDivisionFrequency As Integer
    Public AutoSpacing As Boolean
    Public MajorDivisionsBase As Boolean
    Public MajorDivisionsGridColor As System.Drawing.Color
    'Public MajorDivisionsGridLineStyle As NationalInstruments.UI.LineStyle
    Public MajorDivisionsGridVisible As Boolean
  End Structure

  Public Structure GraphYAxisDefaults
    Public AutoMinorDivisionFrequency As Integer
    Public AutoSpacing As Boolean
    Public MajorDivisionsBase As Boolean
    Public MajorDivisionsGridColor As System.Drawing.Color
    'Public MajorDivisionsGridLineStyle As NationalInstruments.UI.LineStyle
    Public MajorDivisionsGridVisible As Boolean
    'Public AxisMode As NationalInstruments.UI.AxisMode
  End Structure

  Public Structure LegendDefaults
    Public LocationX As Integer
    Public LocationY As Integer
    Public Width As Integer
    Public Height As Integer
    Public Visible As Boolean
    'Public Border As NationalInstruments.UI.Border
  End Structure

  Public Structure LotShiftDefinition
    Public ShiftName As String
    Public StartTime As String
    Public EndTime As String
  End Structure

  Public Enum MetricControlFlagEnum As Integer
    ENRScanEnabled = 0
    HysteresisEnabled = 1
    FullPedalTravelCheckEnabled = 2
  End Enum

  Public Structure OverviewScreenControls
    Private m As Boolean
    Public Const SerialNumber = "SerialNumber"
    Public Const ButtonToButton = "BToBTime"
    Public Const CycleTime = "CycleTime"
    Public Const ShiftPassed = "ShiftPassed"
    Public Const ShiftFailed = "ShiftFailed"
  End Structure

  Public Enum PlotNameEnum As Integer
    LowLimit = 0
    HighLimit = 1
    ForwardScan = 2
    ReverseScan = 3
  End Enum

  Public Structure PrintedPageCell
    Public CellAdditionalHeight As Integer
    Public CellFractionalWidthNum As Integer
    Public CellFractionalWidthDem As Integer
    Public CellText As String
    Public CellTextAlignment As System.Drawing.StringAlignment
    Public CellTextLineAlignment As System.Drawing.StringAlignment
    Public CellType As String
  End Structure

  Public Structure PrintedPageCellType
    Private m As Boolean
    Public Const Normal As String = "Normal"
    Public Const NewLine As String = "NewLine"
  End Structure

  Public Structure StatusCaptions
    Private m As Boolean
    Public Const NoData As String = "No Data"
    Public Const Pass As String = "Pass"
    Public Const Fail As String = "Fail"
    Public Const ReportOnly As String = "Report Only"
  End Structure

  Public Structure SetupInfoItems ' 2.0.1.0 TER
    Private m As Boolean
    Public Const Comment = "Comment"
    Public Const DateCode = "DateCode"
    Public Const ExternalSerialNumber = "ExternalSerialNumber"
    Public Const ExternalTestMethodSelected = "ExternalTestMethodSelected"
    Public Const OperatorName = "OperatorName"
    Public Const PedalArmReleaseType As String = "PedalArmReleaseType"
    Public Const SampleNumber As String = "SampleNumber"
    Public Const Series As String = "Series"
    Public Const Shift = "Shift"
    Public Const SoakTime = "SoakTime"
    Public Const StrokeLength As String = "StrokeLength"
    Public Const Temperature As String = "Temperature"
    Public Const TestLogNumber As String = "TestLogNumber"
  End Structure

  Public Enum SubProcessEnum As Integer
    Initialization = 0
    Configuration = 1
    Programming = 2
    FunctionalTest = 3
  End Enum

  Public Enum TaskListFilterEnum As Integer
    None = 0
    Program = 1
    Test = 2
    ProgramAndTest = 3
    ParamSetInit = 4
    TsopEnd = 5
  End Enum

  Public Enum TaskListSectionEnum As Integer
    INITIAL = 0
    PROG = 1
    TEST = 2
    PARAMSETINIT = 3
    TSOPEND = 4
    TSOPINIT = 5
  End Enum

#End Region

#Region "Public Functions =================================================="

  Public Sub AntiTieDownTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    Try
      gtmrAntiTieDownTimer.Enabled = False
      gblnAntiTieDownTimeout = True
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  'Public Function BuildLotName() As String
  '  Dim intDayOfYear As Integer
  '  Dim intShiftCount As Integer
  '  Dim strYear As String
  '  Dim strCurrentDate As String
  '  Dim CurrentTime As DateTime
  '  Dim MidNight As DateTime
  '  Dim FirstShiftStartTime As DateTime
  '  Dim ShiftStartTime As DateTime
  '  Dim ShiftEndTime As DateTime
  '  Dim strShiftName As String
  '  Dim strErrorMessage As String
  '  Dim strLotName As String

  '  Try

  '    'Get DOY
  '    intDayOfYear = Now.DayOfYear

  '    'Get Year
  '    strYear = Now.ToString("yy")

  '    'Get Current Date String
  '    strCurrentDate = Now.Date.ToString("MM/dd/yyyy")

  '    'Get Current Time
  '    CurrentTime = Now
  '    'CurrentTime = "11/14/2013 5:59:59"

  '    MidNight = Convert.ToDateTime(strCurrentDate & " " & "0:0")

  '    FirstShiftStartTime = Convert.ToDateTime(strCurrentDate & " " & gLotShiftDefinitions(0).StartTime)

  '    strShiftName = String.Empty
  '    If CurrentTime >= MidNight And CurrentTime < FirstShiftStartTime Then
  '      'Day of year is determined by shift start date
  '      'It is after midnight but before 1st shift, so subtract a day.
  '      intDayOfYear -= 1
  '      strShiftName = gLotShiftDefinitions(gLotShiftDefinitions.Count - 1).ShiftName
  '    Else
  '      'Determine shift/Lookup Shift
  '      For intShiftCount = 0 To gLotShiftDefinitions.Count - 1

  '        ShiftStartTime = Convert.ToDateTime(strCurrentDate & " " & gLotShiftDefinitions(intShiftCount).StartTime)
  '        ShiftEndTime = Convert.ToDateTime(strCurrentDate & " " & gLotShiftDefinitions(intShiftCount).EndTime)

  '        If gLotShiftDefinitions(intShiftCount).EndTime < gLotShiftDefinitions(intShiftCount).StartTime Then
  '          'Shift spans midnight, so add a day to the end time for comparison below
  '          ShiftEndTime = DateAdd(DateInterval.Day, 1, ShiftEndTime)
  '        End If

  '        If CurrentTime >= ShiftStartTime And CurrentTime < ShiftEndTime Then
  '          strShiftName = gLotShiftDefinitions(intShiftCount).ShiftName
  '          Exit For
  '        End If
  '      Next
  '    End If

  '    If strShiftName = String.Empty Then
  '      strErrorMessage = "Current Shift Not Found in Shift Definition."
  '      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
  '      Throw New TsopAnomalyException
  '    End If

  '    gstrLotDayOfYear = intDayOfYear.ToString("000")
  '    gstrLotYear = strYear
  '    gstrLotShiftName = strShiftName

  '    strLotName = gstrLotDayOfYear & gstrLotYear & gstrLotShiftName
  '    Return strLotName

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '    Return Nothing
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '    Return Nothing
  '  End Try
  'End Function

  Public Function BuildLotName() As String
    Dim intDayOfYear As Integer
    Dim intShiftCount As Integer
    Dim strYear As String
    Dim strCurrentDate As String
    Dim CurrentTime As DateTime
    Dim FirstShiftStartTime As DateTime
    Dim ShiftStartTime As DateTime
    Dim ShiftEndTime As DateTime
    Dim strShiftName As String
    Dim strLotName As String

    Dim strTomorrowDate As String
    Dim LastShiftStartTime As DateTime
    Dim MidNightToday As DateTime
    Dim MidNightTomorrow As DateTime
    Try

      'Get DOY
      intDayOfYear = Now.DayOfYear

      'Get Year
      strYear = Now.ToString("yy")

      'Get Current Date String
      strCurrentDate = Now.Date.ToString("MM/dd/yyyy")
      'strCurrentDate = "11/25/2015"

      'Get Current Time
      CurrentTime = Now
      'CurrentTime = "11/25/2015 22:30:00"

      'Get Next Date String
      strTomorrowDate = DateAdd(DateInterval.Day, 1, CurrentTime).ToString("MM/dd/yyyy")

      FirstShiftStartTime = Convert.ToDateTime(strCurrentDate & " " & gLotShiftDefinitions(0).StartTime)
      LastShiftStartTime = Convert.ToDateTime(strCurrentDate & " " & gLotShiftDefinitions(gLotShiftDefinitions.Count - 1).StartTime)

      MidNightToday = Convert.ToDateTime(strCurrentDate & " " & "0:0")
      MidNightTomorrow = Convert.ToDateTime(strTomorrowDate & " " & "0:0")

      'Determine Shift
      strShiftName = String.Empty
      If CurrentTime < FirstShiftStartTime Then
        'Last Shift
        strShiftName = gLotShiftDefinitions(gLotShiftDefinitions.Count - 1).ShiftName
      Else
        'Determine Shift
        For intShiftCount = 0 To gLotShiftDefinitions.Count - 1

          ShiftStartTime = Convert.ToDateTime(strCurrentDate & " " & gLotShiftDefinitions(intShiftCount).StartTime)
          ShiftEndTime = Convert.ToDateTime(strCurrentDate & " " & gLotShiftDefinitions(intShiftCount).EndTime)

          If gLotShiftDefinitions(intShiftCount).EndTime < gLotShiftDefinitions(intShiftCount).StartTime Then
            'Shift spans midnight, so add a day to the end time for comparison below
            ShiftEndTime = DateAdd(DateInterval.Day, 1, ShiftEndTime)
          End If

          If CurrentTime >= ShiftStartTime And CurrentTime < ShiftEndTime Then
            strShiftName = gLotShiftDefinitions(intShiftCount).ShiftName
            Exit For
          End If
        Next
      End If

      'If Last Shift, determine Day Of Year
      If strShiftName = gLotShiftDefinitions(gLotShiftDefinitions.Count - 1).ShiftName Then
        'Determine Day Of Year
        If gControlFlags.LastShiftUseNextLotDay Then
          If CurrentTime >= LastShiftStartTime And CurrentTime < MidNightTomorrow Then
            'Between Last Shift Start Time and Midnight, so set day of year to next day
            intDayOfYear += 1
          Else
            'Day of year is already the next day
          End If
        Else
          If CurrentTime >= MidNightToday And CurrentTime < FirstShiftStartTime Then
            'Between Midnight and First Shift Start Time, so set day of year to previous day
            intDayOfYear -= 1
          Else
            'Day of year is already the previous day
          End If
        End If
      End If

      gstrLotDayOfYear = intDayOfYear.ToString("000")
      gstrLotYear = strYear
      gstrLotShiftName = strShiftName

      strLotName = gstrLotDayOfYear & gstrLotYear & gstrLotShiftName

      Return strLotName
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function





  Public Function FindParentMenuItem(ByVal drMenu As DataRow) As ToolStripMenuItem
    Dim miParentMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim lfrmTsopMain As frmTsopMain = My.Application.OpenForms("frmTsopMain")
    Dim strMenuBranchName As String
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String
    Dim intLoop As Integer
    Dim intLoopCount As Integer

    Try
      'Determine how many loops to find highest level parent
      intLoopCount = drMenu.Item("MenuLevel") - 2 'Highest menu level is 2, which is 1 below the main tool strip menu

      'Setup search criteria
      strMenuBranchName = drMenu.Item("MenuBranchName")
      strFilterExpression = "MenuBranchName = '" & strMenuBranchName & "'"

      'Search menu data table for all menus in branch
      rowsFound = gdtMenuDefinitions.Select(strFilterExpression)
      If rowsFound.Length = 0 Then
        strErrorMessage = "MenuBranchName '" & strMenuBranchName & "' Not Found In Menu Definitions Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      Else
        'Found menu branch, highest level menu is the item on the main menu strip control
        miParentMenuItem = lfrmTsopMain.MenuStripMain.Items(rowsFound(0).Item("ParentMenuItem"))
        'Loop to find remaining parents in branch
        For intLoop = 1 To intLoopCount
          'Next parent in branch
          miParentMenuItem = miParentMenuItem.DropDownItems(rowsFound(intLoop).Item("ParentMenuItem"))
        Next
      End If

      Return miParentMenuItem

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function


  Public Function GetClassPropertyInstanceValue(ByVal sClassName As String, _
                                              ByVal sPropertyName As String) As Object
    Dim myType As Type
    Dim myProperty As PropertyInfo
    Dim oPropertyValue As Object

    oPropertyValue = Nothing

    Select Case sClassName
      Case "DataArrays"
        myType = GetType(DataArrays)
        myProperty = myType.GetProperty(sPropertyName)
        oPropertyValue = myProperty.GetValue(gDataArrays, Nothing)
      Case "MechanicalOperationAndLoad"
        myType = GetType(MechanicalOperationAndLoad)
        myProperty = myType.GetProperty(sPropertyName)
        oPropertyValue = myProperty.GetValue(gMechanicalOperationAndLoad, Nothing)
      Case "AnalysisAndManagementOfAcquiredData"
        myType = GetType(AnalysisAndManagementOfAcquiredData)
        myProperty = myType.GetProperty(sPropertyName)
        oPropertyValue = myProperty.GetValue(gAmad, Nothing)
        'Case "ProductProgramming"
        '  myType = GetType(ProductProgramming)
        '  myProperty = myType.GetProperty(sPropertyName)
        '  oPropertyValue = myProperty.GetValue(gProductProgramming, Nothing)

    End Select
    Return oPropertyValue
  End Function

  Public Function GetDateTimeStamp() As String
    Dim strDateTimeStamp As String
    Dim CurrentDateTime As Date
    Try
      CurrentDateTime = Now()
      strDateTimeStamp = Format(DatePart(DateInterval.Year, Now()), "####")
      strDateTimeStamp = strDateTimeStamp & Format(DatePart(DateInterval.Month, CurrentDateTime), "0#")
      strDateTimeStamp = strDateTimeStamp & Format(DatePart(DateInterval.Day, CurrentDateTime), "0#")
      strDateTimeStamp = strDateTimeStamp & Format(DatePart(DateInterval.Hour, CurrentDateTime), "0#")
      strDateTimeStamp = strDateTimeStamp & Format(DatePart(DateInterval.Minute, CurrentDateTime), "0#")
      strDateTimeStamp = strDateTimeStamp & Format(DatePart(DateInterval.Second, CurrentDateTime), "0#")

      Return strDateTimeStamp

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetDisplayGridColumns(ByVal gridID As Guid) As DataTable
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim rowFiltered As DataRow
    Dim dtCols As DataTable = gdtDisplayGridColumns.Clone

    Try
      strFilterExpression = "DisplayGridID = '" & gridID.ToString & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtDisplayGridColumns.Select(strFilterExpression)
      For Each rowFiltered In rowsFound
        dtCols.ImportRow(rowFiltered)
      Next
      Return dtCols

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Function GetDisplayGridRows(ByVal gridID As Guid) As DataTable
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim rowFiltered As DataRow
    Dim dtRows As DataTable = gdtDisplayGridRows.Clone

    Try
      strFilterExpression = "DisplayGridID = '" & gridID.ToString & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtDisplayGridRows.Select(strFilterExpression)
      For Each rowFiltered In rowsFound
        dtRows.ImportRow(rowFiltered)
      Next
      Return dtRows

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Function GetGraphAxesSettings(ByVal graphName) As DataRow() ' 3.1.0.1 TER
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow

    Try
      strFilterExpression = "GraphName = '" & graphName & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtGraphAxesDefinitions.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      Return rowsFound

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetGraphPlotsSettings(ByVal graphName) As DataRow()
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow

    Try
      strFilterExpression = "GraphName = '" & graphName & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtGraphPlotDefinitions.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      Return rowsFound

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetGraphSettings(ByVal graphName) As DataRow
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String

    Try
      strFilterExpression = "GraphName = '" & graphName & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtGraphDefinitions.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      If rowsFound.Length > 0 Then
        Return rowsFound(0)
      Else
        strErrorMessage = "Graph '" & graphName & "' Not Found In Graph Definitions Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If
      Return rowsFound(0)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Sub GetLotNames(ByVal strLotName As String, ByVal blnIncLotRun As Boolean)
    Dim intLastRow As Integer
    Try

      gdtLotNames = gDatabase.GetLotNamesDataTable(strLotName, blnIncLotRun)
      intLastRow = gdtLotNames.Rows.Count - 1
      gintLotRun = gdtLotNames.Rows(intLastRow).Item("LotRun")
      gDatabase.LotID = gdtLotNames.Rows(intLastRow).Item("LotID")

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetLotShiftDefinitions()
    Dim dtLotShiftDef As DataTable
    Dim drShiftRow As DataRow
    Dim LotShiftDef As New LotShiftDefinition
    Try

      dtLotShiftDef = gDatabase.GetLotShiftDefDataTable()
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If
      For Each drShiftRow In dtLotShiftDef.Rows
        LotShiftDef.StartTime = Convert.ToDateTime(drShiftRow.Item("LotShiftStartTime").ToString).TimeOfDay.ToString
        LotShiftDef.EndTime = Convert.ToDateTime(drShiftRow.Item("LotShiftEndTime").ToString).TimeOfDay.ToString
        LotShiftDef.ShiftName = drShiftRow.Item("LotShiftName").ToString
        gLotShiftDefinitions.Add(LotShiftDef)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Function GetMetricPropertiesRow(ByVal metricID) As DataRow
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String
    Try
      strFilterExpression = "FunctionMetricID = '" & metricID.ToString & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtMetricProperties.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      If rowsFound.Length > 0 Then
        Return rowsFound(0)
      Else
        strErrorMessage = "FunctionMetricID '" & metricID.ToString & "' Not Found In Metric Properties Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetMetricPropertiesRowFromAssiciatedParameter(ByVal strParameterName) As DataRow
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String
    Try
      strFilterExpression = "ParameterName = '" & strParameterName & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtMetricProperties.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      If rowsFound.Length > 0 Then
        Return rowsFound(0)
      Else
        strErrorMessage = "ParameterName '" & strParameterName & "' Not Found In Metric Properties Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetMetricPropertiesRowFromAssiciatedMetric(ByVal metMetric As Metric) As DataRow
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String
    Try
      'strFilterExpression = "ParameterName = '" & strParameterName & "'"
      strFilterExpression = "FunctionMetricID = '" & metMetric.FunctionMetricID.ToString & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtMetricProperties.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      If rowsFound.Length > 0 Then
        Return rowsFound(0)
      Else
        strErrorMessage = "Metric '" & metMetric.MetricName & "' Not Found In Metric Properties Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Sub GetMetricPropertiesTableFromCsv() 'Temporary just to determine database structure needed
    'Dim strFilter As String
    Dim strFileName As String

    Try

      strFileName = "MetricProperties.csv"

      ''Set filter 
      'strFilter = "GraphGroup = " & """" & gControlAndInterface.GraphGroup & """"

      'Clear graph definitions table
      gdtMetricProperties.Clear()
      'Read graph definitions from file into data table
      'gdtMetricProperties = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, strFileName, strFilter)
      'gdtMetricProperties = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, strFileName, Nothing)
      gdtMetricProperties = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, strFileName, Nothing)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub GetOverviewScreenConfig()
    Dim strFilter As String
    Try

      'Set filter for graph definitions based on defined group
      strFilter = "" '"GraphGroup = " & """" & gControlAndInterface.GraphGroup & """"

      'Clear graph definitions table
      gdtOverviewScreenDefinitions.Clear()

      'Read definitions from file into data table
      'gdtOverviewScreenDefinitions = ReadCsvFileIntoDataTable(gstrClassConfigFilePath, gstrOverviewScreenDefinitionFile, strFilter)
      gdtOverviewScreenDefinitions = StreamCsvFileIntoDataTable(gstrClassConfigFilePath, gstrOverviewScreenDefinitionFile, strFilter)
      If Not (gAnomaly Is Nothing) Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  'Public Function GetPlotIndex(ByVal strGraphName As String, ByVal strPlotName As String) As Integer
  '  Dim intPlotCount As Integer
  '  Dim intPlotIndex As Integer
  '  Dim blnPlotFound As Boolean
  '  Dim strErrorMessage As String
  '  Try
  '    blnPlotFound = False
  '    intPlotIndex = -1
  '    For intPlotCount = 0 To gGraphsCollection(strGraphName).Plots.Count - 1
  '      If gGraphsCollection(strGraphName).Plots(intPlotCount).Tag = strPlotName Then
  '        blnPlotFound = True
  '        intPlotIndex = intPlotCount
  '      End If
  '    Next
  '    If Not blnPlotFound Then
  '      strErrorMessage = "Plot Not Found"
  '      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
  '      Throw New TsopAnomalyException
  '    End If

  '    Return intPlotIndex

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '    Return Nothing
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '    Return Nothing
  '  End Try
  'End Function

  Public Sub GetProductDateCode()
    Try

      Dim strDayOfYear As String
      Dim strYear As String
      Dim strShift As String

      'Get DOY
      strDayOfYear = Now.DayOfYear.ToString("000") ' Format(CStr(Now.DayOfYear), "000")

      'Get Year
      strYear = Now.ToString("yy")

      strShift = GetShiftLetter()

      gDeviceInProcess.DateCode = strDayOfYear & strYear & strShift

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Function GetProductDateCodeBySeries(ByVal strProductSeries As String) As String
    Dim strDateCode As String
    Dim strStationNumber As String
    Try
      strDateCode = ""
      strStationNumber = "1" 'Move this to app.config

      Select Case strProductSeries
        Case "706"
          strDateCode = gstrLotYear & gstrLotDayOfYear & gstrLotShiftName & strStationNumber
      End Select

      Return strDateCode

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    Finally
    End Try

  End Function

  Public Function GetReportSection(ByVal reportSection, ByVal reportType) As DataRow() ' 3.1.0.1 TER
    Dim strFilterExpression As String
    Dim rowsFound() As DataRow
    Dim strErrorMessage As String

    Try

      strFilterExpression = "ReportSection = '" & reportSection & "' and ReportType = '" & reportType & "'"

      ' Use the Select method to find all rows matching the filter.
      rowsFound = gdtPrintedReportDefinitions.Select(strFilterExpression)

      'Return first row, should only be one row available from specified filter
      If rowsFound.Length > 0 Then
        Return rowsFound
      Else
        strErrorMessage = "ReportSection '" & reportSection & "' Not Found In Graph Definitions Table"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      End If
      Return rowsFound

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Function GetShiftLetter() As String
    '
    '   PURPOSE: To build the shift letter based on the time of day.
    '
    '  INPUT(S): None
    ' OUTPUT(S): returns the Shift Letter (String)
    Try

      Dim lintHours As Integer
      Dim lintMinutes As Integer
      Dim lintMinuteOfDay As Integer
      Dim lstrShift As String

      lstrShift = ""
      'Determine the minute of the day (x/1439)
      lintHours = Now.Hour 'DateTime.Hour(DateTime.Now)         'Get the hour
      lintMinutes = Now.Minute 'DateTime.Minute(DateTime.Now)     'Get the minutes
      lintMinuteOfDay = lintHours * 60 + lintMinutes  'Calculate the current minute of the day

      'Select the shift based on what minute of the day it is
      Select Case lintMinuteOfDay
        '12:00AM to 6:59AM, 11:00PM to 11:59PM
        Case 0 To 419, 1380 To 1439
          lstrShift = "C"
          '7:00AM to 2:59PM
        Case 420 To 899
          lstrShift = "A"
          '3:00PM to 10:59PM
        Case 900 To 1379
          lstrShift = "B"
      End Select

      'Return the selected shift letter
      Return lstrShift

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Sub GetTaskListSection(ByVal sectionName As String)
    Dim dr As DataRow
    Dim intRow As Integer
    Dim intMaxRow As Integer
    Try

      intMaxRow = gdtAppTaskList.Rows.Count - 1
      For intRow = 0 To intMaxRow
        dr = gdtAppTaskList.Rows(intRow)
        'Find requested section
        If dr.Item("ModuleName") = sectionName Then
          Do
            'Section found, get first task 
            intRow += 1
            If intRow <= intMaxRow Then
              dr = gdtAppTaskList.Rows(intRow)
              'Check if start of next section, if not import task to filtered list
              If dr.Item("MethodName") <> "START" Then
                gdtFilteredTaskList.ImportRow(dr)
              End If
            End If
          Loop Until (dr.Item("MethodName") = "START") Or intRow > intMaxRow
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  'Public Sub GetTsopModeID()
  '  Dim bTorque As Boolean
  '  Dim bTest As Boolean
  '  Dim bENR As Boolean
  '  Dim intTest As Int32
  '  Dim intTorque As Int32
  '  Dim intENR As Int32
  '  Dim intTsopModeValue As Int32

  '  Try
  '    'Set these equal to global flags
  '    bTorque = gControlFlags.TorqueScanEnabled
  '    'bTest = gblnStartScan
  '    bTest = gControlFlags.StartScan
  '    'bENR = gblnENRScan
  '    bENR = gControlFlags.ENRScanEnabled

  '    intTest = Math.Abs(CInt(bTest)) * gTsopFunctionValues("Test Output Versus Position")
  '    'intTorque = Math.Abs(CInt(bTorque)) * gTsopFunctionValues("Test Torque Versus Position")
  '    'intENR = Math.Abs(CInt(bENR)) * gTsopFunctionValues("Test ENR")

  '    intTsopModeValue = intTorque + intTest + intENR

  '    gDatabase.TsopModeID = gTsopModeValues(intTsopModeValue)

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try

  'End Sub

  'Public Sub GraphResults()
  '  Dim myType As Type
  '  Dim myProperty As PropertyInfo
  '  Dim graphResults As clsGraph
  '  Dim intPlotIndex As Integer
  '  Dim dp As New clsGraph.DataPlot
  '  Dim XArray() As Double
  '  Dim YArray() As Double

  '  Try

  '    myType = GetType(DataArrays)

  '    For Each graphResults In gGraphsCollection.Values

  '      If graphResults.Enabled Then

  '        'strGraphName = graphResults.Name
  '        For Each dp In graphResults.DataPlots

  '          intPlotIndex = dp.PlotIndex
  '          'Get X Data Array
  '          myProperty = myType.GetProperty(dp.XArrayName)
  '          'myProperty = myType.GetProperty("gDataArrays.PedalReturnTimeArrayMax")
  '          XArray = myProperty.GetValue(gDataArrays, Nothing)

  '          'Get Y Data Array
  '          myProperty = myType.GetProperty(dp.YArrayName)
  '          'myProperty = myType.GetProperty("gDataArrays.PedalReturnDut1VoltageArrayMax")
  '          YArray = myProperty.GetValue(gDataArrays, Nothing)

  '          ''Resize Arrays if necessary
  '          'If XArray.Length <> YArray.Length Then
  '          '  Call ResizeArraysToMatch(XArray, YArray)
  '          'End If

  '          If XArray IsNot Nothing And YArray IsNot Nothing Then
  '            graphResults.Plots(intPlotIndex).CanScaleXAxis = dp.XAxisAutoScaleEnabled
  '            graphResults.Plots(intPlotIndex).CanScaleYAxis = dp.YAxisAutoScaleEnabled

  '            graphResults.Plots(intPlotIndex).PlotXY(XArray, YArray)
  '          End If
  '        Next
  '      End If
  '    Next

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub


  Public Sub InitializeControlFlags()
    Try
      gMetricControlFlags.Clear()
      gMetricControlFlags.Add(MetricControlFlagEnum.ENRScanEnabled.ToString, False)
      gMetricControlFlags.Add(MetricControlFlagEnum.HysteresisEnabled.ToString, True)
      gMetricControlFlags.Add(MetricControlFlagEnum.FullPedalTravelCheckEnabled.ToString, False)

      gControlFlags.AbortTaskList = False
      gControlFlags.AbortTest = False
      gControlFlags.ENRScanEnabled = False
      gControlFlags.CyclePartEnabled = False
      gControlFlags.DeviceMarkingEnabled = False
      gControlFlags.ForceScanEnabled = False
      gControlFlags.FullPedalTravelCheckEnabled = False
      gControlFlags.HasADRDatabase = False
      gControlFlags.IsLinearPart = False
      gControlFlags.KickdownEnable = False
      gControlFlags.LockIC = True
      gControlFlags.LockRejectParts = False
      gControlFlags.LotFileLoaded = False
      gControlFlags.MetricsLoaded = False
      gControlFlags.OnPlcStartProgram = False
      gControlFlags.OnPlcStartTest = False
      gControlFlags.ParametersLoaded = False
      gControlFlags.PedalFaceFound = False
      gControlFlags.ProductionMode = False
      gControlFlags.ProgFailure = False
      gControlFlags.PauseTask = False
      gControlFlags.RetryMeasurement = False
      gControlFlags.ReverseScanOnly = False
      gControlFlags.SetupDone = False
      gControlFlags.StartScan = False
      gControlFlags.ManualScan = False
      gControlFlags.TestFailure = False
      gControlFlags.TestInProgress = False
      gControlFlags.TorqueScanEnabled = False
      gControlFlags.UsesOverviewScreen = False
      gControlFlags.UsesPLC = False
      gControlFlags.UsesProgramming = False
      gControlFlags.UsesADRDatabase = False
      gControlFlags.UsesDynamicSetupInfo = False
      gControlFlags.UsesMotionControl = False
      gControlFlags.UsesDcMotor = False
      gControlFlags.LaserTestEnabled = False

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub InitializeGrid(ByVal rowGrid As DataRow)
    Dim gridNew As New DataGridView
    Dim myTabPage As New TabPage()
    'Dim intRow As Int16
    Dim intColumn As Int16
    Dim intTabIndex As Int16
    Dim dtCol As DataTable
    Dim colRow As DataRow
    Dim dtRow As DataTable
    Dim rowRow As DataRow
    Dim intRow As Int16
    Dim RemoveRowNumbers As New List(Of Int16)
    Dim NumRowsRemoved As Int16
    Dim CurrentMetric As Metric
    Dim MetricID As Guid
    Dim i As Integer
    Try
      i = 0

      'Set property values appropriate for read-only display and 
      'limited interactivity. 
      gridNew.AllowUserToAddRows = False
      gridNew.AllowUserToDeleteRows = False
      gridNew.AllowUserToOrderColumns = False
      gridNew.ReadOnly = True
      gridNew.MultiSelect = False
      gridNew.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.None
      gridNew.AllowUserToResizeColumns = False
      gridNew.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing
      gridNew.AllowUserToResizeRows = False
      gridNew.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing
      gridNew.RowHeadersVisible = False
      gridNew.Dock = DockStyle.Fill
      gridNew.ColumnHeadersHeight = 40

      gridNew.ColumnCount = rowGrid.Item("ColsCount")
      gridNew.RowCount = rowGrid.Item("RowsCount")

      'dtCol = gDatabase.GetDisplayGridColumnProperties(gridRow.Item("DisplayGridID"))
      'dtCol = gDatabase.GetMetricsDisplayGridColumnsDataTable(rowGrid.Item("DisplayGridID"))
      dtCol = GetDisplayGridColumns(rowGrid.Item("DisplayGridID"))
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      'for each column property, set gridview column property
      For Each colRow In dtCol.Rows
        gridNew.Columns(CInt(colRow.Item("ColumnNumber"))).Width = colRow.Item("ColumnWidth") * frmTsopMain.tabResults.Width
        gridNew.Columns(CInt(colRow.Item("ColumnNumber"))).HeaderText = colRow.Item("ColumnHeader")
      Next

      'dtRow = gDatabase.GetDisplayGridRowProperties(gridRow.Item("DisplayGridID"))
      'dtRow = gDatabase.GetMetricsDisplayGridRowsDataTable(rowGrid.Item("DisplayGridID"))
      dtRow = GetDisplayGridRows(rowGrid.Item("DisplayGridID"))
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      For Each rowRow In dtRow.Rows
        'Check for RowType, metric or separator
        If rowRow.Item("RowType") = "Metric" Then
          MetricID = New Guid(rowRow.Item("FunctionMetricID").ToString)
          CurrentMetric = gMetrics(MetricID)

          If CurrentMetric.Enabled Then
            gridNew(0, rowRow.Item("RowNumber")).Value = rowRow.Item("RowHeader")
            If (rowGrid.Item("DisplayGridCaption").ToString.Contains("Results")) Then
              gstrMetricID(i) = rowRow.Item("RowHeader")
              i = i + 1
            End If
          Else
            RemoveRowNumbers.Add(rowRow.Item("RowNumber"))
          End If
        Else
          gridNew(0, rowRow.Item("RowNumber")).Value = rowRow.Item("RowHeader")
        End If
      Next

      'remove specified rows
      NumRowsRemoved = 0
      For Each intRow In RemoveRowNumbers
        gridNew.Rows.RemoveAt(intRow - NumRowsRemoved)
        NumRowsRemoved += 1
      Next
      '/\

      With gridNew.ColumnHeadersDefaultCellStyle
        .Alignment = DataGridViewContentAlignment.MiddleCenter
        .Font = New Font(gridNew.Font, FontStyle.Bold)
        '.WrapMode = DataGridViewTriState.True
      End With

      For intColumn = 0 To gridNew.ColumnCount - 1
        gridNew.Columns(intColumn).SortMode = DataGridViewColumnSortMode.NotSortable
      Next

      myTabPage.Text = rowGrid.Item("DisplayGridCaption")
      myTabPage.Name = rowGrid.Item("DisplayGridName")
      myTabPage.Controls.Add(gridNew) 'Add new grid to new page

      frmTsopMain.tabResults.TabPages.Add(myTabPage) 'Add new page to Tab Control
      intTabIndex = frmTsopMain.tabResults.TabPages.Count - 1

      gDataGrids.Add(rowGrid.Item("DisplayGridName"), frmTsopMain.tabResults.TabPages(intTabIndex).Controls(0)) 'Store reference to grid in a collection for later use

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub LoadParameters()
    Try

      '==================================================
      'Classes that load their property values from database parameters
      '==================================================
      gStationMonitorAndAlarm = New StationMonitorAndAlarm(gDatabase)
      If Not (gStationMonitorAndAlarm.Anomaly Is Nothing) Then
        gAnomaly = gStationMonitorAndAlarm.Anomaly
        gStationMonitorAndAlarm.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gElectricalOperationAndLoad = New ElectricalOperationAndLoad(gDatabase)
      If Not (gElectricalOperationAndLoad.Anomaly Is Nothing) Then
        gAnomaly = gElectricalOperationAndLoad.Anomaly
        gElectricalOperationAndLoad.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gAmad = New AnalysisAndManagementOfAcquiredData(gDatabase)
      If Not (gAmad.Anomaly Is Nothing) Then
        gAnomaly = gAmad.Anomaly
        gAmad.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gControlAndInterface = New ControlandInterface(gDatabase)
      If Not (gControlAndInterface.Anomaly Is Nothing) Then
        gAnomaly = gControlAndInterface.Anomaly
        gControlAndInterface.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gCalibrationConstant = New CalibrationConstant(gDatabase)
      If Not (gCalibrationConstant.Anomaly Is Nothing) Then
        gAnomaly = gCalibrationConstant.Anomaly
        gCalibrationConstant.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gDataAcquisitionSystem = New DataAcquisitionSystem(gDatabase)
      If Not (gDataAcquisitionSystem.Anomaly Is Nothing) Then
        gAnomaly = gDataAcquisitionSystem.Anomaly
        gDataAcquisitionSystem.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gMechanicalOperationAndLoad = New MechanicalOperationAndLoad(gDatabase)
      If Not (gMechanicalOperationAndLoad.Anomaly Is Nothing) Then
        gAnomaly = gMechanicalOperationAndLoad.Anomaly
        gMechanicalOperationAndLoad.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      gDeviceInProcess = New DeviceInProcess(gDatabase)
      If gDeviceInProcess.Anomaly IsNot Nothing Then
        gAnomaly = gDeviceInProcess.Anomaly
        gDeviceInProcess.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub LoadParameterSets()
    Dim paramSetTable As DataTable

    Try
      'Clear Combo Parameter Set List
      frmTsopMain.cboParameterSets.DataSource = Nothing
      frmTsopMain.cboParameterSets.Items.Clear()

      If gblnUseUnreleasedSets Then
        'Get table of Unreleased Parameter Sets
        paramSetTable = gDatabase.GetUnreleasedParameterSetsDataTable
        If Not (gDatabase.Anomaly Is Nothing) Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
      Else
        'Get table of Released Parameter Sets
        paramSetTable = gDatabase.GetParameterSetsDataTable(gblnProductionSet)
        If Not (gDatabase.Anomaly Is Nothing) Then
          gAnomaly = gDatabase.Anomaly
          gDatabase.Anomaly = Nothing
          Throw New TsopAnomalyException
        End If
      End If

      If paramSetTable.Rows.Count > 0 Then
        'Bind data table to combo box
        frmTsopMain.cboParameterSets.DataSource = paramSetTable
        frmTsopMain.cboParameterSets.ValueMember = paramSetTable.Columns("ProcessParameterID").ToString
        frmTsopMain.cboParameterSets.DisplayMember = paramSetTable.Columns("ParameterSet").ToString
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  Public Sub LotShiftTimerElapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs)
    Dim strLotName As String
    Dim lfrmUpdate As frmTsopMain = My.Application.OpenForms("frmTsopMain")

    Try
      gtmrLotShift.Stop()

      If gDatabase.LotID <> Guid.Empty Then
        'TempSetShiftsForDebug()
        strLotName = BuildLotName()
        If strLotName <> gstrLotName Then
          Call GetLotNames(strLotName, False)
          'Call frmTsopMain.ConfigureLotNamesMenu()
          lfrmUpdate.Invoke(New ConfigLotNamesMenu(AddressOf lfrmUpdate.ConfigureLotNamesMenu))
          'Call UpdateDisplayForCurrentLot()
          lfrmUpdate.Invoke(New UpdateCurrentLotDisplay(AddressOf UpdateDisplayForCurrentLot))
          gstrLotName = strLotName
        End If
      End If

      gtmrLotShift.Start()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub LogActivity(ByVal strLogData As String)
    Dim fsLog As FileStream = Nothing
    Dim swLog As StreamWriter = Nothing
    Dim lDate As Date
    Dim lstrYear As String
    Dim lstrMonth As String
    Dim lstrDay As String
    Dim lblnFileExists As Boolean
    Dim strDate As String = ""

    Try
      lDate = DateTime.Today
      lstrMonth = Format(Month(lDate), "00")
      lstrYear = Format(Year(lDate), "0000")
      lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

      If Directory.Exists(gstrActivityLogPath & lstrMonth & "_" & lstrYear) = False Then
        Directory.CreateDirectory(gstrActivityLogPath & lstrMonth & "_" & lstrYear)
      End If

      Dim lstrFilePath As String = gstrActivityLogPath & lstrMonth & "_" & lstrYear & "\" & lstrDay & ".log"

      'Open the file
      If File.Exists(lstrFilePath) Then
        fsLog = New FileStream(lstrFilePath, FileMode.Append, FileAccess.Write, FileShare.None)
        lblnFileExists = True
      Else
        fsLog = New FileStream(lstrFilePath, FileMode.CreateNew)
        lblnFileExists = False
      End If

      swLog = New StreamWriter(fsLog)
      strDate = Format(Now, "HH:mm:ss")

      swLog.WriteLine(strDate & "," & gActivityLogStopWatch.ElapsedMilliseconds & "," & strLogData)

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    Finally
      swLog.Close()
      fsLog.Close()
    End Try
  End Sub

  'Public Sub LogTestData(ByVal strLogData As String)
  '  Dim fs As FileStream = Nothing
  '  Dim bw As StreamWriter = Nothing
  '  Dim lDate As Date
  '  Dim lstrYear As String
  '  Dim lstrMonth As String
  '  Dim lstrDay As String
  '  Dim lblnFileExists As Boolean
  '  Dim strDate As String = ""

  '  Try
  '    lDate = DateTime.Today
  '    lstrMonth = Format(Month(lDate), "00")
  '    lstrYear = Format(Year(lDate), "0000")
  '    lstrDay = Format(Microsoft.VisualBasic.DateAndTime.Day(lDate), "00")

  '    If Directory.Exists(gstrDataFileBasePath & "LogData\" & lstrMonth & "_" & lstrYear) = False Then
  '      Directory.CreateDirectory(gstrDataFileBasePath & "LogData\" & lstrMonth & "_" & lstrYear)
  '    End If

  '    Dim lstrFilePath As String = gstrDataFileBasePath & "LogData\" & lstrMonth & "_" & lstrYear & "\" & lstrDay & ".log"

  '    'Open the stats file
  '    If File.Exists(lstrFilePath) Then
  '      fs = New FileStream(lstrFilePath, FileMode.Append, FileAccess.Write, FileShare.None)
  '      lblnFileExists = True
  '      bw = New StreamWriter(fs)
  '    Else
  '      fs = New FileStream(lstrFilePath, FileMode.CreateNew)
  '      lblnFileExists = False
  '      bw = New StreamWriter(fs)
  '      bw.WriteLine("Date,Elapsed Time,Data")
  '    End If

  '    strDate = Format(Now, "HH:mm:ss")

  '    'bw.WriteLine(strDate & "," & Format(gSWTestTimeStopWatch.ElapsedMilliseconds, "0#######") & "," & strLogData)
  '    bw.WriteLine(strDate & "," & Format(gCycleTimeStopWatch.ElapsedMilliseconds, "0#######") & "," & strLogData)

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  Finally
  '    bw.Close()
  '    fs.Close()
  '  End Try
  'End Sub


  Public Sub ProductAppSpecificInitialization() ' 3.1.0.2 TER
    Dim strModuleName As String
    Dim strMethodName As String
    Dim mfType As Type
    Dim mfMethod As MethodInfo

    Try

      'If gControlAndInterface.InitializationMethodName IsNot Nothing Then
      strModuleName = gstrAssemblyName & "." & gControlAndInterface.InitializationModuleName
      strMethodName = gControlAndInterface.InitializationMethodName

      mfType = Type.GetType(strModuleName)
      mfMethod = mfType.GetMethod(strMethodName)

      mfMethod.Invoke(Nothing, Nothing)
      If gAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If
      'Else

      '  Call SensorAppInitialize()
      '  If gAnomaly IsNot Nothing Then
      '    Throw New TsopAnomalyException
      '  End If
      'End If


    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Function ReadCsvFileIntoDataTable(ByVal strFilePath As String, ByVal strFileName As String, _
                                             Optional ByVal strFilter As String = Nothing) As DataTable

    Dim cnConnection As OleDbConnection
    Dim daDataAdapter As OleDbDataAdapter
    Dim dtTable As New DataTable
    Dim strCommandText As String
    Dim strConnection As String
    Dim cmdCommand As OleDbCommand

    Try

      'Instantiate OLEDB Data Adapter, used to fill data table from csv file
      daDataAdapter = New OleDbDataAdapter

      'Connection string for OLEDB comma delimited text file (CSV), file path specified from passed in function argument
      strConnection = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strFilePath & ";Extended Properties=" & """" & "text;HDR=Yes;FMT=Delimited" & """" & ";"

      'Instantiate OLEDB Connection Object
      cnConnection = New OleDbConnection(strConnection)


      'Specify command text to select all rows and columns from file
      strCommandText = "SELECT * FROM " & strFileName

      If strFilter <> Nothing Then
        strCommandText = strCommandText & " Where " & strFilter
      End If

      'Instantiate OLEDB Command Object
      cmdCommand = New OleDbCommand(strCommandText, cnConnection)

      'Open OLEDB Connection
      cnConnection.Open()

      'Specify the Select Command for the Data Adapter
      daDataAdapter.SelectCommand = cmdCommand

      'Make sure data table is empty
      dtTable.Clear()

      'Executes Select Command and fills the data table with data from the csv file
      daDataAdapter.Fill(dtTable)

      'Close the connection
      cnConnection.Close()

      Return dtTable

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try

  End Function

  Public Sub RedrawFormTabPages()
    Dim rowGrid As DataRow
    Dim myTabPage As New TabPage()
    Dim SetupTab As New TabPage

    Try

      For Each myTabPage In frmTsopMain.tabResults.TabPages
        If myTabPage.Name = "SetupInformation" Then
          SetupTab = myTabPage
        End If
      Next

      'Clear grids and change data
      frmTsopMain.tabResults.TabPages.Clear()

      If gControlFlags.UsesOverviewScreen Then
        Call InitializeOverviewTab()
        If gAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If
      End If

      'Update Status LED
      UpdatePassFailLED(Metric.PassFailStatusEnum.NoData)

      If gControlFlags.UsesDynamicSetupInfo Then
        'grpSetupInfo.Visible = False
        frmTsopMain.tabResults.TabPages.Add(SetupTab)
      End If

      gDataGrids.Clear()

      For Each rowGrid In gdtDisplayGrids.Rows
        Call InitializeGrid(rowGrid)
      Next

      'Call ConfigureGraphDisplay()
      'Call DisplayGraphs()
      Call DisplayMetrics()
      Call InitializeDefaultTabPageMenu()
      Call SelectDefaultTabPage()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub




  Public Sub SetNonProductionTSOPDefaults()
    Try

      gControlFlags.ProductionMode = False

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SetProductionTSOPDefaults()
    Try
      gControlFlags.ProductionMode = True

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Function StreamCsvFileIntoDataTable(ByVal strFilePath As String, ByVal strFileName As String, Optional ByVal strFilter As String = Nothing) As DataTable
    Dim dt As New System.Data.DataTable
    Dim blnFirstLine As Boolean = True
    Dim strFilterFieldValue As String
    Dim strFilterColumnName As String
    Dim intFilterColumn As Integer
    Dim strFile As String
    Dim cols() As String
    Dim data() As String
    Dim col As String

    Try

      strFilterColumnName = ""
      strFilterFieldValue = ""
      intFilterColumn = -1
      cols = Nothing
      data = Nothing

      If strFilter <> Nothing Then
        strFilterColumnName = (strFilter.Split("=")(0)).Trim
        strFilterFieldValue = (strFilter.Split("=")(1)).Trim
      End If

      strFile = strFilePath & strFileName

      If IO.File.Exists(strFile) Then
        Using sr As New StreamReader(strFile)
          While Not sr.EndOfStream
            If blnFirstLine Then
              blnFirstLine = False
              cols = sr.ReadLine.Split(",")
              For Each col In cols
                dt.Columns.Add(New DataColumn(col, GetType(String)))
              Next
              If strFilterColumnName <> "" Then
                intFilterColumn = Array.IndexOf(cols, strFilterColumnName)
              End If
            Else
              data = sr.ReadLine.Split(",")
              If intFilterColumn <> -1 Then
                If data(intFilterColumn) = strFilterFieldValue Then
                  dt.Rows.Add(data.ToArray)
                End If
              Else
                dt.Rows.Add(data.ToArray)
              End If
            End If
          End While
        End Using
      End If
      Return dt

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  Public Sub gtmrUpdateStatusBarTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Dim PercentComplete As Double
    Try

      PercentComplete = (gCycleTimeStopWatch.ElapsedMilliseconds / (gdblExpectedCycleTime * 1000)) * 100
      If PercentComplete >= 100 Then
        PercentComplete = 100
        gtmrUpdateStatusBarTimer.Enabled = False
      End If

      PercentComplete = Format(PercentComplete, "000")
      gOverviewProgressBar.Value = PercentComplete
      gOverviewProgressLabel.Text = PercentComplete & " % Complete"

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdatePassFailLED(ByVal PassStatus As Metric.PassFailStatusEnum)
    Try
      Select Case PassStatus
        Case Is = Metric.PassFailStatusEnum.Good
          frmTsopMain.lblPassFailIndicator.BackColor = Color.Lime
          frmTsopMain.lblPassFailIndicator.Text = StatusCaptions.Pass
        Case Is = Metric.PassFailStatusEnum.Reject
          frmTsopMain.lblPassFailIndicator.BackColor = Color.Red
          frmTsopMain.lblPassFailIndicator.Text = StatusCaptions.Fail
        Case Is = Metric.PassFailStatusEnum.ReportOnly
          frmTsopMain.lblPassFailIndicator.BackColor = Color.Gray
          frmTsopMain.lblPassFailIndicator.Text = StatusCaptions.ReportOnly
        Case Is = Metric.PassFailStatusEnum.NoData
          frmTsopMain.lblPassFailIndicator.BackColor = Color.Goldenrod
          frmTsopMain.lblPassFailIndicator.Text = StatusCaptions.NoData
      End Select

      If gControlFlags.UsesOverviewScreen Then
        gOverviewScreenPanel.BackColor = frmTsopMain.lblPassFailIndicator.BackColor
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub UpdatePositionMonitor(ByVal position As Double)
    Try
      'If gControlFlags.IsLinearPart = True Then
      '  Tank1.Value = position '* 1000
      '  Tank1.Caption = Format(position, "#0.##")
      '  Tank1.Refresh()
      'Else
      '  PositionMonitor.Value = position
      '  PositionMonitor.Caption = Format(position, "#0.##")
      '  PositionMonitor.Refresh()
      'End If
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub



#End Region

#Region "Anomaly Handling =================================================="
  Public Sub LogAnomaly()
    Dim strAnomalyAction As String
    If gAnomaly IsNot Nothing Then
      glstAnomaly.Add(gAnomaly)
    End If
    For Each gAnomaly In glstAnomaly
      strAnomalyAction = AnomalyActionsEnum.DisplayMessage.ToString
      'If PerformAnomalyAction(AnomalyActions.DisplayMessage) Then
      If PerformAnomalyAction(strAnomalyAction) Then
        DisplayAnomaly()
      End If

      If gAnomaly.CallerList <> String.Empty Then
        gAnomaly.AddAttribute("CallerList", gAnomaly.CallerList)
      End If
      If gAnomaly.ModuleName <> String.Empty Then
        gAnomaly.AddAttribute("ModuleName", gAnomaly.ModuleName)
      End If
      If gAnomaly.ProcedureName <> String.Empty Then
        gAnomaly.AddAttribute("ProcedureName", gAnomaly.ProcedureName)
      End If
      If gAnomaly.ProcedureName <> String.Empty Then
        gAnomaly.AddAttribute("ExceptionMessage", gAnomaly.AnomalyExceptionMessage)
      End If

      If Not gAnomaly.AnomalyDbError Then
        gAnomaly.WriteToDatabase(gDatabase)
        If gAnomaly.AnomalyDbError Then
          Dim sMessage As String = Nothing

          If gAnomaly.AnomalyMessageText <> String.Empty Then
            sMessage = "Can't log this Anomaly to the database." & vbCrLf & gAnomaly.AnomalyMessageText
          Else
            sMessage = "Can't log this Anomaly to the database."
          End If

          If gAnomaly.CallerList <> String.Empty Then
            sMessage = sMessage & vbCrLf & vbCrLf & "Caller List:" & vbCrLf & gAnomaly.CallerList
          End If

          MessageBox.Show(sMessage, "Anomaly Database Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End If
      Else
        MessageBox.Show("Can't log this Anomaly to the database.", "Anomaly Database Error Occured", MessageBoxButtons.OK, MessageBoxIcon.Error)
      End If
    Next

    'increment scan error counter
    gTestLot.CountErrors = gTestLot.CountErrors + 1
    gStationMonitorAndAlarm.ConsecutiveAnomalyCount = gStationMonitorAndAlarm.ConsecutiveAnomalyCount + 1
    'gControlFlags.TestFailure = True
    gControlFlags.TestError = True


    'if more than 2 anomalies have occured display message
    If gStationMonitorAndAlarm.ConsecutiveAnomalyCount >= 3 Then
      MessageBox.Show("3 Anomalies in a row have occurred" & vbCrLf & "Please check all DUT Connections", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error)
      gStationMonitorAndAlarm.ConsecutiveAnomalyCount = 0
    End If

    glstAnomaly.Clear()
    gAnomaly = Nothing
  End Sub

  Public Function PerformAnomalyAction(ByVal actionName As String) As Boolean
    Dim row As DataRow
    Dim dt As DataTable
    Dim bPerformAction As Boolean = True

    If Not gAnomaly.AnomalyActions Is Nothing Then
      dt = gAnomaly.AnomalyActions
      For Each row In dt.Rows
        If actionName = row.Item("AnomalyActionName") Then
          bPerformAction = row.Item("PerformAction")
          Exit For
        End If
      Next
    End If
    Return bPerformAction
  End Function

  Public Sub DisplayAnomaly()
    Dim sMessage As String

    If gstrRoutingMessage <> "" Then
      frmRouting.ShowDialog()
      gstrRoutingMessage = ""
    Else
      sMessage = "Anomaly " & gAnomaly.AnomalyNumber & " Occured in Procedure:" & _
        vbCrLf & gAnomaly.ModuleName & "." & gAnomaly.ProcedureName

      If gAnomaly.AnomalyExceptionMessage <> String.Empty Then
        sMessage = sMessage & vbCrLf & vbCrLf & gAnomaly.AnomalyExceptionMessage
      End If

      If gAnomaly.AnomalyMessageText <> String.Empty Then
        sMessage = sMessage & vbCrLf & vbCrLf & gAnomaly.AnomalyMessageText
      End If

      If gAnomaly.CallerList <> String.Empty Then
        sMessage = sMessage & vbCrLf & vbCrLf & "Caller List:" & vbCrLf & gAnomaly.CallerList
      End If

      MessageBox.Show(sMessage, gAnomaly.AnomalyMessageTitle, MessageBoxButtons.OK, MessageBoxIcon.Error)
    End If


  End Sub

#End Region

End Module
