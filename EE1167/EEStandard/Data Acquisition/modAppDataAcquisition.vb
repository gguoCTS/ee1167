'Imports NationalInstruments.DAQmx

Public Module modAppDataAcquisition

#Region "Global Variables =================================================="

  Public gstrAnalogChannelList As New Dictionary(Of String, Integer)

  Public gstrChFilters As String()

  Public gstrDaqMultiFunctionDeviceName As String
  Public gstrDaqDIODeviceName As String
  Public gDIOAddressNameChannelPrefix As String
  Public gDIOAddressNameFilterPrefix As String

  Public gsngENRCurrent As Single                     'value of ENR current

  Public gintMeasurementNumber As Integer

  Public gintOpenResistanceValue As Integer

  Public gintResistanceInSeriesWithOverallRes As Integer
  Public gintResistanceInSeriesWithSeriesRes As Integer

  Public gintVRefDivisor As Integer ' 2.0.1.0 TER 
  Public gintNumCycles As Integer

#End Region

#Region "Enumerations =================================================="

  Public Enum DaqSessionEnum As Integer
    ForwardScan = &H100
    ReverseScan = &H200
    Homing = &H300
    LoadPositionMove = &H400
    ForwardScan2 = &H500
    ReverseScan2 = &H600
    PedalReturnTest1 = &H700
    PedalReturnTest2 = &H800
    PedalReturnTest3 = &H900
    PedalReturnTest4 = &HA00
    PedalReturnTest5 = &HB00
    ForwardScanENR = &HC00 ' 3.0.0.0 TER
    DCMotorTest = &HD00
    ChipResetDetection = &HE00
    LowEndMechanicalSearch = &HF00
    HighEndMechanicalSearch = &H1000
    KickDownSearch = &H1100
  End Enum

  Public Enum AnalogInputChannelEnum As Byte
    DutOutput1 = 0
    DutOutput2 = 1
    DutOutput3 = 2
    DutOutput4 = 3
    Force = 4
    Torque = 5
    VRef = 6
    Prog1Vout = 7
    Prog2Vout = 8
    Prog1VRef = 9
    Prog2VRef = 10
  End Enum

  Public Enum AnalogOutputChannelEnum As Byte
    VRefControl = 0
  End Enum

  Public Enum DIOAddressNameEnum As Integer
    OverallResistance = 0
    SeriesResistance = 1
    SwapHeLe = 2
    ExternalFilterLoad = 3
    Ch1Filter1 = 4
    Ch1Filter2 = 5
    Ch1Filter3 = 6
    Ch1Filter4 = 7
    Ch1Load1 = 8
    Ch1Load2 = 9
    Ch2Filter1 = 10
    Ch2Filter2 = 11
    Ch2Filter3 = 12
    Ch2Filter4 = 13
    Ch2Load1 = 14
    Ch2Load2 = 15
    Ch3Filter1 = 16
    Ch3Filter2 = 17
    Ch3Filter3 = 18
    Ch3Filter4 = 19
    Ch3Load1 = 20
    Ch3Load2 = 21
    Ch4Filter1 = 22
    Ch4Filter2 = 23
    Ch4Filter3 = 24
    Ch4Filter4 = 25
    Ch4Load1 = 26
    Ch4Load2 = 27
    Ch1InAmp = 28
    Ch1OutAmp = 29
    Ch2InAmp = 30
    Ch2OutAmp = 31
    Ch3InAmp = 32
    Ch3OutAmp = 33
    Ch4InAmp = 34
    Ch4OutAmp = 35
    HomeProx = 36
    RightOptoButton = 37
    LeftOptoButton = 38
    ClampPart = 39
    Kistler = 40
    ENREnable = 41
    VRefEnable = 42
    Programmer1Enable = 43
    Programmer2Enable = 44
    StartTrigger = 45
  End Enum

#End Region

#Region "Public Methods =================================================="
  'Public Sub AssertStartTrigger_old() 'TER6Gen, replace with new method in callers
  '  Dim StartTriggerCallback As AsyncCallback = New AsyncCallback(AddressOf ResetStartTrigger)

  '  Try
  '    'Start Synchronized tasks
  '    gwriteStartTriggerControl.BeginWriteSingleSampleSingleLine(True, True, StartTriggerCallback, Nothing)

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try

  'End Sub


  'Public Sub CalibrateDAQ()

  '  Dim Device As Device
  '  Try
  '    'Device = DaqSystem.Local.LoadDevice("Dev2")
  '    Device = DaqSystem.Local.LoadDevice(gstrDaqMultiFunctionDeviceName)
  '    Device.Reset()
  '    Device.SelfCalibrate()
  '    'Device = DaqSystem.Local.LoadDevice("Dev1")
  '    Device = DaqSystem.Local.LoadDevice(gstrDaqDIODeviceName)
  '    Device.Reset()

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try

  'End Sub


#End Region




End Module
