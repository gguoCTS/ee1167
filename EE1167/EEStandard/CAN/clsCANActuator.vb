﻿
Imports System.Xml
Imports System.Text
Imports System.IO
Imports System.Linq
Imports System.Threading
Imports System.Collections.Concurrent
Imports System.Text.RegularExpressions
Imports System.Runtime.InteropServices
Imports EE1167.Peak.Can.Basic

Public Class clsCANActuator

#Region "Constructors ========================================================================="
	Public Sub New(ByVal configFilePath As String, strActuatorVariant As String)
		Dim strErrorMessage As String
		Dim CounterFreq As Long
		Dim strVariant As String

		Try

			strVariant = UCase(strActuatorVariant)
			Me.mstrClassConfigFilePath = configFilePath

			Me.mTargetActuator = DirectCast([Enum].Parse(GetType(ActuatorVariantEnum), strVariant), Byte)

			Select Case mTargetActuator
				Case ActuatorVariantEnum.CATFUEL, ActuatorVariantEnum.CATTHROTTLE
					mintToolMessagePriority = CAT_TOOL_MSG_PRIORITY
				Case ActuatorVariantEnum.COVINGTON
					mintToolMessagePriority = TOOL_MSG_PRIORITY 'does this need to change?
				Case Else
					mintToolMessagePriority = TOOL_MSG_PRIORITY
			End Select

			' maybe have a property for ActuatorAddress that can change this from the app code from the parameter set
			mActuatorAddress = DFLT_BOOTLDR_ADDR
			mblnApplicationCodeRunning = True

			' Get the frequency of the Performance Counter
			QueryPerformanceFrequency(CounterFreq)
			' Store the period of the Performance Counter * 1000
			CounterPeriod = (1.0 / CDbl(CounterFreq)) * 1000

			' Get the CAN message handler.
			mCanMsgHandler = clsCanMsgHandling.GetInstance()
			If mCanMsgHandler Is Nothing Then
				strErrorMessage = "Error Creating Instance of clsCanMessageHandling Class"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			'Read configuration settings from file
			Call GetConfigurationSettings()
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			'Set Path for logging CAN Messages
			mCanMsgHandler.CANMessageLogFilePath = mstrCANMessageLogFilePath

			'Get CAN Message Definitions from file
			Call GetCANMessages()
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			'Get Address Ranges from file
			Call GetAddressRanges()
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Call mCanMsgHandler.StartCAN()
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			mCanMsgHandler.RcvMsgDelegates.AddToolMsgDelegate(AddressOf ToolMsgRespRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			' Create transmit CAN message thread and set priority
			mActuatorDownloadThread = New Thread(New ThreadStart(AddressOf ActuatorDownloadThreadFunction))
			mActuatorDownloadThread.Priority = ThreadPriority.AboveNormal

			If (mActuatorDownloadThread.ThreadState = ThreadState.Unstarted) Then
				mActuatorDownloadThread.Start()
			End If

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		Finally
		End Try
	End Sub

#End Region

#Region "Private Member Variables for Multithreading =========================================="

	' CAN message handling class.
	Private mCanMsgHandler As clsCanMsgHandling

	' Collection of request for address claim message responses.
	Private mAddressClaimMsgs As New ConcurrentDictionary(Of String, TPCANMsg)

	' Locking object for address claim collection.
	Private mAddrClaimLock As Object = New Object

	' Collection of received status messages.
	Private mStatusMsgs As New ConcurrentDictionary(Of String, TPCANMsg)

	' Locking object for status message collection.
	Private mStatusMsgLock As Object = New Object

	' Collection of tool response messages.
	Private mToolResponseMsgs As New ConcurrentDictionary(Of String, clsCanMsgHandling.CAN_Msg_Struct)

	' Locking object for tool response message collection.
	Private mToolResponseLock As Object = New Object

	' Collection of tool response messages.
	Private mDfltBtldrToolRespMsgs As New ConcurrentDictionary(Of String, clsCanMsgHandling.CAN_Msg_Struct)

	' Locking object for tool response message collection.
	Private mDfltBtldrToolRespLock As Object = New Object

	' True when actuator is running application code
	Private mblnApplicationCodeRunning As Boolean = False
	' True when actuator is running default bootloader code
	Private mblnDefaultBootloaderCodeRunning As Boolean = False
	' received string from CAT bootloader status message
	Private mstrDefaultBootloaderVersion As String

	' Address of actuator that responded to request for address claim messages.
	Private mActuatorAddress As Byte

	Private mActuatorCount As Byte

	' Elapsed Time Counter (high resolution)
	Private Declare Function QueryPerformanceCounter Lib "Kernel32.dll" (ByRef lpPerformanceCount As Long) As Boolean
	Private Declare Function QueryPerformanceFrequency Lib "Kernel32.dll" (ByRef lpFrequency As Long) As Boolean
	Private HP_CounterValue As Long
	'Private StartingTime As Double
	'Private CurrentTime As Double
	Private CounterPeriod As Double

	'ActuatorDownload Thread Objects
	Private mActuatorDownloadWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
	Private mActuatorDownloadThread As Thread
	Private mblnTerminateActuatorDownloadThread As Boolean = False
	Private mblnActuatorDownloadComplete As Boolean
	Private mintActuatorDownloadPercentComplete As Integer

#End Region

#Region "Private Member Variables ============================================================="

	Private mAnomaly As clsAnomaly
	Private mStackTrace As New clsStackTrace

	Private mstrActuatorDataPath As String
	Private mdtAddressRanges As DataTable
	Private mstrAddressRangesFileName As String
	Private mintAttrIdList(9) As Integer
	Private mintAttrValueList(9) As Integer

	'Private mintBaudRate As Integer

	'Private mCAN As nican
	'Private mintCANHandle As Integer
	'Private mstrCanInterfaceName As String
	Private mCANMessages As New Dictionary(Of clsCANMessage.MessageFunctions, clsCANMessage)
	Private mstrCANMessageDefinitionsFileName As String
	Private mblnCanMessageLoggingEnabled As Boolean
	Private mblnCanActuatorActivityLoggingEnabled As Boolean

	Private mstrCANMessageLogFilePath As String
	Private mCANMessagesToRead As New List(Of clsCANMessage)
	Private mblnCANPortOpen As Boolean
	Private mstrClassConfigFilePath As String

	'Private mEEPromData As New ArrayList


	Private mIntelHexFile As New List(Of IntelDataRecord)
	Private mIntelHexFileDownload As New List(Of IntelDataRecord)
	Private mIntelHexFileDownloadTarget As TargetMemoryAreaEnum

	'Private mintReadQLength As Integer

	Private mstrSerialNumber As String
	'Private mintStartOnOpen As Integer
	Private mStopwatch As New clsStopWatch

	Private mTargetActuator As ActuatorVariantEnum
	Private mbytDefaultBootloaderFault As Byte
	Private mintToolMessagePriority As Integer

	Private mblnWriteComplete As Boolean
	'Private mintWriteQLength As Integer

	Private gDefaultBootloaderAttnOpcode As UShort

#End Region

#Region "Public Constants, Structures, Enums =================================================="

	'Public Structure TPCANMsg
	'  Public ID As Integer   ' 11/29 bit identifier
	'  Public MSGTYPE As Byte ' Bits from MSGTYPE_*
	'  Public LEN As Byte     ' Data Length Code of the Msg (0..8)
	'  Public DATA As Byte()  ' Data 0 .. 7
	'End Structure

	Public Enum TargetMemoryAreaEnum As Byte
		ProgramMemory = 1
		EEProm = 3
		BootLoader = 5
	End Enum

	Public Enum ActuatorVariantEnum As Byte
		LYSANDER = 0
		AYALA = 1
		VIKING = 2
		UNDEFINED = 3
		CATTHROTTLE = 4
		CATFUEL = 5
		COVINGTON = 6
	End Enum

	Public Enum AtmelUniqueID As Byte
		Section0 = 0
		Section1 = 1
		Section2 = 2
	End Enum

	Public Enum ReturnStrFormat As Byte
		Ascii = 0
		AsciiDotDelimited = 1
		ContiguousBytes = 2
		DotDelimited = 3
		HexByteValues = 4
		IntegerValue = 5
		MixedDotDelimited = 6
	End Enum

	' Default bootloader address.
	Public Const DFLT_BOOTLDR_ADDR As Byte = &H2

	' CAN Message ID's
	Public Const STATUS_MESSAGE As Integer = &H18FFC500
	Public Const CAT_FUEL_STATUS_MESSAGE As Integer = &H18FE8100
	Public Const CAT_THROTTLE_STATUS_MESSAGE As Integer = &H18FCF200
	Public Const TOOL_MSG_RESPONSE As Integer = &H18FF0D00
	Public Const UDS_MSG_RESPONSE As Integer = &H18DAF100
	Public Const ADDR_CLAIM As Integer = &H18EEFF00
	Public Const CAT_ADDR_CLAIM As Integer = &H18EE0000

	Public Const TOOL_MSG_PRIORITY = &H18000000
	Public Const CAT_TOOL_MSG_PRIORITY = &HC000000
	Public Const CTT_CMD_MSG_PRIORITY = &HC000000

	Public Const TOOL_MSG_PGN = &HFF0E00
	Public Const CTT_CMD_MSG_PGN = &HFFC600
	'Public Const CTT_ADXCLAIMREQ_MSG_PGN = &HAEFF00

	' CAN Message ID address masks.
	Public Const SRC_ADDR_MASK As Integer = &HFFFFFF00


#End Region

#Region "Public Properties ===================================================================="
	Public ReadOnly Property ActuatorDownloadPercentComplete As Integer
		Get
			Return mintActuatorDownloadPercentComplete
		End Get
	End Property

	Public ReadOnly Property ActuatorDownloadComplete As Boolean
		Get
			Return mblnActuatorDownloadComplete
		End Get
	End Property

	Public Property Anomaly() As clsAnomaly
		Get
			Return mAnomaly
		End Get
		Set(ByVal value As clsAnomaly)
			mAnomaly = value
		End Set
	End Property

	Public ReadOnly Property ApplicationCodeRunning As Boolean
		Get
			Return mblnApplicationCodeRunning
		End Get
	End Property

	Public ReadOnly Property CANMessages As Dictionary(Of clsCANMessage.MessageFunctions, clsCANMessage)
		Get
			Return Me.mCANMessages
		End Get
	End Property

	'Public ReadOnly Property EEPromData As ArrayList
	'  Get
	'    Return Me.mEEPromData
	'  End Get
	'End Property

	Public ReadOnly Property DefaultBootloaderFault As Byte
		Get
			Return mbytDefaultBootloaderFault
		End Get
	End Property


	Public ReadOnly Property IntelHexFile As List(Of IntelDataRecord)
		Get
			Return mIntelHexFile
		End Get
	End Property

	Public ReadOnly Property IntelHexFileDownload As List(Of IntelDataRecord)
		Get
			Return mIntelHexFileDownload
		End Get

	End Property
	Public Property IntelHexFileDownloadTarget As TargetMemoryAreaEnum
		Get
			Return mIntelHexFileDownloadTarget
		End Get
		Set(value As TargetMemoryAreaEnum)
			mIntelHexFileDownloadTarget = value
		End Set
	End Property
	Public Property CanActuatorActivityLoggingEnabled As Boolean
		Get
			Return mblnCanActuatorActivityLoggingEnabled
		End Get
		Set(value As Boolean)
			mblnCanActuatorActivityLoggingEnabled = value
			mCanMsgHandler.CanActuatorActivityLoggingEnabled = value
		End Set
	End Property

	Public Property CanMessageLoggingEnabled As Boolean
		Get
			Return mblnCanMessageLoggingEnabled
		End Get
		Set(value As Boolean)
			mblnCanMessageLoggingEnabled = value
			mCanMsgHandler.CanMessageLoggingEnabled = value
			If mCanMsgHandler.CanMessageLoggingEnabled Then
				mCanMsgHandler.StartCanLoggingThread()
			End If

		End Set
	End Property

	Public ReadOnly Property TargetActuator() As ActuatorVariantEnum
		Get
			Return mTargetActuator
		End Get
	End Property

	Public ReadOnly Property CanMsgHandler() As clsCanMsgHandling
		Get
			Return mCanMsgHandler
		End Get
	End Property

#End Region

#Region "Private Methods ======================================================================"

	Private Function CloseSession() As Boolean
		Dim canMsg As New TPCANMsg
		Dim return_value As Boolean
		Try

			' Close the default bootloader programming session.
			Dim closeSessionMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderCloseSessionCmd)
			mCanMsgHandler.LoadCANMsgOutputQueue(closeSessionMsg)
			' Wait for Ack from actuator.
			return_value = GetCloseSessionAck(canMsg)
			If (return_value = False) Then
				Console.WriteLine("Close Session Failed 1st Try, Retrying")
				' Try closing the default bootloader programming session again.
				mCanMsgHandler.LoadCANMsgOutputQueue(closeSessionMsg)
				' Wait for Ack from actuator.
				return_value = GetCloseSessionAck(canMsg)
				If return_value = True And mAnomaly IsNot Nothing Then
					'Close Session succeded, clear the previous Anomaly
					mAnomaly = Nothing
				End If
				Console.WriteLine("Close Session Result 2nd Try: " & CStr(return_value))
			End If
			Return return_value

		Catch ex As TsopAnomalyException
			gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Private Sub GetAddressRanges(Optional ByVal strFilter As String = Nothing)
		Dim dt As New System.Data.DataTable
		Dim blnFirstLine As Boolean = True
		Dim strFilterFieldValue As String
		Dim strFilterColumnName As String
		Dim intFilterColumn As Integer
		Dim strFile As String
		Dim cols() As String
		Dim col As String
		Dim data() As String
		Dim dr As DataRow
		Try

			strFilterColumnName = ""
			strFilterFieldValue = ""
			intFilterColumn = -1
			cols = Nothing
			data = Nothing

			If strFilter <> Nothing Then
				strFilterColumnName = (strFilter.Split("=")(0)).Trim
				strFilterFieldValue = (strFilter.Split("=")(1)).Trim
			End If

			'strFile = strFilePath & strFileName
			strFile = mstrClassConfigFilePath & mstrAddressRangesFileName

			If IO.File.Exists(strFile) Then
				Using sr As New StreamReader(strFile)
					While Not sr.EndOfStream
						If blnFirstLine Then
							blnFirstLine = False
							cols = sr.ReadLine.Split(",")
							For Each col In cols
								dt.Columns.Add(New DataColumn(col, GetType(String)))
							Next
							If strFilterColumnName <> "" Then
								intFilterColumn = Array.IndexOf(cols, strFilterColumnName)
							End If
						Else
							data = sr.ReadLine.Split(",")
							If intFilterColumn <> -1 Then
								If data(intFilterColumn) = strFilterFieldValue Then
									dt.Rows.Add(data.ToArray)
								End If
							Else
								dt.Rows.Add(data.ToArray)
							End If
						End If
					End While
				End Using
			End If

			For Each dr In dt.Rows
				dr.Item("ActuatorVariant") = UCase(dr.Item("ActuatorVariant"))
			Next

			mdtAddressRanges = dt

		Catch ex As TsopAnomalyException
			gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Private Sub GetCANMessages(Optional ByVal strFilter As String = Nothing)
		Dim dt As New System.Data.DataTable
		Dim blnFirstLine As Boolean = True
		Dim strFilterFieldValue As String
		Dim strFilterColumnName As String
		Dim intFilterColumn As Integer
		Dim strFile As String
		Dim cols() As String
		Dim col As String
		Dim data() As String
		Dim newCANMessage As clsCANActuator.clsCANMessage
		Dim rowCAN As DataRow
		Dim intDataIndex As Integer
		Dim strTempData() As String
		Try

			strFilterColumnName = ""
			strFilterFieldValue = ""
			intFilterColumn = -1
			cols = Nothing
			data = Nothing

			If strFilter <> Nothing Then
				strFilterColumnName = (strFilter.Split("=")(0)).Trim
				strFilterFieldValue = (strFilter.Split("=")(1)).Trim
			End If

			'strFile = strFilePath & strFileName
			strFile = mstrClassConfigFilePath & mstrCANMessageDefinitionsFileName

			If IO.File.Exists(strFile) Then
				Using sr As New StreamReader(strFile)
					While Not sr.EndOfStream
						If blnFirstLine Then
							blnFirstLine = False
							cols = sr.ReadLine.Split(",")
							For Each col In cols
								dt.Columns.Add(New DataColumn(col, GetType(String)))
							Next
							If strFilterColumnName <> "" Then
								intFilterColumn = Array.IndexOf(cols, strFilterColumnName)
							End If
						Else
							data = sr.ReadLine.Split(",")
							If intFilterColumn <> -1 Then
								If data(intFilterColumn) = strFilterFieldValue Then
									dt.Rows.Add(data.ToArray)
								End If
							Else
								dt.Rows.Add(data.ToArray)
							End If
						End If
					End While
				End Using
			End If

			For Each rowCAN In dt.Rows
				newCANMessage = New clsCANMessage
				Try
					newCANMessage.MessageFunction = DirectCast([Enum].Parse(GetType(clsCANMessage.MessageFunctions),
																  rowCAN.Item("Function")), Integer)
					If rowCAN.Item("OpCode").ToString.Contains("x") Then
						newCANMessage.OpCode = Convert.ToInt32(rowCAN.Item("OpCode").Split("x")(1).Trim, 16)
					Else
						newCANMessage.OpCode = Convert.ToInt32(rowCAN.Item("OpCode"), 16)
					End If
					'newCANMessage.MessageType = DirectCast([Enum].Parse(GetType(clsCANMessage.MessageTypes), rowCAN.Item("MessageType")), Integer)
					newCANMessage.SendArbitrationID = Convert.ToInt32(rowCAN.Item("SendArbitrationID"), 16)
					'newCANMessage.ReceiveArbitrationIDMin = Convert.ToInt32(rowCAN.Item("ReceiveArbitrationIDMin"), 16)
					'newCANMessage.ReceiveArbitrationIDMax = Convert.ToInt32(rowCAN.Item("ReceiveArbitrationIDMax"), 16)
					'newCANMessage.FrameRateMilliseconds = rowCAN.Item("FrameRateMilliseconds")
					newCANMessage.DataLength = rowCAN.Item("DataLength")
					'newCANMessage.DecodingMethod = DirectCast([Enum].Parse(GetType(clsCANMessage.DecodingMethods), rowCAN.Item("DecodingMethod")), Integer)
					newCANMessage.DecodingStartByte = rowCAN.Item("DecodingStartByte")
					newCANMessage.DecodingByteLength = rowCAN.Item("DecodingByteLength")
					'newCANMessage.FieldsToReturn = rowCAN.Item("FieldsToReturn")
					newCANMessage.ReverseByteOrder = CBool(rowCAN.Item("ReverseByteOrder"))
					'newCANMessage.DynamicWriteByteCount = rowCAN.Item("DynamicWriteByteCount")
					'newCANMessage.NumberOfTimesToSendMessage = rowCAN.Item("NumberOfTimesToSendMessage")
					strTempData = Split(rowCAN.Item("StaticData"), ".")
					For intDataIndex = 0 To strTempData.Length - 1 '7
						If strTempData(intDataIndex).ToUpper = "XX" Then
							newCANMessage.StaticData(intDataIndex) = strTempData(intDataIndex)
						Else
							newCANMessage.StaticData(intDataIndex) = Convert.ToInt32(Trim(strTempData(intDataIndex)), 16)
							newCANMessage.Data(intDataIndex) = Convert.ToInt32(Trim(strTempData(intDataIndex)), 16)
						End If
					Next

					'strTempData = Split(rowCAN.Item("ReturnFieldBytePositions"), ".")
					'ReDim newCANMessage.ReturnFieldBytePositions(strTempData.Length - 1)
					'For intDataIndex = 0 To strTempData.Length - 1
					'  newCANMessage.ReturnFieldBytePositions(intDataIndex) = Convert.ToInt32(strTempData(intDataIndex), 16)
					'Next

					mCANMessages.Add(newCANMessage.MessageFunction, newCANMessage)
				Catch e As ArgumentException
					'skip this entry from the CSV file, not in the enum
				End Try
			Next
			gDefaultBootloaderAttnOpcode = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest).OpCode

		Catch ex As TsopAnomalyException
			gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Private Sub GetConfigurationSettings()
		Dim configReader As XmlTextReader
		Dim configSet As New DataSet
		Dim configTable As DataTable
		Dim configRow As DataRow
		Dim strClassName As String
		Try

			strClassName = Me.GetType.Name

			'Create the XML Reader
			configReader = New XmlTextReader(Me.mstrClassConfigFilePath & strClassName & ".cfg")

			'Clear data set that holds xml file contents
			configSet.Clear()
			'Read the file
			configSet.ReadXml(configReader)

			'Config File not hierarchical, only 1 table and 1 row, with many fields
			configTable = configSet.Tables(0)
			configRow = configTable.Rows(0)

			'Assign field values to variables
			'Me.mintBaudRate = configRow("BaudRate")
			'Me.mintStartOnOpen = configRow("StartOnOpen")
			'Me.mintReadQLength = configRow("ReadQLength")
			'Me.mintWriteQLength = configRow("WriteQLength")
			mCanMsgHandler.NICanInterfaceName = configRow("NICANInterfaceName")
			'Me.mstrCANInterfaceName = configRow("CANInterfaceName")

			Select Case mTargetActuator
				Case ActuatorVariantEnum.CATFUEL, ActuatorVariantEnum.CATTHROTTLE
					Me.mstrCANMessageDefinitionsFileName = configRow("CATCANMessageDefinitionsFileName")
				Case ActuatorVariantEnum.COVINGTON
					Me.mstrCANMessageDefinitionsFileName = configRow("COVCANMessageDefinitionsFileName")
				Case Else
					Me.mstrCANMessageDefinitionsFileName = configRow("CANMessageDefinitionsFileName")
			End Select

			'Me.mstrCANMessageDefinitionsFileName = configRow("CANMessageDefinitionsFileName")
			Me.mstrAddressRangesFileName = configRow("AddressRangesFileName")
			Me.mstrActuatorDataPath = configRow("ActuatorDataPath")
			Me.mstrCANMessageLogFilePath = configRow("CANMessageLogFilePath")
			mCanMsgHandler.CanInterfaceMfg = DirectCast([Enum].Parse(GetType(clsCanMsgHandling.CanInterfaceMfgEnum),
															   configRow("CANInterfaceMfg")), Integer)
			mCanMsgHandler.PeakCanHandle = configRow("PeakCanHandle")

			'Close reader, releases file
			configReader.Close()

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		Finally
		End Try
	End Sub

	Private Function GetCloseSessionAck(ByRef canMsg As TPCANMsg) As Boolean
		Dim blnreturnValue As Boolean
		Dim intOpCodeDBLCloseSessionAck As UShort
		Dim intOpCodeDBLFault As UShort
		Dim strErrorMessage As String
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double
		Try
			Dim closeSessionAck = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderCloseSessionAck)
			intOpCodeDBLCloseSessionAck = closeSessionAck.OpCode
			intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
			mbytDefaultBootloaderFault = 0

			' Initialize the elapsed time starting value
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
			' Wait up to two seconds.
			Do
				If ((GetToolMsgRespCount(intOpCodeDBLCloseSessionAck) > 0) OrElse
			(GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
					' Received message count > 0
					Exit Do
				End If
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
			Loop While (dblCurrentTime - dblStartTime < 2000)

			' Read the default bootloader close session acknowledge.
			If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
				'strErrorMessage = "Fault Occured In Default Bootloader"
				mbytDefaultBootloaderFault = canMsg.DATA(1)
				strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) &
						  " Occured In Default Bootloader: Close Session Ack"
				Console.WriteLine(strErrorMessage)
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
				'Exit Try
			ElseIf (GetToolMsgResp(intOpCodeDBLCloseSessionAck, canMsg) = True) Then
				blnreturnValue = True
			End If
			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Function GetMemoryReadInitAck(ByRef canMsg As TPCANMsg) As Boolean

		Dim blnreturnValue As Boolean = False
		Dim intOpCodeDBLInitMemReadAck As UShort
		Dim intOpCodeDBLFault As UShort
		Dim strErrorMessage As String
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double

		Try
			Dim initMemReadAck = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryReadAck)
			intOpCodeDBLInitMemReadAck = initMemReadAck.OpCode
			intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
			mbytDefaultBootloaderFault = 0

			' Initialize the elapsed time starting value
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
			' Wait up to one second.
			Do
				'If ((GetToolMsgRespCount(OPCODE.DFLT_BOOTLDR_INIT_READ_ACK) > 0) OrElse _
				'   (GetToolMsgRespCount(OPCODE.DFLT_BOOTLDR_FAULT) > 0)) Then
				'  ' Received message count > 0
				'  Exit Do
				'End If
				If ((GetToolMsgRespCount(intOpCodeDBLInitMemReadAck) > 0) OrElse
			 (GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
					' Received message count > 0
					Exit Do
				End If
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
			Loop While (dblCurrentTime - dblStartTime < 1000)

			' Read the default bootloader initiate memory read acknowledge.
			If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
				'strErrorMessage = "Fault Occured In Default Bootloader"
				mbytDefaultBootloaderFault = canMsg.DATA(1)
				strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) &
						  " Occured In Default Bootloader: Init Memory Read Ack"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
				'Exit Try 'throw new TsopAnomalyException
			ElseIf (GetToolMsgResp(intOpCodeDBLInitMemReadAck, canMsg) = True) Then
				blnreturnValue = True
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Function GetMemoryReadData(ByRef canMsg As TPCANMsg) As Boolean 'As RESULT
		Dim blnreturnValue As Boolean
		Dim intOpCodeDBLMemReadData As UShort
		Dim intOpCodeDBLFault As UShort
		Dim strErrorMessage As String
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double

		Try
			blnreturnValue = False
			intOpCodeDBLMemReadData = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryReadData).OpCode
			intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
			mbytDefaultBootloaderFault = 0

			' Initialize the elapsed time starting value
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
			' Wait up to one second.
			Do
				If ((GetToolMsgRespCount(intOpCodeDBLMemReadData) > 0) OrElse
			(GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
					' Received message count > 0
					Exit Do
				End If
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
			Loop While (dblCurrentTime - dblStartTime < 1000)

			' Read the default bootloader memory read data response message.
			If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
				'strErrorMessage = "Fault Occured In Default Bootloader"
				mbytDefaultBootloaderFault = canMsg.DATA(1)
				strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) &
						  " Occured In Default Bootloader: Memory Read Data"
				Console.WriteLine(strErrorMessage)
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
				'Exit Try
			ElseIf (GetToolMsgResp(intOpCodeDBLMemReadData, canMsg) = True) Then
				blnreturnValue = True
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Function GetMemoryWriteInitAck(ByRef canMsg As TPCANMsg) As Boolean
		Dim blnReturnValue As Boolean = False
		Dim intOpCodeDBLInitMemWriteAck As UShort
		Dim intOpCodeDBLFault As UShort
		Dim strErrorMessage As String = ""
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double

		Try
			intOpCodeDBLInitMemWriteAck =
		CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryWriteAck).OpCode
			intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
			mbytDefaultBootloaderFault = 0

			' Initialize the elapsed time starting value
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod

			' Wait up to one second.
			Do
				If ((GetToolMsgRespCount(intOpCodeDBLInitMemWriteAck) > 0) OrElse
			(GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
					' Received message count > 0
					Exit Do
				End If
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
			Loop While (dblCurrentTime - dblStartTime < 500)

			' Read the default bootloader initiate memory write acknowledge.
			If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
				'strErrorMessage = "Fault Occured In Default Bootloader"
				mbytDefaultBootloaderFault = canMsg.DATA(1)
				strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) &
					" Occured In Default Bootloader: Init Memory Write Ack"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
				'Exit Try 'throw new TsopAnomalyException
			ElseIf (GetToolMsgResp(intOpCodeDBLInitMemWriteAck, canMsg) = True) Then
				blnReturnValue = True
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Function GetMemoryWriteDataAck(ByRef canMsg As TPCANMsg) As Boolean
		Dim blnreturnValue As Boolean = False
		Dim intOpCodeDBLMemWriteDataAck As UShort
		Dim intOpCodeDBLFault As UShort
		Dim strErrorMessage As String = ""
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double

		Try

			intOpCodeDBLMemWriteDataAck =
		CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryWriteDataAck).OpCode
			intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
			mbytDefaultBootloaderFault = 0

			' Initialize the elapsed time starting value
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod

			' Wait up to one second.
			Do
				If ((GetToolMsgRespCount(intOpCodeDBLMemWriteDataAck) > 0) OrElse
			(GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
					' Received message count > 0
					Exit Do
				End If
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
			Loop While (dblCurrentTime - dblStartTime < 500)

			' Read the default bootloader memory write acknowledge.
			If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
				'strErrorMessage = "Fault Occured In Default Bootloader"
				mbytDefaultBootloaderFault = canMsg.DATA(1)
				strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) &
						  " Occured In Default Bootloader: Init Memory Write Ack"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			ElseIf (GetToolMsgResp(intOpCodeDBLMemWriteDataAck, canMsg) = True) Then
				blnreturnValue = True
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Function GetToolMsgResp(ByVal msgOpcode As UShort, ByRef canMsg As TPCANMsg) As Boolean
		Dim readMsgStruct As clsCanMsgHandling.CAN_Msg_Struct
		Dim returnValue As Boolean = False
		Dim key As String = ""
		Dim msg_str As New clsCanMsgHandling.CAN_Msg_Struct

		Try
			' Check for a valid opcode. Invalid opcodes result in return value of False.
			If (mblnApplicationCodeRunning = True) Then
				SyncLock mToolResponseLock
					'Select Case msgOpcode
					'  Case OPCODE.SW_VERSION_ID, _
					'       OPCODE.APPLICATION_CHKSUM_CALC, _
					'       OPCODE.SERIAL_NUMBER, _
					'       OPCODE.HW_PART_NUM_MS, _
					'       OPCODE.HW_PART_NUM_LS, _
					'       OPCODE.SW_PART_NUM_MS, _
					'       OPCODE.SW_PART_NUM_LS, _
					'       OPCODE.INTERNAL_TEMP, _
					'       OPCODE.BATTERY_VOLTAGE, _
					'       OPCODE.MICRO_OSC_FREQ, _
					'       OPCODE.MICRO_PLL_SELECT
					If msgOpcode = &HFFFFUS Then
						Dim found As Boolean = False
						Dim theKey As String = ""
						For Each msgItem As KeyValuePair(Of String, clsCanMsgHandling.CAN_Msg_Struct) In mToolResponseMsgs
							If ((msgItem.Value.CanMsg.ID And &HFFFFFF00) = UDS_MSG_RESPONSE) Then
								found = True
								readMsgStruct = msgItem.Value
								theKey = msgItem.Key
								canMsg = readMsgStruct.CanMsg
							End If
						Next

						If found Then
							mToolResponseMsgs.TryRemove(theKey, readMsgStruct)
							returnValue = True
						End If
					Else
						key = msgOpcode.ToString

						If (mToolResponseMsgs.ContainsKey(key)) Then
							readMsgStruct = mToolResponseMsgs.Item(key)
							' Return selected message.
							canMsg = readMsgStruct.CanMsg
							' Consume the message (message struct). Until another message
							' is received , GetToolMsgRespCount() will return 0 for this opcode.
							mToolResponseMsgs.TryRemove(key, msg_str)
							returnValue = True
						End If
					End If

					'End Select
				End SyncLock
			Else
				SyncLock mDfltBtldrToolRespLock
					'Select Case msgOpcode
					'  Case OPCODE.DFLT_BOOTLDR_ATTN, _
					'       OPCODE.DFLT_BOOTLDR_INIT_WRITE, _
					'       OPCODE.DFLT_BOOTLDR_MEM_WRITE, _
					'       OPCODE.DFLT_BOOTLDR_INIT_READ, _
					'       OPCODE.DFLT_BOOTLDR_MEM_READ, _
					'       OPCODE.DFLT_BOOTLDR_CALC_CHKSUM, _
					'       OPCODE.DFLT_BOOTLDR_SERIAL_NUMBER, _
					'       OPCODE.DFLT_BOOTLDR_CLOSE_SESSION, _
					'       OPCODE.DFLT_BOOTLDR_START_APP, _
					'       OPCODE.DFLT_BOOTLDR_RESTART, _
					'       OPCODE.DFLT_BOOTLDR_FAULT

					key = msgOpcode.ToString
					If (mDfltBtldrToolRespMsgs.ContainsKey(key)) Then
						readMsgStruct = mDfltBtldrToolRespMsgs.Item(key)
						' Return specified message.
						canMsg = readMsgStruct.CanMsg
						' Consume the message (message struct). Until another message
						' is received , GetToolMsgRespCount() will return 0 for this opcode.
						mDfltBtldrToolRespMsgs.TryRemove(key, msg_str)
						returnValue = True
					End If
					'End Select
				End SyncLock
			End If

			Return returnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Function GetToolMsgRespCount(ByVal msgOpcode As UShort) As UShort
		Dim readMsgStruct As clsCanMsgHandling.CAN_Msg_Struct
		Dim msgCount As UShort = 0
		Dim key As String = ""

		Try
			'' Check for valid opcode. Invalid opcodes result in return value of 0.
			If (mblnApplicationCodeRunning = True) Then
				SyncLock mToolResponseLock
					'    Select Case msgOpcode
					'      Case OPCODE.SW_VERSION_ID, _
					'           OPCODE.APPLICATION_CHKSUM_CALC, _
					'           OPCODE.SERIAL_NUMBER, _
					'           OPCODE.HW_PART_NUM_MS, _
					'           OPCODE.HW_PART_NUM_LS, _
					'           OPCODE.SW_PART_NUM_MS, _
					'           OPCODE.SW_PART_NUM_LS, _
					'           OPCODE.INTERNAL_TEMP, _
					'           OPCODE.BATTERY_VOLTAGE, _
					'           OPCODE.MICRO_OSC_FREQ, _
					'           OPCODE.MICRO_PLL_SELECT

					If msgOpcode = &HFFFFUS Then
						Dim theKey As String = ""
						For Each msgItem As KeyValuePair(Of String, clsCanMsgHandling.CAN_Msg_Struct) In mToolResponseMsgs
							If ((msgItem.Value.CanMsg.ID And &HFFFFFF00) = UDS_MSG_RESPONSE) Then
								msgCount += 1
							End If
						Next

					Else
						key = msgOpcode.ToString
						If (mToolResponseMsgs.ContainsKey(key)) Then
							' Return the number of specified messages received since
							' the last GetToolMsgResp() call for specified opcode.
							readMsgStruct = mToolResponseMsgs.Item(key)
							msgCount = readMsgStruct.msgCount
						End If
					End If
					'End Select
				End SyncLock
			Else
				SyncLock mDfltBtldrToolRespLock
					'Select Case msgOpcode
					'  Case OPCODE.DFLT_BOOTLDR_ATTN, _
					'       OPCODE.DFLT_BOOTLDR_INIT_WRITE, _
					'       OPCODE.DFLT_BOOTLDR_MEM_WRITE, _
					'       OPCODE.DFLT_BOOTLDR_INIT_READ, _
					'       OPCODE.DFLT_BOOTLDR_MEM_READ, _
					'       OPCODE.DFLT_BOOTLDR_CALC_CHKSUM, _
					'       OPCODE.DFLT_BOOTLDR_SERIAL_NUMBER, _
					'       OPCODE.DFLT_BOOTLDR_CLOSE_SESSION, _
					'       OPCODE.DFLT_BOOTLDR_START_APP, _
					'       OPCODE.DFLT_BOOTLDR_RESTART, _
					'       OPCODE.DFLT_BOOTLDR_FAULT

					key = msgOpcode.ToString
					If (mDfltBtldrToolRespMsgs.ContainsKey(key)) Then
						' Return the number of specified messages received since
						' the last GetToolMsgResp() call for specified opcode.
						readMsgStruct = mDfltBtldrToolRespMsgs.Item(key)
						msgCount = readMsgStruct.msgCount
					End If
					'End Select
				End SyncLock
			End If

			Return msgCount

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function

	Private Function GetStatusMsg(ByVal sourceID As UShort, ByRef canMsg As TPCANMsg) As Boolean
		Dim readMsgStruct As TPCANMsg
		Dim returnValue As Boolean = False
		Dim key As String = ""

		Try

			SyncLock mStatusMsgLock
				key = sourceID.ToString
				If (mStatusMsgs.ContainsKey(key)) Then
					readMsgStruct = mStatusMsgs.Item(key)
					' Return selected message.
					canMsg = readMsgStruct
					' Consume the message (message struct). Until another message
					' is received , GetToolMsgRespCount() will return 0 for this opcode.
					mStatusMsgs.TryRemove(key, readMsgStruct)
					returnValue = True
				End If
			End SyncLock

			Return returnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function


	Private Function RecordToString(ByVal Record As IntelDataRecord) As String
		Dim RecordStr As String = ""

		Try
			RecordStr = RecordStr & ":"
			If Record.DataLength < 16 Then
				RecordStr = RecordStr & "0" & Hex(Record.DataLength)
			Else
				RecordStr = RecordStr & Hex(Record.DataLength)
			End If
			Select Case Record.StartAddress
				Case Is < 16
					RecordStr = RecordStr & "000" & Hex(Record.StartAddress)
				Case Is < 256
					RecordStr = RecordStr & "00" & Hex(Record.StartAddress)
				Case Is < 4096
					RecordStr = RecordStr & "0" & Hex(Record.StartAddress)
				Case Else
					RecordStr = RecordStr & Hex(Record.StartAddress)
			End Select
			If Record.RecordType < 16 Then
				RecordStr = RecordStr & "0" & Hex(Record.RecordType)
			Else
				RecordStr = RecordStr & Hex(Record.RecordType)
			End If

			If Record.DataLength <> 0 Then
				For j As Integer = 0 To Record.Data.Length - 1
					If Record.Data(j) < 16 Then
						RecordStr = RecordStr & "0" & Hex(Record.Data(j))
					Else
						RecordStr = RecordStr & Hex(Record.Data(j))
					End If
				Next
			End If

			If Record.CheckSum < 16 Then
				RecordStr = RecordStr & "0" & Hex(Record.CheckSum)
			Else
				RecordStr = RecordStr & Hex(Record.CheckSum)
			End If
			Return RecordStr

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function


	Private Function ReturnASCIIString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
		Try
			Dim intIndex As Integer
			Dim intStartByte As Integer
			Dim intEndByte As Integer
			Dim intByteIncrement As Integer
			Dim strByteValue As String
			Dim strTemp As StringBuilder
			Dim strReturnValue As String

			strTemp = New StringBuilder((Data.Length * 2))

			If CANMessage.ReverseByteOrder Then
				intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intEndByte = CANMessage.DecodingStartByte
				intByteIncrement = -1
			Else
				intStartByte = CANMessage.DecodingStartByte
				intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intByteIncrement = 1
			End If

			strByteValue = ""
			For intIndex = intStartByte To intEndByte Step intByteIncrement
				If (Data(intIndex) < 48) Then '< "0" 0x30
					strByteValue = strByteValue & "?"
				Else
					strByteValue = strByteValue & Chr(Data(intIndex))
				End If
			Next
			strReturnValue = strByteValue
			Return strReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function

	Private Function ReturnASCIIStringDotDelimitEveryTwoBytes(ByVal CANMessage As clsCANMessage,
															ByVal Data() As Byte) As String
		Try
			Dim intIndex As Integer
			Dim intStartByte As Integer
			Dim intEndByte As Integer
			Dim intByteIncrement As Integer
			Dim strByteValue As String
			Dim strTemp As StringBuilder
			Dim strReturnValue As String

			strTemp = New StringBuilder((Data.Length * 2))

			If CANMessage.ReverseByteOrder Then
				intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intEndByte = CANMessage.DecodingStartByte
				intByteIncrement = -1
			Else
				intStartByte = CANMessage.DecodingStartByte
				intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intByteIncrement = 1
			End If

			strByteValue = ""
			For intIndex = intStartByte To intEndByte Step intByteIncrement
				strByteValue = strByteValue & Chr(Data(intIndex))
				intIndex += intByteIncrement
				strByteValue = strByteValue & Chr(Data(intIndex))
				If intIndex <> intEndByte Then
					strByteValue = strByteValue & "."
				End If
			Next
			strReturnValue = strByteValue
			Return strReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function


	Private Function ReturnMixedASCIIStringDotDelimitEveryTwoBytes(ByVal CANMessage As clsCANMessage,
															ByVal Data() As Byte) As String
		Try
			Dim intIndex As Integer
			Dim intStartByte As Integer
			Dim intEndByte As Integer
			Dim intByteIncrement As Integer
			Dim strByteValue As String
			Dim strTemp As StringBuilder
			Dim strReturnValue As String

			strTemp = New StringBuilder((Data.Length * 2))

			If CANMessage.ReverseByteOrder Then
				intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intEndByte = CANMessage.DecodingStartByte
				intByteIncrement = -1
			Else
				intStartByte = CANMessage.DecodingStartByte
				intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intByteIncrement = 1
			End If

			strByteValue = ""
			For intIndex = intStartByte To intEndByte Step intByteIncrement
				strByteValue = strByteValue & Chr(Data(intIndex))
				intIndex += intByteIncrement
				strByteValue = strByteValue & Data(intIndex)
				If intIndex <> intEndByte Then
					strByteValue = strByteValue & "."
				End If
			Next
			strReturnValue = strByteValue
			Return strReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function
	Private Function ReturnDotDelimitedString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
		Try
			Dim intIndex As Integer
			Dim intStartByte As Integer
			Dim intEndByte As Integer
			Dim intByteIncrement As Integer
			Dim strByteValue As String
			Dim strTemp As StringBuilder
			Dim strReturnValue As String

			strTemp = New StringBuilder((Data.Length * 2))

			If CANMessage.ReverseByteOrder Then
				intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intEndByte = CANMessage.DecodingStartByte
				intByteIncrement = -1
			Else
				intStartByte = CANMessage.DecodingStartByte
				intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intByteIncrement = 1
			End If

			strByteValue = ""
			For intIndex = intStartByte To intEndByte Step intByteIncrement
				strByteValue = strByteValue & Data(intIndex)
				'intIndex += intByteIncrement
				'strByteValue = strByteValue & Chr(Data(intIndex))
				If intIndex <> intEndByte Then
					strByteValue = strByteValue & "."
				End If
			Next
			strReturnValue = strByteValue
			Return strReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function

	Private Function ReturnContiguousByteString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
		Try
			Dim intIndex As Integer
			Dim intStartByte As Integer
			Dim intEndByte As Integer
			Dim intByteIncrement As Integer
			Dim strByteValue As String
			Dim strTemp As StringBuilder
			Dim strReturnValue As String

			strTemp = New StringBuilder((Data.Length * 2))

			If CANMessage.ReverseByteOrder Then
				intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intEndByte = CANMessage.DecodingStartByte
				intByteIncrement = -1
			Else
				intStartByte = CANMessage.DecodingStartByte
				intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intByteIncrement = 1
			End If

			' A string containing multiple non-delimited decimal values doesn't seem
			' very useful. This function will return a single byte string without delimiter.
			' This is currently the only use case.
			' Multi-byte result strings will be comma delimited.
			strByteValue = Data(intStartByte)
			For intIndex = intStartByte + 1 To intEndByte Step intByteIncrement
				strByteValue = strByteValue & "," & Data(intIndex)
			Next
			strReturnValue = strByteValue
			Return strReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function

	Private Function ReturnHexString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
		Try
			Dim intIndex As Integer
			Dim intStartByte As Integer
			Dim intEndByte As Integer
			Dim intByteIncrement As Integer
			'Dim intByteValue As Integer
			Dim strTemp As StringBuilder
			Dim strReturnValue As String

			strTemp = New StringBuilder((Data.Length * 2))

			If CANMessage.ReverseByteOrder Then
				intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intEndByte = CANMessage.DecodingStartByte
				intByteIncrement = -1
			Else
				intStartByte = CANMessage.DecodingStartByte
				intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intByteIncrement = 1
			End If

			For intIndex = intStartByte To intEndByte Step intByteIncrement
				'intByteValue = Data(intIndex)
				'strTemp.Append((Conversion.Hex(intByteValue)).PadLeft(2, "0"))
				strTemp.Append(Data(intIndex).ToString("X2"))
			Next
			strReturnValue = strTemp.ToString
			Return strReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function

	Private Function ReturnIntegerValueString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
		Try
			Dim intIndex As Integer
			Dim intStartByte As Integer
			Dim intEndByte As Integer
			Dim intByteIncrement As Integer
			Dim intByteValue As UInt32
			Dim intValue As UInt32
			Dim strReturnValue As String
			Dim intShift As Integer

			'Reverse Byte Order means MSB first (left), LSB last (right)?

			If CANMessage.ReverseByteOrder Then
				intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intEndByte = CANMessage.DecodingStartByte
				intByteIncrement = -1
			Else
				intStartByte = CANMessage.DecodingStartByte
				intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
				intByteIncrement = 1
			End If

			intByteValue = 0
			intValue = 0
			intShift = 0
			For intIndex = intStartByte To intEndByte Step intByteIncrement
				intByteValue = Data(intIndex)
				intValue = intValue + (intByteValue << (8 * intShift))
				intShift += 1
			Next
			strReturnValue = CStr(intValue)
			Return strReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function

	Private Function SendMessageNoResponse(canMessage As clsCANMessage) As Boolean

		Try

			'Load Output Queue with Message.
			mCanMsgHandler.LoadCANMsgOutputQueue(canMessage)

			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Return True

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Function SendMessageGetResponse(canMessage As clsCANMessage, ByRef data() As Byte, ByRef CustomOpCodes() As UInteger) As Boolean
		Dim blnreturnValue As Boolean
		Dim canMsg As New TPCANMsg
		Dim blnretry As Boolean
		Dim blnMessageAvailable As Boolean
		Dim strErrorMessage As String

		Try
			blnreturnValue = False
			blnretry = True
			blnMessageAvailable = False
			strErrorMessage = ""

			'Consume any previous response.
			For Each eachOpCode As UInteger In CustomOpCodes
				GetToolMsgResp(eachOpCode, canMsg)
			Next

			Do
				'Consume any previous response.
				'GetToolMsgResp(canMessage.OpCode, canMsg)

				'Load Output Queue with Message.
				mCanMsgHandler.LoadCANMsgOutputQueue(canMessage)

				'Wait for response or timeout
				For Each eachOpCode As UInteger In CustomOpCodes
					blnMessageAvailable = WaitForMessageAvailable(eachOpCode)
					If blnMessageAvailable Then
						canMessage.OpCode = eachOpCode
						Exit For
					End If
				Next


				' Read the response.
				If (blnMessageAvailable AndAlso
			(GetToolMsgResp(canMessage.OpCode, canMsg) = True) AndAlso
			(canMsg.DATA.Length >= 7)) Then

					data = canMsg.DATA
					blnreturnValue = True
				End If

				If (blnreturnValue = False) Then
					' Retry one time, if necessary.
					If (blnretry = True) Then
						blnretry = False
					Else
						Exit Do
					End If
				End If
			Loop While (blnreturnValue = False)

			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function



	Private Function SendMessageGetResponse(canMessage As clsCANMessage, ByRef data() As Byte) As Boolean
		Dim blnreturnValue As Boolean
		Dim canMsg As New TPCANMsg
		Dim blnretry As Boolean
		Dim blnMessageAvailable As Boolean
		Dim strErrorMessage As String

		Try
			blnreturnValue = False
			blnretry = True
			blnMessageAvailable = False
			strErrorMessage = ""

			'Consume any previous response.
			GetToolMsgResp(canMessage.OpCode, canMsg)
			Do
				'Consume any previous response.
				'GetToolMsgResp(canMessage.OpCode, canMsg)

				'Load Output Queue with Message.
				mCanMsgHandler.LoadCANMsgOutputQueue(canMessage)

				'Wait for response or timeout
				blnMessageAvailable = WaitForMessageAvailable(canMessage.OpCode)

				' Read the response.
				If (blnMessageAvailable AndAlso
			(GetToolMsgResp(canMessage.OpCode, canMsg) = True) AndAlso
			(canMsg.DATA.Length >= 7)) Then

					data = canMsg.DATA
					blnreturnValue = True
				End If

				If (blnreturnValue = False) Then
					' Retry one time, if necessary.
					If (blnretry = True) Then
						blnretry = False
					Else
						Exit Do
					End If
				End If
			Loop While (blnreturnValue = False)

			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
		End Try
	End Function

	Private Sub AddressClaimRcvd(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)

		' An address claim message has been received.
		' Store one message for each responding actuator.
		Dim rcvdCanMsg As TPCANMsg = MsgStruct.CanMsg

		Try
			SyncLock mAddrClaimLock
				If (mAddressClaimMsgs.ContainsKey(CByte(rcvdCanMsg.ID And &HFF).ToString) <> True) Then
					' This is a message with a new source address so store it.
					mAddressClaimMsgs.TryAdd(CByte(rcvdCanMsg.ID And &HFF).ToString, rcvdCanMsg)
					' Get new actuator count.
					mActuatorCount = mAddressClaimMsgs.Count
				End If
			End SyncLock

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		Finally
		End Try
	End Sub

	Private Sub StatusMsgRcvd(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)

		' A status message has been received.
		' Store one message for each responding actuator.
		Dim rcvdCanMsg As TPCANMsg = MsgStruct.CanMsg 'New Message
		Dim msg_str As New TPCANMsg 'Dummy to hold messages for TryRemove
		Dim strKey As String

		Try
			'Key is SourceID
			strKey = CByte(rcvdCanMsg.ID And &HFF).ToString

			SyncLock mStatusMsgLock
				If (mStatusMsgs.ContainsKey(strKey) = True) Then
					' Remove the message previously received from this actuator.
					mStatusMsgs.TryRemove(strKey, msg_str)
				End If
				' Store the latest message.
				rcvdCanMsg = MsgStruct.CanMsg
				mStatusMsgs.TryAdd(strKey, rcvdCanMsg)
			End SyncLock

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		Finally
		End Try
	End Sub

	Private Sub ToolMsgRespRcvd(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)

		Dim rcvdCanMsg As TPCANMsg = MsgStruct.CanMsg 'New Message
		Dim readMsgStruct As clsCanMsgHandling.CAN_Msg_Struct 'Previous Message of Same OpCode
		Dim msgOpcode As UShort
		Dim key As String = ""
		Dim msg_str As New clsCanMsgHandling.CAN_Msg_Struct 'Dummy to hold messages for TryRemove
		'Dim DefaultBootloaderAttnOpcode As UShort
		'Dim strCANMessageLog As String
		Dim intResponse As Integer
		Try
			'this is now defined once when the csv file is read. gDefaultBootloaderAttnOpcode RPW
			'DefaultBootloaderAttnOpcode = gCANActuator.CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest).OpCode
			'DefaultBootloaderAttnOpcode = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest).OpCode

			' Store received messages of interest, all others are ignored
			If (mblnApplicationCodeRunning = True) Then
				'If ((rcvdCanMsg.ID >= &H38FF0D00) AndAlso (rcvdCanMsg.ID <= &H38FF0DFF) AndAlso _
				'If ((rcvdCanMsg.ID = (TOOL_MSG_RESPONSE + mActuatorAddress)) AndAlso1
				intResponse = rcvdCanMsg.ID And SRC_ADDR_MASK

				If ((intResponse = TOOL_MSG_RESPONSE Or intResponse = UDS_MSG_RESPONSE) AndAlso (rcvdCanMsg.DATA.Length > 1)) Then
					msgOpcode = rcvdCanMsg.DATA(0)
					msgOpcode += CUShort(rcvdCanMsg.DATA(1)) << 8
					SyncLock mToolResponseLock
						'Select Case msgOpcode
						'  Case OPCODE.SW_VERSION_ID, _
						'       OPCODE.APPLICATION_CHKSUM_CALC, _
						'       OPCODE.SERIAL_NUMBER, _
						'       OPCODE.HW_PART_NUM_MS, _
						'       OPCODE.HW_PART_NUM_LS, _
						'       OPCODE.SW_PART_NUM_MS, _
						'       OPCODE.SW_PART_NUM_LS, _
						'       OPCODE.INTERNAL_TEMP, _
						'       OPCODE.BATTERY_VOLTAGE, _
						'       OPCODE.MICRO_OSC_FREQ, _
						'       OPCODE.MICRO_PLL_SELECT

						key = msgOpcode.ToString
						If (mToolResponseMsgs.ContainsKey(key)) Then
							' This is not the first message of its type, so increment the message count.
							readMsgStruct = mToolResponseMsgs.Item(key)
							' Remove previous message.
							mToolResponseMsgs.TryRemove(key, msg_str)
							' Increment and store message count.
							MsgStruct.msgCount = readMsgStruct.msgCount + 1
						Else
							MsgStruct.msgCount = 1
						End If
						' Store latest message.
						mToolResponseMsgs.TryAdd(key, MsgStruct)
						'Log CAN Messages if enabled
						'strCANMessageLog = "Tool Message Response Received, MessageID =, " & Hex(rcvdCanMsg.ID) & ", OpCode =, " & rcvdCanMsg.DATA(1).ToString("X2") & rcvdCanMsg.DATA(0).ToString("X2")
						'Call mCanMsgHandler.LogCanMessage(strCANMessageLog)
						'Call mCanMsgHandler.LogCanMessage()
						'strCANMessageLog = "Tool Message Response Received, MessageID =, " & Hex(rcvdCanMsg.ID) & ", OpCode =, " & rcvdCanMsg.DATA(1).ToString("X2") & rcvdCanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", rcvdCanMsg.DATA)
						'mCanMsgHandler.LoadCANMsgLogQueue(strCANMessageLog)

						'End Select
					End SyncLock
				End If

			Else
				If ((rcvdCanMsg.ID = (TOOL_MSG_RESPONSE + DFLT_BOOTLDR_ADDR)) AndAlso
			(rcvdCanMsg.DATA.Length > 0)) Then
					msgOpcode = rcvdCanMsg.DATA(0)
					SyncLock mDfltBtldrToolRespLock
						'Select Case msgOpcode
						'  Case OPCODE.DFLT_BOOTLDR_ATTN, _
						'       OPCODE.DFLT_BOOTLDR_INIT_WRITE, _
						'       OPCODE.DFLT_BOOTLDR_MEM_WRITE, _
						'       OPCODE.DFLT_BOOTLDR_INIT_READ, _
						'       OPCODE.DFLT_BOOTLDR_MEM_READ, _
						'       OPCODE.DFLT_BOOTLDR_CALC_CHKSUM, _
						'       OPCODE.DFLT_BOOTLDR_SERIAL_NUMBER, _
						'       OPCODE.DFLT_BOOTLDR_CLOSE_SESSION, _
						'       OPCODE.DFLT_BOOTLDR_START_APP, _
						'       OPCODE.DFLT_BOOTLDR_RESTART, _
						'       OPCODE.DFLT_BOOTLDR_FAULT

						key = msgOpcode.ToString
						If (mDfltBtldrToolRespMsgs.ContainsKey(key)) Then
							' This is not the first message of its type, so increment the message count.
							readMsgStruct = mDfltBtldrToolRespMsgs.Item(key)
							' Remove previous message.
							mDfltBtldrToolRespMsgs.TryRemove(key, msg_str)
							' Increment and store message count.
							MsgStruct.msgCount = readMsgStruct.msgCount + 1
						Else
							MsgStruct.msgCount = 1
						End If
						' Store latest message.
						mDfltBtldrToolRespMsgs.TryAdd(key, MsgStruct)
						If ((msgOpcode = gDefaultBootloaderAttnOpcode) AndAlso (rcvdCanMsg.LEN = 7)) Then
							mblnDefaultBootloaderCodeRunning = True
							mstrDefaultBootloaderVersion = Mid(System.Text.Encoding.ASCII.GetString(rcvdCanMsg.DATA), 2)
						End If
						'Log CAN Messages if enabled
						'strCANMessageLog = "Tool Message Response Received, MessageID=, " & Hex(rcvdCanMsg.ID) & ", OpCode=, " & rcvdCanMsg.DATA(0).ToString("X2")
						'Call mCanMsgHandler.LogCanMessage(strCANMessageLog)
						'Call mCanMsgHandler.LogCanMessage()
						'strCANMessageLog = "DBL Tool Message Response Received, MessageID=, " & Hex(rcvdCanMsg.ID) & ", OpCode=, " & rcvdCanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", rcvdCanMsg.DATA)
						'mCanMsgHandler.LoadCANMsgLogQueue(strCANMessageLog)

						'End Select
					End SyncLock
				End If
			End If

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		Finally
		End Try
	End Sub

	Private Function WaitForMessageAvailable(ByVal OpCode As UInteger) As Boolean

		Dim returnValue As Boolean = False
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double
		Dim lElapsed As New clsStopWatch



		Try
			' Initialize the elapsed time starting value
			lElapsed.Start()
			'QueryPerformanceCounter(HP_CounterValue)
			'dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
			' Wait up to one second.

			Do
				If (GetToolMsgRespCount(OpCode) > 0) Then
					' Received message count > 0
					returnValue = True
					Exit Do
				End If
				' Retrieve the current counter value.
				'QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				'dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				'Loop While (dblCurrentTime - dblStartTime < 1000)
				System.Windows.Forms.Application.DoEvents()
				Threading.Thread.Sleep(1)
			Loop While (lElapsed.ElapsedMilliseconds < 3000)

			Return returnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	''Public Sub WaveFormToCANWrite(ByVal FrameRate As Double, ByVal SignalY() As Double, ByRef CANWrite() As nican.NCTYPE_CAN_STRUCT, ByVal arbitrationIDCommand As Integer, ByVal strMovementType As String)
	Private Sub WaveFormToCANWrite(ByVal FrameRate As Double,
								 ByVal SignalY() As Double,
								 ByRef CANWrite() As nican.NCTYPE_CAN_STRUCT,
								 ByVal canMessage As clsCANMessage,
								 ByVal intPositionStartByte As Integer)
		Dim i As Integer
		Dim LowPosition As Byte
		Dim HighPosition As Byte
		Dim Ydata As Integer
		Dim position As Double
		'Dim Data(7) As Byte
		Dim intByte As Byte
		Try

			ReDim CANWrite(SignalY.Length - 1)

			'Create a CAN Frame for each array value
			For i = 0 To (SignalY.Length - 1)
				'size array
				CANWrite(i).sizedata()
				CANWrite(i).ArbitrationId = canMessage.SendArbitrationID Or nican.NC_FL_CAN_ARBID_XTD
				CANWrite(i).DataLength = 8
				CANWrite(i).FrameType = 0
				CANWrite(i).Timestamp.HighPart = 0
				'resolution is 100ns, Frame Rate is in ms
				CANWrite(i).Timestamp.LowPart = (CType((FrameRate * 10000), Integer)) * i
				'Scale data with CAN Resolution 
				Ydata = SignalY(i) '* 1 / DIP.CANCommandResolutionLSB
				position = Ydata
				LowPosition = position And (&HFF) 'Mask off Upper bytes
				HighPosition = (position And &HFF00) / &H100 'Mask off lower bytes

				'Requested Position
				CANWrite(i).Data = canMessage.Data.Clone
				CANWrite(i).Data(intPositionStartByte) = LowPosition
				CANWrite(i).Data(intPositionStartByte + 1) = HighPosition

				'If CANWrite(i).Data(DIP.CANCommandPositionByte) = 0 And
				'   CANWrite(i).Data(DIP.CANCommandPositionByte + 1) = 0 And 
				'   strMovementType = "Position" Then
				'
				'  CANWrite(i).Data(DIP.CANCommandPositionByte) = 1
				'End If

				''Commanded State
				'If DIP.CANCommandModeByte <> -1 Then
				'  If strMovementType = "Position" Then
				'    CANWrite(i).Data(DIP.CANCommandModeByte) = DIP.CANCommandModePositionValue
				'  ElseIf strMovementType = "Hysteresis" Then
				'    CANWrite(i).Data(DIP.CANCommandModeByte) = DIP.CANCommandModeHysteresisTestValue
				'  End If
				'End If
			Next

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub


	Private Sub ActuatorDownloadThreadFunction()
		Try
			Do
				' Block until a CAN message is queued for transmit.
				mActuatorDownloadWaitHandle.WaitOne()
				If (mblnTerminateActuatorDownloadThread = True) Then
					' Allow this thread to terminate.
					Exit Do
				End If

				Call ActuatorDownload()

			Loop

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Private Sub StopActuatorDownloadThread()
		Try

			If ((mActuatorDownloadThread IsNot Nothing) AndAlso
		  (mActuatorDownloadThread.ThreadState <> ThreadState.AbortRequested) AndAlso
		  (mActuatorDownloadThread.ThreadState <> ThreadState.Aborted)) Then

				mblnTerminateActuatorDownloadThread = True
				mActuatorDownloadWaitHandle.Set()

				If mActuatorDownloadThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
					While (mActuatorDownloadThread.ThreadState <> System.Threading.ThreadState.Stopped)
						Application.DoEvents()
						Thread.Sleep(1)
					End While
				End If
				'XmtCanMessageThread.Abort()
				mActuatorDownloadThread = Nothing
			End If

			mblnTerminateActuatorDownloadThread = False

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Private Function ActuatorDownload() As Boolean

		Dim blnReturnValue As Boolean = False
		Dim dataRecord As IntelDataRecord
		Dim intRecord As Integer
		Dim idx1 As Byte
		Dim idx2 As Byte
		Dim data(7) As Byte
		Dim recBytesWritten As Byte
		Dim recBytesToBeWritten As Byte
		Dim canMsg As New TPCANMsg
		Dim blnDownloadAborted As Boolean
		Dim blnFirstPass As Boolean = False
		Dim intTotalBytesWritten As Integer
		Dim intLastTotalBytesWritten As Integer
		Dim ComErrorCount As Byte
		Dim EEPromDataChecksumPass() As Byte = {&HFF, 0, 0, 0, 0, 0, 0, 0}
		Dim PageRetryCount As Byte

		Dim strErrorMessage As String = ""
		Dim DfltBtldrInitMemWriteMsg As clsCANMessage
		Dim DfltBtldMemWriteDataMsg As clsCANMessage
		Dim strSupplementalDiagnosticText As String = ""
		Dim lStopWatch As New clsStopWatch
		Try

			DfltBtldrInitMemWriteMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryWriteRequest)
			DfltBtldMemWriteDataMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryWriteData)

			lStopWatch.Reset()
			lStopWatch.Start()
			Do

				'Application.DoEvents()
				'Thread.Sleep(1)

				ComErrorCount = 0
				intTotalBytesWritten = 0
				intLastTotalBytesWritten = 0
				blnFirstPass = blnFirstPass Xor True
				blnDownloadAborted = False

				' Clear default bootloader tool message response storage.
				SyncLock mDfltBtldrToolRespLock
					mDfltBtldrToolRespMsgs.Clear()
				End SyncLock

				intRecord = 0
				'While (i <= IntelHexFile.Count)
				While (intRecord <= mIntelHexFileDownload.Count)
					If (ComErrorCount > 10) Then
						If (blnFirstPass = False) Then
							'finalRslt = FinalResult.ExcessiveComFailures
							strErrorMessage = "Excessive Communication Failures"
						End If
						blnDownloadAborted = True
						Exit While
					End If
					If (intRecord = 0) Then
						' Writing to eeprom memory to erase application checksum valid byte.
						dataRecord = New IntelDataRecord
						dataRecord.StartAddress = &H7FF
						dataRecord.EndAddress = &H7FF
						dataRecord.DataLength = 1
						dataRecord.Data = EEPromDataChecksumPass
						' Setup an initiate memory write CAN message for this data record.
						data = DfltBtldrInitMemWriteMsg.Data
						' Load the CAN message data bytes.
						data(1) = TargetMemoryAreaEnum.EEProm
						data(2) = CByte(dataRecord.StartAddress And &HFF)
						data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
						data(4) = CByte(dataRecord.EndAddress And &HFF)
						data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
						DfltBtldrInitMemWriteMsg.Data = data
					Else
						' Initiate next write to flash memory.
						dataRecord = mIntelHexFileDownload(intRecord - 1)
						' Setup an initiate memory write CAN message for this data record.
						data = DfltBtldrInitMemWriteMsg.Data
						' Writing to program memory.
						data(1) = TargetMemoryAreaEnum.ProgramMemory
						' Load the CAN message data bytes.
						data(2) = CByte(dataRecord.StartAddress And &HFF)
						data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
						data(4) = CByte(dataRecord.EndAddress And &HFF)
						data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
						DfltBtldrInitMemWriteMsg.Data = data
					End If

					' Send the initiate memory write CAN message.
					mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrInitMemWriteMsg)
					'Call mCanMsgHandler.LogCanActuatorActivity("Sent Init Mem Write, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

					' Look for an acknowledgement (ACK) CAN message from the
					' actuator specifying the number of bytes to be written.
					If (GetMemoryWriteInitAck(canMsg) <> True) Then
						' The actuator returned an error message in response to an initiate
						' memory write command or failed to respond to an initiate memory
						' write command while downloading application.
						ComErrorCount += 1
						PageRetryCount += 1
						If (CloseSession() = False) Then
							' Failed to close DBL programming session prior to re-writing page.
							'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
							strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
							blnDownloadAborted = True
							Exit While
						Else
							' Start again at the beginning of the current page.
							strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Error."
							If (intTotalBytesWritten <= 256) Then
								intRecord = 0
							ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
								intRecord = (intTotalBytesWritten \ 256) - 1
							Else
								intRecord = intTotalBytesWritten \ 256
							End If
							intTotalBytesWritten = intRecord * 256
							intRecord = intTotalBytesWritten \ 16
							intLastTotalBytesWritten = intTotalBytesWritten
							'_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
							Continue While
						End If
					Else
						'Call mCanMsgHandler.LogCanActuatorActivity("Received Init Mem Write Ack, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
						If (dataRecord.DataLength <> (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
							' The number of bytes specified in the ACK message does
							' not match the number of bytes in the data record.
							ComErrorCount += 1
							PageRetryCount += 1
							If (CloseSession() = False) Then
								' Failed to close DBL programming session prior to re-writing page.
								'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
								strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
								blnDownloadAborted = True
								Exit While
							Else
								' Start again at the beginning of the current page.
								strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Byte Count."
								If (intTotalBytesWritten <= 256) Then
									intRecord = 0
								ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
									intRecord = (intTotalBytesWritten \ 256) - 1
								Else
									intRecord = intTotalBytesWritten \ 256
								End If
								intTotalBytesWritten = intRecord * 256
								intRecord = intTotalBytesWritten \ 16
								intLastTotalBytesWritten = intTotalBytesWritten
								'_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
								Continue While
							End If
						End If
					End If

					' Setup a memory write data CAN message for this data record.
					recBytesWritten = 0
					data = DfltBtldMemWriteDataMsg.Data

					'Console.WriteLine("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount)
					mintActuatorDownloadPercentComplete = CInt(intRecord / mIntelHexFileDownload.Count * 100)
					'Call mCanMsgHandler.LogCanActuatorActivity("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

					Do
						' Setup write byte count. A maximum of 7 bytes can be written in one message.
						If ((dataRecord.DataLength - recBytesWritten) <= 7) Then
							recBytesToBeWritten = dataRecord.DataLength - recBytesWritten
						Else
							recBytesToBeWritten = 7
						End If
						' Load the CAN message data bytes.
						' Message payload begins at data(1).
						idx1 = 1
						' Assumes that bytesToBeWritten will never be 0.
						For idx2 = recBytesWritten To (recBytesWritten + recBytesToBeWritten - 1)
							data(idx1) = dataRecord.Data(idx2)
							idx1 += 1
						Next
						DfltBtldMemWriteDataMsg.Data = data
						' Set the CAN message data length (data byte count + 1 byte opcode).
						DfltBtldMemWriteDataMsg.DataLength = recBytesToBeWritten + 1
						' Send the memory write data CAN message.
						mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldMemWriteDataMsg)
						'Call mCanMsgHandler.LogCanActuatorActivity("Sent Mem Write Data, Record, " & intRecord & ", Bytes, " & recBytesToBeWritten & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

						'Console.WriteLine("Writing Record " & intRecord & ": Data= " & String.Join(",", data))

						' Look for an acknowledgement (ACK) CAN message from the actuator
						' containing a value that represents the number of bytes remaining to be written.
						If (GetMemoryWriteDataAck(canMsg) <> True) Then
							' The actuator returned an error message in response to a
							' memory write data command or failed to respond to a
							' memory write data command while downloading application.
							ComErrorCount += 1
							PageRetryCount += 1
							If (CloseSession() = False) Then
								' Failed to close DBL programming session prior to re-writing page.
								'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
								strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
								blnDownloadAborted = True
								Exit While
							Else
								' Start again at the beginning of the current page.
								strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Error."
								If (intTotalBytesWritten <= 256) Then
									intRecord = 0
								ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
									intRecord = (intTotalBytesWritten \ 256) - 1
								Else
									intRecord = intTotalBytesWritten \ 256
								End If
								intTotalBytesWritten = intRecord * 256
								intRecord = intTotalBytesWritten \ 16
								intLastTotalBytesWritten = intTotalBytesWritten
								'_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
								Continue While
							End If
						Else
							'Call mCanMsgHandler.LogCanActuatorActivity("Received Mem Write Data Ack, Record, " & intRecord & ", BytesRemaining, " & (canMsg.DATA(1) + (canMsg.DATA(2) << 8)) & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
							If ((dataRecord.DataLength - (recBytesWritten + recBytesToBeWritten)) <>
				  (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
								' The number of bytes remaining value contained in the ACK message does
								' not match the number of remaining bytes calculated based on the length
								' of this data record and the number of bytes written so far.
								ComErrorCount += 1
								PageRetryCount += 1
								If (CloseSession() = False) Then
									' Failed to close DBL programming session prior to re-writing page.
									'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
									strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
									blnDownloadAborted = True
									Exit While
								Else
									' Start again at the beginning of the current page.
									strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Byte Count."
									If (intTotalBytesWritten <= 256) Then
										intRecord = 0
									ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
										intRecord = (intTotalBytesWritten \ 256) - 1
									Else
										intRecord = intTotalBytesWritten \ 256
									End If
									intTotalBytesWritten = intRecord * 256
									intRecord = intTotalBytesWritten \ 16
									intLastTotalBytesWritten = intTotalBytesWritten
									'_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
									Continue While
								End If
							End If
						End If
						' Update the bytesWritten count.
						recBytesWritten += recBytesToBeWritten
						'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Bytes To Be Written, " & recBytesToBeWritten)
						'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
						'Call mCanMsgHandler.LogCanActuatorActivity("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
					Loop While (recBytesWritten < dataRecord.DataLength)

					If (intRecord <> 0) Then
						intTotalBytesWritten += recBytesWritten
					End If
					If ((intTotalBytesWritten - intLastTotalBytesWritten) >= 100) Then
						'ReportProgress(((intTotalBytesWritten * 100) / _fileSize) + 100)
						intLastTotalBytesWritten = intTotalBytesWritten
					End If
					intRecord += 1
				End While

				' Delay briefly before sending the next command.
				Delay(25)
				SyncLock mDfltBtldrToolRespLock
					mDfltBtldrToolRespMsgs.Clear()
				End SyncLock
				' Close the default bootloader programming session.
				If (CloseSession() = False) Then ' two tries built in to CloseSession
					' Failed to receive close session ack msg in three attempts.? actually two, TER
					If (blnDownloadAborted = False) Then
						' The download was otherwise successful so try starting application code.
						If StartApplication() Then
							blnReturnValue = True
							mbytDefaultBootloaderFault = 0
							Exit Do
						Else
							blnDownloadAborted = True
							strSupplementalDiagnosticText = "ActuatorDownload() - Failed To Close Program Session."
						End If
					ElseIf (blnFirstPass <> True) Then
						' Failed to close DBL programming session at end of download attempt.
						'finalRslt = FinalResult.ClosePrgmSessionFailed
						strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
						Exit Do
					End If
				End If

				If (blnDownloadAborted = False) Then
					' This was a clean download.
					blnReturnValue = True
					Exit Do
				ElseIf (blnFirstPass = True) Then
					' The first download failed so attempt to download one more time.
					' Delay briefly before sending the next command.
					Delay(25)
					' Clear default bootloader tool message response storage.
					SyncLock mDfltBtldrToolRespLock
						mDfltBtldrToolRespMsgs.Clear()
					End SyncLock
					'_dfltBootldrResponded = False
					mblnDefaultBootloaderCodeRunning = False
					mblnApplicationCodeRunning = False
					' Send a default bootloader attention request message.
					Dim attnMsg = mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest)
					mCanMsgHandler.LoadCANMsgOutputQueue(attnMsg)
					Delay(25)
					If (mblnDefaultBootloaderCodeRunning <> True) Then
						' Failed to detect a DBL attention request response. The download has failed.
						'finalRslt = FinalResult.NoDblResponseDuringRetry
						strErrorMessage = "Failed to detect Default Bootloader Attention Request"
						Exit Do
					End If
					' We're here because the DBL responded, so attempt to download one more time.
				End If
				' Loop one additional time if an error causes the download to abort.
				' If 2nd attempt fails then fall through and exit.
			Loop While ((blnDownloadAborted = True) AndAlso (blnFirstPass = True))

			If blnDownloadAborted Then
				If strSupplementalDiagnosticText <> "" Then

					strSupplementalDiagnosticText = "Page Retries = " &
										  PageRetryCount.ToString() & ", " &
										  strSupplementalDiagnosticText
				End If
				strErrorMessage = strErrorMessage & strSupplementalDiagnosticText
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
			lStopWatch.Stop()
			mblnActuatorDownloadComplete = True
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				mCanMsgHandler.Anomaly = Nothing
			End If
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
			End If
		End Try

	End Function


#End Region

#Region "Public Methods ======================================================================="


	Public Sub PrepareBitsForStation(setOnBits As Byte())
		Dim N = setOnBits.Length
		Dim msgData()() As Integer = New Integer(N - 1)() {}
		For i As Integer = 0 To N - 1
			msgData(i) = New Integer() {setOnBits(i)}
		Next

		Dim returnStr As String = ""
		Dim flag = ResetSpecialMemoryLine1(returnStr)
		flag = ResetSpecialMemoryLine2(returnStr)
		For i As Integer = 0 To msgData.GetUpperBound(0)
			flag = WriteSpecialMemoryLine1(msgData(i), returnStr)
			flag = WriteSpecialMemoryLine2(msgData(i), returnStr)
		Next
	End Sub
	Public Function ActuatorPresentOnBus() As Boolean
		Dim blnActuatorPresent As Boolean
		Try

			'Clear Previous Status Messages
			SyncLock mStatusMsgLock
				mStatusMsgs.Clear()
			End SyncLock

			'Register for status messages call-back
			mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Dim Count As Int16 = 0
			Do
				Delay(20)
				Count += 1

				'Console.WriteLine(StatusMsgs.Count)

				If mStatusMsgs.Count >= 1 Then
					'Actuator is present since status message was found for it
					blnActuatorPresent = True
				Else
					blnActuatorPresent = False
				End If
			Loop Until blnActuatorPresent Or Count > 100

			Return blnActuatorPresent

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)

		End Try
	End Function

	Public Function ActuatorAbsentOnBus() As Boolean
		Dim blnActuatorAbsent As Boolean
		Try
			'Clear Previous Status Messages
			SyncLock mStatusMsgLock
				mStatusMsgs.Clear()
			End SyncLock
			'Register for status messages call-back
			mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Dim Count As Int16 = 0
			Do
				Delay(20)
				Count += 1

				'Console.WriteLine(StatusMsgs.Count)

				If mStatusMsgs.Count >= 1 Then
					'Actuator is present since status message was found for it
					blnActuatorAbsent = False
					'Clear Status Messages
					SyncLock mStatusMsgLock
						mStatusMsgs.Clear()
					End SyncLock
				Else
					blnActuatorAbsent = True
				End If
			Loop Until blnActuatorAbsent Or Count > 100

			Return blnActuatorAbsent

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)

		End Try
	End Function

	Public Function APSCalibrate(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.APSCalibrate, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error in APS Calibrate CAN Message"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function CumminsClearStops() As Boolean
		Dim blnreturnValue As Boolean = False
		Dim CanMessage As clsCANMessage
		Dim strErrorMessage As String
		Dim returnData(0) As Byte

		Try

			blnreturnValue = False
			strErrorMessage = ""

			'Put In Override State
			Call CumminsCommandOverride()
			'Call WriteOverrideCommand(returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Call Delay(100)

			'Find CAN Message in collection
			CanMessage = mCANMessages(clsCANMessage.MessageFunctions.CumminsClearStops)

			'CanMessage.SendArbitrationID = CTT_CMD_MSG_PRIORITY + CTT_CMD_MSG_PGN + &H0      XXX KTK

			'Send Message and Get Response
			blnreturnValue = SendMessageNoResponse(CanMessage)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnreturnValue = False Then
				strErrorMessage = "Error Clearing Stops"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function CumminsCommandOverride() As Boolean
		Dim blnreturnValue As Boolean = False
		Dim CanMessage As clsCANMessage
		Dim strErrorMessage As String
		Dim returnData(0) As Byte
		Try
			blnreturnValue = False
			strErrorMessage = ""

			'Find CAN Message in collection
			CanMessage = mCANMessages(clsCANMessage.MessageFunctions.CumminsCommandOverride)

			'CanMessage.SendArbitrationID = CTT_CMD_MSG_PRIORITY + CTT_CMD_MSG_PGN + &H0      XXX KTK

			'Send Message and Get Response
			blnreturnValue = SendMessageNoResponse(CanMessage)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnreturnValue = False Then
				strErrorMessage = "Error Commanding Override"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function CumminsCommandPosition(ByVal dynamicData() As Integer) As Boolean
		Dim blnreturnValue As Boolean = False
		Dim CanMessage As clsCANMessage
		Dim strErrorMessage As String
		Dim returnData(0) As Byte
		Dim intDynamicWriteByteIndex As Integer
		Dim intDataIndex As Integer
		Try
			blnreturnValue = False
			strErrorMessage = ""

			'Find CAN Message in collection
			CanMessage = mCANMessages(clsCANMessage.MessageFunctions.CumminsCommandPosition)

			'CanMessage.SendArbitrationID = CTT_CMD_MSG_PRIORITY + CTT_CMD_MSG_PGN + &H0      XXX KTK

			intDynamicWriteByteIndex = 0
			For intDataIndex = 0 To CanMessage.StaticData.Length - 1
				If CanMessage.StaticData(intDataIndex).ToUpper = "XX" Then
					CanMessage.Data(intDataIndex) = dynamicData(intDynamicWriteByteIndex)
					intDynamicWriteByteIndex += 1
				End If
			Next

			'Send Message and Get Response
			blnreturnValue = SendMessageNoResponse(CanMessage)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnreturnValue = False Then
				strErrorMessage = "Error Commaning Position"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function CumminsCommandPositionStepWaveform(ByVal FrameRateMS As Integer,
													 ByVal LowPosition As Double,
													 ByVal HighPosition As Double,
													 ByVal LSBPercent As Double) As Boolean
		Dim blnreturnValue As Boolean = False
		Dim CanMessage As clsCANMessage
		Dim strErrorMessage As String
		'Dim returnData(0) As Byte
		'Dim intDynamicWriteByteIndex As Integer
		Dim intDataIndex As Integer
		Dim intPositionStartByte As Integer
		Dim dblPositionArray(500) As Double
		Dim CANWrite(500) As nican.NCTYPE_CAN_STRUCT
		Dim intStatus As Integer

		Try
			blnreturnValue = False
			strErrorMessage = ""

			'Find CAN Message in collection
			CanMessage = mCANMessages(clsCANMessage.MessageFunctions.CumminsCommandPosition)

			'CanMessage.SendArbitrationID = CTT_CMD_MSG_PRIORITY + CTT_CMD_MSG_PGN + &H0      XXX KTK

			'intDynamicWriteByteIndex = 0
			For intDataIndex = 0 To CanMessage.StaticData.Length - 1
				If CanMessage.StaticData(intDataIndex).ToUpper = "XX" Then
					intPositionStartByte = intDataIndex
					Exit For
				End If
			Next

			'For i = 0 To 100
			For i As Integer = 0 To 249
				dblPositionArray(i) = LowPosition / LSBPercent '0.1
			Next
			'For i = 101 To 500
			For i As Integer = 250 To 500
				dblPositionArray(i) = HighPosition / LSBPercent '99.9
			Next

			Call WaveFormToCANWrite(FrameRateMS, dblPositionArray, CANWrite, CanMessage, intPositionStartByte)

			intStatus = mCanMsgHandler.WriteMultiple(500, CANWrite)
			If intStatus = 0 Then
				blnreturnValue = True
			End If
			''Send Message and Get Response
			'blnreturnValue = SendMessageNoResponse(CanMessage)
			'If mAnomaly IsNot Nothing Then
			'  Throw New TsopAnomalyException
			'End If

			If blnreturnValue = False Then
				strErrorMessage = "Error Commaning Position"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function CumminsCommandSelfCalibrate() As Boolean
		Dim blnreturnValue As Boolean = False
		Dim CanMessage As clsCANMessage
		Dim strErrorMessage As String
		Try
			blnreturnValue = False
			strErrorMessage = ""

			'Find CAN Message in collection
			CanMessage = mCANMessages(clsCANMessage.MessageFunctions.CumminsCommandSelfCalibrate)

			'CanMessage.SendArbitrationID = CTT_CMD_MSG_PRIORITY + CTT_CMD_MSG_PGN + &H0      XXX KTK

			'Send Message and Get Response
			blnreturnValue = SendMessageNoResponse(CanMessage)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnreturnValue = False Then
				strErrorMessage = "Error Commaning Self Calibrate"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Sub Delay(ByVal time As UInteger)
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double

		Try
			' Initialize the elapsed time starting value.
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
			Do
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				Threading.Thread.Sleep(Math.Min(time, 500)) 'allow other things to happen for max half a second (like receive CAN messages)
			Loop While (dblCurrentTime - dblStartTime < time)

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Public Function DisablePositionCommandTimeout(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.DisablePositionCommandTimeout, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Disabling Position Command Timeout"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function DisableZenCorrection(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.DisableZenCorrection, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Disabling ZEN Correction"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Sub DownloadIntelHexIntoEEProm()
		'Dim intPercentMarks() As Integer
		'Dim intNextMark As Integer
		Try

			'intPercentMarks = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110}
			'intNextMark = 0
			mblnActuatorDownloadComplete = False
			mintActuatorDownloadPercentComplete = 0

			' Unblock the Actuator Download thread.
			mActuatorDownloadWaitHandle.Set()

			'Do
			'  'Application.DoEvents()
			'  Thread.Sleep(0)
			'  'If mintActuatorDownloadPercentComplete >= intPercentMarks(intNextMark) Then
			'  '  Console.WriteLine("Actuator Download " & mintActuatorDownloadPercentComplete & " Percent Complete")
			'  '  If intNextMark < intPercentMarks.Length - 1 Then
			'  '    intNextMark += 1
			'  '  End If
			'  'End If
			'Loop Until mblnActuatorDownloadComplete = True

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		Finally
		End Try
	End Sub

	'Public Function DownloadIntelHexIntoEEProm() As Boolean

	'  Dim blnReturnValue As Boolean = False
	'  Dim dataRecord As IntelDataRecord
	'  Dim intRecord As Integer
	'  Dim idx1 As Byte
	'  Dim idx2 As Byte
	'  Dim data(7) As Byte
	'  Dim recBytesWritten As Byte
	'  Dim recBytesToBeWritten As Byte
	'  Dim canMsg As New TPCANMsg
	'  Dim blnDownloadAborted As Boolean
	'  Dim blnFirstPass As Boolean = False
	'  Dim intTotalBytesWritten As Integer
	'  Dim intLastTotalBytesWritten As Integer
	'  Dim ComErrorCount As Byte
	'  Dim EEPromDataChecksumPass() As Byte = {&HFF, 0, 0, 0, 0, 0, 0, 0}
	'  Dim PageRetryCount As Byte

	'  Dim strErrorMessage As String = ""
	'  Dim DfltBtldrInitMemWriteMsg As clsCANMessage
	'  Dim DfltBtldMemWriteDataMsg As clsCANMessage
	'  Dim strSupplementalDiagnosticText As String = ""
	'  Dim lStopWatch As New clsStopWatch
	'  Try

	'    DfltBtldrInitMemWriteMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryWriteRequest)
	'    DfltBtldMemWriteDataMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryWriteData)

	'    lStopWatch.Reset()
	'    lStopWatch.Start()
	'    Do

	'      'Application.DoEvents()
	'      'Thread.Sleep(1)

	'      ComErrorCount = 0
	'      intTotalBytesWritten = 0
	'      intLastTotalBytesWritten = 0
	'      blnFirstPass = blnFirstPass Xor True
	'      blnDownloadAborted = False

	'      ' Clear default bootloader tool message response storage.
	'      SyncLock mDfltBtldrToolRespLock
	'        mDfltBtldrToolRespMsgs.Clear()
	'      End SyncLock

	'      intRecord = 0
	'      'While (i <= IntelHexFile.Count)
	'      While (intRecord <= mIntelHexFileDownload.Count)
	'        If (ComErrorCount > 10) Then
	'          If (blnFirstPass = False) Then
	'            'finalRslt = FinalResult.ExcessiveComFailures
	'            strErrorMessage = "Excessive Communication Failures"
	'          End If
	'          blnDownloadAborted = True
	'          Exit While
	'        End If
	'        If (intRecord = 0) Then
	'          ' Writing to eeprom memory to erase application checksum valid byte.
	'          dataRecord = New IntelDataRecord
	'          dataRecord.StartAddress = &H7FF
	'          dataRecord.EndAddress = &H7FF
	'          dataRecord.DataLength = 1
	'          dataRecord.Data = EEPromDataChecksumPass
	'          ' Setup an initiate memory write CAN message for this data record.
	'          data = DfltBtldrInitMemWriteMsg.Data
	'          ' Load the CAN message data bytes.
	'          data(1) = TargetMemoryAreaEnum.EEProm
	'          data(2) = CByte(dataRecord.StartAddress And &HFF)
	'          data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
	'          data(4) = CByte(dataRecord.EndAddress And &HFF)
	'          data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
	'          DfltBtldrInitMemWriteMsg.Data = data
	'        Else
	'          ' Initiate next write to flash memory.
	'          dataRecord = mIntelHexFileDownload(intRecord - 1)
	'          ' Setup an initiate memory write CAN message for this data record.
	'          data = DfltBtldrInitMemWriteMsg.Data
	'          ' Writing to program memory.
	'          data(1) = TargetMemoryAreaEnum.ProgramMemory
	'          ' Load the CAN message data bytes.
	'          data(2) = CByte(dataRecord.StartAddress And &HFF)
	'          data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
	'          data(4) = CByte(dataRecord.EndAddress And &HFF)
	'          data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
	'          DfltBtldrInitMemWriteMsg.Data = data
	'        End If

	'        ' Send the initiate memory write CAN message.
	'        mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrInitMemWriteMsg)
	'        Call mCanMsgHandler.LogCanActuatorActivity("Sent Init Mem Write, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

	'        ' Look for an acknowledgement (ACK) CAN message from the
	'        ' actuator specifying the number of bytes to be written.
	'        If (GetMemoryWriteInitAck(canMsg) <> True) Then
	'          ' The actuator returned an error message in response to an initiate
	'          ' memory write command or failed to respond to an initiate memory
	'          ' write command while downloading application.
	'          ComErrorCount += 1
	'          PageRetryCount += 1
	'          If (CloseSession() = False) Then
	'            ' Failed to close DBL programming session prior to re-writing page.
	'            'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
	'            strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
	'            blnDownloadAborted = True
	'            Exit While
	'          Else
	'            ' Start again at the beginning of the current page.
	'            strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Error."
	'            If (intTotalBytesWritten <= 256) Then
	'              intRecord = 0
	'            ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
	'              intRecord = (intTotalBytesWritten \ 256) - 1
	'            Else
	'              intRecord = intTotalBytesWritten \ 256
	'            End If
	'            intTotalBytesWritten = intRecord * 256
	'            intRecord = intTotalBytesWritten \ 16
	'            intLastTotalBytesWritten = intTotalBytesWritten
	'            '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
	'            Continue While
	'          End If
	'        Else
	'          Call mCanMsgHandler.LogCanActuatorActivity("Received Init Mem Write Ack, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
	'          If (dataRecord.DataLength <> (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
	'            ' The number of bytes specified in the ACK message does
	'            ' not match the number of bytes in the data record.
	'            ComErrorCount += 1
	'            PageRetryCount += 1
	'            If (CloseSession() = False) Then
	'              ' Failed to close DBL programming session prior to re-writing page.
	'              'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
	'              strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
	'              blnDownloadAborted = True
	'              Exit While
	'            Else
	'              ' Start again at the beginning of the current page.
	'              strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Byte Count."
	'              If (intTotalBytesWritten <= 256) Then
	'                intRecord = 0
	'              ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
	'                intRecord = (intTotalBytesWritten \ 256) - 1
	'              Else
	'                intRecord = intTotalBytesWritten \ 256
	'              End If
	'              intTotalBytesWritten = intRecord * 256
	'              intRecord = intTotalBytesWritten \ 16
	'              intLastTotalBytesWritten = intTotalBytesWritten
	'              '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
	'              Continue While
	'            End If
	'          End If
	'        End If

	'        ' Setup a memory write data CAN message for this data record.
	'        recBytesWritten = 0
	'        data = DfltBtldMemWriteDataMsg.Data

	'        'Console.WriteLine("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount)
	'        'Call mCanMsgHandler.LogCanActuatorActivity("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

	'        Do
	'          ' Setup write byte count. A maximum of 7 bytes can be written in one message.
	'          If ((dataRecord.DataLength - recBytesWritten) <= 7) Then
	'            recBytesToBeWritten = dataRecord.DataLength - recBytesWritten
	'          Else
	'            recBytesToBeWritten = 7
	'          End If
	'          ' Load the CAN message data bytes.
	'          ' Message payload begins at data(1).
	'          idx1 = 1
	'          ' Assumes that bytesToBeWritten will never be 0.
	'          For idx2 = recBytesWritten To (recBytesWritten + recBytesToBeWritten - 1)
	'            data(idx1) = dataRecord.Data(idx2)
	'            idx1 += 1
	'          Next
	'          DfltBtldMemWriteDataMsg.Data = data
	'          ' Set the CAN message data length (data byte count + 1 byte opcode).
	'          DfltBtldMemWriteDataMsg.DataLength = recBytesToBeWritten + 1
	'          ' Send the memory write data CAN message.
	'          mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldMemWriteDataMsg)
	'          Call mCanMsgHandler.LogCanActuatorActivity("Sent Mem Write Data, Record, " & intRecord & ", Bytes, " & recBytesToBeWritten & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

	'          'Console.WriteLine("Writing Record " & intRecord & ": Data= " & String.Join(",", data))

	'          ' Look for an acknowledgement (ACK) CAN message from the actuator
	'          ' containing a value that represents the number of bytes remaining to be written.
	'          If (GetMemoryWriteDataAck(canMsg) <> True) Then
	'            ' The actuator returned an error message in response to a
	'            ' memory write data command or failed to respond to a
	'            ' memory write data command while downloading application.
	'            ComErrorCount += 1
	'            PageRetryCount += 1
	'            If (CloseSession() = False) Then
	'              ' Failed to close DBL programming session prior to re-writing page.
	'              'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
	'              strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
	'              blnDownloadAborted = True
	'              Exit While
	'            Else
	'              ' Start again at the beginning of the current page.
	'              strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Error."
	'              If (intTotalBytesWritten <= 256) Then
	'                intRecord = 0
	'              ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
	'                intRecord = (intTotalBytesWritten \ 256) - 1
	'              Else
	'                intRecord = intTotalBytesWritten \ 256
	'              End If
	'              intTotalBytesWritten = intRecord * 256
	'              intRecord = intTotalBytesWritten \ 16
	'              intLastTotalBytesWritten = intTotalBytesWritten
	'              '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
	'              Continue While
	'            End If
	'          Else
	'            Call mCanMsgHandler.LogCanActuatorActivity("Received Mem Write Data Ack, Record, " & intRecord & ", BytesRemaining, " & (canMsg.DATA(1) + (canMsg.DATA(2) << 8)) & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
	'            If ((dataRecord.DataLength - (recBytesWritten + recBytesToBeWritten)) <>
	'                (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
	'              ' The number of bytes remaining value contained in the ACK message does
	'              ' not match the number of remaining bytes calculated based on the length
	'              ' of this data record and the number of bytes written so far.
	'              ComErrorCount += 1
	'              PageRetryCount += 1
	'              If (CloseSession() = False) Then
	'                ' Failed to close DBL programming session prior to re-writing page.
	'                'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
	'                strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
	'                blnDownloadAborted = True
	'                Exit While
	'              Else
	'                ' Start again at the beginning of the current page.
	'                strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Byte Count."
	'                If (intTotalBytesWritten <= 256) Then
	'                  intRecord = 0
	'                ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
	'                  intRecord = (intTotalBytesWritten \ 256) - 1
	'                Else
	'                  intRecord = intTotalBytesWritten \ 256
	'                End If
	'                intTotalBytesWritten = intRecord * 256
	'                intRecord = intTotalBytesWritten \ 16
	'                intLastTotalBytesWritten = intTotalBytesWritten
	'                '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
	'                Continue While
	'              End If
	'            End If
	'          End If
	'          ' Update the bytesWritten count.
	'          recBytesWritten += recBytesToBeWritten
	'          'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Bytes To Be Written, " & recBytesToBeWritten)
	'          'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
	'          'Call mCanMsgHandler.LogCanActuatorActivity("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
	'        Loop While (recBytesWritten < dataRecord.DataLength)

	'        If (intRecord <> 0) Then
	'          intTotalBytesWritten += recBytesWritten
	'        End If
	'        If ((intTotalBytesWritten - intLastTotalBytesWritten) >= 100) Then
	'          'ReportProgress(((intTotalBytesWritten * 100) / _fileSize) + 100)
	'          intLastTotalBytesWritten = intTotalBytesWritten
	'        End If
	'        intRecord += 1
	'      End While

	'      ' Delay briefly before sending the next command.
	'      Delay(25)
	'      SyncLock mDfltBtldrToolRespLock
	'        mDfltBtldrToolRespMsgs.Clear()
	'      End SyncLock
	'      ' Close the default bootloader programming session.
	'      If (CloseSession() = False) Then ' two tries built in to CloseSession
	'        ' Failed to receive close session ack msg in three attempts.? actually two, TER
	'        If (blnDownloadAborted = False) Then
	'          ' The download was otherwise successful so try starting application code.
	'          If StartApplication() Then
	'            blnReturnValue = True
	'            mbytDefaultBootloaderFault = 0
	'            Exit Do
	'          Else
	'            blnDownloadAborted = True
	'            strSupplementalDiagnosticText = "ActuatorDownload() - Failed To Close Program Session."
	'          End If
	'        ElseIf (blnFirstPass <> True) Then
	'          ' Failed to close DBL programming session at end of download attempt.
	'          'finalRslt = FinalResult.ClosePrgmSessionFailed
	'          strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
	'          Exit Do
	'        End If
	'      End If

	'      If (blnDownloadAborted = False) Then
	'        ' This was a clean download.
	'        blnReturnValue = True
	'        Exit Do
	'      ElseIf (blnFirstPass = True) Then
	'        ' The first download failed so attempt to download one more time.
	'        ' Delay briefly before sending the next command.
	'        Delay(25)
	'        ' Clear default bootloader tool message response storage.
	'        SyncLock mDfltBtldrToolRespLock
	'          mDfltBtldrToolRespMsgs.Clear()
	'        End SyncLock
	'        '_dfltBootldrResponded = False
	'        mblnDefaultBootloaderCodeRunning = False
	'        mblnApplicationCodeRunning = False
	'        ' Send a default bootloader attention request message.
	'        mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest))
	'        Delay(25)
	'        If (mblnDefaultBootloaderCodeRunning <> True) Then
	'          ' Failed to detect a DBL attention request response. The download has failed.
	'          'finalRslt = FinalResult.NoDblResponseDuringRetry
	'          strErrorMessage = "Failed to detect Default Bootloader Attention Request"
	'          Exit Do
	'        End If
	'        ' We're here because the DBL responded, so attempt to download one more time.
	'      End If
	'      ' Loop one additional time if an error causes the download to abort.
	'      ' If 2nd attempt fails then fall through and exit.
	'    Loop While ((blnDownloadAborted = True) AndAlso (blnFirstPass = True))

	'    If blnDownloadAborted Then
	'      If strSupplementalDiagnosticText <> "" Then
	'        strSupplementalDiagnosticText = "Page Retries = " & PageRetryCount.ToString() & ", " & strSupplementalDiagnosticText
	'      End If
	'      strErrorMessage = strErrorMessage & strSupplementalDiagnosticText
	'      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
	'      Throw New TsopAnomalyException
	'    End If

	'    Return blnReturnValue

	'  Catch ex As TsopAnomalyException
	'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
	'    Return False
	'  Catch ex As Exception
	'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
	'    Return False
	'  Finally
	'    ' Unregister for status messages.
	'    mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
	'    lStopWatch.Stop()

	'  End Try

	'End Function

	'Public Function EnterBootloader() As Boolean
	'  Dim returnValue As Boolean = False
	'  Dim canMsg As TPCANMsg = New TPCANMsg
	'  Dim dblAttempts As Byte = 0
	'  Dim actuator_cnt As Integer
	'  Dim kv_pair As KeyValuePair(Of String, TPCANMsg)
	'  Dim strErrorMessage As String
	'  Try
	'    ' Verify that one actuator is present on the CAN bus.
	'    ' Register for status messages.
	'    mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
	'    If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
	'      Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
	'      mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
	'      Throw New TsopAnomalyException
	'    End If

	'    mblnDefaultBootloaderCodeRunning = False
	'    mblnApplicationCodeRunning = False
	'    ' Clear previously received status messages.
	'    SyncLock mStatusMsgLock
	'      mStatusMsgs.Clear()
	'    End SyncLock
	'    ' Wait for 100 milliseconds.
	'    Delay(100)
	'    SyncLock mStatusMsgLock
	'      actuator_cnt = mStatusMsgs.Count
	'      If (actuator_cnt = 1) Then
	'        ' Store the address of the active actuator.
	'        kv_pair = mStatusMsgs.First()
	'        mActuatorAddress = (kv_pair.Value.ID And &HFF).ToString
	'      End If
	'    End SyncLock
	'    If (actuator_cnt = 1) Then
	'      Do
	'        ' The bootloader has not responded so send a new
	'        ' jump to default bootloader command.
	'        Dim jumpMsg = mCANMessages(clsCANMessage.MessageFunctions.JumpToDefaultBootloader)
	'        mCanMsgHandler.LoadCANMsgOutputQueue(jumpMsg)
	'        ' Wait for 1250 milliseconds.
	'        Delay(1250)
	'        ' Clear default bootloader tool message response storage.
	'        SyncLock mDfltBtldrToolRespLock
	'          mDfltBtldrToolRespMsgs.Clear()
	'        End SyncLock
	'        ' Send default bootloader attention request message.
	'        Dim attnMsg = mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest)
	'        mCanMsgHandler.LoadCANMsgOutputQueue(attnMsg)
	'        Delay(50)
	'        If (mblnDefaultBootloaderCodeRunning = True) Then
	'          ' Success, the bootloader has responded so exit.
	'          returnValue = True
	'          Exit Do
	'        End If
	'        dblAttempts += 1
	'      Loop While (dblAttempts < 3)
	'    Else
	'      strErrorMessage = "No Status Message Received From Actuator."
	'      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
	'                                mStackTrace.CurrentFunctionName, strErrorMessage)
	'      Throw New TsopAnomalyException
	'    End If

	'    If mAnomaly IsNot Nothing Then
	'      Throw New TsopAnomalyException
	'    End If

	'    If mCanMsgHandler.Anomaly IsNot Nothing Then
	'      Me.Anomaly = mCanMsgHandler.Anomaly
	'      mCanMsgHandler.Anomaly = Nothing
	'      Throw New TsopAnomalyException
	'    End If

	'    If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
	'      Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
	'      mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
	'      Throw New TsopAnomalyException
	'    End If

	'    Return returnValue

	'  Catch ex As TsopAnomalyException
	'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
	'    Return False
	'  Catch ex As Exception
	'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
	'    Return False
	'  Finally
	'    ' Unregister for status messages.
	'    mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
	'  End Try
	'End Function


	Public Function EnableToolMessage() As Boolean
		If gCanActuator.TargetActuator = ActuatorVariantEnum.COVINGTON Then
			Dim enableToolMsg As clsCANMessage = New clsCANMessage()
			Dim returnData(0) As Byte
			enableToolMsg.MessageFunction = clsCANMessage.MessageFunctions.EnableToolMessagesG2

			Dim blnresult = SendMessageGetResponse(enableToolMsg, returnData)
			Return blnresult
		Else
			Return False
		End If
	End Function

	Public Function EnterBootloader() As Boolean
		Dim returnValue As Boolean = False
		Dim canMsg As TPCANMsg = New TPCANMsg
		Dim dblAttempts As Byte = 0
		Dim actuator_cnt As Integer
		Dim kv_pair As KeyValuePair(Of String, TPCANMsg)
		Dim strErrorMessage As String
		Try
			' Verify that one actuator is present on the CAN bus.
			' Register for status messages.
			mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			mblnDefaultBootloaderCodeRunning = False
			mblnApplicationCodeRunning = False
			' Clear previously received status messages.
			SyncLock mStatusMsgLock
				mStatusMsgs.Clear()
			End SyncLock
			' Wait for 100 milliseconds.
			Delay(100)
			SyncLock mStatusMsgLock
				actuator_cnt = mStatusMsgs.Count
				If (actuator_cnt = 1) Then
					' Store the address of the active actuator.
					kv_pair = mStatusMsgs.First()
					mActuatorAddress = (kv_pair.Value.ID And &HFF).ToString
				End If
			End SyncLock
			If (actuator_cnt = 1) Then
				If mTargetActuator = ActuatorVariantEnum.CATTHROTTLE Or mTargetActuator = ActuatorVariantEnum.CATFUEL Then
					Dim LoopCnt As Int16 = 0
					' Clear default bootloader tool message response storage.
					SyncLock mDfltBtldrToolRespLock
						mDfltBtldrToolRespMsgs.Clear()
					End SyncLock
					Do
						Dim LoopCnt2 As Int16 = 0
						Do
							' Send default bootloader attention request message.
							mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest))
							Delay(100)
							LoopCnt2 += 1
						Loop While LoopCnt2 < 4 And mblnDefaultBootloaderCodeRunning <> True
						If mblnDefaultBootloaderCodeRunning <> True Then
							' jump to default bootloader command.
							mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.JumpToDefaultBootloader))
							'Call SetSupplyVoltage(0)
							'Delay(400)
							'Call SetSupplyVoltage(gElectricalOperationAndLoad.TestVoltage)
							'Delay(400)
						End If
						LoopCnt += 1
					Loop While mblnDefaultBootloaderCodeRunning <> True And LoopCnt <= 10

					If (mblnDefaultBootloaderCodeRunning = True) Then
						' Success, the bootloader has responded so exit.
						returnValue = True
					End If
				Else

					Do
						' The bootloader has not responded so send a new
						' jump to default bootloader command.
						mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.JumpToDefaultBootloader))
						' Wait for 1250 milliseconds.
						Delay(1250)
						' Clear default bootloader tool message response storage.
						SyncLock mDfltBtldrToolRespLock
							mDfltBtldrToolRespMsgs.Clear()
						End SyncLock
						' Send default bootloader attention request message.
						mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest))
						Delay(50)
						If (mblnDefaultBootloaderCodeRunning = True) Then
							' Success, the bootloader has responded so exit.
							returnValue = True
							Exit Do
						End If
						dblAttempts += 1
					Loop While (dblAttempts < 3)
				End If
			Else
				strErrorMessage = "No Status Message Received From Actuator."
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			'Return returnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			'Return False
			returnValue = False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			'Return False
			returnValue = False
		Finally
			If (mblnDefaultBootloaderCodeRunning = True) Then
				' Success, the bootloader has responded so exit.
				returnValue = True
			End If
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
		End Try
		Return returnValue
	End Function

	Public Function EnterTestMode(ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		Try

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If

			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.StartTestMode, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Writing EnterTestMode Command"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function
	Public Function EnableMotorTestMode(ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		Try

			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableMotorTestMode, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Writing EnableMotorTestMode Command"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function
	Public Function ExitTestMode(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.StopTestMode, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Writing ExitTestMode Command"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ModifySourceID(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ModifySourceID, dynamicData, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error in Modify SourceID"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function PhaseStepMessage1(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.PhaseStepMessage1, dynamicData, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error in Phase Step Message 1"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function PhaseStepMessage2(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.PhaseStepMessage2, dynamicData, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error in Phase Step Message 2"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function ReadApplicationSoftwareVersion(ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		Try

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes

				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSoftwareVersion,
									  ReturnStrFormat.MixedDotDelimited, returnString)
			Else
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSoftwareVersion,
									  ReturnStrFormat.AsciiDotDelimited, returnString)
			End If

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading Application Software Version"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	'no longer needed, not used
	'Public Function ReadApplicationSoftwareVersion(ByRef returnString As String,
	'                                               ByVal softwareGeneration As String) As Boolean

	'  Dim blnResult As Boolean = False

	'  Try
	'    If gCanActuator.mTargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON Then
	'      'not yet working for Covington
	'      returnString = "NotWorking"
	'      Return True
	'    End If
	'    Select Case softwareGeneration
	'      Case "1"
	'        blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSoftwareVersion,
	'                                    ReturnStrFormat.AsciiDotDelimited, returnString)
	'      Case "2"
	'        blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSoftwareVersionG2,
	'                                    ReturnStrFormat.AsciiDotDelimited, returnString)
	'    End Select

	'    If mAnomaly IsNot Nothing Then
	'      Throw New TsopAnomalyException
	'    End If

	'    If Not blnResult Then
	'      Dim strErrorMessage = "Error Reading Application Software Version"
	'      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
	'                                mStackTrace.CurrentFunctionName, strErrorMessage)
	'      Throw New TsopAnomalyException
	'    End If

	'    Return blnResult

	'  Catch ex As TsopAnomalyException
	'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
	'    Return False
	'  Catch ex As Exception
	'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
	'    Return False
	'  End Try
	'End Function

	Public Function ReadAPSCalibrateProgress(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadAPSCalibrateProgress,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading APS Calibration Progress"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadAPSCountValue(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadAPSCountValue,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading APS Count Value"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function ReadAPSType(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadAPSType,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading APS Type"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadBatteryVoltage(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadBatteryVoltage,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			'Parse the response.
			If blnResult Then
				returnString = CSng(returnString) * 0.01
			Else
				Dim strErrorMessage = "Error Reading Battery Voltage"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadBurnInCycles(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadBurnInCyclesMfgSw,
									  ReturnStrFormat.ContiguousBytes, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading BurnIn Cycles"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadBurnInTime(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadBurnInTime,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading BurnIn Time"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadCustomerBootloaderVersion(ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		Dim NotUsed As String
		Try
			If gCanActuator.mTargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes

				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadCustomerBootloaderVersion,
									  ReturnStrFormat.HexByteValues, returnString)

			Else
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadCustomerBootloaderVersion,
									  ReturnStrFormat.AsciiDotDelimited, returnString)
			End If
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading Customer Bootloader Version"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function TestCustomerBootloaderEntry(ByRef returnString As String) As Boolean
		Dim CanMessage = New clsCANMessage

		Try

			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.RequestDIDForECUVariantID,
									  ReturnStrFormat.AsciiDotDelimited, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error testing Customer Bootloader"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If
			' test return string
			'Load the CAN message.
			If Not mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.RequestDIDForECUVariantIDAppResponse, CanMessage) Then
				Dim strErrorMessage = "Read Message Function Type Value Is Out of Range."
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
			End If
			If Mid(System.Text.Encoding.ASCII.GetString(CanMessage.Data), 2) <> returnString Then
				'If CanMessage.data() IsNot returnString Then
				Return False
			End If


			blnResult = SendReadMessage(clsCANMessage.MessageFunctions.CommandTransferToProgrammingSession,
									  ReturnStrFormat.AsciiDotDelimited, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error testing Customer Bootloader"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If
			' test return string
			'Load the CAN message.
			If Not mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.CommandTransferToProgrammingSessionResponse, CanMessage) Then
				Dim strErrorMessage = "Read Message Function Type Value Is Out of Range."
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
			End If
			If Mid(System.Text.Encoding.ASCII.GetString(CanMessage.Data), 2) <> returnString Then
				'If CanMessage.data() IsNot returnString Then
				Return False
			End If

			blnResult = SendReadMessage(clsCANMessage.MessageFunctions.RequestDIDForECUVariantID,
									  ReturnStrFormat.AsciiDotDelimited, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error testing Customer Bootloader"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If
			' test return string
			'Load the CAN message.
			If Not mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.RequestDIDForECUVariantIDCcblResponse, CanMessage) Then
				Dim strErrorMessage = "Read Message Function Type Value Is Out of Range."
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
			End If
			If Mid(System.Text.Encoding.ASCII.GetString(CanMessage.Data), 2) <> returnString Then
				'If CanMessage.data() IsNot returnString Then
				Return False
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadDefaultBootloaderVersion(ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		Dim NotUsed As String
		Try
			If gCanActuator.mTargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON Then
				'not yet working for Covington
				returnString = "NotWorking"
				Return True
			End If
			Select Case mTargetActuator
				Case clsCANActuator.ActuatorVariantEnum.COVINGTON
					blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
					'blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2)
					If mAnomaly IsNot Nothing Then
						Throw New TsopAnomalyException
					End If
					Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes

					blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadDefaultBootloaderVersion,
										  ReturnStrFormat.HexByteValues, returnString)
					If mAnomaly IsNot Nothing Then
						Throw New TsopAnomalyException
					End If

					If Not blnResult Then
						Dim strErrorMessage = "Error Reading Default Bootloader Version"
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
						Throw New TsopAnomalyException
					End If
				Case ActuatorVariantEnum.CATFUEL, ActuatorVariantEnum.CATTHROTTLE
					blnResult = EnterBootloader()
					If Not blnResult Then
						Dim strErrorMessage = "Error Reading Default Bootloader Version"
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
						Throw New TsopAnomalyException
					End If
					returnString = mstrDefaultBootloaderVersion
					blnResult = StartApplication()
					If Not blnResult Then
						Dim strErrorMessage = "Error Restarting Application"
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
						Throw New TsopAnomalyException
					End If
				Case Else
					blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadDefaultBootloaderVersion,
									  ReturnStrFormat.AsciiDotDelimited, returnString)
					If mAnomaly IsNot Nothing Then
						Throw New TsopAnomalyException
					End If

					If Not blnResult Then
						Dim strErrorMessage = "Error Reading Default Bootloader Version"
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
						Throw New TsopAnomalyException
					End If
			End Select

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadEEPromIntoIntelHexFormat() As Boolean

		If (mTargetActuator = ActuatorVariantEnum.COVINGTON) Then
			Return ReadEEPromIntoIntelHexFormatGenB()
		Else
			Return ReadEEPromIntoIntelHexFormatGenA()
		End If

	End Function


	Public Function ReadEEPromIntoIntelHexFormatGenB() As Boolean
		Dim UploadRecord As New IntelDataRecord
		Dim blnReturnValue As Boolean
		Dim StartAddress As Integer
		Dim NextStartAddress As Integer
		Dim EndAddress As Integer
		Dim DataRecordLength As Integer
		Dim targetMemory As TargetMemoryAreaEnum
		Dim read_error As Boolean
		Dim error_cnt As Integer
		Dim strErrorMessage As String
		Dim MemReadMsg As clsCANMessage
		Dim DfltBtldrMemReadDataAckMsg As clsCANMessage
		Dim blnResult As Boolean
		Dim canMsg As New TPCANMsg
		Dim lintTotalReadBytes As Integer
		Dim lintActualNumBytesLeft As Integer
		Dim lintNumBytesRead As Integer
		Dim intDataIndex As Integer
		Dim drAddress As DataRow
		Dim intAddress As Integer

		Try

			MemReadMsg = CANMessages(clsCANMessage.MessageFunctions.ReadEEPROMGenBRequest)
			DfltBtldrMemReadDataAckMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryReadDataAck)
			blnResult = False
			error_cnt = 0
			NextStartAddress = 0
			lintTotalReadBytes = 0
			lintActualNumBytesLeft = 0
			lintNumBytesRead = 0

			'Clear record list
			mIntelHexFile.Clear()

			For Each drAddress In mdtAddressRanges.Rows
				Application.DoEvents()
				Thread.Sleep(1)

				If ((drAddress.Item("ActuatorVariant") <> mTargetActuator.ToString)) Then
					' The address range's variant value doesn't match the actuator variant.
					Continue For
				End If

				StartAddress = Convert.ToInt32(drAddress.Item("StartAddress"), 16)
				NextStartAddress = StartAddress
				EndAddress = Convert.ToInt32(drAddress.Item("EndAddress"), 16)
				DataRecordLength = Convert.ToInt32(drAddress.Item("RecordLength"), 16)
				targetMemory = DirectCast([Enum].Parse(GetType(TargetMemoryAreaEnum),
											   drAddress.Item("TargetMemoryArea")), Byte)

				Console.WriteLine("Processing StartAddress = " & Hex(StartAddress))
				Dim payloadTimes As Integer = Math.Ceiling(DataRecordLength / 6)


				For intAddress = StartAddress To EndAddress Step CUInt(DataRecordLength)
					Dim startCycle As Integer = 0
					'Start a New Data Record
					UploadRecord = New IntelDataRecord
					UploadRecord.RecordType = IntelDataRecord.HEXRecordType.DataRecord

					'Start Address
					'UploadRecord.StartAddress = NextStartAddress
					UploadRecord.StartAddress = intAddress

					'Calculate End Address
					If EndAddress <= UploadRecord.StartAddress + DataRecordLength - 1 Then
						UploadRecord.EndAddress = EndAddress
					Else
						'UploadRecord.EndAddress = CUShort(UploadRecord.StartAddress + 5)
						UploadRecord.EndAddress = CUShort(UploadRecord.StartAddress + DataRecordLength - 1)
					End If

					'Calculate Next Start Address
					'If (UploadRecord.EndAddress < &HFFFF) Then
					'	' This is the max. address value. Don't try to increment value beyond 0xffff.
					'	NextStartAddress = CUShort(UploadRecord.EndAddress + 1)
					'End If

					'Calculate Data Length
					UploadRecord.DataLength = CUShort(UploadRecord.EndAddress - UploadRecord.StartAddress + 1)
					ReDim UploadRecord.Data(UploadRecord.DataLength)

					Do
						read_error = False
						Dim transferBytes As Integer
						If startCycle = payloadTimes - 1 Then
							transferBytes = UploadRecord.DataLength - startCycle * 6
						Else
							transferBytes = 6
						End If

						'Data For Initiate Memory Read Command
						MemReadMsg.Data(0) = (MemReadMsg.Data(0) And (&HF0)) Or ((UploadRecord.StartAddress + startCycle * 6) >> 8)
						Console.WriteLine(MemReadMsg.Data(0))
						MemReadMsg.Data(1) = transferBytes
						MemReadMsg.Data(2) = (UploadRecord.StartAddress + startCycle * 6) And (&HFF)            'Mask off Upper bytes

						' Send Initiate Memory Read Command
						mCanMsgHandler.LoadCANMsgOutputQueue(MemReadMsg)

						'Wait for Initiate Memory Read Acknowledge
						System.Threading.Thread.Sleep(20)
						Dim base = 240 + (transferBytes << 8)
						blnResult = GetToolMsgResp((base Or ((UploadRecord.StartAddress + startCycle * 6) >> 8)), canMsg)

						If (blnResult = True) Then 'Actuator Acknowledged Initiate Memory Read and Returned the Number Of Bytes to Read
							For intDataIndex = 0 To transferBytes - 1
								UploadRecord.Data(intDataIndex + startCycle * 6) = canMsg.DATA(intDataIndex)
							Next

							startCycle += 1
						Else
							' The actuator returned an error in response to an init memory read command..
							read_error = True
							Console.WriteLine("Read Error After GetMemoryReadInitAck " &
								"Before CloseSession. EEProm Adx = " & intAddress)
						End If
						If (read_error = True) Then
							error_cnt += 1
							' Re-initialize variables for read retry.
							'StartAddress = Convert.ToInt32(drAddress.Item("StartAddress"), 16)
							'NextStartAddress = StartAddress
							NextStartAddress = UploadRecord.StartAddress + startCycle * 6
							'EndAddress = Convert.ToInt32(drAddress.Item("EndAddress"), 16)
							'DataRecordLength = Convert.ToInt32(drAddress.Item("RecordLength"), 16)
							'targetMemory = DirectCast([Enum].Parse(GetType(TargetMemoryAreaEnum), drAddress.Item("TargetMemoryArea")), Byte)
						End If
					Loop While (startCycle < payloadTimes) Or ((read_error = True) AndAlso (error_cnt < 10))

					If (read_error = False) Then
						' Successful read.
						mIntelHexFile.Add(UploadRecord)
						Console.WriteLine("Adding Record StartAddress = " & Hex(UploadRecord.StartAddress))
					ElseIf (error_cnt >= 10) Then
						' Too many communications related failures occurred.
						strErrorMessage = "Too Many Communication Related Failures Occured Reading EEProm"
						Console.WriteLine(strErrorMessage)
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									  mStackTrace.CurrentFunctionName, strErrorMessage)
						Throw New TsopAnomalyException
					End If
				Next 'Record
			Next

			'Add EOF Record
			UploadRecord = New IntelDataRecord
			UploadRecord.RecordType = IntelDataRecord.HEXRecordType.EOFRecord
			UploadRecord.StartAddress = 0
			UploadRecord.EndAddress = 0
			UploadRecord.DataLength = 0
			mIntelHexFile.Add(UploadRecord)

			blnReturnValue = True

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try

	End Function

	Public Function ReadEEPromIntoIntelHexFormatGenA() As Boolean
		Dim UploadRecord As New IntelDataRecord
		Dim blnReturnValue As Boolean
		Dim StartAddress As UShort
		Dim NextStartAddress As UShort
		Dim EndAddress As UShort
		Dim DataRecordLength As Integer
		Dim targetMemory As TargetMemoryAreaEnum
		Dim read_error As Boolean
		Dim error_cnt As Integer
		Dim strErrorMessage As String
		Dim DfltBtldrInitMemReadMsg As clsCANMessage
		Dim DfltBtldrMemReadDataAckMsg As clsCANMessage
		Dim blnResult As Boolean
		Dim canMsg As New TPCANMsg
		Dim lintTotalReadBytes As Integer
		Dim lintActualNumBytesLeft As Integer
		Dim lintNumBytesRead As Integer
		Dim intDataIndex As Integer
		Dim drAddress As DataRow
		Dim intAddress As UShort

		Try

			DfltBtldrInitMemReadMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryReadRequest)
			DfltBtldrMemReadDataAckMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryReadDataAck)
			blnResult = False
			error_cnt = 0
			NextStartAddress = 0
			lintTotalReadBytes = 0
			lintActualNumBytesLeft = 0
			lintNumBytesRead = 0

			'Clear record list
			mIntelHexFile.Clear()

			For Each drAddress In mdtAddressRanges.Rows
				Application.DoEvents()
				Thread.Sleep(1)

				If (mTargetActuator = ActuatorVariantEnum.CATFUEL Or mTargetActuator = ActuatorVariantEnum.CATTHROTTLE) And
					(drAddress.Item("ActuatorVariant") <> mTargetActuator.ToString) Then
					Continue For
				ElseIf ((drAddress.Item("ActuatorVariant") <> ActuatorVariantEnum.UNDEFINED.ToString) AndAlso
			(drAddress.Item("ActuatorVariant") <> mTargetActuator.ToString)) Then
					' The address range's variant value doesn't match the actuator variant.
					Continue For
				End If

				StartAddress = Convert.ToInt32(drAddress.Item("StartAddress"), 16)
				NextStartAddress = StartAddress
				EndAddress = Convert.ToInt32(drAddress.Item("EndAddress"), 16)
				DataRecordLength = Convert.ToInt32(drAddress.Item("RecordLength"), 16)
				targetMemory = DirectCast([Enum].Parse(GetType(TargetMemoryAreaEnum),
											   drAddress.Item("TargetMemoryArea")), Byte)

				Console.WriteLine("Processing StartAddress = " & Hex(StartAddress))

				For intAddress = StartAddress To EndAddress Step CUInt(DataRecordLength)
					Do
						read_error = False

						'Start a New Data Record
						UploadRecord = New IntelDataRecord
						UploadRecord.RecordType = IntelDataRecord.HEXRecordType.DataRecord

						'Start Address
						UploadRecord.StartAddress = NextStartAddress

						'Calculate End Address
						If EndAddress <= UploadRecord.StartAddress + DataRecordLength - 1 Then
							UploadRecord.EndAddress = EndAddress
						Else
							UploadRecord.EndAddress = CUShort(UploadRecord.StartAddress + DataRecordLength - 1)
						End If

						'Calculate Next Start Address
						If (UploadRecord.EndAddress < &HFFFF) Then
							' This is the max. address value. Don't try to increment value beyond 0xffff.
							NextStartAddress = CUShort(UploadRecord.EndAddress + 1)
						End If

						'Calculate Data Length
						UploadRecord.DataLength = CUShort(UploadRecord.EndAddress - UploadRecord.StartAddress + 1)
						ReDim UploadRecord.Data(UploadRecord.DataLength - 1)

						'Data For Initiate Memory Read Command
						DfltBtldrInitMemReadMsg.Data(1) = targetMemory
						DfltBtldrInitMemReadMsg.Data(2) = CByte(UploadRecord.StartAddress And (&HFF))            'Mask off Upper bytes
						DfltBtldrInitMemReadMsg.Data(3) = CByte((UploadRecord.StartAddress And (&HFF00)) / &H100) 'Mask off lower bytes
						DfltBtldrInitMemReadMsg.Data(4) = CByte(UploadRecord.EndAddress And (&HFF))              'Mask off Upper bytes
						DfltBtldrInitMemReadMsg.Data(5) = CByte((UploadRecord.EndAddress And (&HFF00)) / &H100)  'Mask off lower bytes

						' Send Initiate Memory Read Command
						mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrInitMemReadMsg)

						'Wait for Initiate Memory Read Acknowledge
						blnResult = GetMemoryReadInitAck(canMsg)
						'If mAnomaly IsNot Nothing Then
						'  Console.WriteLine("Error After GetMemoryReadInitAck: " & mAnomaly.AnomalyExceptionMessage & ", " & mAnomaly.AnomalyMessageText)
						'End If

						If (blnResult = True) Then 'Actuator Acknowledged Initiate Memory Read and Returned the Number Of Bytes to Read
							lintTotalReadBytes = (CInt(canMsg.DATA(2)) << 8) + canMsg.DATA(1)

							'Check that requested bytes = actual bytes
							If UploadRecord.DataLength = lintTotalReadBytes Then
								'Prepare Data Array for Number of Bytes To Read
								ReDim UploadRecord.Data(lintTotalReadBytes - 1)

								'Initialize Number Of Bytes That Have Been Read
								lintNumBytesRead = 0
								Do 'Begin Memory Data Read

									'Wait for Memory Read Data Message from Actuator
									blnResult = GetMemoryReadData(canMsg)
									'If mAnomaly IsNot Nothing Then
									'  Console.WriteLine("Error After GetMemoryReadData: " & mAnomaly.AnomalyExceptionMessage & ", " & mAnomaly.AnomalyMessageText)
									'End If

									If (blnResult = True) Then 'Memory Was Read Successfully
										'Save Data to Array
										For intDataIndex = lintNumBytesRead + 1 To ((canMsg.LEN - 1) + lintNumBytesRead)
											'Console.WriteLine(Hex(StartAddress) & " To " & Hex(EndAddress) & " (" & DataRecordLength & " Bytes)" & "-" & intDataIndex)
											UploadRecord.Data(intDataIndex - 1) = canMsg.DATA(intDataIndex - lintNumBytesRead)
										Next

										'Update Number of Bytes That Have Been Read and Number of Bytes Left to Read
										lintNumBytesRead = lintNumBytesRead + canMsg.LEN - 1
										lintActualNumBytesLeft = UploadRecord.DataLength - lintNumBytesRead

										'Data For Memory Read Data Acknowledge
										DfltBtldrMemReadDataAckMsg.Data(1) = CByte(lintActualNumBytesLeft And &HFF)
										DfltBtldrMemReadDataAckMsg.Data(2) = CByte(lintActualNumBytesLeft >> 8)

										'Send Memory Read Data Acknowledge, The actuator will continue sending data until all desired bytes have been sent and acknowledged
										mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrMemReadDataAckMsg)
									Else
										' The actuator returned an error in response to an memory read command..
										read_error = True
										Console.WriteLine("Read Error After GetMemoryReadData Before " &
									  "CloseSession. EEProm Adx = " & intAddress)
										If (CloseSession() = False) Then
											' Failed to close DBL programming session prior to re-read attempt.
											strErrorMessage = "Failed to Close Default Bootloader Session " &
										"for Memory Read Retry After GetMemoryReadData"
											If mAnomaly IsNot Nothing Then
												strErrorMessage = strErrorMessage & " " &
										  mAnomaly.AnomalyExceptionMessage &
										  mAnomaly.AnomalyMessageText
											End If
											Console.WriteLine(strErrorMessage)
											mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
												mStackTrace.CurrentFunctionName, strErrorMessage)
											Throw New TsopAnomalyException
										End If
										' DBL programming session closed, so try again.
										Exit Do
									End If
								Loop While (lintActualNumBytesLeft > 0)
							Else
								' Reported number of bytes doesn't match the number of bytes available in the record's address range.
								read_error = True
								Console.WriteLine("Read Error After Compare Data Length to Read " &
								  "Bytes Before CloseSession. EEProm Adx = " & intAddress)
								If (CloseSession() = False) Then
									' Failed to close DBL programming session prior to re-read attempt.
									strErrorMessage = "Failed to Close Default Bootloader Session for " &
									"Memory Read Retry After GetMemoryReadInitAck Before CloseSession"
									If mAnomaly IsNot Nothing Then
										strErrorMessage = strErrorMessage & " " &
									  mAnomaly.AnomalyExceptionMessage &
									  mAnomaly.AnomalyMessageText
									End If
									Console.WriteLine(strErrorMessage)
									mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
											mStackTrace.CurrentFunctionName, strErrorMessage)
									Throw New TsopAnomalyException
								End If
								' DBL programming session closed, so try again.
							End If
						Else
							' The actuator returned an error in response to an init memory read command..
							read_error = True
							Console.WriteLine("Read Error After GetMemoryReadInitAck " &
								"Before CloseSession. EEProm Adx = " & intAddress)
							If (CloseSession() = False) Then
								strErrorMessage = "Failed to Close Default Bootloader Session for Memory Read " &
								  "Retry After GetMemoryReadInitAck Before CloseSession"
								If mAnomaly IsNot Nothing Then
									strErrorMessage = strErrorMessage & " " &
									mAnomaly.AnomalyExceptionMessage &
									mAnomaly.AnomalyMessageText
								End If
								' Failed to close DBL programming session prior to re-read attempt.
								Console.WriteLine(strErrorMessage)
								mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
										  mStackTrace.CurrentFunctionName, strErrorMessage)
								Throw New TsopAnomalyException
							End If
							' DBL programming session closed, so try again.    
						End If
						If (read_error = True) Then
							error_cnt += 1
							' Re-initialize variables for read retry.
							'StartAddress = Convert.ToInt32(drAddress.Item("StartAddress"), 16)
							'NextStartAddress = StartAddress
							NextStartAddress = intAddress
							'EndAddress = Convert.ToInt32(drAddress.Item("EndAddress"), 16)
							'DataRecordLength = Convert.ToInt32(drAddress.Item("RecordLength"), 16)
							'targetMemory = DirectCast([Enum].Parse(GetType(TargetMemoryAreaEnum), drAddress.Item("TargetMemoryArea")), Byte)
						End If
					Loop While ((read_error = True) AndAlso (error_cnt < 10))
					If (read_error = False) Then
						' Successful read.
						mIntelHexFile.Add(UploadRecord)
						Console.WriteLine("Adding Record StartAddress = " & Hex(UploadRecord.StartAddress))
					ElseIf (error_cnt >= 10) Then
						' Too many communications related failures occurred.
						strErrorMessage = "Too Many Communication Related Failures Occured Reading EEProm"
						Console.WriteLine(strErrorMessage)
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									  mStackTrace.CurrentFunctionName, strErrorMessage)
						Throw New TsopAnomalyException
					End If
				Next 'Record
			Next

			'Add EOF Record
			UploadRecord = New IntelDataRecord
			UploadRecord.RecordType = IntelDataRecord.HEXRecordType.EOFRecord
			UploadRecord.StartAddress = 0
			UploadRecord.EndAddress = 0
			UploadRecord.DataLength = 0
			mIntelHexFile.Add(UploadRecord)

			blnReturnValue = True

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Private Function CountBits(ByRef returnString As String)
		Dim splStr As String() = returnString.Split(New Char() {","})
		Dim count As Int32 = 0
		For Each eachStr As String In splStr
			Dim eachBitVec As Byte = CInt(eachStr)
			For s As Int32 = 0 To 7
				count = count + ((eachBitVec >> s) And 1)
			Next s
		Next eachStr
		Return 16 - count
	End Function


	Public Function ReadFaultCount(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadFaultCount,
									  ReturnStrFormat.ContiguousBytes, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			'Parse the response.
			If blnResult Then
				' Counter is decremented, starting value = &HFA = 250.
				returnString = 250 - CInt(returnString)
			Else
				Dim strErrorMessage = "Error Reading Fault Count"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadInternalTemperature(ByRef returnString As String) As Boolean
		'Dim lStopWatch As New clsStopWatch

		Try
			' RPW Sleep(500) 'give some time to penetrate the conformal coating
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadInternalTemperature,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading Internal Temperature"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadActuatorPositionStatus(ByRef returnString As String) As Boolean
		Dim readStatusMsg As New TPCANMsg
		Dim blnReturnValue As Boolean = False
		Dim strErrorMessage As String
		Dim intLoopCount As Integer
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double
		Dim canMsg As New TPCANMsg
		Dim intCurrentPosition As Integer

		Try
			blnReturnValue = False

			'Clear status messages
			SyncLock mStatusMsgLock
				mStatusMsgs.Clear()
			End SyncLock
			Delay(100)

			'Register status message callback delegate
			mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			intLoopCount = 0
			Do
				' Initialize the elapsed time starting value
				QueryPerformanceCounter(HP_CounterValue)
				dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
				Do
					Thread.Sleep(1)
					If (mStatusMsgs.Count > 0) Then
						Exit Do
					End If
					' Retrieve the current counter value.
					QueryPerformanceCounter(HP_CounterValue)
					' Calculate the current time
					dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				Loop While (dblCurrentTime - dblStartTime < 1000)

				intLoopCount += 1

			Loop While ((mStatusMsgs.Count = 0) AndAlso (intLoopCount < 2))

			If GetStatusMsg(2, canMsg) Then
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				intCurrentPosition = canMsg.DATA(1) + canMsg.DATA(2) * 256
				returnString = intCurrentPosition
				blnReturnValue = True
			End If

			If blnReturnValue = False Then
				strErrorMessage = "Error Reading Position Status"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
		End Try
	End Function

	Public Function ReadActuatorState(ByRef returnString As String) As Boolean
		Dim readStatusMsg As New TPCANMsg
		Dim blnReturnValue As Boolean = False
		Dim strErrorMessage As String
		Dim intLoopCount As Integer
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double
		Dim canMsg As New TPCANMsg
		Dim intState As Integer

		Try
			blnReturnValue = False

			'Clear status messages
			SyncLock mStatusMsgLock
				mStatusMsgs.Clear()
			End SyncLock
			Delay(100)

			'Register status message callback delegate
			mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			intLoopCount = 0
			Do
				' Initialize the elapsed time starting value
				QueryPerformanceCounter(HP_CounterValue)
				dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
				Do
					Thread.Sleep(1)
					If (mStatusMsgs.Count > 0) Then
						Exit Do
					End If
					' Retrieve the current counter value.
					QueryPerformanceCounter(HP_CounterValue)
					' Calculate the current time
					dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				Loop While (dblCurrentTime - dblStartTime < 1000)

				intLoopCount += 1

			Loop While ((mStatusMsgs.Count = 0) AndAlso (intLoopCount < 2))

			If GetStatusMsg(2, canMsg) Then
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				intState = canMsg.DATA(0)
				returnString = intState
				blnReturnValue = True
			End If

			If blnReturnValue = False Then
				strErrorMessage = "Error Reading Actuator State"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
		End Try
	End Function

	Public Function ReadFlashChecksum(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadFlashChecksum,
									  ReturnStrFormat.HexByteValues, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading Program Checksum"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadProgramChecksum(ByRef returnString As String) As Boolean

		Dim blnResult As Boolean

		Try
			If mTargetActuator = ActuatorVariantEnum.CATFUEL Or mTargetActuator = ActuatorVariantEnum.CATTHROTTLE Then
				Delay(500)
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadProgramChecksum,
							  ReturnStrFormat.HexByteValues, returnString)
				If returnString = "00000000" Then
					Delay(500)
					blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadProgramChecksum,
							  ReturnStrFormat.HexByteValues, returnString)
				End If
			Else

				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadFlashChecksum,
									  ReturnStrFormat.HexByteValues, returnString)
			End If
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading Program Checksum"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadProgramChecksumInteger(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadProgramChecksumInteger,
									  ReturnStrFormat.IntegerValue, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Reading Program Checksum"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadSerialNumberFromBL(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSerialNumberFromBL,
									  ReturnStrFormat.HexByteValues, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading Serial Number from bootloader"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				If returnString.Length > 8 Then 'tool message request (vs PID)
					mstrSerialNumber = Mid(returnString, 1, 4) & Mid(returnString, 7, 4)
					returnString = mstrSerialNumber 'remove embedded "FF" from return string
				Else
					mstrSerialNumber = returnString 'PID request for serial number
				End If
			End If


			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadSerialNumber(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSerialNumber,
									  ReturnStrFormat.HexByteValues, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Reading Serial Number"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				If returnString.Length > 8 Then 'tool message request (vs PID)
					mstrSerialNumber = Mid(returnString, 1, 4) & Mid(returnString, 7, 4)
					returnString = mstrSerialNumber 'remove embedded "FF" from return string
				Else
					mstrSerialNumber = returnString 'PID request for serial number
				End If
			End If


			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteSerialNumber(ByRef SerialNumber As Integer) As Boolean

		Dim NewMessage As clsCANMessage
		Dim firstbyte As Int16
		Dim secondbyte As Int16
		Dim thirdbyte As Int16
		Dim fourthbyte As Int16
		Dim blnResult As Boolean
		Dim CANMessageData(3) As Integer
		Dim ReturnString As String

		Dim NotUsed As String

		If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
			blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If
			Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes

			'need old serial number to enable this message
			CANMessageData(3) = Convert.ToInt32(Mid(mstrSerialNumber, 1, 2), 16)
			CANMessageData(2) = Convert.ToInt32(Mid(mstrSerialNumber, 3, 2), 16)
			CANMessageData(1) = Convert.ToInt32(Mid(mstrSerialNumber, 5, 2), 16)
			CANMessageData(0) = Convert.ToInt32(Mid(mstrSerialNumber, 7, 2), 16)
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolCommandDataWrite, CANMessageData)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If
			Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
		End If


		firstbyte = (SerialNumber And -16777216) >> 24
		secondbyte = (SerialNumber And 16711680) >> 16
		thirdbyte = (SerialNumber And 65280) >> 8
		fourthbyte = (SerialNumber And 255)

		'NewMessage = mCANMessages(clsCANMessage.MessageFunctions.WriteSerialNumber)
		'NewMessage.StaticData(7) = firstbyte.ToString("X2")
		'NewMessage.StaticData(6) = secondbyte.ToString("X2")
		'NewMessage.StaticData(5) = thirdbyte.ToString("X2")
		'NewMessage.StaticData(4) = fourthbyte.ToString("X2")
		CANMessageData(3) = firstbyte
		CANMessageData(2) = secondbyte
		CANMessageData(1) = thirdbyte
		CANMessageData(0) = fourthbyte

		Try
			'  blnResult = SendMessageNoResponse(NewMessage)
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteSerialNumber, CANMessageData, ReturnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Writing Serial Number"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			'record new serial number
			mstrSerialNumber = firstbyte.ToString("X2") & secondbyte.ToString("X2") & thirdbyte.ToString("X2") & fourthbyte.ToString("X2")

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function
	'no longer needed, not used
	'Public Function ReadSerialNumber(ByRef returnString As String, ByVal softwareGeneration As String) As Boolean

	'  Dim blnResult As Boolean = False

	'  Try
	'    'Find CAN Message in collection
	'    Select Case softwareGeneration
	'      Case "1"
	'        blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSerialNumber,
	'                                    ReturnStrFormat.HexByteValues, returnString)
	'      Case "2"
	'        blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSerialNumberG2,
	'                                    ReturnStrFormat.HexByteValues, returnString)
	'    End Select

	'    If mAnomaly IsNot Nothing Then
	'      Throw New TsopAnomalyException
	'    End If

	'    If Not blnResult Then
	'      Dim strErrorMessage = "Error Reading Serial Number"
	'      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
	'                                mStackTrace.CurrentFunctionName, strErrorMessage)
	'      Throw New TsopAnomalyException
	'    End If

	'    Return blnResult

	'  Catch ex As TsopAnomalyException
	'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
	'    Return False
	'  Catch ex As Exception
	'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
	'    Return False
	'  End Try
	'End Function

	Public Function ReadActuatorSourceID(ByRef returnString As String) As Boolean
		Dim readStatusMsg As New TPCANMsg
		Dim blnReturnValue As Boolean = False
		Dim strErrorMessage As String
		Dim intLoopCount As Integer
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double
		Dim canMsg As New TPCANMsg

		Try
			blnReturnValue = False

			'Clear status messages
			SyncLock mStatusMsgLock
				mStatusMsgs.Clear()
			End SyncLock
			Delay(100)

			'Register status message callback delegate
			mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			intLoopCount = 0
			Do
				' Initialize the elapsed time starting value
				QueryPerformanceCounter(HP_CounterValue)
				dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
				Do
					Thread.Sleep(1)
					If (mStatusMsgs.Count > 0) Then
						Exit Do
					End If
					' Retrieve the current counter value.
					QueryPerformanceCounter(HP_CounterValue)
					' Calculate the current time
					dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				Loop While (dblCurrentTime - dblStartTime < 1000)

				intLoopCount += 1

			Loop While ((mStatusMsgs.Count = 0) AndAlso (intLoopCount < 2))

			If mStatusMsgs.Count > 0 Then
				returnString = mStatusMsgs.Keys(0)
				blnReturnValue = True
			End If

			If blnReturnValue = False Then
				strErrorMessage = "Error Reading Source ID"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
		End Try
	End Function

	Public Function ReadSpecialMemoryLine1(ByRef UpperByte As String, ByRef LowerByte As String) As Boolean

		Try
			Dim resultString As String = ""
			Dim blnResult As Boolean
			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If

			blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSpecialMemoryLine1,
									  ReturnStrFormat.DotDelimited, resultString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult AndAlso resultString.Split(".").Length >= 2 Then
				LowerByte = (resultString.Split(".")(0)).Trim
				UpperByte = (resultString.Split(".")(1)).Trim
			Else
				Dim strErrorMessage = "Error Reading Special Memory Line 1"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadSpecialMemoryLine2(ByRef UpperByte As String, ByRef LowerByte As String) As Boolean

		Try
			Dim resultString As String = ""
			Dim blnResult As Boolean
			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If

			blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSpecialMemoryLine2,
									  ReturnStrFormat.DotDelimited, resultString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			'Parse the response.
			If blnResult AndAlso resultString.Split(".").Length >= 2 Then
				LowerByte = (resultString.Split(".")(0)).Trim
				UpperByte = (resultString.Split(".")(1)).Trim
			Else
				Dim strErrorMessage = "Error Reading Special Memory Line 2"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadPowerCycleCounts(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadPowerCycleCounts,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Reading Power Cycle Counts"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function ReadRunTime(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadRunTime,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Reading Total Run Time"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadSpan(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadSpan,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Reading Span"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadTempHWM(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadTempHWM,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Reading Temp HWM"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadTempLWM(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadTempLWM,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Reading Temp LWM"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadVendorPCB(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadVendorPCB,
									  ReturnStrFormat.ContiguousBytes, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Reading Vendor PCB"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Sub ReleaseCAN()
		Try

			Call mCanMsgHandler.ReleaseCAN()
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Public Function ResetActuator() As Boolean

		Dim blnreturnValue As Boolean = False
		Dim CanMessage As clsCANMessage
		Dim strErrorMessage As String
		Dim returnData(0) As Byte
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double
		Dim dblElapsedTime As Double

		Try
			blnreturnValue = False
			strErrorMessage = ""

			' Register for Address Claim Messages.
			mCanMsgHandler.RcvMsgDelegates.AddAddrClaimDelegate(AddressOf AddressClaimRcvd)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			' Clear previously received Address Claim messages.
			SyncLock mAddrClaimLock
				mAddressClaimMsgs.Clear()
			End SyncLock

			'Find CAN Message in collection
			CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetActuator)

			'Send Message and Get Response
			blnreturnValue = SendMessageNoResponse(CanMessage)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnreturnValue = False Then
				strErrorMessage = "Error Resetting Actuator before reading Address Claim"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			blnreturnValue = False
			' Look for Address Claim Message to verify Actuator was reset
			' Initialize the elapsed time starting value
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
			' Wait up to Two seconds.
			Do
				If mAddressClaimMsgs.Count > 0 Then
					blnreturnValue = True
					Exit Do
				End If
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				dblElapsedTime = dblCurrentTime - dblStartTime
			Loop While (dblElapsedTime < 2000)

			Console.WriteLine("Reset Actuator Elapsed Time = " & dblElapsedTime)
			If dblElapsedTime >= 2000 Then
				strErrorMessage = "Error Resetting Actuator Timeout reading Address Claim"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' UnRegister for Address Claim Messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveAddrClaimDelegate(AddressOf AddressClaimRcvd)
		End Try
	End Function

	Public Function RequestPGNForAdressClaim() As Boolean

		Dim blnreturnValue As Boolean = False
		Dim CanMessage As clsCANMessage
		Dim strErrorMessage As String
		Dim returnData(0) As Byte
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double
		Dim dblElapsedTime As Double

		Try
			blnreturnValue = False
			strErrorMessage = ""

			' Register for Address Claim Messages.
			mCanMsgHandler.RcvMsgDelegates.AddAddrClaimDelegate(AddressOf AddressClaimRcvd)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			' Clear previously received Address Claim messages.
			SyncLock mAddrClaimLock
				mAddressClaimMsgs.Clear()
			End SyncLock

			'Find CAN Message in collection
			CanMessage = mCANMessages(clsCANMessage.MessageFunctions.RequestPGNForAddressClaim)

			'Send Message and Get Response
			blnreturnValue = SendMessageNoResponse(CanMessage)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnreturnValue = False Then
				strErrorMessage = "Error Sending Request For Address Claim"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			blnreturnValue = False
			'Look for Address Claim Message to verify Actuator was reset
			' Initialize the elapsed time starting value
			QueryPerformanceCounter(HP_CounterValue)
			dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
			' Wait up to two second.
			Do
				If mAddressClaimMsgs.Count > 0 Then
					blnreturnValue = True
					Exit Do
				End If
				' Retrieve the current counter value.
				QueryPerformanceCounter(HP_CounterValue)
				' Calculate the current time
				dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				dblElapsedTime = dblCurrentTime - dblStartTime
			Loop While (dblElapsedTime < 2000)

			If dblElapsedTime >= 2000 Then
				strErrorMessage = "Timeout Error reading Address Claim"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnreturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' UnRegister for Address Claim Messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveAddrClaimDelegate(AddressOf AddressClaimRcvd)
		End Try
	End Function


	Public Function ResetEEPromAll(ByRef returnString As String) As Boolean

		Dim CANMessageData(3) As Integer
		Dim blnResult As Boolean
		Dim NotUsed As String
		Try

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then

				blnResult = WriteOverrideCommand(returnString)

				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes


				'need old serial number to enable this message
				blnResult = ReadSerialNumberFromBL(NotUsed) ' sets mstrSerialNumber from the device
				CANMessageData(3) = Convert.ToInt32(Mid(mstrSerialNumber, 1, 2), 16)
				CANMessageData(2) = Convert.ToInt32(Mid(mstrSerialNumber, 3, 2), 16)
				CANMessageData(1) = Convert.ToInt32(Mid(mstrSerialNumber, 5, 2), 16)
				CANMessageData(0) = Convert.ToInt32(Mid(mstrSerialNumber, 7, 2), 16)
				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolCommandDataWrite, CANMessageData)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetEEPromAll, CANMessageData)
			Else
				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetEEPromAll, returnString)
			End If



			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Resetting EEProm All"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ResetEEPromPowerCycles(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetEEPromPowerCycles, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Resetting EEProm Power Cycle Counts"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ResetEEPromRunTime(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetEEPromRunTime, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Resetting EEProm Total Run Time"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ResetEEPromSpan(ByRef returnString As String) As Boolean

		Dim strErrorMessage As String
		Dim returnData(0) As Byte
		Dim blnResult As Boolean

		Try
			'Put In Override State
			Call WriteOverrideCommand(returnString)
			If returnString <> "1" Then
				strErrorMessage = "Error Writing Override Command during Reset Span"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Call Delay(100)

			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetEEPromSpan, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				strErrorMessage = "Error Resetting EEProm Span"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ResetSerialNumberHighByte(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetSerialNumberHighByte,
										 dynamicData, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error in Reset Serial Number High Byte"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function ResetSourceID(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetSourceID,
										 dynamicData, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error in Reset SourceID"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function ResetSpecialMemoryLine1(ByRef returnString As String) As Boolean

		Try
			Dim blnResult As Boolean

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If

			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetSpecialMemoryLine1, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Resetting Special Memory Line1"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ResetSpecialMemoryLine2(ByRef returnString As String) As Boolean

		Try
			Dim blnResult As Boolean

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ResetSpecialMemoryLine2, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Resetting Special Memory Line2"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function RecordtoStringWithChecksum(ByVal Record As IntelDataRecord) As String
		Dim calcCC As UInteger
		Dim RecordStr As String = ""

		Try
			' Calculate checksum.
			calcCC = 0
			calcCC = calcCC + Record.DataLength
			calcCC = CUInt(calcCC + Record.StartAddress And (&HFF)) 'Mask off Upper bytes
			calcCC = CUInt(calcCC + (Record.StartAddress And (&HFF00)) / &H100) 'Mask off lower bytes
			calcCC = CUInt(calcCC + Record.RecordType)
			For i As Integer = 0 To Record.Data.Length - 1
				calcCC = calcCC + Record.Data(i)
			Next
			calcCC = CUInt(calcCC Mod 256)
			If calcCC <> 0 Then
				calcCC = CUInt(&H100 - calcCC)
			End If
			Record.CheckSum = CUShort(calcCC)
			RecordStr = RecordToString(Record)
			Return RecordStr

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return Nothing
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return Nothing
		Finally
		End Try
	End Function

	Public Sub ReStartCAN()
		Try

			mAnomaly = Nothing 'clear outstanding anomalies

			Call mCanMsgHandler.ReleaseCAN()
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			'Call mCanMsgHandler.StartCAN(Me.mstrCANInterfaceName)
			Call mCanMsgHandler.StartCAN()
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Public Function JumpToDefaultBootloader(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.JumpToDefaultBootloader, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error in Jump To Default Bootloader"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Sub ShutDown()
		Try

			mCanMsgHandler.RcvMsgDelegates.RemoveAllToolMsgDelegates(AddressOf ToolMsgRespRcvd)
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Console.WriteLine("Error After RemoveAllToolMsgDelegates " &
						  mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " &
						  mCanMsgHandler.Anomaly.AnomalyMessageText)
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			mCanMsgHandler.RcvMsgDelegates.RemoveAllStatusMsgDelegates(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Console.WriteLine("Error After RemoveAllStatusMsgDelegates " &
						  mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " &
						  mCanMsgHandler.Anomaly.AnomalyMessageText)
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			mCanMsgHandler.RcvMsgDelegates.RemoveAllAddrClaimDelegates(AddressOf AddressClaimRcvd)
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Console.WriteLine("Error After RemoveAllAddrClaimDelegates " &
						  mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " &
						  mCanMsgHandler.Anomaly.AnomalyMessageText)
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			Call StopActuatorDownloadThread()
			If mAnomaly IsNot Nothing Then
				Console.WriteLine("Error After StopActuatorDownloadThread " &
						  mAnomaly.AnomalyExceptionMessage & " " &
						  mAnomaly.AnomalyMessageText)
				Throw New TsopAnomalyException
			End If

			Call mCanMsgHandler.ShutDown()
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Console.WriteLine("Error After mCanMsgHandler.ShutDown " &
						  mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " &
						  mCanMsgHandler.Anomaly.AnomalyMessageText)
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	'Public Function StartApplication() As Boolean
	'  Dim blnReturnValue As Boolean
	'  Dim strErrorMessage As String
	'  Try
	'    blnReturnValue = False

	'    'Clear status messages
	'    SyncLock mStatusMsgLock
	'      mStatusMsgs.Clear()
	'    End SyncLock

	'    'Register status message callback delegate
	'    mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
	'    Delay(100)

	'    ' Command application start.
	'    mCanMsgHandler.LoadCANMsgOutputQueue(CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderStartApplication))

	'    ' Wait 500 msec while the application starts.
	'    Delay(500)

	'    If (mStatusMsgs.Count > 0) Then
	'      ' The actuator application has started.
	'      blnReturnValue = True
	'      mblnApplicationCodeRunning = True
	'      mblnDefaultBootloaderCodeRunning = False
	'    Else
	'      ' Clear previously received status messages.
	'      SyncLock mStatusMsgLock
	'        mStatusMsgs.Clear()
	'      End SyncLock
	'      ' Command application start.
	'      mCanMsgHandler.LoadCANMsgOutputQueue(CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderStartApplication))
	'      ' Wait 500 msec while the application starts.
	'      Delay(500)
	'      If (mStatusMsgs.Count > 0) Then
	'        ' The actuator application has started.
	'        blnReturnValue = True
	'        mblnApplicationCodeRunning = True
	'        mblnDefaultBootloaderCodeRunning = False
	'      Else
	'        strErrorMessage = "Application Restart Failed"
	'      End If
	'    End If

	'    Return blnReturnValue

	'  Catch ex As TsopAnomalyException
	'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
	'    Return False
	'  Catch ex As Exception
	'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
	'    Return False
	'  Finally
	'    ' Unregister for status messages.
	'    mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
	'  End Try
	'End Function

	Public Function StartApplication() As Boolean

		Dim blnReturnValue As Boolean
		Dim strErrorMessage As String
		Dim intLoopCount As Integer
		Dim dblStartTime As Double
		Dim dblCurrentTime As Double

		Try
			blnReturnValue = False

			'Clear status messages
			SyncLock mStatusMsgLock
				mStatusMsgs.Clear()
			End SyncLock
			Delay(100)

			'Register status message callback delegate
			mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
			If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
				mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

			intLoopCount = 0
			Do
				' Command application start.
				Dim startMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderStartApplication)
				mCanMsgHandler.LoadCANMsgOutputQueue(startMsg)

				' Initialize the elapsed time starting value
				QueryPerformanceCounter(HP_CounterValue)
				dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
				Do
					Thread.Sleep(1)
					If (mStatusMsgs.Count > 0) Then
						Exit Do
					End If
					' Retrieve the current counter value.
					QueryPerformanceCounter(HP_CounterValue)
					' Calculate the current time
					dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
				Loop While (dblCurrentTime - dblStartTime < 2500.0)

				intLoopCount += 1

			Loop While ((mStatusMsgs.Count = 0) AndAlso (intLoopCount < 2))

			If (mStatusMsgs.Count > 0) Then
				' The actuator application has started.
				blnReturnValue = True
				mblnApplicationCodeRunning = True
				mblnDefaultBootloaderCodeRunning = False
			Else
				strErrorMessage = "Application Restart Failed"
			End If

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnReturnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			' Unregister for status messages.
			mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
		End Try
	End Function

	Public Sub StartCAN()
		Try

			'Call mCanMsgHandler.StartCAN(Me.mstrCANInterfaceName)
			Call mCanMsgHandler.StartCAN()
			If mCanMsgHandler.Anomaly IsNot Nothing Then
				Me.Anomaly = mCanMsgHandler.Anomaly
				mCanMsgHandler.Anomaly = Nothing
				Throw New TsopAnomalyException
			End If

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		End Try
	End Sub

	Public Function StartContinuousMove(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		Try
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.StartContinuousMove,
										 dynamicData, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Start Continuous Move CAN Message"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function StartContinuousMoveG2(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean

		Dim strErrorMessage As String
		Dim blnResult As Boolean
		Dim NotUsed As String
		Try
			blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
			'Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If
			Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			If blnResult Then
				'Find CAN Message in collection
				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.StartContinuousMoveG2)

				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.CANCommandMessage, dynamicData, returnString)

				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If

				If Not blnResult Then
					strErrorMessage = "Error Start Continuous Move CAN Message"
					mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									mStackTrace.CurrentFunctionName, strErrorMessage)
					Throw New TsopAnomalyException
				End If
			Else
				strErrorMessage = "Error Start Continuous Move CAN Message"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function


	Public Function ValveCalibrate(ByRef returnString As String) As Boolean

		Dim strErrorMessage As String

		Try
			'Put In Override State
			Call WriteOverrideCommand(returnString)
			If returnString <> "1" Then
				strErrorMessage = "Error Writing Override Command during Valve Calibrate"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Call Delay(100)

			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.ValveCalibrate, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				strErrorMessage = "Error in Valve Calibrate CAN Message"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteIntelHexFile() As Boolean

		Dim returnValue As Boolean = False
		Dim myRecord As IntelDataRecord
		Dim calcCC As UInteger
		Dim lblneof As Boolean = False
		Dim ReadData(7) As Byte
		Dim lblnResult As Boolean = False
		Dim UploadStreamWriter As IO.StreamWriter = Nothing
		Dim fileName As String

		Try
			fileName = mstrActuatorDataPath & "\" & Date.Today.Year.ToString & Date.Today.Month.ToString("D2") &
				 Date.Today.Day.ToString("D2") & Date.Now.ToString("HHmmss") & "_" & mstrSerialNumber & ".txt"
			UploadStreamWriter = New IO.StreamWriter(fileName, False)
			If (UploadStreamWriter IsNot Nothing) Then
				For Each myRecord In mIntelHexFile
					' Calculate checksum.
					calcCC = 0
					calcCC = calcCC + myRecord.DataLength
					calcCC = CUInt(calcCC + myRecord.StartAddress And (&HFF)) 'Mask off Upper bytes
					calcCC = CUInt(calcCC + (myRecord.StartAddress And (&HFF00)) / &H100) 'Mask off lower bytes
					calcCC = CUInt(calcCC + myRecord.RecordType)
					If myRecord.DataLength <> 0 Then
						For i As Integer = 0 To myRecord.Data.Length - 1
							calcCC = calcCC + myRecord.Data(i)
						Next
					End If
					calcCC = CUInt(calcCC Mod 256)
					If calcCC <> 0 Then
						calcCC = CUInt(&H100 - calcCC)
					End If
					myRecord.CheckSum = CUShort(calcCC)
					'setup string to write to file and write it to file
					UploadStreamWriter.WriteLine(RecordToString(myRecord))
				Next
				UploadStreamWriter.Flush()
				returnValue = True
			End If
			Return returnValue

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		Finally
			If (UploadStreamWriter IsNot Nothing) Then
				UploadStreamWriter.Close()
			End If
		End Try
	End Function

	Public Function WriteMotorOffCommand(ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		'Dim testData() As Int32 = {2, 0, 0, 0, &HFF, &HFF}
		Try

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then

				blnResult = CumminsCommandOverride() ' turns motor off until a power cycle
				'blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, returnString)
				'blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.StartTestMode, testData, returnString)
			Else
				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteMotorOffCommand, returnString)
			End If

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Writing Motor Off Command"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteOverrideCommand(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteOverrideCommand, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Writing Override Command"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WritePCAID(ByVal msgData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WritePCAID, msgData, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Writing PCA ID"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteSpecialMemoryLine1(ByVal msgData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult As Boolean

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If

			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteSpecialMemoryLine1,
										 msgData, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Writing Special Memory Line1"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteSpecialMemoryLine2(ByVal msgData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult As Boolean

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If

			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteSpecialMemoryLine2,
										 msgData, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Writing Special Memory Line2"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteStatusUpdateTimeOverride(ByVal msgData() As Integer, ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteStatusUpdateTimeOverride,
										 msgData, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnResult = False Then
				Dim strErrorMessage = "Error Writing Status Update Time Override"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadHardwarePartNumberLowBytes(ByRef returnString As String) As Boolean
		Dim blnResult As Boolean
		Try
			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes

				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadHardwarePartNumberLowBytes,
									  ReturnStrFormat.HexByteValues, returnString)
			Else
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadHardwarePartNumberLowBytes,
									  ReturnStrFormat.Ascii, returnString)
			End If
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadHardwarePartNumberHighBytes(ByRef returnString As String) As Boolean

		Dim blnResult As Boolean
		Try
			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes

				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadHardwarePartNumberHighBytes,
									  ReturnStrFormat.HexByteValues, returnString)
			Else
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadHardwarePartNumberHighBytes,
									  ReturnStrFormat.Ascii, returnString)
			End If
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadChryslerHWVersionDate(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadChryslerHWVersionDate,
									  ReturnStrFormat.HexByteValues, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadChryslerHWVersionPatch(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadChryslerHWVersionPatch,
									  ReturnStrFormat.HexByteValues, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadAtmelUniqueID(ByVal enumSection As AtmelUniqueID, ByRef returnString As String) As Boolean

		Dim blnResult As Boolean = False

		Try
			Select Case enumSection
				Case AtmelUniqueID.Section0
					blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadAtmelUniqueID0,
									  ReturnStrFormat.HexByteValues, returnString)
				Case AtmelUniqueID.Section1
					blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadAtmelUniqueID1,
									  ReturnStrFormat.HexByteValues, returnString)
				Case AtmelUniqueID.Section2
					blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadAtmelUniqueID2,
									  ReturnStrFormat.HexByteValues, returnString)
				Case Else
					Dim strErrorMessage = "Unique ID section select value is out of range."
					mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									mStackTrace.CurrentFunctionName, strErrorMessage)
			End Select
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function ReadActuatorStepCount(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendReadMessage(clsCANMessage.MessageFunctions.ReadActuatorStepCount,
									  ReturnStrFormat.IntegerValue, returnString)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Private Function SendReadMessage(ByVal msgFunc As clsCANMessage.MessageFunctions,
								   ByVal returnType As ReturnStrFormat,
								   ByRef returnString As String) As Boolean
		Dim returnData(0) As Byte
		Dim CanMessage = New clsCANMessage

		Try
			'Load the CAN message.
			If Not mCANMessages.TryGetValue(msgFunc, CanMessage) Then
				Dim strErrorMessage = "Read Message Function Type Value Is Out of Range."
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				'CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0  ' ?????? XXX KTK
			End If

			'Send Message and Get Response
			Dim blnresult = SendMessageGetResponse(CanMessage, returnData)
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			'Parse the response.
			If blnresult Then
				Select Case returnType
					Case ReturnStrFormat.Ascii
						returnString = ReturnASCIIString(CanMessage, returnData)
					Case ReturnStrFormat.AsciiDotDelimited
						returnString = ReturnASCIIStringDotDelimitEveryTwoBytes(CanMessage, returnData)
					Case ReturnStrFormat.ContiguousBytes
						returnString = ReturnContiguousByteString(CanMessage, returnData)
					Case ReturnStrFormat.DotDelimited
						returnString = ReturnDotDelimitedString(CanMessage, returnData)
					Case ReturnStrFormat.HexByteValues
						returnString = ReturnHexString(CanMessage, returnData)
					Case ReturnStrFormat.IntegerValue
						returnString = ReturnIntegerValueString(CanMessage, returnData)
					Case ReturnStrFormat.MixedDotDelimited
						returnString = ReturnMixedASCIIStringDotDelimitEveryTwoBytes(CanMessage, returnData)
					Case Else
						Dim strErrorMessage = "Return String Type value is out of range."
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									  mStackTrace.CurrentFunctionName, strErrorMessage)
				End Select
			End If

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnresult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteSerialNumber(ByVal msgData() As Integer) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteSerialNumber, msgData)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function
	Public Function WriteHWPartNumberLowBytes(ByVal msgData() As Integer) As Boolean
		Dim blnResult As Boolean
		Try

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteHardwarePartNumberLowBytes, msgData)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteHWPartNumberHighBytes(ByVal msgData() As Integer) As Boolean

		Dim blnResult As Boolean
		Try

			Dim NotUsed As String

			If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If
				Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			End If
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteHardwarePartNumberHighBytes, msgData)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function WriteChryslerHWVersionInfo(ByVal msgData() As Integer) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteChryslerHWVersionInfo, msgData)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function SendMotorOffCommand(ByRef returnString As String) As Boolean

		Try
			' Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteMotorOffCommand, returnString)
			Dim blnResult = WriteMotorOffCommand(returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function SendBrakeMotorCommand(ByRef returnString As String) As Boolean

		Try
			Dim blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteMotorBrakeCommand, returnString)

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Private Function SendCommandMessage(ByVal msgFunc As clsCANMessage.MessageFunctions) As Boolean

		Dim msgData As Integer() = {}
		Return SendCommandMessage(msgFunc, msgData, Nothing)

	End Function

	Private Function SendCommandMessage(ByVal msgFunc As clsCANMessage.MessageFunctions,
									  ByVal msgData() As Integer) As Boolean

		Return SendCommandMessage(msgFunc, msgData, Nothing)

	End Function

	Private Function SendCommandMessage(ByVal msgFunc As clsCANMessage.MessageFunctions,
									  ByRef returnString As String) As Boolean

		Dim msgData As Integer() = {}
		Return SendCommandMessage(msgFunc, msgData, returnString)

	End Function

	Private Function SendCommandMessage(ByVal msgFunc As clsCANMessage.MessageFunctions,
									  ByVal msgData() As Integer,
									  ByRef returnString As String) As Boolean

		Dim blnresult As Boolean = False
		Dim CanMessage = New clsCANMessage
		Dim returnData(0) As Byte
		Dim intDynamicWriteByteIndex As Integer
		Dim intDataIndex As Integer

		Try

			'Load the CAN message.
			If Not mCANMessages.TryGetValue(msgFunc, CanMessage) Then
				Dim strErrorMessage = "Write Message Function Type value is out of range."
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				'CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0  ' ?????? XXX KTK
			End If

			If msgData.Length > 0 Then
				intDynamicWriteByteIndex = 0
				For intDataIndex = 0 To CanMessage.StaticData.Length - 1
					If CanMessage.StaticData(intDataIndex).ToUpper = "XX" Then
						If intDynamicWriteByteIndex > msgData.Length - 1 Then
							CanMessage.Data(intDataIndex) = 255 ' hex FF
						Else
							CanMessage.Data(intDataIndex) = msgData(intDynamicWriteByteIndex)
						End If
						intDynamicWriteByteIndex += 1
					End If
				Next
			End If

			If returnString IsNot Nothing AndAlso returnString <> "" Then
				'Send Message and get response.
				blnresult = SendMessageGetResponse(CanMessage, returnData)
			Else
				' Send message, no repsonse required.
				blnresult = SendMessageNoResponse(CanMessage)
			End If

			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If blnresult AndAlso returnString IsNot Nothing AndAlso returnString <> "" Then
				' In the Tool write/cmd case we're really only interested in the ACK/NACK byte.
				returnString = ReturnContiguousByteString(CanMessage, returnData)
			End If

			Return blnresult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

	Public Function EnableCovingtonBurnIn() As Boolean

		Dim blnResult As Boolean
		Dim CANMessageData(6) As Integer
		Dim ReturnString As String
		Dim NotUsed As String

		Dim RotationTime_ms As Int16 = 10000 '#10Seconds In forward And 10Seconds In Reverse direction
		Dim RotationTimeMSB As Int16
		Dim RotationTimeLSB As Int16

		Dim PhaseStepRate_ms As Int16 = 30 '#ms phase Step rate
		Dim PhaseStepRateMSB As Int16
		Dim PhaseStepRateLSB As Int16

		Dim ForwardCurrentLmtF As Decimal = 5.77 '#5.7amps current limit In forward direction
		Dim ForwardCurrentLmt As Int16
		Dim ForwardCurrentLmtMSB As Int16
		Dim ForwardCurrentLmtLSB As Int16

		Dim ReverseCurrentLmtF As Decimal = 3.3 '#3.5amps current limit In reverse direction
		Dim ReverseCurrentLmt As Int16
		Dim ReverseCurrentLmtMSB As Int16
		Dim ReverseCurrentLmtLSB As Int16

		Dim ForwardCurrentThreshold As Int16 = 2 '#2amps If current 
		Dim ForwardCurrentThresholdMSB As Int16
		Dim ForwardCurrentThresholdLSB As Int16

		Dim ReverseCurrentThresholdF As Decimal = 3.8 '#3.8amps
		Dim ReverseCurrentThreshold As Int16
		Dim ReverseCurrentThresholdMSB As Int16
		Dim ReverseCurrentThresholdLSB As Int16

		Dim ForwardPWM As Int16 = 17 '#%
		Dim ForwardPWM_MSB As Int16
		Dim ForwardPWM_LSB As Int16

		Dim ReversePWM As Int16 = 64 '#%
		Dim ReversePWM_MSB As Int16
		Dim ReversePWM_LSB As Int16
		'#dim BaseTime                = 2 '#2ms used In Embedded software

		Try
			If Not (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
				Return True
				Exit Function
			End If
			blnResult = WriteOverrideCommand(ReturnString)
			blnResult = SendReadMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2, ReturnStrFormat.DotDelimited, NotUsed)
			'lnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolMessagesG2) '0x11
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If
			Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes
			'need old serial number to enable this message
			blnResult = ReadSerialNumber(NotUsed) ' sets mstrSerialNumber from the device
			CANMessageData(3) = Convert.ToInt32(Mid(mstrSerialNumber, 1, 2), 16)
			CANMessageData(2) = Convert.ToInt32(Mid(mstrSerialNumber, 3, 2), 16)
			CANMessageData(1) = Convert.ToInt32(Mid(mstrSerialNumber, 5, 2), 16)
			CANMessageData(0) = Convert.ToInt32(Mid(mstrSerialNumber, 7, 2), 16)
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.EnableToolCommandDataWrite, CANMessageData) '0x91
			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If
			Thread.Sleep(3) 'allow time to take affect, 1 is too short, 2 works sometimes

			'#Rotation time For forward, Reverse
			'#RotationTime_ms = int(RotationTime_ms/BaseTime)
			RotationTimeMSB = RotationTime_ms >> 8
			RotationTimeLSB = RotationTime_ms And &HFF

			'#PhaseStepRate_ms = int(PhaseStepRate_ms/BaseTime)
			PhaseStepRateMSB = PhaseStepRate_ms >> 8
			PhaseStepRateLSB = PhaseStepRate_ms And &HFF

			ForwardCurrentLmt = Int(ForwardCurrentLmtF * 256)
			ForwardCurrentLmtMSB = ForwardCurrentLmt >> 8
			ForwardCurrentLmtLSB = ForwardCurrentLmt And &HFF


			ReverseCurrentLmt = Int(ReverseCurrentLmtF * 256)
			ReverseCurrentLmtMSB = ReverseCurrentLmt >> 8
			ReverseCurrentLmtLSB = ReverseCurrentLmt And &HFF


			ForwardCurrentThreshold = Int(ForwardCurrentThreshold * 256)
			ForwardCurrentThresholdMSB = ForwardCurrentThreshold >> 8
			ForwardCurrentThresholdLSB = ForwardCurrentThreshold And &HFF


			ReverseCurrentThreshold = Int(ReverseCurrentThresholdF * 256)
			ReverseCurrentThresholdMSB = ReverseCurrentThreshold >> 8
			ReverseCurrentThresholdLSB = ReverseCurrentThreshold And &HFF


			ForwardPWM = Int((ForwardPWM * 256) / 100)
			ForwardPWM_MSB = ForwardPWM >> 8
			ForwardPWM_LSB = ForwardPWM And &HFF


			ReversePWM = Int((ReversePWM * 256) / 100)
			ReversePWM_MSB = ReversePWM >> 8
			ReversePWM_LSB = ReversePWM And &HFF


			'# Setup the burn-In test Then start burn-In.
			'# Set the burn-In forward And reverse rotation And phase steps times.
			'SendCanMsg("18FF0E63, 8, 3F, 04, 00," + Format(RotationTimeLSB,'02x') + "," + format(RotationTimeMSB,'02x') + "," + format(PhaseStepRateLSB, '02x') + "," + format(PhaseStepRateMSB, '02x') + ", FF")
			CANMessageData(5) = &HFF
			CANMessageData(4) = PhaseStepRateMSB
			CANMessageData(3) = PhaseStepRateLSB
			CANMessageData(2) = RotationTimeMSB
			CANMessageData(1) = RotationTimeLSB
			CANMessageData(0) = 0
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteInternalTestTableEntry, CANMessageData)


			'# Set the burn-In forward And reverse current limits.
			'SendCanMsg("18FF0E63, 8, 3F, 04, 01," + Format(ForwardCurrentLmtLSB,'02x') + "," + format(ForwardCurrentLmtMSB,'02x') + "," + format(ReverseCurrentLmtLSB, '02x') + "," + format(ReverseCurrentLmtMSB, '02x') + ", FF")
			CANMessageData(4) = ReverseCurrentLmtMSB
			CANMessageData(3) = ReverseCurrentLmtLSB
			CANMessageData(2) = ForwardCurrentLmtMSB
			CANMessageData(1) = ForwardCurrentLmtLSB
			CANMessageData(0) = 1
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteInternalTestTableEntry, CANMessageData)

			'# Set the burn-In forward And reverse current Thresholds.
			'SendCanMsg("18FF0E63, 8, 3F, 04, 02," + Format(ForwardCurrentThresholdLSB,'02x') + "," + format(ForwardCurrentThresholdMSB,'02x') + "," + format(ReverseCurrentThresholdLSB, '02x') + "," + format(ReverseCurrentThresholdMSB, '02x') + ", FF")
			CANMessageData(4) = ReverseCurrentThresholdMSB
			CANMessageData(3) = ReverseCurrentThresholdLSB
			CANMessageData(2) = ForwardCurrentThresholdMSB
			CANMessageData(1) = ForwardCurrentThresholdLSB
			CANMessageData(0) = 2
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteInternalTestTableEntry, CANMessageData)

			'# Set the burn-In forward And reverse PWM.
			'SendCanMsg("18FF0E63, 8, 3F, 04, 03," + Format(ForwardPWM_LSB,'02x') + "," + format(ForwardPWM_MSB,'02x') + "," + format(ReversePWM_LSB, '02x') + "," + format(ReversePWM_MSB, '02x') + ", FF")
			CANMessageData(4) = ReversePWM_MSB
			CANMessageData(3) = ReversePWM_LSB
			CANMessageData(2) = ForwardPWM_MSB
			CANMessageData(1) = ForwardPWM_LSB
			CANMessageData(0) = 3
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteInternalTestTableEntry, CANMessageData)

			'# Write the remaining internal test bytes To 0.
			'      For i in range(4,16)
			'        SendCanMsg("18FF0E63, 8, 3F, 04," + Format(i, 'x') + ", 00, 00, 00, 00, FF")
			'        time.sleep(0.1)
			CANMessageData(4) = 0
			CANMessageData(3) = 0
			CANMessageData(2) = 0
			CANMessageData(1) = 0
			Dim i As Int16
			For i = 4 To 16
				CANMessageData(0) = i
				blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteInternalTestTableEntry, CANMessageData)
			Next

			''# Write test configuration bytes.
			'    SendCanMsg("18FF0E63, 8, 3E, 01, 82, 02, 32, 01, 82, 02")
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteInternalTestConfigurationBytes)

			''# Command the override state.
			'    SendCanMsg("18FF0E63, 8, F4, 01, 06, FF, FF, FF, FF, FF")
			''#Enable tool functions.
			'    SendCanMsg("18FF0E63, 8, 10, 02, 11, 1A, FF, FF, FF, FF")
			''#Enable To commanded EEPROM write.
			'    SendCanMsg("18FF0E63, 8, 10, FF, 91, FF, FF, FF, FF, FF")
			''# Write internal test configuration And test table To EEPROM.
			'    SendCanMsg("18FF0E63, 8, 10, 02, E9, 80, FF, FF, FF, FF")
			blnResult = SendCommandMessage(clsCANMessage.MessageFunctions.WriteInternalTestSequenceToEEProm)

			'#    time.sleep(3)
			'#    TurnIgnOff()
			'#    time.sleep(2)
			'#    TurnIgnOn()
			'#    time.sleep(.5)
			'#    # Enable tool functions.
			'#    SendCanMsg("18FF0E63, 8, 10, 02, 11, 1A, FF, FF, FF, FF")
			'#    # Activate internal test table.
			'#    SendCanMsg("18FF0E63, 8, 10, 01, E5, FF, FF, FF, FF, FF")

			'      except KeyboardInterrupt, e
			'# You must Catch this exception To allow the user,
			'# If necessaryThen, To cleanly terminate this script.
			'sys.Exit(0)


			If mAnomaly IsNot Nothing Then
				Throw New TsopAnomalyException
			End If

			If Not blnResult Then
				Dim strErrorMessage = "Error Entering Burn In mode"
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
								  mStackTrace.CurrentFunctionName, strErrorMessage)
				Throw New TsopAnomalyException
			End If

			Return blnResult

		Catch ex As TsopAnomalyException
			mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Return False
		Catch ex As Exception
			mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			Return False
		End Try
	End Function

#End Region

#Region "Class clsCANMessage"
	Public Class clsCANMessage

#Region "Private Member Variables ============================================================="

		Private mintDataLength As Integer
		Private mintDecodingStartByte As Integer
		Private mintDecodingByteLength As Integer
		'Private mintDecodingMethod As DecodingMethods
		Private mbytData(7) As Byte
		'Private mintDynamicWriteByteCount As Integer

		'Private mintFrameRateMilliseconds As Integer
		'Private mintFieldsToReturn As Integer

		Private mintMessageFunction As MessageFunctions
		'Private mintMessageType As MessageTypes

		'Private mintNumberOfTimesToSendMessage As Integer

		Private mOpCode As UInteger

		'Private mintReceiveArbitrationIDMax As Integer
		'Private mintReceiveArbitrationIDMin As Integer
		'Private mintReturnFieldBytePositions() As Integer

		Private mblnReverseByteOrder As Boolean

		Private mintSendArbitrationID As Integer

		Private mstrStaticData(7) As String

#End Region

#Region "Public Constants, Structures, Enums =================================================="

		'Public Enum DecodingMethods As Integer
		'  None = 0
		'  ReturnHexString = 1
		'  ReturnASCIIStringDotDelimitEveryTwoBytes = 2
		'  ReturnCommaDelimitedString = 3
		'  ReturnASCIIString = 4
		'  ReturnContiguousByteString = 5
		'End Enum

		Public Enum MessageFunctions
			ReadCustomerBootloaderVersion = 0
			ReadDefaultBootloaderVersion = 1
			ReadHardwarePartNumberHighBytes = 2
			ReadHardwarePartNumberLowBytes = 3
			ReadProgramChecksum = 4
			ReadSerialNumber = 5
			ReadSpecialMemoryLine1 = 6
			ReadSpecialMemoryLine2 = 7
			ReadSoftwareVersion = 8
			ReadVendorPCB = 9
			ResetSpecialMemoryLine1 = 10
			ResetSpecialMemoryLine2 = 11
			WriteSpecialMemoryLine1 = 12
			WriteSpecialMemoryLine2 = 13
			JumpToDefaultBootloader = 14
			DefaultBootloaderAttnRequest = 15
			DefaultBootloaderAttnResponse = 16
			DefaultBootloaderInitMemoryReadRequest = 17
			DefaultBootloaderInitMemoryReadAck = 18
			DefaultBootloaderMemoryReadData = 19
			DefaultBootloaderMemoryReadDataAck = 20
			DefaultBootLoaderFaultResponse = 21
			DefaultBootloaderCloseSessionCmd = 22
			DefaultBootloaderCloseSessionAck = 23
			DefaultBootloaderStartApplication = 24
			ReadPowerCycleCounts = 25
			ReadRunTime = 26
			ResetEEPromAll = 27
			ResetEEPromPowerCycles = 28
			ResetEEPromRunTime = 29
			ReadSpan = 30
			ResetEEPromSpan = 31
			ReadFaultCount = 32
			ResetActuator = 33
			DisableZenCorrection = 34
			DisablePositionCommandTimeout = 35
			ReadTempHWM = 36
			ReadBurnInCycles = 37
			ReadBurnInTime = 38
			ReadAPSType = 39
			WriteStatusUpdateTimeOverride = 40
			APSCalibrate = 41
			ReadAPSCalibrateProgress = 42
			WriteOverrideCommand = 43
			ReadInternalTemperature = 44
			ReadBatteryVoltage = 45
			DefaultBootloaderInitMemoryWriteRequest = 46
			DefaultBootloaderInitMemoryWriteAck = 47
			DefaultBootloaderMemoryWriteData = 48
			DefaultBootloaderMemoryWriteDataAck = 49
			ReadProgramChecksumInteger = 50
			CumminsCommandPosition = 51
			ReadAPSCountValue = 52
			PhaseStepMessage1 = 53
			PhaseStepMessage2 = 54
			CumminsCommandOverride = 55
			ValveCalibrate = 56
			CumminsClearStops = 57
			StartContinuousMove = 58
			EnableToolMessagesG2 = 59
			StartContinuousMoveG2 = 60
			StopTestMode = 61
			ReadSerialNumberG2 = 62
			ReadSoftwareVersionG2 = 63
			ReadFlashChecksum = 64
			RequestPGNForAddressClaim = 65
			ResetSourceID = 66
			ModifySourceID = 67
			CumminsCommandSelfCalibrate = 68
			ReadBurnInCyclesMfgSw = 69
			ResetSerialNumberHighByte = 70
			ReadTempLWM = 71
			WriteMotorOffCommand = 72
			WritePCAID = 73
			WriteHardwarePartNumberHighBytes = 74
			WriteHardwarePartNumberLowBytes = 75
			ReadChryslerHWVersionDate = 76
			WriteChryslerHWVersionInfo = 77
			ReadAtmelUniqueID0 = 78
			ReadAtmelUniqueID1 = 79
			ReadAtmelUniqueID2 = 80
			WriteMotorBrakeCommand = 81
			ReadActuatorStepCount = 82
			ReadChryslerHWVersionPatch = 83
			RequestDIDForECUVariantID = 84
			RequestDIDForECUVariantIDAppResponse = 85
			RequestDIDForECUVariantIDCcblResponse = 86
			CommandTransferToProgrammingSession = 87
			CommandTransferToProgrammingSessionResponse = 88
			WriteSerialNumber = 89
			StartTestMode = 90
			EnableToolCommandDataWrite = 91
			EnableMotorTestMode = 92
			ReadInternalTestTableEntry = 93
			WriteInternalTestTableEntry = 94
			WriteInternalTestSequenceToEEProm = 95
			WriteInternalTestConfigurationBytes = 96
			CANCommandMessage = 97
			ReadEEPROMGenBRequest = 98
			UDSByteOrderDID = 99
			UDSECUVariantDID = 100
			UDSB834DID = 101
			UDSDiagnosticProgramSession = 102
			UDSTesterPresent = 103
			UDSSecurityAccess = 104
			UDSSendKey = 105
			UDSKeyDataMessage1 = 106
			UDSKeyDataMessage2 = 107
			UDSEraseMemory = 108
			UDSRequestDownload = 109
			UDSSendBytesToDownload = 110
			UDSStartDataTransferByBlock = 111
			UDSAckDataTransferByBlock = 112
			UDSExitDataTransfer = 113
			UDSCheckDownloadCRC = 114
			UDSReset = 115
			ReadSerialNumberFromBL = 116
			WriteEEPROMGenBRequest = 117


		End Enum

		'Public Enum MessageTypes As Integer
		'  Tool = 0
		'End Enum

#End Region

#Region "Properties ==========================================================================="

		Public Property DataLength As Integer
			Get
				Return mintDataLength
			End Get
			Set(value As Integer)
				mintDataLength = value
			End Set
		End Property

		Public Property DecodingByteLength As Integer
			Get
				Return Me.mintDecodingByteLength
			End Get
			Set(value As Integer)
				Me.mintDecodingByteLength = value
			End Set
		End Property

		Public Property DecodingStartByte As Integer
			Get
				Return Me.mintDecodingStartByte
			End Get
			Set(value As Integer)
				Me.mintDecodingStartByte = value
			End Set
		End Property

		'Public Property DecodingMethod As DecodingMethods
		'  Get
		'    Return Me.mintDecodingMethod
		'  End Get
		'  Set(value As DecodingMethods)
		'    Me.mintDecodingMethod = value
		'  End Set
		'End Property

		Public Property Data() As Byte()
			Get
				Return Me.mbytData
			End Get
			Set(value As Byte())
				Me.mbytData = value
			End Set
		End Property

		'Public Property DynamicWriteByteCount As Integer
		'  Get
		'    Return mintDynamicWriteByteCount
		'  End Get
		'  Set(value As Integer)
		'    mintDynamicWriteByteCount = value
		'  End Set
		'End Property


		'Public Property FrameRateMilliseconds As Integer
		'  Get
		'    Return mintFrameRateMilliseconds
		'  End Get
		'  Set(value As Integer)
		'    mintFrameRateMilliseconds = value
		'  End Set
		'End Property

		'Public Property FieldsToReturn As Integer
		'  Get
		'    Return mintFieldsToReturn
		'  End Get
		'  Set(value As Integer)
		'    mintFieldsToReturn = value
		'  End Set
		'End Property

		Public Property MessageFunction As MessageFunctions
			Get
				Return mintMessageFunction
			End Get
			Set(value As MessageFunctions)
				mintMessageFunction = value
			End Set
		End Property

		'Public Property MessageType As MessageTypes
		'  Get
		'    Return mintMessageType
		'  End Get
		'  Set(value As MessageTypes)
		'    mintMessageType = value
		'  End Set
		'End Property


		'Public Property NumberOfTimesToSendMessage As Integer
		'  Get
		'    Return mintNumberOfTimesToSendMessage
		'  End Get
		'  Set(value As Integer)
		'    mintNumberOfTimesToSendMessage = value
		'  End Set
		'End Property


		Public Property OpCode As UInteger
			Get
				Return mOpCode
			End Get
			Set(value As UInteger)
				mOpCode = value
			End Set
		End Property


		'Public Property ReceiveArbitrationIDMax As Integer
		'  Get
		'    Return mintReceiveArbitrationIDMax
		'  End Get
		'  Set(value As Integer)
		'    mintReceiveArbitrationIDMax = value
		'  End Set
		'End Property

		'Public Property ReceiveArbitrationIDMin As Integer
		'  Get
		'    Return mintReceiveArbitrationIDMin
		'  End Get
		'  Set(value As Integer)
		'    mintReceiveArbitrationIDMin = value
		'  End Set
		'End Property

		'Public Property ReturnFieldBytePositions() As Integer()
		'  Get
		'    Return Me.mintReturnFieldBytePositions
		'  End Get
		'  Set(value As Integer())
		'    Me.mintReturnFieldBytePositions = value
		'  End Set
		'End Property

		Public Property ReverseByteOrder As Boolean
			Get
				Return mblnReverseByteOrder
			End Get
			Set(value As Boolean)
				mblnReverseByteOrder = value
			End Set
		End Property


		Public Property SendArbitrationID As Integer
			Get
				Return mintSendArbitrationID
			End Get
			Set(value As Integer)
				mintSendArbitrationID = value
			End Set
		End Property

		Public Property StaticData() As String()
			Get
				Return Me.mstrStaticData
			End Get
			Set(value As String())
				Me.mstrStaticData = value
			End Set
		End Property



#End Region

#Region "Public Methods ======================================================================="

#End Region

#Region "Private Methods ======================================================================"

#End Region

	End Class
#End Region  ' ===============================================================

#Region "Class clsCanMsgHandling"
	Public Class clsCanMsgHandling

#Region "Constructor"
		Protected Sub New()
			Dim strErrorMessage As String
			Dim CounterFreq As Long
			Try

				' Get the frequency of the Performance Counter
				QueryPerformanceFrequency(CounterFreq)
				' Store the period of the Performance Counter * 1000
				CounterPeriod = (1.0 / CDbl(CounterFreq)) * 1000

				' Get an instance of the receive CAN message delegate class.
				mRcvMsgDelegates = clsCANActuator.clsRcvCanMsgDelegates.GetInstance()
				If mRcvMsgDelegates Is Nothing Then
					strErrorMessage = "Error Creating Instance of clsRcvCanMsgDelegates Class"
					mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
					Throw New TsopAnomalyException
				End If

				CreateThreads()
				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub
#End Region

#Region "Private Member Variables"
		Private mAnomaly As clsAnomaly
		Private mStopwatch As New clsStopWatch
		Private mStackTrace As New clsStackTrace
		Private mstrNICanInterfaceName As String
		Private mintCanInterfaceMfg As CanInterfaceMfgEnum
		Private mintPeakCanHandle As Byte

		' Private single instance of this class.
		Private Shared _CanMsgHandlingInstance As clsCanMsgHandling

		Private mstrCANMessageLogFilePath As String

		Private mblnCanMessageLoggingEnabled As Boolean
		Private mblnCanActuatorActivityLoggingEnabled As Boolean

		' Receive CAN message queued event wait handle
		Private mRcvCanMsgDeliveryWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
		' Receive CAN message delivery thread
		Private mRcvCanMsgDeliveryThread As Thread
		' Transmit CAN message queued event wait handle
		Private mXmtCanMsgWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
		' Transmit CAN message thread
		Private mXmtCanMessageThread As Thread
		' Receive CAN message delivery thread
		Private mQueueRcvdCanMsgThread As Thread
		' Received CAN message queue
		'Private mRcvdCanMessageQueue As New System.Collections.Concurrent.ConcurrentQueue(Of CAN_Msg_Struct)
		Private mRcvdCanMessageQueue As New System.Collections.Concurrent.ConcurrentQueue(Of RcvdCANMsg)
		' CAN message output queue.
		Private mCanMsgOutputQueue As New System.Collections.Concurrent.ConcurrentQueue(Of clsCANActuator.clsCANMessage) 'TER

		'CAN Logging Output Queue
		Private mCanMsgLogQueue As New System.Collections.Concurrent.ConcurrentQueue(Of String) 'TER
		' CAN Logging event wait handle
		Private mCanLogWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
		' CAN Logging thread
		Private mCanLogThread As Thread
		Private mblnTerminateCanLogThread As Boolean = False
		Private mCanLogStartTime As Double



		' Multicast delegates of delegate subs that have been registered to
		' be called when corresponding messages are received and methods to access them.
		'Private Shared RcvdMsgDelegates As RcvCanMsgDelegates'TER
		Private mRcvMsgDelegates As clsCANActuator.clsRcvCanMsgDelegates
		' True when CAN and message handling threads have been started.
		Private mblnCanStarted As Boolean = False
		' NiCAN attributes
		Private mintObjectHandle As Integer
		Private mPCAN_DeviceHandle As Byte = PCANBasic.PCAN_NONEBUS

		Private mintCurrentState As Integer
		' Flag used to terminate thread procs.
		Private mblnTerminateRcvCanMsgDeliverThread As Boolean = False
		Private mblnTerminateXmtCanMsgThread As Boolean = False
		Private mblnTerminateQueueRcvdCanMsgThread As Boolean = False
		Private mNonDecimal As New Regex("[^0-9]+")

		' Elapsed Time Counter (high resolution)
		Private Declare Function QueryPerformanceCounter Lib "Kernel32.dll" (ByRef lpPerformanceCount As Long) As Boolean
		Private Declare Function QueryPerformanceFrequency Lib "Kernel32.dll" (ByRef lpFrequency As Long) As Boolean
		Private HP_CounterValue As Long
		'Private StartingTime As Double
		'Private CurrentTime As Double
		Private CounterPeriod As Double

		Private mCanInterfaceMfg As CanInterfaceMfgEnum

#End Region

#Region "Public Structures and Enums"
		Public Structure CAN_Msg_Struct

			' Contains received CAN message, corresponding time-stamp
			' and corresponding properties.
			Private _CanMsg As TPCANMsg
			'Private _Timestamp As nican.NCTYPE_UINT64
			Private _Timestamp As CAN_MsgTimestamp

			Private _msgCount As UShort

			'Public Sub New(ByVal msg As TPCANMsg, ByVal timestamp As nican.NCTYPE_UINT64)
			'  _CanMsg = msg
			'  _Timestamp = timestamp
			'  _msgCount = 0
			'End Sub

			Public Sub New(ByVal msg As TPCANMsg, ByVal timestamp As CAN_MsgTimestamp)
				_CanMsg = msg
				_Timestamp = timestamp
				_msgCount = 0
			End Sub

			Public ReadOnly Property CanMsg() As TPCANMsg
				Get
					Return _CanMsg
				End Get
			End Property

			'Public ReadOnly Property Timestamp() As nican.NCTYPE_UINT64
			'  Get
			'    Return _Timestamp
			'  End Get
			'End Property

			Public ReadOnly Property Timestamp() As CAN_MsgTimestamp
				Get
					Return _Timestamp
				End Get
			End Property


			Public Property msgCount() As UShort
				Get
					Return _msgCount
				End Get
				Set(ByVal value As UShort)
					_msgCount = value
				End Set
			End Property
		End Structure

		''' <summary>
		''' Represents a CAN message Timestamp
		''' </summary>
		Public Structure CAN_MsgTimestamp

			'Private PeakTimeStamp As Peak.Can.Basic.TPCANTimestamp
			Private PeakTimeStamp As TPCANTimestamp
			Private NI_TimeStampOvflw As UInteger

			'Public Sub New(ByVal tpc_time As Peak.Can.Basic.TPCANTimestamp, Optional ByVal nic_time_ovflw As UInteger = 0)
			Public Sub New(ByVal tpc_time As TPCANTimestamp, Optional ByVal nic_time_ovflw As UInteger = 0)
				PeakTimeStamp = tpc_time
				NI_TimeStampOvflw = nic_time_ovflw
			End Sub

			'Public ReadOnly Property PCAN_MsgTimeStamp() As Peak.Can.Basic.TPCANTimestamp
			Public ReadOnly Property PCAN_MsgTimeStamp() As TPCANTimestamp
				Get
					Return PeakTimeStamp
				End Get
			End Property

			Public ReadOnly Property MiCAN_MsgTimeStampOvflw() As UInteger
				Get
					Return NI_TimeStampOvflw
				End Get
			End Property
		End Structure


		Public Structure RcvdCANMsg
			Private Message As TPCANMsg
			Private TimeStamp As CAN_MsgTimestamp
			Public Sub New(ByVal Msg As TPCANMsg, ByVal Time As CAN_MsgTimestamp)
				Message = Msg
				TimeStamp = Time
			End Sub
			Public ReadOnly Property CANMsg() As TPCANMsg
				Get
					Return Message
				End Get
			End Property
			Public ReadOnly Property CANMsgTimeStamp() As CAN_MsgTimestamp
				Get
					Return TimeStamp
				End Get
			End Property
		End Structure


		Public Enum CanInterfaceMfgEnum As Integer
			NI = 0
			Peak = 1
		End Enum

#End Region

#Region "Properties"
		Public Property Anomaly() As clsAnomaly
			Get
				Return mAnomaly
			End Get
			Set(ByVal value As clsAnomaly)
				mAnomaly = value
			End Set
		End Property

		Public Property CanActuatorActivityLoggingEnabled As Boolean
			Get
				Return mblnCanActuatorActivityLoggingEnabled
			End Get
			Set(value As Boolean)
				mblnCanActuatorActivityLoggingEnabled = value
			End Set
		End Property

		Public Property CanInterfaceMfg As CanInterfaceMfgEnum
			Get
				Return mintCanInterfaceMfg
			End Get
			Set(value As CanInterfaceMfgEnum)
				mintCanInterfaceMfg = value
			End Set
		End Property

		Public Property NICanInterfaceName As String
			Get
				Return Me.mstrNICanInterfaceName
			End Get
			Set(value As String)
				mstrNICanInterfaceName = value
			End Set
		End Property

		Public Property PeakCanHandle As Byte
			Get
				Return mintPeakCanHandle
			End Get
			Set(value As Byte)
				mintPeakCanHandle = value
			End Set
		End Property

		Public Property CANMessageLogFilePath As String
			Get
				Return mstrCANMessageLogFilePath
			End Get
			Set(value As String)
				mstrCANMessageLogFilePath = value
			End Set
		End Property

		Public Property CanMessageLoggingEnabled As Boolean
			Get
				Return mblnCanMessageLoggingEnabled
			End Get
			Set(value As Boolean)
				mblnCanMessageLoggingEnabled = value
			End Set
		End Property

		Public ReadOnly Property ObjectHandle() As Integer
			Get
				Return mintObjectHandle
			End Get
		End Property

		Public ReadOnly Property RcvMsgDelegates As clsCANActuator.clsRcvCanMsgDelegates
			Get
				Return mRcvMsgDelegates
			End Get
		End Property

#End Region

#Region "Public Methods"

		Public Sub Delay(ByVal time As UInteger)
			Dim dblStartTime As Double
			Dim dblCurrentTime As Double

			Try
				' Initialize the elapsed time starting value.
				QueryPerformanceCounter(HP_CounterValue)
				dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
				Do
					' Retrieve the current counter value.
					QueryPerformanceCounter(HP_CounterValue)
					' Calculate the current time
					dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
					System.Windows.Forms.Application.DoEvents()
					Threading.Thread.Sleep(1)
				Loop While (dblCurrentTime - dblStartTime < time)

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Public Shared Function GetInstance() As clsCanMsgHandling 'TER
			Try
				' Only one instance of this class (singleton) is allowed.
				' Initialize single instance of this class if it hasn't already been done.
				If (_CanMsgHandlingInstance Is Nothing) Then
					_CanMsgHandlingInstance = New clsCanMsgHandling
					If _CanMsgHandlingInstance.Anomaly IsNot Nothing Then
						_CanMsgHandlingInstance = Nothing
						Throw New TsopAnomalyException
					End If
				End If

				Return _CanMsgHandlingInstance

			Catch ex As Exception
				Return Nothing
			End Try
		End Function

		'Public Sub LogCanMessage(strMessage)
		'  Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing
		'  Dim strFileName As String
		'  Dim strDate As String
		'  Try
		'    If mblnCanMessageLoggingEnabled Then
		'      strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")

		'      strFileName = mstrCANMessageLogFilePath & "\CanMessageLog.txt"
		'      CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
		'      CanMessageLogStreamWriter.WriteLine(strDate & ", " & strMessage)
		'      CanMessageLogStreamWriter.Flush()
		'      CanMessageLogStreamWriter.Close()
		'    End If

		'  Catch ex As TsopAnomalyException
		'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		'  Catch ex As Exception
		'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		'  End Try
		'End Sub

		'Public Sub LogCanMessage()
		'  Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing
		'  Dim strFileName As String
		'  Dim strDate As String
		'  Dim strMessage As String = ""
		'  Dim blnDequeued As Boolean
		'  Try
		'    If mblnCanMessageLoggingEnabled Then
		'      strFileName = mstrCANMessageLogFilePath & "\CanMessageLog.txt"
		'      If mCanMsgLogQueue.Count <> 0 Then
		'        Do
		'          strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")
		'          blnDequeued = mCanMsgLogQueue.TryDequeue(strMessage)
		'          If blnDequeued Then
		'            CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
		'            CanMessageLogStreamWriter.WriteLine(strDate & ", " & strMessage)
		'          End If
		'        Loop While mCanMsgLogQueue.Count > 0
		'        CanMessageLogStreamWriter.Flush()
		'        CanMessageLogStreamWriter.Close()
		'      End If

		'    End If

		'  Catch ex As TsopAnomalyException
		'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		'  Catch ex As Exception
		'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		'  End Try
		'End Sub


		Public Sub LogCanActuatorActivity(strMessage)
			Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing
			Dim strFileName As String
			Dim strDate As String
			Try
				If mblnCanActuatorActivityLoggingEnabled Then

					strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")

					strFileName = mstrCANMessageLogFilePath & "\CanActuatorActivityLog.txt"
					CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
					CanMessageLogStreamWriter.WriteLine(strDate & ", " & strMessage)
					CanMessageLogStreamWriter.Flush()
					CanMessageLogStreamWriter.Close()
				End If

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Public Sub ReleaseCAN()

			Try

				Select Case mintCanInterfaceMfg
					Case CanInterfaceMfgEnum.NI
						If (mintObjectHandle <> 0) Then
							nican.ncCloseObject(mintObjectHandle)
						End If
					Case CanInterfaceMfgEnum.Peak
						If (mPCAN_DeviceHandle <> 0) Then
							PCANBasic.Uninitialize(mPCAN_DeviceHandle)
						End If
				End Select

				mblnCanStarted = False

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Public Function StartCAN() As Boolean
			Dim blnReturnValue As Boolean = False
			Try

				mblnCanStarted = False

				Select Case mintCanInterfaceMfg
					Case CanInterfaceMfgEnum.NI
						Call StartNICAN(mstrNICanInterfaceName)
					Case CanInterfaceMfgEnum.Peak
						Call StartPCAN(mintPeakCanHandle)
				End Select

				If mAnomaly IsNot Nothing Then
					Throw New TsopAnomalyException
				End If

				' The CAN device was started successfully.
				' Start reading and delivering received CAN messages.
				If (mRcvCanMsgDeliveryThread.ThreadState = ThreadState.Unstarted) Then
					mRcvCanMsgDeliveryThread.Start()
					' Start transmitting CAN messages.
					If (mXmtCanMessageThread.ThreadState = ThreadState.Unstarted) Then
						mXmtCanMessageThread.Start()
						If (mQueueRcvdCanMsgThread.ThreadState = ThreadState.Unstarted) Then
							mQueueRcvdCanMsgThread.Start()
							mblnCanStarted = True
							blnReturnValue = True
						End If
					End If
				End If

				Return blnReturnValue

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
				Return False
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
				Return False
			End Try
		End Function

		Public Function StartNICAN(ByVal strCANInterfaceName As String) As Boolean

			Dim returnValue As Boolean = False
			Dim attr_list(0) As Integer '= {nican.NC_ATTR_BAUD_RATE}
			Dim attr_value_list(0) As Integer '= {nican.NC_BAUD_250K}
			Dim status As Integer
			Dim strErrorMessage As String

			Try

				mblnCanStarted = False

				Call GetCANConfigSettings(attr_list, attr_value_list)

				If ((strCANInterfaceName <> "") AndAlso (strCANInterfaceName.Length > 3) AndAlso
			(strCANInterfaceName.StartsWith("CAN") = True) AndAlso
			(mNonDecimal.IsMatch(strCANInterfaceName.Substring(strCANInterfaceName.IndexOf("N"c) + 1)) = False) AndAlso
			(strCANInterfaceName.Substring(strCANInterfaceName.IndexOf("N"c) + 1) < 64)) Then
					' Option the USB NiCAN device.
					status = nican.ncConfig(strCANInterfaceName, 1, attr_list(0), attr_value_list(0))
					'status = nican.ncConfig(strCANInterfaceName, 8, attr_list(0), attr_value_list(0))
					If (status = nican.NC_SUCCESS) Then
						status = nican.ncOpenObject(strCANInterfaceName, mintObjectHandle)
						If (status = nican.NC_SUCCESS) Then
							' Reset the USB NiCAN device to clear errors.
							status = nican.ncAction(mintObjectHandle, nican.NC_OPCODE_ENUM.NC_OP_RESET, 0)
							If (status = nican.NC_SUCCESS) Then
								Delay(500) 'TER
								'mStopwatch.DelayTime(500)
								' Start the USB NiCAN device.
								status = nican.ncAction(mintObjectHandle, nican.NC_OPCODE_ENUM.NC_OP_START, 0)
								If (status = nican.NC_SUCCESS) Then
									' The USB NiCAN device was started successfully.
									' Start reading and delivering received CAN messages.
								Else
									' Failed to start the CAN device.
									'finalRslt = FinalResult.CanDeviceError5
									strErrorMessage = "Failed To Start The CAN Device"
									mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
											mStackTrace.CurrentFunctionName, strErrorMessage)
									Throw New TsopAnomalyException
								End If
							Else
								' Failed to reset the CAN device.
								'finalRslt = FinalResult.CanDeviceError4
								strErrorMessage = "Failed To Reset The CAN Device"
								mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
										  mStackTrace.CurrentFunctionName, strErrorMessage)
								Throw New TsopAnomalyException
							End If
						Else
							' Failed to open the CAN device.
							'finalRslt = FinalResult.CanDeviceError3
							strErrorMessage = "Failed To Open The CAN Device"
							mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
										mStackTrace.CurrentFunctionName, strErrorMessage)
							Throw New TsopAnomalyException
						End If
					Else
						' Failed to set the CAN device baud rate.
						'finalRslt = FinalResult.CanDeviceError2
						strErrorMessage = "Failed To Configure CAN Device"
						mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									  mStackTrace.CurrentFunctionName, strErrorMessage)
						Throw New TsopAnomalyException
					End If
				Else
					' CAN device object name is invalid.
					'finalRslt = FinalResult.CanDeviceError1
					strErrorMessage = "CAN Device Name Is Invalid"
					mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									mStackTrace.CurrentFunctionName, strErrorMessage)
					Throw New TsopAnomalyException
				End If

				Return returnValue

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
				Return False
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
				Return False
			End Try
		End Function

		Public Function StartPCAN(ByVal handle As Byte) As Boolean
			Dim returnValue As Boolean = False
			Dim Result As TPCANStatus = TPCANStatus.PCAN_ERROR_UNKNOWN
			Dim strErrorMessage As String
			Try
				'mPCAN_DeviceHandle = CType(_PCAN_Devices(PCAN_HandleStr(handle)), PCAN_Device).Handle
				mPCAN_DeviceHandle = handle
				'Result = PCANBasic.Initialize(_PCAN_DeviceHandle, _PCAN_BaudRate)
				Result = PCANBasic.Initialize(mPCAN_DeviceHandle, TPCANBaudrate.PCAN_BAUD_250K)
				If (Result = TPCANStatus.PCAN_ERROR_OK) Then
					' The Peak CAN device was started successfully.
					' Start reading and delivering received CAN messages.
					returnValue = True
				Else
					' Failed to start the CAN device.
					strErrorMessage = "Failed To Start The Peak CAN Device"
					mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName,
									mStackTrace.CurrentFunctionName, strErrorMessage)
					Throw New TsopAnomalyException
				End If

				Return returnValue

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
				Return False
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
				Return False
			End Try
		End Function


		Public Sub ShutDown()
			Try
				' If necessary, abort the CAN message threads.
				If ((mRcvCanMsgDeliveryThread IsNot Nothing) AndAlso
			(mRcvCanMsgDeliveryThread.ThreadState <> ThreadState.AbortRequested) AndAlso
			(mRcvCanMsgDeliveryThread.ThreadState <> ThreadState.Aborted)) Then

					mblnTerminateRcvCanMsgDeliverThread = True
					mRcvCanMsgDeliveryWaitHandle.Set()

					If mRcvCanMsgDeliveryThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
						While (mRcvCanMsgDeliveryThread.ThreadState <> System.Threading.ThreadState.Stopped)
							Application.DoEvents()
							Thread.Sleep(1)
						End While
					End If
					'RcvCanMsgDeliveryThread.Abort()
					mRcvCanMsgDeliveryThread = Nothing
				End If

				If ((mXmtCanMessageThread IsNot Nothing) AndAlso
			(mXmtCanMessageThread.ThreadState <> ThreadState.AbortRequested) AndAlso
			(mXmtCanMessageThread.ThreadState <> ThreadState.Aborted)) Then

					mblnTerminateXmtCanMsgThread = True
					mXmtCanMsgWaitHandle.Set()

					If mXmtCanMessageThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
						While (mXmtCanMessageThread.ThreadState <> System.Threading.ThreadState.Stopped)
							Application.DoEvents()
							Thread.Sleep(1)
						End While
					End If
					'XmtCanMessageThread.Abort()
					mXmtCanMessageThread = Nothing
				End If

				'If ((mQueueRcvdCanMsgThread IsNot Nothing) AndAlso (mQueueRcvdCanMsgThread.IsAlive = True)) Then
				If ((mQueueRcvdCanMsgThread IsNot Nothing)) Then

					mblnTerminateQueueRcvdCanMsgThread = True

					'While (QueueRcvdCanMsgThread.ThreadState <> ThreadState.Stopped)
					'End While
					If mQueueRcvdCanMsgThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
						While (mQueueRcvdCanMsgThread.ThreadState <> System.Threading.ThreadState.Stopped)
							Application.DoEvents()
							Thread.Sleep(1)
						End While
					End If
					'QueueRcvdCanMsgThread.Abort()
					mQueueRcvdCanMsgThread = Nothing
				End If

				Call StopCanLoggingThread()

				' Release the USB NiCAN device.
				ReleaseCAN()

				_CanMsgHandlingInstance = Nothing

				mblnTerminateRcvCanMsgDeliverThread = False
				mblnTerminateXmtCanMsgThread = False
				mblnTerminateQueueRcvdCanMsgThread = False

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Public Sub StartCanLoggingThread()
			Try

				If (mCanLogThread.ThreadState = ThreadState.Unstarted) Then
					mCanLogThread.Start()
				End If

				'' Initialize the elapsed time starting value.
				'QueryPerformanceCounter(HP_CounterValue)
				'mCanLogStartTime = CDbl(HP_CounterValue) * CounterPeriod
				mCanLogStartTime = 0


			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Public Sub StopCanLoggingThread()
			Try

				If ((mCanLogThread IsNot Nothing) AndAlso
			(mCanLogThread.ThreadState <> ThreadState.AbortRequested) AndAlso
			(mCanLogThread.ThreadState <> ThreadState.Aborted)) Then

					mblnTerminateCanLogThread = True
					mCanLogWaitHandle.Set()

					If mCanLogThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
						While (mCanLogThread.ThreadState <> System.Threading.ThreadState.Stopped)
							Application.DoEvents()
							Thread.Sleep(1)
						End While
					End If
					'XmtCanMessageThread.Abort()
					mCanLogThread = Nothing
				End If

				mblnTerminateCanLogThread = False

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		'Friend Sub LoadCANMsgOutputQueue(ByVal CanMsg As TPCANMsg)
		Public Sub LoadCANMsgOutputQueue(ByVal CanMsg As clsCANActuator.clsCANMessage)
			' Queue individual CAN messages

			Try
				' Lock the queue while writing
				mCanMsgOutputQueue.Enqueue(CanMsg)
				' Unblock the CAN transmit thread.
				mXmtCanMsgWaitHandle.Set()

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Public Sub LoadCANMsgLogQueue(ByVal strMessage As String)
			' Queue individual CAN messages

			Try

				If CanMessageLoggingEnabled Then
					' Lock the queue while writing
					mCanMsgLogQueue.Enqueue(strMessage)
					' Unblock the CAN transmit thread.
					mCanLogWaitHandle.Set()
				End If

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Public Function WriteMultiple(ByVal FramesToWrite As Integer, ByRef Data() As nican.NCTYPE_CAN_STRUCT) As Integer

			Dim BytesToWrite As Integer
			Dim Buffer() As Byte
			Dim NumFrame As Integer
			Dim Status As Integer
			Try
				BytesToWrite = FramesToWrite * 22
				ReDim Buffer(BytesToWrite)

				For NumFrame = 0 To FramesToWrite - 1
					Call RtlMoveMemory(Buffer((NumFrame) * 22), Data(NumFrame), 22)
				Next
				Status = nican.ncWriteMult(mintObjectHandle, BytesToWrite, Buffer(0))
				Call Delay(1)
				Return Status

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
				Return Nothing
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
				Return Nothing
			End Try
		End Function


#End Region

#Region "Private Methods"

		Private Sub GetCANConfigSettings(ByRef attr_list() As Integer, ByRef attr_value_list() As Integer)
			Try

				ReDim attr_list(11)
				ReDim attr_value_list(11)

				attr_list(0) = nican.NC_ATTR_BAUD_RATE
				attr_value_list(0) = nican.NC_BAUD_250K
				attr_list(1) = nican.NC_ATTR_START_ON_OPEN
				attr_value_list(1) = nican.NC_FALSE
				attr_list(2) = nican.NC_ATTR_READ_Q_LEN
				attr_value_list(2) = 100
				attr_list(3) = nican.NC_ATTR_WRITE_Q_LEN
				attr_value_list(3) = 10
				attr_list(4) = nican.NC_ATTR_CAN_COMP_STD
				attr_value_list(4) = 0
				attr_list(5) = nican.NC_ATTR_CAN_MASK_STD
				attr_value_list(5) = nican.NC_CAN_MASK_STD_DONTCARE
				attr_list(6) = nican.NC_ATTR_CAN_COMP_XTD
				attr_value_list(6) = 0
				attr_list(7) = nican.NC_ATTR_CAN_MASK_XTD
				attr_value_list(7) = nican.NC_CAN_MASK_XTD_DONTCARE
				attr_list(8) = nican.NC_ATTR_TRANSMIT_MODE
				attr_value_list(8) = nican.NC_TX_MODE_TIMESTAMPED

				'These for future functionality using RTSI
				attr_list(9) = nican.NC_ATTR_RTSI_MODE
				attr_value_list(9) = nican.NC_RTSI_OUT_ON_TX
				attr_list(10) = nican.NC_ATTR_RTSI_SIG_BEHAV
				attr_value_list(10) = nican.NC_RTSISIG_PULSE
				attr_list(11) = nican.NC_ATTR_RTSI_SIGNAL
				attr_value_list(11) = nican.NC_DEST_TERM_RTSI1



			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		Private Sub CreateThreads()

			Try
				' Create receive CAN message delivery thread and set priority
				mRcvCanMsgDeliveryThread = New Thread(New ThreadStart(AddressOf DeliverRcvdCANMsgs))
				mRcvCanMsgDeliveryThread.Priority = ThreadPriority.AboveNormal 'ThreadPriority.AboveNormal

				' Create transmit CAN message thread and set priority
				mXmtCanMessageThread = New Thread(New ThreadStart(AddressOf Xmt_CAN_Msgs))
				mXmtCanMessageThread.Priority = ThreadPriority.AboveNormal 'ThreadPriority.AboveNormal

				' Create Queue Rcv CAN message thread and set priority
				mQueueRcvdCanMsgThread = New Thread(New ThreadStart(AddressOf Queue_Rcvd_CAN_Msgs))
				mQueueRcvdCanMsgThread.Priority = ThreadPriority.Highest 'ThreadPriority.AboveNormal

				' Create transmit CAN Log message thread and set priority
				mCanLogThread = New Thread(New ThreadStart(AddressOf CAN_Logging))
				mCanLogThread.Priority = ThreadPriority.AboveNormal


			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

#End Region

#Region "Thread Handlers"
		Private Sub DeliverRcvdCANMsgs()

			Dim msgStruct As CAN_Msg_Struct
			Dim msgCount As Integer = 0
			Dim dequeued As Boolean = False
			'Dim strCANMessageLog As String

			Dim rcvdMessage As RcvdCANMsg = New RcvdCANMsg


			Try
				Do
					' Block until received messages have been queued.
					mRcvCanMsgDeliveryWaitHandle.WaitOne()
					If (mblnTerminateRcvCanMsgDeliverThread = True) Then
						' Allow this thread to terminate.
						Exit Do
					End If
					' Deliver all messages in the delivery queue.
					Do
						If (mRcvdCanMessageQueue.Count <> 0) Then
							' Grab the next message along with the latest queued message count.
							'dequeued = mRcvdCanMessageQueue.TryDequeue(msgStruct)
							dequeued = mRcvdCanMessageQueue.TryDequeue(rcvdMessage)
							msgCount = mRcvdCanMessageQueue.Count
							dequeued = True
						Else
							dequeued = False
							msgCount = 0
						End If
						If (dequeued = True) Then
							' Invoke the multicast delegate corresponding to the type of the received message.
							' Messages that don't have a corresponding delegate are ignored.

							msgStruct = New CAN_Msg_Struct(rcvdMessage.CANMsg, rcvdMessage.CANMsgTimeStamp)

							Select Case msgStruct.CanMsg.ID And SRC_ADDR_MASK
				'Select Case msgStruct.CanMsg.ID 'And &HFFFFFF00 'SRC_ADDR_MASK
								Case ADDR_CLAIM, CAT_ADDR_CLAIM
									If (mRcvMsgDelegates.AddrClaimDelegates IsNot Nothing) Then
										mRcvMsgDelegates.AddrClaimDelegates.DynamicInvoke(msgStruct)
									End If
								Case STATUS_MESSAGE, CAT_FUEL_STATUS_MESSAGE, CAT_THROTTLE_STATUS_MESSAGE '&H38FFC502 '&H18FFC500      ' Cummins 18FFC5xx
									If (mRcvMsgDelegates.StatusMsgDelegates IsNot Nothing) Then
										mRcvMsgDelegates.StatusMsgDelegates.DynamicInvoke(msgStruct)
										'Log CAN Messages if enabled
										'strCANMessageLog = "CAN Status Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2")
										'Call LogCanMessage(strCANMessageLog)
										'Call LogCanMessage()
										'strCANMessageLog = "CAN Status Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", msgStruct.CanMsg.DATA)
										'LoadCANMsgLogQueue(strCANMessageLog)
									End If
								Case TOOL_MSG_RESPONSE, UDS_MSG_RESPONSE '&H18FF0D00 '&H38FF0D02   &H18DAF102
									If (mRcvMsgDelegates.ToolResponseDelegates IsNot Nothing) Then
										mRcvMsgDelegates.ToolResponseDelegates.DynamicInvoke(msgStruct)
										'Log CAN Messages if enabled
										'strCANMessageLog = "CAN Tool Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2")
										'Call LogCanMessage(strCANMessageLog)
										'Call LogCanMessage()
										'strCANMessageLog = "CAN Tool Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", msgStruct.CanMsg.DATA)
										'LoadCANMsgLogQueue(strCANMessageLog)
									End If
									'Case &H38DAF100 '&H18DAF100 'CUST_BTLDR_RESPONSE
									'  If (RcvMsgDelegates.CustBtldrResponseDelegates IsNot Nothing) Then
									'    RcvMsgDelegates.CustBtldrResponseDelegates.DynamicInvoke(msgStruct)
									'  End If
							End Select
						End If
					Loop While (msgCount > 0)
				Loop

			Catch ex As TsopAnomalyException
				mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
			Catch ex As Exception
				mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
			End Try
		End Sub

		'Public Sub Queue_Rcvd_CAN_Msgs()

		'  Dim rcvMsg As nican.NCTYPE_CAN_STRUCT = New nican.NCTYPE_CAN_STRUCT
		'  Dim status As Integer
		'  Dim msgID As Integer
		'  Dim canMsg As TPCANMsg
		'  Dim msgs_queued As Boolean = False
		'  Dim strCANMessageLog As String
		'  Try
		'    Do
		'      Do
		'        If (mblnTerminateQueueRcvdCanMsgThread = True) Then
		'          Exit Do
		'        End If
		'        ' Read in received CAN messages from the CAN device.
		'        status = nican.ncWaitForState(mintObjectHandle, nican.NC_STATE_ENUM.NC_ST_READ_AVAIL, 100, mintCurrentState)
		'        'If ((status = nican.NC_SUCCESS) AndAlso ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) > 0)) Then
		'        If ((status = nican.NC_SUCCESS) AndAlso ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) = nican.NC_STATE_ENUM.NC_ST_READ_AVAIL)) Then
		'          status = nican.ncRead(mintObjectHandle, Marshal.SizeOf(rcvMsg), rcvMsg)
		'          If (status = nican.NC_SUCCESS) Then
		'            canMsg = New TPCANMsg
		'            'Strip Off Extended Message Bit
		'            msgID = rcvMsg.ArbitrationId And &HF0000000 '38FF0D02 And F0000000 = 30000000
		'            msgID >>= 1 '= 18000000
		'            msgID = msgID + (rcvMsg.ArbitrationId And &HFFFFFF) '= 18000000 + (38FF0D02 And FFFFFF) = 18000000 + FF0D02 = 18FF0D02
		'            'Store ArbitrationID without extended message bit
		'            canMsg.ID = msgID
		'            canMsg.DATA = rcvMsg.Data
		'            canMsg.LEN = rcvMsg.DataLength
		'            canMsg.MSGTYPE = rcvMsg.FrameType
		'            'Console.WriteLine(Hex(msgID))
		'            ' Successfully read CAN message, now queue it for delivery.
		'            mRcvdCanMessageQueue.Enqueue(New CAN_Msg_Struct(canMsg, rcvMsg.Timestamp))
		'            msgs_queued = True

		'            'Log CAN Messages if enabled
		'            'strCANMessageLog = "CAN Message Received and Queued, MessageID =, " & Hex(canMsg.ID) & ", OpCode =, " & canMsg.DATA(1).ToString("X2") & canMsg.DATA(0).ToString("X2")
		'            'Call LogCanMessage(strCANMessageLog)
		'            'Call LogCanMessage()
		'            strCANMessageLog = "CAN Message Received and Queued, MessageID =, " & Hex(canMsg.ID) & ", OpCode =, " & canMsg.DATA(1).ToString("X2") & canMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", canMsg.DATA)
		'            LoadCANMsgLogQueue(strCANMessageLog)
		'          End If
		'        End If
		'      Loop While ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) > 1)
		'      If (msgs_queued = True) Then
		'        ' Unblock receive CAN message delivery thread.
		'        mRcvCanMsgDeliveryWaitHandle.Set()
		'        msgs_queued = False
		'      End If
		'      Thread.Sleep(1)
		'    Loop While (mblnTerminateQueueRcvdCanMsgThread = False)

		'  Catch ex As TsopAnomalyException
		'    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
		'  Catch ex As Exception
		'    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
		'  End Try
		'End Sub

		Private Sub Queue_Rcvd_CAN_Msgs()
			Dim result As TPCANStatus

			Try
				Do

					Select Case mintCanInterfaceMfg
						Case CanInterfaceMfgEnum.NI
							Do
								' Query the NiCan device for received CAN messages once every 1 msec.
								'Thread.Sleep(1)
								If (mblnTerminateQueueRcvdCanMsgThread = True) Then
									' Allow this thread to terminate.
									Exit Do
								End If
								Call NiCan_Queue_Rcvd_CAN_Msgs()
							Loop While ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) > 1)
						Case CanInterfaceMfgEnum.Peak
							Do
								'Thread.Sleep(1)
								If (mblnTerminateQueueRcvdCanMsgThread = True) Then
                  ' Allow this thread to terminate.
                  Exit Do
                End If
                result = PCAN_Queue_Rcvd_CAN_Msgs()
              Loop While ((result And TPCANStatus.PCAN_ERROR_QRCVEMPTY) <> TPCANStatus.PCAN_ERROR_QRCVEMPTY)
          End Select
        Loop While (mblnTerminateQueueRcvdCanMsgThread = False)

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Private Sub NiCan_Queue_Rcvd_CAN_Msgs()

      Dim status As Integer
      Dim msgId As Integer
      Dim canMsg As TPCANMsg
      Dim peakTimeStamp = New TPCANTimestamp
      Dim statusString As String = ""

      Dim rcvMsg As nican.NCTYPE_CAN_STRUCT = New nican.NCTYPE_CAN_STRUCT
      Dim msgs_queued As Boolean = False
      Dim strCANMessageLog As String

      Dim timeStamp As CAN_MsgTimestamp
      Dim rcvdMessage As RcvdCANMsg

      ' Read in received CAN messages from the CAN device.
      status = nican.ncWaitForState(mintObjectHandle,
                                    nican.NC_STATE_ENUM.NC_ST_READ_AVAIL,
                                    100, mintCurrentState)
      'If ((status = nican.NC_SUCCESS) AndAlso ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) > 0)) Then
      If ((status = nican.NC_SUCCESS) AndAlso
          ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) =
           nican.NC_STATE_ENUM.NC_ST_READ_AVAIL)) Then
        status = nican.ncRead(mintObjectHandle, Marshal.SizeOf(rcvMsg), rcvMsg)
        If (status = nican.NC_SUCCESS) Then

          canMsg = New TPCANMsg
          msgId = rcvMsg.ArbitrationId And &HF0000000
          msgId >>= 1
          canMsg.ID = msgId + (rcvMsg.ArbitrationId And &HFFFFFF)
          canMsg.DATA = rcvMsg.Data
          canMsg.LEN = rcvMsg.DataLength
          canMsg.MSGTYPE = rcvMsg.FrameType
          ' Store NiCAN timestamp lower 32 bits in Peak timestamp.
          peakTimeStamp.millis = rcvMsg.Timestamp.LowPart
          ' Store Peak timestamp and NiCAN timestamp upper
          ' 32 bits (overflow) for later conversion to
          ' obtain final timestamp value.
          timeStamp = New CAN_MsgTimestamp(peakTimeStamp, rcvMsg.Timestamp.HighPart)
          rcvdMessage = New RcvdCANMsg(canMsg, timeStamp)
          ' Successfully read CAN message, now queue it for delivery.
          mRcvdCanMessageQueue.Enqueue(rcvdMessage)


          'canMsg = New TPCANMsg
          ''Strip Off Extended Message Bit
          'msgId = rcvMsg.ArbitrationId And &HF0000000 '38FF0D02 And F0000000 = 30000000
          'msgId >>= 1 '= 18000000
          'msgId = msgId + (rcvMsg.ArbitrationId And &HFFFFFF) '= 18000000 + (38FF0D02 And FFFFFF) = 18000000 + FF0D02 = 18FF0D02
          ''Store ArbitrationID without extended message bit
          'canMsg.ID = msgId
          'canMsg.DATA = rcvMsg.Data
          'canMsg.LEN = rcvMsg.DataLength
          'canMsg.MSGTYPE = rcvMsg.FrameType
          ''Console.WriteLine(Hex(msgID))
          '' Successfully read CAN message, now queue it for delivery.
          'mRcvdCanMessageQueue.Enqueue(New CAN_Msg_Struct(canMsg, rcvMsg.Timestamp))

          msgs_queued = True
          mRcvCanMsgDeliveryWaitHandle.Set()

          'Log CAN Messages if enabled
          'strCANMessageLog = "CAN Message Received and Queued, MessageID =, " & Hex(canMsg.ID) & ", OpCode =, " & canMsg.DATA(1).ToString("X2") & canMsg.DATA(0).ToString("X2")
          'Call LogCanMessage(strCANMessageLog)
          'Call LogCanMessage()
          strCANMessageLog = "CAN Message Received and Queued, MessageID =, " &
                             Hex(canMsg.ID) & ", OpCode =, " & canMsg.DATA(1).ToString("X2") &
                             canMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", canMsg.DATA)
          LoadCANMsgLogQueue(strCANMessageLog)
        End If
      End If

    End Sub

    Private Function PCAN_Queue_Rcvd_CAN_Msgs() As TPCANStatus

      Dim rcvdMessage As RcvdCANMsg
      Dim result As TPCANStatus = TPCANStatus.PCAN_ERROR_OK

      'Dim canMsg As TPCANMsg

      Dim msgs_queued As Boolean = False
      Dim strCANMessageLog As String

      'Dim rcvdMessage As RcvdCANMsg
      'Dim message As Peak.Can.Basic.TPCANMsg = New Peak.Can.Basic.TPCANMsg
      Dim peakTimeStamp As TPCANTimestamp
      Dim timeStamp As CAN_MsgTimestamp


      Dim message As TPCANMsg = New TPCANMsg


      ' Read CAN Messages
      result = PCANBasic.Read(mPCAN_DeviceHandle, message, peakTimeStamp)
      If (result = TPCANStatus.PCAN_ERROR_OK) Then

        timeStamp = New CAN_MsgTimestamp(peakTimeStamp)
        rcvdMessage = New RcvdCANMsg(message, timeStamp)
        ' Queue the received CAN message.
        mRcvdCanMessageQueue.Enqueue(rcvdMessage)

        'timeStamp = New CAN_MsgTimestamp(peakTimeStamp)
        'canMsg = New TPCANMsg
        ''Store ArbitrationID without extended message bit
        'canMsg.ID = message.ID
        'canMsg.DATA = message.DATA
        'canMsg.LEN = message.LEN
        'canMsg.MSGTYPE = message.MSGTYPE
        ''Console.WriteLine(Hex(msgID))
        '' Successfully read CAN message, now queue it for delivery.
        'mRcvdCanMessageQueue.Enqueue(New CAN_Msg_Struct(canMsg, timeStamp))

        msgs_queued = True
        mRcvCanMsgDeliveryWaitHandle.Set()

        'Log CAN Messages if enabled
        'strCANMessageLog = "CAN Message Received and Queued, MessageID =, " & Hex(canMsg.ID) & ", OpCode =, " & canMsg.DATA(1).ToString("X2") & canMsg.DATA(0).ToString("X2")
        'Call LogCanMessage(strCANMessageLog)
        'Call LogCanMessage()
        strCANMessageLog = "CAN Message Received and Queued, MessageID =, " &
                           Hex(rcvdMessage.CANMsg.ID) & ", OpCode =, " &
                           rcvdMessage.CANMsg.DATA(1).ToString("X2") &
                           rcvdMessage.CANMsg.DATA(0).ToString("X2") & ", Data =, " &
                           String.Join(",", rcvdMessage.CANMsg.DATA)
        LoadCANMsgLogQueue(strCANMessageLog)
      End If


      Return result
    End Function


    'Private Sub Xmt_CAN_Msgs()

    '  Dim nicanFrame As nican.NCTYPE_CAN_FRAME = New nican.NCTYPE_CAN_FRAME
    '  Dim status As Integer
    '  'Dim msgToSend As TPCANMsg = Nothing
    '  Dim msgToSend As New clsCANActuator.clsCANMessage
    '  Dim count As Integer = 0
    '  Dim dequeued As Boolean = False
    '  Dim strCANMessageLog As String
    '  Try
    '    Do
    '      ' Block until a CAN message is queued for transmit.
    '      mXmtCanMsgWaitHandle.WaitOne()
    '      If (mblnTerminateXmtCanMsgThread = True) Then
    '        ' Allow this thread to terminate.
    '        Exit Do
    '      End If
    '      Do
    '        ' Send the next queued message.
    '        If (mCanMsgOutputQueue.Count <> 0) Then
    '          ' Lock the queue while reading.
    '          dequeued = mCanMsgOutputQueue.TryDequeue(msgToSend)
    '          count = mCanMsgOutputQueue.Count
    '        Else
    '          count = 0
    '          dequeued = False
    '        End If
    '        If (dequeued = True) Then
    '          nicanFrame.IsRemote = nican.NC_FRMTYPE_DATA
    '          'nicanFrame.ArbitrationId = msgToSend.SendArbitrationID 'nican.NC_FL_CAN_ARBID_XTD Or msgToSend.ID
    '          nicanFrame.ArbitrationId = msgToSend.SendArbitrationID Or nican.NC_FL_CAN_ARBID_XTD
    '          nicanFrame.DataLength = msgToSend.DataLength 'msgToSend.LEN
    '          nicanFrame.Data = msgToSend.Data 'msgToSend.DATA
    '          status = nican.ncWrite(mintObjectHandle, Marshal.SizeOf(nicanFrame), nicanFrame)

    '          strCANMessageLog = "CAN Message Transmitted, MessageID =, " & Hex(nicanFrame.ArbitrationId) & ", OpCode =, " & nicanFrame.Data(1).ToString("X2") & nicanFrame.Data(0).ToString("X2") & ", Data =, " & String.Join(",", nicanFrame.Data)
    '          LoadCANMsgLogQueue(strCANMessageLog)
    '        End If
    '      Loop While ((count > 0) AndAlso (status <> nican.CanErrComm))
    '    Loop

    '  Catch ex As TsopAnomalyException
    '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    '  Catch ex As Exception
    '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    '  End Try
    'End Sub

    Private Sub Xmt_CAN_Msgs()

      Dim dequeued As Boolean = False

      Try
        Do
          ' SEND CAN MESSAGES
          ' Block until a CAN message is queued for transmit.
          mXmtCanMsgWaitHandle.WaitOne()

          Select Case mintCanInterfaceMfg
            Case CanInterfaceMfgEnum.NI
              Do
                If (mblnTerminateXmtCanMsgThread = True) Then
                  ' Allow this thread to terminate.
                  Exit Do
                End If
                dequeued = NiCan_Xmt_CAN_Msgs()
              Loop While (dequeued = True)
            Case CanInterfaceMfgEnum.Peak
              If (mblnTerminateXmtCanMsgThread = True) Then
                ' Allow this thread to terminate.
                Exit Do
              End If
              Do
                dequeued = PCAN_Xmt_CAN_Msgs()
              Loop While (dequeued = True)
          End Select

        Loop While (mblnTerminateXmtCanMsgThread = False)

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Private Function NiCan_Xmt_CAN_Msgs() As Boolean
      Dim nicanFrame As nican.NCTYPE_CAN_FRAME = New nican.NCTYPE_CAN_FRAME
      Dim status As Integer
      Dim msgToSend As New clsCANActuator.clsCANMessage
      Dim dequeued As Boolean = False
      Dim statusString As String = ""
      Dim strCANMessageLog As String
      Try
        ' Send the next queued message.
        dequeued = mCanMsgOutputQueue.TryDequeue(msgToSend)
        If (dequeued = True) Then
          If (dequeued = True) Then
            nicanFrame.IsRemote = nican.NC_FRMTYPE_DATA
            'nicanFrame.ArbitrationId = msgToSend.SendArbitrationID 'nican.NC_FL_CAN_ARBID_XTD Or msgToSend.ID
            nicanFrame.ArbitrationId = msgToSend.SendArbitrationID Or nican.NC_FL_CAN_ARBID_XTD
            nicanFrame.DataLength = msgToSend.DataLength 'msgToSend.LEN
            nicanFrame.Data = msgToSend.Data 'msgToSend.DATA
            status = nican.ncWrite(mintObjectHandle, Marshal.SizeOf(nicanFrame), nicanFrame)

            strCANMessageLog = "CAN Message Transmitted, MessageID =, " &
                               Hex(nicanFrame.ArbitrationId) & ", OpCode =, " &
                               nicanFrame.Data(1).ToString("X2") &
                               nicanFrame.Data(0).ToString("X2") & ", Data =, " &
                               String.Join(",", nicanFrame.Data)
            LoadCANMsgLogQueue(strCANMessageLog)
          End If
        End If
        Return dequeued

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
        Return False
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
        Return False
      End Try
    End Function

    Private Function PCAN_Xmt_CAN_Msgs() As Boolean

      'Dim msgToSend As TPCANMsg = New TPCANMsg
      Dim msgToSend As New clsCANActuator.clsCANMessage
      Dim result As TPCANStatus = 0
      Dim dequeued As Boolean = False

      'Dim msgToWrite As Peak.Can.Basic.TPCANMsg = New Peak.Can.Basic.TPCANMsg
      Dim msgToWrite As TPCANMsg = New TPCANMsg
      Dim strCANMessageLog As String

      Try
        ' SEND CAN MESSAGES
        dequeued = mCanMsgOutputQueue.TryDequeue(msgToSend)
        If (dequeued = True) Then
          result = PCANBasic.GetStatus(mPCAN_DeviceHandle)
          If ((result <> TPCANStatus.PCAN_ERROR_QXMTFULL) AndAlso
              (result <> TPCANStatus.PCAN_ERROR_BUSOFF) AndAlso
              (result <> TPCANStatus.PCAN_ERROR_ILLHW)) Then
            msgToWrite.ID = msgToSend.SendArbitrationID
            msgToWrite.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED 'TPCANMessageType.PCAN_MESSAGE_EXTENDED
            msgToWrite.LEN = msgToSend.DataLength
            msgToWrite.DATA = msgToSend.Data

            result = PCANBasic.Write(mPCAN_DeviceHandle, msgToWrite)

            strCANMessageLog = "CAN Message Transmitted, MessageID =, " &
                               Hex(msgToWrite.ID) & ", OpCode =, " &
                               msgToWrite.DATA(1).ToString("X2") &
                               msgToWrite.DATA(0).ToString("X2") & ", Data =, " &
                               String.Join(",", msgToWrite.DATA)
            LoadCANMsgLogQueue(strCANMessageLog)

          End If
        End If

        Return dequeued

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
        Return False
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
        Return False
      End Try
    End Function


    Private Sub CAN_Logging()
      'Dim nicanFrame As nican.NCTYPE_CAN_FRAME = New nican.NCTYPE_CAN_FRAME
      'Dim status As Integer
      ''Dim msgToSend As TPCANMsg = Nothing
      'Dim msgToSend As New clsCANActuator.clsCANMessage
      Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing

      Dim count As Integer = 0
      Dim dequeued As Boolean = False
      Dim strLogMessage As String = ""
      Dim strFileName As String = ""
      Dim strDate As String = ""
      Dim dblElapsedTime As Double
      Try

        strFileName = mstrCANMessageLogFilePath & "\CanMessageLog.txt"
        Do
          ' Block until a CAN message is queued for transmit.
          mCanLogWaitHandle.WaitOne()
          If (mblnTerminateCanLogThread = True) Then
            ' Allow this thread to terminate.
            Exit Do
          End If
          Do
            ' Send the next queued message.
            If (mCanMsgLogQueue.Count <> 0) Then
              ' Lock the queue while reading.
              dequeued = mCanMsgLogQueue.TryDequeue(strLogMessage)
              count = mCanMsgLogQueue.Count
            Else
              count = 0
              dequeued = False
            End If
            If (dequeued = True) Then
              strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")
              If mCanLogStartTime = 0 Then
                dblElapsedTime = 0
              Else
                QueryPerformanceCounter(HP_CounterValue)
                dblElapsedTime = (CDbl(HP_CounterValue) * CounterPeriod) - mCanLogStartTime
              End If
              strDate = strDate & ", " & dblElapsedTime
              CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
              CanMessageLogStreamWriter.WriteLine(strDate & ", " & strLogMessage)
              CanMessageLogStreamWriter.Flush()
              CanMessageLogStreamWriter.Close()
              QueryPerformanceCounter(HP_CounterValue)
              mCanLogStartTime = (CDbl(HP_CounterValue) * CounterPeriod)
            End If
          Loop While (count > 0)
        Loop

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub


#End Region

  End Class

#End Region ' ============================================================

#Region "Class clsRcvCanMsgDelegates"
  Public Class clsRcvCanMsgDelegates

#Region "Constructor"
    Protected Sub New()
      ' Prevents compiler from creating a default Public constructor and also
      ' prevents any caller from creating their own instance of this class.
    End Sub
#End Region

#Region "Private Member Variables"

    Private mAnomaly As clsAnomaly
    Private mStackTrace As New clsStackTrace
    Private mStopwatch As New clsStopWatch

    ' Private single instance of this class.
    Private Shared _RcvCanMsgDelegatesInstance As clsRcvCanMsgDelegates
    ' Multicast Delegate used to call registered delegates when address claim messages are received.
    Private mAddrClaimDelegates As [Delegate]
    ' Locking object for address claim multicast delegate.
    Private mAddrClaimLock As Object = New Object
    ' Multicast Delegate used to call registered delegates when status messages are received.
    Private mStatusMsgDelegates As [Delegate]
    ' Locking object for address claim multicast delegate.
    Private mStatusMsgLock As Object = New Object
    ' Multicast Delegate used to call registered delegates when satus messages are received.
    Private mToolResponseDelegates As [Delegate]
    ' Locking object for tool reponse message multicast delegate.
    Private mToolMsgLock As Object = New Object
    ' Multicast Delegate used to call registered delegates when custom bootloader response messages are received.
    Private mCustBtldrResponseDelegates As [Delegate]
    ' Locking object for tool reponse message multicast delegate.
    Private mCustBtldrMsgLock As Object = New Object
#End Region

#Region "Public Variables"
    ' Delegate type to be used when registering callbacks for specific messages.
    Public Delegate Sub DelegateSub(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)

#End Region

#Region "Properties"
    Public Property Anomaly() As clsAnomaly
      Get
        Return mAnomaly
      End Get
      Set(ByVal value As clsAnomaly)
        mAnomaly = value
      End Set
    End Property

    Public ReadOnly Property AddrClaimDelegates() As DelegateSub
      Get
        SyncLock mAddrClaimLock
          Return mAddrClaimDelegates
        End SyncLock
      End Get
    End Property

    Public ReadOnly Property StatusMsgDelegates() As DelegateSub
      Get
        SyncLock mStatusMsgLock
          Return mStatusMsgDelegates
        End SyncLock
      End Get
    End Property

    Public ReadOnly Property ToolResponseDelegates() As DelegateSub
      Get
        SyncLock mToolMsgLock
          Return mToolResponseDelegates
        End SyncLock
      End Get
    End Property

    Public ReadOnly Property CustBtldrResponseDelegates() As DelegateSub
      Get
        SyncLock mCustBtldrMsgLock
          Return mCustBtldrResponseDelegates
        End SyncLock
      End Get
    End Property
#End Region

#Region "Public Methods"
    Public Shared Function GetInstance() As clsRcvCanMsgDelegates 'TER
      Try
        ' Only one instance of this class (singleton) is allowed.
        ' Instantiate single instance of this class if it hasn't already been done.
        If (_RcvCanMsgDelegatesInstance Is Nothing) Then
          _RcvCanMsgDelegatesInstance = New clsRcvCanMsgDelegates
        End If

        Return _RcvCanMsgDelegatesInstance

      Catch ex As Exception
        Return Nothing
      End Try
    End Function

    Public Sub AddAddrClaimDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mAddrClaimLock
          mAddrClaimDelegates = MulticastDelegate.Combine(mAddrClaimDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAddrClaimDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mAddrClaimLock
          mAddrClaimDelegates = MulticastDelegate.Remove(mAddrClaimDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAllAddrClaimDelegates(ByVal del As DelegateSub)
      Try
        Do
          SyncLock mAddrClaimLock
            mAddrClaimDelegates = MulticastDelegate.Remove(mAddrClaimDelegates, del)
          End SyncLock
        Loop Until mAddrClaimDelegates Is Nothing

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub AddStatusMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mStatusMsgLock
          mStatusMsgDelegates = MulticastDelegate.Combine(mStatusMsgDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveStatusMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mStatusMsgLock
          mStatusMsgDelegates = MulticastDelegate.Remove(mStatusMsgDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAllStatusMsgDelegates(ByVal del As DelegateSub)
      Try
        Do
          SyncLock mStatusMsgLock
            mStatusMsgDelegates = MulticastDelegate.Remove(mStatusMsgDelegates, del)
          End SyncLock
        Loop Until mStatusMsgDelegates Is Nothing

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub AddToolMsgDelegate(ByVal del As DelegateSub)
      Try

        SyncLock mToolMsgLock
          mToolResponseDelegates = MulticastDelegate.Combine(mToolResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveToolMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mToolMsgLock
          mToolResponseDelegates = MulticastDelegate.Remove(mToolResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAllToolMsgDelegates(ByVal del As DelegateSub)
      Try
        Do
          SyncLock mToolMsgLock
            mToolResponseDelegates = MulticastDelegate.Remove(mToolResponseDelegates, del)
          End SyncLock
        Loop Until mToolResponseDelegates Is Nothing

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub AddCustBtldrMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mCustBtldrMsgLock
          mCustBtldrResponseDelegates = MulticastDelegate.Combine(mCustBtldrResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveCustbtldrMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mCustBtldrMsgLock
          mCustBtldrResponseDelegates = MulticastDelegate.Remove(mCustBtldrResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

#End Region

#Region "Private Methods"

#End Region
  End Class
#End Region ' ========================================================


#Region "CTTDownload Related"

	'Private Function LoadUDS_Msg(ByVal data() As Byte) As TPCANMsg
	'	' Prepare a Tool message to be queued.
	'	Dim CANMsg As TPCANMsg = New TPCANMsg
	'	Dim MsgData() As Byte = {data(0), data(1), data(2), data(3), data(4), data(5), data(6), data(7)}

	'	CANMsg.ID = gUDSReqID
	'	CANMsg.LEN = 8
	'	If (CANMsg.ID > &H7FF) Then
	'		CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED
	'	Else
	'		CANMsg.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_STANDARD
	'	End If
	'	CANMsg.DATA = MsgData
	'	Return CANMsg
	'End Function

	Public Sub UDS_InitProgramSession(ByRef GenB_MemoryImage As Byte())
		SendUDS_CumminsDIDRequests()
		SendUDS_ProgramSessionRequest()
		Dim partSeed As UInt32 = SendUDS_SeedRequest()
		SendUDS_SendKeyFromTool(partSeed)
		SendUDS_EraseMemory()
		SendUDS_RequestDownload()
		Dim N As Integer = CalEndAddr + 1 - AppStartAddr
		Dim cycle = Math.Ceiling(N / 256.0)
		Dim subArray(256) As Byte
		For i As Integer = 0 To cycle - 1
			Array.Copy(GenB_MemoryImage, AppStartAddr - 1 + i * 256, subArray, 0, 256)
			Console.WriteLine("Copied " & i & " blocks," & cycle & " cycles in total")
			SendUDS_StartDataTransferByBlock(subArray, i + 1)
		Next
		SendUDS_ExitDataTransfer()
		SendUDS_CheckDownloadCRC()
		SendUDS_Reset()
	End Sub

	'Change to Function later
	Private Sub SendUDS_CumminsDIDRequests()
		Dim ECUVarDIDMsg = New clsCANMessage
		Dim returnData(8) As Byte
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSECUVariantDID, ECUVarDIDMsg)
		If Not SendMessageGetResponse(ECUVarDIDMsg, returnData) Then
			Throw New Exception("UDS Cummins DID request -- ECU VarID failed")
		End If

		Dim ByteOrder = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSByteOrderDID, ByteOrder)
		If Not SendMessageGetResponse(ByteOrder, returnData) Then
			Throw New Exception("UDS Cummins DID request -- Byte Order failed")
		End If
	End Sub

	Private Sub SendUDS_ProgramSessionRequest()
		Dim DiagProgamSessionRequest = New clsCANMessage
		Dim returnData(8) As Byte
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSDiagnosticProgramSession, DiagProgamSessionRequest)
		If Not SendMessageGetResponse(DiagProgamSessionRequest, returnData) Then
			Throw New Exception("UDS request diagnostic programming session control failed")
		End If

		Dim TesterPresent = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSTesterPresent, TesterPresent)
		SendMessageNoResponse(TesterPresent)

		Dim B834DIDRequest = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSB834DID, B834DIDRequest)
		If Not SendMessageGetResponse(B834DIDRequest, returnData) Then
			Throw New Exception("UDS request for B834 DID failed")
		End If
	End Sub

	Private Function SendUDS_SeedRequest() As UInt32
		Dim SeedRequest = New clsCANMessage
		Dim returnData(8) As Byte
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSSecurityAccess, SeedRequest)
		If Not SendMessageGetResponse(SeedRequest, returnData) Then
			Throw New Exception("UDS request for security access failed")
		End If
		'Need to save the seed
		Dim seed As String = ""
		For pos As Integer = 3 To 6
			seed = seed & Convert.ToString(returnData(pos), 16)
		Next pos
		Return Convert.ToUInt32(seed, 16)
	End Function

	Private Sub UDS_CalcCumminsKeyFromSeed(lSeed1 As UInt32, lSeed2 As UInt32, lIdx As Byte, ByRef calcKey As UInteger())
		Dim lSum As ULong = 0 '&HC6EF3720
		Dim lBigValue As ULong = 0
		Dim V0 As ULong = lSeed1
		Dim V1 As ULong = lSeed2
		Dim I As Byte
		Dim X1 As ULong
		Dim X2 As ULong
		Dim X3 As ULong
		Dim lMask As ULong = 4294967295
		Try
			For I = 0 To 31
				Try
					lSum = (lSum + CTT_DELTA) And lMask
				Catch ex As Exception
					Console.WriteLine("CalcKeyFromSeed(0 I=" + I.ToString + ") Caught Exception: " & ex.ToString)
					'Exit For
				End Try
				Try
					X1 = ((V1 << 4) + CTT_KEY0) 'And lMask
					X2 = (V1 + lSum) 'And lMask
					X3 = ((V1 >> 5) + CTT_KEY1) 'And lMask
				Catch ex As Exception
					Console.WriteLine("CalcKeyFromSeed(1: I=" + I.ToString + ") Caught Exception: " & ex.ToString)
					X1 = ((V1 * 16) + CTT_KEY0) And lMask
					X2 = (V1 + lSum) And lMask
					X3 = ((V1 \ 32) + CTT_KEY1) And lMask
				End Try
				Try
					lBigValue = ((X1 Xor X2) Xor X3) 'And lMask
					V0 = ((V0 + lBigValue)) And lMask
				Catch ex As Exception
					lBigValue = (X1 Xor X2) And lMask
					lBigValue = (lBigValue Xor X3) And lMask
					V0 = ((V0 + lBigValue)) And lMask
					Console.WriteLine("CalcKeyFromSeed(2: I=" + I.ToString + ")  Caught Exception: " & ex.ToString)
					'Exit For
				End Try
				Try
					X1 = (V0 + lSum) 'And lMask
					X2 = ((V0 << 4) + CTT_KEY2) 'And lMask
					X3 = ((V0 >> 5) + CTT_KEY3) 'And lMask
				Catch ex As Exception
					X1 = (V0 + lSum) And lMask
					X2 = ((V0 * 16) + CTT_KEY2) And lMask
					X3 = ((V0 * 32) + CTT_KEY3) And lMask
					Console.WriteLine("CalcKeyFromSeed(3: I=" + I.ToString + ")  Caught Exception: " & ex.ToString)
				End Try
				Try
					lBigValue = ((X1 Xor X2) Xor X3) 'And lMask
					V1 = ((V1 + lBigValue)) And lMask
				Catch ex As Exception
					lBigValue = (X1 Xor X2) And lMask
					lBigValue = (lBigValue Xor X3) And lMask
					V1 = ((V1 + lBigValue)) And lMask
					Console.WriteLine("CalcKeyFromSeed(4: I=" + I.ToString + ") Caught Exception: " & ex.ToString)
					'Exit For
				End Try
			Next I
			calcKey(lIdx) = V0 And lMask
			calcKey(lIdx + 1) = V1 And lMask

		Catch ex As Exception
			Console.WriteLine("CalcKeyFromSeed(9) Caught Exception: " & ex.ToString)
		End Try
	End Sub

	Public Sub CorrectBL2AppJump()
		Dim eepromMod As clsCANMessage
		Dim returnData(8) As Byte

		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.WriteEEPROMGenBRequest, eepromMod)
		eepromMod.Data(1) = &H91
		eepromMod.Data(2) = &H1E
		eepromMod.Data(3) = &H0
		SendMessageNoResponse(eepromMod)
	End Sub
	Private Sub SendUDS_SendKeyFromTool(partSeed As UInt32)
		Dim lSeed = C_UDS_CTT_TOOLSEED0
		Dim lMask As ULong = 4294967295
		Dim lValue As ULong
		Dim calcKey(4) As UInteger

		Dim KeyToSend = New clsCANMessage
		Dim returnData(8) As Byte

		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSSendKey, KeyToSend)
		UDS_CalcCumminsKeyFromSeed(partSeed, lSeed, 0, calcKey)
		UDS_CalcCumminsKeyFromSeed(C_UDS_CTT_TOOLSEED1, C_UDS_CTT_TOOLSEED2, 2, calcKey)

		lValue = calcKey(0) And lMask
		KeyToSend.Data(7) = (lValue And &HFF)
		KeyToSend.Data(6) = ((lValue >> 8) And &HFF)
		KeyToSend.Data(5) = ((lValue >> 16) And &HFF)
		KeyToSend.Data(4) = ((lValue >> 24) And &HFF)

		Dim CustomizedOpCodes() As UInteger = {&H30, &H430}

		If Not SendMessageGetResponse(KeyToSend, returnData, CustomizedOpCodes) Then
			Throw New Exception("UDS request for sending key from tool failed")
		End If

		Dim KeyToolMsg1 = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSKeyDataMessage1, KeyToolMsg1)

		KeyToolMsg1.Data(0) = &H21
		lValue = calcKey(1) And lMask
		KeyToolMsg1.Data(1) = ((lValue >> 24) And &HFF)
		KeyToolMsg1.Data(2) = ((lValue >> 16) And &HFF)
		KeyToolMsg1.Data(3) = ((lValue >> 8) And &HFF)
		KeyToolMsg1.Data(4) = (lValue And &HFF)
		lValue = calcKey(2) And lMask
		KeyToolMsg1.Data(5) = ((lValue >> 24) And &HFF)
		KeyToolMsg1.Data(6) = ((lValue >> 16) And &HFF)
		KeyToolMsg1.Data(7) = ((lValue >> 8) And &HFF)

		If Not SendMessageNoResponse(KeyToolMsg1) Then
			Throw New Exception("UDS request for sending key data part 1 from tool failed")
		End If

		Dim KeyToolMsg2 = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSKeyDataMessage2, KeyToolMsg2)

		KeyToolMsg2.Data(0) = &H22
		KeyToolMsg2.Data(1) = (lValue And &HFF)
		lValue = calcKey(3) And lMask
		KeyToolMsg2.Data(2) = ((lValue >> 24) And &HFF)
		KeyToolMsg2.Data(3) = ((lValue >> 16) And &HFF)
		KeyToolMsg2.Data(4) = ((lValue >> 8) And &HFF)
		KeyToolMsg2.Data(5) = (lValue And &HFF)
		KeyToolMsg2.Data(6) = &H0
		KeyToolMsg2.Data(7) = &H0

		If Not SendMessageGetResponse(KeyToolMsg2, returnData) Then
			Throw New Exception("UDS request for sending key data part 1 from tool failed")
		End If

	End Sub

	Private Sub SendUDS_EraseMemory()
		Dim EraseMemMsg = New clsCANMessage
		Dim returnData(8) As Byte
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSEraseMemory, EraseMemMsg)
		If Not SendMessageGetResponse(EraseMemMsg, returnData) Then
			Throw New Exception("UDS request for erasing memory failed")
		End If
	End Sub

	Private Sub SendUDS_RequestDownload()
		Dim RequestDownload = New clsCANMessage
		Dim returnData(8) As Byte
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSRequestDownload, RequestDownload)
		For i As Integer = 3 To 1 Step -1
			RequestDownload.Data(8 - i) = ((AppStartAddr >> (8 * i)) And &HFF)
		Next

		If Not SendMessageGetResponse(RequestDownload, returnData) Then
			Throw New Exception("UDS request for data download to device failed")
		End If

		Dim NumOfBytesToDownload = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSSendBytesToDownload, NumOfBytesToDownload)

		NumOfBytesToDownload.Data(1) = (AppStartAddr And &HFF)
		Dim N As Integer = CalEndAddr + 1 - AppStartAddr
		For i As Integer = 2 To 5
			NumOfBytesToDownload.Data(i) = ((N >> ((5 - i) * 8)) And &HFF)
		Next


		If Not SendMessageGetResponse(NumOfBytesToDownload, returnData) Then
			Throw New Exception("UDS request for number of bytes to download failed")
		End If
	End Sub

	Private Sub SendUDS_StartDataTransferByBlock(ByRef dataToTransfer As Byte(), frameNo As Integer)
		Dim N = dataToTransfer.Length
		Dim StartDataTransfer = New clsCANMessage
		Dim returnData(8) As Byte
		Const offset As Integer = 4
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSStartDataTransferByBlock, StartDataTransfer)
		StartDataTransfer.Data(3) = frameNo And &HFF
		For i As Byte = 0 To offset - 2
			StartDataTransfer.Data(i + 4) = dataToTransfer(i + 1)
		Next
		StartDataTransfer.Data(7) = dataToTransfer(0)
		If Not SendMessageGetResponse(StartDataTransfer, returnData) Then
			Throw New Exception("UDS request for starting data transfer failed")
		End If

		Dim cycles = (N - offset) \ 7
		Dim count As Byte = 0
		Dim DataTransferByBlock = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSAckDataTransferByBlock, DataTransferByBlock)
		Dim msgToWrite As TPCANMsg = New TPCANMsg

		For i As UShort = 0 To cycles - 1
			count += 1
			DataTransferByBlock.Data(0) = (count And &HF) + 32
			For j As Byte = 1 To 7
				DataTransferByBlock.Data(j) = dataToTransfer(i * 7 + offset + j)
			Next
			msgToWrite.ID = DataTransferByBlock.SendArbitrationID
			msgToWrite.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED 'TPCANMessageType.PCAN_MESSAGE_EXTENDED
			msgToWrite.LEN = DataTransferByBlock.DataLength
			msgToWrite.DATA = DataTransferByBlock.Data

			If i < cycles - 1 Then
				Dim result = PCANBasic.Write(gCanActuator.CanMsgHandler.PeakCanHandle, msgToWrite)
				'SendMessageNoResponse(DataTransferByBlock)
			Else
				If Not SendMessageGetResponse(DataTransferByBlock, returnData) Then
					Throw New Exception("UDS data transfer failed to receive response from device in last data transfer message")
				End If
			End If
		Next


	End Sub

	Private Sub SendUDS_ExitDataTransfer()
		Dim returnData(8) As Byte
		Dim ExitDataTransfer = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSExitDataTransfer, ExitDataTransfer)
		If Not SendMessageGetResponse(ExitDataTransfer, returnData) Then
			Throw New Exception("UDS data transfer failed in exit data transfer mode")
		End If
	End Sub


	Private Sub SendUDS_CheckDownloadCRC()
		Dim returnData(8) As Byte
		Dim CheckDownloadCRC = New clsCANMessage
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSCheckDownloadCRC, CheckDownloadCRC)
		If Not SendMessageGetResponse(CheckDownloadCRC, returnData) Then
			Throw New Exception("UDS data transfer failed in exit data transfer mode")
		End If
	End Sub


	Private Sub SendUDS_Reset()
		Dim resetMsg = New clsCANMessage
		Dim returnData(8) As Byte
		Dim msgToWrite As TPCANMsg = New TPCANMsg
		mCANMessages.TryGetValue(clsCANMessage.MessageFunctions.UDSReset, resetMsg)
		msgToWrite.ID = resetMsg.SendArbitrationID
		msgToWrite.MSGTYPE = TPCANMessageType.PCAN_MESSAGE_EXTENDED 'TPCANMessageType.PCAN_MESSAGE_EXTENDED
		msgToWrite.LEN = resetMsg.DataLength
		msgToWrite.DATA = resetMsg.Data
		PCANBasic.Write(gCanActuator.CanMsgHandler.PeakCanHandle, msgToWrite)
		'If Not SendMessageGetResponse(resetMsg, returnData) Then
		'	Throw New Exception("UDS request failed to reset device")
		'End If
	End Sub

#End Region


End Class
