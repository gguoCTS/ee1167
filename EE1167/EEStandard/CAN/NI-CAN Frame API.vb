Option Strict Off
Option Explicit On
Imports System
Imports System.Text
Imports System.Runtime.InteropServices
Public Class nican

    '/*****************************************************************************/
    '/******************** N I - C A N   F R A M E    A P I ***********************/
    '/*****************************************************************************/

#Region "Datatypes"

    '/* This two-part declaration is required for compilers which do not
    '   provide support for native 64-bit integers.  */
    <StructLayout(LayoutKind.Sequential, Pack:=1)> Public Structure NCTYPE_UINT64
        Dim LowPart As Integer
        Dim HighPart As Integer
    End Structure

    '/* Type for ncWrite of CAN Network Interface Object */
    <StructLayout(LayoutKind.Sequential, Pack:=1)> Public Structure NCTYPE_CAN_FRAME
        Dim ArbitrationId As Integer
        Dim IsRemote As Byte
        Dim DataLength As Byte
        'MarshalAs removes the need to redim the array later
        <MarshalAs(UnmanagedType.ByValArray, sizeconst:=8)> Dim Data() As Byte
        Sub sizedata()
            ReDim Data(7)
        End Sub
    End Structure

    '/* Type for ncRead of CAN Network Interface Object */
    <StructLayout(LayoutKind.Sequential, Pack:=1)> Public Structure NCTYPE_CAN_FRAME_TIMED
        Dim Timestamp As NCTYPE_UINT64
        Dim ArbitrationId As Integer
        Dim IsRemote As Byte
        Dim DataLength As Byte
        <MarshalAs(UnmanagedType.ByValArray, sizeconst:=8)> Dim Data() As Byte
    End Structure

    '/* Type for ncRead of CAN Network Interface Object (using FrameType instead of IsRemote).
    '   Type for ncWrite of CAN Network Interface Object when timed transmission is enabled. */
    <StructLayout(LayoutKind.Sequential, Pack:=1)> Public Structure NCTYPE_CAN_STRUCT
        Public Timestamp As NCTYPE_UINT64
        Public ArbitrationId As Integer
        Public FrameType As Byte
        Public DataLength As Byte
        <MarshalAs(UnmanagedType.ByValArray, sizeconst:=8)> Public Data() As Byte
        Sub sizedata()
            ReDim Data(7)
        End Sub
    End Structure
  

    '/* Type for ncWrite of CAN Object */
    <StructLayout(LayoutKind.Sequential, Pack:=1)> Public Structure NCTYPE_CAN_DATA
        <MarshalAs(UnmanagedType.ByValArray, sizeconst:=8)> Dim Data() As Byte
    End Structure

    '/* Type for ncRead of CAN Object */
    <StructLayout(LayoutKind.Sequential, Pack:=1)> Public Structure NCTYPE_CAN_DATA_TIMED
        Dim Timestamp As NCTYPE_UINT64
        <MarshalAs(UnmanagedType.ByValArray, sizeconst:=8)> Dim Data() As Byte
    End Structure
#End Region
#Region "Status Constants"

    '/***********************************************************************
    '                              S T A T U S
    '***********************************************************************/

    '/* NCTYPE_STATUS

    '   NI-CAN and NI-DNET use the standard NI status format.
    '   This status format does not use bit-fields, but rather simple
    '   codes in the lower byte, and a common base for the upper bits.
    '   This standard NI status format ensures that all NI-CAN errors are located
    '   in a specific range of codes.  Since this range does not overlap with
    '   errors reported from other NI products, NI-CAN is supported within
    '   environments such as LabVIEW and MeasurementStudio.

    '   If your application currently uses the NI-CAN legacy error codes,
    '   you must change to the standard NI error codes.  For instructions on updating
    '   your code, refer to KnowledgeBase article # 2BBD8JHR on www.ni.com.

    '   If you shipped an executable to your customers that uses the legacy
    '   NI-CAN error codes, and you must upgrade those customers to the
    '   newest version of NI-CAN, contact National Instruments Technical
    '   Support to obtain instructions for re-enabling the legacy status format.
    '   */
    Public Const NICAN_WARNING_BASE As Integer = &H3FF62000
    Public Const NICAN_ERROR_BASE As Integer = &HBFF62000

    Public Const NC_SUCCESS As Integer = 0

    '/* Success values (you can simply use zero as well)  */
    Public Const CanSuccess As Short = 0
    Public Const DnetSuccess As Short = 0

    '/* Numbers 0x001 to 0x0FF are used for status codes defined prior to
    '   NI-CAN v1.5.  These codes can be mapped to/from the legacy status format.  */
    Public Const CanErrFunctionTimeout As Boolean = NICAN_ERROR_BASE Or &H1S
    Public Const CanErrWatchdogTimeout As Boolean = NICAN_ERROR_BASE Or &H21S
    Public Const DnetErrConnectionTimeout As Boolean = NICAN_ERROR_BASE Or &H41S
    Public Const DnetWarnConnectionTimeout As Boolean = NICAN_WARNING_BASE Or &H41S
    Public Const CanErrScheduleTimeout As Boolean = NICAN_ERROR_BASE Or &HA1S
    Public Const CanErrDriver As Boolean = NICAN_ERROR_BASE Or &H2S
    Public Const CanWarnDriver As Boolean = NICAN_WARNING_BASE Or &H2S
    Public Const CanErrBadNameSyntax As Boolean = NICAN_ERROR_BASE Or &H3S
    Public Const CanErrBadIntfName As Boolean = NICAN_ERROR_BASE Or &H23S
    Public Const CanErrBadCanObjName As Boolean = NICAN_ERROR_BASE Or &H43S
    Public Const CanErrBadParam As Boolean = NICAN_ERROR_BASE Or &H4S
    Public Const CanErrBadHandle As Boolean = NICAN_ERROR_BASE Or &H24S
    Public Const CanErrBadAttributeValue As Boolean = NICAN_ERROR_BASE Or &H5S
    Public Const CanErrAlreadyOpen As Boolean = NICAN_ERROR_BASE Or &H6S
    Public Const CanWarnAlreadyOpen As Boolean = NICAN_WARNING_BASE Or &H6S
    Public Const DnetErrOpenIntfMode As Boolean = NICAN_ERROR_BASE Or &H26S
    Public Const DnetErrOpenConnType As Boolean = NICAN_ERROR_BASE Or &H46S
    Public Const CanErrNotStopped As Boolean = NICAN_ERROR_BASE Or &H7S
    Public Const CanErrOverflowWrite As Boolean = NICAN_ERROR_BASE Or &H8S
    Public Const CanErrOverflowCard As Boolean = NICAN_ERROR_BASE Or &H28S
    Public Const CanErrOverflowChip As Boolean = NICAN_ERROR_BASE Or &H48S
    Public Const CanErrOverflowRxQueue As Boolean = NICAN_ERROR_BASE Or &H68S
    Public Const CanWarnOldData As Boolean = NICAN_WARNING_BASE Or &H9S
    Public Const CanErrNotSupported As Boolean = NICAN_ERROR_BASE Or &HAS
    Public Const CanWarnComm As Boolean = NICAN_WARNING_BASE Or &HBS
    Public Const CanErrComm As Boolean = NICAN_ERROR_BASE Or &HBS
    Public Const CanWarnCommStuff As Boolean = NICAN_WARNING_BASE Or &H2BS
    Public Const CanErrCommStuff As Boolean = NICAN_ERROR_BASE Or &H2BS
    Public Const CanWarnCommFormat As Boolean = NICAN_WARNING_BASE Or &H4BS
    Public Const CanErrCommFormat As Boolean = NICAN_ERROR_BASE Or &H4BS
    Public Const CanWarnCommNoAck As Boolean = NICAN_WARNING_BASE Or &H6BS
    Public Const CanErrCommNoAck As Boolean = NICAN_ERROR_BASE Or &H6BS
    Public Const CanWarnCommTx1Rx0 As Boolean = NICAN_WARNING_BASE Or &H8BS
    Public Const CanErrCommTx1Rx0 As Boolean = NICAN_ERROR_BASE Or &H8BS
    Public Const CanWarnCommTx0Rx1 As Boolean = NICAN_WARNING_BASE Or &HABS
    Public Const CanErrCommTx0Rx1 As Boolean = NICAN_ERROR_BASE Or &HABS
    Public Const CanWarnCommBadCRC As Boolean = NICAN_WARNING_BASE Or &HCBS
    Public Const CanErrCommBadCRC As Boolean = NICAN_ERROR_BASE Or &HCBS
    Public Const CanWarnCommUnknown As Boolean = NICAN_WARNING_BASE Or &HEBS
    Public Const CanErrCommUnknown As Boolean = NICAN_ERROR_BASE Or &HEBS
    Public Const CanWarnTransceiver As Boolean = NICAN_WARNING_BASE Or &HCS
    Public Const CanWarnRsrcLimitQueues As Boolean = NICAN_WARNING_BASE Or &H2DS
    Public Const CanErrRsrcLimitQueues As Boolean = NICAN_ERROR_BASE Or &H2DS
    Public Const DnetErrRsrcLimitIO As Boolean = NICAN_ERROR_BASE Or &H4DS
    Public Const DnetErrRsrcLimitWriteSrvc As Boolean = NICAN_ERROR_BASE Or &H6DS
    Public Const DnetErrRsrcLimitReadSrvc As Boolean = NICAN_ERROR_BASE Or &H8DS
    Public Const DnetErrRsrcLimitRespPending As Boolean = NICAN_ERROR_BASE Or &HADS
    Public Const DnetWarnRsrcLimitRespPending As Boolean = NICAN_WARNING_BASE Or &HADS
    Public Const CanErrRsrcLimitRtsi As Boolean = NICAN_ERROR_BASE Or &HCDS
    Public Const DnetErrNoReadAvail As Boolean = NICAN_ERROR_BASE Or &HES
    Public Const DnetErrBadMacId As Boolean = NICAN_ERROR_BASE Or &HFS
    Public Const DnetErrDevInitOther As Boolean = NICAN_ERROR_BASE Or &H10S
    Public Const DnetErrDevInitIoConn As Boolean = NICAN_ERROR_BASE Or &H30S
    Public Const DnetErrDevInitInputLen As Boolean = NICAN_ERROR_BASE Or &H50S
    Public Const DnetErrDevInitOutputLen As Boolean = NICAN_ERROR_BASE Or &H70S
    Public Const DnetErrDevInitEPR As Boolean = NICAN_ERROR_BASE Or &H90S
    Public Const DnetErrDevInitVendor As Boolean = NICAN_ERROR_BASE Or &HB0S
    Public Const DnetErrDevInitDevType As Boolean = NICAN_ERROR_BASE Or &HD0S
    Public Const DnetErrDevInitProdCode As Boolean = NICAN_ERROR_BASE Or &HF0S
    Public Const DnetErrDeviceMissing As Boolean = NICAN_ERROR_BASE Or &H11S
    Public Const DnetWarnDeviceMissing As Boolean = NICAN_WARNING_BASE Or &H11S
    Public Const DnetErrFragmentation As Boolean = NICAN_ERROR_BASE Or &H12S
    Public Const DnetErrIntfNotOpen As Boolean = NICAN_ERROR_BASE Or &H33S
    Public Const DnetErrErrorResponse As Boolean = NICAN_ERROR_BASE Or &H14S
    Public Const CanWarnNotificationPending As Boolean = NICAN_WARNING_BASE Or &H15S
    Public Const CanErrConfigOnly As Boolean = NICAN_ERROR_BASE Or &H17S
    Public Const CanErrPowerOnSelfTest As Boolean = NICAN_ERROR_BASE Or &H18S

    Public Const LinErrCommBit As Boolean = NICAN_ERROR_BASE Or &H1A0S
    Public Const LinErrCommFraming As Boolean = NICAN_ERROR_BASE Or &H1A1S
    Public Const LinErrCommResponseTimout As Boolean = NICAN_ERROR_BASE Or &H1A2S
    Public Const LinErrCommWakeup As Boolean = NICAN_ERROR_BASE Or &H1A3S
    Public Const LinErrCommForm As Boolean = NICAN_ERROR_BASE Or &H1A4S
    Public Const LinErrCommBusNoPowered As Boolean = NICAN_ERROR_BASE Or &H1A5S

    '//The percent difference between the passed in baud rate and the actual baud rate was greater than or equal to 0.5%.  LIN 2.0 specifies a clock tolerance of less than 0.5% for a master and less than 1.5% for a slave.
    Public Const LinWarnBaudRateOutOfTolerance As Boolean = NICAN_WARNING_BASE Or &H1A6S

    '// Numbers 0x100 to 0x1FF are used for the NI-CAN Frame API, the NI-DNET API, and LIN.
    '// Numbers 0x1A0 to 0x1DF are used for the NI-CAN Frame API for LIN .
    '// Numbers 0x200 to 0x2FF are used for the NI-CAN Channel API.
    '// Numbers 0x300 to 0x3FF are reserved for future use.
    Public Const CanErrMaxObjects As Boolean = NICAN_ERROR_BASE Or &H100S
    Public Const CanErrMaxChipSlots As Boolean = NICAN_ERROR_BASE Or &H101S
    Public Const CanErrBadDuration As Boolean = NICAN_ERROR_BASE Or &H102S
    Public Const CanErrFirmwareNoResponse As Boolean = NICAN_ERROR_BASE Or &H103S
    Public Const CanErrBadIdOrOpcode As Boolean = NICAN_ERROR_BASE Or &H104S
    Public Const CanWarnBadSizeOrLength As Boolean = NICAN_WARNING_BASE Or &H105S
    Public Const CanErrBadSizeOrLength As Boolean = NICAN_ERROR_BASE Or &H105S
    Public Const CanErrNotifAlreadyInUse As Boolean = NICAN_ERROR_BASE Or &H107S
    Public Const CanErrOneProtocolPerCard As Boolean = NICAN_ERROR_BASE Or &H108S
    Public Const CanWarnPeriodsTooFast As Boolean = NICAN_WARNING_BASE Or &H109S
    Public Const CanErrDllNotFound As Boolean = NICAN_ERROR_BASE Or &H10AS
    Public Const CanErrFunctionNotFound As Boolean = NICAN_ERROR_BASE Or &H10BS
    Public Const CanErrLangIntfRsrcUnavail As Boolean = NICAN_ERROR_BASE Or &H10CS
    Public Const CanErrRequiresNewHwSeries As Boolean = NICAN_ERROR_BASE Or &H10DS
    Public Const CanErrHardwareNotSupported As Boolean = NICAN_ERROR_BASE Or &H10DS
    Public Const CanErrSeriesOneOnly As Boolean = NICAN_ERROR_BASE Or &H10ES
    Public Const CanErrSetAbsTime As Boolean = NICAN_ERROR_BASE Or &H10FS
    Public Const CanErrBothApiSameIntf As Boolean = NICAN_ERROR_BASE Or &H110S
    Public Const CanErrWaitOverlapsSameObj As Boolean = NICAN_ERROR_BASE Or &H111S
    Public Const CanErrNotStarted As Boolean = NICAN_ERROR_BASE Or &H112S
    Public Const CanErrConnectTwice As Boolean = NICAN_ERROR_BASE Or &H113S
    Public Const CanErrConnectUnsupported As Boolean = NICAN_ERROR_BASE Or &H114S
    Public Const CanErrStartTrigBeforeFunc As Boolean = NICAN_ERROR_BASE Or &H115S
    Public Const CanErrStringSizeTooLarge As Boolean = NICAN_ERROR_BASE Or &H116S
    Public Const CanErrQueueReqdForReadMult As Boolean = NICAN_ERROR_BASE Or &H117S
    Public Const CanErrHardwareInitFailed As Boolean = NICAN_ERROR_BASE Or &H118S
    Public Const CanErrOldDataLost As Boolean = NICAN_ERROR_BASE Or &H119S
    Public Const CanErrOverflowChannel As Boolean = NICAN_ERROR_BASE Or &H11AS
    Public Const CanErrUnsupportedModeMix As Boolean = NICAN_ERROR_BASE Or &H11CS
    Public Const CanErrNoNetIntfConfig As Boolean = NICAN_ERROR_BASE Or &H11DS
    Public Const CanErrBadTransceiverMode As Boolean = NICAN_ERROR_BASE Or &H11ES
    Public Const CanErrWrongTransceiverAttr As Boolean = NICAN_ERROR_BASE Or &H11FS
    Public Const CanErrRequiresXS As Boolean = NICAN_ERROR_BASE Or &H120S
    Public Const CanErrDisconnected As Boolean = NICAN_ERROR_BASE Or &H121S
    Public Const CanErrNoTxForListenOnly As Boolean = NICAN_ERROR_BASE Or &H122S
    Public Const CanErrSetOnly As Boolean = NICAN_ERROR_BASE Or &H123S
    Public Const CanErrBadBaudRate As Boolean = NICAN_ERROR_BASE Or &H124S
    Public Const CanErrOverflowFrame As Boolean = NICAN_ERROR_BASE Or &H125S
    Public Const CanWarnRTSITooFast As Boolean = NICAN_WARNING_BASE Or &H126S
    Public Const CanErrNoTimebase As Boolean = NICAN_ERROR_BASE Or &H127S
    Public Const CanErrTimerRunning As Boolean = NICAN_ERROR_BASE Or &H128S
    Public Const DnetErrUnsupportedHardware As Boolean = NICAN_ERROR_BASE Or &H129S
    Public Const CanErrInvalidLogfile As Boolean = NICAN_ERROR_BASE Or &H12AS
    Public Const CanErrMaxPeriodicObjects As Boolean = NICAN_ERROR_BASE Or &H130S
    Public Const CanErrUnknownHardwareAttribute As Boolean = NICAN_ERROR_BASE Or &H131S
    Public Const CanErrDelayFrameNotSupported As Boolean = NICAN_ERROR_BASE Or &H132S
    Public Const CanErrVirtualBusTimingOnly As Boolean = NICAN_ERROR_BASE Or &H133S

    Public Const CanErrVirtualNotSupported As Boolean = NICAN_ERROR_BASE Or &H135S
    Public Const CanErrWriteMultLimit As Boolean = NICAN_ERROR_BASE Or &H136S
    Public Const CanErrObsoletedHardware As Boolean = NICAN_ERROR_BASE Or &H137S
    Public Const CanErrVirtualBusTimingMismatch As Boolean = NICAN_ERROR_BASE Or &H138S
    Public Const CanErrVirtualBusOnly As Boolean = NICAN_ERROR_BASE Or &H139S
    Public Const CanErrConversionTimeRollback As Boolean = NICAN_ERROR_BASE Or &H13AS
    Public Const CanErrInterFrameDelayExceeded As Boolean = NICAN_ERROR_BASE Or &H140S
    Public Const CanErrLogConflict As Boolean = NICAN_ERROR_BASE Or &H141S
    Public Const CanErrBootLoaderUpdated As Boolean = NICAN_ERROR_BASE Or &H142S

    '/* Included for backward compatibility with older versions of NI-CAN */
    Public Const CanWarnLowSpeedXcvr As Boolean = NICAN_WARNING_BASE Or &HCS
    Public Const CanErrOverflowRead As Boolean = NICAN_ERROR_BASE Or &H28S

#End Region


#Region "Attribute Constants"
    '/***********************************************************************
    '                          A T T R I B U T E   I D S
    '***********************************************************************/

    '/* Attributes of the NI driver (NCTYPE_ATTRID values)
    '      For every attribute ID, its full name, datatype,
    '      permissions, and applicable objects are listed in the comment.
    '   */

    '/* Current State, NCTYPE_STATE, Get, CAN Interface/Object */
    Public Const NC_ATTR_STATE As Integer = &H80000009
    '/* Status, NCTYPE_STATUS, Get, CAN Interface/Object */
    Public Const NC_ATTR_STATUS As Integer = &H8000000A
    '/* Baud Rate, Set, CAN Interface
    '            Note that in addition to standard baud rates like 125000,
    '            this attribute also allows you to program non-standard
    '            or otherwise uncommon baud rates.  If bit 31 (0x80000000)
    '            is set, the low 16 bits of this attribute are programmed
    '            directly into the bit timing registers of the CAN
    '            communications controller.  The low byte is programmed as
    '            BTR0 of the Intel 82527 chip (8MHz clock), and the high byte
    '            as BTR1, resulting in the following bit map:
    '               15  14  13  12  11  10   9   8   7   6   5   4   3   2   1   0
    '               sam (tseg2 - 1) (  tseg1 - 1   ) (sjw-1) (     presc - 1     )
    '            For example, baud rate 0x80001C03 programs the CAN communications
    '            controller for 125000 baud (same baud rate 125000 decimal).
    '            For more information, refer to the reference manual for
    '            any CAN communications controller chip.  */
    Public Const NC_ATTR_BAUD_RATE As Integer = &H80000007
    '/* Start On Open, NCTYPE_BOOL, Set, CAN Interface */
    Public Const NC_ATTR_START_ON_OPEN As Integer = &H80000006
    '/* Absolute Time, NCTYPE_ABS_TIME, Get, CAN Interface */
    Public Const NC_ATTR_ABS_TIME As Integer = &H80000008
    '/* Period, NCTYPE_DURATION, Set, CAN Object */
    Public Const NC_ATTR_PERIOD As Integer = &H8000000F
    '/* Timestamping, NCTYPE_BOOL, Set, CAN Interface */
    Public Const NC_ATTR_TIMESTAMPING As Integer = &H80000010
    '/* Read Pending, NCTYPE_UINT32, Get, CAN Interface/Object */
    Public Const NC_ATTR_READ_PENDING As Integer = &H80000011
    '/* Write Pending, NCTYPE_UINT32, Get, CAN Interface/Object */
    Public Const NC_ATTR_WRITE_PENDING As Integer = &H80000012
    '/* Read Queue Length, NCTYPE_UINT32, Set, CAN Interface/Object */
    Public Const NC_ATTR_READ_Q_LEN As Integer = &H80000013
    '/* Write Queue Length, NCTYPE_UINT32, Set, CAN Interface/Object */
    Public Const NC_ATTR_WRITE_Q_LEN As Integer = &H80000014
    '/* Receive Changes Only, NCTYPE_BOOL, Set, CAN Object */
    Public Const NC_ATTR_RX_CHANGES_ONLY As Integer = &H80000015
    '/* Communication Type, NCTYPE_COMM_TYPE, Set, CAN Object */
    Public Const NC_ATTR_COMM_TYPE As Integer = &H80000016
    '/* RTSI Mode, NCTYPE_RTSI_MODE, Set, CAN Interface/Object */
    Public Const NC_ATTR_RTSI_MODE As Integer = &H80000017
    '/* RTSI Signal, NCTYPE_UINT8, Set, CAN Interface/Object */
    Public Const NC_ATTR_RTSI_SIGNAL As Integer = &H80000018
    '/* RTSI Signal Behavior, NCTYPE_RTSI_SIG_BEHAV,
    '         Set, CAN Interface/Object */
    Public Const NC_ATTR_RTSI_SIG_BEHAV As Integer = &H80000019
    '/* RTSI Frame for CAN Object, NCTYPE_UINT32, Get/Set, CAN Object */
    Public Const NC_ATTR_RTSI_FRAME As Integer = &H80000020
    '/* RTSI Skip Count, NCTYPE_UINT32, Set, CAN Interface/Object */
    Public Const NC_ATTR_RTSI_SKIP As Integer = &H80000021

    '/* Standard Comparator, NCTYPE_CAN_ARBID, Set, CAN Interface */
    Public Const NC_ATTR_COMP_STD As Integer = &H80010001
    '/* Standard Mask (11 bits), NCTYPE_UINT32, Set, CAN Interface */
    Public Const NC_ATTR_MASK_STD As Integer = &H80010002
    '/* Extended Comparator (29 bits), NCTYPE_CAN_ARBID, Set, CAN Interface */
    Public Const NC_ATTR_COMP_XTD As Integer = &H80010003
    '/* Extended Mask (29 bits), NCTYPE_UINT32, Set, CAN Interface */
    Public Const NC_ATTR_MASK_XTD As Integer = &H80010004
    '/* Transmit By Response, NCTYPE_BOOL, Set, CAN Object */
    Public Const NC_ATTR_TX_RESPONSE As Integer = &H80010006
    '/* Data Frame Length, NCTYPE_UINT32, Set, CAN Object */
    Public Const NC_ATTR_DATA_LEN As Integer = &H80010007
    '/* Log Comm Errors, NCTYPE_BOOL, Get/Set,
    '         CAN Interface (Low-speed boards only) */
    Public Const NC_ATTR_LOG_COMM_ERRS As Integer = &H8001000A
    '/* Notify Multiple Length, NCTYPE_UINT32, Get/Set, CAN Interface/Object */
    Public Const NC_ATTR_NOTIFY_MULT_LEN As Integer = &H8001000B
    '/* Receive Queue Length, NCTYPE_UINT32, Set, CAN Interface */
    Public Const NC_ATTR_RX_Q_LEN As Integer = &H8001000C
    '/* Is bus timing enabled on virtual hardware,
    '            NC_FALSE: Used in case of frame-channel conversion
    '            NC_TRUE: NI-CAN will simluate the bus timing on TX, set timestamp
    '            of the frame when it is received,default behavior,NCTYPE_UINT32,Get/Set,
    '            CAN Interface   */

    Public Const NC_ATTR_VIRTUAL_BUS_TIMING As Integer = &HA0000031
    '/* Transmit mode,NCTYPE_UINT32,Get/Set,
    '             0 (immediate) [default], 1 (timestamped)
    '            CAN Interface */
    Public Const NC_ATTR_TRANSMIT_MODE As Integer = &H80020029
    '/* Log start trigger in the read queue,NCTYPE_UINT32,Get/Set,
    '            CAN Interface*/
    Public Const NC_ATTR_LOG_START_TRIGGER As Integer = &H80020031
    '/* Timestamp format in absolute or relative mode, NCTYPE_UINT32, Get/Set,
    '            CAN Interface .*/
    Public Const NC_ATTR_TIMESTAMP_FORMAT As Integer = &H80020032
    '/* Rate of the incoming clock pulses.Rate can either be
    '            10(M Series DAQ) or 20(E series DAQ) Mhz,NCTYPE_UINT32,Get/Set,
    '            CAN Interface */
    Public Const NC_ATTR_MASTER_TIMEBASE_RATE As Integer = &H80020033
    '/* Number of frames that can be written without overflow,NCTYPE_UINT32, Get,CAN Interface*/
    Public Const NC_ATTR_WRITE_ENTRIES_FREE As Integer = &H80020034
    '/* Timeline recovery,NCTYPE_UINT32,Get/Set,
    '             Valid only in timestamped transmit mode (NC_ATTR_TRANSMIT_MODE = 1)
    '             0 (false) [default], 1 (true)
    '             CAN Interface .*/

    Public Const NC_ATTR_TIMELINE_RECOVERY As Integer = &H80020035
    '/* Log Bus Errors, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             0 (false) [default], 1 (true)*/
    Public Const NC_ATTR_LOG_BUS_ERROR As Integer = &H80020037
    '/* Log Transceiver Faults, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             Log NERR into NI read queue
    '             0 (false) [default], 1 (true)*/
    Public Const NC_ATTR_LOG_TRANSCEIVER_FAULT As Integer = &H80020038
    '/* Termination Select, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             CAN HS - Not Selectable
    '             CAN LS - 0 (1k ohm) [default], 1 (5k ohm)
    '             LIN - 0 (disabled) [default], 1 (enabled) */
    Public Const NC_ATTR_TERMINATION As Integer = &H80020041
    '/*** LIN Attributes **/
    '/* LIN - Sleep, NCTYPE_UINT32, Get/Set,
    '             0 (False) [default], 1 (true) */
    Public Const NC_ATTR_LIN_SLEEP As Integer = &H80020042
    '/* LIN - Check Sum Type, NCTYPE_UINT32, Get/Set,
    '             0 (classic) [default], 1 (enhanced) */
    Public Const NC_ATTR_LIN_CHECKSUM_TYPE As Integer = &H80020043
    '/* LIN - Response Timeout, NCTYPE_UINT32, Get/Set,
    '             0 [default], 1 - 65535 (in 50 us increments for LIN specific frame response timing) */
    Public Const NC_ATTR_LIN_RESPONSE_TIMEOUT As Integer = &H80020044
    '/* LIN - Enable DLC Check, NCTYPE_UINT32, Get/Set,
    '             0 (false) [default], 1 (true) */
    Public Const NC_ATTR_LIN_ENABLE_DLC_CHECK As Integer = &H80020045
    '/* LIN - Log Wakeup, NCTYPE_UINT32, Get/Set,
    '             0 (false) [default], 1 (true) */
    Public Const NC_ATTR_LIN_LOG_WAKEUP As Integer = &H80020046

    '/* Attributes specific to Series 2 hardware (not supported on Series 1). */

    '/* Enable Listen Only on SJA1000, NCTYPE_UINT32, Get/Set
    '             0 (false) [default], 1 (enable)*/
    Public Const NC_ATTR_LISTEN_ONLY As Integer = &H80010010
    '/* Returns Receive Error Counter from SJA1000, NCTYPE_UINT32, Get, CAN Interface/Object */
    Public Const NC_ATTR_RX_ERROR_COUNTER As Integer = &H80010011
    '/* Returns Send Error Counter from SJA1000, NCTYPE_UINT32, Get, CAN Interface/Object */
    Public Const NC_ATTR_TX_ERROR_COUNTER As Integer = &H80010012
    '/* Series 2 Comparator, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             11 bits or 29 bits depending on NC_ATTR_SERIES2_FILTER_MODE
    '             0 [default] */
    Public Const NC_ATTR_SERIES2_COMP As Integer = &H80010013
    '/* Series 2 Mask, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             11 bits or 29 bits depending on NC_ATTR_SERIES2_FILTER_MODE
    '             0xFFFFFFFF [default] */
    Public Const NC_ATTR_SERIES2_MASK As Integer = &H80010014
    '/* Series 2 Filter Mode, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             NC_FILTER_SINGLE_STANDARD [default], NC_FILTER_SINGLE_EXTENDED,
    '             NC_FILTER_DUAL_EXTENDED, NC_FILTER_SINGLE_STANDARD*/
    Public Const NC_ATTR_SERIES2_FILTER_MODE As Integer = &H80010015
    '/* Self Reception, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             Echo transmitted frames in read queue
    '             0 (false) [default], 1 (true) */
    Public Const NC_ATTR_SELF_RECEPTION As Integer = &H80010016
    '/* Single Shot Transmit, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             Single Shot = No retry on error transmissions
    '             0 (false) [default]. 1 (true) */
    Public Const NC_ATTR_SINGLE_SHOT_TX As Integer = &H80010017
    Public Const NC_ATTR_BEHAV_FINAL_OUT As Integer = &H80010018
    '/* Transceiver Mode, NCTYPE_UINT32, Get/Set, CAN Interface/Object
    '             NC_TRANSCEIVER_MODE_NORMAL [default], NC_TRANSCEIVER_MODE_SLEEP,
    '             NC_TRANSCEIVER_MODE_SW_HIGHSPEED, NC_TRANSCEIVER_MODE_SW_WAKEUP*/
    Public Const NC_ATTR_TRANSCEIVER_MODE As Integer = &H80010019
    '/* Transceiver External Out, NCTYPE_UINT32, Bitmask, Get/Set, CAN Interface
    '             on XS cards and external transceivers, it sets MODE0 and MODE1 pins on CAN port,
    '             and sleep of CAN controller chip
    '             NC_TRANSCEIVER_OUT_MODE0 | NC_TRANSCEIVER_OUT_MODE1 [default]
    '             NC_TRANSCEIVER_OUT_MODE0, NC_TRANSCEIVER_OUT_MODE1, NC_TRANSCEIVER_OUT_SLEEP*/

    Public Const NC_ATTR_TRANSCEIVER_EXTERNAL_OUT As Integer = &H8001001A
    '/* Transceiver External In, NCTYPE_UINT32, Get, CAN Interface
    '             on XS cards, reads STATUS pin on CAN port*/
    Public Const NC_ATTR_TRANSCEIVER_EXTERNAL_IN As Integer = &H8001001B
    '/* Error Code Capture and Arbitration Lost Capture, NCTYPE_UINT32, Get, CAN Interface/Object
    '             Returns Error Code Capture and Arbitration Lost Capture registers */
    Public Const NC_ATTR_SERIES2_ERR_ARB_CAPTURE As Integer = &H8001001C
    '/* Transceiver Type, NCTYPE_UINT32, Get/Set, CAN Interface
    '             NC_TRANSCEIVER_TYPE_DISC (disconnect), NC_TRANSCEIVER_TYPE_EXT (external),
    '             NC_TRANSCEIVER_TYPE_HS (high speed), NC_TRANSCEIVER_TYPE_LS (low speed),
    '             NC_TRANSCEIVER_TYPE_SW (single wire)*/
    Public Const NC_ATTR_TRANSCEIVER_TYPE As Integer = &H80020007

    '/* Informational attributes (hardware and version info).  Get, CAN Interface only
    '   These attribute IDs can be used with ncGetHardwareInfo and ncGetAttribute.  */
    Public Const NC_ATTR_NUM_CARDS As Integer = &H80020002
    Public Const NC_ATTR_HW_SERIAL_NUM As Integer = &H80020003
    Public Const NC_ATTR_HW_FORMFACTOR As Integer = &H80020004
    Public Const NC_ATTR_HW_SERIES As Integer = &H80020005
    Public Const NC_ATTR_NUM_PORTS As Integer = &H80020006
    Public Const NC_ATTR_HW_TRANSCEIVER As Integer = &H80020007
    Public Const NC_ATTR_INTERFACE_NUM As Integer = &H80020008
    Public Const NC_ATTR_VERSION_MAJOR As Integer = &H80020009
    Public Const NC_ATTR_VERSION_MINOR As Integer = &H8002000A
    Public Const NC_ATTR_VERSION_UPDATE As Integer = &H8002000B
    Public Const NC_ATTR_VERSION_PHASE As Integer = &H8002000C
    Public Const NC_ATTR_VERSION_BUILD As Integer = &H8002000D
    Public Const NC_ATTR_VERSION_COMMENT As Integer = &H8002000E

    '/* Included for backward compatibility with older versions of NI-CAN */
    '/* NCTYPE_ATTRID values, CAN Interface/Object */
    Public Const NC_ATTR_PROTOCOL As Integer = &H80000001
    Public Const NC_ATTR_PROTOCOL_VERSION As Integer = &H80000002
    Public Const NC_ATTR_SOFTWARE_VERSION As Integer = &H80000003
    Public Const NC_ATTR_BKD_READ_SIZE As Integer = &H8000000B
    Public Const NC_ATTR_BKD_WRITE_SIZE As Integer = &H8000000C
    Public Const NC_ATTR_BKD_TYPE As Integer = &H8000000D
    Public Const NC_ATTR_BKD_WHEN_USED As Integer = &H8000000E
    Public Const NC_ATTR_BKD_PERIOD As Integer = &H8000000F
    Public Const NC_ATTR_BKD_CHANGES_ONLY As Integer = &H80000015
    Public Const NC_ATTR_SERIAL_NUMBER As Integer = &H800000A0
    Public Const NC_ATTR_CAN_BIT_TIMINGS As Integer = &H80010005
    Public Const NC_ATTR_BKD_CAN_RESPONSE As Integer = &H80010006
    Public Const NC_ATTR_CAN_DATA_LENGTH As Integer = &H80010007
    Public Const NC_ATTR_CAN_COMP_STD As Integer = &H80010001
    Public Const NC_ATTR_CAN_MASK_STD As Integer = &H80010002
    Public Const NC_ATTR_CAN_COMP_XTD As Integer = &H80010003
    Public Const NC_ATTR_CAN_MASK_XTD As Integer = &H80010004
    Public Const NC_ATTR_CAN_TX_RESPONSE As Integer = &H80010006
    Public Const NC_ATTR_NOTIFY_MULT_SIZE As Integer = &H8001000B
    Public Const NC_ATTR_RESET_ON_START As Integer = &H80010008
    Public Const NC_ATTR_NET_SYNC_COUNT As Integer = &H8001000D
    Public Const NC_ATTR_IS_NET_SYNC As Integer = &H8001000E
    Public Const NC_ATTR_START_TRIG_BEHAVIOR As Integer = &H80010023
    '/* NCTYPE_BKD_TYPE values */
    Public Const NC_BKD_TYPE_PEER2PEER As Short = &H1S
    Public Const NC_BKD_TYPE_REQUEST As Short = &H2S
    Public Const NC_BKD_TYPE_RESPONSE As Short = &H3S
    '/* NCTYPE_BKD_WHEN values */
    Public Const NC_BKD_WHEN_PERIODIC As Short = &H1S
    Public Const NC_BKD_WHEN_UNSOLICITED As Short = &H2S
    '/* Special values for background read/write data
    '      sizes (NC_ATTR_BKD_READ_SIZE and NC_ATTR_BKD_WRITE_SIZE). */
    Public Const NC_BKD_CAN_ZERO_SIZE As Short = &H8000S

#End Region

#Region "Misc. Constants"
    '/***********************************************************************
    '                    O T H E R   C O N S T A N T S
    '***********************************************************************/

    '/* NCTYPE_BOOL (true/false values) */
    Public Const NC_TRUE As Short = 1
    Public Const NC_FALSE As Short = 0

    '/* NCTYPE_DURATION (values in one millisecond ticks) */
    Public Const NC_DURATION_NONE As Short = 0
    Public Const NC_DURATION_INFINITE As Integer = &HFFFFFFFF
    Public Const NC_DURATION_1MS As Short = 1
    Public Const NC_DURATION_10MS As Short = 10
    Public Const NC_DURATION_100MS As Short = 100
    Public Const NC_DURATION_1SEC As Short = 1000
    Public Const NC_DURATION_10SEC As Short = 10000
    Public Const NC_DURATION_100SEC As Integer = 100000
    Public Const NC_DURATION_1MIN As Integer = 60000

    '/* NCTYPE_PROTOCOL (values for supported protocols) */
    Public Const NC_PROTOCOL_CAN As Short = 1
    Public Const NC_PROTOCOL_DNET As Short = 2
    Public Const NC_PROTOCOL_LIN As Short = 3

    '/* NCTYPE_STATE (bit masks for states).
    '   Refer to other NC_ST values below for backward compatibility. */
    Public Enum NC_STATE_ENUM
        '/* Any object */
        NC_ST_READ_AVAIL = &H1S
        '/* Any object */
        NC_ST_WRITE_SUCCESS = &H2S
        '/* Expl Msg object only */
        NC_ST_ESTABLISHED = &H8S
        '/* Included for backward compatibility with older versions of NI-CAN */
        '/* NCTYPE_STATE (bit masks for states): Prior to NI-CAN 2.0,
        '      the Stopped state emabled detection of the Bus Off condition,
        '      which stopped communication independent of NI-CAN functions such as ncAction(Stop).
        '      Since the Bus Off condition is an error, and errors are detected automatically
        '      in v2.0 and later, this state is now obsolete. For compatibility, it may be
        '      returned as the DetectedState of ncWaitForState, but this bit should be
        '      ignored by new NI-CAN applications. */
        NC_ST_STOPPED = &H4S
        '/* NCTYPE_STATE (bit masks for states): Prior to NI-CAN 2.0,
        '      the Error state emabled detection of background errors (such as the Bus Off condition).
        '      For v2.0 and later, the ncWaitForState function returns automatically when any
        '      error occurs, so this state is now obsolete. For compatibility, it may be
        '      returned as the DetectedState of ncWaitForState, but this bit should be
        '      ignored by new NI-CAN applications. */
        NC_ST_ERROR = &H10S
        '/* NCTYPE_STATE (bit masks for states): Prior to NI-CAN 2.0,
        '      the Warning state emabled detection of background warnings (such as Error Passive).
        '      For v2.0 and later, the ncWaitForState function will not abort when a warning occurs,
        '      but it will return the warning, so this state is now obsolete. For compatibility,
        '      it may be returned as the DetectedState of ncWaitForState, but this bit should be
        '      ignored by new NI-CAN applications. */
        NC_ST_WARNING = &H20S
    End Enum

    '/* Any object */
    Public Const NC_ST_READ_MULT As Short = &H8S
    '/* State to detect when a CAN port has been woken up remotely.*/
    Public Const NC_ST_WRITE_SUCCESS As Integer = &H2S
    Public Const NC_ST_REMOTE_WAKEUP As Short = &H40S

    '/* NI only */
    Public Const NC_ST_WRITE_MULT As Short = &H80S

    '/* NCTYPE_OPCODE values */
    Public Enum NC_OPCODE_ENUM
        '/* Interface object */
        NC_OP_START = &H80000001
        '/* Interface object */
        NC_OP_STOP = &H80000002
        '/* Interface object */
        NC_OP_RESET = &H80000003
        '/* Interface object only */
        NC_OP_ACTIVE = &H80000004
        '/* Interface object only */
        NC_OP_IDLE = &H80000005
    End Enum

    '/* Interface object, Param is used */
    Public Const NC_OP_RTSI_OUT As Integer = &H80000004

    '/* NCTYPE_BAUD_RATE (values for baud rates) */
    Public Const NC_BAUD_10K As Short = 10000
    Public Const NC_BAUD_100K As Integer = 100000
    Public Const NC_BAUD_125K As Integer = 125000
    Public Const NC_BAUD_250K As Integer = 250000
    Public Const NC_BAUD_500K As Integer = 500000
    Public Const NC_BAUD_1000K As Integer = 1000000

    '/* NCTYPE_COMM_TYPE values */
    Public Const NC_CAN_COMM_RX_UNSOL As Short = &H0S
    Public Const NC_CAN_COMM_TX_BY_CALL As Short = &H1S
    Public Const NC_CAN_COMM_RX_PERIODIC As Short = &H2S
    Public Const NC_CAN_COMM_TX_PERIODIC As Short = &H3S
    Public Const NC_CAN_COMM_RX_BY_CALL As Short = &H4S
    Public Const NC_CAN_COMM_TX_RESP_ONLY As Short = &H5S
    Public Const NC_CAN_COMM_TX_WAVEFORM As Short = &H6S

    '/* NCTYPE_RTSI_MODE values */
    Public Const NC_RTSI_NONE As Short = 0
    Public Const NC_RTSI_TX_ON_IN As Short = 1
    Public Const NC_RTSI_TIME_ON_IN As Short = 2
    Public Const NC_RTSI_OUT_ON_RX As Short = 3
    Public Const NC_RTSI_OUT_ON_TX As Short = 4
    Public Const NC_RTSI_OUT_ACTION_ONLY As Short = 5

    '/* NCTYPE_RTSI_SIG_BEHAV values */
    Public Const NC_RTSISIG_PULSE As Short = 0
    Public Const NC_RTSISIG_TOGGLE As Short = 1

    '/* NC_ATTR_START_TRIG_BEHAVIOUR values */
    Public Const NC_START_TRIG_NONE As Short = 0
    Public Const NC_RESET_TIMESTAMP_ON_START As Short = 1
    Public Const NC_LOG_START_TRIG As Short = 2

    '/* NCTYPE_CAN_ARBID (bit masks) */
    '/* When frame type is data (NC_FRMTYPE_DATA) or remote (NC_FRMTYPE_REMOTE),
    '         this bit in ArbitrationId is interpreted as follows:
    '         If this bit is clear, the ArbitrationId is standard (11-bit).
    '         If this bit is set, the ArbitrationId is extended (29-bit).  */
    Public Const NC_FL_CAN_ARBID_XTD As Integer = &H20000000

    '/* NCTYPE_CAN_ARBID (special values) */
    '/* Special value used to disable comparators. */

    Public Const NC_CAN_ARBID_NONE As Integer = &HCFFFFFFF

    '/* Values for the FrameType (IsRemote) field of CAN frames.  */
    Public Const NC_FRMTYPE_DATA As Short = 0
    Public Const NC_FRMTYPE_REMOTE As Short = &H1S
    '/* NI only */
    Public Const NC_FRMTYPE_COMM_ERR As Short = &H2S
    Public Const NC_FRMTYPE_RTSI As Short = &H3S

    '/* Status for Driver NetIntf (and Driver CanObjs):
    '            ArbID=0
    '            DataLength=0
    '            Timestamp=<time of start trigger>
    '  */
    Public Const NC_FRMTYPE_TRIG_START As Short = &H4S
    Public Const NC_FRMTYPE_DELAY As Short = &H5S

    '/* CAN Frame to indicate Bus Error. Format: Byte 0:CommState,Byte1:Tx Err Ctr,
    '     Byte2: Rx Err Ctr, Byte 3: ECC register. Byte 4-7: Don't care.*/
    Public Const NC_FRMTYPE_BUS_ERR As Short = &H6S
    '/* Frame to indicate status of Nerr. Byte 0: 1 = NERR , 0 = No Fault.*/
    Public Const NC_FRMTYPE_TRANSCEIVER_ERR As Short = &H7S

    '//Response frame for LIN communication
    Public Const NC_FRMTYPE_LIN_RESPONSE_ENTRY As Short = &H10S
    '//Header frame for LIN communication
    Public Const NC_FRMTYPE_LIN_HEADER As Short = &H11S
    '//Full frame for LIN communication
    Public Const NC_FRMTYPE_LIN_FULL As Short = &H12S
    '//Wakeup frame for LIN communication
    Public Const NC_FRMTYPE_LIN_WAKEUP_RECEIVED As Short = &H13S
    '//Sleep frame for LIN communication
    Public Const NC_FRMTYPE_LIN_BUS_INACTIVE As Short = &H14S
    '//Bus error frame for LIN communication
    Public Const NC_FRMTYPE_LIN_BUS_ERR As Short = &H15S

    '/* Special values for CAN mask attributes (NC_ATTR_MASK_STD/XTD) */
    Public Const NC_MASK_STD_MUSTMATCH As Short = &H7FFS
    Public Const NC_MASK_XTD_MUSTMATCH As Integer = &H1FFFFFFF
    Public Const NC_MASK_STD_DONTCARE As Short = &H0S
    Public Const NC_MASK_XTD_DONTCARE As Short = &H0S
    Public Const NC_SERIES2_MASK_MUSTMATCH As Short = &H0S
    Public Const NC_SERIES2_MASK_DONTCARE As Integer = &HFFFFFFFF

    '// Values for NC_ATTR_HW_SERIES attribute
    Public Const NC_HW_SERIES_1 As Short = 0
    Public Const NC_HW_SERIES_2 As Short = 1
    Public Const NC_HW_SERIES_847X As Short = 2
    Public Const NC_HW_SERIES_847X_SYNC As Short = 3

    '// Values for SourceTerminal of ncConnectTerminals.
    Public Const NC_SRC_TERM_RTSI0 As Short = 0
    Public Const NC_SRC_TERM_RTSI1 As Short = 1
    Public Const NC_SRC_TERM_RTSI2 As Short = 2
    Public Const NC_SRC_TERM_RTSI3 As Short = 3
    Public Const NC_SRC_TERM_RTSI4 As Short = 4
    Public Const NC_SRC_TERM_RTSI5 As Short = 5
    Public Const NC_SRC_TERM_RTSI6 As Short = 6
    Public Const NC_SRC_TERM_RTSI_CLOCK As Short = 7
    Public Const NC_SRC_TERM_PXI_STAR As Short = 8
    Public Const NC_SRC_TERM_INTF_RECEIVE_EVENT As Short = 9
    Public Const NC_SRC_TERM_INTF_TRANSCEIVER_EVENT As Short = 10
    Public Const NC_SRC_TERM_PXI_CLK10 As Short = 11
    Public Const NC_SRC_TERM_20MHZ_TIMEBASE As Short = 12
    Public Const NC_SRC_TERM_10HZ_RESYNC_CLOCK As Short = 13
    Public Const NC_SRC_TERM_START_TRIGGER As Short = 14

    '// Values for DestinationTerminal of ncConnectTerminals.
    Public Const NC_DEST_TERM_RTSI0 As Short = 0
    Public Const NC_DEST_TERM_RTSI1 As Short = 1
    Public Const NC_DEST_TERM_RTSI2 As Short = 2
    Public Const NC_DEST_TERM_RTSI3 As Short = 3
    Public Const NC_DEST_TERM_RTSI4 As Short = 4
    Public Const NC_DEST_TERM_RTSI5 As Short = 5
    Public Const NC_DEST_TERM_RTSI6 As Short = 6
    Public Const NC_DEST_TERM_RTSI_CLOCK As Short = 7
    Public Const NC_DEST_TERM_MASTER_TIMEBASE As Short = 8
    Public Const NC_DEST_TERM_10HZ_RESYNC_CLOCK As Short = 9
    Public Const NC_DEST_TERM_START_TRIGGER As Short = 10

    '// Values for NC_ATTR_HW_FORMFACTOR attribute
    Public Const NC_HW_FORMFACTOR_PCI As Short = 0
    Public Const NC_HW_FORMFACTOR_PXI As Short = 1
    Public Const NC_HW_FORMFACTOR_PCMCIA As Short = 2
    Public Const NC_HW_FORMFACTOR_AT As Short = 3
    Public Const NC_HW_FORMFACTOR_USB As Short = 4

    '// Values for NC_ATTR_TRANSCEIVER_TYPE attribute
    Public Const NC_TRANSCEIVER_TYPE_HS As Short = 0
    Public Const NC_TRANSCEIVER_TYPE_LS As Short = 1
    Public Const NC_TRANSCEIVER_TYPE_SW As Short = 2
    Public Const NC_TRANSCEIVER_TYPE_EXT As Short = 3
    Public Const NC_TRANSCEIVER_TYPE_DISC As Short = 4
    Public Const NC_TRANSCEIVER_TYPE_LIN As Short = 5
    Public Const NC_TRANSCEIVER_TYPE_UNKNOWN As Short = &HFFS

    '// Values for legacy NC_ATTR_HW_TRANSCEIVER attribute
    Public Const NC_HW_TRANSCEIVER_HS As Short = 0
    Public Const NC_HW_TRANSCEIVER_LS As Short = 1
    Public Const NC_HW_TRANSCEIVER_SW As Short = 2
    Public Const NC_HW_TRANSCEIVER_EXT As Short = 3
    Public Const NC_HW_TRANSCEIVER_DISC As Short = 4

    '// Values for NC_ATTR_TRANSCEIVER_MODE attribute.
    Public Const NC_TRANSCEIVER_MODE_NORMAL As Short = 0
    Public Const NC_TRANSCEIVER_MODE_SLEEP As Short = 1
    Public Const NC_TRANSCEIVER_MODE_SW_WAKEUP As Short = 2
    Public Const NC_TRANSCEIVER_MODE_SW_HIGHSPEED As Short = 3

    '// Values for NC_ATTR_BEHAV_FINAL_OUT attribute (CAN Objs of type NC_CAN_COMM_TX_PERIODIC)
    Public Const NC_OUT_BEHAV_REPEAT_FINAL As Short = 0
    Public Const NC_OUT_BEHAV_CEASE_TRANSMIT As Short = 1

    '// Values for NC_ATTR_SERIES2_FILTER_MODE
    Public Const NC_FILTER_SINGLE_STANDARD As Short = 0
    Public Const NC_FILTER_SINGLE_EXTENDED As Short = 1
    Public Const NC_FILTER_DUAL_STANDARD As Short = 2
    Public Const NC_FILTER_DUAL_EXTENDED As Short = 3

    '// Values for SourceTerminal of ncConnectTerminals.
    Public Const NC_SRC_TERM_10HZ_RESYNC_EVENT As Short = 13
    Public Const NC_SRC_TERM_START_TRIG_EVENT As Short = 14
    '// Values for DestinationTerminal of ncConnectTerminals.
    Public Const NC_DEST_TERM_10HZ_RESYNC As Short = 9
    Public Const NC_DEST_TERM_START_TRIG As Short = 10
    '/* NCTYPE_VERSION (NC_ATTR_SOFTWARE_VERSION); ncGetHardwareInfo preferable */
    Public Const NC_MK_VER_MAJOR As Integer = &HFF000000
    Public Const NC_MK_VER_MINOR As Integer = &HFF0000
    Public Const NC_MK_VER_SUBMINOR As Short = &HFF00S
    Public Const NC_MK_VER_BETA As Short = &HFFS
    '/* ArbitrationId; use IsRemote or FrameType to determine RTSI frame. */
    Public Const NC_FL_CAN_ARBID_INFO As Integer = &H40000000
    Public Const NC_ARBID_INFO_RTSI_INPUT As Short = &H1S
    '/* NC_ATTR_STD_MASK and NC_ATTR_XTD_MASK */
    Public Const NC_CAN_MASK_STD_MUSTMATCH As Short = &H7FFS
    Public Const NC_CAN_MASK_XTD_MUSTMATCH As Integer = &H1FFFFFFF
    Public Const NC_CAN_MASK_STD_DONTCARE As Short = &H0S
    Public Const NC_CAN_MASK_XTD_DONTCARE As Short = &H0S

    '/* Values for NC_ATTR_TRANSMIT_MODE(Immediate or timestamped).*/
    Public Const NC_TX_MODE_IMMEDIATE As Short = 0
    Public Const NC_TX_MODE_TIMESTAMPED As Short = 1

    '/* Values for NC_ATTR_TIMESTAMP_FORMAT.*/
    Public Const NC_TIME_FORMAT_ABSOLUTE As Short = 0
    Public Const NC_TIME_FORMAT_RELATIVE As Short = 1
    '/* Values for NC_ATTR_MASTER_TIMEBASE_RATE.Rate can either be
    '         10(M Series DAQ) or 20(E series DAQ) Mhz.
    '         This attribute is applicable only to PCI/PXI.*/
    Public Const NC_TIMEBASE_RATE_10 As Short = 10
    Public Const NC_TIMEBASE_RATE_20 As Short = 20
#End Region

#Region "Function Prototypes"
    '/***********************************************************************
    '                F U N C T I O N   P R O T O T Y P E S
    '***********************************************************************/

    '/* Naming conventions for sizes:
    '   Sizeof?        Indicates size of buffer passed in, and has no relation to
    '                  the number of bytes sent/received on the network (C only).
    '   ?Length        Indicates number of bytes to send on network.
    '   Actual?Length  Indicates number of bytes received from network.
    '*/
    <DllImport("nican.dll", EntryPoint:="ncAction")> _
    Public Shared Function ncAction(ByVal ObjHandle As Integer, ByVal Opcode As Integer, ByVal Param As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncCloseObject")> _
    Public Shared Function ncCloseObject(ByVal ObjHandle As Integer) As Integer
    End Function
  <DllImport("nican.dll", EntryPoint:="ncConfig")> _
  Public Shared Function ncConfig(ByVal ObjName As String, ByVal NumAttrs As Integer, ByRef AttrIdList As Integer, ByRef AttrValueList As Integer) As Integer
  End Function
    <DllImport("nican.dll", EntryPoint:="ncConnectTerminals")> _
    Public Shared Function ncConnectTerminals(ByVal ObjHandle As Integer, ByVal SourceTerminal As Integer, ByVal DestinationTerminal As Integer, ByVal Modifiers As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncDisconnectTerminals")> _
    Public Shared Function ncDisconnectTerminals(ByVal ObjHandle As Integer, ByVal SourceTerminal As Integer, ByVal DestinationTerminal As Integer, ByVal Modifiers As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncGetAttribute")> _
    Public Shared Function ncGetAttribute(ByVal ObjHandle As Integer, ByVal AttrId As Integer, ByVal SizeofAttr As Integer, ByRef Attr As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncGetHardwareInfo")> _
    Public Shared Function ncGetHardwareInfo(ByVal CardIndex As Integer, ByVal PortIndex As Integer, ByVal AttrId As Integer, ByVal AttrSize As Integer, ByRef Attr As UIntPtr) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncOpenObject")> _
    Public Shared Function ncOpenObject(ByVal ObjName As String, ByRef ObjHandle As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncRead")> _
    Public Shared Function ncRead(ByVal ObjHandle As Integer, ByVal SizeofData As Integer, ByRef Data As nican.NCTYPE_CAN_STRUCT) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncReadMult")> _
    Public Shared Function ncReadMult(ByVal ObjHandle As Integer, ByVal SizeofData As Integer, ByRef Data As Byte, ByRef ActualDataSize As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncReset")> _
    Public Shared Function ncReset(ByVal IntfName As String, ByVal Param As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncSetAttribute")> _
    Public Shared Function ncSetAttribute(ByVal ObjHandle As Integer, ByVal AttrId As Integer, ByVal SizeofAttr As Integer, ByRef Attr As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncStatusToString")> _
    Public Shared Function ncStatusToString(ByVal Status As Integer, ByVal SizeofString As Integer, ByRef ErrorString As Byte) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncWaitForState")> _
    Public Shared Function ncWaitForState(ByVal ObjHandle As Integer, ByVal DesiredState As Integer, ByVal Timeout As Integer, ByRef CurrentState As Integer) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncWrite")> _
    Public Shared Function ncWrite(ByVal ObjHandle As Integer, ByVal SizeofData As Integer, ByRef Data As nican.NCTYPE_CAN_FRAME) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncWriteMult")> _
    Public Shared Function ncWriteMult(ByVal ObjHandle As Integer, ByVal SizeofData As Integer, ByRef FrameArray As Byte) As Integer
    End Function
    <DllImport("nican.dll", EntryPoint:="ncCreateNotification")> _
        Public Shared Function ncCreateNotification(ByVal ObjHandle As Integer, ByVal DesiredState As Integer, ByVal Timeout As Integer, ByRef RefData As Integer, ByVal Callback As CANDataCallBack) As Integer
    End Function
    Delegate Sub CANDataCallBack(ByVal ObjHandle As Integer, ByVal DesiredState As Integer, ByVal Status As Integer, ByRef RefData As Integer)
#End Region


#Region "Function Implementation"
    '*******************************************************
    '** Some additional functions used in the examples.   **
    '*******************************************************

    Public ErrString As String

    Public Structure FileTime
        Dim dwLowDateTime As Integer
        Dim dwHighDateTime As Integer
    End Structure


  'Public Function ncWriteSingle(ByVal ObjHandle As Integer, ByRef Data As nican.NCTYPE_CAN_FRAME) As Integer

  '  Dim BytesToWrite As Integer = 0

  '  Dim Status As Integer
  '  Try



  '    Status = ncWrite(ObjHandle, 10, Data)

  '    ' ncWriteSingle = Status
  '    KillTime(1)
  '  Catch ex As Exception

  '  End Try
  'End Function

  'Public Function ncReadWriteSingle(ByVal ObjHandle As Integer, ByRef Data As nican.NCTYPE_CAN_FRAME, ByVal DataOut As nican.NCTYPE_CAN_STRUCT) As Integer

  '  Dim BytesToWrite As Integer = 0

  '  Dim Status As Integer

  '  Try


  '    '  ReadCANQueueWorker.RunWorkerAsync()

  '    Status = ncWrite(ObjHandle, 10, Data)
  '    ' Status = ncRead(ObjHandle, 1000, DataOut)
  '    KillTime(50)


  '    ' ncWriteSingle = Status
  '    KillTime(1)
  '  Catch ex As Exception

  '  End Try
  'End Function
  '  '**************************************************************

  '  'This function is used to print the absolute time obtained from ncRead.
  '  Public Function ConvAbsTime(ByRef time As NCTYPE_UINT64) As String

  '      Dim stime As SystemTime
  '      Dim localftime As FileTime
  '      Dim localtime As FileTime
  '      Dim res As Integer

  '      ' This Win32 function converts from UTC (international) time
  '      'to the local time zone.  The card keeps time in UTC
  '      'format (refer to the description of NCTYPE_ABS_TIME in
  '      'the section of the NI-CAN reference titled Common Host Data Types).  */
  '      localtime.dwLowDateTime = time.LowPart
  '      localtime.dwHighDateTime = time.HighPart
  '      res = FileTimeToLocalFileTime(localtime, localftime)

  '      ' This Win32 function converts an absolute time (FILETIME)
  '      'into SYSTEMTIME, a structure with fields for year, month, day,
  '      'and so on.  */
  '      res = FileTimeToSystemTime(localftime, stime)

  '      ConvAbsTime = stime.wMonth & "/" & stime.wDay & "/" & stime.wYear & " " & stime.wHour & ":" & stime.wMinute & ":" & stime.wSecond & ":" & stime.wMilliseconds
  '  End Function

  '  Public Sub ncStatToStr(ByVal Status As Integer, ByRef ErrString As String)

  '      ' This function wraps ncStatusToStr function and makes available to the
  '      ' user the error string in string format

  '      Dim str_Renamed(1024) As Byte
  '      Dim i As Short

  '      i = 1
  '      ncStatusToString(Status, 1024, str_Renamed(0))
  '      ErrString = Chr(str_Renamed(0))
  '      While ((i < 1024) And (Chr(str_Renamed(i)) <> "\0"))
  '          ErrString = ErrString & Chr(str_Renamed(i))
  '          i = i + 1
  '      End While
  '  End Sub

  '  ' This is a utility function used in the examples and refrences frmMain. If you do not
  '  ' have frmMain, this function can be deleted

  '  Public Function CheckStatus(ByVal Status As Integer, ByVal FuncName As String) As Boolean
  '      Try
  '          If (Status <> NC_SUCCESS) Then
  '      ncStatToStr(Status, ErrString)

  '      TsopAnomaly = New Anomaly(10001, "NI-CAN Frame API", "CheckStatus", ErrString, TsopDatabase)
  '      Throw New TsopAnomalyException

  '      '     MsgBox(Hex(Status) & " " & FuncName & " " & ErrString, MsgBoxStyle.Critical + MsgBoxStyle.ApplicationModal + MsgBoxStyle.OkOnly, "CAN ERROR")


  '          End If

  '          If (Status < 0) Then
  '              CheckStatus = True
  '          Else
  '              CheckStatus = False
  '    End If

  '  Catch ex As TsopAnomalyException
  '    ' TsopAnomaly.AppendCallerList("NI_CAN Frame API.CheckStatus")

  '  Catch ex As Exception
  '    TsopAnomaly = New Anomaly(10001, "NI-CAN Frame API", "CheckStatus", "Runtime Error", TsopDatabase)
  '    Throw New TsopAnomalyException
  '  End Try

  '  End Function

#End Region

End Class