﻿
Imports System.Xml
Imports System.Text
Imports System.Reflection
Imports System.IO
Imports System.Linq
Imports System.Threading
'Imports System.Threading.Thread
Imports System.Collections.Concurrent
Imports System.Text.RegularExpressions
Imports System.Runtime.InteropServices

Public Class clsCANActuator
  'Inherits MarshalByRefObject

#Region "Constructors =================================================="
  Public Sub New(ByVal configFilePath As String, strActuatorVariant As String)
    Dim strErrorMessage As String
    Dim CounterFreq As Long
    Dim strVariant As String

    Try

      strVariant = UCase(strActuatorVariant)
      Me.mstrClassConfigFilePath = configFilePath

      Me.mTargetActuator = DirectCast([Enum].Parse(GetType(ActuatorVariantEnum), strVariant), Byte)

      Select Case mTargetActuator
        Case ActuatorVariantEnum.CATFUEL, ActuatorVariantEnum.CATTHROTTLE
          mintToolMessagePriority = CAT_TOOL_MSG_PRIORITY
        Case Else
          mintToolMessagePriority = TOOL_MSG_PRIORITY
      End Select

      mActuatorAddress = DFLT_BOOTLDR_ADDR ' maybe have a property for ActuatorAddress that can change this from the app code from the parameter set
      mblnApplicationCodeRunning = True

      ' Get the frequency of the Performance Counter
      QueryPerformanceFrequency(CounterFreq)
      ' Store the period of the Performance Counter * 1000
      CounterPeriod = (1.0 / CDbl(CounterFreq)) * 1000

      ' Get the CAN message handler.
      mCanMsgHandler = clsCanMsgHandling.GetInstance()
      If mCanMsgHandler Is Nothing Then
        strErrorMessage = "Error Creating Instance of clsCanMessageHandling Class"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      ' Get the receive CAN message delegates.
      '_RcvMsgDelegates = clsRcvCanMsgDelegates.GetInstance()

      'Read configuration settings from file
      Call GetConfigurationSettings()
      If mAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      'Set Path for logging CAN Messages
      mCanMsgHandler.CANMessageLogFilePath = mstrCANMessageLogFilePath

      'Get CAN Message Definitions from file
      Call GetCANMessages()
      If mAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      'Get Address Ranges from file
      Call GetAddressRanges()
      If mAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      Call mCanMsgHandler.StartCAN(Me.mstrCANInterfaceName)
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      '_RcvMsgDelegates.AddToolMsgDelegate(AddressOf ToolMsgRespRcvd)
      mCanMsgHandler.RcvMsgDelegates.AddToolMsgDelegate(AddressOf ToolMsgRespRcvd)
      If mCanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
        Me.Anomaly = mCanMsgHandler.RcvMsgDelegates.Anomaly
        mCanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      ' Create transmit CAN message thread and set priority
      mActuatorDownloadThread = New Thread(New ThreadStart(AddressOf ActuatorDownloadThreadFunction))
      mActuatorDownloadThread.Priority = ThreadPriority.AboveNormal

      If (mActuatorDownloadThread.ThreadState = ThreadState.Unstarted) Then
        mActuatorDownloadThread.Start()
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

#End Region

#Region "Private Member Variables for Multithreading"

  ' CAN message handling class.
  Private mCanMsgHandler As clsCanMsgHandling

  ' Collection of request for address claim message responses.
  Private mAddressClaimMsgs As New ConcurrentDictionary(Of String, TPCANMsg)

  ' Locking object for address claim collection.
  Private mAddrClaimLock As Object = New Object

  ' Collection of received status messages.
  Private mStatusMsgs As New ConcurrentDictionary(Of String, TPCANMsg)

  ' Locking object for status message collection.
  Private mStatusMsgLock As Object = New Object

  ' Collection of tool response messages.
  Private mToolResponseMsgs As New ConcurrentDictionary(Of String, clsCanMsgHandling.CAN_Msg_Struct)

  ' Locking object for tool response message collection.
  Private mToolResponseLock As Object = New Object

  ' Collection of tool response messages.
  Private mDfltBtldrToolRespMsgs As New ConcurrentDictionary(Of String, clsCanMsgHandling.CAN_Msg_Struct)

  ' Locking object for tool response message collection.
  Private mDfltBtldrToolRespLock As Object = New Object

  ' True when actuator is running application code
  Private mblnApplicationCodeRunning As Boolean = False
  ' True when actuator is running default bootloader code
  Private mblnDefaultBootloaderCodeRunning As Boolean = False

  ' Address of actuator that responded to request for address claim messages.
  Private mActuatorAddress As Byte

  Private mActuatorCount As Byte

  ' Elapsed Time Counter (high resolution)
  Private Declare Function QueryPerformanceCounter Lib "Kernel32.dll" (ByRef lpPerformanceCount As Long) As Boolean
  Private Declare Function QueryPerformanceFrequency Lib "Kernel32.dll" (ByRef lpFrequency As Long) As Boolean
  Private HP_CounterValue As Long
  'Private StartingTime As Double
  'Private CurrentTime As Double
  Private CounterPeriod As Double

  'ActuatorDownload Thread Objects
  Private mActuatorDownloadWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
  Private mActuatorDownloadThread As Thread
  Private mblnTerminateActuatorDownloadThread As Boolean = False
  Private mblnActuatorDownloadComplete As Boolean
  Private mintActuatorDownloadPercentComplete As Integer

#End Region

#Region "Private Member Variables =================================================="

  Private mAnomaly As clsAnomaly
  Private mStackTrace As New clsStackTrace

  Private mstrActuatorDataPath As String
  Private mdtAddressRanges As DataTable
  Private mstrAddressRangesFileName As String
  Private mintAttrIdList(9) As Integer
  Private mintAttrValueList(9) As Integer

  'Private mintBaudRate As Integer

  'Private mCAN As nican
  'Private mintCANHandle As Integer
  Private mstrCANInterfaceName As String
  Private mCANMessages As New Dictionary(Of clsCANMessage.MessageFunctions, clsCANMessage)
  Private mstrCANMessageDefinitionsFileName As String
  Private mblnCanMessageLoggingEnabled As Boolean
  Private mblnCanActuatorActivityLoggingEnabled As Boolean

  Private mstrCANMessageLogFilePath As String
  Private mCANMessagesToRead As New List(Of clsCANMessage)
  Private mblnCANPortOpen As Boolean
  Private mstrClassConfigFilePath As String

  'Private mEEPromData As New ArrayList


  Private mIntelHexFile As New List(Of IntelDataRecord)
  Private mIntelHexFileDownload As New List(Of IntelDataRecord)

  'Private mintReadQLength As Integer

  Private mstrSerialNumber As String
  'Private mintStartOnOpen As Integer
  Private mStopwatch As New clsStopWatch

  Private mTargetActuator As ActuatorVariantEnum
  Private mbytDefaultBootloaderFault As Byte
  Private mintToolMessagePriority As Integer

  Private mblnWriteComplete As Boolean
  'Private mintWriteQLength As Integer

#End Region

#Region "Public Constants, Structures, Enums =================================================="
  Public Structure TPCANMsg
    Public ID As Integer   ' 11/29 bit identifier
    Public MSGTYPE As Byte ' Bits from MSGTYPE_*
    Public LEN As Byte     ' Data Length Code of the Msg (0..8)
    Public DATA As Byte()  ' Data 0 .. 7
  End Structure

  Public Enum TargetMemoryAreaEnum As Byte
    ProgramMemory = 1
    EEProm = 3
    BootLoader = 5
  End Enum

  Public Enum ActuatorVariantEnum As Byte
    LYSANDER = 0
    AYALA = 1
    VIKING = 2
    UNDEFINED = 3
    CATTHROTTLE = 4
    CATFUEL = 5
  End Enum

  ' Default bootloader address.
  Public Const DFLT_BOOTLDR_ADDR As Byte = &H2

  ' CAN Message ID's
  Public Const STATUS_MESSAGE As Integer = &H18FFC500
  Public Const TOOL_MSG_RESPONSE As Integer = &H18FF0D00
  Public Const ADDR_CLAIM As Integer = &H18EEFF00
  Public Const CAT_ADDR_CLAIM As Integer = &H18EE0000

  Public Const TOOL_MSG_PRIORITY = &H18000000
  Public Const CAT_TOOL_MSG_PRIORITY = &H0C000000

  Public Const TOOL_MSG_PGN = &HFF0E00

  ' CAN Message ID address masks.
  Public Const SRC_ADDR_MASK As Integer = &HFFFFFF00


#End Region

#Region "Properties =================================================="
  Public ReadOnly Property ActuatorDownloadPercentComplete As Integer
    Get
      Return mintActuatorDownloadPercentComplete
    End Get
  End Property

  Public ReadOnly Property ActuatorDownloadComplete As Boolean
    Get
      Return mblnActuatorDownloadComplete
    End Get
  End Property

  Public Property Anomaly() As clsAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsAnomaly)
      mAnomaly = value
    End Set
  End Property

  Public ReadOnly Property ApplicationCodeRunning As Boolean
    Get
      Return mblnApplicationCodeRunning
    End Get
  End Property

  Public ReadOnly Property CANMessages As Dictionary(Of clsCANMessage.MessageFunctions, clsCANMessage)
    Get
      Return Me.mCANMessages
    End Get
  End Property

  'Public ReadOnly Property EEPromData As ArrayList
  '  Get
  '    Return Me.mEEPromData
  '  End Get
  'End Property

  Public ReadOnly Property DefaultBootloaderFault As Byte
    Get
      Return mbytDefaultBootloaderFault
    End Get
  End Property


  Public ReadOnly Property IntelHexFile As List(Of IntelDataRecord)
    Get
      Return mIntelHexFile
    End Get
  End Property

  Public ReadOnly Property IntelHexFileDownload As List(Of IntelDataRecord)
    Get
      Return mIntelHexFileDownload
    End Get
  End Property

  Public Property CanActuatorActivityLoggingEnabled As Boolean
    Get
      Return mblnCanActuatorActivityLoggingEnabled
    End Get
    Set(value As Boolean)
      mblnCanActuatorActivityLoggingEnabled = value
      mCanMsgHandler.CanActuatorActivityLoggingEnabled = value
    End Set
  End Property

  Public Property CanMessageLoggingEnabled As Boolean
    Get
      Return mblnCanMessageLoggingEnabled
    End Get
    Set(value As Boolean)
      mblnCanMessageLoggingEnabled = value
      mCanMsgHandler.CanMessageLoggingEnabled = value
      If mCanMsgHandler.CanMessageLoggingEnabled Then
        mCanMsgHandler.StartCanLoggingThread()
      End If

    End Set
  End Property

#End Region

#Region "Private Methods =================================================="

  Private Function CloseSession() As Boolean
    Dim canMsg As New TPCANMsg
    Dim return_value As Boolean
    Try

      ' Close the default bootloader programming session.
      mCanMsgHandler.LoadCANMsgOutputQueue(CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderCloseSessionCmd))
      ' Wait for Ack from actuator.
      return_value = GetCloseSessionAck(canMsg)
      If (return_value = False) Then
        Console.WriteLine("Close Session Failed 1st Try, Retrying")
        ' Try closing the default bootloader programming session again.
        mCanMsgHandler.LoadCANMsgOutputQueue(CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderCloseSessionCmd))
        ' Wait for Ack from actuator.
        return_value = GetCloseSessionAck(canMsg)
        If return_value = True And mAnomaly IsNot Nothing Then
          'Close Session succeded, clear the previous Anomaly
          mAnomaly = Nothing
        End If
        Console.WriteLine("Close Session Result 2nd Try: " & CStr(return_value))
      End If
      Return return_value

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Private Sub GetAddressRanges(Optional ByVal strFilter As String = Nothing)
    Dim dt As New System.Data.DataTable
    Dim blnFirstLine As Boolean = True
    Dim strFilterFieldValue As String
    Dim strFilterColumnName As String
    Dim intFilterColumn As Integer
    Dim strFile As String
    Dim cols() As String
    Dim col As String
    Dim data() As String
    Dim dr As DataRow
    Try

      strFilterColumnName = ""
      strFilterFieldValue = ""
      intFilterColumn = -1
      cols = Nothing
      data = Nothing

      If strFilter <> Nothing Then
        strFilterColumnName = (strFilter.Split("=")(0)).Trim
        strFilterFieldValue = (strFilter.Split("=")(1)).Trim
      End If

      'strFile = strFilePath & strFileName
      strFile = mstrClassConfigFilePath & mstrAddressRangesFileName

      If IO.File.Exists(strFile) Then
        Using sr As New StreamReader(strFile)
          While Not sr.EndOfStream
            If blnFirstLine Then
              blnFirstLine = False
              cols = sr.ReadLine.Split(",")
              For Each col In cols
                dt.Columns.Add(New DataColumn(col, GetType(String)))
              Next
              If strFilterColumnName <> "" Then
                intFilterColumn = Array.IndexOf(cols, strFilterColumnName)
              End If
            Else
              data = sr.ReadLine.Split(",")
              If intFilterColumn <> -1 Then
                If data(intFilterColumn) = strFilterFieldValue Then
                  dt.Rows.Add(data.ToArray)
                End If
              Else
                dt.Rows.Add(data.ToArray)
              End If
            End If
          End While
        End Using
      End If

      For Each dr In dt.Rows
        dr.Item("ActuatorVariant") = UCase(dr.Item("ActuatorVariant"))
      Next

      mdtAddressRanges = dt

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Private Sub GetCANMessages(Optional ByVal strFilter As String = Nothing)
    Dim dt As New System.Data.DataTable
    Dim blnFirstLine As Boolean = True
    Dim strFilterFieldValue As String
    Dim strFilterColumnName As String
    Dim intFilterColumn As Integer
    Dim strFile As String
    Dim cols() As String
    Dim col As String
    Dim data() As String
    Dim newCANMessage As clsCANActuator.clsCANMessage
    Dim rowCAN As DataRow
    Dim intDataIndex As Integer
    Dim strTempData() As String
    Try

      strFilterColumnName = ""
      strFilterFieldValue = ""
      intFilterColumn = -1
      cols = Nothing
      data = Nothing

      If strFilter <> Nothing Then
        strFilterColumnName = (strFilter.Split("=")(0)).Trim
        strFilterFieldValue = (strFilter.Split("=")(1)).Trim
      End If

      'strFile = strFilePath & strFileName
      strFile = mstrClassConfigFilePath & mstrCANMessageDefinitionsFileName

      If IO.File.Exists(strFile) Then
        Using sr As New StreamReader(strFile)
          While Not sr.EndOfStream
            If blnFirstLine Then
              blnFirstLine = False
              cols = sr.ReadLine.Split(",")
              For Each col In cols
                dt.Columns.Add(New DataColumn(col, GetType(String)))
              Next
              If strFilterColumnName <> "" Then
                intFilterColumn = Array.IndexOf(cols, strFilterColumnName)
              End If
            Else
              data = sr.ReadLine.Split(",")
              If intFilterColumn <> -1 Then
                If data(intFilterColumn) = strFilterFieldValue Then
                  dt.Rows.Add(data.ToArray)
                End If
              Else
                dt.Rows.Add(data.ToArray)
              End If
            End If
          End While
        End Using
      End If

      For Each rowCAN In dt.Rows
        newCANMessage = New clsCANMessage
        newCANMessage.MessageFunction = DirectCast([Enum].Parse(GetType(clsCANMessage.MessageFunctions), rowCAN.Item("Function")), Integer)
        newCANMessage.OpCode = Convert.ToInt32(rowCAN.Item("OpCode"), 16)
        'newCANMessage.MessageType = DirectCast([Enum].Parse(GetType(clsCANMessage.MessageTypes), rowCAN.Item("MessageType")), Integer)
        newCANMessage.SendArbitrationID = Convert.ToInt32(rowCAN.Item("SendArbitrationID"), 16)
        'newCANMessage.ReceiveArbitrationIDMin = Convert.ToInt32(rowCAN.Item("ReceiveArbitrationIDMin"), 16)
        'newCANMessage.ReceiveArbitrationIDMax = Convert.ToInt32(rowCAN.Item("ReceiveArbitrationIDMax"), 16)
        'newCANMessage.FrameRateMilliseconds = rowCAN.Item("FrameRateMilliseconds")
        newCANMessage.DataLength = rowCAN.Item("DataLength")
        'newCANMessage.DecodingMethod = DirectCast([Enum].Parse(GetType(clsCANMessage.DecodingMethods), rowCAN.Item("DecodingMethod")), Integer)
        newCANMessage.DecodingStartByte = rowCAN.Item("DecodingStartByte")
        newCANMessage.DecodingByteLength = rowCAN.Item("DecodingByteLength")
        'newCANMessage.FieldsToReturn = rowCAN.Item("FieldsToReturn")
        newCANMessage.ReverseByteOrder = CBool(rowCAN.Item("ReverseByteOrder"))
        'newCANMessage.DynamicWriteByteCount = rowCAN.Item("DynamicWriteByteCount")
        'newCANMessage.NumberOfTimesToSendMessage = rowCAN.Item("NumberOfTimesToSendMessage")
        strTempData = Split(rowCAN.Item("StaticData"), ".")
        For intDataIndex = 0 To strTempData.Length - 1 '7
          If strTempData(intDataIndex).ToUpper = "XX" Then
            newCANMessage.StaticData(intDataIndex) = strTempData(intDataIndex)
          Else
            newCANMessage.StaticData(intDataIndex) = Convert.ToInt32(strTempData(intDataIndex), 16)
            newCANMessage.Data(intDataIndex) = Convert.ToInt32(strTempData(intDataIndex), 16)
          End If
        Next

        'strTempData = Split(rowCAN.Item("ReturnFieldBytePositions"), ".")
        'ReDim newCANMessage.ReturnFieldBytePositions(strTempData.Length - 1)
        'For intDataIndex = 0 To strTempData.Length - 1
        '  newCANMessage.ReturnFieldBytePositions(intDataIndex) = Convert.ToInt32(strTempData(intDataIndex), 16)
        'Next

        mCANMessages.Add(newCANMessage.MessageFunction, newCANMessage)
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Private Sub GetConfigurationSettings()
    Dim configReader As XmlTextReader
    Dim configSet As New DataSet
    Dim configTable As DataTable
    Dim configRow As DataRow
    Dim strClassName As String
    Try

      strClassName = Me.GetType.Name

      'Create the XML Reader
      configReader = New XmlTextReader(Me.mstrClassConfigFilePath & strClassName & ".cfg")

      'Clear data set that holds xml file contents
      configSet.Clear()
      'Read the file
      configSet.ReadXml(configReader)

      'Config File not hierarchical, only 1 table and 1 row, with many fields
      configTable = configSet.Tables(0)
      configRow = configTable.Rows(0)

      'Assign field values to variables
      'Me.mintBaudRate = configRow("BaudRate")
      'Me.mintStartOnOpen = configRow("StartOnOpen")
      'Me.mintReadQLength = configRow("ReadQLength")
      'Me.mintWriteQLength = configRow("WriteQLength")
      Me.mstrCANInterfaceName = configRow("CANInterfaceName")
      Me.mstrCANMessageDefinitionsFileName = configRow("CANMessageDefinitionsFileName")
      Me.mstrAddressRangesFileName = configRow("AddressRangesFileName")
      Me.mstrActuatorDataPath = configRow("ActuatorDataPath")
      Me.mstrCANMessageLogFilePath = configRow("CANMessageLogFilePath")

      'Close reader, releases file
      configReader.Close()

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  Private Function GetCloseSessionAck(ByRef canMsg As TPCANMsg) As Boolean
    Dim blnreturnValue As Boolean
    Dim intOpCodeDBLCloseSessionAck As UShort
    Dim intOpCodeDBLFault As UShort
    Dim strErrorMessage As String
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double
    Try
      intOpCodeDBLCloseSessionAck = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderCloseSessionAck).OpCode
      intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
      mbytDefaultBootloaderFault = 0

      ' Initialize the elapsed time starting value
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
      ' Wait up to two seconds.
      Do
        If ((GetToolMsgRespCount(intOpCodeDBLCloseSessionAck) > 0) OrElse _
            (GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
          ' Received message count > 0
          Exit Do
        End If
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < 2000)

      ' Read the default bootloader close session acknowledge.
      If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
        'strErrorMessage = "Fault Occured In Default Bootloader"
        mbytDefaultBootloaderFault = canMsg.DATA(1)
        strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) & " Occured In Default Bootloader: Close Session Ack"
        Console.WriteLine(strErrorMessage)
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
        'Exit Try
      ElseIf (GetToolMsgResp(intOpCodeDBLCloseSessionAck, canMsg) = True) Then
        blnreturnValue = True
      End If
      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Function GetMemoryReadInitAck(ByRef canMsg As TPCANMsg) As Boolean 'As RESULT
    'Dim returnValue As RESULT = RESULT.NO_RESPONSE
    Dim blnreturnValue As Boolean
    Dim intOpCodeDBLInitMemReadAck As UShort
    Dim intOpCodeDBLFault As UShort
    Dim strErrorMessage As String
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try
      blnreturnValue = False
      intOpCodeDBLInitMemReadAck = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryReadAck).OpCode
      intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
      mbytDefaultBootloaderFault = 0

      ' Initialize the elapsed time starting value
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
      ' Wait up to one second.
      Do
        'If ((GetToolMsgRespCount(OPCODE.DFLT_BOOTLDR_INIT_READ_ACK) > 0) OrElse _
        '   (GetToolMsgRespCount(OPCODE.DFLT_BOOTLDR_FAULT) > 0)) Then
        '  ' Received message count > 0
        '  Exit Do
        'End If
        If ((GetToolMsgRespCount(intOpCodeDBLInitMemReadAck) > 0) OrElse _
             (GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
          ' Received message count > 0
          Exit Do
        End If
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < 1000)

      ' Read the default bootloader initiate memory read acknowledge.
      If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
        'strErrorMessage = "Fault Occured In Default Bootloader"
        mbytDefaultBootloaderFault = canMsg.DATA(1)
        strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) & " Occured In Default Bootloader: Init Memory Read Ack"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
        'Exit Try 'throw new TsopAnomalyException
      ElseIf (GetToolMsgResp(intOpCodeDBLInitMemReadAck, canMsg) = True) Then
        blnreturnValue = True
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Function GetMemoryReadData(ByRef canMsg As TPCANMsg) As Boolean 'As RESULT
    Dim blnreturnValue As Boolean
    Dim intOpCodeDBLMemReadData As UShort
    Dim intOpCodeDBLFault As UShort
    Dim strErrorMessage As String
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try
      blnreturnValue = False
      intOpCodeDBLMemReadData = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryReadData).OpCode
      intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
      mbytDefaultBootloaderFault = 0

      ' Initialize the elapsed time starting value
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
      ' Wait up to one second.
      Do
        If ((GetToolMsgRespCount(intOpCodeDBLMemReadData) > 0) OrElse _
            (GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
          ' Received message count > 0
          Exit Do
        End If
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < 1000)

      ' Read the default bootloader memory read data response message.
      If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
        'strErrorMessage = "Fault Occured In Default Bootloader"
        mbytDefaultBootloaderFault = canMsg.DATA(1)
        strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) & " Occured In Default Bootloader: Memory Read Data"
        Console.WriteLine(strErrorMessage)
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
        'Exit Try
      ElseIf (GetToolMsgResp(intOpCodeDBLMemReadData, canMsg) = True) Then
        blnreturnValue = True
      End If
      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Function GetMemoryWriteInitAck(ByRef canMsg As TPCANMsg) As Boolean
    Dim blnReturnValue As Boolean = False
    Dim intOpCodeDBLInitMemWriteAck As UShort
    Dim intOpCodeDBLFault As UShort
    Dim strErrorMessage As String = ""
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try
      intOpCodeDBLInitMemWriteAck = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryWriteAck).OpCode
      intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
      mbytDefaultBootloaderFault = 0

      ' Initialize the elapsed time starting value
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod

      ' Wait up to one second.
      Do
        If ((GetToolMsgRespCount(intOpCodeDBLInitMemWriteAck) > 0) OrElse _
            (GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
          ' Received message count > 0
          Exit Do
        End If
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < 500)

      ' Read the default bootloader initiate memory write acknowledge.
      If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
        'strErrorMessage = "Fault Occured In Default Bootloader"
        mbytDefaultBootloaderFault = canMsg.DATA(1)
        strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) & " Occured In Default Bootloader: Init Memory Write Ack"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
        'Exit Try 'throw new TsopAnomalyException
      ElseIf (GetToolMsgResp(intOpCodeDBLInitMemWriteAck, canMsg) = True) Then
        blnReturnValue = True
      End If

      Return blnReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Function GetMemoryWriteDataAck(ByRef canMsg As TPCANMsg) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim intOpCodeDBLMemWriteDataAck As UShort
    Dim intOpCodeDBLFault As UShort
    Dim strErrorMessage As String = ""
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try

      intOpCodeDBLMemWriteDataAck = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryWriteDataAck).OpCode
      intOpCodeDBLFault = CANMessages(clsCANMessage.MessageFunctions.DefaultBootLoaderFaultResponse).OpCode
      mbytDefaultBootloaderFault = 0

      ' Initialize the elapsed time starting value
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod

      ' Wait up to one second.
      Do
        If ((GetToolMsgRespCount(intOpCodeDBLMemWriteDataAck) > 0) OrElse _
            (GetToolMsgRespCount(intOpCodeDBLFault) > 0)) Then
          ' Received message count > 0
          Exit Do
        End If
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < 500)

      ' Read the default bootloader memory write acknowledge.
      If (GetToolMsgResp(intOpCodeDBLFault, canMsg) = True) Then
        'strErrorMessage = "Fault Occured In Default Bootloader"
        mbytDefaultBootloaderFault = canMsg.DATA(1)
        strErrorMessage = "Fault 0x" & Hex(mbytDefaultBootloaderFault) & " Occured In Default Bootloader: Init Memory Write Ack"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      ElseIf (GetToolMsgResp(intOpCodeDBLMemWriteDataAck, canMsg) = True) Then
        blnreturnValue = True
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Function GetToolMsgResp(ByVal msgOpcode As UShort, ByRef canMsg As TPCANMsg) As Boolean
    Dim readMsgStruct As clsCanMsgHandling.CAN_Msg_Struct
    Dim returnValue As Boolean = False
    Dim key As String = ""
    Dim msg_str As New clsCanMsgHandling.CAN_Msg_Struct

    Try
      ' Check for a valid opcode. Invalid opcodes result in return value of False.
      If (mblnApplicationCodeRunning = True) Then
        SyncLock mToolResponseLock
          'Select Case msgOpcode
          '  Case OPCODE.SW_VERSION_ID, _
          '       OPCODE.APPLICATION_CHKSUM_CALC, _
          '       OPCODE.SERIAL_NUMBER, _
          '       OPCODE.HW_PART_NUM_MS, _
          '       OPCODE.HW_PART_NUM_LS, _
          '       OPCODE.SW_PART_NUM_MS, _
          '       OPCODE.SW_PART_NUM_LS, _
          '       OPCODE.INTERNAL_TEMP, _
          '       OPCODE.BATTERY_VOLTAGE, _
          '       OPCODE.MICRO_OSC_FREQ, _
          '       OPCODE.MICRO_PLL_SELECT

          key = msgOpcode.ToString
          If (mToolResponseMsgs.ContainsKey(key)) Then
            readMsgStruct = mToolResponseMsgs.Item(key)
            ' Return selected message.
            canMsg = readMsgStruct.CanMsg
            ' Consume the message (message struct). Until another message
            ' is received , GetToolMsgRespCount() will return 0 for this opcode.
            mToolResponseMsgs.TryRemove(key, msg_str)
            returnValue = True
          End If
          'End Select
        End SyncLock
      Else
        SyncLock mDfltBtldrToolRespLock
          'Select Case msgOpcode
          '  Case OPCODE.DFLT_BOOTLDR_ATTN, _
          '       OPCODE.DFLT_BOOTLDR_INIT_WRITE, _
          '       OPCODE.DFLT_BOOTLDR_MEM_WRITE, _
          '       OPCODE.DFLT_BOOTLDR_INIT_READ, _
          '       OPCODE.DFLT_BOOTLDR_MEM_READ, _
          '       OPCODE.DFLT_BOOTLDR_CALC_CHKSUM, _
          '       OPCODE.DFLT_BOOTLDR_SERIAL_NUMBER, _
          '       OPCODE.DFLT_BOOTLDR_CLOSE_SESSION, _
          '       OPCODE.DFLT_BOOTLDR_START_APP, _
          '       OPCODE.DFLT_BOOTLDR_RESTART, _
          '       OPCODE.DFLT_BOOTLDR_FAULT

          key = msgOpcode.ToString
          If (mDfltBtldrToolRespMsgs.ContainsKey(key)) Then
            readMsgStruct = mDfltBtldrToolRespMsgs.Item(key)
            ' Return specified message.
            canMsg = readMsgStruct.CanMsg
            ' Consume the message (message struct). Until another message
            ' is received , GetToolMsgRespCount() will return 0 for this opcode.
            mDfltBtldrToolRespMsgs.TryRemove(key, msg_str)
            returnValue = True
          End If
          'End Select
        End SyncLock
      End If

      Return returnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Function GetToolMsgRespCount(ByVal msgOpcode As UShort) As UShort
    Dim readMsgStruct As clsCanMsgHandling.CAN_Msg_Struct
    Dim msgCount As UShort = 0
    Dim key As String = ""

    Try
      '' Check for valid opcode. Invalid opcodes result in return value of 0.
      If (mblnApplicationCodeRunning = True) Then
        SyncLock mToolResponseLock
          '    Select Case msgOpcode
          '      Case OPCODE.SW_VERSION_ID, _
          '           OPCODE.APPLICATION_CHKSUM_CALC, _
          '           OPCODE.SERIAL_NUMBER, _
          '           OPCODE.HW_PART_NUM_MS, _
          '           OPCODE.HW_PART_NUM_LS, _
          '           OPCODE.SW_PART_NUM_MS, _
          '           OPCODE.SW_PART_NUM_LS, _
          '           OPCODE.INTERNAL_TEMP, _
          '           OPCODE.BATTERY_VOLTAGE, _
          '           OPCODE.MICRO_OSC_FREQ, _
          '           OPCODE.MICRO_PLL_SELECT

          key = msgOpcode.ToString
          If (mToolResponseMsgs.ContainsKey(key)) Then
            ' Return the number of specified messages received since
            ' the last GetToolMsgResp() call for specified opcode.
            readMsgStruct = mToolResponseMsgs.Item(key)
            msgCount = readMsgStruct.msgCount
          End If
          'End Select
        End SyncLock
      Else
        SyncLock mDfltBtldrToolRespLock
          'Select Case msgOpcode
          '  Case OPCODE.DFLT_BOOTLDR_ATTN, _
          '       OPCODE.DFLT_BOOTLDR_INIT_WRITE, _
          '       OPCODE.DFLT_BOOTLDR_MEM_WRITE, _
          '       OPCODE.DFLT_BOOTLDR_INIT_READ, _
          '       OPCODE.DFLT_BOOTLDR_MEM_READ, _
          '       OPCODE.DFLT_BOOTLDR_CALC_CHKSUM, _
          '       OPCODE.DFLT_BOOTLDR_SERIAL_NUMBER, _
          '       OPCODE.DFLT_BOOTLDR_CLOSE_SESSION, _
          '       OPCODE.DFLT_BOOTLDR_START_APP, _
          '       OPCODE.DFLT_BOOTLDR_RESTART, _
          '       OPCODE.DFLT_BOOTLDR_FAULT

          key = msgOpcode.ToString
          If (mDfltBtldrToolRespMsgs.ContainsKey(key)) Then
            ' Return the number of specified messages received since
            ' the last GetToolMsgResp() call for specified opcode.
            readMsgStruct = mDfltBtldrToolRespMsgs.Item(key)
            msgCount = readMsgStruct.msgCount
          End If
          'End Select
        End SyncLock
      End If

      Return msgCount

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function


  Private Function RecordToString(ByVal Record As IntelDataRecord) As String
    Dim RecordStr As String = ""

    Try
      RecordStr = RecordStr & ":"
      If Record.DataLength < 16 Then
        RecordStr = RecordStr & "0" & Hex(Record.DataLength)
      Else
        RecordStr = RecordStr & Hex(Record.DataLength)
      End If
      Select Case Record.StartAddress
        Case Is < 16
          RecordStr = RecordStr & "000" & Hex(Record.StartAddress)
        Case Is < 256
          RecordStr = RecordStr & "00" & Hex(Record.StartAddress)
        Case Is < 4096
          RecordStr = RecordStr & "0" & Hex(Record.StartAddress)
        Case Else
          RecordStr = RecordStr & Hex(Record.StartAddress)
      End Select
      If Record.RecordType < 16 Then
        RecordStr = RecordStr & "0" & Hex(Record.RecordType)
      Else
        RecordStr = RecordStr & Hex(Record.RecordType)
      End If

      If Record.DataLength <> 0 Then
        For j As Integer = 0 To Record.Data.Length - 1
          If Record.Data(j) < 16 Then
            RecordStr = RecordStr & "0" & Hex(Record.Data(j))
          Else
            RecordStr = RecordStr & Hex(Record.Data(j))
          End If
        Next
      End If

      If Record.CheckSum < 16 Then
        RecordStr = RecordStr & "0" & Hex(Record.CheckSum)
      Else
        RecordStr = RecordStr & Hex(Record.CheckSum)
      End If
      Return RecordStr

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function


  Private Function ReturnASCIIString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
    Try
      Dim intIndex As Integer
      Dim intStartByte As Integer
      Dim intEndByte As Integer
      Dim intByteIncrement As Integer
      Dim strByteValue As String
      Dim strTemp As StringBuilder
      Dim strReturnValue As String

      strTemp = New StringBuilder((Data.Length * 2))

      If CANMessage.ReverseByteOrder Then
        intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intEndByte = CANMessage.DecodingStartByte
        intByteIncrement = -1
      Else
        intStartByte = CANMessage.DecodingStartByte
        intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intByteIncrement = 1
      End If

      strByteValue = ""
      For intIndex = intStartByte To intEndByte Step intByteIncrement
        strByteValue = strByteValue & Chr(Data(intIndex))
      Next
      strReturnValue = strByteValue
      Return strReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function

  Private Function ReturnASCIIStringDotDelimitEveryTwoBytes(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
    Try
      Dim intIndex As Integer
      Dim intStartByte As Integer
      Dim intEndByte As Integer
      Dim intByteIncrement As Integer
      Dim strByteValue As String
      Dim strTemp As StringBuilder
      Dim strReturnValue As String

      strTemp = New StringBuilder((Data.Length * 2))

      If CANMessage.ReverseByteOrder Then
        intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intEndByte = CANMessage.DecodingStartByte
        intByteIncrement = -1
      Else
        intStartByte = CANMessage.DecodingStartByte
        intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intByteIncrement = 1
      End If

      strByteValue = ""
      For intIndex = intStartByte To intEndByte Step intByteIncrement
        strByteValue = strByteValue & Chr(Data(intIndex))
        intIndex += intByteIncrement
        strByteValue = strByteValue & Chr(Data(intIndex))
        If intIndex <> intEndByte Then
          strByteValue = strByteValue & "."
        End If
      Next
      strReturnValue = strByteValue
      Return strReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function

  Private Function ReturnDotDelimitedString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
    Try
      Dim intIndex As Integer
      Dim intStartByte As Integer
      Dim intEndByte As Integer
      Dim intByteIncrement As Integer
      Dim strByteValue As String
      Dim strTemp As StringBuilder
      Dim strReturnValue As String

      strTemp = New StringBuilder((Data.Length * 2))

      If CANMessage.ReverseByteOrder Then
        intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intEndByte = CANMessage.DecodingStartByte
        intByteIncrement = -1
      Else
        intStartByte = CANMessage.DecodingStartByte
        intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intByteIncrement = 1
      End If

      strByteValue = ""
      For intIndex = intStartByte To intEndByte Step intByteIncrement
        strByteValue = strByteValue & Data(intIndex)
        'intIndex += intByteIncrement
        'strByteValue = strByteValue & Chr(Data(intIndex))
        If intIndex <> intEndByte Then
          strByteValue = strByteValue & "."
        End If
      Next
      strReturnValue = strByteValue
      Return strReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function

  Private Function ReturnContiguousByteString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
    Try
      Dim intIndex As Integer
      Dim intStartByte As Integer
      Dim intEndByte As Integer
      Dim intByteIncrement As Integer
      Dim strByteValue As String
      Dim strTemp As StringBuilder
      Dim strReturnValue As String

      strTemp = New StringBuilder((Data.Length * 2))

      If CANMessage.ReverseByteOrder Then
        intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intEndByte = CANMessage.DecodingStartByte
        intByteIncrement = -1
      Else
        intStartByte = CANMessage.DecodingStartByte
        intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intByteIncrement = 1
      End If

      strByteValue = ""
      For intIndex = intStartByte To intEndByte Step intByteIncrement
        strByteValue = strByteValue & Data(intIndex)
      Next
      strReturnValue = strByteValue
      Return strReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function

  Private Function ReturnHexString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
    Try
      Dim intIndex As Integer
      Dim intStartByte As Integer
      Dim intEndByte As Integer
      Dim intByteIncrement As Integer
      'Dim intByteValue As Integer
      Dim strTemp As StringBuilder
      Dim strReturnValue As String


      strTemp = New StringBuilder((Data.Length * 2))

      If CANMessage.ReverseByteOrder Then
        intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intEndByte = CANMessage.DecodingStartByte
        intByteIncrement = -1
      Else
        intStartByte = CANMessage.DecodingStartByte
        intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intByteIncrement = 1
      End If

      For intIndex = intStartByte To intEndByte Step intByteIncrement
        'intByteValue = Data(intIndex)
        'strTemp.Append((Conversion.Hex(intByteValue)).PadLeft(2, "0"))
        strTemp.Append(Data(intIndex).ToString("X2"))
      Next
      strReturnValue = strTemp.ToString
      Return strReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function

  Private Function ReturnIntegerValueString(ByVal CANMessage As clsCANMessage, ByVal Data() As Byte) As String
    Try
      Dim intIndex As Integer
      Dim intStartByte As Integer
      Dim intEndByte As Integer
      Dim intByteIncrement As Integer
      Dim intByteValue As UInt32
      Dim intValue As UInt32
      'Dim strTemp As StringBuilder
      Dim strReturnValue As String
      Dim intShift As Integer

      'strTemp = New StringBuilder((Data.Length * 2))

      If CANMessage.ReverseByteOrder Then
        intStartByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intEndByte = CANMessage.DecodingStartByte
        intByteIncrement = -1
      Else
        intStartByte = CANMessage.DecodingStartByte
        intEndByte = CANMessage.DecodingStartByte + CANMessage.DecodingByteLength - 1
        intByteIncrement = 1
      End If

      intByteValue = 0
      intValue = 0
      intShift = 0
      For intIndex = intStartByte To intEndByte Step intByteIncrement
        'intByteValue = intByteValue + (Data(intIndex) * (2^(8 * intShift)))
        intByteValue = Data(intIndex)
        intValue = intValue + (intByteValue << (8 * intShift))
        intShift += 1
      Next
      strReturnValue = CStr(intValue)
      Return strReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function

  Private Function SendMessageNoResponse(canMessage As clsCANMessage) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim canMsg As New TPCANMsg
    Dim blnretry As Boolean
    Dim blnMessageAvailable As Boolean
    Dim strErrorMessage As String

    Try
      blnretry = True
      blnMessageAvailable = False
      strErrorMessage = ""

      'Do
      '  'Consume any previous response.
      '  GetToolMsgResp(canMessage.OpCode, canMsg)

      '  'Load Output Queue with Message.
      '  mCanMsgHandler.LoadCANMsgOutputQueue(canMessage)

      '  'Wait for response or timeout
      '  blnMessageAvailable = WaitForMessageAvailable(canMessage.OpCode)

      '  ' Read the response.
      '  If (blnMessageAvailable AndAlso (GetToolMsgResp(canMessage.OpCode, canMsg) = True) AndAlso (canMsg.DATA.Length >= 7)) Then
      '    data = canMsg.DATA
      '    blnreturnValue = True
      '  End If

      '  If (blnreturnValue = False) Then
      '    ' Retry one time, if necessary.
      '    If (blnretry = True) Then
      '      blnretry = False
      '    Else
      '      Exit Do
      '    End If
      '  End If
      'Loop While (blnreturnValue = False)

      'Load Output Queue with Message.
      mCanMsgHandler.LoadCANMsgOutputQueue(canMessage)

      Return True 'blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Function SendMessageGetResponse(canMessage As clsCANMessage, ByRef data() As Byte) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim canMsg As New TPCANMsg
    Dim blnretry As Boolean
    Dim blnMessageAvailable As Boolean
    Dim strErrorMessage As String

    Try
      blnretry = True
      blnMessageAvailable = False
      strErrorMessage = ""

      Do
        'Consume any previous response.
        GetToolMsgResp(canMessage.OpCode, canMsg)

        'Load Output Queue with Message.
        mCanMsgHandler.LoadCANMsgOutputQueue(canMessage)

        'Wait for response or timeout
        blnMessageAvailable = WaitForMessageAvailable(canMessage.OpCode)

        ' Read the response.
        If (blnMessageAvailable AndAlso (GetToolMsgResp(canMessage.OpCode, canMsg) = True) AndAlso (canMsg.DATA.Length >= 7)) Then
          data = canMsg.DATA
          blnreturnValue = True
        End If

        If (blnreturnValue = False) Then
          ' Retry one time, if necessary.
          If (blnretry = True) Then
            blnretry = False
          Else
            Exit Do
          End If
        End If
      Loop While (blnreturnValue = False)

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
    End Try
  End Function

  Private Sub AddressClaimRcvd(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)

    ' An address claim message has been received.
    ' Store one message for each responding actuator.
    Dim rcvdCanMsg As TPCANMsg = MsgStruct.CanMsg

    Try
      SyncLock mAddrClaimLock
        If (mAddressClaimMsgs.ContainsKey(CByte(rcvdCanMsg.ID And &HFF).ToString) <> True) Then
          ' This is a message with a new source address so store it.
          mAddressClaimMsgs.TryAdd(CByte(rcvdCanMsg.ID And &HFF).ToString, rcvdCanMsg)
          ' Get new actuator count.
          mActuatorCount = mAddressClaimMsgs.Count
        End If
      End SyncLock

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  Private Sub StatusMsgRcvd(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)
    ' A status message has been received.
    ' Store one message for each responding actuator.
    Dim rcvdCanMsg As TPCANMsg = MsgStruct.CanMsg 'New Message
    Dim msg_str As New TPCANMsg 'Dummy to hold messages for TryRemove
    Dim strKey As String
    Dim strCANMessageLog As String

    Try
      'Key is SourceID
      strKey = CByte(rcvdCanMsg.ID And &HFF).ToString

      SyncLock mStatusMsgLock
        If (mStatusMsgs.ContainsKey(strKey) = True) Then
          ' Remove the message previously received from this actuator.
          mStatusMsgs.TryRemove(strKey, msg_str)
        End If
        ' Store the latest message.
        rcvdCanMsg = MsgStruct.CanMsg
        mStatusMsgs.TryAdd(strKey, rcvdCanMsg)

        'Log CAN Messages if enabled
        'strCANMessageLog = "Status Message Response Received, MessageID =, " & Hex(rcvdCanMsg.ID) & ", OpCode =, " & MsgStruct.CanMsg.DATA(1).ToString("X2") & MsgStruct.CanMsg.DATA(0).ToString("X2")
        'Call mCanMsgHandler.LogCanMessage(strCANMessageLog)
        'Call mCanMsgHandler.LogCanMessage()
        'strCANMessageLog = "Status Message Response Received, MessageID =, " & Hex(rcvdCanMsg.ID) & ", OpCode =, " & MsgStruct.CanMsg.DATA(1).ToString("X2") & MsgStruct.CanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", MsgStruct.CanMsg.DATA)
        'mCanMsgHandler.LoadCANMsgLogQueue(strCANMessageLog)
      End SyncLock

      'Console.WriteLine(Hex(rcvdCanMsg.ID))

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub


  Private Sub ToolMsgRespRcvd(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)

    Dim rcvdCanMsg As TPCANMsg = MsgStruct.CanMsg 'New Message
    Dim readMsgStruct As clsCanMsgHandling.CAN_Msg_Struct 'Previous Message of Same OpCode
    Dim msgOpcode As UShort
    Dim key As String = ""
    Dim msg_str As New clsCanMsgHandling.CAN_Msg_Struct 'Dummy to hold messages for TryRemove
    Dim DefaultBootloaderAttnOpcode As UShort
    Dim strCANMessageLog As String
    Dim intResponse As Integer
    Try
      DefaultBootloaderAttnOpcode = gCANActuator.CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest).OpCode

      ' Store received messages of interest, all others are ignored
      If (mblnApplicationCodeRunning = True) Then
        'If ((rcvdCanMsg.ID >= &H38FF0D00) AndAlso (rcvdCanMsg.ID <= &H38FF0DFF) AndAlso _
        'If ((rcvdCanMsg.ID = (TOOL_MSG_RESPONSE + mActuatorAddress)) AndAlso1
        intResponse = rcvdCanMsg.ID And SRC_ADDR_MASK
        If ((intResponse = TOOL_MSG_RESPONSE) AndAlso (rcvdCanMsg.DATA.Length > 1)) Then
          msgOpcode = rcvdCanMsg.DATA(0)
          msgOpcode += CUShort(rcvdCanMsg.DATA(1)) << 8
          SyncLock mToolResponseLock
            'Select Case msgOpcode
            '  Case OPCODE.SW_VERSION_ID, _
            '       OPCODE.APPLICATION_CHKSUM_CALC, _
            '       OPCODE.SERIAL_NUMBER, _
            '       OPCODE.HW_PART_NUM_MS, _
            '       OPCODE.HW_PART_NUM_LS, _
            '       OPCODE.SW_PART_NUM_MS, _
            '       OPCODE.SW_PART_NUM_LS, _
            '       OPCODE.INTERNAL_TEMP, _
            '       OPCODE.BATTERY_VOLTAGE, _
            '       OPCODE.MICRO_OSC_FREQ, _
            '       OPCODE.MICRO_PLL_SELECT

            key = msgOpcode.ToString
            If (mToolResponseMsgs.ContainsKey(key)) Then
              ' This is not the first message of its type, so increment the message count.
              readMsgStruct = mToolResponseMsgs.Item(key)
              ' Remove previous message.
              mToolResponseMsgs.TryRemove(key, msg_str)
              ' Increment and store message count.
              MsgStruct.msgCount = readMsgStruct.msgCount + 1
            Else
              MsgStruct.msgCount = 1
            End If
            ' Store latest message.
            mToolResponseMsgs.TryAdd(key, MsgStruct)
            'Log CAN Messages if enabled
            'strCANMessageLog = "Tool Message Response Received, MessageID =, " & Hex(rcvdCanMsg.ID) & ", OpCode =, " & rcvdCanMsg.DATA(1).ToString("X2") & rcvdCanMsg.DATA(0).ToString("X2")
            'Call mCanMsgHandler.LogCanMessage(strCANMessageLog)
            'Call mCanMsgHandler.LogCanMessage()
            'strCANMessageLog = "Tool Message Response Received, MessageID =, " & Hex(rcvdCanMsg.ID) & ", OpCode =, " & rcvdCanMsg.DATA(1).ToString("X2") & rcvdCanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", rcvdCanMsg.DATA)
            'mCanMsgHandler.LoadCANMsgLogQueue(strCANMessageLog)

            'End Select
          End SyncLock
        End If
      Else
        If ((rcvdCanMsg.ID = (TOOL_MSG_RESPONSE + DFLT_BOOTLDR_ADDR)) AndAlso _
            (rcvdCanMsg.DATA.Length > 0)) Then
          msgOpcode = rcvdCanMsg.DATA(0)
          SyncLock mDfltBtldrToolRespLock
            'Select Case msgOpcode
            '  Case OPCODE.DFLT_BOOTLDR_ATTN, _
            '       OPCODE.DFLT_BOOTLDR_INIT_WRITE, _
            '       OPCODE.DFLT_BOOTLDR_MEM_WRITE, _
            '       OPCODE.DFLT_BOOTLDR_INIT_READ, _
            '       OPCODE.DFLT_BOOTLDR_MEM_READ, _
            '       OPCODE.DFLT_BOOTLDR_CALC_CHKSUM, _
            '       OPCODE.DFLT_BOOTLDR_SERIAL_NUMBER, _
            '       OPCODE.DFLT_BOOTLDR_CLOSE_SESSION, _
            '       OPCODE.DFLT_BOOTLDR_START_APP, _
            '       OPCODE.DFLT_BOOTLDR_RESTART, _
            '       OPCODE.DFLT_BOOTLDR_FAULT

            key = msgOpcode.ToString
            If (mDfltBtldrToolRespMsgs.ContainsKey(key)) Then
              ' This is not the first message of its type, so increment the message count.
              readMsgStruct = mDfltBtldrToolRespMsgs.Item(key)
              ' Remove previous message.
              mDfltBtldrToolRespMsgs.TryRemove(key, msg_str)
              ' Increment and store message count.
              MsgStruct.msgCount = readMsgStruct.msgCount + 1
            Else
              MsgStruct.msgCount = 1
            End If
            ' Store latest message.
            mDfltBtldrToolRespMsgs.TryAdd(key, MsgStruct)
            If ((msgOpcode = DefaultBootloaderAttnOpcode) AndAlso (rcvdCanMsg.LEN = 7)) Then
              mblnDefaultBootloaderCodeRunning = True
            End If
            'Log CAN Messages if enabled
            'strCANMessageLog = "Tool Message Response Received, MessageID=, " & Hex(rcvdCanMsg.ID) & ", OpCode=, " & rcvdCanMsg.DATA(0).ToString("X2")
            'Call mCanMsgHandler.LogCanMessage(strCANMessageLog)
            'Call mCanMsgHandler.LogCanMessage()
            'strCANMessageLog = "DBL Tool Message Response Received, MessageID=, " & Hex(rcvdCanMsg.ID) & ", OpCode=, " & rcvdCanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", rcvdCanMsg.DATA)
            'mCanMsgHandler.LoadCANMsgLogQueue(strCANMessageLog)

            'End Select
          End SyncLock
        End If
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub


  Private Function WaitForMessageAvailable(ByVal OpCode As UInteger) As Boolean

    Dim returnValue As Boolean = False
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try
      ' Initialize the elapsed time starting value
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
      ' Wait up to one second.
      Do
        If (GetToolMsgRespCount(OpCode) > 0) Then
          ' Received message count > 0
          returnValue = True
          Exit Do
        End If
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < 1000)

      Return returnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Private Sub ActuatorDownloadThreadFunction()
    Try
      Do
        ' Block until a CAN message is queued for transmit.
        mActuatorDownloadWaitHandle.WaitOne()
        If (mblnTerminateActuatorDownloadThread = True) Then
          ' Allow this thread to terminate.
          Exit Do
        End If

        Call ActuatorDownload()

      Loop

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Private Sub StopActuatorDownloadThread()
    Try

      If ((mActuatorDownloadThread IsNot Nothing) AndAlso _
          (mActuatorDownloadThread.ThreadState <> ThreadState.AbortRequested) AndAlso _
          (mActuatorDownloadThread.ThreadState <> ThreadState.Aborted)) Then

        mblnTerminateActuatorDownloadThread = True
        mActuatorDownloadWaitHandle.Set()

        If mActuatorDownloadThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
          While (mActuatorDownloadThread.ThreadState <> System.Threading.ThreadState.Stopped)
            Application.DoEvents()
            Thread.Sleep(1)
          End While
        End If
        'XmtCanMessageThread.Abort()
        mActuatorDownloadThread = Nothing
      End If

      mblnTerminateActuatorDownloadThread = False

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub


  Private Function ActuatorDownload() As Boolean

    Dim blnReturnValue As Boolean = False
    Dim dataRecord As IntelDataRecord
    Dim intRecord As Integer
    Dim idx1 As Byte
    Dim idx2 As Byte
    Dim data(7) As Byte
    Dim recBytesWritten As Byte
    Dim recBytesToBeWritten As Byte
    Dim canMsg As New TPCANMsg
    Dim blnDownloadAborted As Boolean
    Dim blnFirstPass As Boolean = False
    Dim intTotalBytesWritten As Integer
    Dim intLastTotalBytesWritten As Integer
    Dim ComErrorCount As Byte
    Dim EEPromDataChecksumPass() As Byte = {&HFF, 0, 0, 0, 0, 0, 0, 0}
    Dim PageRetryCount As Byte

    Dim strErrorMessage As String = ""
    Dim DfltBtldrInitMemWriteMsg As clsCANMessage
    Dim DfltBtldMemWriteDataMsg As clsCANMessage
    Dim strSupplementalDiagnosticText As String = ""
    Dim lStopWatch As New clsStopWatch
    Try

      DfltBtldrInitMemWriteMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryWriteRequest)
      DfltBtldMemWriteDataMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryWriteData)

      lStopWatch.Reset()
      lStopWatch.Start()
      Do

        'Application.DoEvents()
        'Thread.Sleep(1)

        ComErrorCount = 0
        intTotalBytesWritten = 0
        intLastTotalBytesWritten = 0
        blnFirstPass = blnFirstPass Xor True
        blnDownloadAborted = False

        ' Clear default bootloader tool message response storage.
        SyncLock mDfltBtldrToolRespLock
          mDfltBtldrToolRespMsgs.Clear()
        End SyncLock

        intRecord = 0
        'While (i <= IntelHexFile.Count)
        While (intRecord <= mIntelHexFileDownload.Count)
          If (ComErrorCount > 10) Then
            If (blnFirstPass = False) Then
              'finalRslt = FinalResult.ExcessiveComFailures
              strErrorMessage = "Excessive Communication Failures"
            End If
            blnDownloadAborted = True
            Exit While
          End If
          If (intRecord = 0) Then
            ' Writing to eeprom memory to erase application checksum valid byte.
            dataRecord = New IntelDataRecord
            dataRecord.StartAddress = &H7FF
            dataRecord.EndAddress = &H7FF
            dataRecord.DataLength = 1
            dataRecord.Data = EEPromDataChecksumPass
            ' Setup an initiate memory write CAN message for this data record.
            data = DfltBtldrInitMemWriteMsg.Data
            ' Load the CAN message data bytes.
            data(1) = TargetMemoryAreaEnum.EEProm
            data(2) = CByte(dataRecord.StartAddress And &HFF)
            data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
            data(4) = CByte(dataRecord.EndAddress And &HFF)
            data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
            DfltBtldrInitMemWriteMsg.Data = data
          Else
            ' Initiate next write to flash memory.
            dataRecord = mIntelHexFileDownload(intRecord - 1)
            ' Setup an initiate memory write CAN message for this data record.
            data = DfltBtldrInitMemWriteMsg.Data
            ' Writing to program memory.
            data(1) = TargetMemoryAreaEnum.ProgramMemory
            ' Load the CAN message data bytes.
            data(2) = CByte(dataRecord.StartAddress And &HFF)
            data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
            data(4) = CByte(dataRecord.EndAddress And &HFF)
            data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
            DfltBtldrInitMemWriteMsg.Data = data
          End If

          ' Send the initiate memory write CAN message.
          mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrInitMemWriteMsg)
          Call mCanMsgHandler.LogCanActuatorActivity("Sent Init Mem Write, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

          ' Look for an acknowledgement (ACK) CAN message from the
          ' actuator specifying the number of bytes to be written.
          If (GetMemoryWriteInitAck(canMsg) <> True) Then
            ' The actuator returned an error message in response to an initiate
            ' memory write command or failed to respond to an initiate memory
            ' write command while downloading application.
            ComErrorCount += 1
            PageRetryCount += 1
            If (CloseSession() = False) Then
              ' Failed to close DBL programming session prior to re-writing page.
              'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
              strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
              blnDownloadAborted = True
              Exit While
            Else
              ' Start again at the beginning of the current page.
              strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Error."
              If (intTotalBytesWritten <= 256) Then
                intRecord = 0
              ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
                intRecord = (intTotalBytesWritten \ 256) - 1
              Else
                intRecord = intTotalBytesWritten \ 256
              End If
              intTotalBytesWritten = intRecord * 256
              intRecord = intTotalBytesWritten \ 16
              intLastTotalBytesWritten = intTotalBytesWritten
              '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
              Continue While
            End If
          Else
            Call mCanMsgHandler.LogCanActuatorActivity("Received Init Mem Write Ack, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
            If (dataRecord.DataLength <> (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
              ' The number of bytes specified in the ACK message does
              ' not match the number of bytes in the data record.
              ComErrorCount += 1
              PageRetryCount += 1
              If (CloseSession() = False) Then
                ' Failed to close DBL programming session prior to re-writing page.
                'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
                strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
                blnDownloadAborted = True
                Exit While
              Else
                ' Start again at the beginning of the current page.
                strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Byte Count."
                If (intTotalBytesWritten <= 256) Then
                  intRecord = 0
                ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
                  intRecord = (intTotalBytesWritten \ 256) - 1
                Else
                  intRecord = intTotalBytesWritten \ 256
                End If
                intTotalBytesWritten = intRecord * 256
                intRecord = intTotalBytesWritten \ 16
                intLastTotalBytesWritten = intTotalBytesWritten
                '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
                Continue While
              End If
            End If
          End If

          ' Setup a memory write data CAN message for this data record.
          recBytesWritten = 0
          data = DfltBtldMemWriteDataMsg.Data

          'Console.WriteLine("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount)
          mintActuatorDownloadPercentComplete = CInt(intRecord / mIntelHexFileDownload.Count * 100)
          'Call mCanMsgHandler.LogCanActuatorActivity("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

          Do
            ' Setup write byte count. A maximum of 7 bytes can be written in one message.
            If ((dataRecord.DataLength - recBytesWritten) <= 7) Then
              recBytesToBeWritten = dataRecord.DataLength - recBytesWritten
            Else
              recBytesToBeWritten = 7
            End If
            ' Load the CAN message data bytes.
            ' Message payload begins at data(1).
            idx1 = 1
            ' Assumes that bytesToBeWritten will never be 0.
            For idx2 = recBytesWritten To (recBytesWritten + recBytesToBeWritten - 1)
              data(idx1) = dataRecord.Data(idx2)
              idx1 += 1
            Next
            DfltBtldMemWriteDataMsg.Data = data
            ' Set the CAN message data length (data byte count + 1 byte opcode).
            DfltBtldMemWriteDataMsg.DataLength = recBytesToBeWritten + 1
            ' Send the memory write data CAN message.
            mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldMemWriteDataMsg)
            Call mCanMsgHandler.LogCanActuatorActivity("Sent Mem Write Data, Record, " & intRecord & ", Bytes, " & recBytesToBeWritten & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

            'Console.WriteLine("Writing Record " & intRecord & ": Data= " & String.Join(",", data))

            ' Look for an acknowledgement (ACK) CAN message from the actuator
            ' containing a value that represents the number of bytes remaining to be written.
            If (GetMemoryWriteDataAck(canMsg) <> True) Then
              ' The actuator returned an error message in response to a
              ' memory write data command or failed to respond to a
              ' memory write data command while downloading application.
              ComErrorCount += 1
              PageRetryCount += 1
              If (CloseSession() = False) Then
                ' Failed to close DBL programming session prior to re-writing page.
                'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
                strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
                blnDownloadAborted = True
                Exit While
              Else
                ' Start again at the beginning of the current page.
                strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Error."
                If (intTotalBytesWritten <= 256) Then
                  intRecord = 0
                ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
                  intRecord = (intTotalBytesWritten \ 256) - 1
                Else
                  intRecord = intTotalBytesWritten \ 256
                End If
                intTotalBytesWritten = intRecord * 256
                intRecord = intTotalBytesWritten \ 16
                intLastTotalBytesWritten = intTotalBytesWritten
                '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
                Continue While
              End If
            Else
              Call mCanMsgHandler.LogCanActuatorActivity("Received Mem Write Data Ack, Record, " & intRecord & ", BytesRemaining, " & (canMsg.DATA(1) + (canMsg.DATA(2) << 8)) & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
              If ((dataRecord.DataLength - (recBytesWritten + recBytesToBeWritten)) <>
                  (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
                ' The number of bytes remaining value contained in the ACK message does
                ' not match the number of remaining bytes calculated based on the length
                ' of this data record and the number of bytes written so far.
                ComErrorCount += 1
                PageRetryCount += 1
                If (CloseSession() = False) Then
                  ' Failed to close DBL programming session prior to re-writing page.
                  'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
                  strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
                  blnDownloadAborted = True
                  Exit While
                Else
                  ' Start again at the beginning of the current page.
                  strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Byte Count."
                  If (intTotalBytesWritten <= 256) Then
                    intRecord = 0
                  ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
                    intRecord = (intTotalBytesWritten \ 256) - 1
                  Else
                    intRecord = intTotalBytesWritten \ 256
                  End If
                  intTotalBytesWritten = intRecord * 256
                  intRecord = intTotalBytesWritten \ 16
                  intLastTotalBytesWritten = intTotalBytesWritten
                  '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
                  Continue While
                End If
              End If
            End If
            ' Update the bytesWritten count.
            recBytesWritten += recBytesToBeWritten
            'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Bytes To Be Written, " & recBytesToBeWritten)
            'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
            'Call mCanMsgHandler.LogCanActuatorActivity("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
          Loop While (recBytesWritten < dataRecord.DataLength)

          If (intRecord <> 0) Then
            intTotalBytesWritten += recBytesWritten
          End If
          If ((intTotalBytesWritten - intLastTotalBytesWritten) >= 100) Then
            'ReportProgress(((intTotalBytesWritten * 100) / _fileSize) + 100)
            intLastTotalBytesWritten = intTotalBytesWritten
          End If
          intRecord += 1
        End While

        ' Delay briefly before sending the next command.
        Delay(25)
        SyncLock mDfltBtldrToolRespLock
          mDfltBtldrToolRespMsgs.Clear()
        End SyncLock
        ' Close the default bootloader programming session.
        If (CloseSession() = False) Then ' two tries built in to CloseSession
          ' Failed to receive close session ack msg in three attempts.? actually two, TER
          If (blnDownloadAborted = False) Then
            ' The download was otherwise successful so try starting application code.
            If StartApplication() Then
              blnReturnValue = True
              mbytDefaultBootloaderFault = 0
              Exit Do
            Else
              blnDownloadAborted = True
              strSupplementalDiagnosticText = "ActuatorDownload() - Failed To Close Program Session."
            End If
          ElseIf (blnFirstPass <> True) Then
            ' Failed to close DBL programming session at end of download attempt.
            'finalRslt = FinalResult.ClosePrgmSessionFailed
            strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
            Exit Do
          End If
        End If

        If (blnDownloadAborted = False) Then
          ' This was a clean download.
          blnReturnValue = True
          Exit Do
        ElseIf (blnFirstPass = True) Then
          ' The first download failed so attempt to download one more time.
          ' Delay briefly before sending the next command.
          Delay(25)
          ' Clear default bootloader tool message response storage.
          SyncLock mDfltBtldrToolRespLock
            mDfltBtldrToolRespMsgs.Clear()
          End SyncLock
          '_dfltBootldrResponded = False
          mblnDefaultBootloaderCodeRunning = False
          mblnApplicationCodeRunning = False
          ' Send a default bootloader attention request message.
          mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest))
          Delay(25)
          If (mblnDefaultBootloaderCodeRunning <> True) Then
            ' Failed to detect a DBL attention request response. The download has failed.
            'finalRslt = FinalResult.NoDblResponseDuringRetry
            strErrorMessage = "Failed to detect Default Bootloader Attention Request"
            Exit Do
          End If
          ' We're here because the DBL responded, so attempt to download one more time.
        End If
        ' Loop one additional time if an error causes the download to abort.
        ' If 2nd attempt fails then fall through and exit.
      Loop While ((blnDownloadAborted = True) AndAlso (blnFirstPass = True))

      If blnDownloadAborted Then
        If strSupplementalDiagnosticText <> "" Then
          strSupplementalDiagnosticText = "Page Retries = " & PageRetryCount.ToString() & ", " & strSupplementalDiagnosticText
        End If
        strErrorMessage = strErrorMessage & strSupplementalDiagnosticText
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
      ' Unregister for status messages.
      mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
      lStopWatch.Stop()
      mblnActuatorDownloadComplete = True
    End Try

  End Function


#End Region

#Region "Public Methods =================================================="

  'Public Function ActuatorPresentOnBus() As Boolean
  '  Dim blnActuatorPresent As Boolean
  '  Try

  '    'Clear Previous Status Messages
  '    SyncLock StatusMsgLock
  '      StatusMsgs.Clear()
  '    End SyncLock

  '    'Register for status messages call-back
  '    _CanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
  '    If _CanMsgHandler.RcvMsgDelegates.Anomaly IsNot Nothing Then
  '      Me.Anomaly = _CanMsgHandler.RcvMsgDelegates.Anomaly
  '      _CanMsgHandler.RcvMsgDelegates.Anomaly = Nothing
  '      Throw New TsopAnomalyException
  '    End If
  '    Delay(100)

  '    'Console.WriteLine(StatusMsgs.Count)

  '    If StatusMsgs.Count = 1 Then
  '      'Actuator is present since status message was found for it
  '      blnActuatorPresent = True
  '    Else
  '      blnActuatorPresent = False
  '    End If

  '    Return blnActuatorPresent

  '  Catch ex As TsopAnomalyException
  '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
  '    Return False
  '  Catch ex As Exception
  '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
  '    Return False
  '  Finally
  '    ' Unregister for status messages.
  '    _CanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)

  '  End Try
  'End Function

  Public Function APSCalibrate(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim intDynamicWriteByteIndex As Integer
    Dim intDataIndex As Integer

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.APSCalibrate)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      intDynamicWriteByteIndex = 0
      For intDataIndex = 0 To CanMessage.StaticData.Length - 1
        If CanMessage.StaticData(intDataIndex).ToUpper = "XX" Then
          CanMessage.Data(intDataIndex) = dynamicData(intDynamicWriteByteIndex)
          intDynamicWriteByteIndex += 1
        End If
      Next
      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        'Console.WriteLine(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error in APS Calibrate CAN Message"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


  Public Sub Delay(ByVal time As UInteger)
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try
      ' Initialize the elapsed time starting value.
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
      Do
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < time)

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Public Function DisablePositionCommandTimeout(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.DisableZenCorrection)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        'Console.WriteLine(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Disabling Position Command Timeout"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function DisableZenCorrection(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.DisableZenCorrection)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        'Console.WriteLine(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Disabling ZEN Correction"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Sub DownloadIntelHexIntoEEProm()
    'Dim intPercentMarks() As Integer
    'Dim intNextMark As Integer
    Try

      'intPercentMarks = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110}
      'intNextMark = 0
      mblnActuatorDownloadComplete = False
      mintActuatorDownloadPercentComplete = 0

      ' Unblock the Actuator Download thread.
      mActuatorDownloadWaitHandle.Set()

      'Do
      '  'Application.DoEvents()
      '  Thread.Sleep(0)
      '  'If mintActuatorDownloadPercentComplete >= intPercentMarks(intNextMark) Then
      '  '  Console.WriteLine("Actuator Download " & mintActuatorDownloadPercentComplete & " Percent Complete")
      '  '  If intNextMark < intPercentMarks.Length - 1 Then
      '  '    intNextMark += 1
      '  '  End If
      '  'End If
      'Loop Until mblnActuatorDownloadComplete = True

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  'Public Function DownloadIntelHexIntoEEProm() As Boolean

  '  Dim blnReturnValue As Boolean = False
  '  Dim dataRecord As IntelDataRecord
  '  Dim intRecord As Integer
  '  Dim idx1 As Byte
  '  Dim idx2 As Byte
  '  Dim data(7) As Byte
  '  Dim recBytesWritten As Byte
  '  Dim recBytesToBeWritten As Byte
  '  Dim canMsg As New TPCANMsg
  '  Dim blnDownloadAborted As Boolean
  '  Dim blnFirstPass As Boolean = False
  '  Dim intTotalBytesWritten As Integer
  '  Dim intLastTotalBytesWritten As Integer
  '  Dim ComErrorCount As Byte
  '  Dim EEPromDataChecksumPass() As Byte = {&HFF, 0, 0, 0, 0, 0, 0, 0}
  '  Dim PageRetryCount As Byte

  '  Dim strErrorMessage As String = ""
  '  Dim DfltBtldrInitMemWriteMsg As clsCANMessage
  '  Dim DfltBtldMemWriteDataMsg As clsCANMessage
  '  Dim strSupplementalDiagnosticText As String = ""
  '  Dim lStopWatch As New clsStopWatch
  '  Try

  '    DfltBtldrInitMemWriteMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryWriteRequest)
  '    DfltBtldMemWriteDataMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryWriteData)

  '    lStopWatch.Reset()
  '    lStopWatch.Start()
  '    Do

  '      'Application.DoEvents()
  '      'Thread.Sleep(1)

  '      ComErrorCount = 0
  '      intTotalBytesWritten = 0
  '      intLastTotalBytesWritten = 0
  '      blnFirstPass = blnFirstPass Xor True
  '      blnDownloadAborted = False

  '      ' Clear default bootloader tool message response storage.
  '      SyncLock mDfltBtldrToolRespLock
  '        mDfltBtldrToolRespMsgs.Clear()
  '      End SyncLock

  '      intRecord = 0
  '      'While (i <= IntelHexFile.Count)
  '      While (intRecord <= mIntelHexFileDownload.Count)
  '        If (ComErrorCount > 10) Then
  '          If (blnFirstPass = False) Then
  '            'finalRslt = FinalResult.ExcessiveComFailures
  '            strErrorMessage = "Excessive Communication Failures"
  '          End If
  '          blnDownloadAborted = True
  '          Exit While
  '        End If
  '        If (intRecord = 0) Then
  '          ' Writing to eeprom memory to erase application checksum valid byte.
  '          dataRecord = New IntelDataRecord
  '          dataRecord.StartAddress = &H7FF
  '          dataRecord.EndAddress = &H7FF
  '          dataRecord.DataLength = 1
  '          dataRecord.Data = EEPromDataChecksumPass
  '          ' Setup an initiate memory write CAN message for this data record.
  '          data = DfltBtldrInitMemWriteMsg.Data
  '          ' Load the CAN message data bytes.
  '          data(1) = TargetMemoryAreaEnum.EEProm
  '          data(2) = CByte(dataRecord.StartAddress And &HFF)
  '          data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
  '          data(4) = CByte(dataRecord.EndAddress And &HFF)
  '          data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
  '          DfltBtldrInitMemWriteMsg.Data = data
  '        Else
  '          ' Initiate next write to flash memory.
  '          dataRecord = mIntelHexFileDownload(intRecord - 1)
  '          ' Setup an initiate memory write CAN message for this data record.
  '          data = DfltBtldrInitMemWriteMsg.Data
  '          ' Writing to program memory.
  '          data(1) = TargetMemoryAreaEnum.ProgramMemory
  '          ' Load the CAN message data bytes.
  '          data(2) = CByte(dataRecord.StartAddress And &HFF)
  '          data(3) = CByte((dataRecord.StartAddress >> 8) And &HFF)
  '          data(4) = CByte(dataRecord.EndAddress And &HFF)
  '          data(5) = CByte((dataRecord.EndAddress >> 8) And &HFF)
  '          DfltBtldrInitMemWriteMsg.Data = data
  '        End If

  '        ' Send the initiate memory write CAN message.
  '        mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrInitMemWriteMsg)
  '        Call mCanMsgHandler.LogCanActuatorActivity("Sent Init Mem Write, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

  '        ' Look for an acknowledgement (ACK) CAN message from the
  '        ' actuator specifying the number of bytes to be written.
  '        If (GetMemoryWriteInitAck(canMsg) <> True) Then
  '          ' The actuator returned an error message in response to an initiate
  '          ' memory write command or failed to respond to an initiate memory
  '          ' write command while downloading application.
  '          ComErrorCount += 1
  '          PageRetryCount += 1
  '          If (CloseSession() = False) Then
  '            ' Failed to close DBL programming session prior to re-writing page.
  '            'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
  '            strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
  '            blnDownloadAborted = True
  '            Exit While
  '          Else
  '            ' Start again at the beginning of the current page.
  '            strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Error."
  '            If (intTotalBytesWritten <= 256) Then
  '              intRecord = 0
  '            ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
  '              intRecord = (intTotalBytesWritten \ 256) - 1
  '            Else
  '              intRecord = intTotalBytesWritten \ 256
  '            End If
  '            intTotalBytesWritten = intRecord * 256
  '            intRecord = intTotalBytesWritten \ 16
  '            intLastTotalBytesWritten = intTotalBytesWritten
  '            '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
  '            Continue While
  '          End If
  '        Else
  '          Call mCanMsgHandler.LogCanActuatorActivity("Received Init Mem Write Ack, Record, " & intRecord & ", , , ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
  '          If (dataRecord.DataLength <> (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
  '            ' The number of bytes specified in the ACK message does
  '            ' not match the number of bytes in the data record.
  '            ComErrorCount += 1
  '            PageRetryCount += 1
  '            If (CloseSession() = False) Then
  '              ' Failed to close DBL programming session prior to re-writing page.
  '              'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
  '              strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
  '              blnDownloadAborted = True
  '              Exit While
  '            Else
  '              ' Start again at the beginning of the current page.
  '              strSupplementalDiagnosticText = "Last Page Retry - Write Init Ack Byte Count."
  '              If (intTotalBytesWritten <= 256) Then
  '                intRecord = 0
  '              ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
  '                intRecord = (intTotalBytesWritten \ 256) - 1
  '              Else
  '                intRecord = intTotalBytesWritten \ 256
  '              End If
  '              intTotalBytesWritten = intRecord * 256
  '              intRecord = intTotalBytesWritten \ 16
  '              intLastTotalBytesWritten = intTotalBytesWritten
  '              '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
  '              Continue While
  '            End If
  '          End If
  '        End If

  '        ' Setup a memory write data CAN message for this data record.
  '        recBytesWritten = 0
  '        data = DfltBtldMemWriteDataMsg.Data

  '        'Console.WriteLine("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount)
  '        'Call mCanMsgHandler.LogCanActuatorActivity("Writing Record, " & intRecord & ", ComErrorCount, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

  '        Do
  '          ' Setup write byte count. A maximum of 7 bytes can be written in one message.
  '          If ((dataRecord.DataLength - recBytesWritten) <= 7) Then
  '            recBytesToBeWritten = dataRecord.DataLength - recBytesWritten
  '          Else
  '            recBytesToBeWritten = 7
  '          End If
  '          ' Load the CAN message data bytes.
  '          ' Message payload begins at data(1).
  '          idx1 = 1
  '          ' Assumes that bytesToBeWritten will never be 0.
  '          For idx2 = recBytesWritten To (recBytesWritten + recBytesToBeWritten - 1)
  '            data(idx1) = dataRecord.Data(idx2)
  '            idx1 += 1
  '          Next
  '          DfltBtldMemWriteDataMsg.Data = data
  '          ' Set the CAN message data length (data byte count + 1 byte opcode).
  '          DfltBtldMemWriteDataMsg.DataLength = recBytesToBeWritten + 1
  '          ' Send the memory write data CAN message.
  '          mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldMemWriteDataMsg)
  '          Call mCanMsgHandler.LogCanActuatorActivity("Sent Mem Write Data, Record, " & intRecord & ", Bytes, " & recBytesToBeWritten & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)

  '          'Console.WriteLine("Writing Record " & intRecord & ": Data= " & String.Join(",", data))

  '          ' Look for an acknowledgement (ACK) CAN message from the actuator
  '          ' containing a value that represents the number of bytes remaining to be written.
  '          If (GetMemoryWriteDataAck(canMsg) <> True) Then
  '            ' The actuator returned an error message in response to a
  '            ' memory write data command or failed to respond to a
  '            ' memory write data command while downloading application.
  '            ComErrorCount += 1
  '            PageRetryCount += 1
  '            If (CloseSession() = False) Then
  '              ' Failed to close DBL programming session prior to re-writing page.
  '              'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
  '              strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
  '              blnDownloadAborted = True
  '              Exit While
  '            Else
  '              ' Start again at the beginning of the current page.
  '              strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Error."
  '              If (intTotalBytesWritten <= 256) Then
  '                intRecord = 0
  '              ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
  '                intRecord = (intTotalBytesWritten \ 256) - 1
  '              Else
  '                intRecord = intTotalBytesWritten \ 256
  '              End If
  '              intTotalBytesWritten = intRecord * 256
  '              intRecord = intTotalBytesWritten \ 16
  '              intLastTotalBytesWritten = intTotalBytesWritten
  '              '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
  '              Continue While
  '            End If
  '          Else
  '            Call mCanMsgHandler.LogCanActuatorActivity("Received Mem Write Data Ack, Record, " & intRecord & ", BytesRemaining, " & (canMsg.DATA(1) + (canMsg.DATA(2) << 8)) & ", ComErrors, " & ComErrorCount & ", " & lStopWatch.ElapsedMilliseconds)
  '            If ((dataRecord.DataLength - (recBytesWritten + recBytesToBeWritten)) <>
  '                (canMsg.DATA(1) + (canMsg.DATA(2) << 8))) Then
  '              ' The number of bytes remaining value contained in the ACK message does
  '              ' not match the number of remaining bytes calculated based on the length
  '              ' of this data record and the number of bytes written so far.
  '              ComErrorCount += 1
  '              PageRetryCount += 1
  '              If (CloseSession() = False) Then
  '                ' Failed to close DBL programming session prior to re-writing page.
  '                'finalRslt = FinalResult.ClosePrgmSessionForPageRetryFailed
  '                strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
  '                blnDownloadAborted = True
  '                Exit While
  '              Else
  '                ' Start again at the beginning of the current page.
  '                strSupplementalDiagnosticText = "Last Page Retry - Write Data Ack Byte Count."
  '                If (intTotalBytesWritten <= 256) Then
  '                  intRecord = 0
  '                ElseIf ((intTotalBytesWritten Mod 256) = 0) Then
  '                  intRecord = (intTotalBytesWritten \ 256) - 1
  '                Else
  '                  intRecord = intTotalBytesWritten \ 256
  '                End If
  '                intTotalBytesWritten = intRecord * 256
  '                intRecord = intTotalBytesWritten \ 16
  '                intLastTotalBytesWritten = intTotalBytesWritten
  '                '_parentForm.ErrorLED.Invoke(New Form1.ControlErrorLedDelegate(AddressOf _parentForm.ControlErrorLed), New Object() {True})
  '                Continue While
  '              End If
  '            End If
  '          End If
  '          ' Update the bytesWritten count.
  '          recBytesWritten += recBytesToBeWritten
  '          'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Bytes To Be Written, " & recBytesToBeWritten)
  '          'Console.WriteLine("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
  '          'Call mCanMsgHandler.LogCanActuatorActivity("Record, " & intRecord & ", Bytes Written, " & recBytesWritten & ", Record Data Length, " & dataRecord.DataLength)
  '        Loop While (recBytesWritten < dataRecord.DataLength)

  '        If (intRecord <> 0) Then
  '          intTotalBytesWritten += recBytesWritten
  '        End If
  '        If ((intTotalBytesWritten - intLastTotalBytesWritten) >= 100) Then
  '          'ReportProgress(((intTotalBytesWritten * 100) / _fileSize) + 100)
  '          intLastTotalBytesWritten = intTotalBytesWritten
  '        End If
  '        intRecord += 1
  '      End While

  '      ' Delay briefly before sending the next command.
  '      Delay(25)
  '      SyncLock mDfltBtldrToolRespLock
  '        mDfltBtldrToolRespMsgs.Clear()
  '      End SyncLock
  '      ' Close the default bootloader programming session.
  '      If (CloseSession() = False) Then ' two tries built in to CloseSession
  '        ' Failed to receive close session ack msg in three attempts.? actually two, TER
  '        If (blnDownloadAborted = False) Then
  '          ' The download was otherwise successful so try starting application code.
  '          If StartApplication() Then
  '            blnReturnValue = True
  '            mbytDefaultBootloaderFault = 0
  '            Exit Do
  '          Else
  '            blnDownloadAborted = True
  '            strSupplementalDiagnosticText = "ActuatorDownload() - Failed To Close Program Session."
  '          End If
  '        ElseIf (blnFirstPass <> True) Then
  '          ' Failed to close DBL programming session at end of download attempt.
  '          'finalRslt = FinalResult.ClosePrgmSessionFailed
  '          strErrorMessage = "Failed to Close Default Bootloader Prior to Re-Writing Page"
  '          Exit Do
  '        End If
  '      End If

  '      If (blnDownloadAborted = False) Then
  '        ' This was a clean download.
  '        blnReturnValue = True
  '        Exit Do
  '      ElseIf (blnFirstPass = True) Then
  '        ' The first download failed so attempt to download one more time.
  '        ' Delay briefly before sending the next command.
  '        Delay(25)
  '        ' Clear default bootloader tool message response storage.
  '        SyncLock mDfltBtldrToolRespLock
  '          mDfltBtldrToolRespMsgs.Clear()
  '        End SyncLock
  '        '_dfltBootldrResponded = False
  '        mblnDefaultBootloaderCodeRunning = False
  '        mblnApplicationCodeRunning = False
  '        ' Send a default bootloader attention request message.
  '        mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest))
  '        Delay(25)
  '        If (mblnDefaultBootloaderCodeRunning <> True) Then
  '          ' Failed to detect a DBL attention request response. The download has failed.
  '          'finalRslt = FinalResult.NoDblResponseDuringRetry
  '          strErrorMessage = "Failed to detect Default Bootloader Attention Request"
  '          Exit Do
  '        End If
  '        ' We're here because the DBL responded, so attempt to download one more time.
  '      End If
  '      ' Loop one additional time if an error causes the download to abort.
  '      ' If 2nd attempt fails then fall through and exit.
  '    Loop While ((blnDownloadAborted = True) AndAlso (blnFirstPass = True))

  '    If blnDownloadAborted Then
  '      If strSupplementalDiagnosticText <> "" Then
  '        strSupplementalDiagnosticText = "Page Retries = " & PageRetryCount.ToString() & ", " & strSupplementalDiagnosticText
  '      End If
  '      strErrorMessage = strErrorMessage & strSupplementalDiagnosticText
  '      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
  '      Throw New TsopAnomalyException
  '    End If

  '    Return blnReturnValue

  '  Catch ex As TsopAnomalyException
  '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
  '    Return False
  '  Catch ex As Exception
  '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
  '    Return False
  '  Finally
  '    ' Unregister for status messages.
  '    mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
  '    lStopWatch.Stop()

  '  End Try

  'End Function

  Public Function EnterBootloader() As Boolean
    Dim returnValue As Boolean = False
    Dim canMsg As TPCANMsg = New TPCANMsg
    Dim dblAttempts As Byte = 0
    Dim actuator_cnt As Integer
    Dim kv_pair As KeyValuePair(Of String, TPCANMsg)
    Dim strErrorMessage As String
    Try
      ' Verify that one actuator is present on the CAN bus.
      ' Register for status messages.
      mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)

      mblnDefaultBootloaderCodeRunning = False
      mblnApplicationCodeRunning = False
      ' Clear previously received status messages.
      SyncLock mStatusMsgLock
        mStatusMsgs.Clear()
      End SyncLock
      ' Wait for 100 milliseconds.
      Delay(100)
      SyncLock mStatusMsgLock
        actuator_cnt = mStatusMsgs.Count
        If (actuator_cnt = 1) Then
          ' Store the address of the active actuator.
          kv_pair = mStatusMsgs.First()
          mActuatorAddress = (kv_pair.Value.ID And &HFF).ToString
        End If
      End SyncLock
      If (actuator_cnt = 1) Then
        Do
          ' The bootloader has not responded so send a new
          ' jump to default bootloader command.
          mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.JumpToDefaultBootloader))
          ' Wait for 1250 milliseconds.
          Delay(1250)
          ' Clear default bootloader tool message response storage.
          SyncLock mDfltBtldrToolRespLock
            mDfltBtldrToolRespMsgs.Clear()
          End SyncLock
          ' Send default bootloader attention request message.
          mCanMsgHandler.LoadCANMsgOutputQueue(mCANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderAttnRequest))
          Delay(50)
          If (mblnDefaultBootloaderCodeRunning = True) Then
            ' Success, the bootloader has responded so exit.
            returnValue = True
            Exit Do
          End If
          dblAttempts += 1
        Loop While (dblAttempts < 3)
      Else
        strErrorMessage = "No Status Message Received From Actuator."
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return returnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
      ' Unregister for status messages.
      mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
    End Try
  End Function


  Public Function ReadApplicationSoftwareVersion(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadSoftwareVersion)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnASCIIStringDotDelimitEveryTwoBytes(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Application Software Version"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadAPSCalibrateProgress(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadAPSCalibrateProgress)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading APS Calibration Progress"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


  Public Function ReadAPSType(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadAPSType)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading APS Type"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadBatteryVoltage(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadBatteryVoltage)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        returnString = CSng(returnString) * 0.01
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Battery Voltage"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


  Public Function ReadBurnInCycles(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadBurnInCycles)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        returnString = 250 - CInt(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading BurnIn Cycles"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadBurnInTime(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadBurnInTime)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading BurnIn Time"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


  Public Function ReadCustomerBootloaderVersion(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadCustomerBootloaderVersion)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnASCIIStringDotDelimitEveryTwoBytes(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Customer Bootloader Version"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadDefaultBootloaderVersion(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadDefaultBootloaderVersion)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnASCIIStringDotDelimitEveryTwoBytes(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Default Bootloader Version"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadEEPromIntoIntelHexFormat() As Boolean
    Dim UploadRecord As New IntelDataRecord
    Dim blnReturnValue As Boolean
    Dim StartAddress As UShort
    Dim NextStartAddress As UShort
    Dim EndAddress As UShort
    Dim DataRecordLength As Integer
    Dim targetMemory As TargetMemoryAreaEnum
    Dim read_error As Boolean
    Dim error_cnt As Integer
    Dim strErrorMessage As String
    Dim DfltBtldrInitMemReadMsg As clsCANMessage
    Dim DfltBtldrMemReadDataAckMsg As clsCANMessage
    Dim blnResult As Boolean
    Dim canMsg As New TPCANMsg
    Dim lintTotalReadBytes As Integer
    Dim lintActualNumBytesLeft As Integer
    Dim lintNumBytesRead As Integer
    Dim intDataIndex As Integer
    Dim drAddress As DataRow
    Dim intAddress As UShort

    Try

      DfltBtldrInitMemReadMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderInitMemoryReadRequest)
      DfltBtldrMemReadDataAckMsg = CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderMemoryReadDataAck)
      blnResult = False
      error_cnt = 0
      NextStartAddress = 0
      lintTotalReadBytes = 0
      lintActualNumBytesLeft = 0
      lintNumBytesRead = 0

      'Clear record list
      mIntelHexFile.Clear()

      For Each drAddress In mdtAddressRanges.Rows
        Application.DoEvents()

        If ((drAddress.Item("ActuatorVariant") <> ActuatorVariantEnum.UNDEFINED.ToString) AndAlso (drAddress.Item("ActuatorVariant") <> mTargetActuator.ToString)) Then
          ' The address range's variant value doesn't match the actuator variant.
          Continue For
        End If

        StartAddress = Convert.ToInt32(drAddress.Item("StartAddress"), 16)
        NextStartAddress = StartAddress
        EndAddress = Convert.ToInt32(drAddress.Item("EndAddress"), 16)
        DataRecordLength = Convert.ToInt32(drAddress.Item("RecordLength"), 16)
        targetMemory = DirectCast([Enum].Parse(GetType(TargetMemoryAreaEnum), drAddress.Item("TargetMemoryArea")), Byte)

        Console.WriteLine("Processing StartAddress = " & Hex(StartAddress))

        For intAddress = StartAddress To EndAddress Step CUInt(DataRecordLength)
          Do
            read_error = False

            'Start a New Data Record
            UploadRecord = New IntelDataRecord
            UploadRecord.RecordType = IntelDataRecord.HEXRecordType.DataRecord

            'Start Address
            UploadRecord.StartAddress = NextStartAddress

            'Calculate End Address
            If EndAddress <= UploadRecord.StartAddress + DataRecordLength - 1 Then
              UploadRecord.EndAddress = EndAddress
            Else
              UploadRecord.EndAddress = CUShort(UploadRecord.StartAddress + DataRecordLength - 1)
            End If

            'Calculate Next Start Address
            If (UploadRecord.EndAddress < &HFFFF) Then
              ' This is the max. address value. Don't try to increment value beyond 0xffff.
              NextStartAddress = CUShort(UploadRecord.EndAddress + 1)
            End If

            'Calculate Data Length
            UploadRecord.DataLength = CUShort(UploadRecord.EndAddress - UploadRecord.StartAddress + 1)
            ReDim UploadRecord.Data(UploadRecord.DataLength - 1)

            'Data For Initiate Memory Read Command
            DfltBtldrInitMemReadMsg.Data(1) = targetMemory
            DfltBtldrInitMemReadMsg.Data(2) = CByte(UploadRecord.StartAddress And (&HFF))            'Mask off Upper bytes
            DfltBtldrInitMemReadMsg.Data(3) = CByte((UploadRecord.StartAddress And (&HFF00)) / &H100) 'Mask off lower bytes
            DfltBtldrInitMemReadMsg.Data(4) = CByte(UploadRecord.EndAddress And (&HFF))              'Mask off Upper bytes
            DfltBtldrInitMemReadMsg.Data(5) = CByte((UploadRecord.EndAddress And (&HFF00)) / &H100)  'Mask off lower bytes

            ' Send Initiate Memory Read Command
            mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrInitMemReadMsg)

            'Wait for Initiate Memory Read Acknowledge
            blnResult = GetMemoryReadInitAck(canMsg)
            'If mAnomaly IsNot Nothing Then
            '  Console.WriteLine("Error After GetMemoryReadInitAck: " & mAnomaly.AnomalyExceptionMessage & ", " & mAnomaly.AnomalyMessageText)
            'End If

            If (blnResult = True) Then 'Actuator Acknowledged Initiate Memory Read and Returned the Number Of Bytes to Read
              lintTotalReadBytes = (CInt(canMsg.DATA(2)) << 8) + canMsg.DATA(1)

              'Check that requested bytes = actual bytes
              If UploadRecord.DataLength = lintTotalReadBytes Then
                'Prepare Data Array for Number of Bytes To Read
                ReDim UploadRecord.Data(lintTotalReadBytes - 1)

                'Initialize Number Of Bytes That Have Been Read
                lintNumBytesRead = 0
                Do 'Begin Memory Data Read

                  'Wait for Memory Read Data Message from Actuator
                  blnResult = GetMemoryReadData(canMsg)
                  'If mAnomaly IsNot Nothing Then
                  '  Console.WriteLine("Error After GetMemoryReadData: " & mAnomaly.AnomalyExceptionMessage & ", " & mAnomaly.AnomalyMessageText)
                  'End If

                  If (blnResult = True) Then 'Memory Was Read Successfully
                    'Save Data to Array
                    For intDataIndex = lintNumBytesRead + 1 To ((canMsg.LEN - 1) + lintNumBytesRead)
                      'Console.WriteLine(Hex(StartAddress) & " To " & Hex(EndAddress) & " (" & DataRecordLength & " Bytes)" & "-" & intDataIndex)
                      UploadRecord.Data(intDataIndex - 1) = canMsg.DATA(intDataIndex - lintNumBytesRead)
                    Next

                    'Update Number of Bytes That Have Been Read and Number of Bytes Left to Read
                    lintNumBytesRead = lintNumBytesRead + canMsg.LEN - 1
                    lintActualNumBytesLeft = UploadRecord.DataLength - lintNumBytesRead

                    'Data For Memory Read Data Acknowledge
                    DfltBtldrMemReadDataAckMsg.Data(1) = CByte(lintActualNumBytesLeft And &HFF)
                    DfltBtldrMemReadDataAckMsg.Data(2) = CByte(lintActualNumBytesLeft >> 8)

                    'Send Memory Read Data Acknowledge, The actuator will continue sending data until all desired bytes have been sent and acknowledged
                    mCanMsgHandler.LoadCANMsgOutputQueue(DfltBtldrMemReadDataAckMsg)
                  Else
                    ' The actuator returned an error in response to an memory read command..
                    read_error = True
                    Console.WriteLine("Read Error After GetMemoryReadData Before CloseSession. EEProm Adx = " & intAddress)
                    If (CloseSession() = False) Then
                      ' Failed to close DBL programming session prior to re-read attempt.
                      strErrorMessage = "Failed to Close Default Bootloader Session for Memory Read Retry After GetMemoryReadData"
                      If mAnomaly IsNot Nothing Then
                        strErrorMessage = strErrorMessage & " " & mAnomaly.AnomalyExceptionMessage & mAnomaly.AnomalyMessageText
                      End If
                      Console.WriteLine(strErrorMessage)
                      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
                      Throw New TsopAnomalyException
                    End If
                    ' DBL programming session closed, so try again.
                    Exit Do
                  End If
                Loop While (lintActualNumBytesLeft > 0)
              Else
                ' Reported number of bytes doesn't match the number of bytes available in the record's address range.
                read_error = True
                Console.WriteLine("Read Error After Compare Data Length to Read Bytes Before CloseSession. EEProm Adx = " & intAddress)
                If (CloseSession() = False) Then
                  ' Failed to close DBL programming session prior to re-read attempt.
                  strErrorMessage = "Failed to Close Default Bootloader Session for Memory Read Retry After GetMemoryReadInitAck Before CloseSession"
                  If mAnomaly IsNot Nothing Then
                    strErrorMessage = strErrorMessage & " " & mAnomaly.AnomalyExceptionMessage & mAnomaly.AnomalyMessageText
                  End If
                  Console.WriteLine(strErrorMessage)
                  mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
                  Throw New TsopAnomalyException
                End If
                ' DBL programming session closed, so try again.
              End If
            Else
              ' The actuator returned an error in response to an init memory read command..
              read_error = True
              Console.WriteLine("Read Error After GetMemoryReadInitAck Before CloseSession. EEProm Adx = " & intAddress)
              If (CloseSession() = False) Then
                strErrorMessage = "Failed to Close Default Bootloader Session for Memory Read Retry After GetMemoryReadInitAck Before CloseSession"
                If mAnomaly IsNot Nothing Then
                  strErrorMessage = strErrorMessage & " " & mAnomaly.AnomalyExceptionMessage & mAnomaly.AnomalyMessageText
                End If
                ' Failed to close DBL programming session prior to re-read attempt.
                Console.WriteLine(strErrorMessage)
                mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
                Throw New TsopAnomalyException
              End If
              ' DBL programming session closed, so try again.    
            End If
            If (read_error = True) Then
              error_cnt += 1
              ' Re-initialize variables for read retry.
              'StartAddress = Convert.ToInt32(drAddress.Item("StartAddress"), 16)
              'NextStartAddress = StartAddress
              NextStartAddress = intAddress
              'EndAddress = Convert.ToInt32(drAddress.Item("EndAddress"), 16)
              'DataRecordLength = Convert.ToInt32(drAddress.Item("RecordLength"), 16)
              'targetMemory = DirectCast([Enum].Parse(GetType(TargetMemoryAreaEnum), drAddress.Item("TargetMemoryArea")), Byte)
            End If
          Loop While ((read_error = True) AndAlso (error_cnt < 10))
          If (read_error = False) Then
            ' Successful read.
            mIntelHexFile.Add(UploadRecord)
            Console.WriteLine("Adding Record StartAddress = " & Hex(UploadRecord.StartAddress))
          ElseIf (error_cnt >= 10) Then
            ' Too many communications related failures occurred.
            strErrorMessage = "Too Many Communication Related Failures Occured Reading EEProm"
            Console.WriteLine(strErrorMessage)
            mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
            Throw New TsopAnomalyException
          End If
        Next 'Record
      Next

      'Add EOF Record
      UploadRecord = New IntelDataRecord
      UploadRecord.RecordType = IntelDataRecord.HEXRecordType.EOFRecord
      UploadRecord.StartAddress = 0
      UploadRecord.EndAddress = 0
      UploadRecord.DataLength = 0
      mIntelHexFile.Add(UploadRecord)

      blnReturnValue = True

      Return blnReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadFaultCount(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadFaultCount)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        returnString = 250 - CInt(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Fault Count"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


  Public Function ReadHardwarePartNumberHighBytes(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadHardwarePartNumberHighBytes)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnASCIIString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Hardware Part Number High Bytes"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadHardwarePartNumberLowBytes(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadHardwarePartNumberLowBytes)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnASCIIString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Hardware Part Number Low Bytes"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadInternalTemperature(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadInternalTemperature)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Internal Temperature"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadProgramChecksum(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadProgramChecksum)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)
      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnHexString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Program Checksum"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadProgramChecksumInteger(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadProgramChecksumInteger)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)
      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Program Checksum"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadSerialNumber(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadSerialNumber)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnHexString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Serial Number"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadSpecialMemoryLine1(ByRef UpperByte As String, ByRef LowerByte As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim strResult As String

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadSpecialMemoryLine1)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        strResult = ReturnDotDelimitedString(CanMessage, returnData)
        LowerByte = (strResult.Split(".")(0)).Trim
        UpperByte = (strResult.Split(".")(1)).Trim
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Special Memory Line 1"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadSpecialMemoryLine2(ByRef UpperByte As String, ByRef LowerByte As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim strResult As String

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadSpecialMemoryLine2)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        strResult = ReturnDotDelimitedString(CanMessage, returnData)
        LowerByte = (strResult.Split(".")(0)).Trim
        UpperByte = (strResult.Split(".")(1)).Trim
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Special Memory Line 2"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadPowerCycleCounts(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadPowerCycleCounts)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Power Cycle Counts"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadRunTime(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadRunTime)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Total Run Time"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadSpan(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadSpan)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Span"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadTempHWM(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim intReturnValue As Integer

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadTempHWM)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        'returnString = ReturnContiguousByteString(CanMessage, returnData)
        intReturnValue = returnData(2) + returnData(3) * 8
        returnString = intReturnValue
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Temp HWM"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ReadVendorPCB(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ReadVendorPCB)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Reading Vendor PCB"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Sub ReleaseCAN()
    Try

      Call mCanMsgHandler.ReleaseCAN()
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Public Function ResetActuator() As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try
      blnreturnValue = False
      strErrorMessage = ""

      ' Register for Address Claim Messages.
      mCanMsgHandler.RcvMsgDelegates.AddAddrClaimDelegate(AddressOf AddressClaimRcvd)

      ' Clear previously received Address Claim messages.
      SyncLock mAddrClaimLock
        mAddressClaimMsgs.Clear()
      End SyncLock

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetActuator)

      'Send Message and Get Response
      blnreturnValue = SendMessageNoResponse(CanMessage)

      If blnreturnValue = False Then
        strErrorMessage = "Error Resetting Actuator"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      blnreturnValue = False
      'Look for Address Claim Message to verify Actuator was reset
      ' Initialize the elapsed time starting value
      QueryPerformanceCounter(HP_CounterValue)
      dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
      ' Wait up to one second.
      Do
        If mAddressClaimMsgs.Count > 0 Then
          blnreturnValue = True
          Exit Do
        End If
        ' Retrieve the current counter value.
        QueryPerformanceCounter(HP_CounterValue)
        ' Calculate the current time
        dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
      Loop While (dblCurrentTime - dblStartTime < 1000)

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
      ' UnRegister for Address Claim Messages.
      mCanMsgHandler.RcvMsgDelegates.RemoveAddrClaimDelegate(AddressOf AddressClaimRcvd)
    End Try
  End Function


  Public Function ResetEEPromAll(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetEEPromAll)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        'Console.WriteLine(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Resetting EEProm All"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ResetEEPromPowerCycles(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try

      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetEEPromPowerCycles)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        'Console.WriteLine(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Resetting EEProm Power Cycle Counts"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ResetEEPromRunTime(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try

      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetEEPromRunTime)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        'Console.WriteLine(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Resetting EEProm Total Run Time"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ResetEEPromSpan(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try

      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetEEPromSpan)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        'Console.WriteLine(returnString)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Resetting EEProm Span"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ResetSpecialMemoryLine1(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetSpecialMemoryLine1)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Resetting Special Memory Line1"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function ResetSpecialMemoryLine2(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.ResetSpecialMemoryLine2)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Resetting Special Memory Line2"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function RecordtoStringWithChecksum(ByVal Record As IntelDataRecord) As String
    Dim calcCC As UInteger
    Dim RecordStr As String = ""

    Try
      ' Calculate checksum.
      calcCC = 0
      calcCC = calcCC + Record.DataLength
      calcCC = CUInt(calcCC + Record.StartAddress And (&HFF)) 'Mask off Upper bytes
      calcCC = CUInt(calcCC + (Record.StartAddress And (&HFF00)) / &H100) 'Mask off lower bytes
      calcCC = CUInt(calcCC + Record.RecordType)
      For i As Integer = 0 To Record.Data.Length - 1
        calcCC = calcCC + Record.Data(i)
      Next
      calcCC = CUInt(calcCC Mod 256)
      If calcCC <> 0 Then
        calcCC = CUInt(&H100 - calcCC)
      End If
      Record.CheckSum = CUShort(calcCC)
      RecordStr = RecordToString(Record)
      Return RecordStr

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function


  Public Sub ReStartCAN()
    Try

      Call mCanMsgHandler.ReleaseCAN()
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      Call mCanMsgHandler.StartCAN(Me.mstrCANInterfaceName)
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Public Function JumpToDefaultBootloader(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.JumpToDefaultBootloader)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error in Jump To Default Bootloader"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


  Public Sub ShutDown()
    Try

      mCanMsgHandler.RcvMsgDelegates.RemoveAllToolMsgDelegates(AddressOf ToolMsgRespRcvd)
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Console.WriteLine("Error After RemoveAllToolMsgDelegates " & mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " & mCanMsgHandler.Anomaly.AnomalyMessageText)
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      mCanMsgHandler.RcvMsgDelegates.RemoveAllStatusMsgDelegates(AddressOf StatusMsgRcvd)
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Console.WriteLine("Error After RemoveAllStatusMsgDelegates " & mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " & mCanMsgHandler.Anomaly.AnomalyMessageText)
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      mCanMsgHandler.RcvMsgDelegates.RemoveAllAddrClaimDelegates(AddressOf AddressClaimRcvd)
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Console.WriteLine("Error After RemoveAllAddrClaimDelegates " & mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " & mCanMsgHandler.Anomaly.AnomalyMessageText)
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

      Call StopActuatorDownloadThread()
      If mAnomaly IsNot Nothing Then
        Console.WriteLine("Error After StopActuatorDownloadThread " & mAnomaly.AnomalyExceptionMessage & " " & mAnomaly.AnomalyMessageText)
        Throw New TsopAnomalyException
      End If

      Call mCanMsgHandler.ShutDown()
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Console.WriteLine("Error After mCanMsgHandler.ShutDown " & mCanMsgHandler.Anomaly.AnomalyExceptionMessage & " " & mCanMsgHandler.Anomaly.AnomalyMessageText)
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  'Public Function StartApplication() As Boolean
  '  Dim blnReturnValue As Boolean
  '  Dim strErrorMessage As String
  '  Try
  '    blnReturnValue = False

  '    'Clear status messages
  '    SyncLock mStatusMsgLock
  '      mStatusMsgs.Clear()
  '    End SyncLock

  '    'Register status message callback delegate
  '    mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)
  '    Delay(100)

  '    ' Command application start.
  '    mCanMsgHandler.LoadCANMsgOutputQueue(CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderStartApplication))

  '    ' Wait 500 msec while the application starts.
  '    Delay(500)

  '    If (mStatusMsgs.Count > 0) Then
  '      ' The actuator application has started.
  '      blnReturnValue = True
  '      mblnApplicationCodeRunning = True
  '      mblnDefaultBootloaderCodeRunning = False
  '    Else
  '      ' Clear previously received status messages.
  '      SyncLock mStatusMsgLock
  '        mStatusMsgs.Clear()
  '      End SyncLock
  '      ' Command application start.
  '      mCanMsgHandler.LoadCANMsgOutputQueue(CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderStartApplication))
  '      ' Wait 500 msec while the application starts.
  '      Delay(500)
  '      If (mStatusMsgs.Count > 0) Then
  '        ' The actuator application has started.
  '        blnReturnValue = True
  '        mblnApplicationCodeRunning = True
  '        mblnDefaultBootloaderCodeRunning = False
  '      Else
  '        strErrorMessage = "Application Restart Failed"
  '      End If
  '    End If

  '    Return blnReturnValue

  '  Catch ex As TsopAnomalyException
  '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
  '    Return False
  '  Catch ex As Exception
  '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
  '    Return False
  '  Finally
  '    ' Unregister for status messages.
  '    mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
  '  End Try
  'End Function

  Public Function StartApplication() As Boolean
    Dim blnReturnValue As Boolean
    Dim strErrorMessage As String
    Dim intLoopCount As Integer
    Dim dblStartTime As Double
    Dim dblCurrentTime As Double

    Try
      blnReturnValue = False

      'Clear status messages
      SyncLock mStatusMsgLock
        mStatusMsgs.Clear()
      End SyncLock
      Delay(100)

      'Register status message callback delegate
      mCanMsgHandler.RcvMsgDelegates.AddStatusMsgDelegate(AddressOf StatusMsgRcvd)

      intLoopCount = 0
      Do
        ' Command application start.
        mCanMsgHandler.LoadCANMsgOutputQueue(CANMessages(clsCANMessage.MessageFunctions.DefaultBootloaderStartApplication))

        ' Initialize the elapsed time starting value
        QueryPerformanceCounter(HP_CounterValue)
        dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
        Do
          Thread.Sleep(1)
          If (mStatusMsgs.Count > 0) Then
            Exit Do
          End If
          ' Retrieve the current counter value.
          QueryPerformanceCounter(HP_CounterValue)
          ' Calculate the current time
          dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
        Loop While (dblCurrentTime - dblStartTime < 2500.0)

        intLoopCount += 1

      Loop While ((mStatusMsgs.Count = 0) AndAlso (intLoopCount < 2))

      If (mStatusMsgs.Count > 0) Then
        ' The actuator application has started.
        blnReturnValue = True
        mblnApplicationCodeRunning = True
        mblnDefaultBootloaderCodeRunning = False
      Else
        strErrorMessage = "Application Restart Failed"
      End If

      Return blnReturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
      ' Unregister for status messages.
      mCanMsgHandler.RcvMsgDelegates.RemoveStatusMsgDelegate(AddressOf StatusMsgRcvd)
    End Try
  End Function


  Public Sub StartCAN()
    Try

      Call mCanMsgHandler.StartCAN(Me.mstrCANInterfaceName)
      If mCanMsgHandler.Anomaly IsNot Nothing Then
        Me.Anomaly = mCanMsgHandler.Anomaly
        mCanMsgHandler.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub


  Public Function TestGetSerialNumberMultipleTrials() As String
    Try

      Dim intLoop As Integer
      Dim strSerialNumber As String
      Console.WriteLine("New Test")

      strSerialNumber = ""
      For intTrials As Integer = 1 To 100
        intLoop = 0
        Do
          intLoop += 1
          strSerialNumber = ""
          Call ReadSerialNumber(strSerialNumber)
          Me.mStopwatch.DelayTime(100)
        Loop Until strSerialNumber <> ""
        Console.WriteLine(intLoop)
        Me.mStopwatch.DelayTime(10)
      Next

      Return strSerialNumber

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    End Try
  End Function

  Public Sub TestReadEEPromMultipleTrials()
    Dim blnDone As Boolean = False
    Dim intTrialsRun As Integer
    Dim blnErrorFound As Boolean
    Dim intRecord As Integer
    'Dim intDataByte As Integer
    Dim CurrentintelHexFile As New ArrayList
    Dim PreviousintelHexFile As New ArrayList

    Try
      intTrialsRun = 0
      blnErrorFound = False

      Call ReadEEPromIntoIntelHexFormat()

      For intRecord = 0 To mIntelHexFile.Count - 1
        CurrentintelHexFile.Add(RecordtoStringWithChecksum(mIntelHexFile(intRecord)))
        PreviousintelHexFile.Add(CurrentintelHexFile(intRecord))
      Next

      Do
        intTrialsRun += 1

        Call ReadEEPromIntoIntelHexFormat()

        CurrentintelHexFile.Clear()

        For intRecord = 0 To mIntelHexFile.Count - 1
          CurrentintelHexFile.Add(RecordtoStringWithChecksum(mIntelHexFile(intRecord)))
          'Console.WriteLine("PreviousRecord" & PreviousintelHexFile(intRecord))
          'Console.WriteLine("Current_Record" & CurrentintelHexFile(intRecord))
          If CurrentintelHexFile(intRecord) <> PreviousintelHexFile(intRecord) Then
            blnErrorFound = True
            Exit Do
          End If
        Next
        Console.WriteLine("Trial " & intTrialsRun)


        'Save hex file to previous
        PreviousintelHexFile.Clear()
        For intRecord = 0 To mIntelHexFile.Count - 1
          PreviousintelHexFile.Add(CurrentintelHexFile(intRecord))
        Next

      Loop Until blnDone Or blnErrorFound

      Stop


    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub


  'Public Function WriteIntelHexFile(ByRef IntelHexFile As List(Of IntelDataRecord), ByVal directoryPath As String) As Boolean
  Public Function WriteIntelHexFile() As Boolean
    Dim returnValue As Boolean = False
    Dim myRecord As IntelDataRecord
    Dim calcCC As UInteger
    Dim lblneof As Boolean = False
    Dim ReadData(7) As Byte
    Dim lblnResult As Boolean = False
    Dim UploadStreamWriter As IO.StreamWriter = Nothing
    Dim fileName As String

    Try
      fileName = mstrActuatorDataPath & "\" & Date.Today.Year.ToString & Date.Today.Month.ToString("D2") & _
                 Date.Today.Day.ToString("D2") & Date.Now.ToString("HHmmss") & "_" & mstrSerialNumber & ".txt"
      UploadStreamWriter = New IO.StreamWriter(fileName, False)
      If (UploadStreamWriter IsNot Nothing) Then
        For Each myRecord In mIntelHexFile
          ' Calculate checksum.
          calcCC = 0
          calcCC = calcCC + myRecord.DataLength
          calcCC = CUInt(calcCC + myRecord.StartAddress And (&HFF)) 'Mask off Upper bytes
          calcCC = CUInt(calcCC + (myRecord.StartAddress And (&HFF00)) / &H100) 'Mask off lower bytes
          calcCC = CUInt(calcCC + myRecord.RecordType)
          If myRecord.DataLength <> 0 Then
            For i As Integer = 0 To myRecord.Data.Length - 1
              calcCC = calcCC + myRecord.Data(i)
            Next
          End If
          calcCC = CUInt(calcCC Mod 256)
          If calcCC <> 0 Then
            calcCC = CUInt(&H100 - calcCC)
          End If
          myRecord.CheckSum = CUShort(calcCC)
          'setup string to write to file and write it to file
          UploadStreamWriter.WriteLine(RecordToString(myRecord))
        Next
        UploadStreamWriter.Flush()
        returnValue = True
      End If
      Return returnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally
      If (UploadStreamWriter IsNot Nothing) Then
        UploadStreamWriter.Close()
      End If
    End Try
  End Function

  Public Function WriteOverrideCommand(ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte

    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.WriteOverrideCommand)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnIntegerValueString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Writing Override Command"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


  Public Function WriteSpecialMemoryLine1(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim intDynamicWriteByteIndex As Integer
    Dim intDataIndex As Integer
    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.WriteSpecialMemoryLine1)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      intDynamicWriteByteIndex = 0
      For intDataIndex = 0 To CanMessage.StaticData.Length - 1
        If CanMessage.StaticData(intDataIndex).ToUpper = "XX" Then
          CanMessage.Data(intDataIndex) = dynamicData(intDynamicWriteByteIndex)
          intDynamicWriteByteIndex += 1
        End If
      Next

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Writing Special Memory Line1"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function WriteSpecialMemoryLine2(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim intDynamicWriteByteIndex As Integer
    Dim intDataIndex As Integer
    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.WriteSpecialMemoryLine2)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      intDynamicWriteByteIndex = 0
      For intDataIndex = 0 To CanMessage.StaticData.Length - 1
        If CanMessage.StaticData(intDataIndex).ToUpper = "XX" Then
          CanMessage.Data(intDataIndex) = dynamicData(intDynamicWriteByteIndex)
          intDynamicWriteByteIndex += 1
        End If
      Next

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Writing Special Memory Line2"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  Public Function WriteStatusUpdateTimeOverride(ByVal dynamicData() As Integer, ByRef returnString As String) As Boolean
    Dim blnreturnValue As Boolean = False
    Dim CanMessage As clsCANMessage
    Dim strErrorMessage As String
    Dim returnData(0) As Byte
    Dim intDynamicWriteByteIndex As Integer
    Dim intDataIndex As Integer
    Try
      blnreturnValue = False
      strErrorMessage = ""

      'Find CAN Message in collection
      CanMessage = mCANMessages(clsCANMessage.MessageFunctions.WriteStatusUpdateTimeOverride)

      CanMessage.SendArbitrationID = mintToolMessagePriority + TOOL_MSG_PGN + &H0

      intDynamicWriteByteIndex = 0
      For intDataIndex = 0 To CanMessage.StaticData.Length - 1
        If CanMessage.StaticData(intDataIndex).ToUpper = "XX" Then
          CanMessage.Data(intDataIndex) = dynamicData(intDynamicWriteByteIndex)
          intDynamicWriteByteIndex += 1
        End If
      Next

      'Send Message and Get Response
      blnreturnValue = SendMessageGetResponse(CanMessage, returnData)

      'Parse the response.
      If blnreturnValue Then
        returnString = ReturnContiguousByteString(CanMessage, returnData)
        blnreturnValue = True
      End If

      If blnreturnValue = False Then
        strErrorMessage = "Error Writing Status Update Time Override"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return blnreturnValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function


#End Region

  '====================================================================================================
#Region "Class clsCANMessage"
  Public Class clsCANMessage

#Region "Private Member Variables =================================================="

    Private mintDataLength As Integer
    Private mintDecodingStartByte As Integer
    Private mintDecodingByteLength As Integer
    'Private mintDecodingMethod As DecodingMethods
    Private mbytData(7) As Byte
    'Private mintDynamicWriteByteCount As Integer

    'Private mintFrameRateMilliseconds As Integer
    'Private mintFieldsToReturn As Integer

    Private mintMessageFunction As MessageFunctions
    'Private mintMessageType As MessageTypes

    'Private mintNumberOfTimesToSendMessage As Integer

    Private mOpCode As UInteger

    'Private mintReceiveArbitrationIDMax As Integer
    'Private mintReceiveArbitrationIDMin As Integer
    'Private mintReturnFieldBytePositions() As Integer

    Private mblnReverseByteOrder As Boolean

    Private mintSendArbitrationID As Integer

    Private mstrStaticData(7) As String

#End Region

#Region "Public Constants, Structures, Enums =================================================="

    'Public Enum DecodingMethods As Integer
    '  None = 0
    '  ReturnHexString = 1
    '  ReturnASCIIStringDotDelimitEveryTwoBytes = 2
    '  ReturnCommaDelimitedString = 3
    '  ReturnASCIIString = 4
    '  ReturnContiguousByteString = 5
    'End Enum

    Public Enum MessageFunctions
      ReadCustomerBootloaderVersion = 0
      ReadDefaultBootloaderVersion = 1
      ReadHardwarePartNumberHighBytes = 2
      ReadHardwarePartNumberLowBytes = 3
      ReadProgramChecksum = 4
      ReadSerialNumber = 5
      ReadSpecialMemoryLine1 = 6
      ReadSpecialMemoryLine2 = 7
      ReadSoftwareVersion = 8
      ReadVendorPCB = 9
      ResetSpecialMemoryLine1 = 10
      ResetSpecialMemoryLine2 = 11
      WriteSpecialMemoryLine1 = 12
      WriteSpecialMemoryLine2 = 13
      JumpToDefaultBootloader = 14
      DefaultBootloaderAttnRequest = 15
      DefaultBootloaderAttnResponse = 16
      DefaultBootloaderInitMemoryReadRequest = 17
      DefaultBootloaderInitMemoryReadAck = 18
      DefaultBootloaderMemoryReadData = 19
      DefaultBootloaderMemoryReadDataAck = 20
      DefaultBootLoaderFaultResponse = 21
      DefaultBootloaderCloseSessionCmd = 22
      DefaultBootloaderCloseSessionAck = 23
      DefaultBootloaderStartApplication = 24
      ReadPowerCycleCounts = 25
      ReadRunTime = 26
      ResetEEPromAll = 27
      ResetEEPromPowerCycles = 28
      ResetEEPromRunTime = 29
      ReadSpan = 30
      ResetEEPromSpan = 31
      ReadFaultCount = 32
      ResetActuator = 33
      DisableZenCorrection = 34
      DisablePositionCommandTimeout = 35
      ReadTempHWM = 36
      ReadBurnInCycles = 37
      ReadBurnInTime = 38
      ReadAPSType = 39
      WriteStatusUpdateTimeOverride = 40
      APSCalibrate = 41
      ReadAPSCalibrateProgress = 42
      WriteOverrideCommand = 43
      ReadInternalTemperature = 44
      ReadBatteryVoltage = 45
      DefaultBootloaderInitMemoryWriteRequest = 46
      DefaultBootloaderInitMemoryWriteAck = 47
      DefaultBootloaderMemoryWriteData = 48
      DefaultBootloaderMemoryWriteDataAck = 49
      ReadProgramChecksumInteger = 50

    End Enum

    'Public Enum MessageTypes As Integer
    '  Tool = 0
    'End Enum

#End Region

#Region "Properties =================================================="

    Public Property DataLength As Integer
      Get
        Return mintDataLength
      End Get
      Set(value As Integer)
        mintDataLength = value
      End Set
    End Property

    Public Property DecodingByteLength As Integer
      Get
        Return Me.mintDecodingByteLength
      End Get
      Set(value As Integer)
        Me.mintDecodingByteLength = value
      End Set
    End Property

    Public Property DecodingStartByte As Integer
      Get
        Return Me.mintDecodingStartByte
      End Get
      Set(value As Integer)
        Me.mintDecodingStartByte = value
      End Set
    End Property

    'Public Property DecodingMethod As DecodingMethods
    '  Get
    '    Return Me.mintDecodingMethod
    '  End Get
    '  Set(value As DecodingMethods)
    '    Me.mintDecodingMethod = value
    '  End Set
    'End Property

    Public Property Data() As Byte()
      Get
        Return Me.mbytData
      End Get
      Set(value As Byte())
        Me.mbytData = value
      End Set
    End Property

    'Public Property DynamicWriteByteCount As Integer
    '  Get
    '    Return mintDynamicWriteByteCount
    '  End Get
    '  Set(value As Integer)
    '    mintDynamicWriteByteCount = value
    '  End Set
    'End Property


    'Public Property FrameRateMilliseconds As Integer
    '  Get
    '    Return mintFrameRateMilliseconds
    '  End Get
    '  Set(value As Integer)
    '    mintFrameRateMilliseconds = value
    '  End Set
    'End Property

    'Public Property FieldsToReturn As Integer
    '  Get
    '    Return mintFieldsToReturn
    '  End Get
    '  Set(value As Integer)
    '    mintFieldsToReturn = value
    '  End Set
    'End Property


    Public Property MessageFunction As MessageFunctions
      Get
        Return mintMessageFunction
      End Get
      Set(value As MessageFunctions)
        mintMessageFunction = value
      End Set
    End Property

    'Public Property MessageType As MessageTypes
    '  Get
    '    Return mintMessageType
    '  End Get
    '  Set(value As MessageTypes)
    '    mintMessageType = value
    '  End Set
    'End Property


    'Public Property NumberOfTimesToSendMessage As Integer
    '  Get
    '    Return mintNumberOfTimesToSendMessage
    '  End Get
    '  Set(value As Integer)
    '    mintNumberOfTimesToSendMessage = value
    '  End Set
    'End Property


    Public Property OpCode As UInteger
      Get
        Return mOpCode
      End Get
      Set(value As UInteger)
        mOpCode = value
      End Set
    End Property


    'Public Property ReceiveArbitrationIDMax As Integer
    '  Get
    '    Return mintReceiveArbitrationIDMax
    '  End Get
    '  Set(value As Integer)
    '    mintReceiveArbitrationIDMax = value
    '  End Set
    'End Property

    'Public Property ReceiveArbitrationIDMin As Integer
    '  Get
    '    Return mintReceiveArbitrationIDMin
    '  End Get
    '  Set(value As Integer)
    '    mintReceiveArbitrationIDMin = value
    '  End Set
    'End Property

    'Public Property ReturnFieldBytePositions() As Integer()
    '  Get
    '    Return Me.mintReturnFieldBytePositions
    '  End Get
    '  Set(value As Integer())
    '    Me.mintReturnFieldBytePositions = value
    '  End Set
    'End Property

    Public Property ReverseByteOrder As Boolean
      Get
        Return mblnReverseByteOrder
      End Get
      Set(value As Boolean)
        mblnReverseByteOrder = value
      End Set
    End Property


    Public Property SendArbitrationID As Integer
      Get
        Return mintSendArbitrationID
      End Get
      Set(value As Integer)
        mintSendArbitrationID = value
      End Set
    End Property

    Public Property StaticData() As String()
      Get
        Return Me.mstrStaticData
      End Get
      Set(value As String())
        Me.mstrStaticData = value
      End Set
    End Property



#End Region

#Region "Public Methods =================================================="

#End Region

#Region "Private Methods =================================================="

#End Region

  End Class
#End Region

  '====================================================================================================
#Region "Class clsCanMsgHandling"
  Public Class clsCanMsgHandling

#Region "Constructor"
    Protected Sub New()
      Dim strErrorMessage As String
      Dim CounterFreq As Long
      Try

        ' Get the frequency of the Performance Counter
        QueryPerformanceFrequency(CounterFreq)
        ' Store the period of the Performance Counter * 1000
        CounterPeriod = (1.0 / CDbl(CounterFreq)) * 1000

        ' Get an instance of the receive CAN message delegate class.
        mRcvMsgDelegates = clsCANActuator.clsRcvCanMsgDelegates.GetInstance()
        If mRcvMsgDelegates Is Nothing Then
          strErrorMessage = "Error Creating Instance of clsRcvCanMsgDelegates Class"
          mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
          Throw New TsopAnomalyException
        End If

        CreateThreads()
        If mAnomaly IsNot Nothing Then
          Throw New TsopAnomalyException
        End If

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub
#End Region

#Region "Private Member Variables"
    Private mAnomaly As clsAnomaly
    Private mStopwatch As New clsStopWatch
    Private mStackTrace As New clsStackTrace

    ' Private single instance of this class.
    Private Shared _CanMsgHandlingInstance As clsCanMsgHandling

    Private mstrCANMessageLogFilePath As String

    Private mblnCanMessageLoggingEnabled As Boolean
    Private mblnCanActuatorActivityLoggingEnabled As Boolean

    ' Receive CAN message queued event wait handle
    Private mRcvCanMsgDeliveryWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
    ' Receive CAN message delivery thread
    Private mRcvCanMsgDeliveryThread As Thread
    ' Transmit CAN message queued event wait handle
    Private mXmtCanMsgWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
    ' Transmit CAN message thread
    Private mXmtCanMessageThread As Thread
    ' Receive CAN message delivery thread
    Private mQueueRcvdCanMsgThread As Thread
    ' Received CAN message queue
    Private mRcvdCanMessageQueue As New System.Collections.Concurrent.ConcurrentQueue(Of CAN_Msg_Struct)
    ' CAN message output queue.
    Private mCanMsgOutputQueue As New System.Collections.Concurrent.ConcurrentQueue(Of clsCANActuator.clsCANMessage) 'TER

    'CAN Logging Output Queue
    Private mCanMsgLogQueue As New System.Collections.Concurrent.ConcurrentQueue(Of String) 'TER
    ' CAN Logging event wait handle
    Private mCanLogWaitHandle As New EventWaitHandle(False, EventResetMode.AutoReset)
    ' CAN Logging thread
    Private mCanLogThread As Thread
    Private mblnTerminateCanLogThread As Boolean = False
    Private mCanLogStartTime As Double



    ' Multicast delegates of delegate subs that have been registered to
    ' be called when corresponding messages are received and methods to access them.
    'Private Shared RcvdMsgDelegates As RcvCanMsgDelegates'TER
    Private mRcvMsgDelegates As clsCANActuator.clsRcvCanMsgDelegates
    ' True when CAN and message handling threads have been started.
    Private mblnCanStarted As Boolean = False
    ' NiCAN attributes
    Private mintObjectHandle As Integer
    Private mintCurrentState As Integer
    ' Flag used to terminate thread procs.
    Private mblnTerminateRcvCanMsgDeliverThread As Boolean = False
    Private mblnTerminateXmtCanMsgThread As Boolean = False
    Private mblnTerminateQueueRcvdCanMsgThread As Boolean = False
    Private mNonDecimal As New Regex("[^0-9]+")

    ' Elapsed Time Counter (high resolution)
    Private Declare Function QueryPerformanceCounter Lib "Kernel32.dll" (ByRef lpPerformanceCount As Long) As Boolean
    Private Declare Function QueryPerformanceFrequency Lib "Kernel32.dll" (ByRef lpFrequency As Long) As Boolean
    Private HP_CounterValue As Long
    'Private StartingTime As Double
    'Private CurrentTime As Double
    Private CounterPeriod As Double


#End Region

#Region "Public Structures and Enums"
    Public Structure CAN_Msg_Struct

      ' Contains received CAN message, corresponding time-stamp
      ' and corresponding properties.
      Private _CanMsg As TPCANMsg
      Private _Timestamp As nican.NCTYPE_UINT64
      Private _msgCount As UShort

      Public Sub New(ByVal msg As TPCANMsg, ByVal timestamp As nican.NCTYPE_UINT64)
        _CanMsg = msg
        _Timestamp = timestamp
        _msgCount = 0
      End Sub

      Public ReadOnly Property CanMsg() As TPCANMsg
        Get
          Return _CanMsg
        End Get
      End Property

      Public ReadOnly Property Timestamp() As nican.NCTYPE_UINT64
        Get
          Return _Timestamp
        End Get
      End Property

      Public Property msgCount() As UShort
        Get
          Return _msgCount
        End Get
        Set(ByVal value As UShort)
          _msgCount = value
        End Set
      End Property
    End Structure

#End Region

#Region "Properties"
    Public Property Anomaly() As clsAnomaly
      Get
        Return mAnomaly
      End Get
      Set(ByVal value As clsAnomaly)
        mAnomaly = value
      End Set
    End Property

    Public Property CanActuatorActivityLoggingEnabled As Boolean
      Get
        Return mblnCanActuatorActivityLoggingEnabled
      End Get
      Set(value As Boolean)
        mblnCanActuatorActivityLoggingEnabled = value
      End Set
    End Property

    Public Property CANMessageLogFilePath As String
      Get
        Return mstrCANMessageLogFilePath
      End Get
      Set(value As String)
        mstrCANMessageLogFilePath = value
      End Set
    End Property

    Public Property CanMessageLoggingEnabled As Boolean
      Get
        Return mblnCanMessageLoggingEnabled
      End Get
      Set(value As Boolean)
        mblnCanMessageLoggingEnabled = value
      End Set
    End Property

    Public ReadOnly Property ObjectHandle() As Integer
      Get
        Return mintObjectHandle
      End Get
    End Property

    Public ReadOnly Property RcvMsgDelegates As clsCANActuator.clsRcvCanMsgDelegates
      Get
        Return mRcvMsgDelegates
      End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub Delay(ByVal time As UInteger)
      Dim dblStartTime As Double
      Dim dblCurrentTime As Double

      Try
        ' Initialize the elapsed time starting value.
        QueryPerformanceCounter(HP_CounterValue)
        dblStartTime = CDbl(HP_CounterValue) * CounterPeriod
        Do
          ' Retrieve the current counter value.
          QueryPerformanceCounter(HP_CounterValue)
          ' Calculate the current time
          dblCurrentTime = CDbl(HP_CounterValue) * CounterPeriod
        Loop While (dblCurrentTime - dblStartTime < time)

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Shared Function GetInstance() As clsCanMsgHandling 'TER
      Try
        ' Only one instance of this class (singleton) is allowed.
        ' Initialize single instance of this class if it hasn't already been done.
        If (_CanMsgHandlingInstance Is Nothing) Then
          _CanMsgHandlingInstance = New clsCanMsgHandling
          If _CanMsgHandlingInstance.Anomaly IsNot Nothing Then
            _CanMsgHandlingInstance = Nothing
            Throw New TsopAnomalyException
          End If
        End If

        Return _CanMsgHandlingInstance

      Catch ex As Exception
        Return Nothing
      End Try
    End Function

    'Public Sub LogCanMessage(strMessage)
    '  Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing
    '  Dim strFileName As String
    '  Dim strDate As String
    '  Try
    '    If mblnCanMessageLoggingEnabled Then
    '      strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")

    '      strFileName = mstrCANMessageLogFilePath & "\CanMessageLog.txt"
    '      CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
    '      CanMessageLogStreamWriter.WriteLine(strDate & ", " & strMessage)
    '      CanMessageLogStreamWriter.Flush()
    '      CanMessageLogStreamWriter.Close()
    '    End If

    '  Catch ex As TsopAnomalyException
    '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    '  Catch ex As Exception
    '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    '  End Try
    'End Sub

    'Public Sub LogCanMessage()
    '  Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing
    '  Dim strFileName As String
    '  Dim strDate As String
    '  Dim strMessage As String = ""
    '  Dim blnDequeued As Boolean
    '  Try
    '    If mblnCanMessageLoggingEnabled Then
    '      strFileName = mstrCANMessageLogFilePath & "\CanMessageLog.txt"
    '      If mCanMsgLogQueue.Count <> 0 Then
    '        Do
    '          strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")
    '          blnDequeued = mCanMsgLogQueue.TryDequeue(strMessage)
    '          If blnDequeued Then
    '            CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
    '            CanMessageLogStreamWriter.WriteLine(strDate & ", " & strMessage)
    '          End If
    '        Loop While mCanMsgLogQueue.Count > 0
    '        CanMessageLogStreamWriter.Flush()
    '        CanMessageLogStreamWriter.Close()
    '      End If

    '    End If

    '  Catch ex As TsopAnomalyException
    '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    '  Catch ex As Exception
    '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    '  End Try
    'End Sub


    Public Sub LogCanActuatorActivity(strMessage)
      Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing
      Dim strFileName As String
      Dim strDate As String
      Try
        If mblnCanActuatorActivityLoggingEnabled Then

          strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")

          strFileName = mstrCANMessageLogFilePath & "\CanActuatorActivityLog.txt"
          CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
          CanMessageLogStreamWriter.WriteLine(strDate & ", " & strMessage)
          CanMessageLogStreamWriter.Flush()
          CanMessageLogStreamWriter.Close()
        End If

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub ReleaseCAN()

      Try
        If (mintObjectHandle <> 0) Then
          nican.ncCloseObject(mintObjectHandle)
        End If
        mblnCanStarted = False

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    'Friend Function StartCAN(ByRef finalRslt As FinalResult) As Boolean 'TER
    Public Function StartCAN(ByVal strCANInterfaceName) As Boolean

      Dim returnValue As Boolean = False
      Dim attr_list(0) As Integer '= {nican.NC_ATTR_BAUD_RATE}
      Dim attr_value_list(0) As Integer '= {nican.NC_BAUD_250K}
      Dim status As Integer
      Dim strErrorMessage As String

      Try

        mblnCanStarted = False

        Call GetCANConfigSettings(attr_list, attr_value_list)

        If ((strCANInterfaceName <> "") AndAlso (strCANInterfaceName.Length > 3) AndAlso _
            (strCANInterfaceName.StartsWith("CAN") = True) AndAlso _
            (mNonDecimal.IsMatch(strCANInterfaceName.Substring(strCANInterfaceName.IndexOf("N"c) + 1)) = False) AndAlso _
            (strCANInterfaceName.Substring(strCANInterfaceName.IndexOf("N"c) + 1) < 64)) Then
          ' Option the USB NiCAN device.
          status = nican.ncConfig(strCANInterfaceName, 1, attr_list(0), attr_value_list(0))
          'status = nican.ncConfig(strCANInterfaceName, 8, attr_list(0), attr_value_list(0))
          If (status = nican.NC_SUCCESS) Then
            status = nican.ncOpenObject(strCANInterfaceName, mintObjectHandle)
            If (status = nican.NC_SUCCESS) Then
              ' Reset the USB NiCAN device to clear errors.
              status = nican.ncAction(mintObjectHandle, nican.NC_OPCODE_ENUM.NC_OP_RESET, 0)
              If (status = nican.NC_SUCCESS) Then
                Delay(500) 'TER
                'mStopwatch.DelayTime(500)
                ' Start the USB NiCAN device.
                status = nican.ncAction(mintObjectHandle, nican.NC_OPCODE_ENUM.NC_OP_START, 0)
                If (status = nican.NC_SUCCESS) Then
                  ' The USB NiCAN device was started successfully.
                  ' Start reading and delivering received CAN messages.
                  If (mRcvCanMsgDeliveryThread.ThreadState = ThreadState.Unstarted) Then
                    mRcvCanMsgDeliveryThread.Start()
                    ' Start transmitting CAN messages.
                    If (mXmtCanMessageThread.ThreadState = ThreadState.Unstarted) Then
                      mXmtCanMessageThread.Start()
                      If (mQueueRcvdCanMsgThread.ThreadState = ThreadState.Unstarted) Then
                        mQueueRcvdCanMsgThread.Start()
                        mblnCanStarted = True
                        returnValue = True
                      End If
                    End If
                  End If
                Else
                  ' Failed to start the CAN device.
                  'finalRslt = FinalResult.CanDeviceError5
                  strErrorMessage = "Failed To Start The CAN Device"
                  mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
                  Throw New TsopAnomalyException
                End If
              Else
                ' Failed to reset the CAN device.
                'finalRslt = FinalResult.CanDeviceError4
                strErrorMessage = "Failed To Reset The CAN Device"
                mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
                Throw New TsopAnomalyException
              End If
            Else
              ' Failed to open the CAN device.
              'finalRslt = FinalResult.CanDeviceError3
              strErrorMessage = "Failed To Open The CAN Device"
              mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
              Throw New TsopAnomalyException
            End If
          Else
            ' Failed to set the CAN device baud rate.
            'finalRslt = FinalResult.CanDeviceError2
            strErrorMessage = "Failed To Configure CAN Device"
            mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
            Throw New TsopAnomalyException
          End If
        Else
          ' CAN device object name is invalid.
          'finalRslt = FinalResult.CanDeviceError1
          strErrorMessage = "CAN Device Name Is Invalid"
          mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
          Throw New TsopAnomalyException
        End If

        Return returnValue

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
        Return False
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
        Return False
      End Try
    End Function

    Public Sub ShutDown()
      Try
        ' If necessary, abort the CAN message threads.
        If ((mRcvCanMsgDeliveryThread IsNot Nothing) AndAlso _
            (mRcvCanMsgDeliveryThread.ThreadState <> ThreadState.AbortRequested) AndAlso _
            (mRcvCanMsgDeliveryThread.ThreadState <> ThreadState.Aborted)) Then

          mblnTerminateRcvCanMsgDeliverThread = True
          mRcvCanMsgDeliveryWaitHandle.Set()

          If mRcvCanMsgDeliveryThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
            While (mRcvCanMsgDeliveryThread.ThreadState <> System.Threading.ThreadState.Stopped)
              Application.DoEvents()
              Thread.Sleep(1)
            End While
          End If
          'RcvCanMsgDeliveryThread.Abort()
          mRcvCanMsgDeliveryThread = Nothing
        End If

        If ((mXmtCanMessageThread IsNot Nothing) AndAlso _
            (mXmtCanMessageThread.ThreadState <> ThreadState.AbortRequested) AndAlso _
            (mXmtCanMessageThread.ThreadState <> ThreadState.Aborted)) Then

          mblnTerminateXmtCanMsgThread = True
          mXmtCanMsgWaitHandle.Set()

          If mXmtCanMessageThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
            While (mXmtCanMessageThread.ThreadState <> System.Threading.ThreadState.Stopped)
              Application.DoEvents()
              Thread.Sleep(1)
            End While
          End If
          'XmtCanMessageThread.Abort()
          mXmtCanMessageThread = Nothing
        End If

        'If ((mQueueRcvdCanMsgThread IsNot Nothing) AndAlso (mQueueRcvdCanMsgThread.IsAlive = True)) Then
        If ((mQueueRcvdCanMsgThread IsNot Nothing)) Then

          mblnTerminateQueueRcvdCanMsgThread = True

          'While (QueueRcvdCanMsgThread.ThreadState <> ThreadState.Stopped)
          'End While
          If mQueueRcvdCanMsgThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
            While (mQueueRcvdCanMsgThread.ThreadState <> System.Threading.ThreadState.Stopped)
              Application.DoEvents()
              Thread.Sleep(1)
            End While
          End If
          'QueueRcvdCanMsgThread.Abort()
          mQueueRcvdCanMsgThread = Nothing
        End If

        ' Release the USB NiCAN device.
        ReleaseCAN()

        _CanMsgHandlingInstance = Nothing

        mblnTerminateRcvCanMsgDeliverThread = False
        mblnTerminateXmtCanMsgThread = False
        mblnTerminateQueueRcvdCanMsgThread = False

        Call StopCanLoggingThread()

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub StartCanLoggingThread()
      Try

        If (mCanLogThread.ThreadState = ThreadState.Unstarted) Then
          mCanLogThread.Start()
        End If

        '' Initialize the elapsed time starting value.
        'QueryPerformanceCounter(HP_CounterValue)
        'mCanLogStartTime = CDbl(HP_CounterValue) * CounterPeriod
        mCanLogStartTime = 0


      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub


    Public Sub StopCanLoggingThread()
      Try

        If ((mCanLogThread IsNot Nothing) AndAlso _
            (mCanLogThread.ThreadState <> ThreadState.AbortRequested) AndAlso _
            (mCanLogThread.ThreadState <> ThreadState.Aborted)) Then

          mblnTerminateCanLogThread = True
          mCanLogWaitHandle.Set()

          If mCanLogThread.ThreadState <> System.Threading.ThreadState.Unstarted Then
            While (mCanLogThread.ThreadState <> System.Threading.ThreadState.Stopped)
              Application.DoEvents()
              Thread.Sleep(1)
            End While
          End If
          'XmtCanMessageThread.Abort()
          mCanLogThread = Nothing
        End If

        mblnTerminateCanLogThread = False

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    'Friend Sub LoadCANMsgOutputQueue(ByVal CanMsg As TPCANMsg)
    Public Sub LoadCANMsgOutputQueue(ByVal CanMsg As clsCANActuator.clsCANMessage)
      ' Queue individual CAN messages

      Try
        ' Lock the queue while writing
        mCanMsgOutputQueue.Enqueue(CanMsg)
        ' Unblock the CAN transmit thread.
        mXmtCanMsgWaitHandle.Set()

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub LoadCANMsgLogQueue(ByVal strMessage As String)
      ' Queue individual CAN messages

      Try

        If CanMessageLoggingEnabled Then
          ' Lock the queue while writing
          mCanMsgLogQueue.Enqueue(strMessage)
          ' Unblock the CAN transmit thread.
          mCanLogWaitHandle.Set()
        End If

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub GetCANConfigSettings(ByRef attr_list() As Integer, ByRef attr_value_list() As Integer)
      Try

        ReDim attr_list(0)
        ReDim attr_value_list(0)

        attr_list(0) = nican.NC_ATTR_BAUD_RATE
        attr_value_list(0) = nican.NC_BAUD_250K
        'attr_list(1) = nican.NC_ATTR_START_ON_OPEN
        'attr_value_list(1) = nican.NC_FALSE
        'attr_list(2) = nican.NC_ATTR_READ_Q_LEN
        'attr_value_list(2) = 100
        'attr_list(3) = nican.NC_ATTR_WRITE_Q_LEN
        'attr_value_list(3) = 10
        'attr_list(4) = nican.NC_ATTR_CAN_COMP_STD
        'attr_value_list(4) = 0
        'attr_list(5) = nican.NC_ATTR_CAN_MASK_STD
        'attr_value_list(5) = nican.NC_CAN_MASK_STD_DONTCARE
        'attr_list(6) = nican.NC_ATTR_CAN_COMP_XTD
        'attr_value_list(6) = 0
        'attr_list(7) = nican.NC_ATTR_CAN_MASK_XTD
        'attr_value_list(7) = nican.NC_CAN_MASK_XTD_DONTCARE

        'These for future functionality using RTSI
        'attr_list(8) = nican.NC_ATTR_RTSI_MODE
        'attr_value_list(8) = nican.NC_RTSI_OUT_ON_TX
        'attr_list(9) = nican.NC_ATTR_RTSI_SIG_BEHAV
        'attr_value_list(9) = nican.NC_RTSISIG_PULSE
        'attr_list(10) = nican.NC_ATTR_RTSI_SIGNAL
        'attr_value_list(10) = nican.NC_DEST_TERM_RTSI1

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Private Sub CreateThreads()

      Try
        ' Create receive CAN message delivery thread and set priority
        mRcvCanMsgDeliveryThread = New Thread(New ThreadStart(AddressOf DeliverRcvdCANMsgs))
        mRcvCanMsgDeliveryThread.Priority = ThreadPriority.Highest 'ThreadPriority.AboveNormal

        ' Create transmit CAN message thread and set priority
        mXmtCanMessageThread = New Thread(New ThreadStart(AddressOf Xmt_CAN_Msgs))
        mXmtCanMessageThread.Priority = ThreadPriority.Highest 'ThreadPriority.AboveNormal

        ' Create Queue Rcv CAN message thread and set priority
        mQueueRcvdCanMsgThread = New Thread(New ThreadStart(AddressOf Queue_Rcvd_CAN_Msgs))
        mQueueRcvdCanMsgThread.Priority = ThreadPriority.Highest 'ThreadPriority.AboveNormal

        ' Create transmit CAN message thread and set priority
        mCanLogThread = New Thread(New ThreadStart(AddressOf CAN_Logging))
        mCanLogThread.Priority = ThreadPriority.AboveNormal


      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

#End Region

#Region "Thread Handlers"
    Private Sub DeliverRcvdCANMsgs()

      Dim msgStruct As New CAN_Msg_Struct
      Dim msgCount As Integer = 0
      Dim dequeued As Boolean = False
      Dim strCANMessageLog As String
      Try
        Do
          ' Block until received messages have been queued.
          mRcvCanMsgDeliveryWaitHandle.WaitOne()
          If (mblnTerminateRcvCanMsgDeliverThread = True) Then
            ' Allow this thread to terminate.
            Exit Do
          End If
          ' Deliver all messages in the delivery queue.
          Do
            If (mRcvdCanMessageQueue.Count <> 0) Then
              ' Grab the next message along with the latest queued message count.
              dequeued = mRcvdCanMessageQueue.TryDequeue(msgStruct)
              msgCount = mRcvdCanMessageQueue.Count
              dequeued = True
            Else
              dequeued = False
              msgCount = 0
            End If
            If (dequeued = True) Then
              ' Invoke the multicast delegate corresponding to the type of the received message.
              ' Messages that don't have a corresponding delegate are ignored.
              Select Case msgStruct.CanMsg.ID And SRC_ADDR_MASK
                'Select Case msgStruct.CanMsg.ID 'And &HFFFFFF00 'SRC_ADDR_MASK
                Case ADDR_CLAIM, CAT_ADDR_CLAIM
                  If (mRcvMsgDelegates.AddrClaimDelegates IsNot Nothing) Then
                    mRcvMsgDelegates.AddrClaimDelegates.DynamicInvoke(msgStruct)
                  End If
                Case STATUS_MESSAGE '&H38FFC502 '&H18FFC500      ' Cummins 18FFC5xx
                  If (mRcvMsgDelegates.StatusMsgDelegates IsNot Nothing) Then
                    mRcvMsgDelegates.StatusMsgDelegates.DynamicInvoke(msgStruct)
                    'Log CAN Messages if enabled
                    'strCANMessageLog = "CAN Status Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2")
                    'Call LogCanMessage(strCANMessageLog)
                    'Call LogCanMessage()
                    'strCANMessageLog = "CAN Status Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", msgStruct.CanMsg.DATA)
                    'LoadCANMsgLogQueue(strCANMessageLog)
                  End If
                Case TOOL_MSG_RESPONSE '&H18FF0D00 '&H38FF0D02  
                  If (mRcvMsgDelegates.ToolResponseDelegates IsNot Nothing) Then
                    mRcvMsgDelegates.ToolResponseDelegates.DynamicInvoke(msgStruct)
                    'Log CAN Messages if enabled
                    'strCANMessageLog = "CAN Tool Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2")
                    'Call LogCanMessage(strCANMessageLog)
                    'Call LogCanMessage()
                    'strCANMessageLog = "CAN Tool Message Response Delivered, MessageID =, " & Hex(msgStruct.CanMsg.ID) & ", OpCode =, " & msgStruct.CanMsg.DATA(1).ToString("X2") & msgStruct.CanMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", msgStruct.CanMsg.DATA)
                    'LoadCANMsgLogQueue(strCANMessageLog)
                  End If
                  'Case &H38DAF100 '&H18DAF100 'CUST_BTLDR_RESPONSE
                  '  If (RcvMsgDelegates.CustBtldrResponseDelegates IsNot Nothing) Then
                  '    RcvMsgDelegates.CustBtldrResponseDelegates.DynamicInvoke(msgStruct)
                  '  End If
              End Select
            End If
          Loop While (msgCount > 0)
        Loop

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub Queue_Rcvd_CAN_Msgs()

      Dim rcvMsg As nican.NCTYPE_CAN_STRUCT = New nican.NCTYPE_CAN_STRUCT
      Dim status As Integer
      Dim msgID As Integer
      Dim canMsg As TPCANMsg
      Dim msgs_queued As Boolean = False
      Dim strCANMessageLog As String
      Try
        Do
          Do
            If (mblnTerminateQueueRcvdCanMsgThread = True) Then
              Exit Do
            End If
            ' Read in received CAN messages from the CAN device.
            status = nican.ncWaitForState(mintObjectHandle, nican.NC_STATE_ENUM.NC_ST_READ_AVAIL, 100, mintCurrentState)
            'If ((status = nican.NC_SUCCESS) AndAlso ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) > 0)) Then
            If ((status = nican.NC_SUCCESS) AndAlso ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) = nican.NC_STATE_ENUM.NC_ST_READ_AVAIL)) Then
              status = nican.ncRead(mintObjectHandle, Marshal.SizeOf(rcvMsg), rcvMsg)
              If (status = nican.NC_SUCCESS) Then
                canMsg = New TPCANMsg
                'Strip Off Extended Message Bit
                msgID = rcvMsg.ArbitrationId And &HF0000000 '38FF0D02 And F0000000 = 30000000
                msgID >>= 1 '= 18000000
                msgID = msgID + (rcvMsg.ArbitrationId And &HFFFFFF) '= 18000000 + (38FF0D02 And FFFFFF) = 18000000 + FF0D02 = 18FF0D02
                'Store ArbitrationID without extended message bit
                canMsg.ID = msgID
                canMsg.DATA = rcvMsg.Data
                canMsg.LEN = rcvMsg.DataLength
                canMsg.MSGTYPE = rcvMsg.FrameType
                'Console.WriteLine(Hex(msgID))
                ' Successfully read CAN message, now queue it for delivery.
                mRcvdCanMessageQueue.Enqueue(New CAN_Msg_Struct(canMsg, rcvMsg.Timestamp))
                msgs_queued = True

                'Log CAN Messages if enabled
                'strCANMessageLog = "CAN Message Received and Queued, MessageID =, " & Hex(canMsg.ID) & ", OpCode =, " & canMsg.DATA(1).ToString("X2") & canMsg.DATA(0).ToString("X2")
                'Call LogCanMessage(strCANMessageLog)
                'Call LogCanMessage()
                strCANMessageLog = "CAN Message Received and Queued, MessageID =, " & Hex(canMsg.ID) & ", OpCode =, " & canMsg.DATA(1).ToString("X2") & canMsg.DATA(0).ToString("X2") & ", Data =, " & String.Join(",", canMsg.DATA)
                LoadCANMsgLogQueue(strCANMessageLog)

              End If
            End If
          Loop While ((mintCurrentState And nican.NC_STATE_ENUM.NC_ST_READ_AVAIL) > 1)
          If (msgs_queued = True) Then
            ' Unblock receive CAN message delivery thread.
            mRcvCanMsgDeliveryWaitHandle.Set()
            msgs_queued = False
          End If
          Thread.Sleep(1)
        Loop While (mblnTerminateQueueRcvdCanMsgThread = False)

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Private Sub Xmt_CAN_Msgs()

      Dim nicanFrame As nican.NCTYPE_CAN_FRAME = New nican.NCTYPE_CAN_FRAME
      Dim status As Integer
      'Dim msgToSend As TPCANMsg = Nothing
      Dim msgToSend As New clsCANActuator.clsCANMessage
      Dim count As Integer = 0
      Dim dequeued As Boolean = False
      Dim strCANMessageLog As String
      Try
        Do
          ' Block until a CAN message is queued for transmit.
          mXmtCanMsgWaitHandle.WaitOne()
          If (mblnTerminateXmtCanMsgThread = True) Then
            ' Allow this thread to terminate.
            Exit Do
          End If
          Do
            ' Send the next queued message.
            If (mCanMsgOutputQueue.Count <> 0) Then
              ' Lock the queue while reading.
              dequeued = mCanMsgOutputQueue.TryDequeue(msgToSend)
              count = mCanMsgOutputQueue.Count
            Else
              count = 0
              dequeued = False
            End If
            If (dequeued = True) Then
              nicanFrame.IsRemote = nican.NC_FRMTYPE_DATA
              'nicanFrame.ArbitrationId = msgToSend.SendArbitrationID 'nican.NC_FL_CAN_ARBID_XTD Or msgToSend.ID
              nicanFrame.ArbitrationId = msgToSend.SendArbitrationID Or nican.NC_FL_CAN_ARBID_XTD
              nicanFrame.DataLength = msgToSend.DataLength 'msgToSend.LEN
              nicanFrame.Data = msgToSend.Data 'msgToSend.DATA
              status = nican.ncWrite(mintObjectHandle, Marshal.SizeOf(nicanFrame), nicanFrame)

              'Log CAN Messages if enabled
              'strCANMessageLog = "CAN Message Transmitted, MessageID =, " & Hex(nicanFrame.ArbitrationId) & ", OpCode =, " & nicanFrame.Data(1).ToString("X2") & nicanFrame.Data(0).ToString("X2")
              'Call LogCanMessage(strCANMessageLog)
              'Call LogCanMessage()
              strCANMessageLog = "CAN Message Transmitted, MessageID =, " & Hex(nicanFrame.ArbitrationId) & ", OpCode =, " & nicanFrame.Data(1).ToString("X2") & nicanFrame.Data(0).ToString("X2") & ", Data =, " & String.Join(",", nicanFrame.Data)
              LoadCANMsgLogQueue(strCANMessageLog)
            End If
          Loop While ((count > 0) AndAlso (status <> nican.CanErrComm))
        Loop

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Private Sub CAN_Logging()
      'Dim nicanFrame As nican.NCTYPE_CAN_FRAME = New nican.NCTYPE_CAN_FRAME
      'Dim status As Integer
      ''Dim msgToSend As TPCANMsg = Nothing
      'Dim msgToSend As New clsCANActuator.clsCANMessage
      Dim CanMessageLogStreamWriter As IO.StreamWriter = Nothing

      Dim count As Integer = 0
      Dim dequeued As Boolean = False
      Dim strLogMessage As String = ""
      Dim strFileName As String = ""
      Dim strDate As String = ""
      Dim dblElapsedTime As Double
      Try

        strFileName = mstrCANMessageLogFilePath & "\CanMessageLog.txt"
        Do
          ' Block until a CAN message is queued for transmit.
          mCanLogWaitHandle.WaitOne()
          If (mblnTerminateCanLogThread = True) Then
            ' Allow this thread to terminate.
            Exit Do
          End If
          Do
            ' Send the next queued message.
            If (mCanMsgLogQueue.Count <> 0) Then
              ' Lock the queue while reading.
              dequeued = mCanMsgLogQueue.TryDequeue(strLogMessage)
              count = mCanMsgLogQueue.Count
            Else
              count = 0
              dequeued = False
            End If
            If (dequeued = True) Then
              strDate = Format(Now, "MM/dd/yyyy hh:mm:ss:fff tt")
              If mCanLogStartTime = 0 Then
                dblElapsedTime = 0
              Else
                QueryPerformanceCounter(HP_CounterValue)
                dblElapsedTime = (CDbl(HP_CounterValue) * CounterPeriod) - mCanLogStartTime
              End If
              strDate = strDate & ", " & dblElapsedTime
              CanMessageLogStreamWriter = New IO.StreamWriter(strFileName, True)
              CanMessageLogStreamWriter.WriteLine(strDate & ", " & strLogMessage)
              CanMessageLogStreamWriter.Flush()
              CanMessageLogStreamWriter.Close()
              QueryPerformanceCounter(HP_CounterValue)
              mCanLogStartTime = (CDbl(HP_CounterValue) * CounterPeriod)
            End If
          Loop While (count > 0)
        Loop

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub


#End Region

  End Class

#End Region

  '====================================================================================================
#Region "Class clsRcvCanMsgDelegates"
  Public Class clsRcvCanMsgDelegates

#Region "Constructor"
    Protected Sub New()
      ' Prevents compiler from creating a default Public constructor and also
      ' prevents any caller from creating their own instance of this class.
    End Sub
#End Region

#Region "Private Member Variables"

    Private mAnomaly As clsAnomaly
    Private mStackTrace As New clsStackTrace
    Private mStopwatch As New clsStopWatch

    ' Private single instance of this class.
    Private Shared _RcvCanMsgDelegatesInstance As clsRcvCanMsgDelegates
    ' Multicast Delegate used to call registered delegates when address claim messages are received.
    Private mAddrClaimDelegates As [Delegate]
    ' Locking object for address claim multicast delegate.
    Private mAddrClaimLock As Object = New Object
    ' Multicast Delegate used to call registered delegates when status messages are received.
    Private mStatusMsgDelegates As [Delegate]
    ' Locking object for address claim multicast delegate.
    Private mStatusMsgLock As Object = New Object
    ' Multicast Delegate used to call registered delegates when satus messages are received.
    Private mToolResponseDelegates As [Delegate]
    ' Locking object for tool reponse message multicast delegate.
    Private mToolMsgLock As Object = New Object
    ' Multicast Delegate used to call registered delegates when custom bootloader response messages are received.
    Private mCustBtldrResponseDelegates As [Delegate]
    ' Locking object for tool reponse message multicast delegate.
    Private mCustBtldrMsgLock As Object = New Object
#End Region

#Region "Public Variables"
    ' Delegate type to be used when registering callbacks for specific messages.
    Public Delegate Sub DelegateSub(ByVal MsgStruct As clsCanMsgHandling.CAN_Msg_Struct)

#End Region

#Region "Properties"
    Public Property Anomaly() As clsAnomaly
      Get
        Return mAnomaly
      End Get
      Set(ByVal value As clsAnomaly)
        mAnomaly = value
      End Set
    End Property

    Public ReadOnly Property AddrClaimDelegates() As DelegateSub
      Get
        SyncLock mAddrClaimLock
          Return mAddrClaimDelegates
        End SyncLock
      End Get
    End Property

    Public ReadOnly Property StatusMsgDelegates() As DelegateSub
      Get
        SyncLock mStatusMsgLock
          Return mStatusMsgDelegates
        End SyncLock
      End Get
    End Property

    Public ReadOnly Property ToolResponseDelegates() As DelegateSub
      Get
        SyncLock mToolMsgLock
          Return mToolResponseDelegates
        End SyncLock
      End Get
    End Property

    Public ReadOnly Property CustBtldrResponseDelegates() As DelegateSub
      Get
        SyncLock mCustBtldrMsgLock
          Return mCustBtldrResponseDelegates
        End SyncLock
      End Get
    End Property
#End Region

#Region "Public Methods"
    Public Shared Function GetInstance() As clsRcvCanMsgDelegates 'TER
      Try
        ' Only one instance of this class (singleton) is allowed.
        ' Instantiate single instance of this class if it hasn't already been done.
        If (_RcvCanMsgDelegatesInstance Is Nothing) Then
          _RcvCanMsgDelegatesInstance = New clsRcvCanMsgDelegates
        End If

        Return _RcvCanMsgDelegatesInstance

      Catch ex As Exception
        Return Nothing
      End Try
    End Function

    Public Sub AddAddrClaimDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mAddrClaimLock
          mAddrClaimDelegates = MulticastDelegate.Combine(mAddrClaimDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAddrClaimDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mAddrClaimLock
          mAddrClaimDelegates = MulticastDelegate.Remove(mAddrClaimDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAllAddrClaimDelegates(ByVal del As DelegateSub)
      Try
        Do
          SyncLock mAddrClaimLock
            mAddrClaimDelegates = MulticastDelegate.Remove(mAddrClaimDelegates, del)
          End SyncLock
        Loop Until mAddrClaimDelegates Is Nothing

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub AddStatusMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mStatusMsgLock
          mStatusMsgDelegates = MulticastDelegate.Combine(mStatusMsgDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveStatusMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mStatusMsgLock
          mStatusMsgDelegates = MulticastDelegate.Remove(mStatusMsgDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAllStatusMsgDelegates(ByVal del As DelegateSub)
      Try
        Do
          SyncLock mStatusMsgLock
            mStatusMsgDelegates = MulticastDelegate.Remove(mStatusMsgDelegates, del)
          End SyncLock
        Loop Until mStatusMsgDelegates Is Nothing

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub AddToolMsgDelegate(ByVal del As DelegateSub)
      Try

        SyncLock mToolMsgLock
          mToolResponseDelegates = MulticastDelegate.Combine(mToolResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveToolMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mToolMsgLock
          mToolResponseDelegates = MulticastDelegate.Remove(mToolResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveAllToolMsgDelegates(ByVal del As DelegateSub)
      Try
        Do
          SyncLock mToolMsgLock
            mToolResponseDelegates = MulticastDelegate.Remove(mToolResponseDelegates, del)
          End SyncLock
        Loop Until mToolResponseDelegates Is Nothing

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub AddCustBtldrMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mCustBtldrMsgLock
          mCustBtldrResponseDelegates = MulticastDelegate.Combine(mCustBtldrResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

    Public Sub RemoveCustbtldrMsgDelegate(ByVal del As DelegateSub)
      Try
        SyncLock mCustBtldrMsgLock
          mCustBtldrResponseDelegates = MulticastDelegate.Remove(mCustBtldrResponseDelegates, del)
        End SyncLock

      Catch ex As TsopAnomalyException
        mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Catch ex As Exception
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      End Try
    End Sub

#End Region

#Region "Private Methods"

#End Region
  End Class
#End Region

End Class
