
Option Explicit On

Public Class IntelDataRecord

#Region "Private Member Variables"

  Private mChecksum As Short

  Private mData() As Byte
  Private mDataLength As UInt16

  Private mEndAddress As UInt16

  Private mRecordType As HEXRecordType

  Private mStartAddress As UInt16

#End Region

#Region "Public Constants, Enums, Structures...etc"
  Public Enum HEXRecordType As Byte
    DataRecord = 0
    EOFRecord = 1
    ExtendedSegmentAddress = 2
		ExtendedLinearAddress = 4
		StartSegmentAddress = 3
	End Enum
#End Region

#Region "Properties"
  Public Property CheckSum() As UInt16
    Get
      Return mChecksum
    End Get
    Set(ByVal value As UInt16)
      mChecksum = value
    End Set
  End Property

  Public Property Data() As Byte()
    Get
      Return mData
    End Get
    Set(ByVal value As Byte())
      mData = value
    End Set
  End Property

  Public Property DataLength() As UInt16
    Get
      Return mDataLength
    End Get
    Set(ByVal value As UInt16)
      mDataLength = value
    End Set
  End Property

  Public Property EndAddress() As UInt16
    Get
      Return mEndAddress
    End Get
    Set(ByVal value As UInt16)
      mEndAddress = value
    End Set
  End Property

  Public Property RecordType() As HEXRecordType
    Get
      Return mRecordType
    End Get
    Set(ByVal value As HEXRecordType)
      mRecordType = value
    End Set
  End Property

  Public Property StartAddress() As UInt16
    Get
      Return mStartAddress
    End Get
    Set(ByVal value As UInt16)
      mStartAddress = value
    End Set
  End Property

#End Region

End Class
