
Imports System.Xml
Imports System.IO
Imports System.Data.OleDb
Imports System.Linq

Public Class clsOPC

#Region "Constructors ==============================================="

  Public Sub New(ByVal configFilePath As String)
    Try

      Me.mstrClassConfigFilePath = configFilePath

      Call CreateConfigDataTable()
      If Not mAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      Call GetConfig()
      If Not mAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      Call GetConfigurationSettings()
      If Not mAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      Call StartOPCCommunications()
      If Not mAnomaly Is Nothing Then
        Throw New TsopAnomalyException
      End If

      KeepAliveTimer = New Timer
      KeepAliveTimer.Interval = OPCKeepAliveTimerMS
      KeepAliveTimer.Enabled = True

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

#End Region

#Region "Delegates ======================="

#End Region

#Region "Events ================================================================================="
  Public Event DataChanged(ByVal strName As String, ByVal strValue As String)
  Public Event StartScan()
  Public EventRaised As Boolean
#End Region

#Region "Private Member Variables =================================================="

  Private mAnomaly As clsAnomaly
  Private mStackTrace As New clsStackTrace
  Private mstrClassConfigFilePath As String
  Private mstrDeviceID As String

  Private mstrOPCServer As String
  Private mstrOPCGroup As String
  Private mstrOPCTopic As String
  Private mSetOPC_Start As String
  Private mintOPCKeepAliveTimerMS As Integer
  Private mArray() As String
  Private mdtPortConfig As New DataTable
  Private mStopWatch As New clsStopWatch
  Private mblnProcessStarted As Boolean

#End Region

#Region "Public Constants, Structures, Enums =================================================="

  Public WithEvents KeepAliveTimer As Timer
  Public WithEvents oOPC As New KepwareOPC
  Public aryOPCItems() As String

  Public oOPCDataExchange As New OpcDataExchange
  'Public gAnomaly As clsAnomaly

  Structure PLCItems
    Dim StartScan As Integer
    Dim StartScanAck As Integer
    Dim PLCComplete As Integer
    Dim PCInitialized As Integer
    Dim PCFault As Integer
    Dim CalcComplete As Integer
    Dim GoodResult As Integer
    Dim WatchdogInhibit As Integer
    Dim BOM As Integer

    Dim StartLeakCheck1 As Integer
    Dim LeakCheck1Started As Integer
    Dim LeakCheck1Done As Integer
    Dim RestartLeakCheck1 As Integer
    Dim StartLeakCheck2 As Integer
    Dim LeakCheck2Started As Integer
    Dim LeakCheck2Done As Integer
    Dim RestartLeakCheck2 As Integer
    Dim WriteReq As Integer
    Dim CartNumberWrite As Integer
    Dim StatusWrite As Integer
    Dim BomWrite As Integer
    Dim QtyWrite As Integer
    Dim IdWrite As Integer
    Dim ReadReqStatus As Integer
    Dim ReadCartNumberStatus As Integer
    Dim ReadStatusOnly As Integer
    Dim LeakPcReadBom As Integer
    Dim LeakPcReadQty As Integer
    Dim LeakPcReadId As Integer
    Dim Dummy As Integer
  End Structure
  Public PLCDataItemPosition As PLCItems

#End Region

#Region "Properties =================================================="

  Public Property Anomaly() As clsAnomaly
    Get
      Return mAnomaly
    End Get
    Set(ByVal value As clsAnomaly)
      mAnomaly = value
    End Set
  End Property

  Property SetOPC_Start() As String
    Get
      SetOPC_Start = mSetOPC_Start
    End Get
    Set(ByVal value As String)
      Dim intdata As Integer
      mSetOPC_Start = value
      intdata = WriteData(OPCGroup, 7, value, "")

    End Set
  End Property

  Public Property OPCGroup() As String
    Get
      Return Me.mstrOPCGroup
    End Get
    Set(ByVal value As String)
      Me.mstrOPCGroup = value
    End Set
  End Property

  Public Property OPCKeepAliveTimerMS() As Integer
    Get
      Return Me.mintOPCKeepAliveTimerMS
    End Get
    Set(ByVal value As Integer)
      Me.mintOPCKeepAliveTimerMS = value
    End Set
  End Property

  Public Property OPCServer() As String
    Get
      Return Me.mstrOPCServer
    End Get
    Set(ByVal value As String)
      Me.mstrOPCServer = value
    End Set
  End Property

  Public Property OPCTopic() As String
    Get
      Return Me.mstrOPCTopic
    End Get
    Set(ByVal value As String)
      Me.mstrOPCTopic = value
    End Set
  End Property

  Public ReadOnly Property PortConfigTable() As DataTable
    Get
      Return Me.mdtPortConfig
    End Get
  End Property

  Public Property ProcessStarted As Boolean
    Get
      Return mblnProcessStarted
    End Get
    Set(value As Boolean)
      mblnProcessStarted = value
    End Set
  End Property

#End Region

#Region "Public Methods =================================================="

  Public Function ChangeData(ByVal strVariable As String, ByVal strValue As String) As Boolean
    Dim intLoop As Int16
    Try
      For intLoop = 1 To mdtPortConfig.Rows.Count
        If UCase(mdtPortConfig.Rows(intLoop).Item("ItemAdress")) = UCase(strVariable) Then
          Call WriteData(OPCGroup, intLoop + 1, strValue, "")
          If mAnomaly IsNot Nothing Then
            Throw New TsopAnomalyException
          End If
          Return True
        End If
      Next
      Return True

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  'Public Sub oOPC_DataChanged(ByVal intArrayPosition As Integer, ByVal strNewValue As String, ByVal strQuality As String) Handles oOPC.DataChanged
  '  Try
  '    mdtPortConfig.Rows(intArrayPosition - 1).Item("Value") = strNewValue

  '    RaiseEvent DataChanged(mdtPortConfig.Rows(intArrayPosition - 1).Item("ItemAddress"), strNewValue)

  '  Catch ex As TsopAnomalyException
  '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
  '  Finally
  '  End Try
  'End Sub

  Private Sub oOPC_DataChanged(ByVal intArrayPosition As Integer, ByVal strNewValue As String, ByVal strQuality As String) Handles oOPC.DataChanged

    Dim intLoop As Integer = 0
    Dim intLoop2 As Integer = 0
    Dim intLoop3 As Integer = 0
    Dim intLoop4 As Integer = 0
    Dim strStatus As String = ""
    Dim strUpperByte As String = ""
    Dim strLowerByte As String = ""
    Dim intNumber(16) As Integer
    Dim intTemp As Int16 = 0
    Dim intTemp2 As Int16 = 0
    Dim intTemp3 As Int16 = 0
    Dim intLoop5 As Int16 = 0
    Dim strMess As String = ""
    Dim strStartScan1 As String = ""
    Dim strStartScan2 As String = ""

    Try
      'Data changed

      'mdtPortConfig.Rows(intArrayPosition - 1).Item("Value") = strNewValue

      'RaiseEvent DataChanged(mdtPortConfig.Rows(intArrayPosition - 1).Item("ItemAddress"), strNewValue)
      'RaiseEvent DataChanged(intArrayPosition, strNewValue)

      Select Case intArrayPosition
        Case PLCDataItemPosition.StartScan
          If strNewValue = "True" Then
            KeepAliveTimer.Enabled = False
            strStartScan1 = "False"
            strStartScan2 = "False"

            Call ReadData(OPCGroup, PLCDataItemPosition.StartScan, strStartScan1, strMess)
            mStopWatch.DelayTime(50)
            Call ReadData(OPCGroup, PLCDataItemPosition.StartScan, strStartScan2, strMess)

            'mblnProcessStarted = True
            If strStartScan1 = "True" And strStartScan2 = "True" Then
              RaiseEvent StartScan()
            End If

          Else
            KeepAliveTimer.Enabled = True
            'mblnProcessStarted = False
          End If
      End Select

      'If intArrayPosition = PLCDataItemPosition.StartScan Then
      '  If strNewValue = "True" Then
      '    strStartScan1 = "False"
      '    strStartScan2 = "False"

      '    Call ReadData(OPCGroup, PLCDataItemPosition.StartScan, strStartScan1, strMess)
      '    mStopWatch.DelayTime(50)
      '    Call ReadData(OPCGroup, PLCDataItemPosition.StartScan, strStartScan2, strMess)

      '    If (strStartScan1 = "True") And (strStartScan2 = "True") Then
      '      RaiseEvent StartScan()
      '    Else
      '      KeepAliveTimer.Enabled = True
      '    End If
      '  End If
      'End If

    Catch ex As Exception
      MsgBox("Error" & ex.ToString)
    End Try

  End Sub


  Public Function ReadData(ByRef strOPCGroup As String, ByRef position As Int16, ByRef strValue As String, ByRef strMess As String) As Boolean
    Dim intResult As Integer
    Dim strErrorMessage As String
    Try

      intResult = oOPC.GetValue(OPCGroup, position, strValue, strMess)
      If intResult <> 0 Then
        strErrorMessage = "Error " & CStr(intResult) & " occured in OPC GetValue. "
        strErrorMessage = strErrorMessage & strMess
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If
      Return True

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    Finally

    End Try
  End Function

  Public Function WriteData(ByRef strOPCGroup As String, ByRef position As Int16, ByRef strValue As String, ByRef strMess As String) As Boolean
    Dim intResult As Integer
    Dim strErrorMessage As String
    Try
      intResult = oOPC.SetValue(OPCGroup, position, strValue, strMess)
      If intResult <> 0 Then
        strErrorMessage = "Error " & CStr(intResult) & " occured in OPC SetValue. "
        strErrorMessage = strErrorMessage & strMess
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If
      Return True

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return False
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return False
    End Try
  End Function

  '==================================================
  'Added below for Leak

  Public Sub SetOutput(ByVal intLocation As Integer, ByVal strValue As String)
    Dim gstrError As String = ""
    Try

      WriteData(OPCGroup, intLocation, strValue, gstrError)

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub

  Public Function GetInput(ByVal intLocation As Integer) As String
    Dim gstrError As String = ""
    Dim blnResult As Boolean
    Dim strValue As String = ""
    Try

      blnResult = ReadData(OPCGroup, intLocation, strValue, gstrError)
      Return strValue

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    End Try
  End Function

#End Region

#Region "Private Methods =================================================="

  Private Sub CreateConfigDataTable()
    Try
      'mdtPortConfig.Columns.Add("ItemName", GetType(String))
      'mdtPortConfig.Columns.Add("ItemAddress", GetType(String))
      'mdtPortConfig.Columns.Add("Direction", GetType(String))

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  Private Sub GetConfig()
    Dim strClassName As String
    Dim strFileName As String
    Dim intLoop As Int16
    Try
      strClassName = Me.GetType.Name
      strFileName = strClassName & "_Config.csv"

      mdtPortConfig.Clear()
      'mdtPortConfig = ReadCsvFileIntoDataTable(mstrClassConfigFilePath, strFileName)
      mdtPortConfig = StreamCsvFileIntoDataTable(mstrClassConfigFilePath, strFileName)

      ReDim aryOPCItems(mdtPortConfig.Rows.Count)

      'For intLoop = 0 To mdtPortConfig.Rows.Count - 1
      '  aryOPCItems(intLoop + 1) = mdtPortConfig.Rows(intLoop).Item(1)
      'Next

      For intLoop = 0 To mdtPortConfig.Rows.Count - 1
        Select Case mdtPortConfig.Rows(intLoop).Item(0).ToString
          Case "OPC_StartScan"
            PLCDataItemPosition.StartScan = intLoop + 1
          Case "OPC_StartAck"
            PLCDataItemPosition.StartScanAck = intLoop + 1
          Case "OPC_PLCComplete"
            PLCDataItemPosition.PLCComplete = intLoop + 1
          Case "OPC_PCInitialized"
            PLCDataItemPosition.PCInitialized = intLoop + 1
          Case "OPC_PCFault"
            PLCDataItemPosition.PCFault = intLoop + 1
          Case "OPC_CalcComplete"
            PLCDataItemPosition.CalcComplete = intLoop + 1
          Case "OPC_GoodResult"
            PLCDataItemPosition.GoodResult = intLoop + 1
          Case "OPC_WatchdogInhibit"
            PLCDataItemPosition.WatchdogInhibit = intLoop + 1
          Case "OPC_StartLeakCheck1"
            PLCDataItemPosition.StartLeakCheck1 = intLoop + 1
          Case "OPC_LeakCheck1Started"
            PLCDataItemPosition.LeakCheck1Started = intLoop + 1
          Case "OPC_LeakCheck1Done"
            PLCDataItemPosition.LeakCheck1Done = intLoop + 1
          Case "OPC_RestartLeakCheck1"
            PLCDataItemPosition.RestartLeakCheck1 = intLoop + 1
          Case "OPC_StartLeakCheck2"
            PLCDataItemPosition.StartLeakCheck2 = intLoop + 1
          Case "OPC_LeakCheck2Started"
            PLCDataItemPosition.LeakCheck2Started = intLoop + 1
          Case "OPC_LeakCheck2Done"
            PLCDataItemPosition.LeakCheck2Done = intLoop + 1
          Case "OPC_RestartLeakCheck2"
            PLCDataItemPosition.RestartLeakCheck2 = intLoop + 1
          Case "OPC_BOM"
            PLCDataItemPosition.BOM = intLoop + 1
          Case "OPC_WriteReq"
            PLCDataItemPosition.WriteReq = intLoop + 1
          Case "OPC_CartNumberWrite"
            PLCDataItemPosition.CartNumberWrite = intLoop + 1
          Case "OPC_StatusWrite"
            PLCDataItemPosition.StatusWrite = intLoop + 1
          Case "OPC_BomWrite"
            PLCDataItemPosition.BomWrite = intLoop + 1
          Case "OPC_QtyWrite"
            PLCDataItemPosition.QtyWrite = intLoop + 1
          Case "OPC_IdWrite"
            PLCDataItemPosition.IdWrite = intLoop + 1
          Case "OPC_ReadReqStatus"
            PLCDataItemPosition.ReadReqStatus = intLoop + 1
          Case "OPC_ReadCartNumberStatus"
            PLCDataItemPosition.ReadCartNumberStatus = intLoop + 1
          Case "OPC_ReadStatusOnly"
            PLCDataItemPosition.ReadStatusOnly = intLoop + 1
          Case "OPC_LeakPcReadBom"
            PLCDataItemPosition.LeakPcReadBom = intLoop + 1
          Case "OPC_LeakPcReadQty"
            PLCDataItemPosition.LeakPcReadQty = intLoop + 1
          Case "OPC_LeakPcReadID"
            PLCDataItemPosition.LeakPcReadId = intLoop + 1
          Case "Dummy"
            PLCDataItemPosition.Dummy = intLoop + 1


        End Select

        aryOPCItems(intLoop + 1) = mdtPortConfig.Rows(intLoop).Item(1)
      Next


    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  Private Sub GetConfigurationSettings()
    Dim configReader As XmlTextReader
    Dim configSet As New DataSet
    Dim configTable As DataTable
    Dim configRow As DataRow
    Dim strClassName As String

    Try
      strClassName = Me.GetType.Name

      'Create the XML Reader
      'configReader = New XmlTextReader(mstrClassConfigFilePath & strClassName & ".cfg")
      configReader = New XmlTextReader(My.Computer.FileSystem.CombinePath(mstrClassConfigFilePath, strClassName & ".cfg"))

      'Clear data set that holds xml file contents
      configSet.Clear()
      'Read the file
      configSet.ReadXml(configReader)

      'Config File not hierarchical, only 1 table and 1 row, with many fields
      configTable = configSet.Tables(0)
      configRow = configTable.Rows(0)

      'Assign field values to variables
      mstrOPCServer = configRow("OPCServer")
      mstrOPCGroup = configRow("OPCGroup")
      mstrOPCTopic = configRow("OPCTopic")
      mintOPCKeepAliveTimerMS = configRow("OPCKeepAliveTimerMS")

      'OPCServer = configTable.Rows(0).Item(1)
      'OPCGroup = configTable.Rows(1).Item(1)
      'OPCTopic = configTable.Rows(2).Item(1)
      'OPCKeepAliveTimerMS = configTable.Rows(3).Item(1)
      'Close reader, releases file
      configReader.Close()

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  Private Function ReadCsvFileIntoDataTable(ByVal strFilePath As String, ByVal strFileName As String) As DataTable
    Dim cnConnection As OleDbConnection
    Dim daDataAdapter As OleDbDataAdapter
    Dim dtTable As New DataTable
    Dim strCommandText As String
    Dim strConnection As String
    Dim cmdCommand As OleDbCommand

    Try

      'Instantiate OLEDB Data Adapter, used to fill data table from csv file
      daDataAdapter = New OleDbDataAdapter

      'Connection string for OLEDB comma delimited text file (CSV), file path specified from passed in function argument
      strConnection = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & strFilePath & ";Extended Properties=" & """" & "text;HDR=Yes;FMT=Delimited" & """" & ";"

      'Instantiate OLEDB Connection Object
      cnConnection = New OleDbConnection(strConnection)

      'Specify command text to select all rows and columns from file
      strCommandText = "SELECT * FROM " & strFileName

      'Instantiate OLEDB Command Object
      cmdCommand = New OleDbCommand(strCommandText, cnConnection)

      'Open OLEDB Connection
      cnConnection.Open()

      'Specify the Select Command for the Data Adapter
      daDataAdapter.SelectCommand = cmdCommand

      'Make sure data table is empty
      dtTable.Clear()

      'Executes Select Command and fills the data table with data from the csv file
      daDataAdapter.Fill(dtTable)

      'Close the connection
      cnConnection.Close()

      Return dtTable

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try
  End Function

  Public Function StreamCsvFileIntoDataTable(ByVal strFilePath As String, ByVal strFileName As String, Optional ByVal strFilter As String = Nothing) As DataTable
    Dim dt As New System.Data.DataTable
    Dim blnFirstLine As Boolean = True
    Dim strFilterFieldValue As String
    Dim strFilterColumnName As String
    Dim intFilterColumn As Integer
    Dim strFile As String
    Dim cols() As String
    Dim data() As String
    Dim col As String

    Try

      strFilterColumnName = ""
      strFilterFieldValue = ""
      intFilterColumn = -1
      cols = Nothing
      data = Nothing

      If strFilter <> Nothing Then
        strFilterColumnName = (strFilter.Split("=")(0)).Trim
        strFilterFieldValue = (strFilter.Split("=")(1)).Trim
      End If

      strFile = strFilePath & strFileName

      If IO.File.Exists(strFile) Then
        Using sr As New StreamReader(strFile)
          While Not sr.EndOfStream
            If blnFirstLine Then
              blnFirstLine = False
              cols = sr.ReadLine.Split(",")
              For Each col In cols
                dt.Columns.Add(New DataColumn(col, GetType(String)))
              Next
              If strFilterColumnName <> "" Then
                intFilterColumn = Array.IndexOf(cols, strFilterColumnName)
              End If
            Else
              data = sr.ReadLine.Split(",")
              If intFilterColumn <> -1 Then
                If data(intFilterColumn) = strFilterFieldValue Then
                  dt.Rows.Add(data.ToArray)
                End If
              Else
                dt.Rows.Add(data.ToArray)
              End If
            End If
          End While
        End Using
      End If
      Return dt

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function


  Private Sub StartOPCCommunications()
    Dim intResult As Integer = 0
    Dim strError As String = ""
    Dim intLoop As Int16 = 0
    Dim localException As Exception = Nothing

    Try
      intResult = oOPCDataExchange.ConnectToOPCServer(oOPC, OPCServer, "", strError)

      If intResult <> 0 Then
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "There Is a Problem Establishing A Connection To The OPC Server")
        Throw New TsopAnomalyException
      End If


      intResult = oOPCDataExchange.AddOPCGroup(oOPC, OPCGroup, "", strError)
      If intResult <> 0 Then
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "There Is a Problem Establishing An OPC Group")
        Throw New TsopAnomalyException
      End If

      ReDim mArray(mdtPortConfig.Rows.Count)

      For intLoop = 1 To mdtPortConfig.Rows.Count
        mArray(intLoop) = OPCTopic & "." & OPCGroup & "." & mdtPortConfig.Rows(intLoop - 1).Item(0)
      Next

      'intResult = oOPCDataExchange.AddOPCItems(oOPC, OPCGroup, mArray, mdtPortConfig.Rows.Count, strError)
      intResult = oOPCDataExchange.AddOPCItems(oOPC, OPCGroup, aryOPCItems, mdtPortConfig.Rows.Count, strError)
      If intResult <> 0 Then
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, "There Is a Problem Adding OPC Items")
        Throw New TsopAnomalyException
      End If

      'Call oOPC.ChangeIsActive(OPCGroup, 19, True, "")

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  Private Sub KeepAliveTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles KeepAliveTimer.Tick
    'Dim intLoop As Int16
    Dim strResult As String = ""
    Try
      'KeepAliveTimer.Stop()
      ''If Not PLCInterface Then
      ''  Exit Sub 'DWD Temp
      ''End If


      ''Call SetOPCOutput("OPC_CalcComplete", 1)

      ''Set PC_ready=true

      ''If Not blnOPCPLCFault Then
      'For intLoop = 0 To mArray.GetUpperBound(0)
      '  If mArray(intLoop) = OPCTopic & "." & OPCGroup & "." & "OPC_PC_Ready" Then
      '    'Set Value to 1
      '    'oOPC.SetValue(OPCGroup, intLoop, "1", "")
      '    WriteData(OPCGroup, intLoop, "1", "")
      '    If mAnomaly IsNot Nothing Then
      '      Throw New TsopAnomalyException
      '    End If
      '    Exit For
      '  End If
      'Next
      ''End If
      'KeepAliveTimer.Start()

      KeepAliveTimer.Enabled = False
      'Set PC_ready=true
      WriteData(OPCGroup, PLCDataItemPosition.PCInitialized, "1", strResult)
      'If Not mblnProcessStarted Then KeepAliveTimer.Enabled = True


    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

#End Region

  'Private Sub clsOPC_DataChanged(ByVal strName As String, ByVal strValue As String) Handles Me.DataChanged
  'End Sub


End Class
