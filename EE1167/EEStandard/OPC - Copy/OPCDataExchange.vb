Public Class OpcDataExchange
#Region "Delegates =================================================="

#End Region

#Region "Events =================================================="

#End Region

#Region "Constructors =================================================="

#End Region

#Region "Private Member Variables =================================================="

#End Region

#Region "Public Constants, Structures, Enums =================================================="

#End Region

#Region "Properties =================================================="

#End Region

#Region "Public Methods =================================================="

    Public Function AddOPCGroup(ByRef OpcObject As KepwareOPC, ByVal strOPCGroup As String, ByVal strOPCRefreshRate As String, ByVal strError As String) As Integer
        Dim intReturn As Integer
        Try
            intReturn = OpcObject.AddOPCGroup(strOPCGroup, strOPCRefreshRate, strError)

            If intReturn <> 0 Then
                strError = strError
                Return intReturn
            End If
        Catch ex As Exception

            strError = ex.ToString
            Return 99


    End Try
    Return 0
    End Function

    Public Function AddOPCItems(ByRef OpcObject As KepwareOPC, ByRef strOPCGroup As String, ByVal strArry() As String, ByVal intArraySize As Integer, ByVal strError As String) As Integer
        Dim intReturn As Integer

        Try

            intReturn = OpcObject.AddItemstoGroup(strOPCGroup, strArry, intArraySize, strError)

            If intReturn <> 0 Then
                strError = strError
                Return intReturn
            End If

        Catch ex As Exception

            strError = ex.ToString
            Return 99


    End Try
    Return 0
    End Function

    Public Function ConnectToOPCServer(ByRef OpcObject As KepwareOPC, ByVal strOPCServerName As String, ByVal strOPCNodeName As String, ByVal strError As String) As Integer

        Dim intReturn As Integer
        Try
            intReturn = OpcObject.OpenOPCServer(strOPCServerName, strOPCNodeName, strError)


            strError = strError
            Return intReturn

        Catch ex As Exception

            strError = ex.ToString
            Return 99

        End Try


    End Function

#End Region

#Region "Private Methods =================================================="

#End Region






End Class
