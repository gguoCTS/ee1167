Public Class frmOPCTest

#Region "Delegates =================================================="

#End Region

#Region "Events =================================================="

  Private Sub frmOPCTest_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    DataGridView1.DataSource = gclsOPC.PortConfigTable
    DataGridView1.Columns(0).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
    DataGridView1.Columns(1).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
    DataGridView1.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.None
    DataGridView1.Columns(3).AutoSizeMode = DataGridViewAutoSizeColumnMode.None


    DataGridView1.Columns(0).Width = 200
    DataGridView1.Columns(1).Width = 200
  End Sub

  Private Sub btnWrite_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnWrite.Click
    Dim intValueToWrite As Int16 = 0

    Dim intValue As Integer = 0
    Dim strData As String = ""

    If rb0.Checked Then
      intValueToWrite = 0
    Else
      intValueToWrite = 1
    End If


    'intValue = gclsOPC.WriteData(gclsOPC.OPCGroup, DataGridView1.CurrentCell.RowIndex + 1, intValueToWrite, strData)
    gclsOPC.SetOutput(DataGridView1.CurrentCell.RowIndex + 1, intValueToWrite)



  End Sub


#End Region

#Region "Constructors =================================================="

#End Region

#Region "Private Member Variables =================================================="

#End Region

#Region "Public Constants, Structures, Enums =================================================="

  'Public mdtPortConfig As DataTable
#End Region

#Region "Properties =================================================="

#End Region

#Region "Public Methods =================================================="

#End Region

#Region "Private Methods =================================================="

#End Region









End Class