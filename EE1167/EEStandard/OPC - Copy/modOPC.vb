Module modOPC

  Public WithEvents gclsOPC As clsOPC

  Public Event StartScan()


  Public Function GetPLCTaskListFilter() As TaskListFilterEnum
    Dim blnProgram As Boolean
    Dim blnTest As Boolean
    Dim intTaskListFilter As TaskListFilterEnum

    Try
      blnProgram = gControlFlags.OnPlcStartProgram
      blnTest = gControlFlags.OnPlcStartTest

      If blnProgram And blnTest Then
        intTaskListFilter = TaskListFilterEnum.ProgramAndTest
      ElseIf blnProgram Then
        intTaskListFilter = TaskListFilterEnum.Program
      ElseIf blnTest Then
        intTaskListFilter = TaskListFilterEnum.Test
      Else
        intTaskListFilter = TaskListFilterEnum.None
      End If

      Return intTaskListFilter

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Return TaskListFilterEnum.None
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Return Nothing
    End Try
  End Function

  'Private Sub OPC_DataChanged(ByVal strName As String, ByVal strValue As String) Handles gclsOPC.DataChanged
  '  Dim intLoop As Integer
  '  Dim strItemName As String
  '  Dim blnValue As Boolean
  '  Try

  '    'Data changed
  '    For intLoop = 0 To gclsOPC.PortConfigTable.Rows.Count
  '      If strName = gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemAddress") Then
  '        strItemName = gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemName")
  '        If strValue IsNot Nothing Then
  '          blnValue = CBool(strValue)
  '          Select Case strItemName
  '            Case "OPC_StartScan"
  '              If blnValue = True Then
  '                gControlFlags.StartScan = True
  '                gControlFlags.ManualScan = False
  '                Call OPCStartScan()
  '                If gAnomaly IsNot Nothing Then
  '                  Throw New TsopAnomalyException
  '                End If
  '                If glstAnomaly.Count > 0 Then
  '                  LogAnomaly()
  '                End If
  '              End If
  '              'Case "OPC_PLC_Fault"
  '              '  If blnValue = True Then
  '              '    gControlFlags.AbortTest = True
  '              '    Call SetOPCOutput("OPC_PC_Fault", 1)
  '              '    If gAnomaly IsNot Nothing Then
  '              '      Throw New TsopAnomalyException
  '              '    End If
  '              '  End If
  '            Case "OPC_ReClamp"
  '              If blnValue = True Then
  '                'add reclamp code
  '              End If
  '            Case "OPC_PalletID"
  '              If strValue <> gDeviceInProcess.Pallet Then
  '                If strValue <> "0" Then
  '                  gDeviceInProcess.Pallet = strValue
  '                End If
  '              End If
  '          End Select
  '        End If
  '        Exit For
  '      End If
  '    Next

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '    Call LogAnomaly()
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '    Call LogAnomaly()
  '  End Try
  'End Sub

  'Private Sub OPC_DataChanged(ByVal intPosition As Integer, ByVal strValue As String) Handles gclsOPC.DataChanged
  '  Dim intLoop As Integer
  '  Dim strItemName As String
  '  Dim blnValue As Boolean
  '  Try

  '    'Data changed
  '    Select Case intPosition
  '      Case gclsOPC.PLCDataItemPosition.StartScan
  '        If blnValue = True Then
  '          gControlFlags.StartScan = True
  '          gControlFlags.ManualScan = False
  '          RaiseEvent StartScan()
  '          'Call OPCStartScan()
  '          If gAnomaly IsNot Nothing Then
  '            Throw New TsopAnomalyException
  '          End If
  '          If glstAnomaly.Count > 0 Then
  '            LogAnomaly()
  '          End If
  '        End If
  '    End Select

  '    'For intLoop = 0 To gclsOPC.PortConfigTable.Rows.Count
  '    '  If strName = gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemAddress") Then
  '    '    strItemName = gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemName")
  '    '    If strValue IsNot Nothing Then
  '    '      blnValue = CBool(strValue)
  '    '      Select Case strItemName
  '    '        Case "OPC_StartScan"
  '    '          If blnValue = True Then
  '    '            gControlFlags.StartScan = True
  '    '            gControlFlags.ManualScan = False
  '    '            Call OPCStartScan()
  '    '            If gAnomaly IsNot Nothing Then
  '    '              Throw New TsopAnomalyException
  '    '            End If
  '    '            If glstAnomaly.Count > 0 Then
  '    '              LogAnomaly()
  '    '            End If
  '    '          End If
  '    '          'Case "OPC_PLC_Fault"
  '    '          '  If blnValue = True Then
  '    '          '    gControlFlags.AbortTest = True
  '    '          '    Call SetOPCOutput("OPC_PC_Fault", 1)
  '    '          '    If gAnomaly IsNot Nothing Then
  '    '          '      Throw New TsopAnomalyException
  '    '          '    End If
  '    '          '  End If
  '    '        Case "OPC_ReClamp"
  '    '          If blnValue = True Then
  '    '            'add reclamp code
  '    '          End If
  '    '        Case "OPC_PalletID"
  '    '          If strValue <> gDeviceInProcess.Pallet Then
  '    '            If strValue <> "0" Then
  '    '              gDeviceInProcess.Pallet = strValue
  '    '            End If
  '    '          End If
  '    '      End Select
  '    '    End If
  '    '    Exit For
  '    '  End If
  '    'Next

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '    Call LogAnomaly()
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '    Call LogAnomaly()
  '  End Try
  'End Sub

  Public Sub OPCStartScan()
    Dim intTaskListFilter As TaskListFilterEnum
    Dim strErrorMessage As String
    Dim intValue As Integer
    Dim lStopwatch As New clsStopWatch

    Try

      'PLC Handshaking Before Test
      Call SetOPCOutput("OPC_StartAck", 1)
      lStopwatch.DelayTime(100)
      Call SetOPCOutput("OPC_WatchDogReset", 1)

      'Determine task list filter based on PLC Start Action
      intTaskListFilter = GetPLCTaskListFilter()
      If gAnomaly IsNot Nothing Then
        Call SetOPCOutput("OPC_PC_Fault", 1)
        gControlFlags.AbortTest = True
        Throw New TsopAnomalyException
      End If

      If intTaskListFilter = TaskListFilterEnum.None Then
        Call SetOPCOutput("OPC_PC_Fault", 1)
        gControlFlags.AbortTest = True

        'Throw Exception
        strErrorMessage = "No PLC Start Action Was Specified"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strErrorMessage, gDatabase)
        Throw New TsopAnomalyException
      Else
        'If gControlFlags.LaserTestEnabled = True then
        '    gDeviceInProcess.EncodedSerialNumber = "12345678910"
        '    gMLX90277(1).ReadParameters.Lot = 23547
        '    gMLX90277(1).ReadParameters.Wafer = 16
        '    gMLX90277(1).ReadParameters.X = 56
        '    gMLX90277(1).ReadParameters.Y = 62
        '    gDeviceInProcess.DateCode = GetProductDateCodeBySeries("706")
        '    Call modTaskList.SendSerialNumberToPLC
        'Else
        Call ExecuteTaskList(intTaskListFilter)
        'End If

        If glstAnomaly.Count > 0 Then
          If gControlAndInterface.UsesProgramming And (gDatabase.SubProcessID = gSubProcesses(SubProcessEnum.Programming.ToString)) Then
            gControlFlags.ProgError = True
            Call UpdateProgLotCounts()
            'Call UpdateProgLotDataDisplay()
          Else
            gControlFlags.TestError = True
            Call UpdateTestLotCounts()
            Call UpdateLotDataDisplay()
          End If
          Call UpdatePassFailStatus()
        End If
      End If

      'PLC Handshaking After Test
      intValue = Not gControlFlags.TestFailure And Not gControlFlags.AbortTest
      If intValue = -1 Then intValue = 1
      Call SetOPCOutput("OPC_Result", intValue)
      lStopwatch.DelayTime(100)
      Call SetOPCOutput("OPC_CalcComplete", 1)
      If gControlFlags.AbortTest Then
        If gControlAndInterface.UsesProgramming And (gDatabase.SubProcessID = gSubProcesses(SubProcessEnum.Programming.ToString)) Then
          gControlFlags.ProgError = True
          Call UpdateProgLotCounts()
          'Call UpdateProgLotDataDisplay()
        Else
          gControlFlags.TestError = True
          Call UpdateTestLotCounts()
          Call UpdateLotDataDisplay()
        End If
        Call UpdatePassFailStatus()
      End If

      gControlFlags.AbortTest = False
      gControlFlags.TestError = False
      gControlFlags.ProgError = False
      gControlFlags.TestFailure = False
      gControlFlags.ProgFailure = False
      gControlFlags.PedalFaceFound = False
      glstAnomaly.Clear()
      Call SetOPCOutput("OPC_PC_Fault", 0)
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
      Call SetOPCOutput("OPC_PC_Fault", 0)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
      Call SetOPCOutput("OPC_PC_Fault", 0)
    End Try
  End Sub

  Public Sub OPCWaitForInput(ByVal strTag As String, ByVal Value As String, ByVal TimeoutMs As Integer, ByRef ErrMessage As String)
    Dim intPos As Integer
    Dim lStopWatch As New clsStopWatch
    Dim intElapsedMilliseconds As Integer
    'Dim StartTime As Integer = timeGetTime
    Dim TimeOutExpired As Boolean
    'Dim ElapsedTime As Integer
    Dim intLoop As Integer
    Dim strExceptionMessage As String
    Try

      ErrMessage = ""
      intPos = -1
      For intLoop = 0 To gclsOPC.PortConfigTable.Rows.Count - 1
        If gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemName") = strTag Then
          intPos = intLoop
          Exit For
        End If
      Next
      If intPos = -1 Then
        strExceptionMessage = "PLC Tag: " & strTag & " was not found in config file"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strExceptionMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

      'Initialize Stopwatch
      intElapsedMilliseconds = 0
      lStopWatch.Reset()
      lStopWatch.Start()
      Do
        Application.DoEvents()
        'mdtPortConfig.Rows(intArrayPosition - 1).Item("Value") = strNewValue
        If gclsOPC.PortConfigTable.Rows(intPos).Item("Value") = Value Then
          Exit Do
        End If
        intElapsedMilliseconds = lStopWatch.ElapsedMilliseconds
        'ElapsedTime = (timeGetTime - StartTime)
        'TimeOutExpired = ElapsedTime > TimeoutMs
        TimeOutExpired = intElapsedMilliseconds > TimeoutMs
      Loop While Not TimeOutExpired

      If TimeOutExpired Then
        strExceptionMessage = "Timeout waiting for " & strTag & " =" & Value & " " & intElapsedMilliseconds & " ms"
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strExceptionMessage, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try

  End Sub

  'Public Sub SetOPCOutput(ByVal strOutput As String, ByVal strValue As String)
  '  Dim intLoop As Integer
  '  Dim strError As String
  '  Try
  '    strError = ""

  '    For intLoop = 0 To gclsOPC.PortConfigTable.Rows.Count - 1
  '      If gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemName") = strOutput Then
  '        gclsOPC.WriteData(gclsOPC.OPCGroup, intLoop + 1, strValue, strError)
  '        If strError <> "" Then
  '          gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strError, gDatabase)
  '          Throw New TsopAnomalyException
  '        End If
  '        Exit For
  '      End If
  '    Next

  '  Catch ex As TsopAnomalyException
  '    gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
  '  End Try
  'End Sub

  Public Sub SetOPCOutput(ByVal intLocation As Integer, ByVal strValue As String)
    Dim strResult As String
    Dim blnResult As Boolean
    Dim lStopwatch As New clsStopWatch

    Try

      'Send 
      Call gclsOPC.SetOutput(intLocation, strValue)
      lStopwatch.DelayTime(50)

      'Verify 
      strResult = gclsOPC.GetInput(intLocation)
      blnResult = CBool(strResult)
      If blnResult <> CBool(strValue) Then
        Call gclsOPC.SetOutput(intLocation, strValue)
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


  Public Sub VerifyOPCOutput(ByVal strOutput As String, ByVal strValue As String)
    Dim intLoop As Integer
    Dim strError As String
    Dim strRead As String

    Try
      strError = ""
      strRead = ""

      For intLoop = 0 To gclsOPC.PortConfigTable.Rows.Count - 1
        If gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemName") = strOutput Then
          gclsOPC.ReadData(gclsOPC.OPCGroup, intLoop + 1, strRead, strError)
          If strError <> "" Then
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strError, gDatabase)
            Throw New TsopAnomalyException
          End If
          If strValue <> strRead Then
            strError = strOutput & " OPC Output not set!"
            gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strError, gDatabase)
            Throw New TsopAnomalyException
          End If
          Exit For
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub WaitForReadInput(ByVal strOutput As String, ByVal strValue As String, ByVal TimeoutMs As Integer)
    Dim intLoop As Integer
    Dim strError As String
    Dim strRead As String
    Dim blnDone As Boolean
    Dim lStopWatch As New clsStopWatch
    Dim TimeOutExpired As Boolean

    Try
      strError = ""
      strRead = ""
      blnDone = False

      For intLoop = 0 To gclsOPC.PortConfigTable.Rows.Count - 1
        If gclsOPC.PortConfigTable.Rows(intLoop).Item("ItemName") = strOutput Then
          Do
            gclsOPC.ReadData(gclsOPC.OPCGroup, intLoop + 1, strRead, strError)
            If strError <> "" Then
              gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strError, gDatabase)
              Throw New TsopAnomalyException
            End If
            If strValue = strRead Then
              blnDone = True
            End If
            TimeOutExpired = lStopWatch.ElapsedMilliseconds > TimeoutMs
          Loop While Not TimeOutExpired And Not blnDone
          Exit For
        End If
      Next

      If TimeOutExpired Then
        strError = "Timeout Expired in wait for read input."
        gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, strError, gDatabase)
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub
End Module
