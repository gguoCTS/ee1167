
Public Class MetricPerformanceCriteria

#Region "Private Member Variables =================================================="
  'Private mHighLimitRegions As New SortedDictionary(Of Int16, MpcValue)
  'Private mIdealValueRegions As New SortedDictionary(Of Int16, MpcValue)
  'Private mLowLimitRegions As New SortedDictionary(Of Int16, MpcValue)

  Private mAnomaly As clsAnomaly 'WTG
  'Private mDatabase As Database 'WTG
  Private mHighLimitRegionsTable As New DataTable
  Private mIdealValueRegionsTable As New DataTable
  Private mLowLimitRegionsTable As New DataTable
  Private mStackTrace As New clsStackTrace 'WTG
  Private mstrClassConfigFilePath As String 'WTG
  Private mAlphaMPCValue As New DataTable
#End Region

#Region "Public Enums =================================================="
  Public Enum LimitShape
    Point = 0
    LineSegment = 1
  End Enum
#End Region

#Region "Constructors =================================================="
  Public Sub New()
    Try
      Me.mHighLimitRegionsTable.Columns.Add("RegionNumber", GetType(Int16))
      Me.mHighLimitRegionsTable.Columns.Add("LimitShape", GetType(Int16))
      Me.mHighLimitRegionsTable.Columns.Add("Abscissa1", GetType(Single))
      Me.mHighLimitRegionsTable.Columns.Add("Ordinate1", GetType(Single))
      Me.mHighLimitRegionsTable.Columns.Add("Abscissa2", GetType(Single))
      Me.mHighLimitRegionsTable.Columns.Add("Ordinate2", GetType(Single))
      Me.mHighLimitRegionsTable.Columns.Add("Slope", GetType(Single))
      Me.mHighLimitRegionsTable.Columns.Add("YIntercept", GetType(Single))

      Me.mIdealValueRegionsTable.Columns.Add("RegionNumber", GetType(Int16))
      Me.mIdealValueRegionsTable.Columns.Add("LimitShape", GetType(Int16))
      Me.mIdealValueRegionsTable.Columns.Add("Abscissa1", GetType(Single))
      Me.mIdealValueRegionsTable.Columns.Add("Ordinate1", GetType(Single))
      Me.mIdealValueRegionsTable.Columns.Add("Abscissa2", GetType(Single))
      Me.mIdealValueRegionsTable.Columns.Add("Ordinate2", GetType(Single))
      Me.mIdealValueRegionsTable.Columns.Add("Slope", GetType(Single))
      Me.mIdealValueRegionsTable.Columns.Add("YIntercept", GetType(Single))

      Me.mLowLimitRegionsTable.Columns.Add("RegionNumber", GetType(Single))
      Me.mLowLimitRegionsTable.Columns.Add("LimitShape", GetType(Int16))
      Me.mLowLimitRegionsTable.Columns.Add("Abscissa1", GetType(Single))
      Me.mLowLimitRegionsTable.Columns.Add("Ordinate1", GetType(Single))
      Me.mLowLimitRegionsTable.Columns.Add("Abscissa2", GetType(Single))
      Me.mLowLimitRegionsTable.Columns.Add("Ordinate2", GetType(Single))
      Me.mLowLimitRegionsTable.Columns.Add("Slope", GetType(Single))
      Me.mLowLimitRegionsTable.Columns.Add("YIntercept", GetType(Single))

      Me.mAlphaMPCValue.Columns.Add("ValueIndex", GetType(Int16))
      Me.mAlphaMPCValue.Columns.Add("Value", GetType(String))

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    End Try
  End Sub
#End Region

#Region "Public Property =================================================="
  Public Property Anomaly() As clsAnomaly
    Get
      Return Me.mAnomaly
    End Get
    Set(ByVal value As clsAnomaly)
      Me.mAnomaly = value
    End Set
  End Property

  Public Property HighLimitRegionsTable() As DataTable
    Get
      Return Me.mHighLimitRegionsTable
    End Get
    Set(ByVal value As DataTable)
      Me.mHighLimitRegionsTable = value
    End Set
  End Property

  Public Property IdealValueRegionsTable() As DataTable
    Get
      Return Me.mIdealValueRegionsTable
    End Get
    Set(ByVal value As DataTable)
      Me.mIdealValueRegionsTable = value
    End Set
  End Property

  Public Property LowLimitRegionsTable() As DataTable
    Get
      Return Me.mLowLimitRegionsTable
    End Get
    Set(ByVal value As DataTable)
      Me.mLowLimitRegionsTable = value
    End Set
  End Property

  'Public Property HighLimitRegions() As SortedDictionary(Of Int16, MpcValue)
  '  Get
  '    Return Me.mHighLimitRegions
  '  End Get
  '  Set(ByVal value As SortedDictionary(Of Int16, MpcValue))
  '    Me.mHighLimitRegions = value
  '  End Set
  'End Property

  'Public Property IdealValueRegions() As SortedDictionary(Of Int16, MpcValue)
  '  Get
  '    Return Me.mIdealValueRegions
  '  End Get
  '  Set(ByVal value As SortedDictionary(Of Int16, MpcValue))
  '    Me.mIdealValueRegions = value
  '  End Set
  'End Property

  'Public Property LowLimitRegions() As SortedDictionary(Of Int16, MpcValue)
  '  Get
  '    Return Me.mLowLimitRegions
  '  End Get
  '  Set(ByVal value As SortedDictionary(Of Int16, MpcValue))
  '    Me.mLowLimitRegions = value
  '  End Set
  'End Property

  Public Property AlphaMPCValue() As DataTable
    Get
      Return Me.mAlphaMPCValue
    End Get
    Set(ByVal value As DataTable)
      Me.mAlphaMPCValue = value
    End Set
  End Property

#End Region
End Class
