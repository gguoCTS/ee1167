Option Explicit On
Option Strict Off
Public Class frmPlotPointSizeSelection

  Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
    Try
      gdblPlotPointSizeHeight = txtWidth.Text
      gdblPlotPointSizeWidth = txtHeight.Text
      Me.Close()
    Catch ex As Exception
      MessageBox.Show(ex.Message, "Error")
    End Try
  End Sub
End Class