﻿Public Class clsActuatorProductionStations

    Public Enum Names
        Rework = 0
        Open1 = 1
        SoftwareDownload = 2
        Open3 = 3
        LeakTest = 4
        APSCalibration = 5
        Open6 = 6
        FunctionalTest = 7
        Open8 = 8
		LASERMarker = 9
		Open10 = 10
		Open11 = 11
		Open12 = 12
        Open13 = 13
        Open14 = 14
		Open15 = 15
	End Enum


	Public Enum CovNames
		Rework = 0
		Open1 = 1
		SoftwareDownload = 2
		Open3 = 3
		LeakTest = 4
		APSCalibration = 5
		Open6 = 6
		FunctionalTest = 7
		Open8 = 8
		CTTDownload = 9
		Open10 = 10
		LASERMarker = 11
		Open12 = 12
		Open13 = 13
		Open14 = 14
		Open15 = 15
	End Enum


	Public Sub HWLineCodeStatus(ByVal HWLine1Upper As Byte,
									 ByVal HWLine1Lower As Byte,
									 ByVal HWLine2Upper As Byte,
									 ByVal HWLine2Lower As Byte,
									 ByRef ReadyForStation As String,
									 ByRef StartedStation As String,
									 ByRef FailedAtStation As String,
									 ByRef AllStationsPass As Boolean,
									 ByRef WithRework As Boolean)

		If (gCanActuator.TargetActuator = clsCANActuator.ActuatorVariantEnum.COVINGTON) Then
			HWLineCodeStatusCov(HWLine1Upper,
								HWLine1Lower,
								HWLine2Upper,
								HWLine2Lower,
								ReadyForStation,
								StartedStation,
								FailedAtStation,
								AllStationsPass,
								WithRework)
		Else
			HWLineCodeStatusLys(HWLine1Upper,
								HWLine1Lower,
								HWLine2Upper,
								HWLine2Lower,
								ReadyForStation,
								StartedStation,
								FailedAtStation,
								AllStationsPass,
								WithRework)
		End If
	End Sub

	Public Sub HWLineCodeStatusLys(ByVal HWLine1Upper As Byte,
									 ByVal HWLine1Lower As Byte,
									 ByVal HWLine2Upper As Byte,
									 ByVal HWLine2Lower As Byte,
									 ByRef ReadyForStation As String,
									 ByRef StartedStation As String,
									 ByRef FailedAtStation As String,
									 ByRef AllStationsPass As Boolean,
									 ByRef WithRework As Boolean)

		Dim strStation As String
		Dim Started As Boolean = False
		Dim Passed As Boolean = False
		Dim enumValues As Array = System.[Enum].GetValues(GetType(Names))

		For Each Station As Names In enumValues 'Check each station against the supplied code
			Select Case Station
				Case 1 To 7 'Lower Bytes
					If Not Station.ToString.Contains("Open") Then
						strStation = Station.ToString
						Started = (Not HWLine1Lower And 2 ^ Station) > 0
						Console.WriteLine(strStation & " " & Started)
						If Not Started Then

							ReadyForStation = strStation
							Exit For
						Else 'Start Bit is set for this station
							Passed = (Not HWLine2Lower And 2 ^ Station) > 0
							If Not Passed Then
								FailedAtStation = strStation
								Exit For
							End If
						End If
					End If

				Case 8 To 15 'Upper Bytes 
					If Not Station.ToString.Contains("Open") Then
						strStation = Station.ToString
						Started = (Not HWLine1Upper And 2 ^ (Station - 8)) > 0
						Console.WriteLine(strStation & " " & Started)
						If Not Started Then
							ReadyForStation = strStation
							Exit For
						Else 'Start Bit is set for this station
							Passed = (Not HWLine2Upper And 2 ^ (Station - 8)) > 0
							If Not Passed Then
								FailedAtStation = strStation
								Exit For
							End If
						End If
						'No more station Left
						AllStationsPass = True
					End If


			End Select


		Next


		WithRework = Not HWLine1Lower And (2 ^ Names.Rework) Or
					  Not HWLine2Lower And (2 ^ Names.Rework)

	End Sub


	Public Sub HWLineCodeStatusCov(ByVal HWLine1Upper As Byte,
								 ByVal HWLine1Lower As Byte,
								 ByVal HWLine2Upper As Byte,
								 ByVal HWLine2Lower As Byte,
								 ByRef ReadyForStation As String,
								 ByRef StartedStation As String,
								 ByRef FailedAtStation As String,
								 ByRef AllStationsPass As Boolean,
								 ByRef WithRework As Boolean)

		Dim strStation As String
		Dim Started As Boolean = False
		Dim Passed As Boolean = False
		Dim enumValues As Array = System.[Enum].GetValues(GetType(CovNames))
		Dim IsLast As Boolean = False

		For Each Station As CovNames In enumValues 'Check each station against the supplied code
			Select Case Station
				Case 1 To 7 'Lower Bytes
					If Not Station.ToString.Contains("Open") Then
						strStation = Station.ToString
						Started = (Not HWLine1Lower And 2 ^ Station) > 0
						Console.WriteLine(strStation & " " & Started)
						If Not Started Then

							ReadyForStation = strStation
							Exit For
						Else 'Start Bit is set for this station
							Passed = (Not HWLine2Lower And 2 ^ Station) > 0
							If Not Passed Then
								FailedAtStation = strStation
								Exit For
							End If
						End If
					End If

				Case 8 To 15 'Upper Bytes 
					If Not Station.ToString.Contains("Open") Then
						strStation = Station.ToString
						Started = (Not HWLine1Upper And 2 ^ (Station - 8)) > 0
						Console.WriteLine(strStation & " " & Started)
						If Not Started Then
							ReadyForStation = strStation
							Exit For
						Else 'Start Bit is set for this station
							Passed = (Not HWLine2Upper And 2 ^ (Station - 8)) > 0
							If Not Passed Then
								FailedAtStation = strStation
								Exit For
							End If
						End If
						'No more station Left

						If IsLast Then
							AllStationsPass = True
						Else
							IsLast = True
						End If

					End If


			End Select


		Next


		WithRework = Not HWLine1Lower And (2 ^ CovNames.Rework) Or
					  Not HWLine2Lower And (2 ^ CovNames.Rework)

	End Sub
End Class

