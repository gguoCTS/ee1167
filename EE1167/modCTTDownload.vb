﻿Imports System.IO
Imports EE1167.Peak.Can.Basic

Module modCTTDownload

	Public Enum UDS_ServiceID As Byte
		StartDiagSession = &H10
		Reset = &H11
		ClearDiagnosticInfo = &H14
		ReadDTCInformation = &H19
		ReadDataByIdentifier = &H22
		ReadMemoryByAddress = &H23
		WriteDataByIdentifier = &H2E
		SecurityAccess = &H27
		CommControl = &H28
		RoutineControl = &H31
		ReqDownload = &H34
		ReqUpload = &H35
		TransferData = &H36
		TransferExit = &H37
		WriteMemoryByAddress = &H3D
		TesterPresent = &H3E
		SecureDataTransfer = &H84
		ControlDTCSetting = &H85
		ResponseOnEvent = &H86
		LinkControl = &H87
	End Enum

	Public Const CalStartAddr As Integer = 83968
	Public Const CalEndAddr As Integer = 86015 * 2 + 1
	Public Const AppStartAddr As Integer = 16384 * 2
	Public Const AppEndAddr As Integer = 81919
	Public Const AppHWConfigAddr As Integer = 83922
	Public Const ChecksumCalcAddrStart As Integer = 33792
	Public Const ChecksumCalcAddrEnd As Integer = 167911
	Public Const ChecksumAddrOffset As Integer = 167928
	Public Const CRCAddrOffset As Integer = 167920

	Public Const CUMMINS_START_CRC As UInt32 = &H8000
	Public Const CUMMINS_END_CRC As UInt32 = &H277FF
	Public Const CUMMINS_INIT_CRC As UInt32 = &HFFFFFFFFUI
	Public Const CUMMINS_CRC32_POLY As UInt32 = &HEDB88320UI

	Public Const CUMMINS_CRC_MSB As UInt32 = &H27FFA
	Public Const CUMMINS_CRC_B2 As UInt32 = &H27FFC
	Public Const CUMMINS_CRC_B1 As UInt32 = &H27FFD
	Public Const CUMMINS_CRC_LSB As UInt32 = &H27FFE

	Public Const CUMMINS_SW_CRC As UInt32 = &H447332B3

	Public Const CTS_SW_CCBL_CRC_OFFSET As UInt32 = &H27FF8
	Public Const CTS_GENB_CRC_MSB As UInt32 = &H27FFD
	Public Const CTS_GENB_CRC_B2 As UInt32 = &H27FFC
	Public Const CTS_GENB_CRC_B1 As UInt32 = &H27FF9
	Public Const CTS_GENB_CRC_LSB As UInt32 = &H27FF8

	Public Const Cy_BufferFill As Byte = &HFF
	Public Const Cy_CumminsBufferFill As Byte = &H0
	Public Const Cg_dsPIC_GenB_MemoryImageSize As UInt32 = 786432
	Public Const C_UDS_CTT_TOOLSEED0 As UInteger = &H46500000
	Public Const C_UDS_CTT_TOOLSEED1 As UInteger = 0 '&H00B50000
	Public Const C_UDS_CTT_TOOLSEED2 As UInteger = 0 '&H14120A19

	Public Const CTT_DELTA As UInteger = 2654435769 '&H9E3779B9
	Public Const CTT_KEY0 As UInteger = &H546C2748
	Public Const CTT_KEY1 As UInteger = &H73313350
	Public Const CTT_KEY2 As UInteger = &H642F3A63
	Public Const CTT_KEY3 As UInteger = &H6A622774

	Public Function HexCharToByte(ByVal digit As Char) As Byte

		Dim Result As Byte

		Select Case digit
			Case "0"c
				Result = 0
			Case "1"c
				Result = 1
			Case "2"c
				Result = 2
			Case "3"c
				Result = 3
			Case "4"c
				Result = 4
			Case "5"c
				Result = 5
			Case "6"c
				Result = 6
			Case "7"c
				Result = 7
			Case "8"c
				Result = 8
			Case "9"c
				Result = 9
			Case "a"c, "A"c
				Result = 10
			Case "b"c, "B"c
				Result = 11
			Case "c"c, "C"c
				Result = 12
			Case "d"c, "D"c
				Result = 13
			Case "e"c, "E"c
				Result = 14
			Case "f"c, "F"c
				Result = 15
			Case Else
				Result = 255
		End Select
		Return Result

	End Function



	Private Sub SetMemoryImageBufferToErased(ByRef GenB_MemoryImage As Byte())
		Dim lPtr As UInt64
		Dim lBufferFill As Byte = Cy_BufferFill
		Dim isCumminsSW As Boolean = False

		Try
			If (isCumminsSW) Then
				lBufferFill = Cy_CumminsBufferFill
			Else
				lBufferFill = Cy_BufferFill
			End If

			For lPtr = 0 To Cg_dsPIC_GenB_MemoryImageSize - 1 Step 4
				GenB_MemoryImage(lPtr) = lBufferFill
				GenB_MemoryImage(lPtr + 1) = lBufferFill
				GenB_MemoryImage(lPtr + 2) = lBufferFill
				GenB_MemoryImage(lPtr + 3) = 0
			Next lPtr

		Catch ex As Exception
			Console.WriteLine("SetMemoryImageBufferToErased()  Caught Exception: " & ex.ToString)
		End Try
	End Sub

	Public Function ReadCTTHexFile(DownloadFilePath As String) As Byte()
		Dim ChecksumAddrOffset = AppEndAddr - 3
		Dim CRCAddrOffset = AppEndAddr - 7

		If Not File.Exists(DownloadFilePath) Then
			System.Windows.Forms.MessageBox.Show("Download File Path" & DownloadFilePath & " does not exist!")
			Throw New Exception("Error: Download File not exist")
		End If

		Dim DownloadStreamReader = New IO.StreamReader(DownloadFilePath)
		Dim EOF As Boolean = False
		Dim FileSize As Integer = 0
		Dim strRecord As String
		Dim RecordArray As String()
		Dim RecordType As UShort
		Dim LinearAddr As UInt64
		Dim ExtendedSegAddr As UInt64
		Dim lgWord As ULong
		Dim FlashAddr As ULong
		Dim BufStart As ULong
		Dim LineChecksum As ULong


		Const Cw_dsPIC33FlashPageSize As UInt32 = 3072
		Const Cg_LastValidBufferSize As UInt32 = (Cg_dsPIC_GenB_MemoryImageSize - Cw_dsPIC33FlashPageSize) + 1
		Dim GenB_MemoryImage(Cg_dsPIC_GenB_MemoryImageSize) As Byte

		If (DownloadStreamReader.BaseStream.Length <> 0) Then
			SetMemoryImageBufferToErased(GenB_MemoryImage)
			Dim g_HighestReadAddr As ULong = 0
			Dim g_LowestReadAddr As ULong = &HFFFFFFFFFF
			While Not EOF
				'Reset Variables
				FileSize = 0
				Dim lcalcCC As ULong = 0
				RecordType = 0
				strRecord = ""
				'read line from file
				strRecord = DownloadStreamReader.ReadLine()
				'remove whitespace
				Trim(strRecord)
				strRecord = Strings.Right(strRecord, (strRecord.Length - 1))
				ReDim RecordArray((strRecord.Length / 2 - 1))
				'split string into bytes
				For i As UInt32 = 0 To RecordArray.Length - 1
					RecordArray(i) = Mid(strRecord, i * 2 + 1, 2)
					If i < (RecordArray.Length - 1) Then
						lcalcCC += CUInt("&H" & RecordArray(i))
					End If
				Next

				'Data length
				Dim BytesInRecord = CType("&H" & RecordArray(0), UInt16)
				'Address
				Dim StartAddr = CType("&H" & RecordArray(1) & RecordArray(2), UInt16)
				Try
					'Record Type
					RecordType = CType("&H" & RecordArray(3), UInt16)
					Select Case RecordType
						Case IntelDataRecord.HEXRecordType.DataRecord
							'0 - Data RecordTry
							If (LinearAddr > &H10000) Then
								lgWord = LinearAddr
							End If
							FlashAddr = (StartAddr + ExtendedSegAddr + LinearAddr)
							If (FlashAddr > g_HighestReadAddr) Then
								g_HighestReadAddr = FlashAddr + BytesInRecord - 1
							End If
							If (FlashAddr < g_LowestReadAddr) Then
								g_LowestReadAddr = FlashAddr
							End If

							BufStart = FlashAddr

							'Load data into buffer
							If (BufStart < Cg_LastValidBufferSize) Then
								FileSize += BytesInRecord
								For w As UInt32 = 0 To (BytesInRecord - 1) Step 4
									GenB_MemoryImage(BufStart) = CByte("&H" & RecordArray(w + 4))
									BufStart += 1
									GenB_MemoryImage(BufStart) = CByte("&H" & RecordArray(w + 5))
									BufStart += 1
									GenB_MemoryImage(BufStart) = CByte("&H" & RecordArray(w + 6))
									BufStart += 1
									GenB_MemoryImage(BufStart) = CByte("&H" & RecordArray(w + 7))
									BufStart += 1
								Next w
							Else
								Console.WriteLine("Read_dsPIC33IntelHexFile() Buffer Size Exceeded" + BufStart + vbCrLf)
							End If

						Case IntelDataRecord.HEXRecordType.EOFRecord
							'1 - End of File
							EOF = True
						Case IntelDataRecord.HEXRecordType.ExtendedSegmentAddress
							'02 - Extended Segment Address (Bits 4-19)
							ExtendedSegAddr = (CType("&H" & RecordArray(4) & RecordArray(5), UInt16)) * 16
						Case IntelDataRecord.HEXRecordType.StartSegmentAddress
							' 3- 
							MsgBox("Invalid Record Type Found In Hex File", MsgBoxStyle.SystemModal + MsgBoxStyle.OkOnly, "ReadIntelHexFile Error")
							Exit Try
						Case IntelDataRecord.HEXRecordType.ExtendedLinearAddress
							'4 - Extended Linear Address - Upper 16 bits of address
							Try
								LinearAddr = (CType("&H" & RecordArray(4) & RecordArray(5), UInt16)) * 65536
							Catch ex As Exception
								Console.WriteLine("Read_dsPIC33IntelHexFile(Select Case)  Caught Exception: " & ex.ToString)
								LinearAddr = ((HexCharToByte(RecordArray(4)) * 256) + HexCharToByte(RecordArray(5))) * 65536
							End Try
							If (LinearAddr > &H10000) Then
								lgWord = LinearAddr
							End If
						Case Else
							MsgBox("Invalid Record Type Found In Hex File", MsgBoxStyle.SystemModal + MsgBoxStyle.OkOnly, "ReadIntelHexFile Error")
							Exit Try
					End Select
				Catch ex As Exception
					Console.WriteLine("Read_dsPIC33IntelHexFile(Select Case)  Caught Exception: " & ex.ToString)
				End Try
				LineChecksum = CType("&H" & RecordArray(BytesInRecord + 4), UInt16)
				'Verify Checksum
				lcalcCC = (lcalcCC And &HFF)
				If lcalcCC <> 0 Then
					lcalcCC = &H100 - lcalcCC
				End If
				If lcalcCC <> LineChecksum Then
					MsgBox("Calculated line checksum does " & vbCrLf & "not match embedded line checksum.", MsgBoxStyle.SystemModal + MsgBoxStyle.OkOnly, "ReadIntelHexFile Error")
				End If

			End While
		End If
		Return GenB_MemoryImage

	End Function

	Public Sub CheckCumminsCRC(ByRef GenB_MemoryImage As Byte())
		' Read CRC from memory location
		Dim CRCHex = ReadCCBL_CRC(GenB_MemoryImage)
		Dim calcCRC = CUMMINS_INIT_CRC

		For i As UInt32 = CUMMINS_START_CRC To CUMMINS_END_CRC Step 4
			Dim byteCRC = GetByteForCumminsCRC((calcCRC Xor GenB_MemoryImage(i)) And &HFF)
			calcCRC = (calcCRC >> 8) Xor byteCRC

			byteCRC = GetByteForCumminsCRC((calcCRC Xor GenB_MemoryImage(i + 1)) And &HFF)
			calcCRC = (calcCRC >> 8) Xor byteCRC

			byteCRC = GetByteForCumminsCRC((calcCRC Xor GenB_MemoryImage(i + 2)) And &HFF)
			calcCRC = (calcCRC >> 8) Xor byteCRC

			byteCRC = GetByteForCumminsCRC((calcCRC Xor (GenB_MemoryImage(i + 2) \ 256)) And &HFF)
			calcCRC = (calcCRC >> 8) Xor byteCRC
		Next
		If calcCRC <> CRCHex Then
			Throw New Exception("CRC Error in hex memory map")
		End If
	End Sub

	Public Sub dsPIC33AppChksumFromMemoryImage(ByRef GenB_MemoryImage As Byte())
		Dim lgSum As UInt64
		Dim lgWord As UInt32
		Dim w As UInt64
		Dim lyArray As Byte() = BitConverter.GetBytes(0)

		Dim lgStart As UInt32 = ChecksumCalcAddrStart
		Dim lgEnd As UInt32 = ChecksumCalcAddrEnd

		Const lStepSize = 4
		'Read Checksum from Memory Image Buffer
		lgSum = GenB_MemoryImage(ChecksumAddrOffset + 5)
		lgSum = (lgSum << 24)
		lgSum += (GenB_MemoryImage(ChecksumAddrOffset + 4) * 65536)


		lgSum += (GenB_MemoryImage(ChecksumAddrOffset + 1) * 256)
		lgSum += GenB_MemoryImage(ChecksumAddrOffset)
		Dim FileChksum = lgSum

		'Calculate Checksum 
		Dim dsPIC_CalcChksum = 0
		Dim gCalcChksum = 0
		lgSum = 0

		For w = lgStart To lgEnd Step lStepSize
			lgWord = (GenB_MemoryImage(w + 2) * 65536) + (GenB_MemoryImage(w + 1) * 256) + GenB_MemoryImage(w)
			lgSum += lgWord
			lgSum = Int(lgSum And &HFFFFFFFF)
		Next w
		lgSum = (lgSum And &HFFFFFFFF)
		gCalcChksum = lgSum

		lyArray = BitConverter.GetBytes(gCalcChksum)
		lgSum = lyArray(3)
		lgSum = (lgSum << 24)
		lgSum += (lyArray(2) * 65536)
		lgSum += (lyArray(1) * 256)
		lgSum += lyArray(0)
		dsPIC_CalcChksum = lgSum
		If (dsPIC_CalcChksum <> FileChksum) Then
			Throw New Exception("Error: Calculated checksum does not match with file checksum!")
		End If

	End Sub

	Public Function dsPIC33AppCRCfromMemoryImage(ByRef GenB_MemoryImage As Byte()) As UInt64
		Dim FileCRC As UInt64

		'Read CRC from Memory Image Buffer
		FileCRC = GenB_MemoryImage(CRCAddrOffset + 4)
		FileCRC = (FileCRC << 24)
		FileCRC += (GenB_MemoryImage(CRCAddrOffset + 3) * 65536)
		FileCRC += (GenB_MemoryImage(CRCAddrOffset + 1) * 256)
		FileCRC += GenB_MemoryImage(CRCAddrOffset)
		Return FileCRC

	End Function


	Private Function GetByteForCumminsCRC(ByVal NextByte As Byte) As UInt32
		Dim lcrc As UInt32 = NextByte
		Dim lPoly As UInt32 = CUMMINS_CRC32_POLY
		Dim i As Byte = 0
		For i = 0 To 7
			If ((lcrc And &H1) = 1) Then
				lcrc = (lcrc >> 1) Xor lPoly
			Else
				lcrc = lcrc >> 1
			End If
		Next i

		Return lcrc
	End Function

	Public Function ReadCCBL_CRC(ByRef GenB_MemoryImage As Byte()) As UInt32
		'Dim lCRC = GenB_MemoryImage(CUMMINS_CRC_MSB) * 65536 * 256
		'lCRC += (GenB_MemoryImage(CUMMINS_CRC_B2) * 65536)
		'lCRC += (GenB_MemoryImage(CUMMINS_CRC_B1) * 256)
		'lCRC += GenB_MemoryImage(CUMMINS_CRC_LSB)

		Dim lCRC = GenB_MemoryImage(CTS_GENB_CRC_MSB) * 65536 * 256
		lCRC += (GenB_MemoryImage(CTS_GENB_CRC_B2) * 65536)
		lCRC += (GenB_MemoryImage(CTS_GENB_CRC_B1) * 256)
		lCRC += GenB_MemoryImage(CTS_GENB_CRC_LSB)

		Return lCRC
	End Function
End Module
