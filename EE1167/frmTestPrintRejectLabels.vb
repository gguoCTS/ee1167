Public Class frmTestPrintRejectLabels
    
    Private Sub btnCell4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCell4.Click
        Dim clsIM As New clsIntermecRejectLabel
        clsIM.rejectLabelStructure.BOM = txtBoxBOM.Text '"07547"
        clsIM.rejectLabelStructure.Voltage = txtVoltage.text '"12V"
        clsIM.rejectLabelStructure.Serial = txtSerial.Text '"FE12DAE1234"
        clsIM.rejectLabelStructure.PrintDateTime = txtDateTime.Text '"08JAN14 19:02"
        clsIM.rejectLabelStructure.ProcessName = txtProcess.text '"APSCAL1"
        clsIM.rejectLabelStructure.Description = txtDescription.text '"26; The actuator failed to respond to a request for calculated application checksum.; ; ; ;"

        Call clsIM.PrintRejectLabel()
    End Sub


End Class
