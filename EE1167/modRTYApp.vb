﻿Imports System.IO

Module modRTYApp

  Public gstrRTYAssemblyLine As String

  Public gstrRTYCurrentShift As String
  Public gstrRTYCurrentHrNumber As String

  Public gstrRTYFailData(10, 2) As String

  Public gintRTYHourlyDataPassed As Integer
  Public gintRTYHourlyDataFailed As Integer

  Public gintRTYLastHour As Integer
  Public gintRTYLastShift As Integer
  Public gstrRTYLastBOM As String

  Public gRTYMachines() As RTYMachineInfo
  Public gdtRTYMachines As DataTable
  Public gstrRTYMachineName As String
  Public gdtRTYMachineConfig As DataTable

  Public gstrRTYProcessDate As String
  Public gstrRTYLastProcessDate As String

  Public gintRTYTotalPassed As Integer
  Public gintRTYTotalFailed As Integer

  Public gintRTYShiftTotalPassed As Integer
  Public gintRTYShiftTotalFailed As Integer

  Public gintRTYPreviousShiftTotalPassed As Integer
  Public gintRTYPreviousShiftTotalFailed As Integer

  Public gRTYTimer1 As RTYTimerStruct
  Public gRTYTimer2 As RTYTimerStruct
  Public gRTYTimer3 As RTYTimerStruct
  Public gRTYTimer4 As RTYTimerStruct
  Public gRTYTimer5 As RTYTimerStruct


  Structure RTYAddressStruct
    Dim Name As String
    Dim address As String
    Dim value As String
    Dim type As String
    Dim id As String
  End Structure

  Structure RTYMachineInfo
    Dim MachineName As String
    Dim MachineID As String
    Dim MachineOpName As String
    Dim data() As RTYAddressStruct
  End Structure

  Structure RTYTimerStruct
    Dim Machine As String
    Dim Shift As String
    Dim Hour As String
    Dim BOM As String
    Dim Count As String
    Dim Value As Double
    Dim DayofYear As String
  End Structure

  Public Sub CheckRTYFailCounts()
    Dim intFailureMode As Integer
    Dim intloop As Integer
    Dim tempData(2) As String
    Try

      If gControlFlags.TestFailure Then
        LogActivity("Checking Fail Counts")
        Select Case Mid(gstrRoutingMessage, 1, 21)
          Case "Wrong Bits Entering S", "Part Started APSCalib", "Part Ready for Functi", _
               "Take Part to Bit Rese", "Take Part to Bit Rese", "Take Part to Leak (A2", _
               "Take Part to APSCAL (", "Take Part to Function", "Take Part to Laser (A", _
               "Contact PC; this part"

            intFailureMode = 1

          Case "Part Failed for Burn ", "Part Failed for HW Te"
            intFailureMode = 2
          Case "Part Failed for APSCA"
            intFailureMode = 3

          Case "Failed Calibration Ch"
            intFailureMode = 4
          Case "Failed Download Versi"
            intFailureMode = 5
          Case "Part Failed to set St"
            intFailureMode = 6
          Case "Part Failed to Set Pa"
            intFailureMode = 7

          Case "Part Failed Power On "
            intFailureMode = 8
          Case "Part Failed Post Test"
            intFailureMode = 9
          Case "Part Failed Calibrate"
            intFailureMode = 10

          Case "Part Failed Evaluate "
            intFailureMode = 11

          Case "Part Failed Read All "
            intFailureMode = 12

          Case "Part Failed Get Fault"
            intFailureMode = 13

          Case "Part Failed Reload So"
            intFailureMode = 14

          Case "Part Failed Downloadi"
            intFailureMode = 15

          Case "Upload Program Failed"
            intFailureMode = 16

          Case "Download Program Fail"
            intFailureMode = 17

          Case "Reload Program Failed"
            intFailureMode = 18

          Case Else
            intFailureMode = 99
            Dim fs As FileStream
            Dim br As StreamWriter

            Dim filename As String
            filename = "C:\EE's\99Codes.txt"
            fs = New FileStream(filename, FileMode.Append, FileAccess.Write, FileShare.None)
            br = New StreamWriter(fs)

            br.WriteLine(gstrRoutingMessage)
            br.WriteLine()

            br.Close()
        End Select

        For intloop = 1 To 10
          If Val(gstrRTYFailData(intloop, 1)) = intFailureMode Then
            gstrRTYFailData(intloop, 2) = gstrRTYFailData(intloop, 2) + 1
            Exit For
          ElseIf gstrRTYFailData(intloop, 1) = "" Then
            gstrRTYFailData(intloop, 1) = intFailureMode
            gstrRTYFailData(intloop, 2) = 1
            Exit For
          End If
        Next
        'Sort Arrayo
        LogActivity("Fail Code = " & intFailureMode)
      End If

      For intloop = 1 To 9
        If gstrRTYFailData(intloop + 1, 2) > gstrRTYFailData(intloop, 2) Then
          'swap
          tempData(1) = gstrRTYFailData(intloop, 1)
          tempData(2) = gstrRTYFailData(intloop, 2)

          gstrRTYFailData(intloop, 1) = gstrRTYFailData(intloop + 1, 1)
          gstrRTYFailData(intloop, 2) = gstrRTYFailData(intloop + 1, 2)

          gstrRTYFailData(intloop + 1, 1) = tempData(1)
          gstrRTYFailData(intloop + 1, 2) = tempData(2)
        End If
      Next

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub GetRTYShiftAndHourCounts()
    Dim fs As FileStream
    Dim br As StreamReader
    Dim strFilename As String
    Try
      strFilename = "C:\EE's\ShiftAndHour.txt"

      'Open the Serial Number File
      If File.Exists(strFilename) Then
        fs = New FileStream(strFilename, FileMode.Open, FileAccess.ReadWrite, FileShare.None)
        br = New StreamReader(fs)

        gstrRTYCurrentShift = br.ReadLine
        gstrRTYCurrentHrNumber = br.ReadLine
        gintRTYHourlyDataPassed = br.ReadLine
        gintRTYHourlyDataFailed = br.ReadLine
        gintRTYTotalPassed = br.ReadLine
        gintRTYTotalFailed = br.ReadLine
        gstrRTYFailData(1, 1) = br.ReadLine
        gstrRTYFailData(1, 2) = br.ReadLine
        gstrRTYFailData(2, 1) = br.ReadLine
        gstrRTYFailData(2, 2) = br.ReadLine
        gstrRTYFailData(3, 1) = br.ReadLine
        gstrRTYFailData(3, 2) = br.ReadLine
        gstrRTYFailData(4, 1) = br.ReadLine
        gstrRTYFailData(4, 2) = br.ReadLine
        gstrRTYFailData(5, 1) = br.ReadLine
        gstrRTYFailData(5, 2) = br.ReadLine
        gstrRTYFailData(6, 1) = br.ReadLine
        gstrRTYFailData(6, 2) = br.ReadLine
        gstrRTYFailData(7, 1) = br.ReadLine
        gstrRTYFailData(7, 2) = br.ReadLine
        gstrRTYFailData(8, 1) = br.ReadLine
        gstrRTYFailData(8, 2) = br.ReadLine
        gstrRTYFailData(9, 1) = br.ReadLine
        gstrRTYFailData(9, 2) = br.ReadLine
        gstrRTYFailData(10, 1) = br.ReadLine
        gstrRTYFailData(10, 2) = br.ReadLine
        gintRTYShiftTotalPassed = br.ReadLine
        gintRTYShiftTotalFailed = br.ReadLine

        gintRTYPreviousShiftTotalPassed = br.ReadLine
        gintRTYPreviousShiftTotalFailed = br.ReadLine
        gstrRTYLastProcessDate = br.ReadLine

        br.Close()

      End If

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub PrepareRTYData()

    Dim intMachineCount As Integer
    Dim intItems As Integer
    Dim intLoop As Integer
    Dim intLoop2 As Integer
    Dim strMachineID As String
    Dim strMachineName As String
    Dim dtShiftHour As New DataTable
    Dim strShiftStart As String = ""
    Dim strShiftEnd As String = ""
    Try

      LogActivity("Getting Shift n Hour Counts")

      Call GetRTYShiftAndHourCounts()

      dtShiftHour = gDatabase.GetRTYCurrentShiftAndHour(gstrRTYAssemblyLine)

      strShiftStart = dtShiftHour.Rows(0).Item(columnName:="ShiftStartTime") ' dtShiftHour.Rows(0).Item(0)
      strShiftEnd = dtShiftHour.Rows(0).Item(columnName:="ShiftEndTime") '  dtShiftHour.Rows(0).Item(1)
      gstrRTYCurrentShift = dtShiftHour.Rows(0).Item(columnName:="CurrentShift") 'dtShiftHour.Rows(0).Item(2)
      gstrRTYCurrentHrNumber = dtShiftHour.Rows(0).Item(columnName:="CurrentHour") ' dtShiftHour.Rows(0).Item(3)
      gstrRTYProcessDate = dtShiftHour.Rows(0).Item(columnName:="JulianDate")

      gdtRTYMachines = gDatabase.GetRTYMachineData(gstrRTYAssemblyLine)

      If gstrRTYLastBOM = "" Then gstrRTYLastBOM = gDeviceInProcess.BOM
      If gintRTYLastShift = 0 Then gintRTYLastShift = gstrRTYCurrentShift
      If gintRTYLastHour = 0 Then gintRTYLastHour = gstrRTYCurrentHrNumber
      If gintRTYLastShift <> gstrRTYCurrentShift Or gstrRTYProcessDate <> gstrRTYLastProcessDate Then
        gintRTYPreviousShiftTotalPassed = gintRTYShiftTotalPassed
        gintRTYPreviousShiftTotalFailed = gintRTYShiftTotalFailed
        gintRTYShiftTotalPassed = 0
        gintRTYShiftTotalFailed = 0
        gstrRTYLastProcessDate = gstrRTYProcessDate
        Call SaveRTYShiftAndHourCounts()
      End If


      'If gintRTYLastShift <> gstrRTYCurrentShift Or gstrRTYLastBOM <> Mid(frmTsopMain.cboParameterSets.Text, 1, 5) Then
      If gintRTYLastShift <> gstrRTYCurrentShift Or gstrRTYLastBOM <> gDeviceInProcess.BOM Then
        'reset shift counts
        gintRTYHourlyDataPassed = 0
        gintRTYHourlyDataFailed = 0
        gintRTYTotalPassed = 0
        gintRTYTotalFailed = 0

        'reset Shift Fail
        For intLoop = 1 To 10
          gstrRTYFailData(intLoop, 1) = ""
          gstrRTYFailData(intLoop, 2) = ""
        Next
        gintRTYLastShift = gstrRTYCurrentShift
        'LastBOM = Mid(frmTsopMain.cboParameterSets.Text, 1, 5)
        gstrRTYLastBOM = gDeviceInProcess.BOM
      End If

      If gintRTYLastHour <> gstrRTYCurrentHrNumber Then
        'reset shift counts
        gintRTYHourlyDataPassed = 0
        gintRTYHourlyDataFailed = 0
        gintRTYLastHour = gstrRTYCurrentHrNumber
      End If

      'If Not gblnTestFailure And Not gblnAbort Then
      If Not gControlFlags.TestFailure And Not gControlFlags.AbortTest Then
        gintRTYHourlyDataPassed = gintRTYHourlyDataPassed + 1
        gintRTYTotalPassed = gintRTYTotalPassed + 1
        gintRTYShiftTotalPassed = gintRTYShiftTotalPassed + 1

      ElseIf gControlFlags.TestFailure Then
        gintRTYTotalFailed = gintRTYTotalFailed + 1
        gintRTYHourlyDataFailed = gintRTYHourlyDataFailed + 1
        gintRTYShiftTotalFailed = gintRTYShiftTotalFailed + 1

      End If

      gOverviewScreenControls(OverviewScreenControls.ShiftPassed).Text = gintRTYShiftTotalPassed
      gOverviewScreenControls(OverviewScreenControls.ShiftFailed).Text = gintRTYShiftTotalFailed

      Call CheckRTYFailCounts()

      intMachineCount = gdtRTYMachines.Rows.Count

      ReDim gRTYMachines(intMachineCount)

      intItems = 0
      LogActivity("Building Data Table")

      For intLoop = 0 To intMachineCount - 1
        If gdtRTYMachines.Rows(intLoop).Item(columnName:="Machinename") = gstrRTYMachineName And gdtRTYMachines.Rows(intLoop).Item(columnName:="AssemblyLineID") = gstrRTYAssemblyLine Then
          LogActivity("Found Line and Machine")
          strMachineID = gdtRTYMachines.Rows(intLoop).Item(columnName:="MachineID") 'dtMachines.Rows.Item(intLoop)(0).ToString
          strMachineName = gdtRTYMachines.Rows(intLoop).Item(columnName:="MachineName") 'dtMachines.Rows.Item(intLoop)(1).ToString
          gRTYMachines(intLoop).MachineName = strMachineName
          gRTYMachines(intLoop).MachineID = strMachineID
          'Load items for each machine
          gdtRTYMachineConfig = gDatabase.GetRTYMachineDataItem(strMachineID)


          ReDim gRTYMachines(intLoop).data(gdtRTYMachineConfig.Rows.Count)
          LogActivity("Building Data Items")
          For intLoop2 = 0 To gdtRTYMachineConfig.Rows.Count - 1
            'Build overall arrays
            gRTYMachines(intLoop).data(intLoop2).id = gdtRTYMachineConfig.Rows(intLoop2).Item(columnName:="MachineDataItemID") ' dtMachineConfig.Rows.Item(intLoop2)(0).ToString
            gRTYMachines(intLoop).data(intLoop2).Name = gdtRTYMachineConfig.Rows(intLoop2).Item(columnName:="DataItemName") 'dtMachineConfig.Rows.Item(intLoop2)(1).ToString

          Next
          'save data
          LogActivity("Calling Save Data Routine")
          Call SaveRTYDatatoDatabase(gRTYMachines(intLoop))

          If gAnomaly IsNot Nothing Then
            Throw New TsopAnomalyException
          End If

          Exit For
        End If
      Next
      SaveRTYShiftAndHourCounts()
    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveRTYDatatoDatabase(ByRef Machine As RTYMachineInfo)

    Dim dtDataTable As DataTable = New DataTable()
    Dim column As DataColumn
    Dim column2 As DataColumn
    Dim row As DataRow
    Dim intLoop As Integer
    Dim strTest As String = ""
    Dim strBOM As String = ""
    Dim strResult As String = ""

    Try

      ' Create first column and add to the DataTable.
      column = New DataColumn()
      column.DataType = System.Type.GetType("System.Int32")
      column.ColumnName = "MachineDataItemID"
      ' column.AutoIncrement = True
      column.Caption = "MachineDataItemID"

      ' Add the column to the DataColumnCollection.
      dtDataTable.Columns.Add(column)

      column2 = New DataColumn()
      strTest = column2.DataType.ToString
      column2.DataType = System.Type.GetType("System.String")
      strTest = column2.DataType.ToString
      column2.ColumnName = "RTYOEEDATAITEMValue"
      'column2.AutoIncrement = True
      column2.Caption = "RTYOEEDATAITEMValue"

      ' Add the column to the DataColumnCollection.
      dtDataTable.Columns.Add(column2)

      For intLoop = 0 To Machine.data.GetUpperBound(0) - 1

        row = dtDataTable.NewRow()

        row("MachineDataItemID") = Machine.data(intLoop).id


        Select Case Machine.data(intLoop).Name
          Case "CurrentHrNumber"
            row("RTYOEEDATAITEMValue") = gstrRTYCurrentHrNumber
            Machine.data(intLoop).value = gstrRTYCurrentHrNumber
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: Hour: " & gstrRTYCurrentHrNumber)
          Case "CurrentShift"
            row("RTYOEEDATAITEMValue") = gstrRTYCurrentShift
            Machine.data(intLoop).value = gstrRTYCurrentShift
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: Shift: " & gstrRTYCurrentShift)
          Case "DateTime"
            row("RTYOEEDATAITEMValue") = Format(Now, "HH:mm:ss MM/dd/yyyy")
            Machine.data(intLoop).value = Format(Now, "HH:mm:ss MM/dd/yyyy")
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: Date/Time: " & Now)
          Case "RTYBOM"
            'strBOM = Mid(frmTsopMain.cboParameterSets.Text, 1, 5)
            strBOM = gDeviceInProcess.BOM
            row("RTYOEEDATAITEMValue") = strBOM
            Machine.data(intLoop).value = strBOM
            dtDataTable.Rows.Add(row)

            LogActivity("Building Data Table: BOM: " & strBOM)
          Case "RTYHourlyFailed"
            row("RTYOEEDATAITEMValue") = gintRTYHourlyDataFailed
            Machine.data(intLoop).value = gintRTYHourlyDataFailed
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: Hr Failed: " & gintRTYHourlyDataFailed)
          Case "RTYHourlyPassed"
            row("RTYOEEDATAITEMValue") = gintRTYHourlyDataPassed
            Machine.data(intLoop).value = gintRTYHourlyDataPassed
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: Hr Passed: " & gintRTYHourlyDataPassed)
          Case "RTYQTY"
            row("RTYOEEDATAITEMValue") = 1
            Machine.data(intLoop).value = 1
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: QTY: " & 1)

          Case "RTYSortCode"

          Case "RTYT1BBTime"
            'row("RTYOEEDATAITEMValue") = CInt(frmTsopMain.lblButtonToButton.Text * 100)
            row("RTYOEEDATAITEMValue") = CInt(gOverviewScreenControls(OverviewScreenControls.ButtonToButton).Text * 100)
            'Machine.data(intLoop).value = CInt(frmTsopMain.lblButtonToButton.Text * 100)
            Machine.data(intLoop).value = CInt(gOverviewScreenControls(OverviewScreenControls.ButtonToButton).Text * 100)
            gRTYTimer1.Machine = Machine.data(intLoop).Name
            'Timer1.Value = Timer1.Value + CInt(frmTsopMain.lblButtonToButton.Text * 100)
            gRTYTimer1.Value = gRTYTimer1.Value + CInt(gOverviewScreenControls(OverviewScreenControls.ButtonToButton).Text * 100)
            gRTYTimer1.Count = gRTYTimer1.Count + 1
            dtDataTable.Rows.Add(row)
            'LogActivity("Building Data Table: RTYT1BBTime: " & CInt(frmTsopMain.lblButtonToButton.Text * 100))
            LogActivity("Building Data Table: RTYT1BBTime: " & CInt(gOverviewScreenControls(OverviewScreenControls.ButtonToButton).Text * 100))


          Case "RTYMachineTime"

          Case "RTYTotalFailed"
            row("RTYOEEDATAITEMValue") = gintRTYShiftTotalFailed
            Machine.data(intLoop).value = gintRTYShiftTotalFailed
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: Total Failed: " & gintRTYShiftTotalFailed)

          Case "RTYTotalPassed"
            row("RTYOEEDATAITEMValue") = gintRTYShiftTotalPassed
            Machine.data(intLoop).value = gintRTYShiftTotalPassed
            dtDataTable.Rows.Add(row)
            LogActivity("Building Data Table: Total Passed: " & gintRTYShiftTotalPassed)

          Case "RTYFailCode1"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(1, 1)
            Machine.data(intLoop).value = gstrRTYFailData(1, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode2"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(2, 1)
            Machine.data(intLoop).value = gstrRTYFailData(2, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode3"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(3, 1)
            Machine.data(intLoop).value = gstrRTYFailData(3, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode4"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(4, 1)
            Machine.data(intLoop).value = gstrRTYFailData(4, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode5"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(5, 1)
            Machine.data(intLoop).value = gstrRTYFailData(5, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode6"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(6, 1)
            Machine.data(intLoop).value = gstrRTYFailData(6, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode7"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(7, 1)
            Machine.data(intLoop).value = gstrRTYFailData(7, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode8"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(8, 1)
            Machine.data(intLoop).value = gstrRTYFailData(8, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode9"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(9, 1)
            Machine.data(intLoop).value = gstrRTYFailData(9, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCode10"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(10, 1)
            Machine.data(intLoop).value = gstrRTYFailData(10, 1)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount1"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(1, 2)
            Machine.data(intLoop).value = gstrRTYFailData(1, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount2"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(2, 2)
            Machine.data(intLoop).value = gstrRTYFailData(2, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount3"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(3, 2)
            Machine.data(intLoop).value = gstrRTYFailData(3, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount4"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(4, 2)
            Machine.data(intLoop).value = gstrRTYFailData(4, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount5"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(5, 2)
            Machine.data(intLoop).value = gstrRTYFailData(5, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount6"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(6, 2)
            Machine.data(intLoop).value = gstrRTYFailData(6, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount7"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(7, 2)
            Machine.data(intLoop).value = gstrRTYFailData(7, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount8"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(8, 2)
            Machine.data(intLoop).value = gstrRTYFailData(8, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount9"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(9, 2)
            Machine.data(intLoop).value = gstrRTYFailData(9, 2)
            dtDataTable.Rows.Add(row)
          Case "RTYFailCount10"
            row("RTYOEEDATAITEMValue") = gstrRTYFailData(10, 2)
            Machine.data(intLoop).value = gstrRTYFailData(10, 2)
            dtDataTable.Rows.Add(row)

          Case Else
            row("RTYOEEDATAITEMValue") = 0
            Machine.data(intLoop).value = 0
            dtDataTable.Rows.Add(row)
        End Select
      Next

      'machine data item id
      'value
      strResult = gDatabase.InsertRTYDataCollected(dtDataTable, Machine.MachineID, Now, gstrRTYCurrentShift, strBOM, gstrRTYCurrentHrNumber, gstrRTYProcessDate)
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If
      LogActivity("RTY Data put in Database " & Machine.MachineID & "," & Now & "," & gstrRTYCurrentShift & "," & strBOM & "," & gstrRTYCurrentHrNumber & "," & gstrRTYProcessDate & "," & strResult)

      strResult = gDatabase.InsertRTYDataCollectedTimers(1, Machine.MachineID, strBOM, gstrRTYCurrentShift, gstrRTYCurrentHrNumber, gstrRTYProcessDate, _
                                                         gRTYTimer1.Value, gRTYTimer2.Value, gRTYTimer3.Value, gRTYTimer4.Value, gRTYTimer5.Value)
      If gDatabase.Anomaly IsNot Nothing Then
        gAnomaly = gDatabase.Anomaly
        gDatabase.Anomaly = Nothing
        Throw New TsopAnomalyException
      End If
      Call SaveRTYShiftAndHourCounts()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub

  Public Sub SaveRTYShiftAndHourCounts()
    Dim filename As String
    Dim fs As FileStream
    Dim br As StreamWriter
    Try

      filename = "C:\EE's\ShiftAndHour.txt"

      'Open the Serial Number File
      ' If File.Exists(filename) Then
      fs = New FileStream(filename, FileMode.Create, FileAccess.Write)
      br = New StreamWriter(fs)

      br.WriteLine(gstrRTYCurrentShift)
      br.WriteLine(gstrRTYCurrentHrNumber)
      br.WriteLine(gintRTYHourlyDataPassed)
      br.WriteLine(gintRTYHourlyDataFailed)
      br.WriteLine(gintRTYTotalPassed)
      br.WriteLine(gintRTYTotalFailed)
      br.WriteLine(gstrRTYFailData(1, 1))
      br.WriteLine(gstrRTYFailData(1, 2))
      br.WriteLine(gstrRTYFailData(2, 1))
      br.WriteLine(gstrRTYFailData(2, 2))
      br.WriteLine(gstrRTYFailData(3, 1))
      br.WriteLine(gstrRTYFailData(3, 2))
      br.WriteLine(gstrRTYFailData(4, 1))
      br.WriteLine(gstrRTYFailData(4, 2))
      br.WriteLine(gstrRTYFailData(5, 1))
      br.WriteLine(gstrRTYFailData(5, 2))
      br.WriteLine(gstrRTYFailData(6, 1))
      br.WriteLine(gstrRTYFailData(6, 2))
      br.WriteLine(gstrRTYFailData(7, 1))
      br.WriteLine(gstrRTYFailData(7, 2))
      br.WriteLine(gstrRTYFailData(8, 1))
      br.WriteLine(gstrRTYFailData(8, 2))
      br.WriteLine(gstrRTYFailData(9, 1))
      br.WriteLine(gstrRTYFailData(9, 2))
      br.WriteLine(gstrRTYFailData(10, 1))
      br.WriteLine(gstrRTYFailData(10, 2))
      br.WriteLine(gintRTYShiftTotalPassed)
      br.WriteLine(gintRTYShiftTotalFailed)

      br.WriteLine(gintRTYPreviousShiftTotalPassed)
      br.WriteLine(gintRTYPreviousShiftTotalFailed)

      br.WriteLine(gstrRTYLastProcessDate)
      br.Close()

    Catch ex As TsopAnomalyException
      gAnomaly.AppendCallerList(gStackTrace.CurrentClassName & "." & gStackTrace.CurrentFunctionName)
    Catch ex As Exception
      gAnomaly = New clsDbAnomaly(10001, gStackTrace.CurrentClassName, gStackTrace.CurrentFunctionName, ex.Message, gDatabase)
    End Try
  End Sub


End Module
