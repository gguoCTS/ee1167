﻿Imports System.Xml
Imports System.IO



Public Class clsDutPower

#Region "Constructors =================================================="
  Public Sub New(ByVal configFilePath As String, ByVal strPowerSupplyUsed As String)
    Try

      Me.mstrClassConfigFilePath = configFilePath

            Me.mSerialPort = New System.IO.Ports.SerialPort

            mstrPowerSupplyUsed = strPowerSupplyUsed

            'Read configuration settings from file
            GetConfigurationSettings()
      If mAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

      Call InitializeCommunication()
      If mAnomaly IsNot Nothing Then
        Throw New TsopAnomalyException
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

#End Region


#Region "Private Member Variables =================================================="
  Private mAnomaly As clsAnomaly

  Private mstrClassConfigFilePath As String

  Private mstrCommandOutputOn As String
  Private mstrCommandOutputOff As String
  Private mstrCommandReadVoltage As String
  Private mstrCommandSetCurrent As String
  Private mstrCommandSetVoltage As String
  Private mchrCommandTerminationCharacter As Char

  Private mdblCurrentOutputOffsetFactor As Double
  Private mdblCurrentOutputScalingFactor As Double

  Private mdblHighestVoltage As Double
  Private mdblLowestVoltage As Double

  Private mdblMaxSupplyCurrent As Double
  Private mdblMaxSupplyVoltage As Double

  Private mdblOverriddenTestVoltage As Double

  Private mstrPowerSupplyUsed As String
  Private mintReadBufferSize As Integer
  Private mintReadTimeout As Integer

    Private mSerialPort As System.IO.Ports.SerialPort
    Private mstrSerialPortName As String
    Private mintSerialPortBaudRate As Integer
    Private mintSerialPortParity As IO.Ports.Parity
  Private mintSerialPortStopBits As IO.Ports.StopBits

  Private mStackTrace As New clsStackTrace
  Private mStopWatch As New clsStopWatch

  Private mblnTestVoltageIsOverridden As Boolean

  Private mdblVoltageOutputOffsetFactor As Double
  Private mdblVoltageOutputScalingFactor As Double

  Private mintWriteBufferSize As Integer
  Private mintWriteTimeout As Integer



#End Region


#Region "Public Constants, Structures, Enums =================================================="

#End Region


#Region "Properties =================================================="
  Public Property Anomaly() As clsAnomaly
    Get
      Return Me.mAnomaly
    End Get
    Set(ByVal value As clsAnomaly)
      Me.mAnomaly = value
    End Set
  End Property

  Public Property CurrentOutputOffsetFactor As Double
    Get
      Return mdblCurrentOutputOffsetFactor
    End Get
    Set(value As Double)
      mdblCurrentOutputOffsetFactor = value
    End Set
  End Property

  Public Property CurrentOutputScalingFactor As Double
    Get
      Return mdblCurrentOutputScalingFactor
    End Get
    Set(value As Double)
      mdblCurrentOutputScalingFactor = value
    End Set
  End Property

  Public Property HighestVoltage As Double
    Get
      Return mdblHighestVoltage
    End Get
    Set(value As Double)
      mdblHighestVoltage = value
    End Set
  End Property

  Public Property LowestVoltage As Double
    Get
      Return mdblLowestVoltage
    End Get
    Set(value As Double)
      mdblLowestVoltage = value
    End Set
  End Property

  Public Property OverriddenTestVoltage As Double
    Get
      Return mdblOverriddenTestVoltage
    End Get
    Set(value As Double)
      mdblOverriddenTestVoltage = value
    End Set
  End Property

    Public Property SerialPort() As System.IO.Ports.SerialPort
        Get
            Return mSerialPort
        End Get
        Set(ByVal value As System.IO.Ports.SerialPort)
            mSerialPort = value
        End Set
    End Property

    Public Property SerialPortBaudRate() As Integer
    Get
      Return Me.mintSerialPortBaudRate
    End Get
    Set(ByVal value As Integer)
      Me.mintSerialPortBaudRate = value
    End Set
  End Property

  Public Property SerialPortParity() As IO.Ports.Parity
    Get
      Return Me.mintSerialPortParity
    End Get
    Set(ByVal value As IO.Ports.Parity)
      Me.mintSerialPortParity = value
    End Set
  End Property

  Public Property SerialPortStopBits() As IO.Ports.StopBits
    Get
      Return Me.mintSerialPortStopBits
    End Get
    Set(ByVal value As IO.Ports.StopBits)
      Me.mintSerialPortStopBits = value
    End Set
  End Property

  Public Property TestVoltageIsOverridden As Boolean
    Get
      Return mblnTestVoltageIsOverridden
    End Get
    Set(value As Boolean)
      mblnTestVoltageIsOverridden = value
    End Set
  End Property

  Public Property VoltageOutputOffsetFactor As Double
    Get
      Return mdblVoltageOutputOffsetFactor
    End Get
    Set(value As Double)
      mdblVoltageOutputOffsetFactor = value
    End Set
  End Property

  Public Property VoltageOutputScalingFactor As Double
    Get
      Return mdblVoltageOutputScalingFactor
    End Get
    Set(value As Double)
      mdblVoltageOutputScalingFactor = value
    End Set
  End Property


#End Region


#Region "Private Methods =================================================="
  Private Sub GetConfigurationSettings()
    Try
      Dim configReader As XmlTextReader
      Dim configSet As New DataSet
      Dim configTable As DataTable = Nothing
      Dim configRow As DataRow
      Dim strClassName As String

      strClassName = Me.GetType.Name

      'Create the XML Reader
      configReader = New XmlTextReader(Me.mstrClassConfigFilePath & strClassName & ".cfg")

      'Clear data set that holds xml file contents
      configSet.Clear()
      'Read the file
      configSet.ReadXml(configReader)

      ''Config File not hierarchical, only 1 table and 1 row, with many fields
      'configTable = configSet.Tables(0)
      'configRow = configTable.Rows(0)

      'Config File contains multiple tables
      For Each dt As DataTable In configSet.Tables
        If dt.TableName = mstrPowerSupplyUsed Then
          configTable = dt
          Exit For
        End If
      Next
      'configTable = configSet.Tables(0)
      configRow = configTable.Rows(0)

      'Assign field values to variables
      mstrSerialPortName = configRow("SerialPortName")
      mintSerialPortBaudRate = configRow("SerialPortBaudRate")
      mintSerialPortParity = DirectCast([Enum].Parse(GetType(IO.Ports.Parity), configRow("SerialPortParity")), Integer)
      mintSerialPortStopBits = DirectCast([Enum].Parse(GetType(IO.Ports.StopBits), configRow("SerialPortStopBits")), Integer)

      mintReadBufferSize = configRow("ReadBufferSize")
      mintReadTimeout = configRow("ReadTimeout")
      mintWriteBufferSize = configRow("WriteBufferSize")
      mintWriteTimeout = configRow("WriteTimeout")

      mdblMaxSupplyCurrent = configRow("MaxSupplyCurrent")
      mdblMaxSupplyVoltage = configRow("MaxSupplyVoltage")

      mdblCurrentOutputOffsetFactor = configRow("CurrentOutputOffsetFactor")
      mdblCurrentOutputScalingFactor = configRow("CurrentOutputScalingFactor")
      mdblVoltageOutputOffsetFactor = configRow("VoltageOutputOffsetFactor")
      mdblVoltageOutputScalingFactor = configRow("VoltageOutputScalingFactor")

      mstrCommandOutputOn = configRow("CommandOutputOn")
      mstrCommandOutputOff = configRow("CommandOutputOff")
      mstrCommandReadVoltage = configRow("CommandReadVoltage")
      mstrCommandSetCurrent = configRow("CommandSetCurrent")
      mstrCommandSetVoltage = configRow("CommandSetVoltage")
      mchrCommandTerminationCharacter = Chr(configRow("CommandTerminationCharacter"))

      'Close reader, releases file
      configReader.Close()

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try

  End Sub

  Private Sub InitializeCommunication()

    Try

      'Make sure the port is closed
      If mSerialPort.IsOpen = True Then
        mSerialPort.Close()
      End If

      mSerialPort.PortName = Me.mstrSerialPortName
      mSerialPort.BaudRate = Me.mintSerialPortBaudRate
      mSerialPort.Parity = Me.mintSerialPortParity
      mSerialPort.StopBits = Me.mintSerialPortStopBits

      mSerialPort.ReadBufferSize = Me.mintReadBufferSize
      mSerialPort.ReadTimeout = Me.mintReadTimeout
      mSerialPort.WriteBufferSize = Me.mintWriteBufferSize
      mSerialPort.WriteTimeout = Me.mintWriteTimeout

      mSerialPort.Open()  'Open COM Port

      'mSerialPort.Write("LOC0" & vbCrLf)

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

#End Region


#Region "Public Methods =================================================="

  Public Function ReadSupplyVoltage() As Double
    Dim lStopWatch As New clsStopWatch
    Dim strVout As String = ""
    Dim strPortRead As String
    Dim strResponse() As String
    Dim intLoop As Integer
    Dim dblMeasureSupplyVoltage As Double
    Dim strErrorMessage As String
    Try


      If Not mSerialPort.IsOpen Then
        mSerialPort.Open()
      End If

      mSerialPort.DiscardInBuffer()

            'if this times out the first timee, do it again once
            Dim repeatcount As Integer = 0
            Dim elapse As Integer = 0
            Do Until repeatcount > 20
                'Send command to measure output voltage 
                mSerialPort.Write(mstrCommandReadVoltage & mchrCommandTerminationCharacter)

                'Loop until all bytes have been written
                Do Until mSerialPort.BytesToWrite = 0
          System.Windows.Forms.Application.DoEvents()
          Threading.Thread.Sleep(1)
        Loop

                'Loop until bytes are available at port

                'NEED a timeout here ########################################################################################################
                lStopWatch.Start()

                Do Until mSerialPort.BytesToRead <> 0 Or elapse > 1000
          System.Windows.Forms.Application.DoEvents()
          Threading.Thread.Sleep(1)
          elapse = lStopWatch.ElapsedMilliseconds
                Loop
                If mSerialPort.BytesToRead <> 0 Then
                    repeatcount = 100

                    'Read Voltage from port
                    strPortRead = mSerialPort.ReadTo(mchrCommandTerminationCharacter) 'Read up to termination character

                    'Some power supplies echo the command back along with the measured voltage
                    'Find numeric portion of response
                    strResponse = strPortRead.Split(" ")
                    For intLoop = 0 To strResponse.Length - 1
                        If IsNumeric(strResponse(intLoop)) Then
                            strVout = strResponse(intLoop)
                            Exit For
                        End If
                    Next
                Else
                    repeatcount += 1
                End If
            Loop

            If strVout <> "" Then
        dblMeasureSupplyVoltage = CDbl(strVout)
      Else
        strErrorMessage = "Unable to Read Power Supply Voltage"
        mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, strErrorMessage)
        Throw New TsopAnomalyException
      End If

      Return dblMeasureSupplyVoltage

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
      Return Nothing
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
      Return Nothing
    Finally
    End Try

  End Function

  Public Sub SetSupplyVoltage(ByVal Voltage As Double)

    Try

      If Not mSerialPort.IsOpen Then
        mSerialPort.Open()
      End If

      mSerialPort.DiscardInBuffer()

      If Voltage = 0 Then
        mSerialPort.Write(mstrCommandOutputOff & " " & mchrCommandTerminationCharacter)
        mSerialPort.Write(mstrCommandSetCurrent & " " & 0 & mchrCommandTerminationCharacter)
        mSerialPort.Write(mstrCommandSetVoltage & " " & 0 & mchrCommandTerminationCharacter)
      Else
        mSerialPort.Write(mstrCommandSetCurrent & " " & mdblMaxSupplyCurrent & mchrCommandTerminationCharacter)
        mSerialPort.Write(mstrCommandSetVoltage & " " & Voltage & mchrCommandTerminationCharacter)
        mSerialPort.Write(mstrCommandOutputOn & " " & mchrCommandTerminationCharacter)
      End If

    Catch ex As TsopAnomalyException
      mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
    Catch ex As Exception
      mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
    Finally
    End Try
  End Sub

  'Public Sub SetSupplyVoltage(ByVal Voltage As Double)
  '  'Dim lstrVout As String
  '  Try

  '    'frmTsopMain.comPowerSupply.DiscardInBuffer()
  '    mSerialPort.DiscardInBuffer()

  '    '       If gdblDUTPowerSupplyUsed = "BK" Then
  '    If gdblDUTPowerSupplyUsed = "BK9123A" Then
  '      If Voltage = 0 Then
  '        mSerialPort.Write("OUTP OFF" & vbCrLf)
  '      Else
  '        mSerialPort.Write("SOUR:CURR " & gstrDUTPowerSupplyMaxCurrent & vbCrLf) 'Max current of supply 
  '        mSerialPort.Write("SOUR:VOLT " & Voltage & vbCrLf)
  '        mSerialPort.Write("OUTP ON " & vbCrLf)
  '      End If

  '    Else
  '      If Voltage = 0 Then
  '        mSerialPort.Write("OUT OFF" & vbCr)

  '      Else
  '        Voltage = Voltage '+ 0.1
  '        mSerialPort.Write("ISET " & 10 & vbCr) 'Max current of supply 
  '        mSerialPort.Write("VSET " & Voltage & vbCr)
  '        mSerialPort.Write("OUT ON" & vbCr)
  '      End If

  '      'check that supply was set properly
  '      mStopWatch.DelayTime(100)
  '      mSerialPort.DiscardInBuffer()

  '      Do
  '        mSerialPort.Write("VOUT?" & vbCr)
  '        lstrVout = gDUTPowerSupplySerialPort.ReadTo(vbCr)
  '        lstrVout = Mid(lstrVout, 6, lstrVout.Length)
  '        lstrVout = Math.Round(CSng(lstrVout), 1)
  '      Loop Until lstrVout = Voltage Or Voltage < 0.5

  '    End If
  '  Catch ex As TsopAnomalyException
  '    mAnomaly.AppendCallerList(mStackTrace.CurrentClassName & "." & mStackTrace.CurrentFunctionName)
  '  Catch ex As Exception
  '    mAnomaly = New clsAnomaly(10001, mStackTrace.CurrentClassName, mStackTrace.CurrentFunctionName, ex.Message)
  '  Finally
  '  End Try
  'End Sub

#End Region


End Class
